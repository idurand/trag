;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin, Irène Durand

(asdf:defsystem
  :autograph-web
  :description "A web UI for Autograph"
  :version "0.0"
  :author "Michael Raskin <raskin@mccme.ru>, Irène Durand <idurand@labri.fr>"
  :depends-on (
               :autograph :trag-web
               :hunchentoot :cl-emb :smackjack
               :cl-ppcre :esrap-peg :cl-json :alexandria
               :external-program :trivial-backtrace
               :bordeaux-threads :local-time
               )
  :components
  (
   (:file "package")
   (:file "util" :depends-on ("package"))
   (:file "graph-terms" :depends-on ("package" "util"))
   (:file "js-definitions" :depends-on ("package"))
   (:file "js-functions" :depends-on ("package" "js-definitions"))
   (:file "lisp-operations" :depends-on ("package" "graph-terms"))
   (:file "js-commands" :depends-on ("package" "js-definitions" "js-functions"))
   (:file "commands" :depends-on ("package" "js-definitions" "js-functions" "graph-terms" "lisp-operations" "js-commands"))
   (:file "menus" :depends-on ("package" "js-definitions"))
   (:file "page" :depends-on ("package" "graph-terms" "js-definitions" "js-functions" "commands" "menus"))
   ))
