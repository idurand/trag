(in-package :autograph-web)

(defcommand-with-backend
    (ag-ids::stable-automaton ag-ids::cwd-automaton *autograph-web-js*)
    () ()
  t
  (autograph:stable-automaton)
  (set-pane-current "automaton" (create-op-id)))

(defcommand-with-backend
  (ag-ids::cwd-automaton-recognizes-p ag-ids::string *autograph-web-js* :js-name "Recognize term")
  () (term automaton)
  (and
   (get-pane-current "automaton")
   (get-pane-current "term")
   (term-and-automaton-compatible-p))
  (if (termauto:recognized-p term automaton)
    "Recognized" "Not recognized")
  (set-pane-current "result"
                    (create-op-id (get-pane-current "term")
                                  (get-pane-current "automaton"))
                    (lambda () (show-pane-time "result"))))
(defcommand-with-backend
  (ag-ids::cwd-automaton-enum-recognizes-p ag-ids::string *autograph-web-js* :js-name "Enum recognize term")
  () (term automaton)
  (and
   (get-pane-current "automaton")
   (get-pane-current "term")
   (term-and-automaton-compatible-p)
   (not (automaton-deterministic-p)))
  (if (termauto:enum-recognized-p term automaton)
    "Recognized" "Not recognized")
  (set-pane-current "result"
                    (create-op-id (get-pane-current "term")
                                  (get-pane-current "automaton"))
                    (lambda () (show-pane-time "result"))))

(defcommand-with-backend
  (ag-ids::compute-target ag-ids::string *autograph-web-js* :js-name "Compute Target")
  () (term automaton)
  (and
   (get-pane-current "automaton")
   (get-pane-current "term")
   (term-and-automaton-compatible-p))
  (format nil "Target state: ~a" (termauto:compute-target term automaton))
  (set-pane-current "result"
                    (create-op-id (get-pane-current "term")
                                  (get-pane-current "automaton"))
                    (lambda () (show-pane-time "result"))))

(defcommand-with-backend
  (ag-ids::compute-sharp ag-ids::integer *autograph-web-js* :js-name "Compute #")
  () (term automaton)
  (and
   (get-pane-current "term")
   (get-pane-current "automaton")
   (or (term-transducer-ok)
       (and
	(not (automaton-deterministic-p))
	(term-and-automaton-compatible-p))))
  (termauto:compute-count term automaton)
  (set-pane-current "result"
                    (create-op-id (get-pane-current "term")
                                  (get-pane-current "automaton"))
                    (lambda () (show-pane-time "result"))))

(defcommand-with-backend
  (ag-ids::compute-sp ag-ids::string *autograph-web-js* :js-name "Compute Sp")
  () (term automaton)
  (term-transducer-ok)
  (format nil "~A" (termauto:compute-spectrum term automaton))
  (set-pane-current "result"
                    (create-op-id (get-pane-current "term")
                                  (get-pane-current "automaton"))))

(defcommand-with-backend
  (ag-ids::compute-msp ag-ids::string *autograph-web-js* :js-name "Compute MSp")
  () (term automaton)
  (term-transducer-ok)
  (format nil "~A" (termauto:compute-multi-spectrum term automaton))
  (set-pane-current "result"
                    (create-op-id (get-pane-current "term")
                                  (get-pane-current "automaton"))))

(defcommand-with-backend
  (ag-ids::compute-sat ag-ids::string *autograph-web-js* :js-name "Compute Sat")
  () (term automaton)
  (term-transducer-ok)
  (format nil "~A" (termauto:compute-sat term automaton))
  (set-pane-current "result"
                    (create-op-id (get-pane-current "term")
                                  (get-pane-current "automaton"))))

(defcommand-with-backend
    (ag-ids::kacyclic-colorability-automaton ag-ids::cwd-automaton *autograph-web-js*)
    ((k 'prompt "Number of colors: ")) ()
  t
  (autograph::k-acyclic-colorability-automaton k)
  (set-pane-current "automaton" (create-op-id (create-immediate-identifier 'integer k))))

(defcommand-with-backend
    (ag-ids::graph-hamiltonian ag-ids::string *autograph-web-js*)
    () (graph)
  (get-pane-current "graph")
  (format nil "Hamiltonian: ~A~%" (if (autograph::graph-hamiltonian-p graph) "Yes" "No"))
  (set-pane-current "result" (create-op-id (get-pane-current "graph"))))

(defcommand-with-backend
  (ag-ids::show-colorings ag-ids::multiple-strings *autograph-web-js*)
  ((k 'prompt "Number of colors: "))
  (term)
  (and
    (get-pane-current "term")
    (term-plain-p))
  ;;  (format nil "~a" (autograph::k-colorings term k))
  (loop for c in (autograph::k-colorings term k)
        collect (with-output-to-string (s) (autograph::format-k-coloring k c s)))
  (set-pane-current "result"
                    (create-op-id
                      (get-pane-current "term")
                      (create-immediate-identifier 'integer k))))

(defcommand-with-backend
  (ag-ids::show-one-coloring ag-ids::string *autograph-web-js*)
  ((k 'prompt "Number of colors: "))
  (term)
  (and
    (get-pane-current "term")
    (term-plain-p))
  (with-output-to-string (s) (autograph::format-k-coloring k (autograph::k-coloring k term) s))
  (set-pane-current "result"
                    (create-op-id
                      (get-pane-current "term")
                      (create-immediate-identifier 'integer k))))

(defcommand-with-backend
    (ag-ids::count-colorings ag-ids::integer *autograph-web-js* :js-name "Count colorings")
    ((k 'prompt "Number of colors: "))
    (term)
    (and
     (get-pane-current "term")
     (term-plain-p))
    (autograph::count-colorings term k)
    (set-pane-current "result" (create-op-id (get-pane-current "term") (create-immediate-identifier 'integer k))))

(defcommand-with-backend
    (ag-ids::load-dimacs-graph ag-ids::graph *autograph-web-js* :js-name "Load DIMACS graph")
    ((data 'prompt "DIMACS file" 'allow-upload t 'allow-expand t 'hide-input t))
    ()
    t
    (with-input-from-string (input data) (graph::load-dimacs-graph-stream input))
    (set-pane-current
     "graph"
     (create-op-id (create-immediate-identifier 'string data))
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::remove-path ag-ids::graph *autograph-web-js* :js-name "Remove path")
    ((path 'prompt "Path" 'allow-expand t))
    (graph)
    (get-pane-current "graph")
    (graph::graph-remove-epath (strings-to-integers (split-component-string path)) graph)
    (set-pane-current
     "graph"
     (create-op-id (get-pane-current "graph")
                   (create-immediate-identifier 'string path))
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::add-path ag-ids::graph *autograph-web-js* :js-name "Add path")
    ((path 'prompt "Path" 'allow-expand t))
    (graph)
    (get-pane-current "graph")
    (graph:graph-from-paths
      (cons
        (strings-to-integers (split-component-string path))
        (graph:graph-to-paths graph))
        :oriented (general:oriented-p graph))
    (set-pane-current
     "graph"
     (create-op-id (get-pane-current "graph")
                   (create-immediate-identifier 'string path))
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::grunbaum-graph ag-ids::u-graph *autograph-web-js*)
    () () t
    (graph:graph-grunbaum)
    (set-pane-current
     "graph"
     (create-op-id)
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::petersen-graph ag-ids::u-graph *autograph-web-js*)
    () () t
    (graph:graph-petersen)
    (set-pane-current
     "graph"
     (create-op-id)
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::mcgee-graph ag-ids::u-graph *autograph-web-js*)
    () () t
    (graph:graph-mcgee)
    (set-pane-current
     "graph"
     (create-op-id)
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::graph-kn ag-ids::u-graph *autograph-web-js* :js-name "Kn graph")
    ((n 'prompt "n")) () t
    (graph:graph-kn n)
    (set-pane-current
     "graph"
     (create-op-id (create-immediate-identifier 'integer n))
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::graph-pn ag-ids::u-graph *autograph-web-js* :js-name "Pn graph")
    ((n 'prompt "n")) () t
    (graph:graph-pn n)
    (set-pane-current
     "graph"
     (create-op-id (create-immediate-identifier 'integer n))
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::graph-cn ag-ids::u-graph *autograph-web-js* :js-name "Cn graph")
    ((n 'prompt "n")) () t
    (graph:graph-cycle n)
    (set-pane-current
     "graph"
     (create-op-id (create-immediate-identifier 'integer n))
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::graph-gnxm ag-ids::u-graph *autograph-web-js* :js-name "Grid nxm graph")
    ((n 'prompt "n") (m 'prompt "m")) () t
    (graph:graph-rgrid n m)
    (set-pane-current
     "graph"
     (create-op-id (create-immediate-identifier 'integer n) (create-immediate-identifier 'integer m))
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::graph-gnxn ag-ids::u-graph *autograph-web-js* :js-name "Grid nxn graph")
    ((n 'prompt "n")) () t
    (graph:graph-square-grid n)
    (set-pane-current
     "graph"
     (create-op-id (create-immediate-identifier 'integer n))
     (lambda ()
       (set-pane-current "term" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current "result" nil nil))))

(defcommand-with-backend
    (ag-ids::gnxn-term ag-ids::cwd-term *autograph-web-js* :js-name "Gnxm term")
    ((n 'prompt "n") (m 'prompt "m")) () t
    (clique-width::cwd-term-rgrid n m)
    (set-pane-current "term"
     (create-op-id (create-immediate-identifier 'integer n) (create-immediate-identifier 'integer m))
     (lambda ()
       (set-pane-current "graph" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current
	"result"
	(create-immediate-identifier
	 ag-ids::string
	 (+ "Clique width: " (get-pane-current-attribute "term" 'clique-width)))))))

(defcommand-with-backend
    (ag-ids::petersen-term ag-ids::cwd-term *autograph-web-js* :js-name "Petersen term")
    ()
    ()
    t
    (autograph::petersen)
    (set-pane-current
     "term"
     (create-op-id)
     (lambda ()
       (set-pane-current "graph" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current
	"result"
	(create-immediate-identifier
	 ag-ids::string
	 (+ "Clique width: " (get-pane-current-attribute "term" 'clique-width)))))))

(defcommand-with-backend
    (ag-ids::mcgee-term ag-ids::cwd-term *autograph-web-js* :js-name "McGee term")
    ()
    ()
    t
    (autograph::mcgee)
    (set-pane-current
     "term" (create-op-id)
     (lambda ()
       (set-pane-current "graph" nil nil)
       (set-pane-current "display" nil nil)
       (set-pane-current
	"result"
	(create-immediate-identifier
	 ag-ids::string
	 (+ "Clique width: " (get-pane-current-attribute "term" 'clique-width)))))))

(defcommand-with-backend
    (ag-ids::term-clique-width ag-ids::integer *autograph-web-js*)
    ()
    (term)
    (get-pane-current "term")
    (progn (format  *error-output* "coucou") (autograph::clique-width term))
    (set-pane-current "result" (create-op-id (get-pane-current "term"))))

(defcommand-with-backend
    (ag-ids::term-depth ag-ids::integer *autograph-web-js*)
    ()
    (term)
    (get-pane-current "term")
    (terms:term-depth term)
    (set-pane-current "result" (create-op-id (get-pane-current "term"))))

(defcommand-with-backend
    (ag-ids::term-size ag-ids::integer *autograph-web-js*)
    ()
    (term)
    (get-pane-current "term")
    (terms::term-size term)
    (set-pane-current "result" (create-op-id (get-pane-current "term"))))

(defcommand-with-backend
    (ag-ids::random-orientation ag-ids::o-graph *autograph-web-js* :js-name "Random orientation")
    () ;; no additional argument
    (graph)
  (get-pane-current "graph") ;; we need a graph
  (graph:graph-randomize-orientation graph)
  (set-pane-current
   "graph"
   (create-op-id (get-pane-current "graph"))
   (lambda ()
     (set-pane-current "term" nil)
     (set-pane-current "display" nil)
     (set-pane-current "result" nil))))

(defcommand-with-backend
    (ag-ids::complete-orientation ag-ids::o-graph *autograph-web-js* :js-name "Complete orientation")
    () ;; no additional argument
    (graph)
  (get-pane-current "graph") ;; we need a graph
  (graph:graph-complete-orientation graph)
  (set-pane-current
   "graph"
   (create-op-id (get-pane-current "graph"))
   (lambda ()
     (set-pane-current "term" nil)
     (set-pane-current "display" nil)
     (set-pane-current "result" nil))))

(defcommand-with-backend
    (ag-ids::erase-orientation ag-ids::u-graph *autograph-web-js* :js-name "Erase orientation")
    () ;; no additional argument
    (graph)
  (get-pane-current "graph") ;; we need a graph
  (graph-from-paths-string (graph-to-paths-string graph) nil)
  (set-pane-current
   "graph"
   (create-op-id (get-pane-current "graph"))
   (lambda ()
     (set-pane-current "term" nil)
     (set-pane-current "display" nil)
     (set-pane-current "result" nil))))

(defcommand-with-backend
    (ag-ids::heuristic-cwd-decomposition ag-ids::cwd-term *autograph-web-js*)
    ()
    (graph)
    (get-pane-current "graph")
    (decomp:cwd-decomposition graph)
    (set-pane-current
     "term"
     (create-op-id (get-pane-current "graph"))
     (lambda ()
       (set-pane-current
	"result"
	(create-immediate-identifier
	 ag-ids::string
	 (+ "Clique width: " (get-pane-current-attribute "term" 'clique-width)))
        (lambda () (show-pane-time "term"))))))

(defcommand-with-backend
    (ag-ids::heuristic-incidence-cwd-decomposition ag-ids::cwd-term *autograph-web-js*)
    ()
    (graph)
    (get-pane-current "graph")
    (decomp:cwd-decomposition-incidence graph)
    (set-pane-current
     "term"
     (create-op-id (get-pane-current "graph"))
     (lambda ()
       (set-pane-current
	"result"
	(create-immediate-identifier
	 ag-ids::string
	 (+ "Clique width: " (get-pane-current-attribute "term" 'clique-width)))))))

(defcommand-with-backend
    (ag-ids::optimal-incidence-cwd-decomposition ag-ids::cwd-term *autograph-web-js*)
    ()
    (graph)
    (get-pane-current "graph")
    (decomp:cwd-decomposition-incidence graph :optimal t)
    (set-pane-current
     "term"
     (create-op-id (get-pane-current "graph"))
     (lambda ()
       (set-pane-current
	"result"
	(create-immediate-identifier
	 ag-ids::string
	 (+ "Clique width: " (get-pane-current-attribute "term" 'clique-width)))))))

(defcommand-with-backend
    (ag-ids::heuristic-cwd-twd-decomposition ag-ids::cwd-term *autograph-web-js*)
    ()
    (graph)
    (and (get-pane-current "graph") (not (current-graph-oriented-p)))
    (decomp:cwd-twd-decomposition graph)
    (set-pane-current
     "term"
     (create-op-id (get-pane-current "graph"))
     (lambda ()
       (set-pane-current
	"result" 
	(create-immediate-identifier
	 ag-ids::string
	 (+ "Clique width: " (get-pane-current-attribute "term" 'clique-width)))))))

(defcommand-with-backend
  (ag-ids::cwd-decomposition ag-ids::cwd-term *autograph-web-js*)
  ()
  (graph)
;;  (and (get-pane-current "graph") (not (current-graph-oriented-p)))
  (get-pane-current "graph")
  (decomp:cwd-decomposition graph t)
  (chain-async-postfix
    (set-pane-current
      "term"
      (create-op-id (get-pane-current "graph")))
    (set-pane-current
      "result" 
      (create-immediate-identifier
        ag-ids::string
        (+ "Clique width: " (get-pane-current-attribute "term" 'clique-width))))
    (show-pane-time "term")))

(defcommand-with-backend
    (ag-ids::heuristic-twd-decomposition ag-ids::string *autograph-web-js*)
    ()
    (graph)
    (and (get-pane-current "graph") (not (current-graph-oriented-p)))
    (format nil "~A" (tree-decomp::super-twd-decomposition graph))
    (set-pane-current
      "result"
      (create-op-id (get-pane-current "graph"))))

(defcommand-with-backend
    (ag-ids::display-graph ag-ids::svg-image *autograph-web-js*)
    ()
    (graph)
    (get-pane-current "graph")
    (graph:graph-to-svg-string graph)
    (set-pane-current "display"
		      (create-op-id (get-pane-current "graph"))))

(add-parenscript-definitions
  (*autograph-web-js*)
  (defcommand "Input term" ((term 'prompt "Term: " 'allow-expand t))
    (set-pane-current "term"
                      (create-immediate-identifier :cwd-term term)
                      (lambda ()
                        (set-pane-current "graph" nil)
                        (set-pane-current "display" nil)
                        (set-pane-current "result" nil)))))

(add-parenscript-definitions
  (*autograph-web-js*)
  (defcommand "Erase term" ((term 'prompt "Term: " 'allow-expand t))
    (set-pane-current "term" nil
                      (lambda ()
                        (set-pane-current "result" nil)))))

(defcommand-with-backend
  (ag-ids::graph-chromatic-polynomial ag-ids::integer-polynomial *autograph-web-js*)
  () (graph)
  (and (get-pane-current "graph") (not (current-graph-oriented-p)))
  (graph:chromatic-polynomial graph)
  (set-pane-current "result"
    (create-op-id (get-pane-current "graph"))))

(defcommand-with-backend
  (ag-ids::graph-chromatic-value ag-ids::integer *autograph-web-js*)
  ((k 'prompt "Number of colors: ")) (polynomial)
  (and (get-pane-current "graph") (not (current-graph-oriented-p)))
  (graph::polynomial-value polynomial k)
  (set-pane-current "result"
    (create-op-id
      (create-operation-identifier
        :integer-polynomial :graph-chromatic-polynomial
        (list (get-pane-current "graph")))
      (create-immediate-identifier 'integer k))))

(defcommand-with-backend
  (ag-ids::projection-automaton ag-ids::cwd-automaton *autograph-web-js* :js-name "Projection automaton")
  () (automaton)
  (and (get-pane-current "automaton") (decorated-automaton-p))
  (termauto::projection-automaton automaton)
  (set-pane-current "automaton"
   (create-op-id (get-pane-current "automaton"))))

(defcommand-with-backend
    (ag-ids::nothing-to-x1 ag-ids::cwd-automaton *autograph-web-js*)
    () (automaton)
    (and
     (get-pane-current "automaton")
     (not (decorated-automaton-p)))
    (termauto::nothing-to-x1 automaton)
    (set-pane-current "automaton"
		      (create-op-id (get-pane-current "automaton"))))

(defcommand-with-backend
    (ag-ids::nothing-to-xj ag-ids::cwd-automaton *autograph-web-js*)
    ((m 'prompt "Number of variables") (j 'prompt "j")) (automaton)
    (and
     (get-pane-current "automaton")
     (not (decorated-automaton-p)))
    (termauto::nothing-to-xj automaton m j)
    (set-pane-current "automaton"
		      (create-op-id (get-pane-current "automaton"))
		      (create-immediate-identifier ag-ids::integer m)
		      (create-immediate-identifier ag-ids::integer j)))

(defcommand-with-backend
  (ag-ids::formula-automaton ag-ids::cwd-automaton *autograph-web-js*)
  ((f 'prompt "Formula: ")) ()
  t
  (autograph::formula-to-automaton (tptp-syntax:tptp-fof-string-to-formula-object f))
  (set-pane-current "automaton"
    (create-op-id
     (create-immediate-identifier ag-ids::string f))))

(defcommand-with-backend
  (ag-ids::formula-oriented-automaton ag-ids::cwd-automaton *autograph-web-js*)
  ((f 'prompt "Formula: ")) ()
  t
  (autograph::formula-to-automaton (tptp-syntax:tptp-fof-string-to-formula-object f) :orientation t)
  (set-pane-current
    "automaton"
    (create-op-id
     (create-immediate-identifier ag-ids::string f))))

(defcommand-with-backend
  (ag-ids::multi-formula-automaton ag-ids::cwd-automaton *autograph-web-js*)
  ((f 'prompt "Formulae: " 'allow-upload t 'allow-expand t)) ()
  t
  (autograph::multi-formula-to-main-automaton f)
  (set-pane-current
    "automaton"
    (create-op-id
     (create-immediate-identifier ag-ids::string f))))

(defcommand-with-backend
  (ag-ids::multi-formula-oriented-automaton
    ag-ids::cwd-automaton *autograph-web-js*)
  ((f 'prompt "Formulae: " 'allow-upload t 'allow-expand t)) ()
  t
  (autograph::multi-formula-to-main-automaton f :orientation t)
  (set-pane-current
    "automaton"
    (create-op-id
      (create-immediate-identifier ag-ids::string f))))

(defcommand-with-backend
  (ag-ids::renumber-graph-vertices ag-ids::graph *autograph-web-js*)
  () (graph)
  t
  (graph:graph-from-paths 
    (renumber-graph-paths (graph:graph-to-paths graph))
    :oriented (general:oriented-p graph))
  (set-pane-current "graph" (create-op-id (get-pane-current "graph"))))

(defcommand-with-backend
    (ag-ids::compute-add-annotation ag-ids::cwd-term *autograph-web-js*)
    ()
    (term)
    (and
     (get-pane-current "term")
     (not (get-pane-current-attribute "term" 'annotated-p)))
    (autograph::compute-add-annotation term)
    (set-pane-current
     "term"
     (create-op-id (get-pane-current "term"))))

(defcommand-with-backend
    (ag-ids::remove-add-annotation ag-ids::cwd-term *autograph-web-js*)
    ()
    (term)
    (and (get-pane-current "term")
	 (get-pane-current-attribute "term" 'annotated-p))
    (autograph::remove-annotation term)
    (set-pane-current
     "term"
     (create-op-id (get-pane-current "term"))))

(defcommand-with-backend
  (ag-ids::colorability-automaton-with-symmetry-optimisation
    ag-ids::cwd-automaton *autograph-web-js*
    :js-name "Colorability automaton with symmetry optimisation (requires add annotation)")
  ((colors 'prompt "Number of colours"))
  ()
  t
  (autograph::symmetric-colorability-automaton colors 0)
  (set-pane-current
    "automaton"
    (create-op-id
      (create-immediate-identifier :integer colors))))

(defcommand-with-backend
  (ag-ids::input-multi-term
    ag-ids::cwd-term *autograph-web-js*
   )
  ((multi-term-string 'prompt  "multi-term: "))
  ()
  t
  (clique-width::input-cwd-multi-term multi-term-string)
  (set-pane-current
    "term"
    (create-op-id
      (create-immediate-identifier :string multi-term-string))))

(defcommand-with-backend
  (ag-ids::show-multi-term ag-ids::string *autograph-web-js*)
  () (term)
  (get-pane-current "term")
  (terms::term-to-multi-term-string term)
  (set-pane-current "result"
                    (create-op-id (get-pane-current "term"))))

(defcommand-with-backend
    (ag-ids::try-cwd ag-ids::maybe-cwd-term *autograph-web-js*)
    ((cwd 'prompt "cwd"))
    (graph)
    (and
     (get-pane-current "graph")
     (not (current-graph-oriented-p)))
    (decomp::try-cwd-sat-decomposition graph cwd)
    (set-pane-current
      "result"
      (create-op-id
        (get-pane-current "graph")
        (create-immediate-identifier ag-ids::integer cwd))
      (lambda ()
        (if (= (ps:chain (get-pane-current "result") type) :cwd-term)
          (set-pane-current "term" (get-pane-current "result"))
          (set-pane-current "term" nil)))))

(defcommand-with-backend
  (ag-ids::add-vbits-to-term ag-ids::cwd-term *autograph-web-js*)
  ((etis 'prompt "Vertex numbers to get 1"))
  (term)
  (get-pane-current "term")
  (terms:term-add-vbits-by-eti-lists
    term (list (strings-to-integers (split-component-string etis))))
  (set-pane-current "term" (create-op-id 
                             (get-pane-current "term")
                             (create-immediate-identifier
                               ag-ids::string etis))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; defdownloads
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defdownload-with-backend
  (ag-ids::download-current-term "autograph-term" *autograph-web-js*)
  (term)
  (get-pane-current "term")
  (let ((terms::*show-term-eti* t)) (format nil "~a" term))
  (create-op-id (get-pane-current "term")))

(defdownload-with-backend
  (ag-ids::download-multi-term "autograph-term" *autograph-web-js*)
  (term)
  (get-pane-current "term")
  (let ((terms::*show-term-eti* t)) (terms::term-to-multi-term-string term))
  (create-op-id (get-pane-current "term")))

(defdownload-with-backend
  (ag-ids::download-dimacs-graph "dimacs-graph" *autograph-web-js*)
  (graph)
  (get-pane-current "graph")
  (with-output-to-string (s) (graph::save-dimacs-graph-stream graph s))
  (create-op-id (get-pane-current "graph")))
