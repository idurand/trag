(in-package :autograph-web)

(defvar *term-max-depth*)
(setq *term-max-depth* 20)

(defun insert-spaces-into-term-string (term)
  (cl-ppcre:regex-replace-all "([(,)])" (format nil "~a" term) "\\1 "))

(define-object-identifier-type (ag-ids::cwd-term)
  (:lisp-type terms:term)
  (:parser clique-width:input-cwd-term)
  (:canonical-immediate-data (value) (format nil "~a" value))
  (:description
    (value)
    (let (
	  (depth (terms::term-depth value)))
      (if (> depth *term-max-depth*)
        (format nil "The term is too deep (depth: ~A) to be shown entirely:~% ~A"
                depth (insert-spaces-into-term-string (terms::term-depth-prefix value *term-max-depth*)))
        (format nil "Clique Width Term:~% ‘‘~a’’"
                (insert-spaces-into-term-string value)))))
  (:html-description
   (value)
   (let ((terms::*show-term-eti* t)
	 (depth (terms::term-depth value)))
      (if (> depth *term-max-depth*)
        (format nil "The term is too deep (depth: ~A) to be shown entirely:<br/> ~A"
                depth (cl-emb::escape-for-xml
                        (insert-spaces-into-term-string 
                          (terms::term-depth-prefix value *term-max-depth*))))
        (format nil "Clique Width Term:<br/> ‘‘~a’’"
                (cl-emb::escape-for-xml
                  (insert-spaces-into-term-string value))))))
  (:attributes (value)
	       (general:make-key-value
		:clique-width (clique-width:clique-width value)
		:oriented-p (general:oriented-p value)
		:annotated-p (symbols:annotated-symbol-p (terms::root value))
		:nb-set-variables (symbols:signature-nb-set-variables (symbols:signature value))
		:nb-colors (symbols:signature-nb-colors (symbols:signature value))
		:term-depth (terms:term-depth value)
		:term-size (terms:term-size value))))

(define-object-identifier-type (ag-ids::maybe-cwd-term)
  (:lisp-type (or terms:term null))
  (:parse (s) (ignore-errors (clique-width:input-cwd-term s)))
  (:canonical-id (x) (if x (create-immediate-identifier
                             'ag-ids::cwd-term
                             (format nil "~a" x))
                       (create-immediate-identifier
                         'ag-ids::maybe-cwd-term "")))
  (:description (value)
                (if value (canonical-description 'ag-ids::cwd-term value)
                  "A term creation operation has not succeded")))

(defun graph-from-paths-string (string oriented)
  (graph:graph-from-paths
   (split-list-of-lists-of-numbers string) :oriented oriented))

(defun graph-to-paths-string (graph)
  (format nil "~{~{~a ~}; ~}" (graph:graph-to-paths graph)))

(defun renumber-graph-paths (l)
  (let* ((flat (reduce 'append l))
         (numbers (loop with ht := (make-hash-table)
                        with res := nil
                        for n in flat
                        unless (gethash n ht)
                        do (progn (push n res)
                                  (setf (gethash n ht) t))
                        finally (return res)))
         (sorted (sort numbers '<))
         (sorted-index (loop with ht := (make-hash-table)
                             for k upfrom 1
                             for n in sorted
                             do (setf (gethash n ht) k)
                             finally (return ht))))
    (loop for path in l collect
          (loop for n in path collect
                (gethash n sorted-index)))))

(define-object-identifier-type (ag-ids::u-graph)
  (:check (value) (and (typep value 'graph:graph) (not (general:oriented-p value))))
  (:description (value) (format nil "Unoriented graph: ~a" value))
  (:unparser graph-to-paths-string)
  (:parse (string) (graph-from-paths-string string nil)))

(define-object-identifier-type (ag-ids::o-graph)
  (:check (value) (and (typep value 'graph:graph) (general:oriented-p value)))
  (:description (value) (format nil "Oriented graph: ~a" value))
  (:unparser graph-to-paths-string)
  (:parse (string) (graph-from-paths-string string t)))

(define-object-identifier-type (ag-ids::graph)
  (:lisp-type graph:graph)
  (:description (value)
		(format nil "~a graph: ~a"
			(if (general:oriented-p value) "Oriented" "Unoriented") value))
  (:parse (string) (error "An explicitly specified graph should be either oriented or unoriented"))
  (:canonical-id (value)
		 (create-immediate-identifier
		  (if (general:oriented-p value) :o-graph :u-graph)
		  (graph-to-paths-string value))))

(defmethod object-identifier-attributes :around (type (value graph:graph))
  (general:merge-key-value
    (call-next-method)
    (general:make-key-value :oriented-p (general:oriented-p value))))

(define-object-identifier-type (ag-ids::svg-image)
  (:lisp-type string)
  (:parser identity)
  (:canonical-immediate-data (value) value)
  (:description (value) "SVG image")
  (:html-description (value) value))

(define-object-identifier-type (ag-ids::svg-graph)
  (:lisp-type graph:graph)
  (:description
   (graph)
   (format nil "~a graph: ~a"
	   (if (general:oriented-p graph) "Oriented" "Unoriented") graph))
  (:html-description
   (graph)
   (format nil "~a<br>~a" graph (graph:graph-to-svg-string graph)))
  (:parse
   (string)
   (error "An explicitly specified graph should be either oriented or unoriented")))

(define-object-identifier-type (ag-ids::cwd-automaton)
  (:lisp-type termauto::abstract-automaton)
  (:canonical-id (value) nil)
  (:description (value) (format nil "Automaton: ~a" value))
  (:parse (string) (error "An automaton cannot be specified directly"))
  (:attributes (value)
     (general:make-key-value
       :deterministic-p (termauto:deterministic-p value)
       :oriented-p (general:oriented-p (symbols:signature value))
       :nb-set-variables (termauto::signature-nb-set-variables (termauto::signature value))
       :nb-colors (termauto::signature-nb-colors (termauto::signature value)))))

(defun polynomial-to-string (value)
  (cl-ppcre:regex-replace-all
    "[+] [-]"
    (format
      nil "~{~a~#[~:; + ~]~}"
      (loop for k downfrom (1- (length value)) to 0
            collect (format nil "~a*x^~a" (elt value k) k)))
    "- "))

(define-object-identifier-type (ag-ids::integer-polynomial)
  (:lisp-type (vector integer))
  (:parse (string) (strings-to-integers (split-integer-string string)))
  (:canonical-immediate-data (value) (format nil "~a" value))
  (:description (value)
     (format nil "Integer polynomial: ~a" (polynomial-to-string value)))
  (:attributes (value)
     (general:make-key-value
       :degree (1- (length value)))))
