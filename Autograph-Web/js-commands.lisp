(in-package :autograph-web)

(add-parenscript-definitions (*autograph-web-js*)
  ;; Commands
  (defcommand "Input oriented graph" ((value 'prompt "Paths: " 'allow-expand t))
    (set-pane-current "graph" (create-immediate-identifier :o-graph value))
    (when (get-pane-current "graph") (set-pane-current "term" nil nil)))
  
  (defcommand "Input unoriented graph" ((value 'prompt "Paths: " 'allow-expand t))
    (set-pane-current
     "graph"
     (create-immediate-identifier :u-graph value)
     (lambda ()
       (when (get-pane-current "graph")
	 (set-pane-current "term" nil nil)
	 (set-pane-current "result" nil nil)))))

  (defcommand "Draw the graph" ()
    (set-pane-current
     "result"
     (create-operation-identifier :svg-image :graph-to-dot-svg
				  (list (get-pane-current "graph")))
     (scale-down-svg (ps:chain *known-panes* result))))
  
  (defcommand "Attach graph drawing to the graph" ()
    (set-pane-current
     "graph"
     (create-operation-identifier :svg-graph :graph-with-svg
				  (list (get-pane-current "graph")))
     (scale-down-svg (ps:chain *known-panes* graph))))

  (defcommand "Graph connectedness" ()
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :graph-connected-p (list (get-pane-current "graph")))))

  (defcommand "Graph acyclicity" ()
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :graph-acyclic-p (list (get-pane-current "graph")))))

  (defcommand "Term to graph" ()
    (set-pane-current
     "graph"
     (create-operation-identifier
      :graph :term-to-graph (list (get-pane-current "term")))))
  
  (defcommand "Term to incidence graph" ()
    (set-pane-current
     "graph"
     (create-operation-identifier
      :graph :term-to-igraph (list (get-pane-current "term")))
     (lambda ()
       (set-pane-current "term" nil nil))))
  
  (defcommand "Incidence term to graph" ()
    (set-pane-current
     "graph"
     (create-operation-identifier
      :graph :iterm-to-graph (list (get-pane-current "term")))
     (lambda ()
       (set-pane-current "term" nil nil))))
  
  (defcommand "Graph clique-width" ()
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :graph-clique-width
      (list (get-pane-current "graph")))))
  
  (defcommand "Draw the term" ()
    (set-pane-current
     "result"
     (create-operation-identifier :svg-image :term-to-dot-svg
				  (list (get-pane-current "term")))
     (scale-down-svg (ps:chain *known-panes* result))))
  
  (defcommand "Kcolorability automaton" ((k 'prompt "Number of colors: "))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :kcolorability-automaton
      (list (create-immediate-identifier 'ag-ids::integer k)))
     (lambda ()
       (set-pane-current "result" nil nil))))
  
  (defcommand "Cn term" ((n 'prompt "n: "))
    (set-pane-current
     "term"
     (create-operation-identifier
      :cwd-term :cwd-term-cycle
      (list (create-immediate-identifier ag-ids::integer n)))
     (lambda ()
       (when (get-pane-current "term")
	 (set-pane-current "graph" nil nil)
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "Kn term" ((n 'prompt "n: "))
    (set-pane-current
     "term"
     (create-operation-identifier
      :cwd-term :cwd-term-kn
      (list (create-immediate-identifier ag-ids::integer n)))
     (lambda ()
       (when (get-pane-current "term")
	 (set-pane-current "graph" nil nil)
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "Pn term" ((n 'prompt "n: "))
    (set-pane-current
     "term"
     (create-operation-identifier
      :cwd-term :cwd-term-pn
      (list (create-immediate-identifier ag-ids::integer n)))
     (lambda ()
       (when (get-pane-current "term")
	 (set-pane-current "graph" nil nil)
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "Cn graph" ((n 'prompt "n: "))
    (set-pane-current
     "graph"
     (create-operation-identifier
      :u-graph :graph-cycle
      (list (create-immediate-identifier ag-ids::integer n)))
     (lambda ()
       (when (get-pane-current "term")
	 (set-pane-current "term" nil nil)
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "Kn graph" ((n 'prompt "n: "))
    (set-pane-current
     "graph"
     (create-operation-identifier
      :u-graph :graph-kn
      (list (create-immediate-identifier ag-ids::integer n)))
     (lambda ()
       (when (get-pane-current "term")
	 (set-pane-current "term" nil nil)
	 (set-pane-current "result" nil nil)))))

  ;; (defcommand "Pn graph" ((n 'prompt "n: "))
  ;;   (set-pane-current
  ;;    "graph"
  ;;    (create-operation-identifier
  ;;     :u-graph :graph-pn
  ;;     (list (create-immediate-identifier ag-ids::integer n)))
  ;;    (lambda ()
  ;;      (when (get-pane-current "term")
  ;; 	 (set-pane-current "term" nil nil)
  ;; 	 (set-pane-current "result" nil nil)))))

  (defcommand "Grid nxn graph" ((n 'prompt "n: "))
    (set-pane-current
     "graph"
     (create-operation-identifier
      :u-graph :graph-gnxn
      (list (create-immediate-identifier ag-ids::integer n)))
     (lambda ()
       (when (get-pane-current "term")
	 (set-pane-current "term" nil nil)
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "Coloring automaton" ((k 'prompt "The number of colors: "))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :coloring-automaton
      (list (create-immediate-identifier ag-ids::integer k)))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "Singleton automaton" ()
    (set-pane-current 
     "automaton"
     (create-operation-identifier :cwd-automaton :singleton-automaton
				  (list))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))

  (defcommand "Circuit automaton" ()
    (set-pane-current 
     "automaton"
     (create-operation-identifier :cwd-automaton :circuit-automaton
				  (list))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))


  (defcommand "Has circuit automaton" ()
    (set-pane-current 
     "automaton"
     (create-operation-identifier :cwd-automaton :has-circuit-automaton
				  (list))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))

(defcommand "Connectedness automaton" ()
    (set-pane-current 
     "automaton"
     (create-operation-identifier :cwd-automaton :connectedness-automaton
				  (list))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))

  (defcommand "Edge basic automaton" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier :cwd-automaton :basic-edge-automaton (list))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))

  (defcommand "Subset basic automaton" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier :cwd-automaton :basic-subset-automaton (list))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))

  (defcommand "Path basic automaton" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier :cwd-automaton :basic-path-automaton (list))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "Edge automaton" ((m 'prompt "The number of variables: ")
				(i 'prompt "i: ")
				(j 'prompt "j: "))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :edge-automaton
      (list
       (create-immediate-identifier ag-ids::integer m)
       (create-immediate-identifier ag-ids::integer i)
       (create-immediate-identifier ag-ids::integer j)))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "Forest automaton" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :forest-automaton (list))))
  
  (defcommand "Oriented hamiltonian automaton" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :oriented-hamiltonian-automaton (list))
     (lambda ()
       (when (get-pane-current "automaton")
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "Unoriented hamiltonian automaton" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :unoriented-hamiltonian-automaton (list))))
  
  (defcommand "Union" ((second 'prompt "The name of the second automaton"
			       'completion-options (kind-names "automaton")))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :union
      (list (get-pane-current "automaton") second))))
  
  (defcommand "Intersection"
      ((second 'prompt "The name of the second automaton"
	       'completion-options (kind-names "automaton")))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :intersection (list (get-pane-current "automaton") second))))
  
  (defcommand "Complement" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :complement (list (get-pane-current "automaton")))))
  
  (defcommand "Deterministic?" ()
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :cwd-automaton-deterministic-p (list (get-pane-current "automaton")))))

  (defcommand "xj-to-~xj" ((j 'prompt "j"))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :automaton-xj-to-~xj (list (get-pane-current "automaton")
						(create-immediate-identifier ag-ids::integer j)))))

  (defcommand "y1-y2-to-xj1-xj2" ((m 'prompt "Number of variables") (j1 'prompt " j1") (j2 'prompt " j2"))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :automaton-y1-y2-to-xj1-xj2
      (list (get-pane-current "automaton")
	    (create-immediate-identifier ag-ids::integer m)
	    (create-immediate-identifier ag-ids::integer j1)
	    (create-immediate-identifier ag-ids::integer j2)))))

  (defcommand "Color projection" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :automaton-color-projection (list (get-pane-current "automaton")))))

  (defcommand "Color cylindrification" ((k 'prompt "Number of colors"))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :automaton-color-cylindrification
      (list (get-pane-current "automaton")
	    (create-immediate-identifier ag-ids::integer k)))))

  (defcommand "Non emptiness witness" ((cwd 'prompt "Fix the cwd"))
    (set-pane-current
     "result"
     (create-operation-identifier
      :cwd-term
      :non-emptiness-witness
      (list (get-pane-current "automaton")
	    (create-immediate-identifier ag-ids::integer cwd)))))

  (defcommand "Emptiness" ((cwd 'prompt "Fix the cwd"))
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :automaton-emptiness
      (list (get-pane-current "automaton")
	    (create-immediate-identifier ag-ids::integer cwd)))))
  
  (defcommand "Compilation" ((cwd 'prompt "Fix the cwd"))
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :compilation
      (list (get-pane-current "automaton")
	    (create-immediate-identifier ag-ids::integer cwd)))))
  
  (defcommand "Compute the automaton target state on the term" ()
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :cwd-automaton-target-state
      (list (get-pane-current "term") (get-pane-current "automaton")))))
  
  (defcommand "Force-send state to server" () (resend-session-to-server))
  
  (defcommand "Cancel computation" ()
    (server-funcall :session-cancel-request (json-args)
		    (lambda () (default-state)
		      (show-text *message-holder* "Succesfully cancelled"))))
  
  (defcommand "Reset session" ()
    (server-funcall :session-destroy (json-args)
		    (lambda () (default-state) (load-session-top-panes)
			    (show-text *message-holder* "Succesfully cleared"))))
  
  (defcommand "Reset session only on server" ()
    (server-funcall :session-destroy (json-args)
		    (lambda () (default-state) (resend-session-to-server))))

  (defcommand "Export session" () (session-export))
  
  (defcommand "Import session" ((data 'prompt "Session data: "
				      'allow-expand t 'allow-upload t))
    (session-import data))
  
  (defcommand "List session names" ()
    (show-text (ps:chain (aref *known-panes* "result") content)
	       (loop for kind in (object-keys *named-objects*)
		     append
		     (append
		      (list (+ "Of kind " kind ": "))
		      (object-keys (aref *named-objects* kind))
		      (list "")))))
  
  (defcommand "Use result as graph" ()
              (set-pane-current "graph" (get-pane-current "result")))
  (defcommand "Use result as term" ()
              (set-pane-current "term" (get-pane-current "result")))
  (defcommand "Use result as automaton" ()
              (set-pane-current "automaton" (get-pane-current "result")))

  (defun show-pane-time (kind then-do)
    (set-pane-current
      "time"
      (let ((value (ps:chain (ps:aref *known-panes* kind) content time)))
        (when value
          (create-immediate-identifier
            :multiple-strings value)))
      then-do))

  (defcommand "Show time of last term evaluation" ()
              (show-pane-time "term"))

  (defcommand "Show time of last result evaluation" ()
              (show-pane-time "result"))

  ;; (defcommand "Download term" ()
  ;;             (download-data
  ;;               (ps:aref (get-pane-current "term") :immediate-data)
  ;;               (date-file-name "autograph-web-term" "txt")
  ;;                "text/plain; charset=utf-8"))
  )
