(in-package :autograph-web)

(defparameter *uri* "/autograph")
(pushnew *uri* trag-web:*uris* :test 'equal)

(defvar *ag-session-table* (make-session-table))

(add-parenscript-definitions (*autograph-web-js* t)
  ;; Panes for displaying data
  (define-pane "graph" "Current graph: ")
  (define-pane "term" "Current term: ")
  (define-pane "automaton" "Current automaton: ")
  (define-pane "result" "Latest information: ")
  (define-pane "display" "Latest display: ")
  (define-pane "time" "Evaluation time: ")
  (let ((old-default-state default-state))
    (defun new-default-state ()
      (toggle-visibility
        (ps:chain (ps:aref *known-panes* "time") content)
        "none")
      (funcall old-default-state))
    (setf default-state new-default-state)))
