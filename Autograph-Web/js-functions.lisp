(in-package :autograph-web)

(add-parenscript-definitions (*autograph-web-js*)
  (defun graph-oriented-p (id def)
    (when id
      (let ((type (ps:chain id type)))
	(cond ((= type "u-graph") nil)
	      ((= type "o-graph") t)
	      ((= type "svg-graph")
	       (graph-oriented-p (elt (ps:chain id parameters) 0) def))
	      (t def)))))
  (defun scale-down-svg (pane)
    (lambda ()
      (let* ((container (ps:chain pane content))
             (svg (ps:chain container last-element-child)))
        (setf (ps:chain svg style height)
              (* (ps:chain svg height base-val value-in-specified-units)
                 (min 1
                      (/ (ps:chain container client-width)
                         (ps:chain svg width base-val value-in-specified-units))))
              (ps:chain svg style width)
              (min
                (ps:chain svg width base-val value-in-specified-units)
                (ps:chain container client-width))))))

  (defun compatible-orientation-p (term-pane automaton-pane)
    (let ((orientation (get-pane-current-attribute automaton-pane 'oriented-p)))
      (or (eql orientation :unoriented)
	  (eql (get-pane-current-attribute term-pane 'oriented-p)
	       orientation))))

  (defun same-nb-colors-p (pane1 pane2)
    (=  (get-pane-current-attribute pane1 'nb-colors)
	(get-pane-current-attribute pane2 'nb-colors)))

  (defun <=-nb-colors-p (pane1 pane2)
    (<=  (get-pane-current-attribute pane1 'nb-colors)
	(get-pane-current-attribute pane2 'nb-colors)))

  (defun same-nb-set-variables-p (pane1 pane2)
    (=  (get-pane-current-attribute pane1 'nb-set-variables)
	(get-pane-current-attribute pane2 'nb-set-variables)))
  
  (defun automaton-deterministic-p ()
    (get-pane-current-attribute "automaton" 'deterministic-p))
 
  (defun term-and-automaton-compatible-p ()
    (and
     (compatible-orientation-p "term" "automaton")
     (<=-nb-colors-p "term" "automaton")
     (same-nb-set-variables-p "term" "automaton")))

  (defun vbits-automaton-p ()
    (< 0 (get-pane-current-attribute "automaton" 'nb-set-variables)))
  
  (defun color-automaton-p ()
    (< 0 (get-pane-current-attribute "automaton" 'nb-colors)))
    
  (defun decorated-automaton-p ()
    (or (vbits-automaton-p) (color-automaton-p)))

  (defun term-transducer-ok ()
    (and
     (get-pane-current "term")
     (get-pane-current "automaton")
     (term-plain-p)
     (decorated-automaton-p)
     (compatible-orientation-p "term" "automaton")))

  (defun term-plain-p ()
    (and
     (= 0 (get-pane-current-attribute "term" 'nb-colors))
     (= 0 (get-pane-current-attribute "term" 'nb-set-variables))))

  (defun current-graph-oriented-p ()
    (get-pane-current-attribute "graph" 'oriented-p)))
