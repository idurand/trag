(in-package :autograph-web)

(define-object-identifier-operations (ag-ids::cwd-term)
  (ag-ids::cwd-vertex (n) (clique-width:cwd-term-vertex n))
  (ag-ids::cwd-term-cycle (n) (clique-width:cwd-term-cycle n))
  (ag-ids::cwd-term-kn (n) (clique-width:cwd-term-kn n))
  (ag-ids::cwd-term-pn (n) (clique-width:cwd-term-pn n))
  (ag-ids::cwd-oplus (g1 g2) (clique-width:cwd-term-oplus g1 g2))
  (ag-ids::cwd-rename (a b g) (clique-width:cwd-term-ren a b g))
  (ag-ids::cwd-uadd (a b g) (clique-width:cwd-term-unoriented-add a b g))
  (ag-ids::cwd-oadd (a b g) (clique-width:cwd-term-oriented-add a b g))
  (ag-ids::non-emptiness-witness
   (automaton cwd)
   (autograph:cwd-non-emptiness-witness automaton cwd))
  )

(define-object-identifier-operations (ag-ids::graph)
  (ag-ids::term-to-graph (term) (decomp:cwd-term-to-graph term))
  (ag-ids::iterm-to-graph (term) (decomp:graph-from-cwd-iterm term))
  (ag-ids::term-to-igraph (term) (decomp:igraph-from-cwd-term term)))

(define-object-identifier-operations (ag-ids::svg-image)
  (ag-ids::graph-to-dot-svg (g) (graph:graph-to-svg-string g))
  (ag-ids::term-to-dot-svg (term) (decomp:cwd-term-to-svg-string term)))

(define-object-identifier-operations (ag-ids::svg-graph)
  (ag-ids::graph-with-svg (g) g))

(define-object-identifier-operations (ag-ids::cwd-automaton)
  (ag-ids::kcolorability-automaton (k) (autograph:kcolorability-automaton k))
  (ag-ids::coloring-automaton (k) (autograph:coloring-automaton k))
  (ag-ids::connectedness-automaton () (autograph:connectedness-automaton))
  (ag-ids::singleton-automaton () (autograph:singleton-automaton))
  (ag-ids::circuit-automaton () (autograph:circuit-automaton))
  (ag-ids::has-circuit-automaton () (autograph:has-circuit-automaton))
  (ag-ids::basic-edge-automaton () (autograph:basic-edge-automaton))
  (ag-ids::basic-subset-automaton () (autograph:basic-subset-automaton))
  (ag-ids::basic-path-automaton () (autograph:basic-path-automaton))
  (ag-ids::edge-automaton (m j1 j2) (autograph:edge-automaton m j1 j2))
  (ag-ids::forest-automaton () (autograph:forest-automaton))
  (ag-ids::oriented-hamiltonian-automaton () (autograph:oriented-hamiltonian-automaton))
  (ag-ids::unoriented-hamiltonian-automaton () (autograph:unoriented-hamiltonian-automaton))
  (ag-ids::union (&rest parameters) (termauto:union-automata parameters))
  (ag-ids::intersection (&rest parameters) (termauto:intersection-automata parameters))
  (ag-ids::complement (a) (termauto:complement-automaton a))
  (ag-ids::automaton-y1-y2-to-xj1-xj2 (a m j1 j2) (termauto::y1-y2-to-xj1-xj2 a m j1 j2))
  (ag-ids::automaton-xj-to-~xj (a j) (termauto::xj-to-cxj a (symbols:signature-nb-set-variables (symbols:signature a)) j))
  (ag-ids::automaton-color-projection (a) (termauto::constants-color-projection a))
  (ag-ids::automaton-color-cylindrification (a k) (termauto::nothing-to-colors-constants a k)))

(define-object-identifier-operations (ag-ids::string)
  (ag-ids::cwd-automaton-target-state
   (term automaton)
   (format nil "Target state: ~a" (termauto:compute-final-target term automaton)))

  (ag-ids::cwd-automaton-deterministic-p
   (automaton)
   (if (termauto:deterministic-p automaton) "Deterministic" "Not deterministic"))

  (ag-ids::automaton-emptiness
   (automaton cwd)
   (if (autograph:cwd-automaton-emptiness automaton cwd) "Empty" "Not empty"))

  (ag-ids::compilation
   (automaton cwd)
   (autograph:compile-cwd-automaton automaton cwd))

  (ag-ids::graph-clique-width (graph) (format nil "~A" (decomp::graph-cwd graph)))
  (ag-ids::graph-connected-p
   (graph)
   (if (termauto:recognized-p (decomp:cwd-decomposition graph)
			      (autograph::connectedness-automaton)) "Connected" "Not connected"))
  (ag-ids::graph-acyclic-p
   (graph)
   (if (termauto:recognized-p
	(decomp:cwd-decomposition graph)
	(autograph::forest-automaton)) "Acyclic" "not acyclic")))
