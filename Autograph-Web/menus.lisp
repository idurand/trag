;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin, Irène Durand

(in-package :autograph-web)

;; default capital letters at each word of the command
;; or you have to rename with :js-name in the defcommand

(add-parenscript-definitions (*autograph-web-js*)
  ;; Menus
  (defmenu "Standard graphs menu"
    (menu-command-entry "Kn" "Kn graph" "Clique of n nodes")
    (menu-command-entry "Cn" "Cn graph" "Cycle of n nodes")
    (menu-command-entry "Pn" "Pn graph" "Path of n nodes")
    (menu-command-entry "Grid nxm" "Grid nxm graph" "Rectangular grid of sides n, m")
    (menu-command-entry "Grid nxn" "Grid nxn graph" "Square grid of side n"))

  (defmenu "Standard terms menu"
    (menu-command-entry "Kn" "Kn term" "Cwd-term for a clique graph")
    (menu-command-entry "Cn" "Cn term" "Cwd-term for a cycle graph")
    (menu-command-entry "Pn" "Pn term" "Cwd-term for a path graph"))

  (defmenu "Computed terms menu"
    (menu-command-entry "Petersen term" "Petersen term" "Cwd-term for Petersen's graph")
    (menu-command-entry "McGee term" "McGee term" "Cwd-term for McGee's graph")
    (menu-command-entry "Gnxm term" "Gnxm term" "Cwd-term for a grid nxm"))

  (defmenu "Computed graphs menu"
    (menu-command-entry "Grunbaum" "Grunbaum Graph" "Grunbaum's graph")
    (menu-command-entry "Petersen" "Petersen Graph" "Petersen's graph")
    (menu-command-entry "McGee" "Mcgee Graph" "McGee's graph"))

  (defmenu "Graphs menu"
    (menu-command-entry "Display graph" "Display Graph"
			nil
			(lambda () (not (get-pane-current "graph"))))
    (menu-command-entry "Random orientation" "Random orientation"
			nil
			(lambda () (or (not (get-pane-current "graph")))))
    (menu-command-entry "Complete orientation" "Complete orientation"
			nil
			(lambda () (or (not (get-pane-current "graph")))))
    (menu-command-entry "Erase orientation" "Erase orientation"
			nil
			(lambda () (or (not (get-pane-current "graph"))
				       (not (get-pane-current-attribute "graph" 'oriented-p)))))
    (menu-command-entry "Add path" "Add path"
			nil
			(lambda () (not (get-pane-current "graph"))))
    (menu-command-entry "Remove path" "Remove path"
			nil
			(lambda () (not (get-pane-current "graph"))))
    (menu-command-entry "Renumber vertices" "Renumber Graph Vertices"
                        "Make vertice number go from 1 up contiguously, preserving order"
                        (lambda () (not (get-pane-current "graph"))))
    (menu-submenu-entry "Standard graphs" "Standard graphs menu")
    (menu-submenu-entry "Computed graphs" "Computed graphs menu")

    (menu-command-entry "Input unoriented" "Input unoriented graph")
    (menu-command-entry "Input oriented" "Input oriented graph")
    (menu-command-entry "Load DIMACS" "Load DIMACS graph")
    (menu-command-entry "Download DIMACS" "Download Dimacs Graph")
    )

  (defmenu "Graph decomposition menu"
    (menu-command-entry "Heuristic cwd-decomposition"
			"Heuristic Cwd Decomposition"
			nil
			(lambda () (not (get-pane-current "graph"))))
    (menu-command-entry "Clique-width decomposition" "Cwd Decomposition"
			"uses the Glucose SAT solver - for small unoriented graphs only")
    (menu-command-entry "Try specific clique-width" "Try Cwd")
    (menu-command-entry "Heuristic cwd-twd-decomposition"
			"Heuristic Cwd Twd Decomposition"
			"cwd-decomposition using an intermediate twd-decomposition"
			(lambda () (not (get-pane-current "graph"))))
    (menu-command-entry "Heuristic twd-decomposition"
			"Heuristic Twd Decomposition"
			nil
			(lambda () (not (get-pane-current "graph"))))
    (menu-command-entry "Heuristic incidence cwd-decomposition"
			"Heuristic Incidence Cwd Decomposition"
			nil
			(lambda () (not (get-pane-current "graph"))))
    (menu-command-entry "Optimal incidence cwd-decomposition"
			"Optimal Incidence Cwd Decomposition"
			nil
			(lambda () (not (get-pane-current "graph")))))

  (defmenu "Graph properties menu"
    (menu-command-entry "Graph connectedness" "Graph connectedness" nil
			(lambda () (not (get-pane-current "graph"))))
    (menu-command-entry "Graph acyclicity" "Graph acyclicity" nil
			(lambda () (not (get-pane-current "graph"))))
    (menu-command-entry "Graph hamiltonian" "Graph Hamiltonian"))

  (defmenu "Graph direct computations menu"
    (menu-command-entry "Chromatic polynomial" "Graph Chromatic Polynomial")
    (menu-command-entry "Chromatic value" "Graph Chromatic Value")
    )
  
  (defmenu "Term annotation menu"
    (menu-command-entry "Compute Add Annotation" "Compute Add Annotation")
    (menu-command-entry "Remove Add Annotation" "Remove Add Annotation")
    )

  (defmenu "Terms menu"
    (menu-command-entry "Term to graph" "Term to graph")
    (menu-command-entry "Term to incidence graph" "Term to incidence graph")
    (menu-command-entry "Incidence term to graph" "Incidence term to graph")
    (menu-command-entry "Clique-width" "Term Clique Width")
    (menu-command-entry "Depth" "Term Depth")
    (menu-command-entry "Size" "Term Size")
    (menu-command-entry "Draw" "Draw the term")
    (menu-submenu-entry "Annotation" "Term annotation menu")
    (menu-submenu-entry "Standard terms" "Standard terms menu")
    (menu-submenu-entry "Computed terms" "Computed terms menu")
    (menu-command-entry "Input term" "Input term")
    (menu-command-entry "Input multi term" "Input Multi Term")
    (menu-command-entry "Add vbit" "Add Vbits To Term")
    (menu-command-entry "Show multi term" "Show Multi Term")
    (menu-command-entry "Download" "Download Current Term")
    (menu-command-entry "Download multi" "Download Multi Term")
    (menu-command-entry "Erase term" "Erase term")
    )

  (defmenu "Current term menu"
    )

  (defmenu "Current automaton menu"
    )

  (defmenu "Automata menu"
    (menu-submenu-entry "Application" "Application menu"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-command-entry "Determistic?" "Deterministic?"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-command-entry "Emptiness" "Emptiness"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-command-entry "Non emptiness witness" "Non emptiness witness"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-command-entry "Compilation" "Compilation"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-submenu-entry "Standard automata" "Standard automata menu")
    (menu-submenu-entry "Hamilton automata" "Hamilton automata menu")
    (menu-submenu-entry "Vbits automata" "Vbits automata menu")
    (menu-submenu-entry "Color automata" "Color automata menu")
    (menu-submenu-entry "Formula automata" "Formula automata menu")
    (menu-command-entry "Projection" "Projection automaton")
    (menu-submenu-entry "Operations" "Automaton operations"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-submenu-entry "Morphisms" "Automaton morphisms"
			nil (lambda () (not (get-pane-current "automaton")))))
  
  (defmenu "Application menu"
    (menu-command-entry "Recognize term" "Recognize term")
    (menu-command-entry "Enum Recognize term" "Enum recognize term")
    (menu-command-entry "Compute target" "Compute Target")
    (menu-command-entry "Compute #" "Compute #")
    (menu-command-entry "Compute Sp" "Compute Sp")
    (menu-command-entry "Compute MSp" "Compute MSp")
    (menu-command-entry "Compute Sat" "Compute Sat")
)

  (defmenu "Standard automata menu"
    (menu-command-entry "Singleton" "Singleton automaton")
    (menu-command-entry "Stable" "Stable Automaton")
    (menu-command-entry "Connectedness" "Connectedness automaton")
    (menu-command-entry "Circuit" "Circuit automaton")
    (menu-command-entry "Has circuit" "Has circuit automaton")
    )
  
  (defmenu "Hamilton automata menu"
    (menu-command-entry "Oriented hamiltonian" "Oriented hamiltonian automaton")
    (menu-command-entry "Unoriented hamiltonian" "Unoriented hamiltonian automaton"))
  
  (defmenu "Color automata menu"
    (menu-command-entry "Coloring" "Coloring automaton")
    (menu-command-entry "Kcolorability" "Kcolorability automaton")
    (menu-command-entry "Kcolorability (with symmetries and annotations)"
                        "Colorability automaton with symmetry optimisation (requires add annotation)")
    (menu-command-entry "Count colorings" "Count colorings")
    (menu-command-entry "Show one coloring" "Show One Coloring")
    (menu-command-entry "Show colorings" "Show Colorings")
    (menu-command-entry "Kacyclic colorability" "Kacyclic Colorability Automaton"))
  
  (defmenu "Formula automata menu"
    (menu-command-entry "Formula Automaton" "Formula Automaton")
    (menu-command-entry "Formula Oriented Automaton" "Formula Oriented Automaton")
    (menu-command-entry "Multi Formula Automaton" "Multi Formula Automaton")
    (menu-command-entry "Multi Formula Oriented Automaton" "Multi Formula Oriented Automaton")
    )

  (defmenu "Vbits automata menu"
    (menu-command-entry "Edge(X1,X2)" "Edge basic automaton")
    (menu-command-entry "Edge(., Xi,.,Xj,.)-m" "Edge automaton")
    (menu-command-entry "Subset(X1,X2)" "Subset basic automaton")
    (menu-command-entry "Path(X1,X2)" "Path basic automaton")
    )

  (defmenu "Vbits morphisms menu"
    (menu-command-entry "0:() to 1:(X1)" "Nothing To X1")
    (menu-command-entry "0:() to m:(.,Xj,.)" "Nothing To Xj")
    (menu-command-entry "2:(Y1,Y2) to m:(.,Xj1,.,Xj2,.)" "y1-y2-to-xj1-xj2")
    (menu-command-entry "m:(.,Xj,.) to m:(.,~Xj,.)" "xj-to-~xj"))

  (defmenu "Color morphisms menu"
    (menu-command-entry "Color projection" "Color projection")
    (menu-command-entry "Color cylindrification"))
  
  (defmenu "Automaton operations"
    (menu-command-entry "Union" "Union")
    (menu-command-entry "Complement" "Complement")
    (menu-command-entry "Intersection" "Intersection"))

  (defmenu "Automaton morphisms"
    (menu-submenu-entry "Color morphisms" "Color morphisms menu")
    (menu-submenu-entry "Vbits morphisms" "Vbits morphisms menu"))
  
  (defmenu "Named objects menu"
    (menu-submenu-entry "Term" "Named term")
    (menu-submenu-entry "Graph" "Named graph")
    (menu-submenu-entry "Automaton" "Named automaton"))
  
  (defmenu "Session menu"
    (menu-command-entry "Cancel computation" "Cancel computation")
    (menu-command-entry "Reset" "Reset session")
    (menu-command-entry "Force to server" "Force-send state to server")
    (menu-command-entry "Export" "Export session")
    (menu-command-entry "Import" "Import session")))
