(defpackage :autograph-web
  (:use :common-lisp :trag-web)
  (:export))

(defpackage :autograph-web-ids
  (:use)
  (:nicknames :ag-ids)
  (:import-from :keyword
                :integer :string :multiple-strings)
  (:import-from :trag-web-demo-ids)
  (:import-from :trag-web-ids
                :web-request-log))

(defpackage :autograph-web-ajax
  (:use :trag-web-ajax-handlers)
  (:nicknames :ag-js))
