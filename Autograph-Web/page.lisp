(in-package :autograph-web)

(add-parenscript-definitions (*autograph-web-js*)
  (setf *top-menus*
	(list
	 (menu-submenu-entry "Session" "Session menu")
	 (menu-submenu-entry "Graphs" "Graphs menu")
	 (menu-submenu-entry "Graph decomposition" "Graph decomposition menu")
	 (menu-submenu-entry "Terms" "Terms menu")
	 (menu-submenu-entry "Automata" "Automata menu")
;;	 (menu-submenu-entry "Term properties" "Term properties menu")
	 (menu-submenu-entry "Graph direct computations" "Graph direct computations menu")
	 (menu-submenu-entry "Graph properties" "Graph properties menu")
	 (menu-submenu-entry "Named objects" "Named objects menu")
	 ))

  ;; Initialization to perform on load
  (defun initialize ())
  
  (defun request-log-entry-selected (x)
    (console-log x)
    (set-pane-current "result" x)))

(define-pane-page
    :uri *uri* :parenscript-code *autograph-web-js* :ajax-package :ag-js
  :object-identifier-package :ag-ids :remote-session-table *ag-session-table*
  :command-line-help "Enter a command; use Ctrl-Space for completion"
  :web-log-pane "result"
  :page-bottom-message 
  "
  <br/><br/><div style=\"font-size: 0.9em; padding: 0.7em;\">
  This is a <a href=\"https://bitbucket.org/idurand/TRAG/\">TRAG</a> demo.
  See the <a href=\"http://dept-info.labri.fr/~idurand/trag/Documentation/TRAGnotice.pdf\">User\'s Manual</a> (in construction).<br/>
  This site uses session cookies to maintain correspondence between the client
  and the server side of computations.
  We reserve the right, but do not promise, to collect, store and process
  the constructions entered into the web application, including the operations
  performed on their combinations, and publish the results of such analysis.
  Please do not submit any personally identifiable data or data that should
  not become public.
  </span>
  "
  :page-bottom-version t)
