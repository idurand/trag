(in-package :autograph-web)

(eval-when
  (:execute :compile-toplevel :load-toplevel)
  (defparameter *term-peg*
    "
    whiteSpace <- [ \\n\\r\\t]
    nameSymbol <- '-' / [a-zA-Z0-9]
    stringEntry <- (![\"\\\\] .) / '\\\\' . 
    string <- '\"' stringEntry* '\"'
    termName <- string / nameSymbol+
    index <- '_' termName
    color <- '~' termName
    term <- whiteSpace* termName index* color? parameters? whiteSpace*
    parameters <- (whiteSpace* '(' whiteSpace*) term ((whiteSpace* ',' whiteSpace*) term)* (whiteSpace* ')' whiteSpace*)
    "))

(defpackage :autograph-web-term-peg (:use))

(in-package :autograph-web-term-peg)

(esrap-peg:peg-compile
  (esrap-peg:basic-parse-peg
    autograph-web::*term-peg*))

(esrap-peg:def-peg-matchers
  (
   (nameSymbol (_ x))
   (termName
     ((string _) (! x))
     (_ (s+ (m! #'! x))))
   (index (_ (! (n2 x))))
   (color (_ (! (n2 x))))
   (stringEntry ((_ ?s) ?s))
   (string ((_ ?l _) (s+ (m! #'! ?l))))
   (term
     ((_ ?name () () () _) (! ?name))
     ((_ ?name ?indices ?color ?parameters _)
      `(,(! ?name)
         ,(m! #'! ?indices)
         ,(cl:when ?color (! ?color))
         ,@(cl:when ?parameters (! ?parameters)))))
   (parameters ((_ ?x ?y _) `(,(! ?x) ,@(m! #'! (m! #'n2 ?y)))))
   )
  :abbreviations :default :arg x)

(cl:in-package :autograph-web)

(defun parse-term (str)
  (esrap-peg:ast-eval (esrap:parse 'autograph-web-term-peg::term str)))

(defun unparse-term-name (s)
  (if (cl-ppcre:scan "^[-a-zA-Z0-9]+$" s)
    s (format nil "~s" s)))

(defun unparse-term (term)
  (if
    (listp term)
    (format nil "~a~{_~a~}~@[~~~a~]~@[(~{~a~#[~:;,~]~})~]"
            (unparse-term-name (first term)) (second term)
            (third term)
            (mapcar 'unparse-term (cdddr term)))
    (unparse-term-name term)))
