(in-package :autograph-web)

(defun first-separator (separator-list string)
  (loop for s in separator-list
        when (cl-ppcre:scan s string) return s))

(defparameter *number-list-separators*
  '("[()]" "[[\\]]" "[{}]" "!" "#" "\\\\" "/" ";" "," "_" "-" "@" )) ;; " "

(defun find-path-separator (string)
  (first-separator *number-list-separators* string))

(defun split-component-string (string)
  (cl-ppcre:split "[^0-9]+" string))

(defun split-integer-string (string)
  (cl-ppcre:split "[^-0-9]+" string))

(defun strings-to-integers (strings)
  (loop for string in strings
	for integer = (parse-integer string :junk-allowed t)
	when integer
	  collect integer))

(defun split-list-of-lists-of-numbers (string)
  (loop
    with separator = (find-path-separator string)
    with components = (if separator (cl-ppcre:split separator string) (list string))
    for component in components
    for ns = (strings-to-integers (split-component-string component))
    when ns collect ns))

