;; NAUTOWRITE> (setf *a* (setup-circuit-automaton 2))
;; 2-CIRCUIT 7 states  58 rules*
Automaton 2-CIRCUIT
States !0{a} !0{b} !0{a-b} !0{ab} !0{ba} !qok !0{ab-ba}
Final States !qok
Transitions
(a) -> !0{a}
(b) -> !0{b}
(add_a_b !0{a}) -> !0{a}
(add_a_b !0{b}) -> !0{b}
(add_a_b !0{a-b}) -> !0{ab}
(add_a_b !0{ab}) -> !0{ab}
(add_a_b !0{ba}) -> !qok
(add_a_b !qok) -> !qok
(add_a_b !0{ab-ba}) -> !qok
(add_b_a !0{a}) -> !0{a}
(add_b_a !0{b}) -> !0{b}
(add_b_a !0{a-b}) -> !0{ba}
(add_b_a !0{ab}) -> !qok
(add_b_a !0{ba}) -> !0{ba}
(add_b_a !qok) -> !qok
(add_b_a !0{ab-ba}) -> !qok
(ren_a_b !0{a}) -> !0{b}
(ren_a_b !0{b}) -> !0{b}
(ren_a_b !0{a-b}) -> !0{b}
(ren_a_b !0{ab}) -> !0{b}
(ren_a_b !0{ba}) -> !0{b}
(ren_a_b !qok) -> !qok
(ren_a_b !0{ab-ba}) -> !0{b}
(ren_b_a !0{a}) -> !0{a}
(ren_b_a !0{b}) -> !0{a}
(ren_b_a !0{a-b}) -> !0{a}
(ren_b_a !0{ab}) -> !0{a}
(ren_b_a !0{ba}) -> !0{a}
(ren_b_a !qok) -> !qok
(ren_b_a !0{ab-ba}) -> !0{a}
(oplus* !0{a} !0{a}) -> !0{a}
(oplus* !0{a} !0{b}) -> !0{a-b}
(oplus* !0{a} !0{a-b}) -> !0{a-b}
(oplus* !0{a} !0{ab}) -> !0{ab}
(oplus* !0{a} !0{ba}) -> !0{ba}
(oplus* !0{a} !qok) -> !qok
(oplus* !0{a} !0{ab-ba}) -> !0{ab-ba}
(oplus* !0{b} !0{b}) -> !0{b}
(oplus* !0{b} !0{a-b}) -> !0{a-b}
(oplus* !0{b} !0{ab}) -> !0{ab}
(oplus* !0{b} !0{ba}) -> !0{ba}
(oplus* !0{b} !qok) -> !qok
(oplus* !0{b} !0{ab-ba}) -> !0{ab-ba}
(oplus* !0{a-b} !0{a-b}) -> !0{a-b}
(oplus* !0{a-b} !0{ab}) -> !0{ab}
(oplus* !0{a-b} !0{ba}) -> !0{ba}
(oplus* !0{a-b} !qok) -> !qok
(oplus* !0{a-b} !0{ab-ba}) -> !0{ab-ba}
(oplus* !0{ab} !0{ab}) -> !0{ab}
(oplus* !0{ab} !0{ba}) -> !0{ab-ba}
(oplus* !0{ab} !qok) -> !qok
(oplus* !0{ab} !0{ab-ba}) -> !0{ab-ba}
(oplus* !0{ba} !0{ba}) -> !0{ba}
(oplus* !0{ba} !qok) -> !qok
(oplus* !0{ba} !0{ab-ba}) -> !0{ab-ba}
(oplus* !qok !qok) -> !qok
(oplus* !qok !0{ab-ba}) -> !qok
(oplus* !0{ab-ba} !0{ab-ba}) -> !0{ab-ba}
