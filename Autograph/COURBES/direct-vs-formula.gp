set term post eps
set output "direct-vs-formula.eps"
#set format xy "$%g$"
set title "direct vs formula"
set xlabel "N"
set ylabel "Time"
#set key at 15,-10
plot 'direct.dat' using 1:2 with lines linecolor rgb "red", \
     'formula.dat' using 1:2 with lines linecolor rgb "blue" 

