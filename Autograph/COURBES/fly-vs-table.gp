set term post eps
set output "fly-vs-table.eps"
set title "fly vs table"
set xlabel "N"
set ylabel "Time"
plot 'fly-vs-table.dat' using 1:2 with lines linecolor rgb "blue", \
     'fly-vs-table.dat' using 1:3 with lines linecolor rgb "red" 
