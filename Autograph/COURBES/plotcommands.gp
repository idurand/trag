set term post eps
set output "courbe.eps"
#set format xy "$%g$"
set title "formula vs direct"
set xlabel "N"
set ylabel "Time"
#set key at 15,-10
plot "2011-11-14.dat" using 1:2 with lines, \
     "2011-11-14.dat" using 1:3 with lines
