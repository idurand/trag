(in-package :autograph)

(defun cwd-term-n-triangles (n)
  (term-num-leaves
   (cwd-term-multi-oplus-list
    (make-list n :initial-element (car (arg (cwd-term-kn 3)))))))

(defun three-triangles () (cwd-term-n-triangles 3))
;; (defun three-triangles ()
;;   (term-num-leaves
;;    (cwd-term-multi-oplus-list
;;     (make-list 3 :initial-element (car (arg (cwd-term-kn 3)))))))

(defun p6 ()
  (car (arg (car (arg (cwd-term-pn 6))))))

(defun pn (n)
  (term-num-leaves (car (arg (car (arg (cwd-term-pn n)))))))

(defun cn (n)
  (term-num-leaves
   (car (arg (car (arg (cwd-term-cycle n)))))))

(defun grid9 ()
  (input-cwd-term
   "add_c_d(add_b_c(oplus(ren_c_a(add_a_d(oplus(ren_d_b(add_c_d(oplus(add_b_c(add_a_b(oplus(oplus(a[1],b[2]),c[3]))),add_a_b(add_b_c(oplus(oplus(oplus(d[4],c[5]),b[6]),a[7])))))),d[8]))),c[9])))"))

(defun grid9-bis ()
  (input-cwd-term 
   "add_c_d(add_b_c(oplus(ren_c_a(add_a_d(oplus(ren_d_b(add_c_d(oplus(add_b_c(add_a_b(oplus(oplus(a^0[1],b^0[2]),c^0[3]))),add_a_b(add_b_c(oplus(oplus(oplus(d^1[4],c^0[5]),b^0[6]),a^0[7])))))),d^1[8]))),c^1[9])))"))

(defun grid3x4 ()
  (term-num-leaves
  (car (arg
	(cwd-term-to-non-redundant-term
	 (clique-width::cwd-term-rgrid 3 4))))))

(defun g33-big-square-diagos ()
  "grid with 1 diago of big square"
  (term-num-leaves
   (car(arg (clique-width::cwd-term-rgrid-with-diag 3 3)))))

(defun g33-edges-to-center ()
  "grid with diago toward center 9"
  (cwd-decomposition (graph-from-paths '((1 2 3 4 5 6 7 8 1 9) (2 9) (3 9) (4 9) (5 9) (6 9) (7 9) (8 9)))))

(defun g33-xx ()
  "grid with nw-se diagos"
  (cwd-decomposition (graph-from-paths '((1 2 3 4 5 6 7 8 1 9) (1 9) (2 4) (8 6) (9 5)))))

(defun g25-plus-x-to-long-side ()
  "grid 2x5 with additional vertex connected to long side"
  (cwd-decomposition
   (graph-from-paths '((1 2 3 4 5 6 7 8 9 10 1)
		       (2 9) (3 8) (4 7) (5 6)
		       (6 11) (7 11) (8 11) (9 11) (10 11)))))
  
(defun term7 ()
  (cwd-decomposition (graph-from-paths '((1 2 3 4 5 6 1 7) (2 7) (3 7) (4 7) (5 7) (6 7)))))

(defun term8 ()
  (cwd-decomposition (graph-from-paths '((1 2 3 4 5 6 7 1 8) (2 8) (3 8) (4 8) (5 8) (6 8) (7 8)))))

(defun g33-diag-nw-se ()
  (cwd-decomposition
   (graph-from-paths '((1 2 3 6 9 8 7 4 1) (1 5 9) (6 2 5 8 4 5 6)))))

(defun pn-spec ()
  (setq *spec* 
	(read-termauto-spec-from-path "PN.txt")))

(defun r-automaton ()
  (pn-spec)
  (let ((r1 (get-automaton "R1"))
	(r2 (get-automaton "R2")))
    (disjoint-union-automaton r1 r2)))

;;(concatenate 'string (initial-data-directory) "PN.txt"))))
  
(defun aut-to-strings (aut n)
  (let ((strings (collect-enum (make-funcall-enumerator #'terms::unary-term-to-string (termauto::recognized-terms-depth=-enumerator aut n)))))
    (values strings (length strings))))

(defun pn-minimal-configurations (n)
  (let ((a (r-automaton)))
    (aut-to-strings (complement-automaton a) n)))
    
(defun check-product-property (t1 t2)
  (setq t1 (term-num-leaves t1 :force t))
  (setq t2 (term-num-leaves t2 :force t :start (term-nb-leaves t1)))
  (multiple-value-bind (c1 n1) (minimal-configurations t1)
    (multiple-value-bind (c2 n2) (minimal-configurations t2)
      (let ((term (cwd-term-oplus t1 t2)))
	(multiple-value-bind (c n) (minimal-configurations term)
	  (assert (= (* n1 n2) n))
	  (values n1 n2 n c1 c2 c))))))


(defun test-term-domination (name term n)
  (format t "~A, cwd=~A, size=~A~%" name (clique-width term) (term-size term))
  (format t "~14A ~14A ~14A~%" (format nil "avgTimeX~A" n) "max-size" "max-nb-states")
  (loop
    with c = 0
    with max-size = 0
    with max-nb-states = 0
    with time = 0
    and f = (basic-x-domination-automaton)
    for xterm = (terms::term-add-random-vbit term) then (terms::term-add-random-vbit term)
    repeat n
    do (multiple-value-bind (res duration)
	   (get-time (compute-final-target xterm f :save-run t))
	 (incf time duration)
	 (let ((states (termauto::states-of-run xterm)))
	   (setq max-size (max max-size (reduce #'max (container-mapcar #'state-size states))))
	   (setq max-nb-states (max max-nb-states (container-size states))))
	 (when res (incf c)))
    finally (format t "~14A ~14A ~14A~%" (coerce time 'float) max-size max-nb-states)))
    
(defun test-mcgee-domination (n)
  (test-term-domination "McGee" (mcgee) n))

(defun test-hmcgee-domination (n)
  (test-term-domination "Heuristic McGee"
			(cwd-decomposition (graph-from-cwd-term (mcgee)) nil)
			n))

(defun test-grid-domination (m n)
  (test-term-domination (format nil "g6x~A" m) (cwd-term-grid 6 m) n))

(defun test-square-grid-domination (m n)
  (test-term-domination (format nil "g~Ax~A" m m) (cwd-term-square-grid m) n))

(defun test-square-grids-domination (m n)
  (loop
    for i from 1 to m
    do (test-square-grid-domination i n)))

(defun test-triangle-domination (m n)
  (test-term-domination (format nil "~A triangles" m) (cwd-term-n-triangles m) n))

(defun test-triangles-domination (m n)
  (loop
    for i from 1 to m
    do (test-triangle-domination i n)))
