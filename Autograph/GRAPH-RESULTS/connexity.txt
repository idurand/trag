;; 2014-06-27 

AUTOGRAPH> (with-time (table-connectedness-automaton 2))
 in 0.006sec
2-CONNECTEDNESS  87 rules* sink: T 10 states
AUTOGRAPH> (with-time (simplify-automaton (table-connectedness-automaton 2)))
 in 0.01sec
2-CONNECTEDNESS  41 rules* sink: T 6 states

AUTOGRAPH> (with-time (table-connectedness-automaton 3))
 in 1.956sec
3-CONNECTEDNESS  10254 rules* sink: T 134 states

AUTOGRAPH> (with-time (simplify-automaton (table-connectedness-automaton 3)))
 in 3.233sec
3-CONNECTEDNESS  2103 rules* sink: T 56 states
