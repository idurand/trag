;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric arc-dports0 (arc-dports)
  (:documentation "ports of unconnected edges"))

(defgeneric arc-dports1 (arc-dports)
  (:documentation "port of edge connected to a node in X1"))

(defgeneric arc-dports2 (arc-dports)
  (:documentation "port edge connected to a node in X2"))

(defclass arc-dports ()
  ((arc-dports0 :type ports :initarg :arc-dports0 :reader arc-dports0)
   (arc-dports1 :type ports :initarg :arc-dports1 :reader arc-dports1)
   (arc-dports2 :type ports :initarg :arc-dports2 :reader arc-dports2)))

(defmethod ports-of ((arc-dports arc-dports))
  (ports-union
   (arc-dports0 arc-dports)
   (ports-union (arc-dports1 arc-dports) (arc-dports2 arc-dports))))

(defgeneric make-arc-dports (arc-dports0 arc-dports1 arc-dports2)
  (:method ((arc-dports0 ports) (arc-dports1 ports) (arc-dports2 ports))
    (make-instance
     'arc-dports
     :arc-dports0 arc-dports0
     :arc-dports1 arc-dports1
     :arc-dports2 arc-dports2)))

(defun make-empty-arc-dports ()
  (make-arc-dports
   (make-empty-ports)
   (make-empty-ports)
   (make-empty-ports)))

(defmethod print-object ((s arc-dports) stream)
  (format stream "D~A-~A-~A"
	  (arc-dports0 s)
	  (arc-dports1 s)
	  (arc-dports2 s)))

(defgeneric arc-dports-union (arc-dports1 arc-dports2)
  (:method ((dports1 arc-dports) (dports2 arc-dports))
    (make-arc-dports
     (ports-union (arc-dports0 dports1) (arc-dports0 dports2))
     (ports-union (arc-dports1 dports1) (arc-dports1 dports2))
     (ports-union (arc-dports2 dports1) (arc-dports2 dports2)))))

(defgeneric arc-dports-subst (a b arc-dports)
  (:method (a b (arc-dports arc-dports))
    (make-arc-dports
     (ports-subst a b (arc-dports0 arc-dports))
     (ports-subst a b (arc-dports1 arc-dports))
     (ports-subst a b (arc-dports2 arc-dports)))))

(defgeneric arc-cport1 (mso-arc-state)
  (:documentation "cport in X1 in a ports container so that it can be missing"))

(defgeneric arc-cport2 (mso-arc-state)
  (:documentation "cport X2 in a ports container so that it can be missing"))

(defclass mso-arc-state (graph-state)
  ((arc-cport1 :type ports :initarg :arc-cport1 :reader arc-cport1)
   (arc-cport2 :type ports :initarg :arc-cport2 :reader arc-cport2)
   (arc-dports :initarg :arc-dports :reader arc-dports)))

(defgeneric make-mso-arc-state (arc-cport1 arc-cport2 arc-dports)
  (:method ((arc-cport1 ports) (arc-cport2 ports) (arc-dports arc-dports))
    (make-instance
     'mso-arc-state :arc-cport1 arc-cport1 :arc-cport2 arc-cport2
		     :arc-dports arc-dports)))

(defmethod print-object ((state mso-arc-state) stream)
  (format stream "~A-~A~A" (arc-cport1 state) (arc-cport2 state) (arc-dports state)))

(defgeneric arc-cports (state)
  (:method ((s mso-arc-state))
    (ports-union (arc-cport1 s) (arc-cport2 s))))

(defmethod graph-oplus-target ((state1 (eql *ok-state*)) (state2 mso-arc-state))
  (graph-oplus-target state2 state1))

(defmethod graph-oplus-target ((state1 mso-arc-state) (state2 (eql *ok-state*)))
  (when (container-empty-p (arc-cports state1))
    *ok-state*))

(defmethod graph-oplus-target ((state1 mso-arc-state) (state2 mso-arc-state))
  (let* ((arc-cport11 (arc-cport1 state1))
	 (arc-cport12 (arc-cport2 state1))
	 (arc-cport21 (arc-cport1 state2))
	 (arc-cport22 (arc-cport2 state2)))
    (unless (or
	     ;; X1 not singleton
	     (> (+ (container-size arc-cport11) (container-size arc-cport21)) 1)
	     ;; X2 not singleton
	     (> (+ (container-size arc-cport12) (container-size arc-cport22)) 1))
      (make-mso-arc-state
       (container-union arc-cport11 arc-cport21)
       (container-union arc-cport12 arc-cport22)
       (arc-dports-union (arc-dports state1) (arc-dports state2))))))

(defgeneric arc-mso-add-ok (a b arc-cport1 arc-cport2
			     arc-dports0 arc-dports1 arc-dports2)
  (:method ((a integer) (b integer) (arc-cport1 ports) (arc-cport2 ports)
	    (arc-dports0 ports) (arc-dports1 ports) (arc-dports2 ports))
    (or (and (ports-member a arc-cport1) (ports-member b arc-dports2))
	(and (ports-member b arc-cport1) (ports-member a arc-dports2)))))

(defmethod graph-add-target (a b (state mso-arc-state))
  (let* ((arc-cport1 (arc-cport1 state))
	 (arc-cport2 (arc-cport2 state))
	 (arc-cports (arc-cports state))
	 (arc-dports (arc-dports state))
	 (arc-dports0 (arc-dports0 arc-dports))
	 (arc-dports1 (arc-dports1 arc-dports))
	 (arc-dports2 (arc-dports2 arc-dports))
	 (all-dports (ports-of arc-dports))
	 (all-ports (ports-union-gen
		 (list arc-cports arc-dports0 arc-dports1 arc-dports2))))
    (unless (and (ports-member a all-ports) (ports-member b all-ports))
      (return-from graph-add-target state))
    (when (or (and (ports-member a arc-cports) (ports-member b arc-cports))
	      (and (ports-member a all-dports) (ports-member b all-dports)))
      (return-from graph-add-target))
    (when (arc-mso-add-ok
	   a b arc-cport1 arc-cport2 arc-dports0 arc-dports1 arc-dports2)
      (return-from graph-add-target *ok-state*))
    (cond ((ports-member a arc-cport1)
	   (make-mso-arc-state
	    arc-cport1
	    arc-cport2
	    (make-arc-dports (ports-remove b arc-dports0)
			      (ports-adjoin b arc-dports1)
			      arc-dports2)))
	  ((ports-member a arc-cport2)
	   (make-mso-arc-state
	    arc-cport1
	    arc-cport2
	    (make-arc-dports (ports-remove b arc-dports0)
			      arc-dports1
			      (ports-adjoin b arc-dports2))))
	  ;; necessarily a in dports0
	  ((ports-member b arc-cport1)
	   (make-mso-arc-state
	    arc-cport1
	    arc-cport2
	    (make-arc-dports (ports-remove a arc-dports0)
			      (ports-adjoin a arc-dports1)
			      arc-dports2)))
	  ((ports-member b arc-cport2)
	   (make-mso-arc-state
	    arc-cport1
	    arc-cport2
	    (make-arc-dports (ports-remove a arc-dports0)
			      arc-dports1
			      (ports-adjoin a arc-dports2))))
	  (t (error "in graph-add-target arc-mso")))))

(defmethod graph-ren-target (a b (state mso-arc-state))
  (make-mso-arc-state
   (ports-subst b a (arc-cport1 state))
   (ports-subst b a (arc-cport2 state))
   (arc-dports-subst b a  (arc-dports state))))

(defgeneric arc-mso-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg null))
    (let* ((port (port-of root))
	   (vbits (vbits root)))
      (if (edge-bit-p vbits) ;; arc
	  (make-mso-arc-state 
	   (make-empty-ports)
	   (make-empty-ports)
	   (make-arc-dports
	    (make-ports-from-port port) (make-empty-ports) (make-empty-ports)))
	  ;; node
	  (let ((vb1 (aref vbits 1))
		(vb2 (aref vbits 2)))
	    (cond
	      ((= 1 (* vb1 vb2))
	       (make-mso-arc-state 
		(make-ports-from-port port)
		(make-ports-from-port port)
		(make-empty-arc-dports)))
	      ((= vb1 1)
	       (make-mso-arc-state 
		(make-ports-from-port port)
		(make-empty-ports)
		(make-empty-arc-dports)))
	      ((= vb2 1)
	       (make-mso-arc-state
		(make-empty-ports)
		(make-ports-from-port port)
		(make-empty-arc-dports)))
	      (t  (make-mso-arc-state 
		   (make-empty-ports)
		   (make-empty-ports)
		   (make-empty-arc-dports))))))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'arc-mso-transitions-fun)))

(defun basic-arc-mso-automaton (&key (cwd 0))
  "automaton for ARC-MSO(X1,X2) with m = 1 + 2"
  (make-cwd-automaton
   (cwd-vbits-signature cwd 3 :orientation t)
   (lambda (root states)
     (arc-mso-transitions-fun root states))
   :name (cwd-automaton-name "ARC-MSO(X1,X2)" cwd)
   ;; :precondition-automaton
   ;; (vbits-cylindrification
   ;;  (oriented-mso-term-automaton :cwd cwd)
   ;;  '(2 3))
   )
  )

(defun table-basic-arc-mso-automaton (cwd)
  (compile-automaton (basic-arc-mso-automaton :cwd cwd)))

(defun test-mso-arc ()
  (let ((oterm (input-cwd-term 
		"add_b->d(add_d->c(oplus(d^010[1],ren_e_a(ren_d_a(add_c->e(add_e->d(oplus(e^001[2],oplus(c^100[5],ren_e_a(ren_c_a(add_d->e(add_e->c(oplus(e^000[3],oplus(d^100[6],add_c->a(oplus(c^100[7],add_a->b(oplus(b^100[8],a^000[4])))))))))))))))))))")))
;;    (assert (mso-term-p oterm))
    (assert (recognized-p oterm (basic-arc-mso-automaton)))))

