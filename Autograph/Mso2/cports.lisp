;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass cmports (mports) ())

(defclass 3cports (cmports) ())
(defclass 2cports (cmports) ())

(defgeneric make-2cports (cports)
  (:method ((cports list))
    (make-multi-max-container-generic cports '2cports #'eql 2)))

(defgeneric make-3cports (cports)
  (:method ((cports list))
    (make-multi-max-container-generic cports '3cports #'eql 3)))

(defmethod print-object :before ((s cmports) stream)
  (format stream "C"))
