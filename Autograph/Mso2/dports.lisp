;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric dports0 (dports)
  (:documentation "port of graph edge with no incidence edge"))

(defgeneric dports1 (dports)
  (:documentation "port of graph edge with one incidence edge"))

(defgeneric dports2 (dports))
(defgeneric dports3 (dports))

;; gamma_ij i:indegree j: outdegree
;; dports0 = gamma_00 labels of isolated d-vertices
;; dports1 = gamma_01 (1 outgoing arc)
;; dports2 = gamma_10 (1 ingoing arg)
;; dports3 = gamma_11 (2 arcs)
;; in the non oriented case, arc a--b with a cport is ingoing for b (dports1 unused)

(defclass dports ()
  ((dports0 :type ports :initarg :dports0 :reader dports0)
   (dports1 :type ports :initarg :dports1 :reader dports1)
   (dports2 :type ports :initarg :dports2 :reader dports2)
   (dports3 :type ports :initarg :dports3 :reader dports3)))

(defgeneric make-dports (dports0 dports1 dports2 dports3)
  (:method ((dports0 ports) (dports1 ports) (dports2 ports) (dports3 ports))
    (make-instance
     'dports
     :dports0 dports0 :dports1 dports1 :dports2 dports2 :dports3 dports3)))

(defmethod print-object ((s dports) stream)
  (format stream "D~A-~A-~A-~A"
	  (dports0 s)
	  (dports1 s)
	  (dports2 s)
	  (dports3 s)))

(defmethod ports-of ((s dports))
  (reduce #'ports-union
	  (list (dports0 s) (dports1 s) (dports2 s) (dports3 s))))

(defgeneric dports-union (dports1 dports2)
  (:method ((dports1 dports) (dports2 dports))
    (make-dports
     (ports-union (dports0 dports1) (dports0 dports2))
     (ports-union (dports1 dports1) (dports1 dports2))
     (ports-union (dports2 dports1) (dports2 dports2))
     (ports-union (dports3 dports1) (dports3 dports2)))))

(defgeneric dports-subst (a b dports)
  (:method (a b (dports dports))
    (make-dports 
     (ports-subst a b (dports0 dports))
     (ports-subst a b (dports1 dports))
     (ports-subst a b (dports2 dports))
     (ports-subst a b (dports3 dports)))))

(defgeneric disjoint-dports (dports)
  (:method  ((dports dports))
    (let ((dports0 (dports0 dports))
	  (dports1 (dports1 dports))
	  (dports2 (dports2 dports))
	  (dports3 (dports3 dports)))
      (and
       (ports-empty-p (ports-intersection dports0 dports1))
       (ports-empty-p (ports-intersection dports0 dports2))
       (ports-empty-p (ports-intersection dports0 dports3))
       (ports-empty-p (ports-intersection dports1 dports2))
       (ports-empty-p (ports-intersection dports1 dports3))
       (ports-empty-p (ports-intersection dports2 dports3))))))
