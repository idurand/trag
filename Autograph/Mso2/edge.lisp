;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric edge-dports0 (edge-dports)
  (:documentation "ports of unconnected edges"))

(defgeneric edge-dports1 (edge-dports)
  (:documentation "port of edge connected to a node in X1"))

(defgeneric edge-dports2 (edge-dports)
  (:documentation "port edge connected to a node in X2"))

(defclass edge-dports ()
  ((edge-dports0 :type ports :initarg :edge-dports0 :reader edge-dports0)
   (edge-dports1 :type ports :initarg :edge-dports1 :reader edge-dports1)
   (edge-dports2 :type ports :initarg :edge-dports2 :reader edge-dports2)))

(defmethod ports-of ((edge-dports edge-dports))
  (ports-union (edge-dports0 edge-dports)
	       (ports-union (edge-dports1 edge-dports)
			    (edge-dports2 edge-dports))))

(defgeneric make-edge-dports (edge-dports0 edge-dports1 edge-dports2)
  (:method ((edge-dports0 ports) (edge-dports1 ports) (edge-dports2 ports))
    (make-instance
     'edge-dports
     :edge-dports0 edge-dports0
     :edge-dports1 edge-dports1
     :edge-dports2 edge-dports2)))

(defun make-empty-edge-dports ()
  (make-edge-dports
   (make-empty-ports)
   (make-empty-ports)
   (make-empty-ports)))

(defmethod print-object ((s edge-dports) stream)
  (format stream "D~A-~A-~A"
	  (edge-dports0 s)
	  (edge-dports1 s)
	  (edge-dports2 s)))

(defgeneric edge-dports-union (edge-dports1 edge-dports2)
  (:method ((dports1 edge-dports) (dports2 edge-dports))
    (make-edge-dports
     (ports-union (edge-dports0 dports1) (edge-dports0 dports2))
     (ports-union (edge-dports1 dports1) (edge-dports1 dports2))
     (ports-union (edge-dports2 dports1) (edge-dports2 dports2)))))

(defgeneric edge-dports-subst (a b edge-dports)
  (:method (a b (edge-dports edge-dports))
    (make-edge-dports
     (ports-subst a b (edge-dports0 edge-dports))
     (ports-subst a b (edge-dports1 edge-dports))
     (ports-subst a b (edge-dports2 edge-dports)))))

(defgeneric edge-cport1 (mso-edge-state)
  (:documentation "cport in X1 in a ports container so that it can be missing"))

(defgeneric edge-cport2 (mso-edge-state)
  (:documentation "cport X2 in a ports container so that it can be missing"))

(defclass mso-edge-state (graph-state)
  ((edge-cport1 :type ports :initarg :edge-cport1 :reader edge-cport1)
   (edge-cport2 :type ports :initarg :edge-cport2 :reader edge-cport2)
   (edge-dports :initarg :edge-dports :reader edge-dports)))

(defgeneric make-mso-edge-state (edge-cport1 edge-cport2 edge-dports)
  (:method ((edge-cport1 ports) (edge-cport2 ports) (edge-dports edge-dports))
    (make-instance
     'mso-edge-state :edge-cport1 edge-cport1 :edge-cport2 edge-cport2
		     :edge-dports edge-dports)))

(defmethod print-object ((state mso-edge-state) stream)
  (format stream "~A-~A~A" (edge-cport1 state) (edge-cport2 state) (edge-dports state)))

(defgeneric edge-cports (state)
  (:method ((s mso-edge-state))
    (ports-union (edge-cport1 s) (edge-cport2 s))))

(defmethod graph-oplus-target ((state1 (eql *ok-state*)) (state2 mso-edge-state))
  (graph-oplus-target state2 state1))

(defmethod graph-oplus-target ((state1 mso-edge-state) (state2 (eql *ok-state*)))
  (when (container-empty-p (edge-cports state1))
    *ok-state*))

(defmethod graph-oplus-target ((state1 mso-edge-state) (state2 mso-edge-state))
  (let* ((edge-cport11 (edge-cport1 state1))
	 (edge-cport12 (edge-cport2 state1))
	 (edge-cport21 (edge-cport1 state2))
	 (edge-cport22 (edge-cport2 state2)))
    (unless (or
	     ;; X1 not singleton
	     (> (+ (container-size edge-cport11) (container-size edge-cport21)) 1)
	     ;; X2 not singleton
	     (> (+ (container-size edge-cport12) (container-size edge-cport22)) 1))
      (make-mso-edge-state
       (container-union edge-cport11 edge-cport21)
       (container-union edge-cport12 edge-cport22)
       (edge-dports-union (edge-dports state1) (edge-dports state2))))))

(defgeneric edge-mso-add-ok (a b edge-cport1 edge-cport2
			     edge-dports0 edge-dports1 edge-dports2)
  (:method ((a integer) (b integer) (edge-cport1 ports) (edge-cport2 ports)
	    (edge-dports0 ports) (edge-dports1 ports) (edge-dports2 ports))
    (or
     (and (ports-member a edge-cport2) (ports-member b edge-dports1))
     (and (ports-member b edge-cport2) (ports-member a edge-dports1))
     (and (ports-member a edge-cport1) (ports-member b edge-dports2))
     (and (ports-member b edge-cport1) (ports-member a edge-dports2))
     (and (ports-member a edge-cport1) (ports-member a edge-cport2)
	  (ports-member b edge-dports0))
     (and (ports-member b edge-cport1) (ports-member b edge-cport2)
	  (ports-member a edge-dports0)))))

(defmethod graph-add-target (a b (state mso-edge-state))
  (let* ((edge-cport1 (edge-cport1 state))
	 (edge-cport2 (edge-cport2 state))
	 (edge-cports (edge-cports state))
	 (edge-dports (edge-dports state))
	 (edge-dports0 (edge-dports0 edge-dports))
	 (edge-dports1 (edge-dports1 edge-dports))
	 (edge-dports2 (edge-dports2 edge-dports))
	 (all-dports (ports-of edge-dports))
	 (all-ports (ports-union-gen
		 (list edge-cports edge-dports0 edge-dports1 edge-dports2))))
    (unless (and (ports-member a all-ports) (ports-member b all-ports))
      (return-from graph-add-target state))
    (when (or (and (ports-member a edge-cports) (ports-member b edge-cports))
	      (and (ports-member a all-dports) (ports-member b all-dports)))
      (return-from graph-add-target))
    (when (edge-mso-add-ok
	   a b edge-cport1 edge-cport2 edge-dports0 edge-dports1 edge-dports2)
      (return-from graph-add-target *ok-state*))
    (cond ((ports-member a edge-cport1)
	   (make-mso-edge-state
	    edge-cport1
	    edge-cport2
	    (make-edge-dports (ports-remove b edge-dports0)
			      (ports-adjoin b edge-dports1)
			      edge-dports2)))
	  ((ports-member a edge-cport2)
	   (make-mso-edge-state
	    edge-cport1
	    edge-cport2
	    (make-edge-dports (ports-remove b edge-dports0)
			      edge-dports1
			      (ports-adjoin b edge-dports2))))
	  ;; necessarily a in dports0
	  ((ports-member b edge-cport1)
	   (make-mso-edge-state
	    edge-cport1
	    edge-cport2
	    (make-edge-dports (ports-remove a edge-dports0)
			      (ports-adjoin a edge-dports1)
			      edge-dports2)))
	  ((ports-member b edge-cport2)
	   (make-mso-edge-state
	    edge-cport1
	    edge-cport2
	    (make-edge-dports (ports-remove a edge-dports0)
			      edge-dports1
			      (ports-adjoin a edge-dports2))))
	  (t (error "in graph-add-target edge mso")))))

(defmethod graph-ren-target (a b (state mso-edge-state))
  (make-mso-edge-state
   (ports-subst b a (edge-cport1 state))
   (ports-subst b a (edge-cport2 state))
   (edge-dports-subst b a  (edge-dports state))))

(defgeneric edge-mso-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg null))
    (let* ((port (port-of root))
	   (vbits (vbits root))
	   (vb1 (aref vbits 1))
	   (vb2 (aref vbits 2)))
      (if (edge-bit-p vbits) ;; edge
	  (and
	   (zerop vb1) (zerop vb2)
	   (make-mso-edge-state 
	    (make-empty-ports)
	    (make-empty-ports)
	    (make-edge-dports
	     (make-ports-from-port port)
	     (make-empty-ports)
	     (make-empty-ports))))
	  ;; node
	  (cond
	    ((= 1 (* vb1 vb2)) nil)
	    ((= vb1 1)
	     (make-mso-edge-state 
	      (make-ports-from-port port)
	      (make-empty-ports)
	      (make-empty-edge-dports)))
	    ((= vb2 1)
	     (make-mso-edge-state
	      (make-empty-ports)
	      (make-ports-from-port port)
	      (make-empty-edge-dports)))
	    (t  (make-mso-edge-state 
		 (make-empty-ports)
		 (make-empty-ports)
		 (make-empty-edge-dports)))))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'edge-mso-transitions-fun)))

(defun basic-edge-mso-automaton (&key (cwd 0))
  "automaton for EDGE-MSO(X1,X2) with m = 1 + 2"
  (make-cwd-automaton
   (cwd-vbits-signature cwd 3)
     (lambda (root states)
       (edge-mso-transitions-fun root states))
     :name (cwd-automaton-name "EDGE-MSO(X1,X2)" cwd)
     :precondition-automaton
     (vbits-cylindrification
      (unoriented-mso-term-automaton :cwd cwd) '(2 3))))

(defun table-basic-edge-mso-automaton (cwd) ; edge-mso between X1 and X2
  (compile-automaton (basic-edge-mso-automaton :cwd cwd)))

(defun test-mso-edge ()
  (let ((term (input-cwd-term "add_b_c(oplus(c^010[1],ren_c_a(ren_d_a(add_c_d(oplus(d^000[3],oplus(add_a_b(oplus(b^100[6],add_a_c(oplus(c^100[8],a^000[4])))),add_a_b(oplus(b^100[5],add_a_c(oplus(c^100[7],a^001[2])))))))))))")))
    (assert (mso-term-p term))
    (assert (recognized-p term (basic-edge-mso-automaton)))))
    
