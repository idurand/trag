;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun hamiltonian-automaton (oriented)
  (if oriented
      (oriented-hamiltonian-automaton)
      (unoriented-hamiltonian-automaton)))

(defun hamiltonian-transducer (oriented)
  (if oriented
      (oriented-hamiltonian-transducer)
      (unoriented-hamiltonian-transducer)))

(defgeneric graph-hamiltonian-p (unoriented-graph)
  (:documentation "is the graph hamiltonian")
  (:method ((graph graph))
    (recognized-p
     (cwd-decomposition-incidence graph)
     (hamiltonian-automaton (oriented-p graph)))))

(defgeneric cwd-term-to-iterm (cwd-term)
  (:method ((term term))
    (cwd-decomposition-incidence (cwd-term-to-graph term))))

(defgeneric cwd-iterm-hamiltonian-p (cwd-iterm)
  (:documentation "is the corresponding unoriented graph hamiltonian")
  (:method ((cwd-iterm term))
    (let ((oriented (oriented-p cwd-iterm)))
      (enum-recognized-p cwd-iterm (hamiltonian-automaton oriented)))))

(defgeneric cwd-term-hamiltonian-p (cwd-term)
  (:documentation "is the corresponding unoriented graph hamiltonian")
  (:method ((cwd-term term))
    (cwd-iterm-hamiltonian-p (cwd-term-to-iterm cwd-term))))

(defun get-cycles (iterm pa num-nodes)
  (let ((eti (positions-assignment-to-etis iterm pa)))
    (mapcar #'car
	    (remove-if
	     (lambda (pair)
	       (or (zerop (aref (cdr pair) 0)) (member (car pair) num-nodes)))
	     eti))))

(defun graph-hamiltonian-cycle-enumerator (graph)
  (let* ((oriented (oriented-p graph))
	 (transducer (hamiltonian-transducer oriented))
	 (iterm (cwd-decomposition-incidence graph))
	 (num-nodes (container-mapcar #'num (graph-nodes graph))))
    (make-no-duplicates-enumerator
     (make-funcall-enumerator (lambda (pa)
				(get-cycles iterm pa num-nodes))
			      (final-value-enumerator iterm transducer))
     :test #'equal)))

(defun hamiltonian-cycles (graph)
  (let* ((iterm (cwd-decomposition-incidence graph))
	 (pas (compute-final-value iterm (hamiltonian-transducer (oriented-p graph))))
	 (num-nodes (container-mapcar #'num (graph-nodes graph))))
    (remove-duplicates
     (mapcar (lambda (pa) (get-cycles iterm pa num-nodes))
	     pas)
     :test #'equal)))

(defun test-graph-hamiltonian ()
  (let* ((paths '((1 2 3 4 1) (1 3)))
	 (g (graph-from-paths paths))
	 (og (graph-from-paths paths :oriented t)))
    (assert (graph-hamiltonian-p g))
    (assert (not (graph-hamiltonian-p (graph-from-paths '((1 2  4 1) (1 3))))))
    (assert (graph-hamiltonian-p og))
    (assert (not (graph-hamiltonian-p (graph-from-paths '((1 2 3) (4 3) (1 3)) :oriented t))))))
