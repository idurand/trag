;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; See Bruno Courcelle's article "Fly-automata for checking MSO2 2 graph properties"
;;; Conference in Lagos Brasil

;;; OLINK(X1,X2): \exists x \in X1 and y \in X2 s.t. x → y (oriented case)
;;;  LINK(X1,X2): \exists x \in X1 and y \in X2 s.t. x -- y (oriented case)

(in-package :autograph)

(defgeneric link-cports1 (state)
  (:documentation "cports in X1"))

(defgeneric link-cports2 (state)
  (:documentation "cports in X2"))

(defgeneric link-dports (state))
(defgeneric link-dports1 (state))
(defgeneric link-dports2 (state))

(defclass mso2-link-state (graph-state)
  ((link-cports1 :initarg :link-cports1 :reader link-cports1)
   (link-cports2 :initarg :link-cports2 :reader link-cports2)
   (link-dports :initarg :link-dports :reader link-dports)
   (link-dports1 :initarg :link-dports1 :reader link-dports1)
   (link-dports2 :initarg :link-dports2 :reader link-dports2)))

(defgeneric make-mso2-link-state
    (link-cports1 link-cports2 link-dports link-dports1 link-dports2)
  (:method ((link-cports1 ports) (link-cports2 ports)
	    (link-dports ports) (link-dports1 ports) (link-dports2 ports))
  (make-instance 'mso2-link-state
		 :link-cports1 link-cports1 :link-cports2 link-cports2
		 :link-dports link-dports
		 :link-dports1 link-dports1 :link-dports2 link-dports2)))

(defmethod print-object ((state mso2-link-state) stream)
  (format stream "C~A~A-D~A-~A-~A"
	  (link-cports1 state)
	  (link-cports2 state)
	  (link-dports state)
	  (link-dports1 state)
	  (link-dports2 state)))

(defmethod link-cports ((s mso2-link-state))
  (ports-union (link-cports1 s) (link-cports2 s)))

(defmethod graph-oplus-target ((state1 mso2-link-state) (state2 mso2-link-state))
  (let ((link-cports11 (link-cports1 state1))
	(link-cports21 (link-cports2 state1))
	(link-cports12 (link-cports1 state2))
	(link-cports22 (link-cports2 state2))
	(link-dports1 (link-dports state1))
	(link-dports11 (link-dports1 state1))
	(link-dports21 (link-dports2 state1))
	(link-dports2 (link-dports state2))
	(link-dports12 (link-dports1 state2))
	(link-dports22 (link-dports2 state2)))
    (make-mso2-link-state
     (ports-union link-cports11 link-cports12)
     (ports-union link-cports21 link-cports22)
     (ports-union link-dports1 link-dports2)
     (ports-union link-dports11 link-dports12)
     (ports-union link-dports21 link-dports22))))

(defgeneric link-ok-add (a b cports1 cports2 dports dports1 dports2) (:method ((a integer) (b integer) (cports1 ports) (cports2 ports) (dports ports) (dports1 ports) (dports2 ports))
    (or (and (ports-member a dports1) (ports-member b cports2))
	    (and (ports-member a dports2)
		 (not *oriented*)
		 (ports-member b cports1))
	    (and (ports-member a cports1) (ports-member b dports2))
	    (and (not *oriented*)
		 (or
		  (and
		   (ports-member a cports1)
		   (ports-member a cports2)
		   (ports-member b dports))
		  (and 
		   (ports-member b cports1)
		   (ports-member b cports2)
		   (ports-member a dports)))))))

(defmethod graph-add-target (a b (state mso2-link-state))
  (let* ((cports1 (link-cports1 state))
	 (cports2 (link-cports2 state))
	 (dports1 (link-dports1 state))
	 (dports2 (link-dports2 state))
	 (dports (link-dports state))
	 (a-dport-p (ports-member a dports)))
    (unless (or (and a-dport-p (ports-member b cports2))
		(and *oriented* (ports-member a cports1))
		(and a-dport-p (not *oriented*)
		     (or (ports-member b cports1)
			 (ports-member b cports2))))
      (return-from graph-add-target state))
    (when (link-ok-add a b cports1 cports2 dports dports1 dports2)
      (return-from graph-add-target *ok-ok-state*))
    (assert (or (and *oriented* (ports-member a cports1))
		(ports-member a dports)))
    (if (ports-member a cports1)
	(make-mso2-link-state
	 cports1
	 cports2
	 dports
	 (ports-adjoin b dports1)
	 dports2)
	(make-mso2-link-state
	 cports1
	 cports2
	 dports
	 (if (ports-member b cports1)
	     (ports-adjoin a dports1)
	     dports1)
	 (if (ports-member b cports2)
	     (ports-adjoin a dports2)
	     dports2)))))

(defmethod graph-ren-target (a b (state mso2-link-state))
  (let ((link-cports1 (link-cports1 state))
	(link-cports2 (link-cports2 state))
	(link-dports (link-dports state))
	(link-dports1 (link-dports1 state))
	(link-dports2 (link-dports2 state)))
    (make-mso2-link-state
     (ports-subst b a link-cports1)
     (ports-subst b a link-cports2)
     (ports-subst b a link-dports)
     (ports-subst b a link-dports1)
     (ports-subst b a link-dports2))))

(defgeneric link-mso2-transitions-fun (root arg)
  (:method ((root vbits-symbol) (arg null))
    (let* ((port (port-of root))
	   (vbits (vbits root))
	   (vb1 (aref vbits 1))
	   (vb2 (aref vbits 2)))
      (if (edge-bit-p vbits) ;; edge
	  (and (zerop vb1) (zerop vb2)
	       (make-mso2-link-state 
		(make-empty-ports) (make-empty-ports)
		(make-ports-from-port port) (make-empty-ports) (make-empty-ports)))
	  (cond ;; node
	      ((= 1 (* vb1 vb2))
	       (make-mso2-link-state
		(make-ports-from-port port) (make-ports-from-port port)
		(make-empty-ports) (make-empty-ports) (make-empty-ports)))
	      ((= vb1 1)
	       (make-mso2-link-state 
		(make-ports-from-port port) (make-empty-ports)
		(make-empty-ports) (make-empty-ports) (make-empty-ports)))
	      ((= vb2 1)
	       (make-mso2-link-state 
		(make-empty-ports)
		(make-ports-from-port port)
		(make-empty-ports) (make-empty-ports) (make-empty-ports)))
	      (t  (make-mso2-link-state 
		   (make-empty-ports)
		   (make-empty-ports)
		   (make-empty-ports) (make-empty-ports) (make-empty-ports)))))))
  (:method  ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'link-mso2-transitions-fun)))

(defun basic-link-mso2-automaton (&key (oriented nil) (cwd 0))
  "automaton for LINK-MSO2(X1,X2) with m = 1 + 2"
  (let ((ports (port-iota cwd)))
    (make-cwd-automaton
     (let ((*oriented* oriented))
       (cwd-vbits-signature cwd 3 :orientation oriented))
     (lambda (root states)
       (let ((*ports* ports)
	     (*oriented* oriented))
	 (link-mso2-transitions-fun root states)))
     :name (cwd-automaton-name
	    (format nil "~A-MSO2(X1,X2)"
		    (if oriented "OLINK" "LINK"))
	    cwd)
     :precondition-automaton (vbits-cylindrification
			      (intersection-automaton
			       (non-redundant-automaton nil :cwd cwd)
			       (unoriented-mso-term-automaton :cwd cwd))
			      '(1 2 3)))))

(defun test-link-mso2 ()
  (let ((oterm1 (input-cwd-term
		 "add_c->b(oplus(c^010[1],ren_d_a(ren_c_a(add_b->d(add_d->c(oplus(d^001[2],oplus(b^100[4],add_c->a(oplus(c^100[5],a^000[3]))))))))))"))
	(oterm2 (input-cwd-term
		 "add_c->b(oplus(c^010[1],ren_d_a(ren_c_a(add_b->d(add_d->c(oplus(d^000[2],oplus(b^100[4],add_c->a(oplus(c^100[5],a^001[3]))))))))))"))
	(of (basic-link-mso2-automaton :oriented t)))
    (assert (recognized-p oterm1 of))
    (assert (not (recognized-p oterm2 of))))
  (let ((f (basic-link-mso2-automaton))
	(term1
	  (input-cwd-term
	   "add_b_c(oplus(c^010[1],ren_c_a(ren_d_a(add_c_d(oplus(d^000[3],oplus(add_a_b(oplus(b^100[6],add_a_c(oplus(c^100[8],a^001[4])))),add_a_b(oplus(b^100[5],add_a_c(oplus(c^100[7],a^000[2])))))))))))
"))
	(term2
	  (input-cwd-term
	   "add_b_c(oplus(c^010[1],ren_c_a(ren_d_a(add_c_d(oplus(d^001[3],oplus(add_a_b(oplus(b^100[6],add_a_c(oplus(c^100[8],a^000[4])))),add_a_b(oplus(b^100[5],add_a_c(oplus(c^100[7],a^000[2])))))))))))
"))
	(term3 (input-cwd-term "add_a_b(oplus(b^100,oplus(a^001,a^010)))")))
    (assert (recognized-p term3 f))
    (assert (not (recognized-p term1 f)))
    (assert (not (recognized-p term2 f)))))

;; (defun test-link-mso2 ()
;;   (let ((oterm (input-cwd-term "add_b#->b(add_b->c#(oplus(b^10[1],add_c#->a(oplus(c#^00[3],add_a->b#(oplus(b#^00[4],a^01[2])))))))")))
;;     (recognized-p oterm (basic-link-mso2-automaton :oriented t)))
;;   (let ((term
;; 	  (input-cwd-term
;; 	   "add_b#_b(oplus(b^01[2],add_b#_a(oplus(b#^00[3],a^10[1]))))")))
;;     (recognized-p term (basic-link-mso2-automaton))))

    
