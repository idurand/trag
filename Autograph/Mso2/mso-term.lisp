;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun mso-term-automaton (&key (oriented nil) (cwd 0))
  (if oriented
      (oriented-mso-term-automaton :cwd cwd)
      (unoriented-mso-term-automaton :cwd cwd)))

(defgeneric mso-term-p (term)
  (:method ((term term))
    (let ((oriented (oriented-p term)))
      (when (> (signature-nb-set-variables (signature term)) 1)
	(setq term (car (vbits-projection term '(1)))))
      (recognized-p term (mso-term-automaton :oriented oriented)))))
