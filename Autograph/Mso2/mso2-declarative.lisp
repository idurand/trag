;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Mikhail Raskin
(in-package :autograph)

(define-declarative-automaton
  mso2-correct-noloop-ori-decl
    (vertex-multiplicity
     ;; edge entries with no connections
     pending-edges
     ;; relation: edge label -> vertex label
     incoming-edges outgoing-edges
     ;; edge entries we must not touch anymore
     completed-edges)
  :vbits 1
  :orientation t
  :final ((s)
          (and (container-empty-p (pending-edges s))
               (container-empty-p (container-union (incoming-edges s)
                                                   (outgoing-edges s)))))
  :valid ((s)
          (disjoint-sets
            (pending-edges s)
            (relation-full-preimage (incoming-edges s))
            (relation-full-preimage (outgoing-edges s))
            (container-union
              (completed-edges s)
              (multiset-support (vertex-multiplicity s)))))
  :vertex ((x vbits)
           (if (= (elt vbits 0) 0)
             (make
               :pending-edges (set-of-args)
               :incoming-edges (set-of-args)
               :outgoing-edges (set-of-args)
               :completed-edges (set-of-args)
               :vertex-multiplicity (multimaxset-of-args 2 x))
             (make
               :pending-edges (set-of-args x)
               :incoming-edges (set-of-args)
               :outgoing-edges (set-of-args)
               :completed-edges (set-of-args)
               :vertex-multiplicity (multimaxset-of-args 2))))
  :ren ((a b s)
        (make
          :pending-edges (rename-in-set a b (pending-edges s))
          :incoming-edges (rename-in-relation a b (incoming-edges s))
          :outgoing-edges (rename-in-relation a b (outgoing-edges s))
          :completed-edges (rename-in-set a b (completed-edges s))
          :vertex-multiplicity
          (rename-in-multiset a b (vertex-multiplicity s))))
  :oplus ((s1 s2)
          (make
            :pending-edges (container-union (pending-edges s1)
                                            (pending-edges s2))
            :incoming-edges (container-union (incoming-edges s1)
                                             (incoming-edges s2))
            :outgoing-edges (container-union (outgoing-edges s1)
                                             (outgoing-edges s2))
            :completed-edges (container-union (completed-edges s1)
                                              (completed-edges s2))
            :vertex-multiplicity (container-union (vertex-multiplicity s1)
                                                  (vertex-multiplicity s2))))
  :add ((a b s)
        (let* ((ab (set-of-args a b))
               (edges (container-union-gen
                        (list 
                          (pending-edges s)
                          (relation-full-preimage (incoming-edges s))
                          (relation-full-preimage (outgoing-edges s))
                          (completed-edges s))))
               (all (container-union edges (multiset-support 
                                             (vertex-multiplicity s))))
               (exists (container-subset-p ab all))
               (overflow
                 (or (> (multiplicity a (vertex-multiplicity s)) 1)
                     (> (multiplicity b (vertex-multiplicity s)) 1)
                     (container-member a (completed-edges s))
                     (container-member b (completed-edges s))
                     (not (container-empty-p (relation-image
                                               a (incoming-edges s))))
                     (not (container-empty-p (relation-image
                                               b (outgoing-edges s))))))
               (same-type
                 (or (container-subset-p ab edges)
                     (container-subset-p ab (vertex-multiplicity s))))
               (loop
                 (or (and (container-member a (vertex-multiplicity s))
                          (container-member (list b a) (incoming-edges s)))
                     (and (container-member b (vertex-multiplicity s))
                          (container-member (list a b) (outgoing-edges s)))))
               (from-vertex (container-member a (vertex-multiplicity s)))
               (completion (or (not (container-empty-p
                                      (relation-image a (outgoing-edges s))))
                               (not (container-empty-p 
                                      (relation-image b (incoming-edges s)))))))
          (cond ((not exists) s)
                (overflow nil)
                (same-type nil)
                (loop nil)
                (t (make
                     :pending-edges (container-difference (pending-edges s) ab)
                     :incoming-edges (cond ((and from-vertex completion)
                                            (container-remove-if
                                              (lambda (x) (= (first x) b))
                                              (incoming-edges s)))
                                           ((or from-vertex completion)
                                            (incoming-edges s))
                                           (t (container-adjoin
                                                (list a b) (incoming-edges s))))
                     :outgoing-edges (cond ((and (not from-vertex) completion)
                                            (container-remove-if
                                              (lambda (x) (= (first x) a))
                                              (outgoing-edges s)))
                                           ((or (not from-vertex) completion)
                                            (outgoing-edges s))
                                           (t (container-adjoin
                                                (list b a) (outgoing-edges s))))
                     :completed-edges (if completion
                                        (container-adjoin
                                          (if from-vertex b a)
                                          (completed-edges s))
                                        (completed-edges s))
                     :vertex-multiplicity (vertex-multiplicity s)))))))

(define-declarative-automaton
  mso2-correct-ori-decl (vertex-multiplicity
                          pending-edges
                          incoming-edges outgoing-edges
                          completed-edges)
  :vbits 1 :orientation t
  :final ((s) (container-empty-p (container-union-gen
                                   (list (pending-edges s)
                                         (incoming-edges s)
                                         (outgoing-edges s)))))
  :valid ((s) (disjoint-sets (pending-edges s)
                             (incoming-edges s)
                             (outgoing-edges s)
                             (container-union (completed-edges s)
                                              (vertex-multiplicity s))))
  :vertex ((x vbits) (if (= (elt vbits 0) 0)
                       (make
                         :pending-edges (set-of-args)
                         :incoming-edges (set-of-args)
                         :outgoing-edges (set-of-args)
                         :completed-edges (set-of-args)
                         :vertex-multiplicity (multimaxset-of-args 2 x))
                       (make
                         :pending-edges (set-of-args x)
                         :incoming-edges (set-of-args)
                         :outgoing-edges (set-of-args)
                         :completed-edges (set-of-args)
                         :vertex-multiplicity (multimaxset-of-args 2))))
  :ren ((a b s)
        (make
          :pending-edges (rename-in-set a b (pending-edges s))
          :incoming-edges (rename-in-set a b (incoming-edges s))
          :outgoing-edges (rename-in-set a b (outgoing-edges s))
          :completed-edges (rename-in-set a b (completed-edges s))
          :vertex-multiplicity
          (rename-in-multiset a b (vertex-multiplicity s))))
  :oplus ((s1 s2)
          (make
            :pending-edges (container-union (pending-edges s1)
                                            (pending-edges s2))
            :incoming-edges (container-union (incoming-edges s1)
                                             (incoming-edges s2))
            :outgoing-edges (container-union (outgoing-edges s1)
                                             (outgoing-edges s2))
            :completed-edges (container-union (completed-edges s1)
                                              (completed-edges s2))
            :vertex-multiplicity (container-union (vertex-multiplicity s1)
                                                  (vertex-multiplicity s2))))
  :add ((a b s)
        (let* ((ab (set-of-args a b))
               (edges (container-union-gen (list (pending-edges s)
                                                 (incoming-edges s)
                                                 (outgoing-edges s)
                                                 (completed-edges s))))
               (all (container-union 
                      edges (multiset-support (vertex-multiplicity s))))
               (exists (container-subset-p ab all))
               (overflow (or (> (multiplicity a (vertex-multiplicity s)) 1)
                             (> (multiplicity b (vertex-multiplicity s)) 1)
                             (container-intersection-not-empty-p
                               ab (completed-edges s))
                             (container-member a (incoming-edges s))
                             (container-member b (outgoing-edges s))))
               (same-type (or (container-subset-p ab edges)
                              (disjoint-sets ab edges)))
               (from-vertex (container-member a (vertex-multiplicity s)))
               (completion (or (container-member a (outgoing-edges s))
                               (container-member b (incoming-edges s)))))
          (cond ((not exists) s)
                (overflow nil)
                (same-type nil)
                (t (make :pending-edges (container-difference
                                          (pending-edges s) ab)
                         :incoming-edges
                         (cond ((and from-vertex completion)
                                (container-remove b (incoming-edges s)))
                               ((or from-vertex completion)
                                   (incoming-edges s))
                               (t (container-adjoin a (incoming-edges s))))
                         :outgoing-edges
                         (cond ((and (not from-vertex) completion)
                                (container-remove a (outgoing-edges s)))
                               ((or (not from-vertex) completion)
                                (outgoing-edges s))
                               (t (container-adjoin b (outgoing-edges s))))
                         :completed-edges 
                         (cond ((not completion)
                                (completed-edges s))
                               (from-vertex 
                                 (container-adjoin b (completed-edges s)))
                               (t (container-adjoin a (completed-edges s))))
                         :vertex-multiplicity (vertex-multiplicity s)))))))

#+nil
(equality-automaton
  (minimize-automaton
    (compile-automaton
      (mso2-correct-ori-decl-automaton :cwd 2)))
  (minimize-automaton
    (compile-automaton
      (oriented-mso-term-automaton :cwd 2))))

#+nil
(Termauto::compile-automata
  (oriented-mso-term-automaton :cwd 0)
  (mso2-correct-ori-decl-automaton :cwd 2))

#+nil
(recognized-p
  (input-cwd-term "add_a->b(add_b->c(oplus(oplus(a^0,b^1),c^0)))")
  (mso2-correct-ori-decl-automaton))

#+nil
(recognized-p
  (input-cwd-term "add_a->b(add_b->a(oplus(oplus(a^0,b^1),c^0)))")
  (mso2-correct-ori-decl-automaton))

#+nil
(recognized-p
  (input-cwd-term "add_b->a(add_a->b(oplus(a^0,b^1)))")
  (mso2-correct-ori-decl-automaton))

#+nil
(recognized-p
  (input-cwd-term "add_a->b(ren_b_a(ren_a_b(oplus(oplus(a^0,b^0),a^0))))")
  (mso2-correct-ori-decl-automaton))

#+nil
(recognized-p
  (input-cwd-term "add_a->b(add_a->b(add_b->c(oplus(oplus(a^0,b^1),c^0))))")
  (mso2-correct-ori-decl-automaton))

(define-declarative-automaton
  mso2-correct-nonori-decl
  (vertex-multiplicity
    pending-edges
    half-edges
    completed-edges)
  :vbits 1 :orientation nil
  :final ((s)
          (and (container-empty-p (pending-edges s))
               (container-empty-p (half-edges s))))
  :valid ((s)
          (disjoint-sets
            (pending-edges s)
            (relation-full-preimage (half-edges s))
            (container-union
              (completed-edges s)
              (multiset-support (vertex-multiplicity s)))))
  :vertex ((x vbits)
           (if (= (elt vbits 0) 0)
             (make-mso2-correct-nonori-decl
               :pending-edges (set-of-args)
               :half-edges (set-of-args)
               :completed-edges (set-of-args)
               :vertex-multiplicity (multimaxset-of-args 3 x))
             (make-mso2-correct-nonori-decl
               :pending-edges (set-of-args x)
               :half-edges (set-of-args)
               :completed-edges (set-of-args)
               :vertex-multiplicity (multimaxset-of-args 3))))
  :ren ((a b s)
        (make-mso2-correct-nonori-decl
          :pending-edges (rename-in-set a b (pending-edges s))
          :half-edges (rename-in-relation a b (half-edges s))
          :completed-edges (rename-in-set a b (completed-edges s))
          :vertex-multiplicity
          (rename-in-multiset a b (vertex-multiplicity s))))
  :oplus ((s1 s2)
          (make-mso2-correct-nonori-decl
            :pending-edges (container-union (pending-edges s1)
                                            (pending-edges s2))
            :half-edges (container-union (half-edges s1)
                                         (half-edges s2))
            :completed-edges (container-union (completed-edges s1)
                                              (completed-edges s2))
            :vertex-multiplicity (container-union (vertex-multiplicity s1)
                                                  (vertex-multiplicity s2))))
  :add ((a b s)
        (let* ((ab (set-of-args a b))
               (edges (container-union-gen
                        (list
                          (pending-edges s)
                          (relation-full-preimage (half-edges s))
                          (completed-edges s))))
               (all (container-union
                      edges (multiset-support (vertex-multiplicity s))))
               (exists (container-subset-p ab all))
               (same-type
                 (or (container-subset-p ab edges)
                     (container-subset-p ab (vertex-multiplicity s))))
               (overflow (or (> (multiplicity a (vertex-multiplicity s)) 2)
                             (> (multiplicity b (vertex-multiplicity s)) 2)
                             (and (> (multiplicity 
                                       a (vertex-multiplicity s))
                                     1)
                                  (not (container-empty-p
                                         (relation-image b (half-edges s)))))
                             (and (> (multiplicity 
                                       b (vertex-multiplicity s))
                                     1)
                                  (not (container-empty-p
                                         (relation-image a (half-edges s)))))
                             (container-intersection-not-empty-p
                               ab (completed-edges s))))
               (double (or (= (multiplicity a (vertex-multiplicity s)) 2)
                           (= (multiplicity b (vertex-multiplicity s)) 2)))
               (completion (container-intersection-not-empty-p
                             ab (relation-full-preimage (half-edges s)))))
          (cond ((not exists) s)
                (same-type nil)
                (overflow nil)
                (double
                  (make-mso2-correct-nonori-decl
                    :pending-edges (container-difference (pending-edges s) ab)
                    :half-edges (half-edges s)
                    :completed-edges (container-union 
                                       (completed-edges s)
                                       (container-intersection
                                         ab (pending-edges s)))
                    :vertex-multiplicity (vertex-multiplicity s)))
                (t (make-mso2-correct-nonori-decl
                     :pending-edges (if completion
                                      (pending-edges s)
                                      (container-difference
                                        (pending-edges s) ab))
                     :half-edges (if completion
                                   (container-remove-if
                                     (lambda (x) (find (first x) (list a b)))
                                     (half-edges s))
                                   (container-union
                                     (if (container-member
                                           a (vertex-multiplicity s))
                                       (set-of-args (list b a))
                                       (set-of-args (list a b)))
                                     (half-edges s)))
                     :completed-edges (if completion
                                        (container-union
                                          (container-intersection
                                            ab (half-edges s))
                                          (completed-edges s))
                                        (completed-edges s))
                     :vertex-multiplicity (vertex-multiplicity s)))))))

(defun test-mso2-declarative ()
(let ((cwd 2))
  (equality-automaton
    (minimize-automaton
      (compile-automaton
        (intersection-automaton
          (vbits-cylindrification (non-redundant-decl-automaton :cwd cwd) '(1))
          (mso2-correct-nonori-decl-automaton :cwd cwd))))
    (minimize-automaton
      (compile-automaton
        (intersection-automaton
          (vbits-cylindrification (non-redundant-decl-automaton :cwd cwd) '(1))
          (unoriented-mso-term-automaton :cwd cwd))))))

(compute-target
  (input-cwd-term "add_a_b(oplus(oplus(b^0,b^0),oplus(a^1,b^0)))")
  (compile-automaton (mso2-correct-nonori-decl-automaton :cwd 2)))

(recognized-p
  (input-cwd-term "add_a_b(oplus(oplus(b^0,b^0),oplus(a^1,b^0)))")
  (unoriented-mso-term-automaton :cwd 2))

(recognized-p
  (input-cwd-term "add_a_b(add_a_b(oplus(a^1,b^0)))")
  (mso2-correct-nonori-decl-automaton))

(recognized-p
  (input-cwd-term "add_a_b(add_a_b(oplus(a^1,b^0)))")
  (unoriented-mso-term-automaton))

(recognized-p
  (input-cwd-term
    "add_a_b(oplus(ren_a_b(add_a_b(oplus(a^1,b^1))),oplus(a^0,a^0)))")
  (vbits-cylindrification (non-redundant-decl-automaton) '(1)))

(recognized-p
  (input-cwd-term
    "add_a_b(oplus(ren_a_b(add_a_b(oplus(a^1,b^1))),oplus(a^0,a^0)))")
  (mso2-correct-nonori-decl-automaton))

(recognized-p
  (input-cwd-term
    "add_a_b(oplus(ren_a_b(add_a_b(oplus(a^1,b^1))),oplus(a^0,a^0)))")
  (unoriented-mso-term-automaton))
)
