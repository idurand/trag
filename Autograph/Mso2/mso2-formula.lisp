(in-package :autograph)

;; MSO2 Term oriented
"
MSO2_correct_oriented(Edges): 
(![X]: (element(X,Edges)
         => 
         ((?[I]: (edge(I,X)&(![I1]:(edge(I1,X)=>I1=I))))&
          (?[O]: (edge(X,O)&(![O1]:(edge(X,O1)=>O1=O)))))))
& (![X,Y]: (edge(X,Y) => (element(X,Edges) <~> element(Y,Edges)))).
element(X,Y): subset(X,Y) & card(1,X).
"

(multi-formula-to-main-automaton
  "
  mso2_correct_oriented(Edges): 
  (![X]: (element(X,Edges)
           => 
           ((?[I]: (edge(I,X)&(![I1]:(edge(I1,X)=>I1=I))))&
            (?[O]: (edge(X,O)&(![O1]:(edge(X,O1)=>O1=O)))))))
  & (![X,Y]: (edge(X,Y) => (element(X,Edges) <~> element(Y,Edges)))).
  element(X,Y): subset(X,Y) & card(1,X).
  "
  :orientation t :cwd 2 :minimize t)

(multi-formula-to-main-automaton
  "
  mso2_correct_oriented(Edges): 
  (![X]: (element(X,Edges)
           => 
           (?[Ends]: (card(2,Ends) & disjoint(Ends,Edges) &
                                   (![E]: (edge(X,E)<=>element(E,Ends)))))))
  & (![X,Y]: (edge(X,Y) => (element(X,Edges) <~> element(Y,Edges)))).
  element(X,Y): subset(X,Y) & card(1,X).
  "
  :orientation nil :cwd 2 :minimize t)

(minimize-automaton (compile-automaton (mso-term-automaton :oriented t :cwd 2)))

(minimize-automaton (compile-automaton (mso-term-automaton :oriented nil :cwd 2)))

(equality-automaton
  (minimize-automaton (compile-automaton
                        (mso-term-automaton :oriented nil :cwd 2)))
  (multi-formula-to-main-automaton
    "
    mso2_correct_oriented(Edges): 
    (![X]: (element(X,Edges)
             => 
             (?[Ends]: (card(2,Ends) & disjoint(Ends,Edges) &
                                     (![E]: (edge(X,E)<=>element(E,Ends)))))))
    & (![X,Y]: (edge(X,Y) => (element(X,Edges) <~> element(Y,Edges)))).
    element(X,Y): subset(X,Y) & card(1,X).
    "
    :orientation nil :cwd 2 :minimize nil))

(recognized-p
  (input-cwd-term "add_a_b(oplus(oplus(a^0,b^1),oplus(a^0,a^0)))")
  (mso-term-automaton :oriented nil :cwd 2))

(recognized-p
  (input-cwd-term "add_a_b(oplus(oplus(a^0,b^1),oplus(a^0,a^0)))")
  (multi-formula-to-main-automaton
    "
    mso2_correct_oriented(Edges): 
    (![X]: (element(X,Edges)
             => 
             (?[Ends]: (card(2,Ends) & disjoint(Ends,Edges) &
                                     (![E]: (edge(X,E)<=>element(E,Ends)))))))
    & (![X,Y]: (edge(X,Y) => (element(X,Edges) <~> element(Y,Edges)))).
    element(X,Y): subset(X,Y) & card(1,X).
    "
    :orientation nil :cwd 2 :minimize nil)
  )

(loop
  for automaton in
  (list
    (minimize-automaton (compile-automaton
                          (mso-term-automaton :oriented t :cwd 2)))
    (multi-formula-to-main-automaton
      "
      mso2_correct_oriented(Edges): 
      (![X]: (element(X,Edges)
               => 
               ((?[I]: (edge(I,X)&(![I1]:(edge(I1,X)=>I1=I))))&
                (?[O]: (edge(X,O)&(![O1]:(edge(X,O1)=>O1=O)))))))
      & (![X,Y]: (edge(X,Y) => (element(X,Edges) <~> element(Y,Edges)))).
      element(X,Y): subset(X,Y) & card(1,X).
      "
      :orientation t :cwd 2 :minimize nil))
  with term := (input-cwd-term "add_a->b(oplus(oplus(a^0,b^1),a^0))")
  collect (recognized-p term automaton))
