(in-package :autograph)

(define-declarative-automaton
  mso2-union-of-cycles-nonori-decl
  (cycle-edges labels-0 labels-1 labels-2)
  :vbits 2 :orientation nil
  :vertex ((v vbits)
           (destructuring-bind (edge cycle)
             (map 'list #'onep vbits)
             (cond ((and cycle (not edge)) nil)
                   (cycle (make :cycle-edges (multimaxset-of-args 3 v)
                                :labels-0 (set-of-args)
                                :labels-1 (set-of-args)
                                :labels-2 (set-of-args)))
                   (edge (make :cycle-edges (multimaxset-of-args 3)
                               :labels-0 (set-of-args)
                               :labels-1 (set-of-args)
                               :labels-2 (set-of-args)))
                   (t (make :cycle-edges (multimaxset-of-args 3)
                               :labels-0 (set-of-args v)
                               :labels-1 (set-of-args)
                               :labels-2 (set-of-args))))))
  :final ((s) (and (container-empty-p (labels-1 s))
                   (container-empty-p (labels-0 s))))
  :valid ((s) (disjoint-sets (labels-0 s) (labels-1 s) (labels-2 s)))
  :ren ((a b s)
        (let ((labels-2 (rename-in-set a b (labels-2 s))))
          (make :cycle-edges (rename-in-multiset a b (cycle-edges s))
                :labels-0 (container-difference
                            (rename-in-set a b (labels-0 s))
                            labels-2)
                :labels-1 (rename-in-set a b (labels-1 s))
                :labels-2 labels-2)))
  :oplus ((r s)
          (let ((labels-2 (container-union (labels-2 r) (labels-2 s))))
            (make :cycle-edges (container-union (cycle-edges r) (cycle-edges s))
                  :labels-0 (container-difference
                              (container-union
                                (labels-0 r) (labels-0 s))
                              labels-2)
                  :labels-1 (container-union (labels-1 r) (labels-1 s))
                  :labels-2 labels-2)))
  :add ((a b s)
        (let* ((ab (set-of-args a b))
               (edge (container-car
                       (container-intersection
                         ab (multiset-support (cycle-edges s)))))
               (quantity (if edge (multiplicity edge (cycle-edges s)) 0))
               (v0 (container-intersection ab (labels-0 s)))
               (v1 (container-intersection ab (labels-1 s)))
               (v2 (container-intersection ab (labels-2 s))))
          (cond ((= quantity 0) s)
                ((> quantity 2) nil)
                ((every 'container-empty-p (list v0 v1 v2)) s)
                ((not (container-empty-p v2)) nil)
                ((and (not (container-empty-p v1)) (> quantity 1)) nil)
                ((and (= quantity 2)
                      (not (container-empty-p v0)))
                 (make :cycle-edges (cycle-edges s)
                       :labels-0 (container-difference (labels-0 s) v0)
                       :labels-1 (labels-1 s)
                       :labels-2 (container-union (labels-2 s) v0)))
                ((not (container-empty-p v0))
                 (make :cycle-edges (cycle-edges s)
                       :labels-0 (container-difference (labels-0 s) v0)
                       :labels-1 (container-union (labels-1 s) v0)
                       :labels-2 (labels-2 s)))
                ((not (container-empty-p v1))
                 (make :cycle-edges (cycle-edges s)
                       :labels-0 (labels-0 s)
                       :labels-1 (container-difference (labels-1 s) v1)
                       :labels-2 (container-union (labels-2 s) v1)))
                (t (error "Please apply mso2-union-of-cycles-nonori-decl only to correct MSO2 terms. Failed: add (~s ~s) to ~s" a b s))))))

(define-declarative-automaton
  mso2-connected-edges-decl #|all vertices + selected edges = connected|#
  (components connected)
  :vbits 2
  :final ((s) (connected s))
  :vertex ((v vbits)
           (make :connected t
                 :components (if (equal vbits
                                        #*10 #|unselected edge|#)
                               (set-of-args)
                               (set-of-args (set-of-args v)))))
  :ren ((a b s)
        (make
          :components
          (set-image (x (components s)) (set-image (y x) (if (= y a) b y)))
          :connected (connected s)))
  :oplus ((s1 s2)
          (make
            :components (container-union (components s1) (components s2))
            :connected (or (container-empty-p (components s1))
                           (container-empty-p (components s2)))))
  :add ((a b s)
        (let ((ab (set-of-args a b))
              (relevant (set-set-union (components s))))
          (if (container-subset-p ab relevant)
            (let* ((null-components
                     (container-remove-if
                       (lambda (x) (container-intersection-not-empty-p ab x))
                       (components s)))
                   (a-b-components
                     (container-difference (components s) null-components))
                   (components (container-union 
                                 null-components
                                 (set-of-args (set-set-union a-b-components)))))
              (make
                :components components
                :connected (<= (container-size components) 1)))
            s))))

(defun mso2-is-hamcycle-nonori-automaton (&key (cwd 0) (minimize nil))
  (let* ((nr (non-redundant-decl-automaton :cwd cwd :orientation nil))
         (nr (vbits-cylindrification nr '(1 2)))
         (nr (if minimize (minimize-automaton (compile-automaton nr)) nr))
         (tc (mso2-correct-nonori-decl-automaton :cwd cwd))
         (tc (vbits-cylindrification tc '(2)))
         (tc (if minimize (minimize-automaton (compile-automaton tc)) tc))
         (good (intersection-automaton nr tc))
         (good (if minimize (minimize-automaton (compile-automaton good)) good))
         (uc (mso2-union-of-cycles-nonori-decl-automaton :cwd cwd))
         (uc (intersection-automaton good uc))
         (uc (if minimize (minimize-automaton (compile-automaton uc)) uc))
         (cw (mso2-connected-edges-decl-automaton :cwd cwd :orientation nil))
         (cw (intersection-automaton good cw))
         (cw (if minimize (minimize-automaton (compile-automaton cw)) cw))
         (ihc (intersection-automaton uc cw))
         (ihc (if minimize (minimize-automaton (compile-automaton ihc)) ihc)))
    ihc))

(define-declarative-automaton
  mso2-union-of-cycles-ori-decl
  (cycle-edges labels-0 labels-in labels-out labels-2)
  :vbits 2 :orientation t
  :vertex ((v vbits)
           (destructuring-bind (edge cycle)
             (map 'list #'onep vbits)
             (cond ((and cycle (not edge)) nil)
                   (cycle (make :cycle-edges (multimaxset-of-args 2 v)
                                :labels-0 (set-of-args)
                                :labels-in (set-of-args)
                                :labels-out (set-of-args)
                                :labels-2 (set-of-args)))
                   (edge (make :cycle-edges (multimaxset-of-args 2)
                               :labels-0 (set-of-args)
                               :labels-in (set-of-args)
                               :labels-out (set-of-args)
                               :labels-2 (set-of-args)))
                   (t (make :cycle-edges (multimaxset-of-args 2)
                               :labels-0 (set-of-args v)
                               :labels-in (set-of-args)
                               :labels-out (set-of-args)
                               :labels-2 (set-of-args))))))
  :final ((s) (and (container-empty-p (labels-in s))
                   (container-empty-p (labels-out s))
                   (container-empty-p (labels-0 s))))
  :valid ((s) (disjoint-sets (labels-0 s) (labels-in s) (labels-out s)
                             (labels-2 s)))
  :ren ((a b s)
        (let ((labels-2 (rename-in-set a b (labels-2 s))))
          (make :cycle-edges (rename-in-multiset a b (cycle-edges s))
                :labels-0 (container-difference
                            (rename-in-set a b (labels-0 s))
                            labels-2)
                :labels-in (rename-in-set a b (labels-in s))
                :labels-out (rename-in-set a b (labels-out s))
                :labels-2 labels-2)))
  :oplus ((r s)
          (let ((labels-2 (container-union (labels-2 r) (labels-2 s))))
            (make :cycle-edges (container-union (cycle-edges r) (cycle-edges s))
                  :labels-0 (container-difference
                              (container-union
                                (labels-0 r) (labels-0 s))
                              labels-2)
                  :labels-in (container-union (labels-in r) (labels-in s))
                  :labels-out (container-union (labels-out r) (labels-out s))
                  :labels-2 labels-2)))
  :add ((a b s)
        (let* ((edge (container-car
                       (container-intersection
                         (set-of-args a b)
                         (multiset-support (cycle-edges s)))))
               (quantity (if edge (multiplicity edge (cycle-edges s)) 0)))
          (cond ((= quantity 0) s)
                ((> quantity 1) nil)
                ((or (container-member a (labels-2 s))
                     (container-member a (labels-out s))
                     (container-member b (labels-2 s))
                     (container-member b (labels-in s)))
                 nil)
                ((container-member a (labels-0 s))
                 (make :cycle-edges (cycle-edges s)
                       :labels-0 (container-difference (labels-0 s)
                                                       (set-of-args a))
                       :labels-in (labels-in s)
                       :labels-out (container-union (labels-out s)
                                                    (set-of-args a))
                       :labels-2 (labels-2 s)))
                ((container-member b (labels-0 s))
                 (make :cycle-edges (cycle-edges s)
                       :labels-0 (container-difference (labels-0 s)
                                                       (set-of-args b))
                       :labels-in (container-union (labels-in s)
                                                   (set-of-args b))
                       :labels-out (labels-out s)
                       :labels-2 (labels-2 s)))
                ((container-member a (labels-in s))
                 (make :cycle-edges (cycle-edges s)
                       :labels-0 (labels-0 s)
                       :labels-in (container-difference (labels-in s)
                                                       (set-of-args a))
                       :labels-out (labels-out s)
                       :labels-2 (container-union (labels-2 s)
                                                    (set-of-args a))))
                ((container-member b (labels-out s))
                 (make :cycle-edges (cycle-edges s)
                       :labels-0 (labels-0 s)
                       :labels-in (labels-in s)
                       :labels-out (container-difference (labels-out s)
                                                         (set-of-args b))
                       :labels-2 (container-union (labels-2 s)
                                                  (set-of-args b))))
                (t s)))))

(defun mso2-is-hamcycle-ori-automaton (&key (cwd 0) (minimize nil))
  (let* ((nr (non-redundant-decl-automaton :cwd cwd :orientation t))
         (nr (vbits-cylindrification nr '(1 2)))
         (nr (if minimize (minimize-automaton (compile-automaton nr)) nr))
         (tc (mso2-correct-ori-decl-automaton :cwd cwd))
         (tc (vbits-cylindrification tc '(2)))
         (tc (if minimize (minimize-automaton (compile-automaton tc)) tc))
         (good (intersection-automaton nr tc))
         (good (if minimize (minimize-automaton (compile-automaton good)) good))
         (uc (mso2-union-of-cycles-ori-decl-automaton :cwd cwd))
         (uc (intersection-automaton good uc))
         (uc (if minimize (minimize-automaton (compile-automaton uc)) uc))
         (cw (mso2-connected-edges-decl-automaton :cwd cwd :orientation t))
         (cw (intersection-automaton good cw))
         (cw (if minimize (minimize-automaton (compile-automaton cw)) cw))
         (ihc (intersection-automaton uc cw))
         (ihc (if minimize (minimize-automaton (compile-automaton ihc)) ihc)))
    ihc))

(defun mso2-has-hamcycle-nonori-automaton (&key (cwd 0) (minimize nil))
  (let* ((ihc (mso2-is-hamcycle-nonori-automaton :cwd cwd :minimize minimize))
         (hhc (vbits-projection ihc '(1)))
         (hhc (if minimize (minimize-automaton (compile-automaton hhc)) hhc)))
    hhc))
                
(defun mso2-has-hamcycle-ori-automaton (&key (cwd 0) (minimize nil))
  (let* ((ihc (mso2-is-hamcycle-ori-automaton :cwd cwd :minimize minimize))
         (hhc (vbits-projection ihc '(1)))
         (hhc (if minimize (minimize-automaton (compile-automaton hhc)) hhc)))
    hhc))
                

#+nil
(compute-target
  (input-cwd-term "add_a_b(oplus(oplus(a^00,a^00),oplus(b^11,b^11)))")
  (mso2-is-hamcycle-nonori-automaton))

#+nil
(compute-target
  (input-cwd-term "add_a_b(oplus(oplus(a^0,a^0),oplus(b^1,b^1)))")
  (mso2-has-hamcycle-nonori-automaton))

#+nil
(recognized-p
  (decomp:cwd-decomposition-incidence
    (graph:graph-from-paths
      '((1 2 3 4 1 3))))
  (mso2-has-hamcycle-nonori-automaton))

#+nil
(recognized-p
  (decomp:cwd-decomposition-incidence
    (graph:graph-from-paths
      '((1 2 3 1))))
  (mso2-has-hamcycle-nonori-automaton))

#+nil
(recognized-p
  (decomp:cwd-decomposition-incidence
    (graph:graph-from-paths
      '((1 2 3))))
  (mso2-has-hamcycle-nonori-automaton))

#+nil
(recognized-p
  (input-cwd-term "add_b_c(oplus(c^0,ren_c_a(ren_d_b(add_c_d(oplus(d^1,ren_d_c(ren_c_a(add_c_d(oplus(d^0,ren_d_a(add_a_d(oplus(d^1,oplus(add_a_b(oplus(b^1,a^0)),add_a_b(oplus(b^1,add_a_c(oplus(c^1,a^0))))))))))))))))))")
  (mso2-has-hamcycle-nonori-automaton))

#+nil
(recognized-p
  (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(ren_d_b(add_c_d(oplus(d^11,ren_d_c(ren_c_a(add_c_d(oplus(d^00,ren_d_a(add_a_d(oplus(d^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^11,add_a_c(oplus(c^11,a^00))))))))))))))))))")
  (mso2-is-hamcycle-nonori-automaton))

#+nil
(graph-show (cwd-term-to-graph (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(ren_d_b(add_c_d(oplus(d^11,ren_d_c(ren_c_a(add_c_d(oplus(d^00,ren_d_a(add_a_d(oplus(d^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^11,add_a_c(oplus(c^11,a^00))))))))))))))))))")))

#+nil
(graph-show (cwd-term-to-graph (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(ren_d_b(add_c_d(oplus(d^11,ren_d_c(ren_c_a(add_c_d(oplus(d^00,ren_d_a(add_a_d(oplus(d^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^10,add_a_c(oplus(c^11,a^00))))))))))))))))))")))

#+nil
(recognized-p
  (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(ren_d_b(add_c_d(oplus(d^11,ren_d_c(ren_c_a(add_c_d(oplus(d^00,ren_d_a(add_a_d(oplus(d^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^10,add_a_c(oplus(c^11,a^00))))))))))))))))))")
  (mso2-is-hamcycle-nonori-automaton))

#+nil
(trace-cwd-automaton
  (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(ren_d_b(add_c_d(oplus(d^11,ren_d_c(ren_c_a(add_c_d(oplus(d^00,ren_d_a(add_a_d(oplus(d^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^10,add_a_c(oplus(c^11,a^00))))))))))))))))))")
  (mso2-union-of-cycles-nonori-decl-automaton))

#+nil
(trace-cwd-automaton
  (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(add_a_c(oplus(c^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^11,a^00))))))))")
  (mso2-union-of-cycles-nonori-decl-automaton))

#+nil
(trace-cwd-automaton
  (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(add_a_c(oplus(c^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^11,a^00))))))))")
  (mso2-connected-edges-decl-automaton))

#+nil
(trace-cwd-automaton
  (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(add_a_c(oplus(c^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^11,a^00))))))))")
  (mso2-is-hamcycle-nonori-automaton))

#+nil
(trace-cwd-automaton
  (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(add_a_c(oplus(c^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^11,a^00))))))))")
  (vbits-cylindrification (mso2-correct-nonori-decl-automaton) '(2)))

#+nil
(recognized-p
  (input-cwd-term "add_b_c(oplus(c^00,ren_c_a(ren_d_b(add_c_d(oplus(d^11,ren_d_c(ren_c_a(add_c_d(oplus(d^00,ren_d_a(add_a_d(oplus(d^11,oplus(add_a_b(oplus(b^11,a^00)),add_a_b(oplus(b^10,add_a_c(oplus(c^11,a^00))))))))))))))))))")
  (mso2-is-hamcycle-nonori-automaton))

#+nil
(recognized-p
  (input-cwd-term "add_b_c(oplus(c^0,ren_c_a(ren_d_b(add_c_d(oplus(d^1,ren_d_c(ren_c_a(add_c_d(oplus(d^0,ren_d_a(add_a_d(oplus(d^1,oplus(add_a_b(oplus(b^1,a^0)),add_a_b(oplus(b^1,add_a_c(oplus(c^1,a^0))))))))))))))))))")
  (mso2-has-hamcycle-nonori-automaton))

#+nil
(recognized-p
  (input-cwd-term "add_b_c(oplus(c^0,ren_c_a(ren_d_b(add_c_d(oplus(d^1,ren_d_c(ren_c_a(add_c_d(oplus(d^0,ren_d_a(add_a_d(oplus(d^1,oplus(add_a_b(oplus(b^1,a^0)),add_a_b(oplus(b^1,add_a_c(oplus(c^1,a^0))))))))))))))))))")
  (vbits-projection (mso2-is-hamcycle-nonori-automaton) '(1)))

#+nil
(recognized-p
  (decomp:cwd-decomposition-incidence
    (graph:graph-from-paths
      '((1 2 3))))
  (mso2-has-hamcycle-nonori-automaton))

#+nil
(trace-cwd-automaton
  (input-cwd-term
    "add_b_c(oplus(c^00,ren_c_a(add_a_c(oplus(c^10,oplus(a^00,add_a_b(oplus(b^10,a^00))))))))")
  (mso2-connected-edges-decl-automaton))

#+nil
(equality-automaton
  (mso2-has-hamcycle-nonori-automaton :cwd 2 :minimize t)
  (unoriented-hamiltonian-automaton 2))

#+nil
(recognized-p
  (input-cwd-term "b^0")
  (mso2-has-hamcycle-nonori-automaton))

(defun test-mso2-hamiltonian-decl ()
(assert
  (not 
    (recognized-p
      (decomp:cwd-decomposition-incidence
        (graph:graph-from-paths
          '((1 2))
          :oriented t))
      (mso2-has-hamcycle-ori-automaton))))

(assert
  (not 
    (recognized-p
      (decomp:cwd-decomposition-incidence
        (graph:graph-from-paths
          '((1 2 3))
          :oriented t))
      (mso2-has-hamcycle-ori-automaton))))

(assert
  (not 
    (recognized-p
      (decomp:cwd-decomposition-incidence
        (graph:graph-from-paths
          '((1 2 3) (1 3))
          :oriented t))
      (mso2-has-hamcycle-ori-automaton))))

(assert
  (recognized-p
      (decomp:cwd-decomposition-incidence
        (graph:graph-from-paths
          '((1 2 3 1))
          :oriented t))
      (mso2-has-hamcycle-ori-automaton)))

(assert
  (recognized-p
      (decomp:cwd-decomposition-incidence
        (graph:graph-from-paths
          '((1 2 3 4 1))
          :oriented t))
      (mso2-has-hamcycle-ori-automaton)))

(assert
  (recognized-p
      (decomp:cwd-decomposition-incidence
        (graph:graph-from-paths
          '((1 2 3 4 1 3))
          :oriented t))
      (mso2-has-hamcycle-ori-automaton)))
)
