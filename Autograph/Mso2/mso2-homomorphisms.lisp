;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; forward
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric nothing-to-y1-f (symbol csign)
  (:method ((s vbits-constant-symbol) (csign abstract-signature))
    (let* ((vbits (vbits s))
	   (b0 (aref vbits 0))
	   (sym (symbol-of s)))
      (assert (= 1 (length vbits)))
      (if (eq sym *empty-symbol*)
	  (mapcar (lambda (cs)
		    (make-vbits-symbol (symbol-of cs) (make-vbits '(1 0))))
		  (signature-symbols csign))
	  (if (zerop b0)
	      (mapcar (lambda (vb)
			(make-vbits-symbol sym (make-vbits vb)))
		      '((0 0) (0 1)))
	      (list (make-vbits-symbol sym (make-vbits '(1 1))))))))
  (:method ((s abstract-symbol) (csign signature))
    (list s)))

(defgeneric nothing-to-y1-h (csign)
  (:method ((csign abstract-signature))
    (lambda (s)
      (nothing-to-y1-f s csign))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; inverse
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric nothing-to-y1-if (symbol)
  (:method ((s vbits-constant-symbol))
    (let* ((vbits (vbits s))
	   (b0 (aref vbits 0))
	   (b1 (aref vbits 1))
	   (sym (symbol-of s)))
      (assert (= 2 (length vbits)))
      (if (and (= 1 b0) (= 1 b1))
	  (make-vbits-symbol sym (make-vbits '(1)))
	  (if (zerop b0)
	      (make-vbits-symbol sym (make-vbits '(0)))
	      (mapcar (lambda (vb)
			(make-vbits-symbol *empty-symbol* (make-vbits vb)))
		      '((0) (1)))))))
  (:method ((s abstract-symbol))
    (list s)))

(defgeneric nothing-to-y1-ih ()
  (:method ()
    (lambda (s)
      (nothing-to-y1-if s))))

(defgeneric nothing-to-y1 (signed-object)
  (:method ((signed-object signed-object))
    (homomorphism
     signed-object
;;     (add-empty-symbols signed-object)
     (nothing-to-y1-h (constant-signature signed-object))
     (nothing-to-y1-ih)
     (cons :nb-set-variables 2))))
