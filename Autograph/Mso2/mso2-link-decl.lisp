(in-package :autograph)

(define-declarative-automaton
    mso2-edge-ori-decl
    (;; track the labels of the endpoints
     start-label end-label
     ;; all the edges from/to endpoints
     start-edges end-edges pending-edges
     ;; and the edge between them, separately
     success-label) ;; WHY NOT USE OKK STATE
  ;; Edges X Y 
  :vbits 3
  :orientation t
  :final ((s) (success-label s))
  :vertex ((v vbits)
           (destructuring-bind
	       (e x y) (map 'list #'onep vbits)
             (cond ((and x y) nil)
                   ((and e (or x y)) nil)
                   (e (make :start-label nil
                            :end-label nil
                            :start-edges (set-of-args)
                            :end-edges (set-of-args)
                            :pending-edges (set-of-args v)
                            :success-label nil))
                   (x (make :start-label v
                            :end-label nil
                            :start-edges (set-of-args)
                            :end-edges (set-of-args)
                            :pending-edges (set-of-args)
                            :success-label nil))
                   (y (make :start-label nil
                            :end-label v
                            :start-edges (set-of-args)
                            :end-edges (set-of-args)
                            :pending-edges (set-of-args)
                            :success-label nil))
                   (t (make :start-label nil
                            :end-label nil
                            :start-edges (set-of-args)
                            :end-edges (set-of-args)
                            :pending-edges (set-of-args)
                            :success-label nil)))))
  :ren ((a b s)
        (let* ((start-edges (unless (success-label s)
                              (rename-in-set a b (start-edges s))))
               (end-edges (unless (success-label s)
                            (rename-in-set a b (end-edges s))))
               (pending-edges (unless (success-label s)
                                (container-difference
				 (rename-in-set a b (pending-edges s))
				 (container-union start-edges end-edges)))))
          (make
	   :start-label (object-rename a b (start-label s))
	   :end-label (object-rename a b (end-label s))
	   :start-edges start-edges
	   :end-edges end-edges
	   :pending-edges pending-edges
	   :success-label (success-label s))))
  :oplus ((r s)
          (let ((r-started (or (start-label r) (success-label r)))
                (r-ended (or (end-label r) (success-label r)))
                (s-started (or (start-label s) (success-label s)))
                (s-ended (or (end-label s) (success-label s))))
            (unless
		(or (and s-started r-started)
		    (and s-ended r-ended))
              (let* ((success (or (success-label r) (success-label s)))
                     (start-edges (unless success
                                    (container-union (start-edges r)
                                                     (start-edges s))))
                     (end-edges (unless success
                                  (container-union (end-edges r)
                                                   (end-edges s))))
                     (pending-edges
                       (unless success
                         (container-difference
			  (container-union (pending-edges s)
					   (pending-edges r))
			  (container-union start-edges end-edges)))))
                (make
		 :start-label (or (start-label r) (start-label s))
		 :end-label (or (end-label r) (end-label s))
		 :start-edges start-edges
		 :end-edges end-edges
		 :pending-edges pending-edges
		 :success-label success)))))
  :add ((a b s)
        (let ((success (or (success-label s)
                           (and (eq a (start-label s))
                                (container-member b (end-edges s))
                                t)
                           (and (eq b (end-label s))
                                (container-member a (start-edges s))
                                t))))
          (if
	   (or (and (eq a (start-label s))
		    (container-member b (pending-edges s)))
	       (and (eq b (end-label s))
		    (container-member a (pending-edges s)))
	       success)
	   (make :start-label (unless success (start-label s))
		 :end-label (unless success (end-label s))
		 :start-edges (cond (success nil)
				    ((eq a (start-label s))
				     (container-union (set-of-args b)
						      (start-edges s)))
				    (t (start-edges s)))
		 :end-edges (cond (success nil)
				  ((eq b (end-label s))
				   (container-union (set-of-args a)
						    (end-edges s)))
				  (t (end-edges s)))
		 :pending-edges (cond (success nil)
				      (t (container-difference
					  (pending-edges s)
					  (set-of-args a b))))
		 :success-label success)
	   s))))

(compile-automaton (mso2-edge-ori-decl-automaton :cwd 2))

(defun test-mso2-arc ()
  (equality-automaton
   (identity
    (intersection-automata
     (list
      (vbits-cylindrification
       (non-redundant-decl-automaton :cwd 3 :orientation t) '(1 2 3))
      (vbits-cylindrification
       (mso2-correct-ori-decl-automaton :cwd 3) '(2 3))
      (basic-arc-mso-automaton :cwd 3))))
   (identity
    (intersection-automata
     (list
      (vbits-cylindrification (non-redundant-decl-automaton :cwd 3) '(1 2 3))
      (vbits-cylindrification (mso2-correct-ori-decl-automaton :cwd 3) '(2 3))
      (mso2-edge-ori-decl-automaton :cwd 3)))))

  (compute-target
   (input-cwd-term "add_a->b(add_b->c(oplus(oplus(a^010,b^100),c^001)))")
   (intersection-automata
    (list
     (vbits-cylindrification
      (non-redundant-decl-automaton :cwd 3) '(1 2 3))
     (vbits-cylindrification
      (mso2-correct-ori-decl-automaton :cwd 3) '(2 3))
     (basic-arc-mso-automaton :cwd 3))))

  (compute-target
   (input-cwd-term "add_a->b(add_b->c(oplus(oplus(a^010,b^100),c^001)))")
   (basic-arc-mso-automaton :cwd 3))

  (compute-target
   (input-cwd-term "add_a->b(add_b->c(oplus(oplus(a^010,b^100),c^001)))")
   (mso2-edge-ori-decl-automaton :cwd 3))

  (signature (input-cwd-term "add_a->b(add_b->c(oplus(oplus(a^010,b^100),c^001)))")))

(define-declarative-automaton mso2-edge-nonori-decl
    (start-label end-label success-label
		 start-edges end-edges pending-edges)
  :vbits 3 :orientation nil :final ((s) (success-label s))
  :vertex ((v vbits)
           (destructuring-bind
	       (e x y) (map 'list #'onep vbits)
             (cond ((and x y) nil)
                   ((and e (or x y)) nil)
                   (e (make :start-label nil
                            :end-label nil
                            :start-edges (set-of-args)
                            :end-edges (set-of-args)
                            :pending-edges (set-of-args v)
                            :success-label nil))
                   (x (make :start-label v
                            :end-label nil
                            :start-edges (set-of-args)
                            :end-edges (set-of-args)
                            :pending-edges (set-of-args)
                            :success-label nil))
                   (y (make :start-label nil
                            :end-label v
                            :start-edges (set-of-args)
                            :end-edges (set-of-args)
                            :pending-edges (set-of-args)
                            :success-label nil))
                   (t (make :start-label nil
                            :end-label nil
                            :start-edges (set-of-args)
                            :end-edges (set-of-args)
                            :pending-edges (set-of-args)
                            :success-label nil)))))
  :ren ((a b s)
        (let* ((start-edges (unless (success-label s)
                              (rename-in-set a b (start-edges s))))
               (end-edges (unless (success-label s)
                            (rename-in-set a b (end-edges s))))
               (pending-edges (unless (success-label s)
                                (container-difference
				 (rename-in-set a b (pending-edges s))
				 (container-union start-edges end-edges)))))
          (make
	   :start-label (object-rename a b (start-label s))
	   :end-label (object-rename a b (end-label s))
	   :start-edges start-edges
	   :end-edges end-edges
	   :pending-edges pending-edges
	   :success-label (success-label s))))
  :oplus ((r s)
          (let ((r-started (or (start-label r) (success-label r)))
                (r-ended (or (end-label r) (success-label r)))
                (s-started (or (start-label s) (success-label s)))
                (s-ended (or (end-label s) (success-label s))))
            (unless
		(or (and s-started r-started)
		    (and s-ended r-ended))
              (let* ((success (or (success-label r) (success-label s)))
                     (start-edges (unless success
                                    (container-union (start-edges r)
                                                     (start-edges s))))
                     (end-edges (unless success
                                  (container-union (end-edges r)
                                                   (end-edges s))))
                     (pending-edges
                       (unless success
                         (container-difference
			  (container-union (pending-edges s)
					   (pending-edges r))
			  (container-union start-edges end-edges)))))
                (make
		 :start-label (or (start-label r) (start-label s))
		 :end-label (or (end-label r) (end-label s))
		 :start-edges start-edges
		 :end-edges end-edges
		 :pending-edges pending-edges
		 :success-label success)))))
  :add ((a b s)
        (let* ((ab (set-of-args a b))
               (se (set-of-args (start-label s) (end-label s)))
               (success (or (success-label s)
                            (and (container-member (start-label s) ab)
                                 (container-intersection-not-empty-p
				  ab (end-edges s))
                                 t)
                            (and (container-member (end-label s) ab)
                                 (container-intersection-not-empty-p
				  ab (start-edges s))
                                 t)
                            (and (eq (start-label s) (end-label s))
                                 (container-member (start-label s) ab)
                                 (container-member
				  (container-car
				   (container-difference
				    ab (set-of-args (start-label s))))
				  (pending-edges s)))))
               (assigned (and (container-intersection-not-empty-p
			       se ab)
                              (container-intersection-not-empty-p
			       ab (pending-edges s)))))
          (if
	   (or assigned
	       success)
	   (make :start-label (unless success (start-label s))
		 :end-label (unless success (end-label s))
		 :start-edges (cond (success nil)
				    ((container-member (start-label s) ab)
				     (container-union
				      (container-intersection
				       ab (pending-edges s))
				      (start-edges s)))
				    (t (start-edges s)))
		 :end-edges (cond (success nil)
				  ((container-member (end-label s) ab)
				   (container-union
				    (container-intersection
				     ab (pending-edges s))
				    (end-edges s)))
				  (t (end-edges s)))
		 :pending-edges (cond (success nil)
				      (t (container-difference
					  (pending-edges s)
					  (set-of-args a b))))
		 :success-label success)
	   s))))

(defun test-mso2-edge ()
  (equality-automaton
   (identity
    (intersection-automata
     (list
      (vbits-cylindrification
       (non-redundant-decl-automaton :orientation nil :cwd 3) '(1 2 3))
      (vbits-cylindrification
       (mso2-correct-nonori-decl-automaton :cwd 3) '(2 3))
      (basic-edge-mso-automaton :cwd 2))))
   (identity
    (intersection-automata
     (list
      (vbits-cylindrification (non-redundant-decl-automaton
			       :orientation nil :cwd 3) '(1 2 3))
      (vbits-cylindrification
       (mso2-correct-nonori-decl-automaton :cwd 3) '(2 3))
      (mso2-edge-nonori-decl-automaton :cwd 2)))))

  (recognized-p
   (input-cwd-term "add_a_b(oplus(oplus(a^010,b^100),a^000))")
   (intersection-automata
    (list
     (vbits-cylindrification (non-redundant-decl-automaton
			      :orientation nil :cwd 3) '(1 2 3))
     (vbits-cylindrification
      (mso2-correct-nonori-decl-automaton :cwd 3) '(2 3))
     (mso2-edge-nonori-decl-automaton :cwd 2))))

  (recognized-p
   (input-cwd-term "add_a_b(oplus(oplus(a^010,b^100),a^000))")
   (intersection-automata
    (list
     (vbits-cylindrification (non-redundant-decl-automaton
			      :orientation nil :cwd 3) '(1 2 3))
     (vbits-cylindrification
      (mso2-correct-nonori-decl-automaton :cwd 3) '(2 3))
     (basic-edge-mso-automaton :cwd 2)))))

(define-declarative-automaton
    mso2-link-ori-decl
    (x-labels y-labels edges x-edges y-edges success-label)
  :vbits 3
  :orientation t
  :final ((s) (success-label s))
  :valid ((s) s t)
  :vertex ((v vbits)
           (destructuring-bind
	       (e x y) (map 'list 'onep vbits)
             (make :x-edges (set-of-args)
                   :y-edges (set-of-args)
                   :x-labels (set-of (when x (list v)))
                   :y-labels (set-of (when y (list v)))
                   :edges (set-of (when e (list v)))
                   :success-label nil)))
  :ren ((a b s)
        (make :x-edges (rename-in-set a b (x-edges s))
              :y-edges (rename-in-set a b (y-edges s))
              :x-labels (rename-in-set a b (x-labels s))
              :y-labels (rename-in-set a b (y-labels s))
              :edges (rename-in-set a b (edges s))
              :success-label (success-label s)))
  :oplus ((s r)
          (make
	   :x-edges (container-union (x-edges r) (x-edges s))
	   :y-edges (container-union (y-edges r) (y-edges s))
	   :edges (container-union (edges r) (edges s))
	   :x-labels (container-union (x-labels r) (x-labels s))
	   :y-labels (container-union (y-labels r) (y-labels s))
	   :success-label (or (success-label s) (success-label r))))
  :add ((a b s)
        (let* ((from-x (container-member a (x-labels s)))
               (to-y (container-member b (y-labels s)))
               (x-edge (container-member a (x-edges s)))
               (y-edge (container-member b (y-edges s)))
               (new-success (or (and from-x y-edge)
                                (and to-y x-edge))))
          (make
	   :x-labels (x-labels s)
	   :y-labels (y-labels s)
	   :edges (edges s)
	   :success-label (or (success-label s) new-success)
	   :x-edges (if from-x (container-adjoin b (x-edges s))
			(x-edges s))
	   :y-edges (if to-y (container-adjoin a (y-edges s))
			(y-edges s))))))

(define-declarative-automaton mso2-link-nonori-decl
    (x-labels y-labels edges x-edges y-edges success-label)
  :vbits 3
  :orientation nil
  :final ((s) (success-label s))
  :valid ((s) s t)
  :vertex ((v vbits)
           (destructuring-bind
	       (e x y) (map 'list 'onep vbits)
             (make :x-edges (set-of-args)
                   :y-edges (set-of-args)
                   :x-labels (set-of (when x (list v)))
                   :y-labels (set-of (when y (list v)))
                   :edges (set-of (when e (list v)))
                   :success-label nil)))
  :ren ((a b s)
        (make :x-edges (rename-in-set a b (x-edges s))
              :y-edges (rename-in-set a b (y-edges s))
              :x-labels (rename-in-set a b (x-labels s))
              :y-labels (rename-in-set a b (y-labels s))
              :edges (rename-in-set a b (edges s))
              :success-label (success-label s)))
  :oplus ((s r)
          (make
	   :x-edges (container-union (x-edges r) (x-edges s))
	   :y-edges (container-union (y-edges r) (y-edges s))
	   :edges (container-union (edges r) (edges s))
	   :x-labels (container-union (x-labels r) (x-labels s))
	   :y-labels (container-union (y-labels r) (y-labels s))
	   :success-label (or (success-label s) (success-label r))))
  :add ((a b s)
        (let* ((ab (set-of-args a b))
               (edge-label (container-intersection ab (edges s)))
               (vertex-label (container-car
			      (container-difference ab (edges s))))
               (x (container-member vertex-label (x-labels s)))
               (y (container-member vertex-label (y-labels s)))
               (xe (container-member vertex-label (x-edges s)))
               (ye (container-member vertex-label (y-edges s)))
               (new-success (or (and x ye) (and y xe))))
          (make
	   :x-labels (x-labels s)
	   :y-labels (y-labels s)
	   :edges (edges s)
	   :success-label (or (success-label s) new-success)
	   :x-edges (if x (container-adjoin edge-label (x-edges s))
			(x-edges s))
	   :y-edges (if y (container-adjoin edge-label (y-edges s))
			(y-edges s))))))

#+nil
(defun test-mso2-link ()
  (equality-automaton
   (identity
    (intersection-automata
     (list
      (vbits-cylindrification
       (non-redundant-decl-automaton :orientation nil :cwd 3) '(1 2 3))
      (vbits-cylindrification
       (mso2-correct-nonori-decl-automaton :cwd 3) '(2 3))
      (basic-link-mso2-automaton :cwd 2))))
   (identity
    (intersection-automata
     (list
      (vbits-cylindrification (non-redundant-decl-automaton
			       :orientation nil :cwd 3) '(1 2 3))
      (vbits-cylindrification
       (mso2-correct-nonori-decl-automaton :cwd 3) '(2 3))
      (mso2-link-nonori-decl-automaton :cwd 2)))))
  (equality-automaton
   (identity
    (intersection-automata
     (list
      (vbits-cylindrification
       (non-redundant-decl-automaton :orientation t :cwd 3) '(1 2 3))
      (vbits-cylindrification
       (mso2-correct-ori-decl-automaton :cwd 3) '(2 3))
      (basic-link-mso2-automaton :cwd 2 :oriented t))))
   (identity
    (intersection-automata
     (list
      (vbits-cylindrification (non-redundant-decl-automaton
			       :orientation t :cwd 3) '(1 2 3))
      (vbits-cylindrification
       (mso2-correct-ori-decl-automaton :cwd 3) '(2 3))
      (mso2-link-ori-decl-automaton :cwd 2))))))

  
