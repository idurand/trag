;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;;; oriented case only

(defun edges-oriented-hamiltonian-automaton (&optional (cwd 0))
  (rename-object
   (nothing-to-y1 (vbits-cylindrification (circuit-automaton :cwd cwd) '(1)))
   (cwd-automaton-name "ORIENTED-HAMILTONIAN(Y1)" cwd)))

(defun oriented-hamiltonian-automaton (&optional (cwd 0))
  (rename-object
   (vbits-projection
    (edges-oriented-hamiltonian-automaton cwd) '(1))
   (cwd-automaton-name "ORIENTED-HAMILTONIAN" cwd)))

(defun oriented-hamiltonian-transducer (&optional (cwd 0))
  (vbits-projection
   (attribute-automaton (edges-oriented-hamiltonian-automaton cwd) *assignment-afun*)
   '(1)))
