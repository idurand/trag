;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun edge-bit-p (vbits)
  (= 1 (aref vbits 0)))

(defgeneric oriented-mso-term-transitions-fun (root arg))

(defgeneric cports (oriented-mso-term-state)
  (:documentation "cports of oriented-mso-term-state"))

(defgeneric make-cports (ports)
  (:method ((ports list))
    (make-mports 2 ports)))
  
(defgeneric dports (oriented-mso-term-state)
  (:documentation "dports of oriented-mso-term-state"))

;; cports1 ports that label a single c-vertex
;; cports2 ports that label at least 2 c-vertices

(defclass oriented-mso-term-state (graph-state)
  ((cports :type mports :initarg :cports :reader cports)
   (dports :type dports :initarg :dports :reader dports)))

(defgeneric make-oriented-mso-term-state (cports dports)
  (:method ((cports mports) (dports dports))
    (make-instance 'oriented-mso-term-state :cports cports :dports dports)))

(defmethod print-object ((s oriented-mso-term-state) stream)
  (format stream "~A-~A" (cports s) (dports s)))

(defmethod state-final-p ((state oriented-mso-term-state))
  (let ((dports (dports state)))
    (and
     (ports-empty-p (dports0 dports))
     (ports-empty-p (dports1 dports))
     (ports-empty-p (dports2 dports)))))

(defmethod graph-oplus-target ((s1 oriented-mso-term-state) (s2 oriented-mso-term-state))
  (let ((union-dports (dports-union (dports s1) (dports s2))))
    (when (disjoint-dports union-dports)
      (make-oriented-mso-term-state
       (container-union (cports s1) (cports s2))
       union-dports))))

(defgeneric oriented-mso-term-add-fail (a b nba nbb dports0 dports1 dports2 dports3 all-dports)
  (:documentation "detection of incorrect oriented-mso-term")
  (:method ((a integer) (b integer) (nba integer) (nbb integer) 
	    (dports0 ports) (dports1 ports) (dports2 ports) (dports3 ports) (all-dports ports))
    (or (and (plusp nba) (plusp nbb))
	(= 2 nba)
	(= 2 nbb)
	(and (ports-member a all-dports) (ports-member b all-dports))
	(and (= 1 nba) ;; already incoming arc on b
	     (or (ports-member b dports2) (ports-member b dports3)))
	(and (= 1 nbb) ;; already incoming arc on a
	     (or (ports-member a dports1) (ports-member a dports3))))))

(defmethod graph-add-target (a b (state oriented-mso-term-state))
  (let* ((cports (cports state))
	 (nba (port-count a cports))
	 (nbb (port-count b cports))
	 (all-cports (ports-of cports))
	 (dports (dports state))
	 (all-dports (ports-of dports))
	 (dports0 (dports0 (dports state)))
	 (dports1 (dports1 (dports state)))
	 (dports2 (dports2 (dports state)))
	 (dports3 (dports3 (dports state)))
	 (ports (ports-union all-cports all-dports)))
      (unless (and (ports-member a ports) (ports-member b ports))
	(return-from graph-add-target state))
      (when (oriented-mso-term-add-fail a b nba nbb dports0 dports1 dports2 dports3 all-dports)
	(return-from graph-add-target))
      (cond ((= 2 nba) ;; not oriented
	     (setq dports0 (ports-remove b dports0))
	     (setq dports3 (ports-adjoin b dports3)))
	    ((= 2 nbb) ;; not oriented
	     (setq dports0 (ports-remove a dports0))
	     (setq dports3 (ports-adjoin a dports3)))
	    ((= 1 nba)
	     (cond ((ports-member b dports0)
;;		    (format t "a=~A in cports1 b=~A in dports0~%" a b)
		    (setq dports0 (ports-remove b dports0))
		    (setq dports2 (ports-adjoin b dports2)))
		   ((ports-member b dports1)
;;		    (format t "a=~A in cports1 b=~A in dports1~%" a b)
		    (setq dports1 (ports-remove b dports1))
		    (setq dports3 (ports-adjoin b dports3)))
		   ((ports-member b dports2)
;;		    (format t "a=~A in cports1 b=~A in dports2~%" a b)
		    (setq dports2 (ports-remove b dports2))
		    (setq dports3 (ports-adjoin b dports3)))))
	    ((= 1 nbb)
	     (cond ((ports-member a dports0)
;;		    (format t "b=~A in cports1 a=~A in dports0~%" b a)
		    (setq dports0 (ports-remove a dports0))
		    (setq dports1 (ports-adjoin a dports1)))
		   ((ports-member a dports1)
;;		    (format t "b=~A in cports1 a=~A in dports1~%" b a)
		    (setq dports1 (ports-remove a dports1))
		    (setq dports2 (ports-adjoin a dports2)))
		   ((ports-member a dports2)
;;		    (format t "b=~A in cports1 a=~A in dports2~%" b a)
		    (setq dports2 (ports-remove a dports2))
		    (setq dports3 (ports-adjoin a dports3))))))
    (make-oriented-mso-term-state
     cports
     (make-dports dports0 dports1 dports2 dports3))))

(defmethod graph-ren-target (a b (state oriented-mso-term-state))
  (make-oriented-mso-term-state
   (container-subst b a (cports state))
   (dports-subst b a (dports state))))

(defmethod oriented-mso-term-transitions-fun ((root vbits-constant-symbol) (arg (eql nil)))
  (let ((port (port-of root)))
    (if (edge-bit-p (vbits root))
	(make-oriented-mso-term-state
	 (make-cports '())
	 (make-dports
	  (make-ports-from-port port)
	  (make-empty-ports)
	  (make-empty-ports)
	  (make-empty-ports)))
	(make-oriented-mso-term-state
	 (make-cports (list port))
	 (make-dports
	  (make-empty-ports)
	  (make-empty-ports)
	  (make-empty-ports)
	  (make-empty-ports))))))

(defmethod oriented-mso-term-transitions-fun ((root abstract-symbol) (arg list))
  (cwd-transitions-fun root arg #'oriented-mso-term-transitions-fun))

(defun oriented-mso-term-automaton (&key (cwd 0))
  "recognize correct incidence terms"
  (make-cwd-automaton
   (cwd-vbits-signature cwd 1 :orientation t)
   (lambda (root states)
     (oriented-mso-term-transitions-fun root states))
   :name (cwd-automaton-name "ORIENTED-MSO-TERM" cwd)
;;   :precondition-automaton (vbits-cylindrification
;;			    (non-redundant-automaton t :cwd cwd)
;;			    '(1))
   ))

(defun test-oriented-mso-term ()
  (let ((of (oriented-mso-term-automaton))
	(circuit (cwd-decomposition-incidence (graph-cycle 5 t)))
	(oterm (input-cwd-term "add_a->b(oplus(oplus(a^0,b^1),a^0))")))
    (assert (recognized-p circuit of))
    (assert (mso-term-p circuit))
    (assert (not (recognized-p oterm of)))
    ))
