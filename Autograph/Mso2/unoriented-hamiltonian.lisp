;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun edges-unoriented-hamiltonian-automaton (&optional (cwd 0))
  (rename-object
   (intersection-automaton
    (nothing-to-y1 (vbits-cylindrification (connectedness-automaton :cwd cwd :orientation nil) '(1)))
    (nothing-to-y1 (vbits-cylindrification (union-cycle-automaton cwd) '(1))))
   (cwd-automaton-name "UNORIENTED-HAMILTONIAN-Y1" cwd)))

(defun unoriented-hamiltonian-automaton (&optional (cwd 0))
  (rename-object
   (vbits-projection
    (edges-unoriented-hamiltonian-automaton cwd) '(1))
   (cwd-automaton-name "UNORIENTED-HAMILTONIAN" cwd)))

(defun unoriented-hamiltonian-transducer (&optional (cwd 0))
  (vbits-projection
   (attribute-automaton
    (edges-unoriented-hamiltonian-automaton cwd) *assignment-afun*)
   '(1)))
