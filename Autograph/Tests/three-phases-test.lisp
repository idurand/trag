;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun 3-phase-coloring (term k afuns  &key enum)
  (termauto::three-phases-compute-final-target
   term
   (fixed-coloring-automaton k)
   afuns
   (lambda (a) (color-projection a k))
   :enum enum))

(defun 1-phase-coloring (term k afuns &key enum)
  (compute-final-target term (fixed-coloring-transducer k afuns) :enum enum))


