;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric intersection-with-ports (pairs ports)
  (:documentation
   "removes from pairs the edges with no intersection with PORTS")
  (:method ((pairs pairs) (port-state port-state))
    (let ((ports (ports port-state)))
      (pairs-remove-if-not
       (lambda (pair)
	 (and (ports-member (first pair) ports)
	      (ports-member (second pair) ports)))
       pairs))))

(defgeneric add-annotation (term father)
  (:documentation "add add-annotation using previously existing ports annotation")
  (:method ((term term) (father null)) (make-empty-pairs))
  (:method ((term term) (father annotated-symbol))
    (let* ((annotation (annotation father)))
      (intersection-with-ports 
       (if (oplus-symbol-p father)
	   annotation
	   (let* ((root (symbol-of father))
		  (ports (symbol-ports root))
		  (a (first ports))
		  (b (second ports)))
	     (cond
	       ((ren-symbol-p root)
		(pairs-subst b a annotation))
	       ((reh-symbol-p root)
		(loop for pair in (clique-width::sequentialize-mapping 
				   (clique-width::mapping-pairs (port-mapping-of root))
				   (graph-ports-of term))
		      do (setq annotation (pairs-subst (cdr pair) (car pair) annotation))
		      finally (return annotation)))
	       ((add-symbol-p root)
		(pairs-adjoin (list a b) annotation))
	       (t (error 'unknown-father-symbol)))))
       (run term)))))

(defgeneric ncompute-add-annotation (aterm)
  (:documentation "destructively sets the add annotation in the non constant symbols of ATERM")
  (:method ((aterm term))
    (transitions-compute-target 
     aterm
     (let ((*cast* nil)
	   (nb-colors (max-color-of-term aterm)))
       (transitions-of (if (zerop nb-colors) (port-automaton) (color-port-automaton nb-colors))))
     :signal nil :save-run t)
    (delete-run
     (ncompute-annotation aterm #'add-annotation))
    aterm))

(defgeneric compute-add-annotation (term)
  (:documentation "equivalent term with add annotation")
  ;; when should compute the nb-colors instead of parameter
  (:method ((term term))
    (let ((aterm (terms::term-to-annotated-term term :constants nil)))
      (ncompute-add-annotation aterm))))

(defgeneric ncompute-vertex-annotation (aterm)
  (:documentation
   "copies the vertex number which is in the eti slot of the leaves, \
    to the symbol annotation")
  (:method ((aterm term))
    (let ((root (root aterm))
	  (arg (arg aterm)))
      (if (endp arg)
	  (setf (annotation root) (term-eti aterm))
	  (mapcar #'ncompute-vertex-annotation arg))
      aterm)))

(defgeneric compute-vertex-annotation (term)
  (:documentation
   "return equivalent term with vertex number (eti slot) as annotation of the symbol \
    if leaves are not numbered, they are numbered first")
  (:method ((term term))
    (let ((aterm (term-to-annotated-term (term-num-leaves term) :non-constants nil)))
      (ncompute-vertex-annotation aterm))))

(defgeneric compute-coloring-annotations (cwd-term)
  (:documentation "cwd-term with color annotation (which vertices have which color")
  (:method ((term term))
    (let ((aterm (term-to-annotated-term (term-num-leaves term))))
      (ncompute-add-annotation aterm)
      (ncompute-vertex-annotation aterm))))

(defun apetersen ()
  "annotated version of the cwd-term for Petersen"
  (compute-add-annotation (petersen)))

(defun amcgee ()
  "annotated version of the cwd-term for McGee"
  (compute-add-annotation (light-cwd-term (mcgee))))

(defun agrunbaum ()
  "annotated version of the cwd-term for Grunbaum"
  (compute-add-annotation (grunbaum)))

(defun agrunbaum-bis ()
  (compute-add-annotation (grunbaum-bis)))
