;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(require :autograph)
(in-package :autograph)
(defparameter *c8* (kcolorability-automaton 8 :cwd 3))
(with-time (mouline *c8* 37 37
		     (lambda (i)
		       (cwd-term-grid 6 i))
		     "/Users/idurand/rcoloring.dat")

(defparameter *cc8* (let (cast-fly-automaton (kcolorability-automaton 8 :cwd 3))))
(with-time (mouline *cc8* 2 10  (lambda (i)
				      (cwd-term-grid 6 i))
		     "/Users/idurand/rcoloring-uncasted.dat" ))
