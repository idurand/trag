;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defvar *bio-array*)

(defparameter *exists-siphon-automaton* (exists-siphon-automaton))
(defparameter *exists-siphon-card2-automaton* (exists-siphon-card-automaton 2))

(setf *char-ports* t)
(defparameter *difficult* '(7 13 14))

(defgeneric bio-term (bg)
  (:documentation
   "term of the biograph-object representing the bio-graph"))

(defclass bio-graph-object ()
  ((bio-number :reader bio-number :initarg :bio-number)
   (bio-graph :reader bio-graph :initarg :bio-graph)
   (bio-term :accessor bio-term :initform nil)
   (bio-res :accessor bio-res :initform nil)))

(defmethod bio-res ((o (eql nil))) nil)

(defmethod print-object ((bgo bio-graph-object) stream)
  (format stream "~A nb-nodes:~A, nb-arcs:~A, res:~A"
	  (bio-number bgo)
	  (nodes-size (graph-nodes (bio-graph bgo)))
	  (container-size (graph-arcs (bio-graph bgo)))
	  (bio-res bgo)))

(defun make-bio-graph (graph bio-number)
  (make-instance 'bio-graph-object :bio-graph graph :bio-number bio-number))

(defparameter *bio-directory* "~/TRAG-PROJECT/FAGES/")

(defun bio-name (n)
  (format nil "BIOMD~10,'0D" n))

(defun bio-graph-name (n)
  (format nil "DIMACS/~A.col" (bio-name n)))

(defun bio-term-name (n)
  (format nil "CWD-TERMS/~A.cwd" (bio-name n)))

(defun absolute-name (name)
  (concatenate 'string *bio-directory* name))

(defun nth-bio-graph-filename (n)
  (absolute-name (bio-graph-name n)))

(defun nth-bio-term-filename (n)
  (absolute-name (bio-term-name n)))

(defun load-nth-bio-graph (n)
  (load-dimacs-graph (nth-bio-graph-filename n)))

(defun load-nth-bio-term (n)
  (let ((*char-ports* nil))
    (with-open-file (stream (nth-bio-term-filename n)
			    :if-does-not-exist nil)
      (when stream 
	(parse-term stream)))))

(defun save-bio-term (term n)
  (when term
    (let ((*char-ports* nil))
      (with-open-file (stream (nth-bio-term-filename n)
			      :direction :output
			      :if-does-not-exist :create
			      :if-exists :supersede)
	(when stream 
	  (format stream "~A" term))))))

(defun build-bio-graphs-array (n)
 (loop
    with bg = (make-array (1+ n) :initial-element nil)
    for k from 1 to n
    do (let ((g (load-nth-bio-graph k)))
	 (when g
	   (setf (aref bg k) (make-bio-graph g k))))
    finally (return bg)))

(defgeneric compute-bio-term (bio-graph-object)
  (:method ((bg bio-graph-object))
  (setf (bio-term bg)
	(cwd-decomposition-bipartite (bio-graph bg) :one-transition t))))
;; in all examples of the benchmark node 1 is a transition"

(defmethod bio-term :around ((bg bio-graph-object))
  (let ((term (call-next-method)))
    (unless term
      (setf term (compute-bio-term bg)))
    term))

(defun nth-bio-term (n)
  (bio-term (aref *bio-array* n)))

(defun save-bio-terms (array)
  (loop
     for k from 1 below (length array)
     do (let ((bg (aref array k)))
	  (when (and bg (bio-term bg) (not (container-empty-p (graph-edges (bio-graph bg)))))
	    (save-bio-term (bio-term bg) k)))))

(defgeneric set-term-property (tag term-fun bio-graph-object)
  (:method (tag term-fun (bgo bio-graph-object))
    (unless (assoc tag (bio-res bgo))
      (push (cons tag (funcall term-fun (bio-term bgo))) (bio-res bgo)))))

(defgeneric set-graph-property (tag graph-fun bio-graph-object)
  (:method (tag graph-fun (bgo bio-graph-object))
    (unless (assoc tag (bio-res bgo))
      (push (cons tag (funcall graph-fun (bio-graph bgo))) (bio-res bgo)))))

(defgeneric set-property (tag bgo-fun bio-graph-object)
  (:method (tag bgo-fun (bgo bio-graph-object))
    (unless (assoc tag (bio-res bgo))
      (push (cons tag (funcall bgo-fun bgo)) (bio-res bgo)))))

(defgeneric unset-property (tag bio-graph-object)
  (:method (tag (bgo bio-graph-object))
    (when (assoc tag (bio-res bgo))
      (setf (bio-res bgo) nil))))

(defun compute-term-property-k (tag term-fun array k)
  (let ((bg (aref array k)))
    (when (and bg (not (container-empty-p (graph-edges (bio-graph bg)))))
      (print k)
      (set-term-property tag term-fun bg))))

(defun compute-term-property (tag term-fun array &key (avoid '()) (start 1) (end 399))
  (loop
     for k from start to end
     unless (member k avoid)
     do (compute-term-property-k tag term-fun array k)))

(defun compute-graph-property-k (tag graph-fun array k)
  (let ((bg (aref array k)))
    (when (and bg (not (container-empty-p (graph-edges (bio-graph bg)))))
      (print k)
      (set-graph-property tag graph-fun bg))))

(defun compute-graph-property (tag graph-fun array &key (avoid '()) (start 1) (end 399))
  (loop
     for k from start to end
     unless (member k avoid)
     do (compute-graph-property-k tag graph-fun array k)))

(defun compute-property-k (tag fun array k)
  (let ((bg (aref array k)))
    (when (and bg (not (container-empty-p (graph-edges (bio-graph bg)))))
      (set-property tag fun bg))))

(defun compute-property (tag fun array &key (avoid '()) (start 1) (end 399))
  (loop
     for k from start to end
     unless (member k avoid :test #'=)
     do (compute-property-k tag fun array k)))

(defun reset-property (tag array)
  (loop
    for k from 1 below (length array)
    do (let ((bg (aref array k)))
	 (when bg
	   (unset-property tag bg)))))

(defun reset-bio-res (array)
  (loop
    for k from 1 below (length array)
    do (let ((bg (aref array k)))
	 (when bg
	    (setf (bio-res bg) nil)))))

(defun reset-bio-term (bg)
  (setf (bio-term bg) nil))

(defun reset-bio-terms (array)
  (loop
     for bg across array
     when bg
     do (reset-bio-term bg)))
 
(defun compute-cwd (array)
  (compute-term-property 'cwd #'clique-width array))

(defun compute-siphon-sizes (array)
  (compute-term-property
   'siphon-sizes
   #'siphon-sizes-verify
   array
   :avoid *difficult*))

(defun compute-exists-siphon-k (array k)
  (compute-term-property-k
   'exists-siphon
   (lambda (term) 
     (enum-recognized-p term *exists-siphon-automaton*))
   array
   k))

(defun compute-exists-siphon (array &key (start 1) (end 399) (avoid *difficult*))
  (compute-term-property
   'exists-siphon
   (lambda (term) 
     (enum-recognized-p term *exists-siphon-automaton*))
   array
   :start start
   :end end
   :avoid avoid
))

(defun reset-siphon-sizes (array)
  (reset-property 'siphon-sizes array))

(defun save-bio-graph (array k)
  (with-open-file
      (stream (format nil "~A/RESULTS/~A-~A.res" *bio-directory* k (time-string))
	      :direction :output :if-does-not-exist :create)
    (format stream "~A" (aref array k))))

(defun save-bio-terms-array (array &key (start 1) (end 399) (avoid '()))
  (loop for k from start to end
       unless (member k avoid)
       do (save-bio-graph array k)))

;;
(defun init-bio-graphs ()
  (setf *bio-array* (build-bio-graphs-array 399)))

(defun bt-minimal-siphons (n)
  (unless (zerop (graph-nb-nodes (bio-graph (aref *bio-array* n))))
    (minimal-siphons (bio-term (aref *bio-array* n)))))
