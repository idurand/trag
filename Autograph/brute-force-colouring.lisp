(defun graph-matrix-from-path-list (ps)
  (loop
    with n := (1+ (reduce 'max (reduce 'append ps)))
    with res := (make-array (list n n) :initial-element nil)
    for path in ps
    do
    (loop
      for edge on path
      for from := (first edge)
      for to := (second edge)
      while to
      do (setf (aref res from to) :bi)
      do (setf (aref res to from) :bi)
      )
    finally (return res)))

(defun graph-matrix-from-paths (&rest ps)
  (graph-matrix-from-path-list ps))

(defun graph-matrix-from-graph-term (term)
  (apply 'graph-matrix-from-paths
    (autograph::graph-to-eedges
      (autograph::cwd-term-to-graph term))))

; (graph-matrix-from-paths '(1 2 3) '(2 4 5))
; (array-dimensions (graph-matrix-from-paths '(1 2 3) '(2 4 5)))

(defun edge-lists-from-matrix (m)
  (loop
    with n := (first (array-dimensions m))
    with res := (make-array n :initial-element nil)
    for i from 0 to (1- n)
    do
    (loop
      for j from (1- n) downto 0
      when (aref m i j)
      do (push j (aref res i)))
    finally (return res)))

; (edge-lists-from-matrix (graph-matrix-from-paths '(1 2 3) '(2 4 5)))

(defun k-col-direct (graph-matrix k)
  (loop
    with n := (first (array-dimensions graph-matrix))
    with edge-lists := (edge-lists-from-matrix graph-matrix)
    with col-array := (make-array k :initial-element t)
    with col-assigned := (make-array n :initial-element nil)
    with max-assigned := (make-array n :initial-element 0)
    with col-allowed := (make-array n :initial-element t)
    with i := 0
    while (< -1 i n)
    for current-allowed := (elt col-allowed i)
    when (eq current-allowed t) do
    (setf
      current-allowed
      (progn
        (loop for j from 0 to (1- k) do (setf (elt col-array j) t))
        (loop
          with left := k
          for e in (elt edge-lists i)
          while (> left 0)
          while (< e i)
          for c := (elt col-assigned e)
          when (elt col-array c)
          do (progn
               (decf left)
               (setf (elt col-array c) nil))
          finally (when (> left 0)
                    (return 
                      (loop
                        for c from 0 to
                        (min (1- k)
                             (1+ (elt max-assigned
                                      (max 0 (1- i)))))
                        when (elt col-array c)
                        collect c))))))
    if current-allowed do
    (progn
      (setf (elt col-assigned i) (car current-allowed))
      (setf (elt max-assigned i)
            (max 
              (elt max-assigned
                   (max 0 (1- i)))
              (elt col-assigned i)))
      (setf (elt col-allowed i) (cdr current-allowed))
      (incf i))
    else do
    (progn
      (setf (elt col-allowed i) t)
      (decf i))
    finally (when (= i n) (return col-assigned))))

; (k-col-direct (graph-matrix-from-paths '(1 2 3) '(2 4 5)) 1)
; (k-col-direct (graph-matrix-from-paths '(1 2 3) '(2 4 5)) 2)
; (k-col-direct (graph-matrix-from-paths '(1 2 3) '(2 4 5)) 3)
; (k-col-direct (graph-matrix-from-paths '(1 2 3 1) '(2 4 5)) 3)

(defun k-col-acyclic-direct (graph-matrix k)
  (loop
    with n := (first (array-dimensions graph-matrix))
    with edge-lists := (edge-lists-from-matrix graph-matrix)
    with col-assigned := (make-array n :initial-element nil)
    with max-assigned := (make-array n :initial-element 0)
    with col-allowed := (make-array n :initial-element t)
    ;
    ; We want to know for every pair of colours and for every vertex
    ; which connected component it belongs to
    ;
    ; We only care about the pair of colours if one of the colours
    ; is the colour of the vertex, though
    ;
    ; The connected component is defined by the list of vertices
    ;
    ; Storage structure: (length (minimal-vertex other-vertices…))
    ; 
    ; For ease of backtracking we store a stack of such structures
    with bicol-components := (make-array (list n k) :initial-element nil)
    ;
    ; scratch array just to avoid reallocation, we need to keep track of
    ; permissible colours on every iteration
    with col-array := (make-array k :initial-element t)
    with col-comp-array := (make-array (list k n) :initial-element nil)
    ;
    with i := 0
    while (< -1 i n)
    for current-allowed := (elt col-allowed i)
    for edges := (elt edge-lists i)
    for relevant-color-list := nil
    when (eq current-allowed t) do
    (setf
      current-allowed
      (progn
        (loop for j from 0 to (1- k) do (setf (elt col-array j) t))
        (loop
          with left := k
          for e in edges
          while (> left 0)
          while (< e i)
          for c := (elt col-assigned e)
          when (elt col-array c)
          do (progn
               (decf left)
               (setf (elt col-array c) nil))
          finally (when (> left 0)
                    (return 
                      (loop
                        for c from 0 to 
                        (min (1- k)
                             (1+ (elt max-assigned
                                      (max 0 (1- i)))))
                        when (elt col-array c)
                        collect c
                        do (setf (elt col-array c) nil)
                        ))))))
    if current-allowed do
    (progn
      (setf (elt col-assigned i) (car current-allowed))
      (setf (elt max-assigned i)
            (max 
              (elt max-assigned
                   (max 0 (1- i)))
              (elt col-assigned i)))
      (setf (elt col-allowed i) (cdr current-allowed))
      (loop
        with trivial-component := `(1 (,i))
        for c from 0 to (1- k)
        do (setf (aref bicol-components i c) (list trivial-component))
        do (setf (elt col-array c) (list trivial-component)))
      (unless
        (loop
          with cleanup :=
          (lambda (to-undo)
            (loop
              for entry in to-undo
              do (setf (aref col-comp-array (first entry) (second entry)) nil)))
          with to-undo := nil
          with my-color := (elt col-assigned i)
          for e in edges
          while (< e i)
          for their-color := (elt col-assigned e)
          for component := (first (aref bicol-components e my-color))
          for component-vertex-list := (second component)
          for comp-min := (first component-vertex-list)
          ;
          when (aref col-comp-array their-color comp-min)
          do (progn
               (funcall cleanup to-undo)
               (return t))
          ;
          unless (cdr (elt col-array their-color))
          do (push their-color relevant-color-list)
          ;
          do (push (list their-color comp-min) to-undo)
          do (setf (aref col-comp-array their-color comp-min) t)
          do (push component (elt col-array their-color))
          finally
          (progn
            (funcall cleanup to-undo)))
        (loop
          with my-color := (elt col-assigned i)
          for c in relevant-color-list
          for components := (elt col-array c)
          for full-length := (reduce '+ (mapcar 'first components))
          for vertex-list :=
          (reduce 'append (sort (mapcar 'second components) '< :key 'first))
          for new-component := (list full-length vertex-list)
          do (loop
               for j in vertex-list
               for col := (if (= my-color (elt col-assigned j)) c my-color)
               do (push new-component (aref bicol-components j col))))
        (incf i)
        ))
    else do
    (progn
      (setf (elt col-allowed i) t)
      (decf i)
      (when
        (>= i 0)
        (loop
          with my-color := (elt col-assigned i)
          for c from 0 to (1- k)
          for vertex-list := (second (first (aref bicol-components i c)))
          do (loop
               for v in vertex-list
               do (pop (aref bicol-components v
                             (if (= my-color (elt col-assigned v))
                                    c my-color))))))
      )
    finally (when (= i n) (return col-assigned))))

; (k-col-acyclic-direct (graph-matrix-from-paths '(0 1 2 0)) 2)
; (k-col-acyclic-direct (graph-matrix-from-paths '(0 1 2 0)) 3)
; (k-col-acyclic-direct (graph-matrix-from-paths '(0 1 2 3 0)) 2)
; (k-col-acyclic-direct (graph-matrix-from-paths '(0 1 2 3 0)) 3)
; (k-col-acyclic-direct (graph-matrix-from-paths '(0 1 2 3 4 0 5 7 2 7 9 4 9 6 1 6 8 3 8 5 )) 3)
; (k-col-acyclic-direct (graph-matrix-from-paths '(0 1 2 3 4 0 5 7 2 7 9 4 9 6 1 6 8 3 8 5 )) 4)
; (k-col-acyclic-direct (graph-matrix-from-paths '(0 1 2 3 4 0 5 7 2 7 9 4 9 6 1 6 8 3 8 5 )) 4)
; (k-col-acyclic-direct (graph-matrix-from-path-list (autograph::single-coloring-grid-edges 10 10)) 20)
; (k-col-acyclic-direct (graph-matrix-from-path-list (autograph::single-coloring-grid-edges 5 5)) 41)
; (k-col-acyclic-direct (graph-matrix-from-path-list (autograph::single-coloring-grid-edges 5 5)) 42)
; (k-col-acyclic-direct (graph-matrix-from-path-list (autograph::single-coloring-grid-edges 3 3)) 13)
; (k-col-acyclic-direct (graph-matrix-from-path-list (autograph::single-coloring-grid-edges 3 3)) 14)
; (k-col-acyclic-direct (graph-matrix-from-path-list (autograph::single-coloring-grid-edges 4 4)) 25)
; (k-col-acyclic-direct (graph-matrix-from-path-list (autograph::single-coloring-grid-edges 4 4)) 26)
;;; General formula: (* 2 (1+ (* h (1- k))))
