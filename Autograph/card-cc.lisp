;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass cc-multi-container (multi-container) ())
(defclass cc-container-2multi (multi-max-mixin cc-multi-container) ())

(defmethod multi-max ((container cc-container-2multi)) 2)

(defgeneric make-cc-multi-container (multi-portss multi-max)
  (:documentation "multiset with possible max-multiplicity of multi-max \
                   of connected components (represented by their ports)")
  (:method ((multi-portss list) multi-max)
    (if multi-max
	(make-multi-max-container-generic
	 multi-portss
	 'cc-container-2multi
	 #'equal
	 2)
	(make-multi-container-generic
	 multi-portss
	 'cc-multi-container
	 #'equal
	 #'+))))

(defun make-empty-cc-multi-container (multi-max)
  (make-cc-multi-container '() multi-max))

(defgeneric make-cc-multi-container-from-ports (ports multi-max)
  (:documentation "container with one component (ports)")
  (:method ((ports ports) multi-max)
    (make-cc-multi-container (list (make-object-multi ports)) multi-max)))

(defclass card-cc-state (graph-state)
  ((components :type 'cc-multi-container
	       :initarg :components :reader components)))

(defmethod ports-of ((state card-cc-state))
  (ports-of (components state)))

(defmethod state-final-p ((state card-cc-state))
  (let* ((components (components state))
	 (nb-cc-type (container-size components)))
    (or (zerop nb-cc-type)
	(and (= nb-cc-type 1)
	     (= 1 (attribute-of (car (container-unordered-contents components))))))))

(defmethod ports-of ((s card-cc-state))
  (ports-of (container-unordered-contents (components s))))

(defgeneric make-card-cc-state (multi-set)
  (:method ((components cc-multi-container))
    (make-instance 'card-cc-state :components components)))

(defmethod print-object ((s card-cc-state) stream)
  (format stream "~A" (components s)))

(defmethod graph-ren-target (a b (state card-cc-state))
  (make-card-cc-state
   (container-subst a b (components state))))

(defmethod graph-reh-target ((port-mapping port-mapping) (state card-cc-state))
  (make-card-cc-state
   (apply-port-mapping port-mapping (components state))))

(defmethod graph-add-target (a b (co card-cc-state))
  (let* ((components (components co))
	 (a-components
	   (container-remove-if-not
	    (lambda (component)
	      (ports-member a (object-of component)))
	    components))
	 (b-components
	   (container-remove-if-not
	    (lambda (component)
	      (ports-member b (object-of component)))
	    components))
	 (no-a-b
	   (container-remove-if
	    (lambda (component)
	      (or (ports-member a (object-of component))
		  (ports-member b (object-of component))))
	    components)))
    (when (or (zerop (container-size a-components))
	      (zerop (container-size b-components)))
      (return-from graph-add-target co))
    (let ((new-component (make-empty-ports)))
      (container-map
       (lambda (c)
	 (ports-nunion new-component (object-of c)))
       a-components)
      (container-map
       (lambda (c)
	 (ports-nunion new-component (object-of c)))
       b-components)
      (make-card-cc-state
       (container-nadjoin
	(make-object-multi new-component)
	no-a-b)))))

(defmethod graph-oplus-target ((co1 card-cc-state) (co2 card-cc-state))
  (make-card-cc-state (container-union (components co1) (components co2))))

(defvar *multi-max* nil)

(defgeneric card-cc-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg null))
    (let ((port (port-of root)))
      (make-card-cc-state
       (make-cc-multi-container-from-ports 
	(make-ports (list port)) *multi-max*))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'card-cc-transitions-fun))
  (:method ((root (eql *empty-symbol*)) (arg null))
    (cwd-transitions-fun root arg #'card-cc-transitions-fun)))

(defun multi-card-cc-automaton (multi-max &key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-signature cwd :orientation orientation)
   (lambda (root states)
     (let ((*multi-max* multi-max)
	   (*neutral-state-final-p* t)
	   (*oriented* nil))
       (card-cc-transitions-fun
	root states)))
   :name (cwd-automaton-name
	  (if multi-max
	      (format nil "CARD-CC~A" multi-max)
	      "CARD-CC")
	  cwd)))

(defun card-cc-automaton (&key (cwd 0) (orientation :unoriented))
  (multi-card-cc-automaton nil :cwd cwd :orientation orientation))

(defun count-cc-transducer (&key (cwd 0) (orientation :unoriented))
  (let ((f (make-cwd-automaton
	    (cwd-signature cwd :orientation orientation)
	    (lambda (root states)
	      (let ((*multi-max* nil)
		    (*neutral-state-final-p* t)
		    (*oriented* nil))
		(card-cc-transitions-fun
		 root states)))
	    :final-state-fun #'identity
	    :name (cwd-automaton-name "COUNT-CC" cwd))))
    (setf (output-fun f)
	  (lambda (s)
	    (container-cardinality (components s))))
    f))

(defun table-card-cc-automaton (cwd &key (orientation :unoriented))
  (compile-automaton (multi-card-cc-automaton 2 :cwd cwd :orientation orientation)))

(defun multi-subgraph-card-cc-automaton (multi-max m j &key (cwd 0) (orientation :unoriented))
  (assert (<= 1 j m))
   (nothing-to-xj
    (multi-card-cc-automaton multi-max :cwd cwd :orientation orientation) m j))

(defun subgraph-card-cc-automaton (m j &key (cwd 0) (orientation :unoriented))
  (multi-subgraph-card-cc-automaton nil m j :cwd cwd :orientation orientation))

(defun table-subgraph-card-cc-automaton (m j cwd &key (orientation :unoriented))
  (compile-automaton (multi-subgraph-card-cc-automaton 2 m j :cwd cwd :orientation orientation)))

(defun test-card-cc ()
  (let* ((a (card-cc-automaton))
	 (t1 (cwd-term-pn 4))
	 (t2 (cwd-term-kn 3))
	 (t3 (cwd-term-cycle 3))
	 (t4 (cwd-term-oplus t1 (cwd-term-oplus t2 t3)))
	 (t5 (cwd-term-oplus t1 (cwd-term-stable 4))))
    (assert (= 1 (container-size (components (compute-target t1 a)))))
    (assert (= 1 (container-size (components (compute-target t2 a)))))
    (assert (= 1 (container-size (components (compute-target t3 a)))))
    (assert (= 3 (container-size (components (compute-target t4 a)))))
    (assert (= 5 (compute-value t5 (count-cc-transducer))))))
