;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass color-domination-state (graph-state oriented-mixin)
  ((ports1 :type ports :initarg :ports1 :reader ports1) ; c1 (0)
   (ports2 :type ports :initarg :ports2 :reader ports2) ; c2 (2)
   ))

(defgeneric make-color-domination-state (ports1 ports2 oriented)
  (:method ((ports1 ports) (ports2 ports) oriented)
    (make-instance
     'color-domination-state
     :ports1 ports1
     :ports2 ports2
     :oriented oriented)))

(defmethod print-object ((s color-domination-state) stream)
  (format stream "{~A}-{~A}" (ports1 s) (ports2 s)))

(defmethod graph-oplus-target 
    ((state1 color-domination-state) (state2 color-domination-state))
  (make-color-domination-state
   (ports-union (ports1 state1) (ports1 state2))
   (ports-union (ports2 state1) (ports2 state2))
   (oriented-p state1)))

(defmethod graph-add-target (a b (ds color-domination-state))
  (let ((oriented (oriented-p ds)))
    (cond ((and (ports-member a (ports1 ds))
		(ports-member b (ports2 ds)))
	   (make-color-domination-state
	    (ports-remove a (ports1 ds))
	    (ports2 ds)
	    oriented))
	  ((and (not (oriented-p ds))
		(ports-member b (ports1 ds))
		(ports-member a (ports2 ds)))
	   (make-color-domination-state
	    (ports-remove b (ports1 ds))
	    (ports2 ds)
	    oriented))
	  (t ds))))

(defmethod graph-ren-target (a b (s color-domination-state))
  (make-color-domination-state
   (ports-subst b a (ports1 s))
   (ports-subst b a (ports2 s))
   (oriented-p s)))

(defgeneric color-domination-transitions-fun (root arg)
  (:method ((root color-constant-symbol) (arg (eql nil)))
    (let ((port (port-of root)))
      (let ((color (color root)))
	(case color
	  (1 (make-color-domination-state
	      (make-ports-from-port port)
	      (make-empty-ports)
	      *oriented*))
	  (2 (make-color-domination-state
	      (make-empty-ports)
	      (make-ports-from-port port)
	      *oriented*))
	  (3
	   (make-color-domination-state
	    (make-empty-ports)
	    (make-empty-ports)
	    *oriented*)))))))

(defmethod color-domination-transitions-fun ((root abstract-symbol) (arg list))
  (cwd-transitions-fun root arg #'color-domination-transitions-fun))

(defmethod state-final-p ((state color-domination-state))
  (and
   (ports-empty-p (ports1 state))
   ;; add that C2 is not empty for roman domination otherwise the min is always 0!
   (not (ports-empty-p (ports2 state)))))

(defun color-domination-automaton (&optional (cwd 0))
  "every vertex of C1 is the head of an edge with tail in C2"
  (make-cwd-automaton
   (cwd-color-signature cwd 3)
   #'color-domination-transitions-fun
   :name (cwd-automaton-name "COLOR-DOMINATION-X1-X2" cwd)))

(defun table-color-domination-automaton (cwd) ; color-domination between X1 and X2
  (compile-automaton
   (color-domination-automaton cwd)))

(defun color-domination-transducer (afun)
  (constants-color-projection
   (attribute-automaton (color-domination-automaton) afun)))

(defgeneric roman-symbol-fun (color-constant-symbol)
  (:method ((s color-constant-symbol))
    (let ((color (color s)))
      (lambda () (make-array
		  2
		  :initial-contents (case color ;; (|c2| |c3|)
				      (1 (list 0 0))
				      (2 (list 1 0))
				      (3 (list 0 1))))))))

(defmethod roman-symbol-fun ((s non-constant-mixin))
  (lambda (&rest attributes)
    (reduce #'vector-sum attributes)))

(defun vector-roman-value (v)
  (+ (* 2 (aref v 0)) ; color2 counts for 2
     (aref v 1)))     ; color3 counts for 1

(defgeneric assignment-roman-value (assignment)
  (:method ((assignment positions-assignment))
    (loop for pa in (position-assignments assignment)
	  sum (case (pa-assignment pa)
		(2 2)
		(3 1)
		(t 0)))))

(defun min-wrt-roman-value (&rest vs) ;; nil stands for infinity
  (setq vs (remove nil vs))
  (unless (endp vs)
    (loop
      with vmin = (car vs)
      for v in (cdr vs)
      when (< (vector-roman-value v) (vector-roman-value vmin))
	do (setq vmin v)
      finally (return vmin))))

(defparameter *roman-value-afun*
  (make-afuns #'roman-symbol-fun #'min-wrt-roman-value "Roman-value"))

(defun roman-value-color-domination-transducer ()
  (let ((transducer (color-domination-transducer *roman-value-afun*)))
    (setf (output-fun transducer) #'vector-roman-value)
    transducer))

;;; computing roman assignments
(defun assignment-color-domination-transducer ()
  (color-domination-transducer *assignment-afun*))

(defgeneric assignment-color-domination-enumerator (term)
  (:method ((term term))
    (final-value-enumerator 
     term 
     (assignment-color-domination-transducer))))

(defun min-roman-assignment (&rest assignments)
  (setq assignments (remove nil assignments))
  (when assignments
    (loop
      with vmin = (car assignments)
      for v in (cdr assignments)
      when (< (assignment-roman-value v) (assignment-roman-value vmin))
	do (setq vmin v)
      finally (return vmin))))

(defun assignment-roman-domination-transducer ()
  (let ((transducer (assignment-color-domination-transducer)))
    (setf (output-fun transducer)
	  (lambda (a)
	    (let ((min (apply #'min (mapcar #'assignment-roman-value a))))
	      (remove-if-not
	       (lambda (assignment)
		 (= (assignment-roman-value assignment) min))
	       a))))
    transducer))

(defgeneric roman-value (term)
  (:method ((term term))
    (compute-final-value term (roman-value-color-domination-transducer))))

(defgeneric assignment-of-value-color-domination-enumerator (term value)
  (:method ((term term) (value integer))
    (make-filter-enumerator  
     (assignment-color-domination-enumerator term)
     (lambda (pa) (= value (assignment-roman-value pa))))))

(defgeneric roman-assignment-enumerator (term)
  (:method ((term term))
    (assignment-of-value-color-domination-enumerator term (roman-value term))))

;; roman value grille NxN
;; N RV
;; 1  2
;; 2  3
;; 3  6
;; 4  8
;; 5 14
;; 6 19
;; 7 24
