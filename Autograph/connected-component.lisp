;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass abstract-connected-component (cwd-mixin key-mixin) ()
  (:documentation "connected component"))

(defclass isolated-vertex-component (abstract-connected-component oriented-mixin)
  ((port :type integer :initarg :port :reader port))
  (:documentation "connected component with one vertex"))

(defmethod compare-object ((s1 isolated-vertex-component) (s2 isolated-vertex-component))
  (= (port s1) (port s2)))

(defmethod strictly-ordered-p
    ((s1 isolated-vertex-component) (s2 isolated-vertex-component))
  (< (port s1) (port s2)))

(defgeneric isolated-vertex-p (abstract-connected-component)
  (:method ((state abstract-connected-component))
    (typep state 'isolated-vertex-component)))

(defmethod ports-of ((iv isolated-vertex-component))
  (make-ports-from-port (port iv)))

(defun make-isolated-vertex-component (port oriented)
  (make-instance 'isolated-vertex-component :port port :oriented oriented))

(defmethod print-object ((iv isolated-vertex-component) stream)
  (if *print-object-readably*
      (format stream "@~A" (port-to-string (port iv)))
      (format stream "~A" (port iv))))

(defgeneric edges (isolated-vertex-component)
  (:method ((iv isolated-vertex-component))
    (make-empty-ports-relation (oriented-p iv))))

(defgeneric paths2 (isolated-vertex-component)
  (:method ((iv isolated-vertex-component))
    (make-empty-ports-relation (oriented-p iv))))

(defgeneric all-paths (abstract-connected-component)
  (:method ((c abstract-connected-component))
    (ports-relation-union (edges c) (paths2 c))))

(defclass connected-component (abstract-connected-component)
  ((edges :initarg :edges :reader edges :type ports-relation :documentation "edge relation a -- b")
   (paths2 :initarg :paths2 :reader paths2 :type ports-relation :documentation "2-edges relation a -- x-- b")))

(defmethod oriented-p ((cc connected-component))
  (oriented-p (edges cc)))

(defmethod compare-object ((s1 connected-component) (s2 connected-component))
  (and
   (compare-object (edges s1) (edges s2))
   (compare-object (paths2 s1) (paths2 s2))))

(defmethod strictly-ordered-p ((co1 connected-component) (co2 connected-component))
  (or (strictly-ordered-p (edges co1) (edges co2))
      (and
       (compare-object  (edges co2) (edges co1))
       (strictly-ordered-p (paths2 co1) (paths2 co2)))))

(defmethod strictly-ordered-p ((iv1 isolated-vertex-component) (iv2 isolated-vertex-component))
  (< (port iv1) (port iv2)))

(defmethod strictly-ordered-p ((iv isolated-vertex-component) (co connected-component))
  t)

(defun make-connected-component (edges paths2)
  (make-instance 'connected-component :edges edges :paths2 paths2))

(defmethod print-object ((co connected-component) stream)
  (if *print-object-readably*
      (format stream "<~A,~A>" (ports-relation-to-string (edges co)) (ports-relation-to-string (paths2 co)))
      (format stream "~A,~A" (ports-relation-to-string (edges co)) (ports-relation-to-string (paths2 co)))))

(defmethod ports-of ((c connected-component))
  (ports-of (edges c)))

(defgeneric component-ren (a b isolated-vertex-component)
  (:method (a b (iv isolated-vertex-component))
    (make-isolated-vertex-component
     (subst b a (port iv))
     (oriented-p iv))))

(defgeneric component-reh (port-mapping isolated-vertex-component)
  (:method ((port-mapping port-mapping) (iv isolated-vertex-component))
    (make-isolated-vertex-component
     (apply-port-mapping port-mapping (port iv))
     (oriented-p iv))))

(defmethod component-ren (a b (c connected-component))
  (make-connected-component
   (ports-relation-subst b a (edges c))
   (ports-relation-subst b a (paths2 c))))

(defmethod component-reh ((port-mapping port-mapping) (c connected-component))
  (make-connected-component
   (apply-port-mapping port-mapping (edges c))
   (apply-port-mapping port-mapping (paths2 c))))

(defgeneric connect-two-components
    (a b abstract-connected-component1 abstract-connected-component2)
  (:method (a b (c1 abstract-connected-component) (c2 abstract-connected-component))
    (let ((edges (ports-relation-union (edges c1) (edges c2)))
	  (paths2 (new-paths2 a b c1 c2)))
      (ports-relation-nadjoin (list a b) edges)
      (make-connected-component
       edges
       (ports-relation-union-gen
	(list
	 (paths2 c1) (paths2 c2) paths2)
	(oriented-p c1))))))

(defgeneric connect-components (a b components)
  (:method (a b (components list))
    (let ((c1 (car components)))
      (loop for c2 in (cdr components)
	    do (setf c1 (connect-two-components a b c1 c2)))
      c1)))

(defgeneric sort-components-according-two-ports (a b components)
  (:method (a b (components list))
    (let ((a-and-b '())
	  (only-a '())
	  (only-b '())
	  (none '()))
      (loop for c in components
	    do (let ((ports (ports-of c)))
		 (cond ((and (ports-member a ports)
			     (ports-member b ports))
			(push c a-and-b))
		       ((ports-member a ports)
			(push c only-a))
		       ((ports-member b ports)
			(push c only-b))
		       (t (push c none)))))
      (values a-and-b only-a only-b none))))

(defgeneric add-to-components (a b components)
  (:method (a b (components list))
    (multiple-value-bind (a-and-b only-a only-b none)
	(sort-components-according-two-ports a b components)
      (when (and (endp a-and-b)
		 (or (endp only-a) (endp only-b)))
	(return-from add-to-components (append none only-a only-b)))
      (when (endp a-and-b)
	(setf a-and-b (list (connect-two-components a b (car only-a) (car only-b))))
	(setf only-a (cdr only-a))
	(setf only-b (cdr only-b)))
      (cons (connect-components a b (append a-and-b only-a only-b) ) none))))

(defgeneric sharp (a isolated-vertex-component)
  (:method  ((a integer) (iv isolated-vertex-component))
    (if (= (port iv) a) 1 0))
  (:method ((a integer) (c connected-component))
    (cond ((ports-relation-member (list a a) (all-paths c)) 2)
	  ((ports-member a (ports-of c)) 1)
	  (t 0)))
  (:method  (a (components list))
    (cond ((not (ports-member a (ports-of components))) 0)
	  ((find-if (lambda (c)
		      (ports-relation-member (list a a) (all-paths c)))
		    components) 2)
	  (t (count-if (lambda (c)
			 (ports-member a (ports-of c)))
		       components)))))
