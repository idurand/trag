;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;;; automaton for testing connectedness of unoriented graphs

;; each component is a ports (container)
(defclass connectedness-state (graph-state)
  ((components :type list :initarg :components :reader components :initform nil)))

(defun sort-components (components) (sort components #'strictly-ordered-p))

(defmethod ports-of ((s connectedness-state))
  (ports-of (components s)))

(defun reduce-components (components)
  (if (endp (cdr components))
      components
      (let ((no-dup (remove-duplicates components
				       :test #'compare-object)))
	(if (>= (length no-dup) 2)
	    no-dup
	    (cons (car no-dup) no-dup)))))

(defun make-connectedness-state (components)
  (make-instance
   'connectedness-state
   :components (sort-components (reduce-components components))))

(defmethod print-object ((co connectedness-state) stream)
  (format stream "[")
  (loop for c in (components co)
	do (format stream "~A" c))
  (format stream "]"))

(defmethod graph-ren-target (a b (co connectedness-state))
  (make-connectedness-state
   (mapcar (lambda (component)
	     (ports-subst
	      b a
	      component))
	   (components co))))

(defmethod graph-reh-target ((port-mapping port-mapping) (co connectedness-state))
  (make-connectedness-state
   (mapcar (lambda (component)
	     (apply-port-mapping port-mapping component))
	   (components co))))

(defmethod graph-add-target (a b (co connectedness-state))
  (let* ((components (components co))
	 (a-components
	   (remove-if-not
	    (lambda (component)
	      (ports-member a component))
	    components))
	 (b-components
	   (remove-if-not
	    (lambda (component)
	      (ports-member b component))
	    components))
	 (no-a-b
	   (remove-if
	    (lambda (component)
	      (or (ports-member a component)
		  (ports-member b component)))
	    components)))
    (unless (and a-components b-components)
      (return-from graph-add-target co))
    (make-connectedness-state
     (cons
      (merge-containers
       (append a-components b-components))
      no-a-b))))

(defmethod graph-oplus-target ((co1 connectedness-state) (co2 connectedness-state))
  (make-connectedness-state
   (append (components co1) (components co2))))

(defmethod state-final-p ((co connectedness-state))
  (and (null (cdr (components co))) co))

(defgeneric connectedness-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
  (make-connectedness-state
   (list (make-ports (list (port-of root))))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'connectedness-transitions-fun))
  (:method ((root (eql *empty-symbol*)) (arg (eql nil)))
    (cwd-transitions-fun root arg #'connectedness-transitions-fun)))

(defun connectedness-automaton (&key (orientation :unoriented) (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd :orientation orientation)
   (lambda (root states)
     (let ((*neutral-state-final-p* t)
	   (*oriented* nil))
       (connectedness-transitions-fun
	root states)))
   :name (cwd-automaton-name "CONNECTEDNESS" cwd)))

(defun table-connectedness-automaton (cwd &key (orientation :unoriented))
  (compile-automaton (connectedness-automaton :cwd cwd :orientation orientation)))

(defun subgraph-connectedness-automaton (m j &key (cwd 0) (orientation :unoriented))
  (assert (<= 1 j m))
  (nothing-to-xj (connectedness-automaton :cwd cwd :orientation orientation) m j))

(defun table-subgraph-connectedness-automaton (m j cwd &key (orientation :unoriented))
  (compile-automaton (subgraph-connectedness-automaton m j :cwd cwd :orientation orientation)))

(defun test-connectedness1 ()
  (let ((a (table-connectedness-automaton 2)))
    (verify-automaton a 10 87)
    (verify-automaton (minimize-automaton a) 6 41)
    (setq a (table-connectedness-automaton 3))
    (verify-automaton a 134 10254)
    (verify-automaton (minimize-automaton a) 56 2103)))

(defun test-connectedness2 ()
  (let ((f (connectedness-automaton))
	(term (input-cwd-term "oplus(add_a_b(oplus(a,b)), add_a_c(oplus(a,c)))")))
    (assert (not (recognized-p term f)))
    (assert (recognized-p (cwd-term-unoriented-add 0 1 term) f))
    (assert (recognized-p (input-cwd-term "add_a->b(oplus(a,b))") f))))
    
