;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun test-count-house-subgraphs ()
  (let* ((m 5)
	 (count-house-automaton
	   (vbits-projection
	    (attribute-automaton
	     (intersection-automata-compatible
	      (list ;; graph in the shape of a house
		    (edge-automaton m 1 2)
		    (edge-automaton m 1 3)	     
		    (edge-automaton m 2 3)	     
		    (edge-automaton m 2 4)	     
		    (edge-automaton m 4 5)	     
		    (edge-automaton m 3 5)
		    (complement-automaton (edge-automaton m 1 4))
		    (complement-automaton (edge-automaton m 1 5))
		    (complement-automaton (edge-automaton m 3 4))
		    (complement-automaton (edge-automaton m 2 5))))
	     *count-afun*))))
    (assert (= 8
	       (compute-final-value
		(cwd-decomposition
		 (graph-from-paths
		  '((1 2 3 4 5 6 7 8 1) (2 4) (4 6) (6 8) (2 8))))
		count-house-automaton)))))
