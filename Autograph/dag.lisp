;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; detection of circuits in terms corresponding to oriented graphs
;;; uses the transition function of forest except at constants where
;;; the states need to be initially oriented

(in-package :autograph)

(defgeneric dag-transitions-fun (root states)
  (:method ((root abstract-symbol) (arg list))
    (forest-transitions-fun root arg)))

(defmethod dag-transitions-fun
    ((root cwd-constant-symbol) (arg (eql nil)))
  (make-forest-state
   (list (make-isolated-vertex-component (port-of root) t))
   '()))

(defmethod dag-transitions-fun ((root abstract-symbol) (arg list))
  (cwd-transitions-fun root arg #'dag-transitions-fun))

(defun dag-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd :orientation t)
   (lambda (root states)
     (let ((*neutral-state-final-p* t))
       (dag-transitions-fun root states)))
   :name (cwd-automaton-name "DAG" cwd)))

(defun subgraph-dag-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
  (nothing-to-xj (dag-automaton cwd) m j))

(defun dag-one-color-automaton (k color &optional (cwd 0))
  (assert (<= 1 color k))
  (nothing-to-color (forest-automaton cwd) k color))

(defun dag-color-automaton (k &optional (cwd 0))
   (nothing-to-colors-constants (forest-automaton cwd) k))

(defun test-dag-automaton (dag-automaton)
  (let* ((stable (cwd-decomposition (graph-stable 4 t)))
	 (p4 (cwd-decomposition (graph-pn 4 t)))
	 (kn (cwd-decomposition (graph-kn 4 t)))
	 (c2 (cwd-decomposition (graph-cycle 2 t)))
	 (c4 (cwd-decomposition (graph-cycle 4 t)))
	 (c (complement-automaton dag-automaton)))
    (assert (recognized-p stable dag-automaton))
    (assert (recognized-p p4 dag-automaton))
    (assert (not (recognized-p kn dag-automaton)))
    (assert (not (recognized-p c2 dag-automaton)))
    (assert (not (recognized-p c4 dag-automaton)))
    (assert (recognized-p kn c))
    (assert (recognized-p c2 c))
    (assert (recognized-p c4 c))))

(defun test-dag ()
  (test-dag-automaton (dag-automaton)))
