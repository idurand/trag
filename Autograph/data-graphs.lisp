;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defparameter *siphon0* (graph-from-paths '((1 2 1)) :oriented t))
(defparameter *tsiphon0* (cwd-decomposition-bipartite *siphon0*))

(defparameter *siphon1* (graph-from-paths '((1 2)) :oriented t))
(defparameter *tsiphon1* (cwd-decomposition-bipartite *siphon1*))

(defparameter *nosiphon1-paths* '((2 1)))
(defparameter *nosiphon1* (graph-from-paths *nosiphon1-paths* :oriented t))
(defparameter *tnosiphon1* (cwd-decomposition-bipartite *nosiphon1*))
(defparameter *nosiphon2-paths* '((2 1) (3 4)))
(defparameter *nosiphon2* (graph-from-paths *nosiphon2-paths* :oriented t))
(defparameter *tnosiphon2* (cwd-decomposition-bipartite *nosiphon2*))
(defparameter *siphon-paths* '((1 2 3 2) (1 4 5) (5 6)))
(defparameter *siphon* (graph-from-paths *siphon-paths* :oriented t))
(defparameter *tsiphon* (cwd-decomposition-bipartite *siphon*))
(defparameter *nosiphon-paths* '((2 1 4)))
(defparameter *nosiphon* (graph-from-paths *nosiphon-paths* :oriented t))
(defparameter *tnosiphon* (cwd-decomposition-bipartite *nosiphon*))
(defparameter *siphon3-paths* '((1 2 3 2) (1 4 5 4) (5 6)))
(defparameter *siphon3* (graph-from-paths *siphon3-paths* :oriented t))
(defparameter *tsiphon3* (cwd-decomposition-bipartite *siphon3*))
(defparameter *tsiphon* (cwd-decomposition-bipartite *siphon*))
(defparameter *nosiphon3-paths* '((1 2 3)))
(defparameter *nosiphon3* (graph-from-paths *nosiphon3-paths* :oriented t))
(defparameter *tnosiphon3* (cwd-decomposition-bipartite *nosiphon3*))
(defparameter *nosiphon4-paths* '((2 1 3)))
(defparameter *nosiphon4* (graph-from-paths *nosiphon4-paths* :oriented t))
(defparameter *tnosiphon4* (cwd-decomposition-bipartite *nosiphon4*))
