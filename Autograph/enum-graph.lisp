;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;;; this is right now an unused file
;;; TODO see what should be kept

;;; voir le init-enumerator de combination

;; Liste des sommets
(defun vertices-list (n)
  (iota n 1))

(defun non-circ-earc (a b)
  (if (or (= a b) (< b a))
      '()
      (list a b)))

(defun all-earcs-from-nodes (v-list)
  (remove
   nil 
   (mapcar (lambda(x) 
	     (non-circ-earc 
	      (first x) (second x))) 
	   (cartesian-product (list v-list v-list)))))

(defun all-earcs-list (n)
  (all-earcs-from-nodes (vertices-list n)))

(defun all-earcs-vector (n)
  (let ((earcs (all-earcs-list n)))
    (make-array (1+ (length earcs)) :initial-conteXnts (cons nil earcs))))

;;Création des tuples
(defun make-binary-tuple-enumerator (n)
  (make-product-enumerator
   (make-list n :initial-element (make-list-enumerator '(1 0)))))

(defun make-m-binary-tuple-enumerator (n m)
  (make-filter-enumerator 
   (make-binary-tuple-enumerator n)
   (lambda (tuple)
     (= m (apply #'+ tuple)))))

;;Sélection des arcs en fonction de la valeur du tuple
(defun earcs-from-tuple (tuple earcs) 
  (mapcan 
   (lambda (arc b)
     (if (onep b)
	 (list arc)
	 nil))
   earcs
   tuple))

(defun make-m-earcs (n m)
  (arrange (all-earcs-list n) m))

(defun earcs-connected-p (earcs nb-nodes)
  (= (length (enodes-from-earcs earcs)) nb-nodes))

(defun make-all-earcs-enumerator (nb-nodes)
  (let* ((earcs (all-earcs-list nb-nodes))
	 (nb-max-edges (length earcs)))
    (make-funcall-enumerator
     (lambda(x)
       (earcs-from-tuple x earcs))
     (make-binary-tuple-enumerator nb-max-edges))))

(defun make-m-earcs-enumerator (nb-nodes nb-edges)
  (let* ((earcs (all-earcs-list nb-nodes))
	 (nb-max-edges (length earcs)))
    (make-funcall-enumerator
     (lambda(x)
       (earcs-from-tuple x earcs))
     (make-m-binary-tuple-enumerator nb-max-edges nb-edges))))

(defun keep-earcs-with (e &optional (edges '((1 2) (1 3) (1 4))))
  (make-filter-enumerator
   e
   (lambda (earcs)
     (loop
       while edges
       unless (equal (pop edges) (pop earcs))
	 do (return)
       finally (return t)))))


(defun filter-graph-enumerator-with-properties (graph-enumerator properties)
  (make-filter-enumerator
   graph-enumerator
   (lambda (g) (every (lambda (property)
			(funcall property g))
		      properties))))

(defun make-all-graphs-enumerator (n &optional (properties (list #'graph-connected-p)))
  (let* ((earcs-enumerator (keep-earcs-with (make-all-earcs-enumerator n)))
	 (e (make-funcall-enumerator
	     (lambda(x)
	       (graph-from-earcs-and-enodes x (vertices-list n))) earcs-enumerator)))
    (filter-graph-enumerator-with-properties e properties)))

(defun make-m-graph-enumerator (nb-nodes nb-edges &optional (properties (list #'graph-connected-p)))
  (let* ((earcs-enumerator (keep-earcs-with (make-m-earcs-enumerator nb-nodes nb-edges)))
	 (e (make-funcall-enumerator
	     (lambda(x)
	       (graph-from-earcs-and-enodes x (vertices-list nb-nodes))) earcs-enumerator)))
    (filter-graph-enumerator-with-properties e properties)))

(defun make-clique-width-enumerator (n)
  (make-no-duplicates-enumerator
   (make-funcall-enumerator 
    (lambda (graph)
      (derivation-cwd (sat-derivation graph)))
    (make-all-graphs-enumerator n))))

(defun has-optimal-cwd (graph cwd)
  (and (not (sat-for-cwd graph (1- cwd)))
       (sat-for-cwd graph cwd)))

(defun optimal-cwd-test (cwd)
  (lambda (graph) (has-optimal-cwd graph cwd)))

(defun make-m-graph-cwd-enumerator (nb-nodes nb-edges cwd)
  (filter-graph-enumerator-with-properties 
   (make-m-graph-enumerator nb-nodes nb-edges)
   (list
    #'graph-connected-p
    #'graph-cycle-p
    (optimal-cwd-test cwd))))

(defparameter *properties* (list  
			    #'graph-connected-p
			    #'graph-cycle-p
			    #'regular-p))

(defun nb-max-edges (n)
  (/ (* n (1- n)) 2))

(defun interesting-nb-edges (nb-nodes)
  (let* ((nb-edges (/ (* nb-nodes (1- nb-nodes)) 2))
	 (middle (floor (/ nb-edges 2)))
	 (fourth (floor (/ middle 2))))
    (loop
      repeat (+ (- middle fourth) 2)
      for i from 0
      collect (- middle i 1)
      collect (+ middle i)
      )))

(defun make-graph-cwd-enumerator (nb-nodes cwd)
  (make-sequential-enumerator
   (mapcar
    (lambda (nb-edges)
      (make-m-graph-cwd-enumerator nb-nodes nb-edges cwd))
    (interesting-nb-edges nb-nodes))))

(defun make-graph-with-cwd-enumerator-bis (n cwd)
  (make-filter-enumerator 
   (make-all-graphs-enumerator n)
   (lambda (graph)
     (and 
      (not (sat-for-cwd graph (1- cwd)))
      (sat-for-cwd graph cwd)
      ))))

(defun timings (function)
  (let ((real-base (get-internal-real-time)))
    (funcall function)
    (values
     (float (/ (- (get-internal-real-time) real-base) internal-time-units-per-second)))))


(defun collect-data-from-graph (gr)
  (let ((cwd-data nil))
    (list (graph-nb-nodes gr)
	  (graph-nb-edges gr)
	  (timings (lambda () (setf cwd-data (cwd-decomposition gr))))
	  (clique-width cwd-data))))

(defun n-file-name (n)
  (substitute (digit-char n) #\n "~/trag/Stage/Results/n-results.txt"))

(defun launch-test-for-n-stream (n stream)
  (let ((graphs (make-all-graphs-enumerator n)))
    (format stream 
	    "nb-nodes , nb-edges , time , cwd ~%")
    (call-enumerator graphs)
    (loop as gr = (next-element graphs)
	  while gr do
	    (let ((data (collect-data-from-graph gr)))
	      (format stream
		      "~D , ~D , ~9,9F , ~D ~%"
		      (first data)
		      (second data)
		      (third data)
		      (fourth data))))))

(defun launch-test-for-n-file (filename n)
  (with-open-file (n-graph filename
			   :direction :output
			   :if-exists :supersede
			   :if-does-not-exist :create)
    (launch-test-for-n-stream n n-graph)))

(defun launch-test-for-n (n)
  (launch-test-for-n-file (n-file-name n) n))

(defun make-all-results ()
  (do* ((n 2 (1+ n)))
       ((= 8 n) t)
    (launch-test-for-n n)))

(defun generic-spliter-with-nil (graphs filter)
  (multiple-value-list 
   (split-list graphs filter)))

(defun generic-spliter (graphs filter)
  (remove nil (generic-spliter-with-nil graphs filter)))

;;   (split-list graphs (lambda(x) (not (funcall filter x))))))
;;   (split-list graphs (complement filter))))

(defun filter-nb-earcs-2 (graphs nb-earcs)
  (do* ((cur-nb 1 (1+ cur-nb))
	(sub (generic-spliter 
	      graphs 
	      (lambda(x) (= cur-nb (graph-nb-edges x))))
	     (generic-spliter 
	      (car (last sub))
	      (lambda(x) (= cur-nb (graph-nb-edges x)))))
	(res (first sub) (cons (first sub) res)))
       ((= nb-earcs cur-nb) res)))

(defun filter-nb-earcs (graphs nb-earcs)
  (filter-classes-with-properties
   (list graphs)
   (mapcar (lambda (i)
	     (lambda (g)
	       (= i (graph-nb-edges g))))
	   (iota (1+ nb-earcs)))))

(defun filter-classes-with-property (classes property)
  (loop
    for classe in classes
    nconc (generic-spliter classe property)))

(defun filter-classes-with-properties (classes properties)
  ;; entree liste de classes
  ;; sortie liste de classes
  (dolist (property properties classes)
    (setf classes (filter-classes-with-property classes property))))

(defun filter-with-properties (objects properties)
  ;; entree liste de graphes et liste de proprietes
  ;; en sortie liste classes (listes) de graphes verifiant les memes proprietes
  (filter-classes-with-properties (list objects) properties))
