;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun make-oterm-enumerator (term)
  (make-term-enumerator term (oprojection-ih)))

(defun make-constants-color-term-enumerator (term k)
  (make-term-enumerator term (constants-color-projection-ih k)))
