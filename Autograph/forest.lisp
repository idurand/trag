;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass forest-state (graph-state)
  ((isolated-vertex-components
    :initform nil :initarg :isolated-vertex-components
    :reader isolated-vertex-components)
   (connected-components
    :initform nil :initarg :connected-components
    :reader connected-components))
  (:documentation
   "state for forest or dag automaton; \
    works for oriented and non-oriented case"))

(defmethod compare-object ((fo1 forest-state) (fo2 forest-state))
  (compare-object (isolated-vertex-components fo1)
		  (isolated-vertex-components fo2))
  (compare-object (connected-components fo1)
		  (connected-components fo2)))

(defmethod strictly-ordered-p ((s1 forest-state) (s2 forest-state))
  (or (strictly-ordered-p (isolated-vertex-components s1)
			  (isolated-vertex-components s2))
      (and (compare-object (isolated-vertex-components s1)
			   (isolated-vertex-components s2))
	   (strictly-ordered-p (connected-components s1)
			       (connected-components s2)))))

(defmethod ports-of ((fo forest-state))
  (ports-union
   (ports-of (isolated-vertex-components fo))
   (ports-of (connected-components fo))))

(defgeneric forest-components (forest-state)
  (:method ((s forest-state))
    (append (isolated-vertex-components s) (connected-components s))))

(defmethod state-final-p ((fo forest-state)) t)

;; todo remove dup at the same time we sort
;; delete-duplicates-except pairs
(defun reduce-forest-components (components)
  (sort
   (remove-duplicates-except-pairs
    components
    :test #'compare-object)
   #'strictly-ordered-p))

(defun make-forest-state (isolated-vertex-components connected-components)
  (make-instance
   'forest-state
   :isolated-vertex-components
   (reduce-forest-components isolated-vertex-components)
   :connected-components
   (reduce-forest-components connected-components)))

(defmethod print-object ((fo forest-state) stream)
  (when *print-object-readably*
    (format stream "["))
  (let ((isolated-vertex-components (isolated-vertex-components fo))
	(connected-components (connected-components fo)))
    (unless (endp isolated-vertex-components)
      (display-sequence (isolated-vertex-components fo) stream :sep ","))
    (unless (or (endp isolated-vertex-components) (endp connected-components))
      (format stream ","))
    (display-sequence (connected-components fo) stream :sep ",")
    (when *print-object-readably*
      (format stream "]"))))

(defun components-ren (a b components)
  (mapcar
   (lambda (cc) (component-ren a b cc))
   components))

(defmethod graph-ren-target (a b (fo forest-state))
  (make-forest-state
   (components-ren a b (isolated-vertex-components fo))
   (components-ren a b (connected-components fo))))

(defmethod graph-reh-target ((port-mapping port-mapping) (fo forest-state))
  (make-forest-state
   (component-reh port-mapping (isolated-vertex-components fo))
   (apply-port-mapping port-mapping (connected-components fo))))

(defun find-component-with (a components)
  (find-if (lambda (c)
	     (ports-member a (ports-of c)))
	   components))

(defgeneric produces-cycle-p (a b components)
  (:method (a b (components list))
    (or
     (and (oriented-p (edges (first components)))
	  (find-if (lambda (c) (ports-relation-member (list  b a) (edges c)))
		   components))
     (find-if (lambda (c)
		(ports-relation-member (list b a) (paths2 c)))
	      components)
     (let ((sharpa (sharp a components))
	   (sharpb (sharp b components)))
       (or
	(and (>= sharpa 2) (>= sharpb 2))
	(and
	 (= sharpa 1)
	 (>= sharpb 2)
	 (let ((ci (find-component-with a components)))
	   (find-if (lambda (c)
		      (and (not (eq c ci))
			   (ports-relation-member
			    (list b b)
			    (all-paths c))))
		    components)))
	(and
	 (= sharpb 1)
	 (>= sharpa 2)
	 (let ((ci (find-component-with b components)))
	   (find-if (lambda (c)
		      (and (not (eq c ci))
			   (ports-relation-member
			    (list a a)
			    (all-paths c))))
		    components))))))))

(defgeneric new-paths2-iv-cc (b isolated-vertex-component connected-component)
  (:method (b (iv isolated-vertex-component)
	    (c connected-component))
    (let ((a (port iv))
	  (to-add '()))
      (container-map
       (lambda (pair)
	 (let ((p1 (first pair))
	       (p2 (second pair)))
	   (when (= p1 b)
	     (pushnew (list a p2) to-add))
	   (when (= p2 b)
	     (pushnew (list a p1) to-add))))
       (pairs (all-paths c)))
      (make-ports-relation to-add (oriented-p (edges c))))))

(defgeneric new-paths2 (a b isolated-vertex-component1 isolated-vertex-component2)
  (:method (a b (iv1 isolated-vertex-component) (iv2 isolated-vertex-component))
    (make-empty-ports-relation (oriented-p iv1)))
  (:method (a b (c connected-component) (iv isolated-vertex-component))
    (new-paths2 a b iv c))
  (:method (a b (iv isolated-vertex-component) (c connected-component))
    (cond
      ((= a (port iv)) (new-paths2-iv-cc b iv c))
      ((= b (port iv)) (new-paths2-iv-cc a iv c))
      (t (make-empty-ports-relation (oriented-p (edges c))))))
  (:method (a b (c1 connected-component) (c2 connected-component))
    (let* ((ports1 (ports-of c1))
	   (ports2 (ports-of c2))
	   (ports (ports-union ports1 ports2))
	   (to-add '()))
      (when *debug* (assert (or (ports-member a ports) (ports-member b ports))))
      (container-map
       (lambda (pair1)
	 (container-map
	  (lambda (pair2)
	    (let ((p11 (first pair1))
		  (p12 (second pair1))
		  (p21 (first pair2))
		  (p22 (second pair2)))
	      (when (and (= a p11) (= b p21))
		(push (list p12 p22) to-add)
		(push (list p12 b) to-add)
		(push (list p22 a) to-add))
	      (when (and (= a p11) (= b p22))
		(push (list p12 p21) to-add)
		(push (list p12 b) to-add)
		(push (list p21 a) to-add))
	      (when (and (= a p12) (= b p21))
		(push (list p11 p22) to-add)
		(push (list p11 b) to-add)
		(push (list p22 a) to-add))
	      (when (and (= a p12) (= b p22))
		(push (list p11 p21) to-add)
		(push (list p11 b) to-add)
		(push (list p21 a) to-add))
	      (when (and (= b p11) (= a p21))
		(push (list p12 p22) to-add)
		(push (list p12 a) to-add)
		(push (list p22 b) to-add))
	      (when (and (= b p11) (= a p22))
		(push (list p12 p21) to-add)
		(push (list p12 a) to-add)
		(push (list p21 b) to-add))
	      (when (and (= b p12) (= a p21))
		(push (list p11 p22) to-add)
		(push (list p11 a) to-add)
		(push (list p22 b) to-add))
	      (when (and (= b p12) (= a p22))
		(push (list p11 p21) to-add) (push (list p11 a) to-add) (push (list p21 b) to-add))))
	  (pairs (all-paths c2))))
       (pairs (all-paths c1)))
      (make-ports-relation to-add (oriented-p (all-paths c1))))))

(defun next-for-add (a b components)
  (let ((new-components (add-to-components a b components)))
    (when *debug*
      (assert (not (produces-cycle-p a b components))))
    (multiple-value-bind (ivs cs)
	(split-list new-components #'isolated-vertex-p)
      (make-forest-state ivs cs))))

(defun simple-identity-case-p (a b components)
  (let ((allports (ports-of components)))
    (or (not (ports-member a allports))
	(not (ports-member b allports)))))

(defmethod graph-add-target (a b (fo forest-state))
  (let ((components (forest-components fo)))
    (if (simple-identity-case-p a b components)
	fo
	(unless (produces-cycle-p a b components)
	  (next-for-add a b components)))))

(defmethod graph-oadd-target (a b (fo forest-state))
  (graph-add-target a b fo))

(defmethod  graph-oplus-target ((fo1 forest-state) (fo2 forest-state))
  (make-forest-state
   (nconc
    (copy-list (isolated-vertex-components fo1))
    (copy-list (isolated-vertex-components fo2)))
   (nconc
    (copy-list (connected-components fo1))
    (copy-list (connected-components fo2)))))

(defmethod  graph-oplus-target-gen ((fo1 forest-state) (fo2 forest-state) states)
  (setq states (cons fo1 (cons fo2 states)))
  (make-forest-state
   (apply #'append (mapcar (lambda (state) (copy-list (isolated-vertex-components state))) states))
   (apply #'append (mapcar
		    (lambda (state) (copy-list (connected-components state)))
		    states))))

(defgeneric forest-transitions-fun (root arg)
  (:method ((root (eql *empty-symbol*)) (arg (eql nil)))
    (cwd-transitions-fun root arg #'forest-transitions-fun))
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (make-forest-state
     (list
      (make-isolated-vertex-component
       (port-of root) nil))
     '()))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'forest-transitions-fun)))

(defun forest-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd)
   (lambda (root states)
     (let ((*neutral-state-final-p* t))
       (forest-transitions-fun root states)))
   :name (cwd-automaton-name "FOREST" cwd)))

(defun table-forest-automaton (cwd)
  (compile-automaton (forest-automaton cwd)))

(defun subgraph-forest-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
   (nothing-to-xj (forest-automaton cwd) m j))

(defun table-subgraph-forest-automaton (m j cwd)
  (compile-automaton
   (subgraph-forest-automaton m j cwd)))

(defun forest-one-color-automaton (k color &optional (cwd 0))
  (assert (<= 1 color k))
   (nothing-to-color (forest-automaton cwd) k color))

(defun forest-color-automaton (k &optional (cwd 0))
  (nothing-to-colors-constants (forest-automaton cwd) k))

(defun test-forest-automaton (forest-automaton)
  (let* ((stable (cwd-term-stable 4))
	 (pn (cwd-term-pn 4))
	 (kn (cwd-term-kn 4))
	 (cn (cwd-term-cycle 4))
	 (term (input-cwd-term
		"add_a_d(oplus(add_c_d(oplus(c,d)), add_b_c(oplus(add_a_b(oplus(a,b)), c))))"))
	 (term1 (input-cwd-term 
		 "oplus(
                   oplus(b,add_c_d(oplus(c,d))),
                   add_b_c(oplus(oplus(b,add_c_d(oplus(c,d))),c)))"))
	 (term2 (input-cwd-term 
		 "oplus(oplus(add_b_c(oplus(b,add_c_d(oplus(c,d)))),add_b_c(oplus(b,add_c_d(oplus(c,d))))),c)"))
	 (c (complement-automaton forest-automaton)))
    (assert (recognized-p stable forest-automaton))
    (assert (recognized-p pn forest-automaton))
    (assert (not (recognized-p kn forest-automaton)))
    (assert (not (recognized-p cn forest-automaton)))
    (assert (recognized-p kn c))
    (assert (recognized-p cn c))
    (assert (recognized-p term forest-automaton))
    (assert (not (recognized-p
		  (cwd-term-unoriented-add 2 4 (cwd-term-oplus term (cwd-term-vertex 4)))
		  forest-automaton)))
    (assert (not (recognized-p pn c)))
    (assert (recognized-p
	     (cwd-term-unoriented-add
	      3 4
	      (cwd-term-oplus
	       (cwd-term-vertex 4)
	       (cwd-term-unoriented-add 0 1 (cwd-term-oplus (cwd-term-vertex 0) term1))))
	     forest-automaton))
    (assert (not (recognized-p
		  (cwd-term-unoriented-add
		   3 4
		   (cwd-term-oplus
		    (cwd-term-vertex 4) (cwd-term-unoriented-add 0 1 (cwd-term-oplus (cwd-term-vertex 0) term2))))
		  forest-automaton)))
    (assert
     (not (recognized-p
	   (input-cwd-term
	    "add_a_b(oplus(add_a_b(oplus(a[1],b[2])),oplus(oplus(a[3],a[4]),oplus(a[5],b[6]))))")
	   forest-automaton)))))

(defun test-forest () (test-forest-automaton (forest-automaton)))
