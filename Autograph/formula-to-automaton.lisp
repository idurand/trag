(in-package :autograph)

(defgeneric formula-to-automaton (f &key cwd orientation minimize))

(defun maybe-add-empty-symbols (s)
  (if (zerop (signature-nb-set-variables (signature s)))
    (add-empty-symbols s) s))

(defun renumber-vbits-1based-body (m pairs l mask vbits)
  (loop for k from 0 to (1- l)
        for new-vbits := (zero-vbits m)
        do (loop for p in pairs
                 for old := (elt vbits (1- (first p)))
                 when (find old '(1 t))
                 do (setf (elt new-vbits (1- (second p))) 1))
        do (loop for src-j upfrom 0
                 for dst-j in mask
                 when (logbitp src-j k)
                 do (setf (elt new-vbits (1- dst-j)) 1))
        collect new-vbits))

(defun renumber-vbits-1based (m pairs)
  (loop for p in pairs
        do (assert (<= 1 (second p) m)))
  (let* ((n (length pairs))
         (f (- m n))
         (l (ash 1 f))
         (mask (loop for k from 1 to m
                     unless (find k pairs :key 'second)
                     collect k)))
    (lambda (vbits)
      (renumber-vbits-1based-body m pairs l mask vbits))))

(defun variable-count (a)
  (let* ((signature (symbols:signature a)))
    (if signature (symbols:signature-nb-set-variables signature) 0)))

(defun hv-renumber (s m pairs)
  (symbols:hvmodification
    s
    (renumber-vbits-1based m pairs)
    (renumber-vbits-1based
      (variable-count s)
      (mapcar 'reverse pairs))
    m
    (loop for k from 1 to (variable-count s)
          unless (find k pairs :key 'first) return nil
          finally (return t))))

(defun variable-rename-renumbering (var1 var2)
  (loop for k upfrom 1
        for x in var1
        for pos := (position x var2 :test 'equal)
        when pos collect (list k (1+ pos))))

(defun hv-renumber-for-rename (s var1 var2)
  (hv-renumber 
    s (length var2) (variable-rename-renumbering var1 var2)))

(defun automaton-logic-formula-free-variables (f)
  (let* ((res nil))
    (logic-formula-walk 
      f 
      (lambda (&key formula bound-variables)
        (cond 
          ((and (typep formula 'logic-predicate-call)
                (equal (name formula) "assert")
                (= (length (arg formula)) 1)
                (typep (first (arg formula)) 'logic-constant))
           (loop for v in (formula:boolean-formula-variables
                            (formula:boolean-formula-with-names-from-string
                              (name (first (arg formula)))))
                 unless (find v bound-variables :test 'equal)
                 do (pushnew v res :test 'equal)))
          ((or (typep formula 'tptp-syntax::logic-predicate-call)
               (typep formula 'tptp-syntax::logic-comparison))
           (loop for v in (logic-formula-free-variables formula)
                 unless (find v bound-variables :test 'equal)
                 do (pushnew v res :test 'equal))))))
    (sort res 'string<)))

(defun automaton-logic-formula-predicates (f)
  (let* ((res nil))
    (logic-formula-walk
      f
      (lambda (&key formula)
        (when (typep formula 'logic-predicate-call)
          (pushnew (name formula) res :test 'equal))))
    res))

(defgeneric evaluate-automaton-argument (vbits value names)
  (:method (vbits (value logic-variable) names)
     (elt vbits (position (name value) names :test 'equal)))
  (:method (vbits (value logic-function-call) names)
     (let* ((name (name value)))
       (cond ((equal name "union")
              (loop for a in (arg value)
                    when (= (evaluate-automaton-argument vbits a names) 1)
                    return 1 finally (return 0)))
             ((equal name "intersection")
              (loop for a in (arg value)
                    when (= (evaluate-automaton-argument vbits a names) 0)
                    return 0 finally (return 1)))
             ((equal name "complement")
              (- 1 (evaluate-automaton-argument
                     vbits (first (arg value)) names)))
             ((equal name "difference")
              (if (= 0 (evaluate-automaton-argument
                         vbits (first (arg value)) names))
                0
                (loop for a in (rest (arg value))
                      when (= (evaluate-automaton-argument vbits a names) 1)
                      return 0 finally (return 1))))
             ((equal name "xor")
              (if 
                (oddp
                  (loop for a in (rest (arg value))
                        sum (evaluate-automaton-argument vbits a names)))
                1 0)))))
  (:method (vbits (value logic-constant) names)
    (declare (ignore vbits names))
    (let* ((name (name value)))
      (cond ((equal name "empty") 0)
            ((equal name "universal") 1)))))

(defun logic-function-calls-image (f vs)
  (lambda (vbits)
    (make-vbits
      (loop
        for a in (arg f)
        when (typep a '(or logic-variable logic-function-call logic-constant))
        collect (evaluate-automaton-argument vbits a vs)))))

(defun fill-automaton-argument-indices (m n func)
  (loop
    with direct := (make-array (make-list m :initial-element 2) :initial-element nil)
    with inverse := (make-array (make-list n :initial-element 2) :initial-element nil)
    for k from 0 to (1- (ash 1 m))
    for arg-vbits := (zero-vbits m)
    for val-vbits :=
    (progn
      (loop for j from 0 to (1- m) when (logbitp j k) do (setf (elt arg-vbits j) 1))
      (funcall func arg-vbits))
    do (push val-vbits (row-major-aref direct 
                                      (apply 'array-row-major-index 
                                             direct
                                             (coerce arg-vbits 'list))))
    do (push arg-vbits (row-major-aref inverse 
                                      (apply 'array-row-major-index 
                                             inverse
                                             (coerce val-vbits 'list))))
    finally (return (values direct inverse))))

(defun adjust-automaton-arguments (f a &key minimize reverse)
  (let* ((vars (automaton-logic-formula-free-variables f))
         (m (length vars))
         (n (length (remove-if (lambda (x)
                                 (not (typep x '(or logic-variable
                                                    logic-function-call
                                                    logic-constant))))
                               (arg f))))
         (argument-function (logic-function-calls-image f vars)))
    (multiple-value-bind (direct inverse) 
      (fill-automaton-argument-indices m n argument-function)
      (let*
        ((automaton 
           (if reverse
             (symbols:hvmodification
               a
               (lambda (vbits) (apply 'aref direct (coerce vbits 'list)))
               (lambda (vbits) (apply 'aref inverse (coerce vbits 'list)))
               n
               nil)
             (symbols:hvmodification
               a
               (lambda (vbits) (apply 'aref inverse (coerce vbits 'list)))
               (lambda (vbits) (apply 'aref direct (coerce vbits 'list)))
               m
               nil))))
        (if minimize
          (nminimize-automaton (compile-automaton automaton))
          automaton)))))

(defvar *formula-named-automata* nil)

(defpackage :autograph-formula-known-automata (:use))

(defun autograph-formula-known-automata::edge (cwd orientation x y)
  (declare (ignorable x y))
  (basic-arc-or-edge-automaton :orientation orientation :cwd cwd))

(defun autograph-formula-known-automata::stable (cwd orientation x)
  (declare (ignorable x orientation))
  (subgraph-stable-automaton 1 1 :cwd cwd))

(defun autograph-formula-known-automata::singleton (cwd orientation x)
  (declare (ignorable x orientation))
  (subgraph-singleton-automaton 1 1 :cwd cwd))

(defun autograph-formula-known-automata::connected (cwd orientation x)
  (declare (ignorable x orientation))
  (subgraph-connectedness-automaton 1 1 :cwd cwd))

(defun autograph-formula-known-automata::path (cwd orientation x y)
  (declare (ignorable x y))
  (basic-path-automaton :cwd cwd :orientation orientation))

(defun autograph-formula-known-automata::oripath (cwd orientation x y z)
  (declare (ignorable x y z))
  (assert (eq orientation t))
  (oripath-automaton :cwd cwd))

(defun autograph-formula-known-automata::hascycle (cwd orientation x)
  (declare (ignorable x))
  (if (equal orientation t)
    (subgraph-has-circuit-automaton 1 1 :cwd cwd)
    (subgraph-nr-has-cycle-automaton 1 1 cwd)))

(defun autograph-formula-known-automata::someedges (cwd orientation x y)
  (declare (ignorable x y))
  (if (equal orientation t)
    (link-ori-ee-decl-automaton :cwd cwd)
    (link-unori-ee-decl-automaton :cwd cwd)))

(defun autograph-formula-known-automata::alledges (cwd orientation x y)
  (declare (ignorable x y))
  (if (equal orientation t)
    (link-ori-aa-decl-automaton :cwd cwd)
    (link-unori-aa-decl-automaton :cwd cwd)))

(defun autograph-formula-known-automata::equal (cwd orientation &rest args)
  (vbits-fun-automaton (length args) (lambda (vbits) (apply '= (coerce vbits 'list)))
                       :cwd cwd :orientation orientation))

(defun autograph-formula-known-automata::inequal (cwd orientation &rest args)
  (vbits-fun-automaton (length args) (lambda (vbits) (not (apply '= (coerce vbits 'list))))
                       :cwd cwd :orientation orientation))

(defun autograph-formula-known-automata::subset (cwd orientation &rest args)
  (vbits-fun-automaton (length args) (lambda (vbits) (apply '<= (coerce vbits 'list)))
                       :cwd cwd :orientation orientation))

(defun autograph-formula-known-automata::empty (cwd orientation &rest args)
  (vbits-fun-automaton (length args) (lambda (vbits) (= 0 (count 1 vbits)))
                       :cwd cwd :orientation orientation))

(defun autograph-formula-known-automata::universal (cwd orientation &rest args)
  (vbits-fun-automaton (length args) (lambda (vbits) (= 0 (count 0 vbits)))
                       :cwd cwd :orientation orientation))

(defun autograph-formula-known-automata::disjoint (cwd orientation &rest args)
  (vbits-fun-automaton (length args) (lambda (vbits) (>= 1 (count 1 vbits)))
                       :cwd cwd :orientation orientation))

(defun autograph-formula-known-automata::assert (cwd orientation expression)
  (let* ((bf (formula:boolean-formula-from-string (name expression)))
         (m (length (formula:boolean-formula-variables bf))))
    (vbits-fun-automaton m (lambda (vbits) (formula:evaluate-indexed-boolean-formula bf vbits))
                         :cwd cwd :orientation orientation)))

(defun autograph-formula-known-automata::card (cwd orientation n x)
  (declare (ignorable x))
  (subgraph-cardinality-automaton
    n 1 1
    :orientation orientation :cwd cwd))

(defun autograph-formula-known-automata::card2 (cwd orientation x)
  (declare (ignorable x))
  (subgraph-cardinality-automaton
    2 1 1
    :orientation orientation :cwd cwd))

(defmethod formula-to-automaton
  ((f logic-predicate-call) &key (cwd 0) (orientation :unoriented) minimize)
  (let* ((name (name f))
         (predefined (find-symbol (string-upcase name) :autograph-formula-known-automata)))
    (cond
      (predefined
       (adjust-automaton-arguments
         f
         (apply predefined cwd orientation (arg f))
         :minimize minimize))
      ((and
         (find name *formula-named-automata* :test 'equal :key 'first)
         (every (lambda (x) (typep x '(or logic-variable logic-function-call)))
                (arg f)))
       (adjust-automaton-arguments
         f
         (second (find name *formula-named-automata* :test 'equal :key 'first))
         :minimize minimize))
      (t (error "Do not know what to do with a predicate call ~a" f)))))

(defmethod formula-to-automaton
  ((f logic-operation) &key (cwd 0) (orientation :unoriented) minimize)
  (let* ((name (name f))
         (variables (automaton-logic-formula-free-variables f))
         (arg-automata
           (loop for a in (arg f)
                 for vars := (automaton-logic-formula-free-variables a)
                 for aut := (formula-to-automaton a :cwd cwd :orientation orientation :minimize minimize)
                 for aut-renamed := (hv-renumber-for-rename aut vars variables)
                 collect aut-renamed)))
    (cond ((equal name "&") (termauto:intersection-automata arg-automata))
          ((equal name "|") (termauto:union-automata arg-automata))
          ((and (equal name "<=>")
                (= (length arg-automata) 2))
           (termauto:intersection-automaton
             (termauto:union-automaton
               (termauto:complement-automaton (first arg-automata)) (second arg-automata))
             (termauto:union-automaton
               (termauto:complement-automaton (second arg-automata)) (first arg-automata))))
          ((and (equal name "<~>")
                (= (length arg-automata) 2))
           (termauto:union-automaton
             (termauto:intersection-automaton
               (termauto:complement-automaton (first arg-automata)) (second arg-automata))
             (termauto:intersection-automaton
               (termauto:complement-automaton (second arg-automata)) (first arg-automata))))
          ((and (equal name "=>")
                (= (length arg-automata) 2))
           (termauto:union-automaton
             (termauto:complement-automaton (first arg-automata)) (second arg-automata)))
          ((and (equal name "<=")
                (= (length arg-automata) 2))
           (termauto:union-automaton
             (termauto:complement-automaton (second arg-automata)) (first arg-automata)))
          ((and (equal name "~")
                (= (length arg-automata) 1))
           (termauto:complement-automaton (first arg-automata)))
          (t (error "Do not know what to do with the operation ~a" f)))))

(defmethod formula-to-automaton
  ((f logic-quantification) &key (cwd 0) (orientation :unoriented) minimize)
  (let* ((name (name f))
         (variables (automaton-logic-formula-free-variables f))
         (subautomaton (formula-to-automaton (subformula f) :cwd cwd :orientation orientation :minimize minimize))
         (subvariables (sort (automaton-logic-formula-free-variables
                               (subformula f)) 'string<)))
    (cond ((equal name "?")
           (hv-renumber-for-rename subautomaton subvariables variables))
          ((equal name "!")
           (termauto:complement-automaton
             (hv-renumber-for-rename 
               (termauto:complement-automaton subautomaton)
               subvariables variables)))
          (t (error "Do not know what to do with the quantification ~a" f)))))

(defmethod formula-to-automaton
  ((f logic-comparison) &key (cwd 0) (orientation :unoriented) minimize)
  (cond
    ((equal (name f) "=")
     (formula-to-automaton
       (make-instance 'logic-predicate-call
                      :name "equal"
                      :arg (arg f))
       :cwd cwd :orientation orientation :minimize minimize))
    ((equal (name f) "!=")
     (complement-automaton
       (formula-to-automaton
         (make-instance 'logic-comparison
                        :name "="
                        :arg (arg f))
         :orientation orientation :cwd cwd :minimize minimize)))
    (t (error "Do not know what to do with the comparison ~a" f))))

(defmethod formula-to-automaton
  ((f string) &key (cwd 0) (orientation :unoriented) minimize)
  (formula-to-automaton (tptp-syntax:tptp-fof-string-to-formula-object f)
                        :orientation orientation :cwd cwd :minimize minimize))

(defmethod formula-to-automaton :around ((f logic-formula) &key (cwd 0) (orientation :unoriented) minimize)
  (declare (ignorable orientation))
  (let* ((automaton (call-next-method))
         (automaton (if minimize (minimize-automaton (compile-automaton automaton)) automaton)))
    (rename-object automaton (cwd-automaton-name (format nil "~a" f) cwd))))

(defparameter *multi-formula-peg*
  "
  Automaton_Name <- constant
  Automaton_Declaration <- Sp? Automaton_Name Sp? '(' Sp? fof_variable_list? Sp? ')' Sp? ':' Sp? fof_logic_formula Sp? '.'
  Automata_Block <- Automaton_Declaration+ Sp?
  ")

(let* ((*package* (find-package :tptp-bnf-syntax)))
  (esrap-peg:ast-eval (esrap-peg:basic-parse-peg *multi-formula-peg*)))

(defclass automaton-logic-definition (logic-formula named-mixin)
  ((arg :type list :accessor arg :initarg :arg)
   (subformula :type logic-formula :initarg :subformula :accessor subformula)))

(defmethod print-object ((object automaton-logic-definition) stream)
  (format stream "~a(~{~a~#[~:;,~]~}):" (name object) (arg object)))

(defun parse-multi-formula (s)
  (let* ((tptp-tree (tptp-syntax::tptp-normalise-tree
                      (esrap:parse 'tptp-bnf-syntax::automata_block s)))
         (args (if (symbolp (first (second tptp-tree)))
                 (rest tptp-tree) (second tptp-tree))))
    (mapcar 
      (lambda (definition)
        (let* ((content (second definition))
               (content (if (equal (elt content 2) #+nil"(" ")")
                          (append (subseq content 0 2)
                                  (list nil)
                                  (subseq content 2 nil))
                          content))
               (name (tptp-syntax::extract-ast-source (elt content 0)))
               (args (and (elt content 2)
                          (tptp-syntax::tptp-to-formula-object (elt content 2))))
               (subf (tptp-syntax::tptp-to-formula-object (elt content 5))))
          (make-instance 'automaton-logic-definition
                         :arg args :name name :subformula subf)))
      args)))

(defun automaton-defined (name)
  (or (find-symbol (string-upcase name) :autograph-formula-known-automata)
      (find name *formula-named-automata* :test 'equal :key 'first)))

(defun dependencies-satisfied (f)
  (every 'automaton-defined (automaton-logic-formula-predicates
                              (subformula f))))

(defun definition-formula-to-automaton (f &key minimize 
                                          (cwd 0) (orientation 0))
  (assert (null (set-difference
                  (automaton-logic-formula-free-variables (subformula f))
                  (mapcar 'name (arg f))
                  :test 'equal)))
  (adjust-automaton-arguments
    (make-instance 'logic-predicate-call :name (name f) :arg (arg f))
    (hv-renumber-for-rename
      (formula-to-automaton (subformula f)
                            :minimize minimize :cwd cwd 
                            :orientation orientation)
      (automaton-logic-formula-free-variables (subformula f))
      (sort (mapcar 'name (arg f)) 'string<))
    :reverse t :minimize minimize))

(defun multi-formula-to-main-automaton (fs &key minimize
                                           (cwd 0) (orientation :unoriented))
  (cond 
    ((stringp fs)
     (multi-formula-to-main-automaton (parse-multi-formula fs)
                                      :minimize minimize
                                      :cwd cwd :orientation orientation))
    ((null fs) (error "Nothing to define"))
    (t
      (multiple-value-bind
        (definable nondefinable) (split-list fs 'dependencies-satisfied)
        (cond ((null definable) (error "cyclical dependency: ~a" fs))
              ((and (null nondefinable) (> (length definable) 1))
               (error "multiple automata defined: ~a" fs))
              ((null nondefinable)
               (definition-formula-to-automaton
                 (first fs)
                 :minimize minimize :cwd cwd :orientation orientation))
              (t
                (let* ((*formula-named-automata*
                         (append
                           (loop for f in definable
                                 for name := (name f)
                                 collect 
                                 (list name
                                       (definition-formula-to-automaton
                                         f
                                         :minimize minimize
                                         :cwd cwd :orientation orientation)))
                           *formula-named-automata*)))
                  (multi-formula-to-main-automaton
                    nondefinable
                    :minimize minimize
                    :cwd cwd :orientation orientation))))))))
