(in-package :autograph)

(defparameter *fp9* "singleton(X3) & singleton(X4) & subset(X3, X1p) & subset(X3, X2) & subset(X4, X2) & edge(X3,X4) & ~subset(X4, X1p)")

(defparameter *fp8* (concatenate 'string "?[X3, X4]: " "(" *fp9* ")"))

(defparameter *fp7* (concatenate 'string "~" "(" *fp8* ")"))

(defparameter *fp6* (concatenate 'string "subset(X3, X1p) & ~subset(X4 ,X1p) & " *fp7*))

(defparameter *fp5* (concatenate 'string "?[X1p]: (" *fp6* ")"))

(defparameter *fp4* (concatenate 'string "~(" *fp5* ")"))

(defparameter *fp2* (concatenate 'string "singleton(X3) & singleton(X4) & subset(X3,X1) & subset(X4,X1) & ~(X3=X4) & card(2,X1) & " *fp4*))

(defparameter *fp1* (concatenate 'string "?[X3,X4]: (" *fp2* ")"))

(defparameter *fpath* (concatenate 'string "subset(X1,X2) & " *fp1*))

;(tptp-fof-string-to-formula-object *fpath*)

(signature (formula-to-automaton *fpath*))

;(compile-automaton (formula-to-automaton *fpath* :cwd 2))

#+nil
(equality-automaton
  (formula-to-automaton *fpath* :cwd 2 :orientation nil)
  (basic-path-automaton-from-formula :cwd 2 :orientation nil))

(formula-to-automaton
"card(2,X1) & subset(X1,X2) & (![C]: (subset(X1,C)|empty(intersection(X1,C))|(?[A,B]:(edge(A,B)&subset(A,C)&empty(intersection(B,C))))))")

(recognized-p
  (input-cwd-term "add_a_b(oplus(a^11,b^11))")
  (formula-to-automaton "card(2,X1) & subset(X1,X2) & (![C]: (subset(X1,C)|empty(intersection(X1,C))|(?[A,B]:(subset(union(A,B),X2)&edge(A,B)&subset(A,C)&empty(intersection(B,C))))))" :cwd 2))

(equality-automaton
  (table-basic-path-automaton-from-formula 2 :orientation nil)
  (formula-to-automaton
    "card(2,X1) & subset(X1,X2) & 
    (![C]: (subset(X1,C)|empty(intersection(X1,C))|
             (?[A,B]:(subset(union(A,B),X2)&edge(A,B)&
                       subset(A,C)&empty(intersection(B,C))))))"
    :cwd 2 :orientation nil))

(equality-automaton
  (table-basic-path-automaton-from-formula 2 :orientation nil)
  (formula-to-automaton
    "card(2,X1) & subset(X1,X2) & 
    (![C]: (subset(X1,C)|disjoint(X1,C)|
             (?[A,B]:(subset(union(A,B),X2)&edge(A,B)&
                       subset(A,C)&disjoint(B,C)))))"
    :cwd 2 :orientation nil :minimize t))

(recognized-p
  (input-cwd-term "add_a_b(oplus(a^11,b^11))")
  (formula-to-automaton *fpath* :cwd 2))

(recognized-p
  (input-cwd-term "add_a_b(oplus(a^11,b^11))")
  (formula-to-automaton "subset(X1,X2)&card(2,X1)&?[X,Y]:(singleton(X)&singleton(Y)&~(X=Y)&![X3]:(((subset(X,X3)) & (![U,V]:((subset(U,X3)&subset(U,X2)&subset(V,X2)&edge(U,V))=>subset(V,X3)))) => (subset(Y,X3))))" :cwd 2))
