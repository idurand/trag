(in-package :autograph)

(defun petal-term ()
 (cwd-term-unoriented-add 0 2
  (cwd-term-oplus
   (cwd-term-vertex 2)
   (cwd-term-unoriented-add 0 1
    (cwd-term-oplus
     (cwd-term-vertex 1)
     (cwd-term-ren 1 0
      (cwd-term-unoriented-add 0 1
       (cwd-term-oplus
	(cwd-term-vertex 0)
	(cwd-term-vertex 1)))))))))

(defun stage-petal-fn (equalizer)
 (let*
  (
   (a (cwd-term-ren 1 3 (cwd-term-ren 2 4  (funcall equalizer))))
   (b (cwd-term-ren 1 5 (cwd-term-ren 2 6  (funcall equalizer))))
   (c (cwd-term-ren 1 7 (cwd-term-ren 2 8  (funcall equalizer))))
   (d (cwd-term-ren 1 9 (cwd-term-ren 2 10 (funcall equalizer))))
   (all (cwd-term-oplus (cwd-term-oplus a b) (cwd-term-oplus c d)))
   (linked 
    (loop
     for edge in '(nil (3 6) (4 7) (5 8) (5 9) (8 10))
     for res := all then (cwd-term-unoriented-add (first edge) (second edge) res)
     finally (return res)
    ))
   (renamed
    (loop
     for edge in '(nil (3 1) (10 2) (4 0) (5 0) (6 0) (7 0) (8 0) (9 0))
     for res := linked then (cwd-term-ren (first edge) (second edge) res)
     finally (return res)
    ))
  )
  renamed))

(defun stage-petal-k (n)
  (loop
     for k from 0 to n
     for res := #'petal-term
     then (let ((res res)) (lambda () (stage-petal-fn res)))
     finally (return (funcall res))))

(defun large-clique (n)
 (loop
  for k from 1 to n
  for res := (cwd-term-vertex 1)
  then
  (cwd-term-unoriented-add 1 2
   (cwd-term-oplus
    (cwd-term-vertex 1)
    (cwd-term-ren 1 2 res)))
  finally (return res)))

(defun clique-based-equalizer (n)
 (cwd-term-ren 3 2
  (cwd-term-ren 2 0
   (cwd-term-unoriented-add 2 3
    (cwd-term-unoriented-add 1 2
     (cwd-term-oplus
      (cwd-term-ren 1 3
       (large-clique n))
      (large-clique n)))))))
   
