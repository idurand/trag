(in-package :autograph)

;; 2*k colours
;; k two-colour columns
;; 2*h rows

(defun single-coloring-grid-edges (k h &key permutation)
  "List the edges of the single-colouring [up to permutations] grid.
  There are k columns and 2h rows.
  Nodes are connected with an edge if they are from different columns,
  or if they are from the same column and adjacent vertically.
  "
  (labels
      ((node-index-raw (i j) (+ i (* (1- j) k)))
       (node-index (i j)
	 (if permutation
	     (elt permutation
		  (1- (node-index-raw i j)))
	     (node-index-raw i j)))
       (straw-edges (i) 
	 (loop 
	   for j from 1 to (1- (* 2 h))
	   collect (list (node-index i j) 
			 (node-index i (1+ j)))))
       (cross-straw-edges (i1 i2)
	 (loop
	   for j1 from 1 to (* 2 h)
	   append
	   (loop
	     for j2 from 1 to (* 2 h)
	     collect
	     (list
	      (node-index i1 j1)
	      (node-index i2 j2)))))
       (cross-straw-edges-from (i1)
	 (loop
	   for i2 from (1+ i1) to k
	   append (cross-straw-edges i1 i2))))
    (loop 
      for i from 1 to k 
      append (straw-edges i)
      append (cross-straw-edges-from i))))

(defun single-coloring-grid-graph (k h &key permutation)
  (graph:graph-from-paths
   (single-coloring-grid-edges k h :permutation permutation)
   :oriented nil))

(defun single-coloring-grid-term-sequential (k h)
  (labels
      ((grow-straw
	   (straw)
	 (cwd-term-ren
	  3 2
	  (cwd-term-ren
           2 1
           (cwd-term-unoriented-add
	    2 3
	    (cwd-term-oplus
	     (cwd-term-vertex 3)
	     straw)))))
       (straw (l) (loop
		    for i from 1 to l
		    for res := (cwd-term-vertex 2)
		      then (grow-straw res)
		    finally (return (cwd-term-ren 2 1 res))))
       (kill-straw (straw) (cwd-term-ren 1 0 straw))
       (add-straw (straw tail)
	 (cwd-term-ren
	  1 0
	  (cwd-term-unoriented-add
	   0 1
	   (cwd-term-oplus straw tail))))
       )
    (loop
      with straw := (straw (* 2 h))
      for i from 1 to k
      for haystack := (kill-straw straw) then
					 (add-straw straw haystack)
      finally (return haystack))))

(defun single-coloring-grid-term-sequential-optimal (k h)
  (labels
      ((grow-straw
	   (straw)
	 (cwd-term-ren
	  0 1
	  (cwd-term-ren
           1 2
           (cwd-term-unoriented-add
	    0 1
	    (cwd-term-oplus straw (cwd-term-vertex 0))))))
       (cool-straw (straw) (cwd-term-ren 1 2 straw))
       (heat-straw (straw) (cwd-term-ren 2 1 straw))
       (add-straw (hay straw)
	 (cool-straw (cwd-term-unoriented-add 
		      1 2 (cwd-term-oplus hay straw)))))
    (loop
      with straw := (loop for j from 1 to (* 2 h)
                          for res := (cwd-term-vertex 1)
			    then (grow-straw res)
                          finally (return res))
      for j from 1 to k
      for res := (cool-straw straw)
	then (add-straw res (heat-straw straw))
      finally (return res))))

(defun single-coloring-grid-term-sequential-optimal-balanced
    (k h &optional (hot 0) (cool 1) (stale 2))
  (cond
    ((= k 0) nil)
    ((= k 1)
     (loop
       for j from 1 to (* 2 h)
       for res := (cwd-term-vertex cool)
	 then 
	 (cwd-term-ren
	  hot cool
	  (cwd-term-ren
           cool stale
           (cwd-term-unoriented-add
	    hot cool
	    (cwd-term-oplus res (cwd-term-vertex hot)))))
       finally (return (cwd-term-ren cool stale res))))
    (t (let*
	   (
	    (half-down (truncate k 2))
	    (half-up (- k half-down))
	    (smaller-half
	      (single-coloring-grid-term-sequential-optimal-balanced
	       half-down h hot cool stale))
	    (larger-half
	      (single-coloring-grid-term-sequential-optimal-balanced
	       half-up h hot stale cool))
	    )
         (cwd-term-ren
	  cool stale
	  (cwd-term-unoriented-add
	   cool stale
	   (cwd-term-oplus larger-half smaller-half)))))))


(defun single-coloring-grid-term-parallel (k h)
  (labels
      (
       (grow-line 
	   (num line)
	 (loop
	   for i from num downto 0
	   for res :=
		   (cwd-term-oplus
		    (cwd-term-vertex i)
		    line)
	     then (cwd-term-unoriented-add i num res)
	   finally (return res)))
       (line (l)
	 (loop 
	   for i from 0 to (1- l)
	   for res := (cwd-term-vertex i) then (grow-line i res)
	   finally (return res)))
       (connect-at
	   (i stack)
	 (loop
	   for j from k downto 0
	   for res := stack then
			    (cwd-term-unoriented-add
			     j (+ i k)
			     (if (= i j) res
				 (cwd-term-unoriented-add
				  j (+ i (* 2 k))
				  res)))
	   finally (return res)))
       (grow-stack
	   (line stack)
	 (loop
	   for i from k downto 0
	   for res := (cwd-term-oplus line stack)
	     then (connect-at i res)
	   finally (return res)))
       (cool-line (stack)
	 (loop
	   for i from k downto 0
	   for res := stack then
			    (cwd-term-ren
			     i (+ i k)
			     (cwd-term-ren
			      (+ i k) (+ i (* 2 k))
			      res))
	   finally (return res)))
       )
    (loop
      with line := (line k)
      for j from 1 to (* 2 h)
      for res := (cool-line line) then (cool-line (grow-stack line res))
      finally (return (cool-line res)))))

(defun single-coloring-grid-term-parallel-eager-edges (k h)
  (labels
      (
       (add-horizontals
	   (chunk c)
	 (loop
	   for j from c downto 0
	   for res := chunk
	     then (cwd-term-unoriented-add j c res)
	   finally (return res)))
       (add-short-connections 
	   (chunk c)
	 (loop
	   for j from (* 2 k) downto k
	   for res := chunk then (cwd-term-unoriented-add c j res)
	   finally (return res)))
       (add-long-connections
	   (chunk c)
	 (loop
	   for j from (* 3 k) downto (* 2 k)
	   for res := chunk
	     then (if (= j (+ (* 2 k) c)) res
		      (cwd-term-unoriented-add c j res))
	   finally (return res)))
       (add-edges
	   (chunk c)
	 (add-long-connections
	  (add-short-connections
           (add-horizontals
	    chunk c) c) c))
       (add-point 
	   (chunk c)
	 (add-edges (cwd-term-oplus (cwd-term-vertex c) chunk) c))
       (cool-line
	   (chunk)
	 (loop 
	   for i from (* 2 k) downto 0
	   for res := chunk then
			    (cwd-term-ren i (+ i k) res)
	   finally (return res)))
       )
    (loop
      for i from 1 to (* 2 h)
      for chunk := (loop
                     for j from 0 to (1- k)
                     for res := (cwd-term-vertex j)
		       then (add-point res j)
                     finally (return (cool-line res)))
	then (loop
	       for j from -1 to (1- k)
	       for res := chunk
		 then (add-point res j)
	       finally (return (cool-line res)))
      finally (return (cool-line chunk)))))

(defun single-coloring-grid-term-sequential-eager-edges (k h)
  (labels
      (
       (add-point
	   (chunk)
	 (cwd-term-ren
	  0 1
	  (cwd-term-ren
           1 2
           (cwd-term-unoriented-add 
	    0 3
	    (cwd-term-unoriented-add
	     0 1
	     (cwd-term-oplus
	      (cwd-term-vertex 0)
	      chunk))))))
       (cool-line
	   (chunk)
	 (cwd-term-ren 2 3 (cwd-term-ren 1 2 chunk)))
       )
    (loop
      for i from 1 to k
      for chunk :=
		(loop
		  for j from 1 to (* 2 h)
		  for res := (cwd-term-vertex 1)
		    then (add-point res)
		  finally (return (cool-line res)))
	then
	(loop
	  for j from 0 to (* 2 h)
	  for res := chunk
	    then (add-point res)
	  finally (return (cool-line res)))
      finally (return chunk))))
