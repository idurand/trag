;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)
;;; Arc2(X1,X2) <=> X1 \= X2 et \exists X3 Arc(X1,X3) et Arc(X3,X2)
;;; est-ce X1 et X2 doivent ^etre des singletons: il semble que oui
;;; oriented graphs only pas fe fleche aller retour a <=> b

(defvar *orientation1*)
(defvar *orientation2*)

(defclass 2arc-state (graph-state)
  ((port1 :initform nil :reader port1 :initarg :port1)
   (port2 :initform nil :reader port2 :initarg :port2)
   (from-x :reader from-x :type ports :initarg :from-x)
   (to-y :reader to-y :type ports :initarg :to-y)
   (others :reader others :type ports :initarg :others)))

(defmethod ports-of ((state 2arc-state))
  (let ((ports
	  (reduce #'ports-union
		  (list (from-x state) (to-y state) (others state))))
	(port1 (port1 state))
	(port2 (port2 state)))
    (when port1
      (setf ports (container-adjoin port1 ports)))
    (when port2
      (setf ports (container-adjoin port2 ports)))
    ports))

(defun 2arc-state-p (state)
  (typep state '2arc-state))

(defun oport (a pair orientation)
  (if (plusp orientation)
      a
      (if (= a (car pair))
	  (second pair)
	  (car pair))))

(defun o1 (a pair) (oport a pair *orientation1*))

(defun o2 (a pair) (oport a pair *orientation2*))

(defmethod print-object ((state 2arc-state) stream)
  (let ((port1 (port1 state))
	(port2 (port2 state))
	(from-x (from-x state))
	(to-y (to-y state))
	(others (others state)))
    (cond
      ((and (null port1) (null port2))
       (format stream "<~A>" (others state)))
      ((and port1 (null port2))
       (format stream "<1,~A,~A,~A>" (port-to-string port1) from-x others))
      ((and port2 (null port1))
       (format stream "<2,~A,~A,~A>"
	       (port-to-string port2) to-y others))
      (t (format stream "<~A,~A,~A,~A,~A>"
		 (port-to-string port1)
		 (port-to-string port2) from-x to-y others)))))

(defmethod compare-object ((state1 2arc-state) (state2 2arc-state))
  (and
   (= (port1 state1) (port1 state2))
   (= (port2 state1) (port2 state2))
   (compare-object (from-x state1) (from-x state2))
   (compare-object (to-y state1) (to-y state2))
   (compare-object (others state1) (others state2))))

(defgeneric make-2arc-state (port1 port2 from-x to-y others)
  (:method (port1 port2 (from-x ports) (to-y ports) (others ports))
    (make-instance '2arc-state
		   :port1 port1 :port2 port2 :to-y to-y
		   :from-x from-x :others others)))

;; (defmethod state-final-p ((so 2arc-state)) nil)

(defmethod graph-ren-target (a b (state 2arc-state))
  (make-2arc-state
   (subst b a (port1 state))
   (subst b a (port2 state))
   (ports-subst b a (from-x state))
   (ports-subst b a (to-y state))
   (ports-subst b a (others state))))

(defmethod graph-add-target (a b (state 2arc-state))
  (let ((ports (ports-of state)))
    (unless (and (ports-member a ports) (ports-member b ports))
      (return-from graph-add-target state)))
  (let ((port1 (port1 state))
	(port2 (port2 state))
	(from-x (from-x state))
	(to-y (to-y state))
	(others (others state))
	(o1a (o1 a (list a b)))
	(o2a (o2 a (list a b)))
	(o1b (o1 b (list a b)))
	(o2b (o2 b (list a b))))
    (cond
      ;; 0
      ((and (null port1) (null port2))
       state)
      ;; 3
      ((and port1 port2)
       (cond ((or (and (= o2b port2) (ports-member o2a from-x))
		  (and (= o1a port1) (ports-member o1b to-y))
		  (and (= o1a port1 o2b port2) (= o1b o2a) (ports-member o2a others))) ;; this case does not occur with o1=o2=1!
	      *ok-state*)
	     ((and (= o1a port1)
		   (/= o1b port2) (ports-member o1b others))
	      (make-2arc-state
	       port1 port2 (container-adjoin o1b from-x) to-y (container-remove o1a others)))
	     ((and (= o2b port2)
		   (/= o2a port1) (ports-member o2a others))
	      (make-2arc-state
	       port1 port2 from-x (ports-adjoin o2a to-y)
	       (ports-remove o2a others)))
	     ((and (= o1a port1 o2a) (= o1b o2b port2))
	      (assert (not (ports-member o1a from-x)))
	      (assert (not (ports-member o1b to-y)))
	      (make-2arc-state
	       port1 port2 (ports-adjoin o1b from-x) (ports-adjoin o1a to-y)
	       (ports-remove o1b (ports-remove o1a others))))
	     ((and (= port1 o2a o1b) (= port2 o1a o2b))
	      (make-2arc-state
	       port1 port2 from-x (ports-adjoin o1b to-y)
	       (ports-remove o1b others)))
	     ((and (= port1 o1a o2b) (= port2 o1b o2a))
	      (make-2arc-state
	       port1 port2 (ports-adjoin o1b from-x) to-y
	       (ports-remove o1b others)))
	     (t state)))
      ;; 1 ou 2
      (port1
       (if (and (= o1a port1) (ports-member o1b others))
	   (make-2arc-state
	    port1 port2
	    (ports-adjoin o1b from-x) to-y (ports-remove o1b others))
	   state))
      (t ;; port2
       (if (and (= o2b port2) (ports-member o2a others))
	   (make-2arc-state
	    port1 port2
	    from-x (ports-adjoin o2a to-y) (ports-remove o2a others))
	   state)))))

(defmethod graph-oadd-target (a b (state 2arc-state))
  (graph-add-target a b state))

(defmethod graph-oplus-target ((state1 2arc-state) (state2 ok-state))
  (when (and (null (port1 state1)) (null (port2 state1)))
    state2))

(defmethod graph-oplus-target ((state1 ok-state) (state2 2arc-state))
  (when (and (null (port1 state2)) (null (port2 state2)))
    state1))

(defmethod graph-oplus-target ((state1 2arc-state) (state2 2arc-state))
  (let ((port1-1 (port1 state1))
	(port1-2 (port1 state2))
	(port2-1 (port2 state1))
	(port2-2 (port2 state2))
	(from-x-1 (from-x state1))
	(from-x-2 (from-x state2))
	(to-y-1 (to-y state1))
	(to-y-2 (to-y state2))
	(others (ports-union (others state1) (others state2))))
    (cond
      ;; 0-0 1-0 2-0 3-0
      ((or
	(every #'null (list port1-1 port2-1 port1-2 port2-2))
	(and (null port1-2) (null port2-2)))
       (make-2arc-state
	port1-1 port2-1 from-x-1 to-y-1 others))
      ;; 0-1 0-2 0-3
      ((and (null port1-1) (null port2-1))
       (make-2arc-state
	port1-2 port2-2 from-x-2 to-y-2 others))
      ;; 1-2
      ((and port1-1 (null port2-1) (null port1-2) port2-2)
       (make-2arc-state
	port1-1 port2-2 from-x-1 to-y-2 others))
      ;; 2-1
      ((and (null port1-1) port2-1 port1-2 (null port2-2))
       (make-2arc-state
	port1-2 port2-1 from-x-2 to-y-1 others))
      )))
;;      (t (warn "+(~A,~A) -> Error" state1 state2)))))

(defgeneric 2arc-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg (eql nil)))
    (let* ((port (port-of root))
	   (vbits (vbits root))
	   (vb1 (aref vbits 0))
	   (vb2 (aref vbits 1)))
      (make-2arc-state
       (and (not (zerop vb1)) port)
       (and (not (zerop vb2)) port)
       (make-empty-ports)
       (make-empty-ports)
       (make-ports (and (zerop vb1) (zerop vb2) (list port))))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'2arc-transitions-fun)))

(defun 2arc-name (o1 o2 cwd)
  (cwd-automaton-name (format nil "2ARC-(~A,~A)(X1,X2)" o1 o2) cwd))

(defun basic-2arc-automaton (o1 o2 &key (cwd 0)) ;; o1 o2 1 or -1
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2 :orientation t)
   (lambda (root states)
   (let ((*orientation1* o1)
	 (*orientation2* o2))
       (2arc-transitions-fun root states)))
   :name (2arc-name o1 o2 cwd)))

(defun 2arc-automaton (m j1 j2 o1 o2 &key (cwd 0))
   (y1-y2-to-xj1-xj2 (basic-2arc-automaton o1 o2 :cwd cwd) m j1 j2))

(defun 2arc-automaton-from-formula (m j1 j2 o1 o2 &key (cwd 0))
  (let ((*oriented* t)
	(mp (1+ m)))
    (rename-object
     (vbits-projection
      (intersection-automaton-compatible-gen
       (if (plusp o1)
	   (arc-automaton mp j1 mp :cwd cwd)
	   (arc-automaton mp mp j1 :cwd cwd))
       (if (plusp o2)
	   (arc-automaton mp mp j2 :cwd cwd)
	   (arc-automaton mp j2 mp :cwd cwd))
       (complement-automaton (equal-automaton mp j1 mp :cwd cwd))
       (complement-automaton (equal-automaton mp j2 mp :cwd cwd)))
      (set-iota m))
     (2arc-name o1 o2 cwd))))

(defun basic-2arc-automaton-from-formula (o1 o2 &key (cwd 0))
  (2arc-automaton-from-formula 2 1 2 o1 o2 :cwd cwd))

(defun basic-all2arc-automaton (&key (cwd 0))
  (intersection-automaton-compatible-gen
   (basic-2arc-automaton 1 1 :cwd cwd)
   (basic-2arc-automaton 1 -1 :cwd cwd)
   (basic-2arc-automaton -1 1 :cwd cwd)
   (basic-2arc-automaton -1 -1 :cwd cwd)))

(defun basic-all2arc-automaton-from-formula (&key (cwd 0))
  (intersection-automaton-compatible-gen
   (basic-2arc-automaton-from-formula 1 1 :cwd cwd)
   (basic-2arc-automaton-from-formula 1 -1 :cwd cwd)
   (basic-2arc-automaton-from-formula -1 1 :cwd cwd)
   (basic-2arc-automaton-from-formula -1 -1 :cwd cwd)))

(defun ok3vbits ()
  (input-cwd-term "add_b->a(add_a->b(oplus(ren_b_a(add_b->a(add_a->b(oplus(a^00,b^01)))),b^10)))"))


;; AUTOGRAPH> (compute-target (ok3vbits) (basic-2arc-automaton 1 -1 3))
;; !Ok
;; AUTOGRAPH> (compute-target (ok3vbits) (basic-2arc-automaton -1 1 3))
;; !<b,a,{a},{ab},{}>
;; AUTOGRAPH> (compute-target (ok3vbits) (basic-2arc-automaton -1 -1 3))
;; !Ok
;; AUTOGRAPH> (compute-target (ok3vbits) (basic-2arc-automaton 1 1 3))
;; !Ok

(defun 2arc-test-term ()
  (cwd-term-oriented-add
   1 0
   (cwd-term-oriented-add
    0 1
    (cwd-term-multi-oplus (cwd-term-vertex 0 '(0 1)) (cwd-term-vertex 1 '(1 0)) (cwd-term-vertex 2 '(0 0))))))

