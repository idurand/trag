;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)
;;; Edge2(X1,X2) <=> \exists X3 Edge(X1,X3) et Edge(X3,X2)  (et X1 \= X2 supprim'e)
;;; est-ce X1 et X2 doivent ^etre des singletons

(defun 2arc-or-edge-automaton-from-formula (m j1 j2 &key (cwd 0) (orientation :unoriented))
  (let ((mp (1+ m)))
    (rename-object
     (vbits-projection
      (intersection-automaton-compatible-gen
       (arc-or-edge-automaton mp j1 mp :orientation orientation :cwd  cwd)
       (arc-or-edge-automaton mp mp j2 :orientation orientation :cwd cwd)
       (complement-automaton (equal-automaton mp j1 mp :cwd cwd :orientation nil))
       (complement-automaton (equal-automaton mp j2 mp :cwd cwd :orientation nil)))
      (set-iota m))
     (cwd-automaton-name 
      (format nil "2~A(~A,~A)" (name-according-orientation orientation) j1 j2) cwd))))

(defun 2edge-automaton-from-formula (m j1 j2 &key (cwd 0))
  (2arc-or-edge-automaton-from-formula m j1 j2 :cwd cwd :orientation nil))

(defun basic-2edge-automaton-from-formula (&key (cwd 0))
  (2edge-automaton-from-formula 2 1 2 :cwd cwd))

(defun 2edge-test-term (n)
  (cwd-term-kn-vbits n '((1 0) (0 0) (0 1))))
