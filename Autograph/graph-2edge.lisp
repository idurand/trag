;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)
;;; Edge2(X1,X2) <=> X1 \= X2 et \exists X3 Edge(X1,X3) et Edge(X3,X2)
;;; est-ce X1 et X2 doivent ^etre des singletons: il semble que oui

(defclass 2edge-state (graph-state)
  ((port1 :initform nil :reader port1 :initarg :port1)
   (port2 :initform nil :reader port2 :initarg :port2)
   (from-x :reader from-x :type ports :initarg :from-x)
   (to-y :reader to-y :type ports :initarg :to-y)
   (others :reader others :type ports :initarg :others)))

(defmethod ports-of ((state 2edge-state))
  (let ((ports
	 (reduce #'ports-union
		 (list (from-x state) (to-y state) (others state))))
	(port1 (port1 state))
	(port2 (port2 state)))
    (when port1
      (setf ports (container-adjoin port1 ports)))
    (when port2
      (setf ports (container-adjoin port2 ports)))
    ports))
    
(defun 2edge-state-p (state)
  (typep state '2edge-state))

(defmethod print-object ((state 2edge-state) stream)
  (let ((port1 (port1 state))
	(port2 (port2 state))
	(from-x (from-x state))
	(to-y (to-y state))
	(others (others state)))
    (cond
      ((and (null port1) (null port2))
       (format stream "<~A>" (others state)))
      ((and port1 (null port2))
       (format stream "<1,~A,~A,~A>" (port-to-string port1) from-x others))
      ((and port2 (null port1))
             (format stream "<2,~A,~A,~A>"
		     (port-to-string port2) to-y others))
      (t (format stream "<~A,~A,~A,~A,~A>"
		 (port-to-string port1)
		 (port-to-string port2) from-x to-y others)))))

(defmethod compare-object ((state1 2edge-state) (state2 2edge-state))
    (and
     (= (port1 state1) (port1 state2))
     (= (port2 state1) (port2 state2))
     (compare-object (from-x state1) (from-x state2))
     (compare-object (to-y state1) (to-y state2))
     (compare-object (others state1) (others state2))))

(defgeneric make-2edge-state (port1 port2 from-x to-y others)
  (:method (port1 port2 (from-x ports) (to-y ports) (others ports))
    (make-instance '2edge-state
		   :port1 port1 :port2 port2 :to-y to-y
		   :from-x from-x :others others)))

(defmethod graph-ren-target (a b (state 2edge-state))
  (make-2edge-state
   (subst b a (port1 state))
   (subst b a (port2 state))
   (ports-subst b a (from-x state))
   (ports-subst b a (to-y state))
   (ports-subst b a (others state))))

(defmethod graph-add-target (a b (state 2edge-state))
  (let ((ports (ports-of state)))
    (unless (and (ports-member a ports) (ports-member b ports))
      (return-from graph-add-target state)))
  (let ((port1 (port1 state))
	(port2 (port2 state))
	(from-x (from-x state))
	(to-y (to-y state))
	(others (others state)))
    (cond
      ;; 0
      ((and (null port1) (null port2))
       state)
      ;; 3
      ((and port1 port2)
       (cond ((or (and (= b port2) (ports-member a from-x))
		  (and (= a port1) (ports-member b to-y)))
	      *ok-state*)
	     ((and (= a port1) (/= b port2) (ports-member b others))
	      (make-2edge-state
	       port1 port2 (container-adjoin b from-x) to-y (container-remove a others)))
	     ((and (= b port2) (/= a port1) (ports-member a others))
	      (make-2edge-state
	       port1 port2 from-x (container-adjoin a to-y) (container-remove a others)))
	     ((and (= a port1) (= b port2))
	      (assert (not (ports-member a from-x)))
	      (assert (not (ports-member b to-y)))
	      (make-2edge-state
	       port1 port2 (container-adjoin b from-x) (container-adjoin a to-y)
	       (container-remove b (container-remove a others))))
	     (t state)))
      ;; 1 ou 2
      (port1
       (if (and (= a port1) (ports-member b others))
	   (make-2edge-state
	    port1 port2
	    (container-adjoin b from-x) to-y (container-remove b others))
	   state))
      (t ;; port2
       (if (and (= b port2) (ports-member a others))
	   (make-2edge-state
	    port1 port2
	    from-x (container-adjoin a to-y) (container-remove a others))
	   state)))))
;;      (t (warn "add-target 2edge-state") state))))

(defmethod graph-oplus-target ((state1 2edge-state) (state2 ok-state))
  (when (and (null (port1 state1)) (null (port2 state1)))
    state2))

(defmethod graph-oplus-target ((state1 ok-state) (state2 2edge-state))
  (when (and (null (port1 state2)) (null (port2 state2)))
    state1))

(defmethod graph-oplus-target ((state1 2edge-state) (state2 2edge-state))
  (let ((port1-1 (port1 state1))
	(port1-2 (port1 state2))
	(port2-1 (port2 state1))
	(port2-2 (port2 state2))
	(from-x-1 (from-x state1))
	(from-x-2 (from-x state2))
	(to-y-1 (to-y state1))
	(to-y-2 (to-y state2))
	(others (ports-union (others state1) (others state2))))
    (cond
      ;; 0-0 1-0 2-0 3-0
      ((or
	(every #'null (list port1-1 port2-1 port1-2 port2-2))
	(and (null port1-2) (null port2-2)))
       (make-2edge-state
	port1-1 port2-1 from-x-1 to-y-1 others))
      ;; 0-1 0-2 0-3
      ((and (null port1-1) (null port2-1))
       (make-2edge-state
	port1-2 port2-2 from-x-2 to-y-2 others))
      ;; 1-2
      ((and port1-1 (null port2-1) (null port1-2) port2-2)
       (make-2edge-state
	port1-1 port2-2 from-x-1 to-y-2 others))
      ;; 2-1
      ((and (null port1-1) port2-1 port1-2 (null port2-2))
       (make-2edge-state
	port1-2 port2-1 from-x-2 to-y-1 others)))))

(defgeneric 2edge-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg (eql nil)))
    (let* ((port (port-of root))
	   (vbits (vbits root))
	   (vb1 (aref vbits 0))
	   (vb2 (aref vbits 1)))
      (make-2edge-state
       (and (not (zerop vb1)) port)
       (and (not (zerop vb2)) port)
       (make-empty-ports)
       (make-empty-ports)
       (make-ports (and (zerop vb1) (zerop vb2) (list port)))))))

(defmethod 2edge-transitions-fun ((root abstract-symbol) (arg list))
  (cwd-transitions-fun root arg #'2edge-transitions-fun))

(defun 2edge-name (cwd) (cwd-automaton-name "2EDGE(X1,X2)" cwd))

(defun basic-2edge-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2)
   #'2edge-transitions-fun
   :name (2edge-name cwd)))

(defun 2edge-automaton (m j1 j2 &optional (cwd 0))
  (y1-y2-to-xj1-xj2 (basic-2edge-automaton cwd) m j1 j2))
