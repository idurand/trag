;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun 2stable-automaton (&optional (cwd 0))
  (rename-object
   (intersection-automaton-compatible
    (cardinality-automaton 2 :cwd cwd) (stable-automaton :cwd cwd))
   (cwd-automaton-name "2STABLE" cwd)))
 
(defun table-2stable-automaton (cwd)
  (compile-automaton (2stable-automaton cwd)))

(defun subgraph-2stable-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
  (nothing-to-xj (2stable-automaton cwd) m j))

(defun color-2stable-automaton (k color &optional (cwd 0))
  (assert (<= 1 color k))
  (nothing-to-color (2stable-automaton cwd) k color))

(defun table-subgraph-2stable-automaton (m j cwd)
  (compile-automaton (subgraph-2stable-automaton m j cwd)))

(defun table-color-2stable-automaton (k color cwd)
  (compile-automaton (color-2stable-automaton k color cwd)))
