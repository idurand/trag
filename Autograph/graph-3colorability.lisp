;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun 3-colorability-automaton (&optional (cwd 0))
  (rename-object
   (constants-color-projection (coloring-automaton 3 cwd))
   (cwd-automaton-name
    (format nil "~A-colorability" 3) cwd)))


