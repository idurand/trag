;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defvar *forest-automaton*)
(setq *forest-automaton* #'forest-automaton)

(defun nocycle-pairs (m)
  (distinct-pairs (set-iota m)))

(defun acyclic-coloring-spec (k cwd)
  (let* ((stable-automata
	  (mapcar (lambda (color)
		    (one-color-stable-automaton k color :cwd cwd :orientation nil))
		  (color-iota k)))
	 (forest (funcall *forest-automaton* cwd))
	 (nocycle-automata
	  (mapcar
	   (lambda (pair)
	     (rename-object
	      (nothing-to-c1-union-c2 forest k (first pair) (second pair))
	      (format nil
		      "~A-FOREST-C~A-U-C~A-~A"
		      cwd
		      (first pair) (second pair) k)))
	   (nocycle-pairs k)))
	 (proper (color-proper-partition-automaton k :cwd cwd :orientation nil))
	 (tautomata ()))
    (push (make-tautomaton "Stables" stable-automata) tautomata)
    (push (make-tautomaton "Forests" nocycle-automata) tautomata)
    (make-graph-spec cwd 'graph-spec
		     (cwd-color-signature cwd k :orientation nil)
		     (list proper)
		      tautomata)))

(defun load-acyclic-coloring-spec (k cwd)
  (set-current-spec (acyclic-coloring-spec k cwd)))

(defun acyclic-coloring-automaton (k &optional (cwd 0))
  (with-spec (acyclic-coloring-spec k cwd)
    (let* ((stables (automata (get-tautomaton "Stables")))
	   (forests (automata (get-tautomaton "Forests")))
	   (automata (append stables forests))
	   (aca (intersection-automata-compatible automata)))
    (rename-object
     aca
     (format nil "~A-AC-~A" cwd k)))))

(defun k-acyclic-colorability-automaton (k &optional (cwd 0))
  (let ((f (acyclic-coloring-automaton k cwd)))
    (rename-object
     (constants-color-projection f)
     (format nil "~A-KAC-~A" cwd k))))

(defun acyclic-coloring-transducer (k afun)
  (constants-color-projection
   (attribute-automaton (acyclic-coloring-automaton k) afun)))

(defun count-acyclic-coloring-transducer (k)
  (acyclic-coloring-transducer k *count-afun*))

(defun spectrum-acyclic-coloring-transducer (k)
  (acyclic-coloring-transducer k (color-spectrum-afun k)))

(defun multi-spectrum-acyclic-coloring-transducer (k)
  (acyclic-coloring-transducer k (color-multi-spectrum-afun k)))

(defun assignment-acyclic-coloring-transducer (k)
  (acyclic-coloring-transducer k *assignment-afun*))

(defun k-acyclic-coloring-enumerator (term k)
  (final-value-enumerator term (assignment-coloring-transducer k)))
