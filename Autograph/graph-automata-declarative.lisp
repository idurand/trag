(in-package :autograph)

(defclass universal-order-mixin () ())

(defmethod container-order-fun ((c universal-order-mixin))
  #'strictly-ordered-p)

(defclass recursive-ordered-key-mixin (ordered-container) ())

(defmethod object-key ((o recursive-ordered-key-mixin))
  (mapcar 'object-key (container-contents o)))

(defclass universal-ordered-container
    (universal-order-mixin recursive-ordered-key-mixin ordered-container) ())

(defclass recursive-ordered-multi-key-mixin (ordered-container) ())

(defmethod object-key ((o recursive-ordered-multi-key-mixin))
  (mapcar (lambda (x) (list (object-key x) (object-key (attribute-of x))))
          (container-contents o)))

(defclass universal-ordered-multicontainer
    (universal-order-mixin recursive-ordered-multi-key-mixin
     ordered-multi-container)
  ())

(defclass universal-ordered-multimaxcontainer
    (universal-order-mixin recursive-ordered-multi-key-mixin
     ordered-multi-max-container)
  ())

(defun set-of (args)
  (make-container-generic args 'universal-ordered-container #'equal))

(defun set-of-args (&rest args) (set-of args))

(defun multiset-of (args)
  (make-multi-container-generic args 'universal-ordered-multicontainer
                                #'equal #'+))

(defun multiset-of-args (&rest args) (multiset-of args))

(defun multimaxset-of (max args)
  (make-multi-max-container-generic
   args 'universal-ordered-multimaxcontainer #'equal max))

(defun multimaxset-of-args (max &rest args) (multimaxset-of max args))

(defgeneric relation-image (preimage relation))
(defgeneric relation-preimage (image relation))

(defmethod relation-image (preimage (relation container))
  (let ((res (set-of-args)))
    (container-map (lambda (x)
                     (when (equal (first x) preimage)
                       (container-nadjoin (second x) res)))
                   relation)
    res))

(defmethod relation-image ((preimage container) (relation container))
  (let ((res (set-of-args)))
    (container-map (lambda (x)
                     (when (container-member (first x) preimage)
                       (container-nadjoin (second x) res)))
                   relation)
    res))

(defmethod relation-image ((preimage list) (relation container))
  (relation-image relation (set-of preimage)))

(defmethod relation-preimage (image (relation container))
  (let ((res (set-of-args)))
    (container-map (lambda (x)
                     (when (equal (second x) image)
                       (container-nadjoin (first x) res)))
                   relation)
    res))

(defmethod relation-preimage ((image container) (relation container))
  (let ((res (set-of-args)))
    (container-map (lambda (x)
                     (when (container-member (second x) image)
                       (container-nadjoin (first x) res)))
                   relation)
    res))

(defmethod relation-preimage ((image list) (relation container))
  (relation-preimage relation (set-of image)))

(defgeneric relation-full-image (relation)
  (:method ((relation container))
    (let ((res (set-of-args)))
      (container-map
       (lambda (x) (container-nadjoin (second x) res))
       relation)
      res)))
(defgeneric relation-full-preimage (relation)
  (:method ((relation container))
    (let ((res (set-of-args)))
      (container-map
       (lambda (x) (container-nadjoin (first x) res))
       relation)
      res)))
(defgeneric relation-reverse (relation)
  (:method ((relation container))
    (let ((res (container-empty-copy relation)))
      (container-map
       (lambda (x) (container-nadjoin (reverse x) res))
       relation)
      res)))

(defgeneric relation-composition (r1 r2)
  (:method ((r1 container) (r2 container))
     (let ((res (container-empty-copy r1)))
       (container-map
         (lambda (x)
           (container-map
             (lambda (y)
               (container-nadjoin (list (first x) y) res))
             (relation-image (second x) r2)))
         r1)
       res)))

(defgeneric complete-relation (left right)
            (:method ((left list) (right list))
                     (let ((res (set-of-args)))
                       (loop for x in left do
                             (loop for y in right do
                                   (container-nadjoin (list x y) res)))
                       res))
            (:method ((left container) (right container))
                     (let ((res (set-of-args)))
                       (container-map
                         (lambda (x)
                           (container-map
                             (lambda (y)
                               (container-nadjoin (list x y) res))
                             right))
                         left)
                       res)))

(defun disjoint-sets (&rest sets)
  (let ((ht (make-hash-table :test 'equal)))
    (catch 'disjoint-sets
      (loop for set in sets do
	(container-map
	 (lambda (x)
	   (let ((key (object-key x)))
	     (when (gethash key ht) (throw 'disjoint-sets nil))
	     (setf (gethash key ht) t)))
	 set))
      t)))

(defmacro define-declarative-automaton
    (name entries &key
		    (vertex (error "Specify vertex transition"))
		    (oplus (error "Specify oplus transition"))
		    (add (error "Specify add transition"))
		    (ren (error "Specify ren transition"))
		    (final '((x) x))
		    (valid '((x) x))
                    (clean '((x) (declare (ignore x))))
		    (orientation :unoriented)
		    (overridable-orientation :unoriented)
		    (max-cwd 0)
		    (vbits 0)
		    sink-state empty-final-p
		    (creator :make)
		    (deterministic t))
  (let* ((object (gensym))
         (graph-vertex-target
           (intern (format nil "~a-VERTEX-TARGET" name)))
         (transitions-fun
           (intern (format nil "~a-TRANSITIONS-FUN" name)))
         (automaton
           (intern (format nil "~a-AUTOMATON" name)))
         (orientation-parameter
           (if (eq orientation :unoriented)
	       'orientation orientation))
         (validp (gensym (format nil "~a-VALID-P" name)))
         (cleaner (gensym (format nil "~a-CLEAN" name)))
         (make (if (or (stringp creator) (keywordp creator))
		   (intern (string creator))
		   creator))
         (maker (intern (format nil "MAKE-~a" name))))
    `(progn
       (defclass ,name (graph-state key-mixin)
         ,(loop for e in entries collect
                `(,e :accessor ,e :initarg ,(intern (string e) :keyword)
                     :initform (error "Please specify ~a" ',e))))
       (defun ,validp ,@valid)
       (defun ,cleaner ,@clean)
       (defun ,maker (&rest args)
         (let ((,object (apply 'make-instance ',name args)))
           (when (,validp ,object) (,cleaner ,object) ,object)))
       (flet ((,make (&rest args) (apply #',maker args)))
         (defmethod object-key ((,object ,name))
           (list ,@(loop for e in entries collect `(object-key (,e ,object)))))
         (defmethod print-object ((,object ,name) stream)
           (format stream "#<~a: ~{~a ~}>" ',name (object-key ,object)))
         (defmethod state-final-p
	     ,(destructuring-bind (x) (first final) `((,x ,name)))
           ,@(rest final))
         (defmethod graph-oplus-target
	     ,(destructuring-bind (s1 s2) (first oplus)
		`((,s1 ,name) (,s2 ,name)))
           ,@(rest oplus))
         (defmethod graph-oplus-target ((s ,name) (n (eql *neutral-state*))) s)
         (defmethod graph-oplus-target ((n (eql *neutral-state*)) (s ,name)) s)
         (defmethod graph-oplus-target
	     ((s ,name) (n (eql *final-neutral-state*))) s)
         (defmethod graph-oplus-target
	     ((n (eql *final-neutral-state*)) (s ,name)) s)
         (defmethod graph-add-target
	     ,(destructuring-bind (a b s) (first add) `(,a ,b (,s ,name)))
           ,@(rest add))
         (defmethod graph-ren-target
	     ,(destructuring-bind (a b s) (first ren) `(,a ,b (,s ,name)))
           ,@(rest ren))
         (defun ,graph-vertex-target ,(first vertex)
           (declare (ignorable ,@(first vertex)))
           ,@(rest vertex)))
       (defgeneric ,transitions-fun (root arg)
	 (:method ((root (eql *empty-symbol*)) (arg null))
	   (if ,empty-final-p
	       *final-neutral-state*
	       *neutral-state*))
	 ,(if (> vbits 0)
	      `(:method ((root vbits-symbol) (arg null))
		 (,graph-vertex-target (port-of (symbol-of root))
				       (decoration-of root)))
	      `(:method ((root abstract-symbol) (arg null))
		 (,graph-vertex-target (port-of root))))
	 (:method ((root abstract-symbol) (arg list))
	   (cwd-transitions-fun root arg
				(function ,transitions-fun)))
	 )
       (defun ,automaton (&key (cwd 0)
			    ,@(when (eq orientation :unoriented)
				`((orientation ,overridable-orientation))))
         (make-cwd-automaton
	  ;; double if, but one is run-time and one is expand-time
	  (cwd-vbits-signature cwd ,vbits :orientation ,orientation-parameter :max-cwd ,max-cwd)
	  (function ,transitions-fun)
	  :name ,(string name) :sink-state ,sink-state
	  :deterministic ,deterministic)))))

(define-declarative-automaton
    universal-declarative
    ()
  :vertex ((x) (make-universal-declarative))
  :oplus ((s1 s2) s1)
  :ren ((a b s) s)
  :add ((a b s) s))

(defmacro set-image ((x set) &body expressions)
  (let ((res (gensym)))
    `(let ((,res (container-empty-copy ,set)))
       (container-map (lambda (,x)
                        (container-nadjoin (progn ,@expressions) ,res))
                      ,set)
       ,res)))

(defmacro multiset-image ((x set) &body expressions)
  (let ((res (gensym)) (attribute (gensym)))
    `(let ((,res (container-empty-copy ,set)))
       (container-map (lambda (,x)
                        (let ((,attribute (attribute-of x))
                              (,x (object-of ,x)))
                          (container-nadjoin
			   (make-object-multi
			    (progn ,@expressions)
			    ,attribute)
			   ,res)))
                      ,set)
       ,res)))

(defun set-set-union (s)
  (let ((res (container-empty-copy (or (container-car s) (set-of-args)))))
    (container-map
     (lambda (x) (container-nunion res x)) s)
    res))

(defun multiset-support (s)
  (let ((res (set-of-args)))
    (container-map (lambda (x) (container-nadjoin (object-of x) res)) s)
    res))

(defun object-rename (a b x) (if (equal a x) b x))

(defun rename-in-set (a b s) (set-image (x s) (object-rename a b x)))
(defun rename-in-multiset (a b s) (multiset-image (x s) (object-rename a b x)))

(defun rename-in-relation (a b r)
  (set-image (x r) (list (object-rename a b (first x))
                         (object-rename a b (second x)))))

(define-declarative-automaton
    connected-declarative
    (components connected)
  :final ((s) (connected s))
  :vertex ((x) (make-connected-declarative
		:connected t
		:components (set-of-args (set-of-args x))))
  :ren ((a b s)
        (make-connected-declarative
	 :components
	 (set-image (x (components s)) (set-image (y x) (if (= y a) b y)))
	 :connected (connected s)))
  :oplus ((s1 s2)
          (make-connected-declarative
	   :components (container-union (components s1) (components s2))
	   :connected nil))
  :add ((a b s)
        (let ((ab (set-of-args a b))
              (relevant (set-set-union (components s))))
          (if (container-subset-p ab relevant)
	      (let* ((null-components
		       (container-remove-if
			(lambda (x) (container-intersection-not-empty-p ab x))
			(components s)))
		     (a-b-components
		       (container-difference (components s) null-components))
		     (components (container-union 
				  null-components
				  (set-of-args (set-set-union a-b-components)))))
		(make-connected-declarative
		 :components components
		 :connected (= (container-size components) 1)))
	      s))))

(define-declarative-automaton
    non-redundant-decl
    (edges vertices)
  :final ((s) s)
  :vertex ((x) (make :edges (set-of-args) :vertices (set-of-args x)))
  :ren ((a b s) (make :edges (rename-in-relation a b (edges s))
                      :vertices (rename-in-set a b (vertices s))))
  :oplus ((s1 s2) (make :edges (container-union (edges s1) (edges s2))
                        :vertices (container-union (vertices s1)
                                                   (vertices s2))))
  :add ((a b s)
        (cond ((container-member (list a b) (edges s)) nil)
              ((not (container-subset-p (set-of-args a b) (vertices s))) s)
              (t (make :edges (container-union
			       (set-of-args (list a b)) (edges s))
                       :vertices (vertices s))))))

(defun test-graph-automata-declarative ()
  (assert
   (equality-automaton
    (universal-declarative-automaton :cwd 2 :orientation :unoriented)
    (universal-graph-automaton-explicit :cwd 2 :orientation :unoriented)))

  (assert
   (equality-automaton
    (universal-declarative-automaton :cwd 2 :orientation :unoriented)
    (universal-graph-automaton :cwd 2 :orientation :unoriented)))

  (assert
   (equality-automaton
    (minimize-automaton
     (compile-automaton
      (connected-declarative-automaton :cwd 2 :orientation :unoriented)))
    (minimize-automaton
     (compile-automaton
      (connectedness-automaton :cwd 2 :orientation :unoriented)))))

  (assert
   (equality-automaton
    (minimize-automaton
     (compile-automaton
      (non-redundant-decl-automaton :cwd 2 :orientation nil)))
    (minimize-automaton
     (compile-automaton
      (non-redundant-automaton nil :cwd 2)))))

  (assert
   (equality-automaton
    (minimize-automaton
     (compile-automaton
      (non-redundant-decl-automaton :cwd 2 :orientation t)))
    (minimize-automaton
     (compile-automaton
      (non-redundant-automaton t :cwd 2))))))

(define-declarative-automaton
  link-unori-ee-decl
  (x y success)
  :vbits 2
  :final ((s) (success s))
  :vertex ((v vbits)
           (destructuring-bind (x y) (map 'list 'onep vbits)
             (make :success nil
                   :x (set-of (when x (list v)))
                   :y (set-of (when y (list v))))))
  :ren ((a b s) (make :success (success s)
                      :x (rename-in-set a b (x s))
                      :y (rename-in-set a b (y s))))
  :oplus ((s1 s2)
          (if (or (success s1) (success s2))
            (make :success t :x (set-of-args) :y (set-of-args))
            (make :success nil
                  :x (container-union (x s1) (x s2))
                  :y (container-union (y s1) (y s2)))))
  :add ((a b s)
        (if (or (and (container-member a (x s))
                     (container-member b (y s)))
                (and (container-member a (y s))
                     (container-member b (x s))))
          (make :success t :x (set-of-args) :y (set-of-args))
          s)))

(define-declarative-automaton
  link-ori-ee-decl
  (x y success)
  :orientation t
  :vbits 2
  :final ((s) (success s))
  :vertex ((v vbits)
           (destructuring-bind (x y) (map 'list 'onep vbits)
             (make :success nil
                   :x (set-of (when x (list v)))
                   :y (set-of (when y (list v))))))
  :ren ((a b s) (make :success (success s)
                      :x (rename-in-set a b (x s))
                      :y (rename-in-set a b (y s))))
  :oplus ((s1 s2)
          (if (or (success s1) (success s2))
            (make :success t :x (set-of-args) :y (set-of-args))
            (make :success nil
                  :x (container-union (x s1) (x s2))
                  :y (container-union (y s1) (y s2)))))
  :add ((a b s)
        (if (and (container-member a (x s))
                 (container-member b (y s)))
          (make :success t :x (set-of-args) :y (set-of-args))
          s)))

(define-declarative-automaton
  link-unori-aa-decl
  (x y missing)
  :vbits 2
  :final ((s) (container-empty-p (missing s)))
  :vertex ((v vbits)
           (destructuring-bind (x y) (map 'list 'onep vbits)
             (make :missing (set-of-args)
                   :x (set-of (when x (list v)))
                   :y (set-of (when y (list v))))))
  :ren ((a b s) (make :missing (rename-in-relation a b (missing s))
                      :x (rename-in-set a b (x s))
                      :y (rename-in-set a b (y s))))
  :oplus ((s1 s2)
          (make :missing (container-union-gen
                           (list
                             (missing s1)
                             (missing s2)
                             (set-of
                               (loop for vx in 
                                     (container-unordered-contents (x s1))
                                     append
                                     (loop for vy in 
                                           (container-unordered-contents (y s2))
                                           unless (= vx vy)
                                           collect (sort (list vx vy) '<))))
                             (set-of
                               (loop for vx in
                                     (container-unordered-contents (x s2))
                                     append
                                     (loop for vy in
                                           (container-unordered-contents (y s1))
                                           unless (= vx vy)
                                           collect (sort (list vx vy) '<))))))
                :x (container-union (x s1) (x s2))
                :y (container-union (y s1) (y s2))))
  :add ((a b s)
        (make :missing (container-remove (sort (list a b) '<) (missing s))
              :x (x s)
              :y (y s))))

(define-declarative-automaton
  link-ori-aa-decl
  (x y missing)
  :vbits 2
  :orientation t
  :final ((s) (container-empty-p (missing s)))
  :vertex ((v vbits)
           (destructuring-bind (x y) (map 'list 'onep vbits)
             (make :missing (set-of-args)
                   :x (set-of (when x (list v)))
                   :y (set-of (when y (list v))))))
  :ren ((a b s) (make :missing (rename-in-relation a b (missing s))
                      :x (rename-in-set a b (x s))
                      :y (rename-in-set a b (y s))))
  :oplus ((s1 s2)
          (make :missing (container-union-gen
                           (list
                             (missing s1)
                             (missing s2)
                             (set-of
                               (loop for vx in
                                     (container-unordered-contents (x s1))
                                     append
                                     (loop for vy in 
                                           (container-unordered-contents (y s2))
                                           unless (= vx vy)
                                           collect (list vx vy))))
                             (set-of
                               (loop for vx in 
                                     (container-unordered-contents (x s2))
                                     append
                                     (loop for vy in 
                                           (container-unordered-contents (y s1))
                                           unless (= vx vy)
                                           collect (list vx vy))))))
                :x (container-union (x s1) (x s2))
                :y (container-union (y s1) (y s2))))
  :add ((a b s)
        (make :missing (container-remove (list a b) (missing s))
              :x (x s)
              :y (y s))))

(define-declarative-automaton
  oripath
  (from-x to-y fragments success)
  :vbits 3
  :orientation t
  :final ((s) (success s))
  :clean ((s)
          (when (success s) (setf (from-x s) (set-of-args)
                                  (to-y s) (set-of-args)
                                  (fragments s) (set-of-args))))
  :vertex ((v vbits)
           (destructuring-bind (x y u) (map 'list 'onep vbits)
             (cond ((and (not u) (or x y)) nil)
                   ((not u) (make :from-x (set-of-args)
                                  :to-y (set-of-args)
                                  :fragments (set-of-args)
                                  :success nil))
                   ((and x y) (make :success t
                                    :from-x (set-of-args)
                                    :to-y (set-of-args)
                                    :fragments (set-of-args)))
                   (x (make :from-x (set-of-args v)
                            :to-y (set-of-args)
                            :fragments (set-of-args)
                            :success nil))
                   (y (make :from-x (set-of-args)
                            :to-y (set-of-args v)
                            :fragments (set-of-args)
                            :success nil))
                   (t (make :from-x (set-of-args)
                            :to-y (set-of-args)
                            :fragments (set-of-args (list v v))
                            :success nil)))))
  :ren ((a b s)
        (make :from-x (rename-in-set a b (from-x s))
              :to-y (rename-in-set a b (to-y s))
              :fragments (rename-in-relation a b (fragments s))
              :success (success s)))
  :oplus ((r s)
          (when (and (or (container-empty-p (from-x s))
                         (container-empty-p (from-x r)))
                     (or (container-empty-p (to-y s))
                         (container-empty-p (to-y r)))
                     (if (success r)
                       (and (container-empty-p (from-x s))
                            (container-empty-p (to-y s))
                            (not (success s)))
                       t)
                     (if (success s)
                       (and (container-empty-p (from-x r))
                            (container-empty-p (to-y r))
                            (not (success r)))
                       t))
            (make :from-x (container-union (from-x s) (from-x r))
                  :to-y (container-union (to-y s) (to-y r))
                  :fragments (container-union (fragments r) (fragments s))
                  :success (or (success r) (success s)))))
  :add ((a b s)
        (let ((xp (container-member a (from-x s)))
              (yp (container-member b (to-y s)))
              (af (relation-preimage a (fragments s)))
              (bf (relation-image b (fragments s))))
          (make :success (or (success s) (and xp yp))
                :from-x (if xp
                          (container-union (from-x s)
                                           bf)
                          (from-x s))
                :to-y (if yp
                        (container-union (to-y s)
                                         af)
                        (to-y s))
                :fragments (container-union
                             (fragments s)
                             (complete-relation af bf))))))

#+nil
(trace graph-add-target graph-oplus-target graph-ren-target
       oripath-vertex-target)

#+nil
(loop for test in
      '(
        (:graph ((1))
                :vbits ((1) (1) (1))
                :result t)
        (:graph ((1 2))
                :vbits ((1) (1) (1))
                :result t)
        (:graph ((1 2))
                :vbits ((1) (1) (1 2))
                :result t)
        (:graph ((1 2))
                :vbits ((1) (2) (1 2))
                :result t)
        (:graph ((1 2 3))
                :vbits ((1) (2) (1 2 3))
                :result t)
        (:graph ((1 2 3))
                :vbits ((1) (3) (1 2 3))
                :result t)
        (:graph ((1 2 3 4))
                :vbits ((1) (3) (1 2 3))
                :result t)
        (:graph ((1 2 3 4))
                :vbits ((1) (4) (1 2 3 4))
                :result t)
        (:graph ((1 2 3 4) (2 5 6))
                :vbits ((1) (4) (1 2 3 4))
                :result t)
        (:graph ((1 2 3 4) (2 5 6))
                :vbits ((1) (4) (1 2 3 4 5 6))
                :result t)
        )
      do (format t "Test case: ~s~%" test)
      do
      (assert
        (eq
          (getf test :result)
          (recognized-p
            (term-add-vbits-by-eti-lists
              (cwd-decomposition
                (graph-from-paths (getf test :graph)
                                  :oriented t))
              (getf test :vbits))
            (oripath-automaton)))))
