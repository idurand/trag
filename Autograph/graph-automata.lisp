;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)
(defparameter *a* nil)
(defparameter *f* nil)
(defvar *ports*)
(defvar *ok-fun-add* nil)

(defun make-cwd-automaton (signature transition-fun &rest initargs)
  (apply #'make-fly-automaton signature transition-fun initargs))

(defun cwd-automaton-name (name cwd)
  (if (zerop cwd) name (format nil "~A-~A" cwd name)))

(defmethod clique-width ((cwd-automaton abstract-automaton))
  (clique-width (signature cwd-automaton)))

(defclass graph-state (uncasted-state cwd-mixin) ())

(defgeneric graph-add-target (a b grc)
  (:documentation "unoriented cwd-add")
  (:method (a b (s ok-state)) s)
  (:method (a b (s ok-ok-state)) s)
  (:method (a b (s neutral-state)) s)
  (:method (a b (s uncasted-state))
    (warn "graph-add-target undefined for ~A ~A" s (type-of s))
    s)
  (:method (a b (s cwd-mixin))
    (warn "graph add-target undefined for ~A" (type-of s))
    s)
  (:method (a b (s (eql *success-state*)))
    (warn "sink (eql ~A *success-state*) ~%" s) s)
  (:method (a b (s casted-state)) (graph-add-target a b (in-state s)))
  (:method :around (a b (state uncasted-state))
    (or (and *ok-fun-add*
	     (funcall *ok-fun-add* a b state))
	(call-next-method))))

(defgeneric graph-oadd-target (a b grc)
  (:documentation "oriented cwd-add")
  (:method (a b (s special-state)) s)
  (:method (a b (s (eql *success-state*))) s)
  (:method (a b (s uncasted-state))
;;    (warn "graph-oadd-target undefined for ~A ~A" s (type-of s))
    (graph-add-target a b s))
  (:method (a b (s casted-state)) (graph-oadd-target a b (in-state s)))
  (:method :around (a b (state uncasted-state))
    (or (and *ok-fun-add*
	     (funcall *ok-fun-add* a b state))
	(call-next-method))))

(defgeneric graph-ren-target (a b grc)
  (:documentation "cwd-renaming")
  (:method (a b (state null)) nil)
  (:method (a b (s uncasted-state)) s)
  (:method (a b (s cwd-mixin))
    (warn "graph ren-target undefined for ~A" (type-of s)) s)
  (:method (a b (ports ports))
    (warn "graph-ren-target of ports")
    (ports-subst b a ports))
  (:method (a b (l list)) (mapcar (lambda (s)
				    (graph-ren-target a b s))
				  l))
  (:method (a b (s casted-state)) (graph-ren-target a b (in-state s))))

(defgeneric graph-reh-target (port-mapping grc)
  (:documentation "")
  (:method ((port-mapping port-mapping) (ports ports))
    (warn "graph-reh-target of ports")
    (apply-port-mapping port-mapping ports))
  (:method ((port-mapping port-mapping) (state null)) nil)
  (:method ((port-mapping port-mapping) (state graph-state))
    ;;  (warn "graph-reh-target undefined for state ~A~%" (type-of state))
    (let ((pm (clique-width::sequentialize-mapping port-mapping (ports-of state))))
      (loop for pair in (clique-width::mapping-pairs pm)
	    do (progn 
		 (setq state (graph-ren-target (car pair) (cdr pair) state))
		 (unless state (return-from graph-reh-target)))
	    finally (return state))))
  (:method ((port-mapping port-mapping) (s cwd-mixin))
    (warn "graph reh-target -undefined for ~A" (type-of s))
    s)
  (:method ((port-mapping port-mapping) (s special-state)) s))

(defgeneric graph-oplus-target (state1 state2)
  (:documentation "")
  (:method ((sc1 ok-state) (sc2 ok-state)) nil)
  (:method ((s1 ok-ok-state) (s2 uncasted-state)) s1)
  (:method ((s1 uncasted-state) (s2 ok-ok-state)) s2)
  (:method ((neutral neutral-state) (sc uncasted-state)) sc)
  (:method ((sc uncasted-state) (neutral neutral-state)) sc)
  (:method ((p1 null) (p2 null)) nil)
  (:method ((p1 null) (p2 uncasted-state)) nil)
  (:method ((p1 uncasted-state) (p2 null)) nil)
  (:method ((p1 uncasted-state) (p2 uncasted-state)) nil)
  (:method ((c1 casted-state) (c2 casted-state))
    (graph-oplus-target (in-state c1) (in-state c2))))

(defun graph-add-target-fun (oriented)
  (if oriented #'graph-oadd-target #'graph-add-target))

(defgeneric cwd-transitions-fun (root arg transition-fun)
  (:method ((root (eql *empty-symbol*)) (arg null) transition-fun)
    (if *neutral-state-final-p*
	*final-neutral-state*
	*neutral-state*))
  (:method ((root annotated-symbol) (arg list) transitions-fun)
    (let ((symbol (symbol-of root)))
      (if arg
	  (cwd-transitions-fun symbol arg transitions-fun)
	  (funcall transitions-fun symbol arg))))
  (:method ((root (eql *oplus-ac-symbol*)) (states list) transitions-fun)
    (assert states)
    (let ((useful-states (remove-if #'state-neutral-p states)))
      (unless useful-states (return-from cwd-transitions-fun (find-if #'state-neutral-p states)))
      (let ((s1 (pop useful-states)))
	(if (endp useful-states)
	    s1
	    (let ((s2 (pop useful-states)))
	      (if (endp useful-states)
		  (graph-oplus-target s1 s2)
		  (graph-oplus-target-gen s1 s2 useful-states)))))))
  (:method ((root (eql *oplus-ac-symbol*)) (arg null) transitions-fun)
    (warn "cwd-transitions-fun with *oplus-ac-symbol*")
    nil)
  (:method ((root non-constant-mixin) (arg null) transitions-fun) nil)
  (:method ((root non-constant-mixin) (arg list) transitions-fun)
    (declare (ignore transitions-fun))
    (cond ((oplus-symbol-p root)
	   (graph-oplus-target (first arg) (second arg)))
	  ((reh-symbol-p root) 
	   (graph-reh-target (port-mapping-of root) (first arg)))
	  (t
	   (let* ((ports (symbol-ports root))
		  (a (first ports))
		  (b (second ports))
		  (state (car arg)))
	     (cond
	       ((ren-symbol-p root)
		(graph-ren-target a b state))
	       ((add-symbol-p root)
		(funcall (graph-add-target-fun (oriented-p root)) a b state))
	       ((reh-symbol-p root)
		(graph-reh-target (port-mapping-of root) state))
	       (t (error "unknown operation ~A" root))))))))

(defgeneric graph-oplus-target-gen (graph-state1 graph-state2 states)
  (:method ((s1 uncasted-state) (s2 uncasted-state) (states list))
    (reduce #'graph-oplus-target (cons s1 (cons s2 states)))))

(defmethod recognized-p ((graph graph) (automaton abstract-automaton) &key (enum nil) (save-run nil) (signal t))
  (recognized-p (cwd-decomposition graph) automaton :enum enum :save-run save-run :signal signal))

(defgeneric cwd-automaton-emptiness (cwd-automaton cwd)
  (:method ((automaton fly-automaton) (cwd integer))
    (automaton-emptiness (change-automaton-cwd automaton cwd))))

(defgeneric cwd-automaton-compile (cwd-automaton cwd)
  (:method ((automaton fly-automaton) (cwd integer))
    (compile-automaton (change-automaton-cwd automaton cwd))))

(defgeneric cwd-non-emptiness-witness (cwd-automaton cwd)
  (:method ((automaton fly-automaton) (cwd integer))
    (non-emptiness-witness (change-automaton-cwd automaton cwd))))

(defgeneric compile-cwd-automaton (cwd-automaton cwd)
  (:method ((cwd-automaton table-automaton) (cwd integer))
    cwd-automaton)
  (:method ((cwd-automaton fly-automaton) (cwd integer))
    (let ((max-cwd (max-clique-width cwd-automaton)))
      (assert (or (zerop max-cwd) (<= cwd max-cwd)))
      (compile-automaton (change-automaton-cwd cwd-automaton cwd)))))
