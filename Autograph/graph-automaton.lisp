;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun graph-automaton (nb-nodes edges noedges)
  (intersection-automata
   (nconc
    (mapcar
     (lambda (edge)
       (edge-automaton nb-nodes (first edge) (second edge)))
     edges)
    (mapcar
     (lambda (edge)
       (complement-automaton
	(edge-automaton nb-nodes (first edge) (second edge))))
     noedges))))

(defun noedges-from-nodes-and-edges (nodes edges)
;; nodes in edges must be ordered
  (loop for origin in nodes
     nconc
       (loop
	  for extremity in nodes
	  for e = (list origin extremity) ;; then (list origin extremity)
	  when (and (< origin extremity)
		    (not (member e edges :test #'equal)))
	  collect e)))

(defun graph-from-edges-automaton (edges)
  (let ((nodes (enodes-from-earcs edges)))
    (graph-automaton
     (length nodes)
     edges
     (noedges-from-nodes-and-edges nodes edges))))

(defun induced-subgraph-afun-transducer (edges afun)
  (let ((nodes (enodes-from-earcs edges)))
    (vbits-projection 
     (attribute-automaton
      (graph-from-edges-automaton edges)
      afun)
     (length nodes))))

 (defun induced-subgraph-assignment-transducer (edges)
   (induced-subgraph-afun-transducer edges *assignment-afun*))

 (defun induced-subgraph-count-transducer (edges)
   (induced-subgraph-afun-transducer edges *count-afun*))

