;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defvar *next-count-fun*)
(defvar *oplus-fun*)
(defvar *add-fun*)
(defvar *ren-fun*)

(defclass counting-state (graph-state)
  ((num :initarg :num :reader num)))

(defun make-counting-state (n)
  (make-instance 'counting-state :num n))

(defmethod print-object ((co counting-state) stream)
  (format stream "[~A]" (num co)))

(defmethod compare-object ((co1 counting-state) (co2 counting-state))
  (= (num co1) (num co2)))

(defmethod graph-oplus-target ((co1 ok-state) (co2 counting-state))
  (when (zerop (num co2))
    co1))

(defmethod graph-oplus-target ((co1 counting-state) (co2 ok-state))
  (when (zerop (num co1))
    co2))

(defmethod graph-add-target (a b (co counting-state))
  (declare (ignore a b))
  (make-counting-state 
   (funcall *add-fun* (num co))))

(defmethod graph-ren-target (a b (co counting-state))
  (declare (ignore a b))
  (make-counting-state 
   (funcall *ren-fun* (num co))))

(defmethod graph-oplus-target ((co1 counting-state) (co2 counting-state))
  (let ((n (funcall *oplus-fun* (num co1) (num co2))))
    (make-counting-state
     (funcall *next-count-fun* n))))

(defgeneric cardinality-transitions-fun (root arg)
  (:documentation "")
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (make-counting-state 1))
  (:method ((root (eql *empty-symbol*)) (arg (eql nil)))
    (make-counting-state 0))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'cardinality-transitions-fun)))

(defun count-automaton
    (pred-final next-count-fun oplus-fun add-fun ren-fun
     neutral-state-p &key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-signature cwd :orientation orientation)
   (lambda (root states)
     (let ((*neutral-state-final-p* neutral-state-p)
	   (*next-count-fun* next-count-fun)
	   (*oplus-fun* oplus-fun)
	   (*add-fun* add-fun)
	   (*ren-fun* ren-fun))
       (cardinality-transitions-fun root states)))
   :final-state-fun (lambda (state) (funcall pred-final (num state)))
   :name (cwd-automaton-name "~A-CARD" cwd)))

(defun cardinality-automaton (count &key (cwd 0) (orientation :unoriented))
  (rename-object
   (count-automaton
    (lambda (n) (= n count))
    (lambda (n) (min n (1+ count)))
    #'+
    #'identity
    #'identity
    nil
    :cwd cwd :orientation orientation)
   (format nil "~A-CARD=~A" cwd count)))

(defun color-cardinality-automaton (count k color &key (cwd 0) (orientation :unoriented)) ; card(Ccolor) = count
  (assert (<= 1 color k))
  (rename-object
   (nothing-to-color 
    (cardinality-automaton count :cwd cwd :orientation orientation) k color)
   (format nil "~A-CARD(C~A)=~A-~A" cwd color count k)))

(defun subgraph-cardinality-automaton (count m j &key (cwd 0) (orientation :unoriented)) ; card(Xj) = count
  (rename-object
   (nothing-to-xj (cardinality-automaton count :cwd cwd :orientation orientation) m j)
   (format nil "~A-CARD(X~A)=~A-~A" cwd j count m)))

(defun counting-automaton (&key (cwd 0) (orientation :unoriented)) ; card(V_G) 
  (rename-object
   (count-automaton
    (lambda (n) n)
    (lambda (n) n)
    #'+
    #'identity
    #'identity
    nil
    :cwd cwd :orientation orientation)
   (format nil "~A-CARD()" cwd)))

(defun subgraph-counting-automaton (m j &key (cwd 0) (orientation :unoriented))
  "automaton for card(Xj) with m = M"
  (rename-object
   (nothing-to-xj (counting-automaton :cwd cwd :orientation orientation) m j)
   (format nil "~A-CARD(X~A)-~A" cwd j m)))

(defun cardinality-between-automaton (count1 count2 &key (cwd 0) (orientation :unoriented))
  (assert (<= count1 count2))
  (rename-object
   (count-automaton
    (lambda (n) (<= count1 n count2))
    (lambda (n) (min n (1+ count2))) ;; to have a finite number of states
    #'+
    #'identity
    #'identity
    t
    :cwd cwd :orientation orientation)
   (format nil "~A-CARD~A<=n<=~A" cwd count1 count2)))

(defun cardinality-inf-automaton (count &key (cwd 0) (orientation :unoriented))
  (rename-object
   (cardinality-between-automaton 0 count :cwd cwd :orientation orientation)
   (format nil "~A-CARD<=~A" cwd count)))

(defun cardinality-sup-automaton (count &key (cwd 0) (orientation :unoriented))
  (rename-object
   (count-automaton
    (lambda (n) (>= n count))
    (lambda (n) (min n (1+ count))) ;; to have a finite number of states
    #'+
    #'identity
    #'identity
    nil
    :cwd cwd :orientation orientation)
   (format nil "~A-CARD>=~A" cwd count)))

(defun cardinality-p-mod-q-automaton (p q &key (cwd 0) (orientation :unoriented))
  (rename-object
   (count-automaton
    (lambda (n) (= n p))
    (lambda (n) (mod n q))
    #'+
    #'identity
    #'identity
    nil
    :cwd cwd :orientation orientation)
   (format nil "~A-CARD=~A-MOD-~A" cwd p q)))

(defun subgraph-cardinality-inf-automaton (count m j &key (cwd 0) (orientation :unoriented))
  "automaton for card(Xj) <= count with m = M"
  (rename-object
   (nothing-to-xj (cardinality-inf-automaton count :cwd cwd :orientation orientation) m j)
   (format nil "~A-CARD(X~A)<=~A-~A" cwd j count m)))

(defun subgraph-cardinality-p-mod-q-automaton (p q m j &key (cwd 0) (orientation :unoriented))
  "automaton for card(Xj) = p mod q with m = M"
  (rename-object
   (nothing-to-xj (cardinality-p-mod-q-automaton p q :cwd cwd :orientation orientation) m j)
   (format nil "~A-CARD(X~A)=~A-MOD-~A-~A" cwd j p q m)))

(defun table-subgraph-cardinality-automaton (count m j cwd &key (orientation :unoriented)) ; card(Xj1) = count
  (compile-automaton
   (subgraph-cardinality-automaton count m j :cwd cwd :orientation orientation)))

(defun table-subgraph-cardinality-inf-automaton (count m j cwd &key (orientation :unoriented)) ; card(Xj1) <= count
  (compile-automaton
   (subgraph-cardinality-inf-automaton count m j :cwd cwd :orientation orientation)))

(defun table-cardinality-automaton (count cwd &key (orientation :unoriented)) ; card_G() = count
  (compile-automaton
   (cardinality-automaton count :cwd cwd :orientation orientation)))

(defun table-cardinality-inf-automaton (count cwd &key (orientation :unoriented)) ; card_G() <= count
  (compile-automaton
   (cardinality-inf-automaton count :cwd cwd :orientation orientation)))

(defun table-cardinality-p-mod-q-automaton (p q cwd &key (orientation :unoriented)) ; card_G() = p mod q
  (compile-automaton
   (cardinality-p-mod-q-automaton p q :cwd cwd :orientation orientation)))
