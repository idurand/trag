;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;; test term (graph)

(defun basic-cc-automaton-from-formula (&key (cwd 0) (orientation :unoriented))
  (intersection-automaton-compatible-gen
   (nothing-to-x1
    (intersection-automaton-compatible
     (connectedness-automaton :cwd cwd)
     (complement-automaton (empty-automaton :cwd cwd :orientation orientation))))
   (complement-automaton (xi-eq-cxj (basic-link-automaton :cwd cwd :orientation orientation) 2 1 2))))

(defun nb-cc-transducer (&key (cwd 0) (orientation :unoriented))
  (rename-object
   (vbits-projection
    (attribute-automaton
     (basic-cc-automaton-from-formula :cwd cwd :orientation orientation)
     *count-afun*))
   "~A-#CC"))

(defun cc-test-term1 ()
  (let* ((tab (cwd-term-unoriented-add
	       0 1 (cwd-term-oplus (cwd-term-vertex 0) (cwd-term-vertex 1))))
	 (tbc (cwd-term-unoriented-add
	       1 2 (cwd-term-oplus (cwd-term-vertex 1) (cwd-term-vertex 2))))
	 (tac (cwd-term-unoriented-add
	       0 2 (cwd-term-oplus (cwd-term-vertex 0) (cwd-term-vertex 2))))
	 (tabc (cwd-term-unoriented-add
		1 2 (cwd-term-oplus tab (cwd-term-vertex 2)))))
	       (cwd-term-multi-oplus tab tbc tac tabc)))

(defun cc-test-term2 ()
    (cwd-term-unoriented-add 0 1 (cc-test-term1)))

(defun basic-not-link-automaton-from-formula (&key (cwd 0) (orientation :unoriented))
  (complement-automaton
   (xi-eq-cxj
    (basic-link-automaton :cwd cwd :orientation orientation) 2 1 2)))

(defun nb-not-link-transducer (&key (cwd 0) (orientation :unoriented))
  (rename-object
   (vbits-projection
    (attribute-automaton
     (basic-not-link-automaton-from-formula
      :cwd cwd :orientation orientation)
     *count-afun*))
   "~A-#NOT-LINK"))
