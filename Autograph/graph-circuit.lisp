;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;; only for the oriented case
(defclass circuit-state (graph-state)
  ((alpha-ports :initarg :alpha-ports :reader alpha-ports :type ports) ;; degree 0
   (beta-ports :initarg :beta-ports :reader beta-ports :type ports)
   (psi-relation :initarg :psi-relation :reader psi-relation :type ports-relation)))

(defgeneric make-circuit-state (alpha-ports beta-ports psi-relation)
  (:method ((alpha-ports ports) 
	    (beta-ports ports) (psi-relation ports-relation))
    (make-instance
     'circuit-state
     :alpha-ports alpha-ports
     :beta-ports beta-ports
     :psi-relation psi-relation)))

(defgeneric cports-of (psi-relation)
  (:method ((psi-relation ports-relation))
    (ports-remove-if #'minusp (ports-of psi-relation))))

(defmethod ports-of ((state circuit-state))
  (ports-of (list (alpha-ports state) (beta-ports state) (ports-of (psi-relation state)))))

(defgeneric circuit-state-union (circuit-state1 circuit-state2)
  (:method ((state1 circuit-state) (state2 circuit-state))
    (make-circuit-state
     (ports-union (alpha-ports state1) (alpha-ports state2))
     (ports-union (beta-ports state1) (beta-ports state2))
     (ports-relation-union (psi-relation state1) (psi-relation state2)))))

(defmethod print-object ((state circuit-state) stream)
  (format stream "H~A~A~A"
	  (alpha-ports state)
	  (beta-ports state)
	  (psi-relation state)))

(defgeneric circuit-state-empty-p (circuit-state)
  (:method ((state circuit-state))
    (and (ports-empty-p (alpha-ports state)) 
	 (ports-empty-p (beta-ports state))
	 (ports-relation-empty-p (psi-relation state)))))

(defmethod graph-oplus-target ((state1 (eql *ok-state*)) (state2 circuit-state))
  (graph-oplus-target state2 state1))

(defmethod graph-oplus-target ((state1 circuit-state) (state2 (eql *ok-state*)))
  (when (circuit-state-empty-p state1)
    *ok-state*))

(defgeneric d-condition (circuit-state)
  (:method ((state circuit-state))
    (let* ((alpha-ports (alpha-ports state))
	   (beta-ports (beta-ports state))
	   (psi-relation (psi-relation state))
	   (cports (cports-of psi-relation)))
      (and
       (container-intersection-empty-p alpha-ports beta-ports)
       (container-intersection-empty-p alpha-ports cports)
       (container-intersection-empty-p beta-ports cports))
      (let* ((pairs (container-contents (pairs psi-relation))))
	(and (no-duplicates (mapcar #'first pairs))
	     (no-duplicates (mapcar #'second pairs)))))))       

(defmethod graph-oplus-target ((state1 circuit-state) (state2 circuit-state))
  (let ((alpha-ports1 (alpha-ports state1))
	(alpha-ports2 (alpha-ports state2))
	(psi-relation1 (psi-relation state1))
	(psi-relation2 (psi-relation state2))
	(union-state (circuit-state-union state1 state2)))
    (when (or
	   (not (container-intersection-empty-p alpha-ports1 alpha-ports2))
	   (not (ports-relation-intersection-empty-p psi-relation1 psi-relation2))
	   (not (d-condition union-state)))
      (return-from graph-oplus-target))
    union-state))

(defmethod graph-ren-target (a b (state circuit-state))
  (let ((alpha-ports (alpha-ports state)))
    (when (and (ports-member a alpha-ports) (ports-member b alpha-ports))
      (return-from graph-ren-target))
    (let* ((beta-ports (beta-ports state))
	   (psi-relation (psi-relation state))
	   (state (make-circuit-state
		   (ports-subst b a alpha-ports)
		   (ports-subst b a beta-ports)
		   (ports-relation-subst b a psi-relation))))
      (when (d-condition state)
	state))))

(defmethod graph-add-target (a b (state circuit-state))
  (let ((ports (ports-of state)))
    (unless (and (ports-member a ports) (ports-member b ports))
      (return-from graph-add-target state))
    (let* ((alpha-ports (alpha-ports state))
	   (beta-ports (beta-ports state))
	   (psi-relation (psi-relation state)))
      (when (and (ports-empty-p alpha-ports) 
		 (compare-object psi-relation (make-ports-relation (list (list b a)) t)))
	(return-from graph-add-target *ok-state*))
      (let ((ok t)
	    (new-alpha alpha-ports)
	    (new-beta beta-ports)
	    (new-ports-relation psi-relation))
	(if (and (ports-member a alpha-ports) (ports-member b alpha-ports))
	    (progn
	      (setq new-alpha (ports-remove a (ports-remove b new-alpha)))
	      (setq new-ports-relation (ports-relation-adjoin (list a b) new-ports-relation)))
	    (let ((c (container-find-if
		      (lambda (pair) (= b (first pair))) (pairs psi-relation)))
		  (d  (container-find-if
		       (lambda (pair) (= a (second pair))) (pairs psi-relation))))
	      (when c (setq c (second c)))
	      (when d (setq d (first d)))
	      (cond
		((and c (ports-member a alpha-ports))
		 (setq new-alpha (ports-remove a new-alpha))
		 (setq new-beta (ports-adjoin b new-beta))
		 (setq new-ports-relation
		       (ports-relation-adjoin 
			(list a c) 
			(ports-relation-remove (list b c) new-ports-relation))))
		((and (ports-member b alpha-ports) d)
		 (setq new-alpha (ports-remove b new-alpha))
		 (setq new-beta (ports-adjoin a new-beta))
		 (setq new-ports-relation
		       (ports-relation-adjoin 
			(list d b) (ports-relation-remove (list d a) new-ports-relation))))
		((and c d)
		 (setq new-beta (ports-adjoin b (ports-adjoin a new-beta)))
		 (setq new-ports-relation (ports-relation-adjoin 
					   (list d c)
					   (ports-relation-remove 
					    (list d a)
					    (ports-relation-remove (list b c) new-ports-relation)))))
		(t (setq ok nil)))))
	(when ok (make-circuit-state new-alpha new-beta new-ports-relation))))))

(defgeneric circuit-transitions-fun (abstract-symbol states)
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'circuit-transitions-fun))
  (:method ((root (eql *empty-symbol*)) (arg null))
    (cwd-transitions-fun root arg #'circuit-transitions-fun)))

(defmethod circuit-transitions-fun ((root cwd-constant-symbol) (arg null))
  (let ((port (port-of root)))
    (make-circuit-state
     (make-ports-from-port port)
     (make-empty-ports)
     (make-empty-ports-relation t))))

(defun circuit-transducer ()
  (warn "todo"))

(defun circuit-automaton (&key (cwd 0))
  "recognize oterm of directed circuit graphs"
  (make-cwd-automaton
   (cwd-signature cwd :orientation t)
   (let ((*neutral-state-final-p* t))
     (lambda (root states)
       (circuit-transitions-fun root states)))
   :name (cwd-automaton-name "CIRCUIT" cwd)))

(defun test-circuit ()
  (let ((f (circuit-automaton))
	(c5 (cwd-decomposition (graph-cycle 5 t)))
	(term (cwd-decomposition (graph-from-earcs '((1 2) (2 3) (3 4) (4 1) (2 5)) :oriented t))))
    (assert (recognized-p c5 f))
    (assert (not (recognized-p term f)))))
  
