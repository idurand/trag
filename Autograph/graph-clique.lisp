;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass clique-state (graph-state)
  ((ports :type ports :initarg :ports :reader ports-of)
   (missing-edges :type ports-relation
		  :initarg :missing-edges
		  :reader missing-edges)))

(defmethod print-object ((co clique-state) stream)
  (format stream "<~A,~A>" (ports-of co) (missing-edges co)))

(defmethod compare-object ((co1 clique-state) (co2 clique-state))
  (and (compare-object (ports-of co1) (ports-of co2))
       (compare-object (missing-edges co1) (missing-edges co2))))

(defgeneric make-clique-state (ports no-edges-object)
  (:method ((ports ports) (no-edges-object ports-relation))
    (make-instance 'clique-state
		   :ports ports
		   :missing-edges no-edges-object)))

(defmethod state-final-p ((co clique-state))
  (ports-relation-empty-p (missing-edges co)))

(defmethod graph-ren-target (a b (co clique-state))
  (let* ((me (missing-edges co))
	 (oriented (oriented-p me)))
    (unless (ports-relation-member (opair a b oriented) me)
      (make-clique-state 
       (ports-subst b a (ports-of co))
       (ports-relation-subst b a me)))))

(defmethod graph-add-target (a b (co clique-state))
  (let ((ports (ports-of co))
	(me (missing-edges co)))
    (if (or (>= 1 (container-size ports))
	    (not (ports-member a ports))
	    (not (ports-member b ports)))
	co
	(let ((new (ports-relation-remove (list a b) me)))
	  (make-clique-state
	   ports
	   new)))))

(defmethod graph-oplus-target ((co1 clique-state) (co2 clique-state))
  (let ((ports1 (ports-of co1))
	(ports2 (ports-of co2)))
    (when (container-intersection-empty-p ports1 ports2)
      (let* ((ports-relation1 (missing-edges co1))
	     (oriented (oriented-p ports-relation1))
	     (me1 (pairs ports-relation1))
	     (missing-pairs (pairs-union me1 (pairs (missing-edges co2)))))
	(container-map
	 (lambda (p1)
	   (container-map
	    (lambda (p2)
	      (when (/= p1 p2)
		(pairs-nadjoin (list p1 p2) missing-pairs)
		(pairs-nadjoin (list p2 p1) missing-pairs)))
	    ports2))
	 ports1)
	(make-clique-state
	 (ports-union ports1 ports2)
	 (make-ports-relation missing-pairs oriented))))))

(defgeneric clique-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg null))
    (make-clique-state
     (make-ports-from-port  (port-of root))
     (make-empty-ports-relation *oriented*)))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'clique-transitions-fun))
  (:method((root (eql *empty-symbol*)) (arg null))
    (cwd-transitions-fun root arg #'clique-transitions-fun)))

(defun clique-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd)
   (lambda (root states)
     (let ((*neutral-state-final-p* t))
       (clique-transitions-fun  root states)))
   :name (cwd-automaton-name "CLIQUE" cwd)))

(defun table-clique-automaton (cwd) (compile-automaton (clique-automaton cwd)))

(defun subgraph-clique-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
  (nothing-to-xj (clique-automaton cwd) m j))

(defun color-clique-automaton (k color &optional (cwd 0))
  (assert (<= 1 color k))
  (nothing-to-color (clique-automaton cwd) k color))

(defun table-subgraph-clique-automaton (m j cwd)
  (compile-automaton (subgraph-clique-automaton m j cwd)))

(defun table-color-clique-automaton (k color cwd)
  (compile-automaton (color-clique-automaton k color cwd)))

(defun test-graph-clique ()
  (let ((a (clique-automaton))
	(tok (cwd-term-kn 3))
	(tnok (cwd-term-pn 3)))
    (assert (recognized-p tok a))
    (assert (not (recognized-p tnok a)))))
