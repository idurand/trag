;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin, Irène Durand

(in-package :autograph)

(defclass symmetric-colorability-state (graph-state key-mixin)
  ((label-colors :type array :accessor label-colors :initarg :label-colors)
   (color-labels :type array :accessor color-labels :initarg :color-labels)
   (multiplier :type :integer :accessor multiplier :initarg :multiplier)))

(defmethod object-key ((object symmetric-colorability-state))
  (label-colors object))

(defmethod print-object ((object symmetric-colorability-state) stream)
  (format
    stream
    "#<symmetric-colorability-state:~{ (~{~a~#[~:; ~]~})~}|~{ (~{~a~#[~:; ~]~})~}*~a>"
    (loop for k from 0 to (1- (length (label-colors object)))
          collect (elt (label-colors object) k))
    (loop for k from 0 to (1- (length (color-labels object)))
          collect (elt (color-labels object) k))
    (multiplier object)
    ))

(defmethod state-final-p ((object symmetric-colorability-state)) t)
;; (defmethod final-state-p ((state symmetric-colorability-state) automaton) t)

(defun make-symmetric-colorability-state
  (label-colors color-labels &key (multiplier 1))
  (make-instance 'symmetric-colorability-state
                 :label-colors label-colors
                 :color-labels color-labels
                 :multiplier multiplier))

(defun make-empty-symmetric-colorability-state (labels colors &key
                                                 (multiplier 1))
  (make-symmetric-colorability-state
    (make-array labels :initial-element nil)
    (make-array colors :initial-element nil)
    :multiplier multiplier))

(defun make-vertex-symmetric-colorability-state
  (label labels colors &key (multiplier 1))
  (let* ((res (make-empty-symmetric-colorability-state 
                labels colors)))
    (push 0 (elt (label-colors res) label))
    (push label (elt (color-labels res) 0))
    (setf (multiplier res) multiplier)
    res))

(defun copy-symmetric-colorability-state (c)
  (let* ((res (make-instance
                'symmetric-colorability-state
                :color-labels (subseq (color-labels c) 0)
                :label-colors (subseq (label-colors c) 0)
                :multiplier (multiplier c))))
    res))

(defgeneric resize-symmetric-colorability-state (c labels)
  (:method ((c symmetric-colorability-state) (labels integer))
     (let* ((colors (length (color-labels c)))
            (old-labels (length (label-colors c))))
       (cond ((= labels old-labels) c)
             ((< labels old-labels)
              (loop for k from old-labels to (1- labels)
                    when (elt (label-colors c) k)
                    do (error
                         "Trying to resize ~s down, but label ~a is used" c k))
              (make-instance 'symmetric-colorability-state
                             :label-colors (subseq (label-colors c) 0 labels)
                             ; just a copy
                             :color-labels (subseq (color-labels c) 0 colors)))
             (t (let ((res (make-empty-symmetric-colorability-state 
                             labels colors)))
                  (loop for k from 0 to (1- colors) do
                        (setf (elt (color-labels res) k)
                              (elt (color-labels c) k)))
                  (loop for k from 0 to (1- old-labels) do
                        (setf (elt (label-colors res) k)
                              (elt (label-colors c) k)))

                  (setf (multiplier res) (multiplier c))
                  res))))))

;; Helper functions for colorability states, trivial operations

(defgeneric recolor (object permutation)
  (:method ((object symmetric-colorability-state) (permutation sequence))
           (let* ((labels (length (label-colors object)))
                  (colors (length (color-labels object)))
                  (res (make-empty-symmetric-colorability-state
                         labels colors)))
             (loop for k downfrom (1- colors) to 0 
                   do
                   (setf (elt (color-labels res) (elt permutation k))
                         (elt (color-labels object) k)))
             (loop for k downfrom (1- colors) to 0 
                   do
                   (loop for l in (elt (color-labels res) k)
                         do (push k (elt (label-colors res) l))))
             (setf (multiplier res) (multiplier object))
             res)))

(defun merge-lists (&rest ls)
  (remove-adjacent-duplicates (sort (copy-list (reduce 'append ls)) '<)))

(defgeneric coloring-rename-label (object old new)
  (:method ((object symmetric-colorability-state) (old integer) (new integer))
    (let* ((labels (length (label-colors object)))
           (colors (length (color-labels object)))
           (res (make-empty-symmetric-colorability-state
                  labels colors)))
      ; copy the vector
      (setf (label-colors res) (subseq (label-colors object) 0))
      (setf (elt (label-colors res) new)
            (merge-lists (elt (label-colors res) new)
                         (elt (label-colors res) old)))
      (setf (elt (label-colors res) old) nil)
      (loop
        for j from 0 to (1- colors)
        for labels-here := (elt (color-labels object) j)
        do
        (setf (elt (color-labels res) j)
              (merge-lists
                (set-difference labels-here (list old new))
                (when (intersection (list old new) labels-here) (list new)))))
      (setf (multiplier res) (multiplier object))
      res)))

(defgeneric merge-colorings (c1 c2 multiplier)
  (:method ((c1 symmetric-colorability-state)
            (c2 symmetric-colorability-state)
            multiplier)
    (let* ((labels (length (label-colors c1)))
           (colors (length (color-labels c2)))
           (res (make-empty-symmetric-colorability-state labels colors)))
      (loop for k from 0 to (1- labels) do
            (setf (elt (label-colors res) k)
                  (merge-lists
                    (elt (label-colors c1) k)
                    (elt (label-colors c2) k))))
      (loop for k from 0 to (1- colors) do
            (setf (elt (color-labels res) k)
                  (merge-lists
                    (elt (color-labels c1) k)
                    (elt (color-labels c2) k))))
      (setf (multiplier res)
            (* multiplier (multiplier c1) (multiplier c2)))
      res)))

(defun permutation-from-matching (n l1 l2 m)
  (let*
      (
       (res  (make-array n :initial-element nil))
       (used (make-array n :initial-element nil))
       (min-unused l1)
       )
    (loop for p in m do
      (progn
	(setf (elt res (second p)) (first p))
	(setf (elt used (first p)) t)
	))
    (loop for k from 0 to (1- l2)
	  unless (elt res k)
	    do
	       (progn
		 (loop
		   while (elt used min-unused)
		   do (incf min-unused))
		 (setf (elt res k) min-unused)
		 (setf (elt used min-unused) t)
		 (incf min-unused)
		 ))
    (setf min-unused 0)
    (loop for k from l2 to (1- n)
	  do
	     (progn
	       (loop
		 while (elt used min-unused)
		 do (incf min-unused))
	       (setf (elt res k) min-unused)
	       (setf (elt used min-unused) t)
	       (incf min-unused)
	       ))
    res))

;; The main algorithm: matching enumeration

(defclass coloring-sym-stack-entry ()
  ((choices-made :accessor choices-made :initarg :choices-made)
   (prefix :accessor prefix :initarg :prefix)
   (edge-classes :accessor edge-classes :initarg :edge-classes)
   (used :accessor used :initarg :used)
   (refused :accessor refused :initarg :refused)
   (multiplier :accessor multiplier :initarg :multiplier)))

(defclass multiplied-matching ()
  ((content :accessor content :initarg :content)
   (multiplier :accessor multiplier :initarg :multiplier)))

(defmethod print-object ((object multiplied-matching) stream)
  (format stream "#<matching: (~{~a~#[~:; ~]~})*~a>"
          (content object)
          (multiplier object)))

(defun matchings (minimal-size allowed-pairs &key id1 id2)
  (loop with l := (length allowed-pairs)
        with max-match :=
        (reduce 'max (or (reduce 'append allowed-pairs) '(0)))
        with stack := (list (make-instance 'coloring-sym-stack-entry
                                           :choices-made 0
                                           :prefix nil
                                           :edge-classes nil
                                           :used nil
                                           :refused nil
                                           :multiplier 1))
        with res := nil
        with sibling-used :=
        (make-array (1+ max-match) :initial-element nil)
        with id2 :=
        (or id2 
            (map
              'vector 'identity 
              (loop for j from 0 to max-match collect j)))
        with id1 :=
        (or id1
            (map 'vector 'identity
                 (loop for j from 0 to l collect j)))
        with counts-id1 :=
        (let ((group-sizes (make-array l :initial-element 0))
              (count-left (make-array l :initial-element 0)))
          (loop for j from 0 to (1- l) do (incf (elt group-sizes (elt id1 j))))
          (loop for j from 0 to (1- l)
                do (setf (elt count-left j) (elt group-sizes (elt id1 j)))
                do (decf (elt group-sizes (elt id1 j))))
          count-left)
        with counts-id2 :=
        (let ((group-sizes (make-array (1+ max-match) :initial-element 0))
              (count-left (make-array (1+ max-match) :initial-element 0)))
          (loop for j from 0 to max-match do
                (incf (elt group-sizes (elt id2 j))))
          (loop for j from 0 to max-match
                do (setf (elt count-left j) (elt group-sizes (elt id2 j)))
                do (decf (elt group-sizes (elt id2 j))))
          count-left)
        for top := (pop stack)
        for choices-made := (choices-made top)
        for prefix := (prefix top)
        for edge-classes := (edge-classes top)
        for used := (used top)
        for refused := (refused top)
        for multiplier := (multiplier top)
        for refused-local := nil
        do
        (cond
          ((= choices-made l) 
           (when (>= (length prefix) minimal-size)
             (push (make-instance 'multiplied-matching
                                  :multiplier multiplier
                                  :content (reverse prefix))
                   res)))
          (t
            (loop for j from 0 to max-match
                  do (setf (elt sibling-used j) nil))
            (loop
              for e in (elt allowed-pairs choices-made)
              for class := (list (elt id1 choices-made) (elt id2 e))
              unless (or (find e used)
                         (when
                           (find class
                                 refused :test 'equalp)
                           t)
                         (elt sibling-used (elt id2 e)))
              do
              (progn
                (setf (elt sibling-used (elt id2 e)) t)
                (push class refused-local)
                (push
                  (make-instance 'coloring-sym-stack-entry
                                 :choices-made (1+ choices-made)
                                 :prefix (cons (list choices-made e) prefix)
                                 :edge-classes (cons class edge-classes)
                                 :used (cons e used) 
                                 :refused (append
                                            (cdr refused-local) refused)
                                 :multiplier (* multiplier
                                                (elt counts-id1 choices-made)
                                                (elt counts-id2 e)
                                                (/ (1+ (count
                                                         class edge-classes
                                                         :test 'equal)))))
                  stack)))
            (unless
              (< (+ (length used) (- l choices-made 1)) minimal-size)
              (push (make-instance 'coloring-sym-stack-entry
                                   :choices-made (1+ choices-made) 
                                   :prefix prefix 
                                   :edge-classes edge-classes
                                   :used used
                                   :refused (append refused-local refused)
                                   :multiplier multiplier)
                    stack))))
        unless stack return res))

;; Drop isolated labels from a colourability state

(defgeneric condense-colors (c)
  (:method ((c symmetric-colorability-state))
    (recolor c (loop with low := -1
                     with high := (length (color-labels c))
                     for set across (color-labels c)
                     collect (if set (incf low) (decf high))))))

(defgeneric drop-labels (c bitmap)
  (:method ((c symmetric-colorability-state) (bitmap array))
    (let* ((colors (length (color-labels c)))
           (used-colors 0)
           (remaining-colors 0)
           (mult (multiplier c))
           (copy (copy-symmetric-colorability-state c)))
      (loop for k upfrom 0
            for b across bitmap
            when b do (setf (elt (label-colors copy) k) nil))
      (loop for k from 0 to (1- colors)
            for labels := (elt (color-labels copy) k)
            for filtered-labels := (loop for l in labels
                                         unless (elt bitmap l) collect l)
            do (setf (elt (color-labels copy) k) filtered-labels)
            do (when labels (incf used-colors))
            do (when filtered-labels (incf remaining-colors)))
      (loop for k from (1+ remaining-colors) to used-colors
            do (setf mult (* mult k)))
      (setf (multiplier copy) mult)
      (print (list used-colors remaining-colors c copy bitmap (multiplier c) (multiplier copy)))
      (condense-colors copy))))

;; Adaptor to use purely combinatorial matchings with colorability states

(defgeneric acceptable-merges (c1 c2 edges &key forbid-identification)
  (:method ((c1 symmetric-colorability-state) 
            (c2 symmetric-colorability-state)
            (edges list)
            &key forbid-identification)
    (loop
      with colors := (length (color-labels c1))
      with labels := (length (label-colors c1))
      with n1 := (or (position nil (color-labels c1)) colors)
      with n2 := (or (position nil (color-labels c2)) colors)
      with min-match := (- (+ n1 n2) colors)
      with adds := (make-array (list labels labels) :initial-element nil)
      with isolated := (make-array (list labels) :initial-element t)
      with compatible-colors :=
      (progn
        (loop for e in edges do
              (setf (aref adds (first e) (second e)) t
                    (aref adds (second e) (first e)) t
                    (aref isolated (first e)) nil
                    (aref isolated (second e)) nil))
        ;;; Needs a reacher add annotations, work in progress
        ;(when (find-if 'identity isolated)
        ;  (setf c1 (drop-labels c1 isolated)
        ;        c2 (drop-labels c2 isolated)))
        (loop
          for k1 from 0 to (1- n1)
          collect
          (loop
            for k2 from 0 to (1- n2)
            unless
            (loop
              for l1 in (elt (color-labels c1) k1)
              when
              (loop
                for l2 in (elt (color-labels c2) k2)
                when (aref adds l1 l2)
                return t)
              return t)
            collect k2)))
      with label-set-ht1 := (make-hash-table :test 'equalp)
      with label-set-ht2 := (make-hash-table :test 'equalp)
      with id2 :=
      (unless forbid-identification
        (loop for j from 0 to (1- n2) do 
              (setf 
                (gethash (elt (color-labels c2) j) label-set-ht2)
                (gethash (elt (color-labels c2) j) label-set-ht2 j)))
        (map
          'vector 'identity
          (loop for j from 0 to (1- n2)
                collect (gethash (elt (color-labels c2) j) label-set-ht2 j)
                )))
      with id1 :=
      (unless forbid-identification
        (loop for j from 0 to (1- n1) do 
              (setf 
                (gethash (elt (color-labels c1) j) label-set-ht1)
                (gethash (elt (color-labels c1) j) label-set-ht1 j)))
        (map
          'vector 'identity
          (loop for j from 0 to (1- n1)
                collect (gethash (elt (color-labels c1) j) label-set-ht1 j)
                )))
      with matchings :=
      (matchings min-match compatible-colors
                 :id1 id1 :id2 id2)
      for m in matchings
      for p := (permutation-from-matching colors n1 n2 (content m))
      for c2-prime := (recolor c2 p)
      for c-merged := (merge-colorings c1 c2-prime (multiplier m))
      collect c-merged)))

;; Attributed state for counting

(defclass attributed-symmetric-colorability-state
  (attributed-state graph-state) ())

(defun make-attributed-symmetric-colorability-state
  (state &optional (multiplier 1))
  (let* ((state-multiplier (multiplier state))
         (copy (copy-symmetric-colorability-state state)))
    (setf (multiplier copy) 1)
    (make-instance
      'attributed-symmetric-colorability-state
      :object copy 
      :attribute (* multiplier state-multiplier)
      :combine-fun #'+)))

;; Formal interface-compatible definition of transitions

(defparameter *symmetric-colorability-annotation* t)

(defmethod graph-oplus-target ((c1 attributed-symmetric-colorability-state)
                               (c2 attributed-symmetric-colorability-state))
  (let* ((cwd (max (length (label-colors (object-of c1)))
                   (length (label-colors (object-of c2)))))
         (c1r (resize-symmetric-colorability-state (object-of c1) cwd))
         (c2r (resize-symmetric-colorability-state (object-of c2) cwd))
         (outputs (acceptable-merges c1r c2r
                                     *symmetric-colorability-annotation*
                                     :forbid-identification nil))
         (rescaled-outputs
           (mapcar (lambda (x)
                     (make-attributed-symmetric-colorability-state
                       x (* (attribute-of c1) (attribute-of c2))))
                   outputs))
         (container (when outputs (state::make-attributed-state-container
                                    rescaled-outputs #'+))))
    container))

(defmethod graph-oplus-target ((c1 symmetric-colorability-state)
                               (c2 symmetric-colorability-state))
  (let* ((cwd (max (length (label-colors c1))
                   (length (label-colors c2))))
         (c1r (resize-symmetric-colorability-state c1 cwd))
         (c2r (resize-symmetric-colorability-state c2 cwd))
         (outputs
           (acceptable-merges
             c1r c2r *symmetric-colorability-annotation*
             :forbid-identification nil))
         (container (when outputs
                      (state::make-attributed-state-container
                        outputs #'+))))
    container))

(defmethod graph-add-target (a b (c1 symmetric-colorability-state)) c1)
(defmethod graph-add-target
  (a b (c1 attributed-symmetric-colorability-state)) c1)

(defmethod graph-ren-target (a b (c symmetric-colorability-state))
  (coloring-rename-label c a b))

(defmethod graph-ren-target (a b (c attributed-symmetric-colorability-state))
  (make-attributed-symmetric-colorability-state
    (coloring-rename-label (object-of c) a b) (attribute-of c)))

(defun create-symmetric-colorability-transitions-fun (labels colors)
  (let*
    ((fun nil))
    (setf fun
          (lambda (root arg)
            (cond ((and (null arg)
                        (typep root 'cwd-constant-symbol))
                   (make-vertex-symmetric-colorability-state
                     (port-of root) (max (1+ (port-of root)) labels) colors))
                  ((null arg) nil)
                  ((typep root 'annotated-symbol)
                   (let ((*symmetric-colorability-annotation*
                           (let* ((res nil)
                                  (annotation (annotation root)))
                             (container-map
                               (lambda (pair) (push pair res))
                               annotation)
                             res)))
                     (cwd-transitions-fun root arg fun)))
                  (t (error "Annotations are required")))))
    fun))

(defun create-attributed-symmetric-colorability-transitions-fun (labels colors)
  (let*
    ((fun nil))
    (setf fun
          (lambda (root arg)
            (cond ((and (null arg)
                        (typep root 'cwd-constant-symbol))
                   (make-attributed-symmetric-colorability-state
                     (make-vertex-symmetric-colorability-state
                       (port-of root) (max (1+ (port-of root)) labels) colors)))
                  ((null arg) nil)
                  ((typep root 'annotated-symbol)
                   (let ((*symmetric-colorability-annotation*
                           (let* ((res nil)
                                  (annotation (annotation root)))
                             (container-map
                               (lambda (pair) (push pair res))
                               annotation)
                             res)))
                     (cwd-transitions-fun root arg fun)))
                  (t (error "Annotations are required")))))
    fun))

;; Make the automata a special class, because they don't behave like other
;; automata in terms of nondeterminism

(defclass symmetric-colorability-automaton (fly-automaton) ())

(defun symmetric-colorability-automaton (k cwd &key (orientation :unoriented))
  (make-instance
    'symmetric-colorability-automaton
    :transitions 
    (termauto::make-fly-transitions
      (cwd-signature cwd :orientation orientation)
      (create-symmetric-colorability-transitions-fun cwd k)
      nil nil)
    :name "colorability"
    :final-state-fun #'state-final-p
    :output-fun #'identity))

(defun attributed-symmetric-colorability-automaton
  (k cwd &key (orientation :unoriented))
  (assert (> cwd 0))
  (make-cwd-automaton
    (cwd-signature cwd :orientation orientation)
    (create-attributed-symmetric-colorability-transitions-fun cwd k)
    :name "colorability-count"))

;; Counting needs a different target function, and given the different
;; nondeterminism it is not combinable anyway

(defmethod count-run-transducer ((a symmetric-colorability-automaton))
  (let* ((sample-state (compute-target (input-cwd-term "a") a))
         (cwd (length (label-colors sample-state)))
         (colors (length (color-labels sample-state)))
         (combinations
           (coerce
             (loop for j downfrom (1+ colors) to 1
                   for c := 1 then (* c j)
                   collect c)
             'vector)))
    (termauto::make-fly-automaton-with-transitions
      (termauto::make-fly-transitions
        (signature a)
        (create-attributed-symmetric-colorability-transitions-fun cwd colors)
        nil nil)
      #'state-final-p
      (lambda (target)
        (if (null target) 0
          (loop
            for state in (container-contents target)
            for n := (attribute-of state)
            for coloring := (object-of state)
            for colors-used := (count-if 'identity (color-labels coloring))
            for color-combinations := (elt combinations colors-used)
            sum (* n color-combinations)
            )))
      "colorability-counter")))

(defmethod compute-count ((term term)
                          (automaton symmetric-colorability-automaton))
  (run-count term automaton))


;;;;;;;;;;; Demo calls under this line

#+nil

(time
  (identity
    (autograph::recognized-p
      (autograph::compute-add-annotation (autograph::petersen))
      (autograph::symmetric-colorability-automaton 5 0))))

#+nil

(time
  (identity
    (autograph::recognized-p
      (autograph::compute-add-annotation (autograph::petersen))
      (autograph::kcolorability-automaton 5 :cwd 10))))

#+nil

(time
  (identity
    (autograph::compute-target
     (autograph::compute-add-annotation
        (autograph::input-cwd-term "add_a_b(oplus(oplus(a,a),oplus(b,b)))"))
      (autograph::symmetric-colorability-automaton 5 0))))


#+nil

(autograph::compute-target
  (autograph::compute-add-annotation
    (decomp::cwd-decomposition
      (graph:graph-from-paths `((1 2 3 4 5)))
      t))
  (autograph::attributed-symmetric-colorability-automaton 3 3))

#+nil

(time
  (identity
    (autograph::run-count
     (autograph::compute-add-annotation
        (autograph::petersen))
      (autograph::symmetric-colorability-automaton 3 0))))

#+nil

(trace
  autograph::recolor autograph::acceptable-merges autograph::matchings 
  autograph::merge-colorings autograph::graph-oplus-target)

#+nil

(list
  (time
    (identity
      (autograph::recognized-p
        (autograph::compute-add-annotation (autograph::mcgee))
        (autograph::symmetric-colorability-automaton 3 10))))
  (time
    (identity
      (autograph::recognized-p
        (autograph::compute-add-annotation (autograph::mcgee))
        (autograph::kcolorability-automaton 3 :cwd 10)))))

#+nil

(autograph::compute-count
  (autograph::compute-add-annotation (autograph::petersen))
  (autograph::symmetric-colorability-automaton 3 0))

#+nil

(list
  (time
    (identity
      (autograph::recognized-p
        (autograph::compute-add-annotation (autograph::petersen))
        (autograph::symmetric-colorability-automaton 3 10))))
  (time
    (identity
      (autograph::recognized-p
        (autograph::compute-add-annotation (autograph::petersen))
        (autograph::kcolorability-automaton 3 :cwd 10)))))

#+nil

(list
  (time
    (autograph::run-count
      (autograph::compute-add-annotation
        (autograph::input-cwd-term "add_a_b(oplus(oplus(a,a),oplus(b,b)))"))
      (autograph::symmetric-colorability-automaton 3 10)))
  (time
    (autograph::run-count
      (autograph::compute-add-annotation
        (autograph::input-cwd-term "add_a_b(oplus(oplus(a,a),oplus(b,b)))"))
     (autograph::kcolorability-automaton 3 :cwd 10))))

#+nil

(let* ((term (autograph::petersen))
       (term (decomp::cwd-decomposition
               (graph:graph-from-paths
                 `((1 2)))
               t))
       (term (autograph::petersen))
       (term-graph (autograph::cwd-term-to-graph term))
       (colors 5)
       (chromatic-polynomial
         (when nil (time (graph:chromatic-polynomial term-graph))))
       (at (autograph::compute-add-annotation term))
       (cwd (clique-width:clique-width term))
       (permutations (loop for k from 1 to colors
                           for fact := k then (* fact k)
                           finally (return fact)))
       (sca (autograph::symmetric-colorability-automaton colors cwd))
       (asca (autograph::attributed-symmetric-colorability-automaton
               colors cwd))
       (kca (autograph::kcolorability-automaton colors :cwd cwd))
       (my (time (autograph::run-count at sca)))
       (chromatic-value
         (when chromatic-polynomial
           (let ((v (graph::polynomial-value
                      chromatic-polynomial colors)))
             (format t "Answer is: ~s [guess: ~s]~%" v my)
             v)))
       (old (time (autograph::run-count at kca)))
       (recoloring-package (find-package :recoloring))
       (recoloring-term
         (and recoloring-package
              (funcall
                (find-symbol "MAKE-ADD-ANNOTATIONS" recoloring-package)
                (funcall
                  (find-symbol "MAKE-LABELS-ANNOTATIONS" recoloring-package)
                  (funcall
                    (find-symbol "CONVERT-FROM-GRAPH-TERM" recoloring-package)
                    term)))))
       (recoloring-counter
         (and recoloring-package
              (find-symbol "CANONICAL-COLORINGS" recoloring-package)))
       (recoloring-count
         (and recoloring-package
              (reduce
                '+
                (mapcar
                  'first
                  (time (funcall recoloring-counter
                                 recoloring-term colors
                                 :forbid-identification t))))))
       (recoloring-normalised (* (or recoloring-count 0) permutations)))
  (list
    term
    :my my
    :rec recoloring-normalised :old old :truth chromatic-value))
