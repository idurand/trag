; n-coloring encoding taken from the clingo manual

(defun graph-arcs-to-clingo (arcs colors)
 (let*
  (
   (n (1+ (reduce 'max (reduce 'append arcs))))
  )
  (format
   nil
   "
   #const n = ~a.
   { color(X,1..n) } = 1 :- node(X).
   :- edge(X,Y), color(X,C), color(Y,C).
   node(1..~a).
   ~{edge(~{~a,~a~}).~%~}
   "
   colors
   n
   arcs)))

(defun graph-term-to-clingo (term colors)
 (graph-arcs-to-clingo
   (autograph::graph-to-eedges
     (autograph::cwd-term-to-graph term))
   colors))
