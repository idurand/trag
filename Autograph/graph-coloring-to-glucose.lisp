(defun graph-arcs-to-glucose (arcs colors)
 (let*
  (
   (n (1+ (reduce 'max (reduce 'append arcs))))
   (m (length arcs))
  )
  (format nil "p cnf ~a ~a~%~{~{~a ~}0~%~}~{~{~{-~a ~}0~%~}~}"
   (* colors n) (+ n (* colors m))
   (loop
    for k from 0 to (1- n)
    collect
    (loop
     for j from 1 to colors
     collect (+ j (* colors k))))
   (loop
    for e in arcs
    collect
    (loop
     for j from 1 to colors
     collect (list (+ j (* colors (first e))) (+ j (* colors (second e))))))
  )
 ))

(defun graph-term-to-glucose (term colors)
 (graph-arcs-to-glucose
   (autograph::graph-to-eedges
     (autograph::cwd-term-to-graph term))
   colors))
