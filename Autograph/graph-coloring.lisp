;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass colors-state (graph-state)
  ((color-fun :initarg :color-fun :reader color-fun :type color-fun)))

(defmethod ports-of ((colors-state colors-state))
  (ports-of (color-fun colors-state)))

(defmethod state-final-p ((co colors-state)) t)

(defmethod print-object ((colors-state colors-state) stream)
  (let ((color-fun (color-fun colors-state)))
    (print-port-fun color-fun stream)))

(defmethod compare-object ((co1 colors-state) (co2 colors-state))
  (compare-object (color-fun co1) (color-fun co2)))

(defgeneric make-colors-state (color-fun)
  (:method ((color-fun color-fun))
    (make-instance 'colors-state :color-fun color-fun)))

(defmethod apply-port-mapping ((port-mapping port-mapping) (colors-state colors-state))
  (make-colors-state
   (apply-port-mapping (color-fun colors-state))))

(defgeneric coloring-applicable-annotation-p (annotated-symbol arg)
  (:method ((root annotated-symbol) (arg list))
    (and (oplus-symbol-p root)
	 (let ((pairs (annotation root))
	       (color-fun (merge-color-fun-gen (mapcar #'color-fun arg))))
	   (container-find-if-not
	    (lambda (pair)
	      (let ((a (first pair))
		    (b (second pair)))
		(colors-intersection-empty-p
		 (get-port-value a color-fun)
		 (get-port-value b color-fun))))
	    pairs)))))

(defmethod graph-oplus-target ((s1 colors-state) (s2 colors-state))
  (make-colors-state (merge-color-fun (color-fun s1) (color-fun s2))))

(defmethod graph-oplus-target-gen ((s1 colors-state) (s2 colors-state) (states list))
  (make-colors-state
   (merge-color-fun-gen (mapcar #'color-fun (cons s1 (cons s2 states))))))

(defmethod graph-add-target
    (a b (colors-state colors-state))
  (let ((color-fun (color-fun colors-state)))
    (when (colors-intersection-empty-p
	   (get-port-value a color-fun)
	   (get-port-value b color-fun))
      colors-state)))

(defmethod graph-ren-target (a b (colors-state colors-state))
  (make-colors-state (ren-color-fun a b (color-fun colors-state))))

(defgeneric coloring-transitions-fun (root arg)
  (:method ((root color-constant-symbol) (arg (eql nil)))
    (make-colors-state
     (make-port-color-fun (color root) (port-of root))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'coloring-transitions-fun))
  (:method ((root annotated-symbol) (arg list))
    (if (add-symbol-p root) ;; the add has already been treated in oplus below
	(car arg)
	(let ((applicable-annotation
		(coloring-applicable-annotation-p root arg)))
	  (unless applicable-annotation
	    (coloring-transitions-fun (symbol-of root) arg))))))

(defun coloring-automaton (k &key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton 
   (cwd-color-signature cwd k :orientation orientation)
   #'coloring-transitions-fun
   :name (cwd-automaton-name (format nil "COLORING(C~A)" k) cwd)))

(defun table-coloring-automaton (k cwd &key (orientation :unoriented))
  (compile-automaton (coloring-automaton k :cwd cwd :orientation orientation)))

(defun coloring-transducer (k afun &key (orientation :unoriented))
  (constants-color-projection
   (attribute-automaton (coloring-automaton k :orientation orientation) afun)))

(defun count-coloring-transducer (k &key (orientation :unoriented))
  (coloring-transducer k *count-afun* :orientation orientation))

(defgeneric count-colorings (cwd-term k &key orientation)
  (:method ((term term) (k integer) &key (orientation :unoriented))
    (let ((final-value
	    (compute-final-value
	     term
	     (count-coloring-transducer k :orientation orientation))))
      (or final-value 0))))

(defun spectrum-coloring-transducer (k  &key (orientation :unoriented))
  (coloring-transducer k (color-spectrum-afun k) :orientation orientation))

(defun multi-spectrum-coloring-transducer (k &key (orientation :unoriented))
  (coloring-transducer k (color-multi-spectrum-afun k) :orientation orientation))

(defun assignment-coloring-transducer (k  &key (orientation :unoriented))
  (coloring-transducer k *assignment-afun* :orientation orientation))

(defgeneric k-coloring (k term)
  (:method ((k integer) (cwd-term term))
    (setq cwd-term (term-num-leaves cwd-term))
    (get-etis-assignment
     cwd-term
     (call-enumerator (k-coloring-enumerator cwd-term k)))))  

(defgeneric k-colorings (cwd-term k &key orientation)
  (:method ((cwd-term term) (k integer) &key (orientation :unoriented))
    (mapcar
     (lambda (pa) (terms::positions-assignment-to-etis cwd-term pa))
     (compute-value cwd-term (assignment-coloring-transducer k :orientation orientation)))))

(defun format-k-coloring (k pairs &optional (stream t))
  (let ((v (make-array k :initial-element nil)))
    (loop for pair in pairs
	  do (push (car pair) (aref v (1- (cdr pair)))))
    (loop initially (format stream " [")
	  for color from 0 below k
	  unless (endp (aref v color))
	  do (format stream "C~A:~A " (1+ color) (sort (aref v color) #'<))
	  finally (format stream "]~%"))))

(defun format-k-colorings (k lpairs &optional (stream t))
  (loop
    for i from 1
    for pairs in lpairs
    do (format-k-coloring k pairs stream)))

(defun k-coloring-enumerator (term k  &key (orientation :unoriented))
  (final-value-enumerator
   term
   (assignment-coloring-transducer k :orientation orientation)))


(defun test-afun ()
  (let ((term (cwd-term-cycle 4)))
    (assert (= (compute-final-value term (constants-color-projection (attribute-automaton (coloring-automaton 3) *count-afun*)))
	       (compute-final-value term (count-coloring-transducer 3))))
    (assert (compare-object 
	     (compute-final-value term (constants-color-projection (attribute-automaton (coloring-automaton 3)
											(color-spectrum-afun 3))))
	     (compute-final-value term (spectrum-coloring-transducer 3))))
    (assert (compare-object 
	     (compute-final-value term (constants-color-projection (attribute-automaton (coloring-automaton 3)
											(color-multi-spectrum-afun 3))))
	     (compute-final-value term (multi-spectrum-coloring-transducer 3))))
    (assert (compare-object 
	     (compute-final-value term (constants-color-projection (attribute-automaton (coloring-automaton 3) *assignment-afun*)))
	     (compute-final-value term (assignment-coloring-transducer 3))))))
