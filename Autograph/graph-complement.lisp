;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun basic-set-complement-automaton (&key (cwd 0) (orientation :unoriented))
  "automaton for X1 = C(X2) with m = 2"
  (rename-object
   (vbits-fun-automaton
    2
    (lambda (vbits)
      (= (+ (aref vbits 0) (aref vbits 1)) 1))
    :cwd cwd :orientation orientation)
   (format nil "~A-COMPLEMENT-X1-C(X2)" cwd)))

(defun table-basic-complement-automaton (cwd &key (orientation :unoriented))
  "table-automaton for X1 = C(X2) with m = 2"
 (compile-automaton
   (basic-set-complement-automaton :cwd cwd :orientation orientation)))

(defun set-complement-automaton (m j1 j2 &key (cwd 0) (orientation :orientation))
  (rename-object
   (y1-y2-to-xj1-xj2
    (basic-set-complement-automaton :cwd cwd :orientation orientation) m j1 j2)
   (format nil "~A-COMPLEMENT-X~A-X~A-~A" cwd j1 j2 m)))

(defun table-set-complement-automaton (m j1 j2 cwd &key (orientation :unoriented))
  (compile-automaton
   (set-complement-automaton m j1 j2 :cwd cwd :orientation orientation)))
