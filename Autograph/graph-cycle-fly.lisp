;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun make-has-cycle-graph-spec
    (len cwd &key (signature nil) (automata nil) (tautomata nil))
  (make-graph-spec cwd 'has-cycle-graph-spec
		   signature automata tautomata
		   :len len))

(defun has-cycle-spec (len cwd)
  (assert (>= len 2))
  (let*
      ((m len)
       (edge-automata nil)
       (nedge-automata nil)
       (nequality-automata nil)
       (tautomata ()))
    (mapc
     (lambda (pair)
       (let
	   ((j1 (first pair))
	    (j2 (second pair)))
	 (if
	  (or (and (= 1 j1) (= len j2))
	      (= 1 (- j2 j1)))
	  (push (edge-automaton m j1 j2 cwd) edge-automata)
	  (progn
	    (push
	     (complement-automaton (edge-automaton m j1 j2 cwd)) nedge-automata)
	    (push
	     (complement-automaton
	      (equal-automaton m j1 j2 cwd)) nequality-automata)
	    ))))
     (distinct-pairs (set-iota m)))
    (push (make-tautomaton "Edges" edge-automata) tautomata)
    (push (make-tautomaton "Nedges" nedge-automata) tautomata)
    (push (make-tautomaton "Nequalities" nequality-automata) tautomata)
    (make-has-cycle-graph-spec
     len
     cwd
     :signature (cwd-vbits-signature cwd m)
     :automata edge-automata
     :tautomata tautomata)))

(defun load-has-cycle-spec (len cwd)
  (set-current-spec (has-cycle-spec len cwd)))

(defun test-has-cycle-step1 (len cwd)
  (with-spec (has-cycle-spec len cwd)
    (when *debug*
	(format *output-stream* "construction of ~A-has-cycle-~A~%" cwd len))
    (let ((edges (automata (get-tautomaton "Edges")))
	  (nedges (automata (get-tautomaton "Nedges")))
	  (nequalities (automata (get-tautomaton "Nequalities")))
	  a)
      (setf a
	    (op-automata-list
	     (append edges nedges nequalities)
	     :op 'intersection-automaton-compatible
	     :cop 'minimize-automaton))
      (rename-and-save (format nil "min-~A-has-cycle-x-~A" cwd len) a))))

(defun test-has-cycle-step2 (len cwd a)
  (setf a (vbits-projection a len))
  (rename-and-save (format nil "~A-has-cycle-~A" cwd len) a))

(defun test-has-cycle-step3 (len cwd a)
  (format *output-stream* "determinizing ")
  (with-time (setf a (determinize-automaton a)))
  (rename-and-save (format nil "~A-has-cycle-~A-det" cwd len) a)
  (format *output-stream* "minimizing ")
  (with-time (setf a (minimize-automaton a)))
  (rename-and-save (format nil "~A-has-cycle-~A-min" cwd len) a))

(defun has-cycle-automaton (len cwd &optional (determinize nil))
  (assert (> len 2))
  (let ((a (test-has-cycle-step1 len cwd)))
    (setf a (test-has-cycle-step2 len cwd a))
    (when determinize 
      (setf a (test-has-cycle-step3 len cwd a)))
    a))
