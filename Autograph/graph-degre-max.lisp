;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defvar *degre* 1)

(defclass degre-graph-spec (graph-spec)
  ((degre :initform 2 :initarg :degre :accessor degre)))

(defun make-degre-graph-spec
    (degre cwd &key (signature nil) (automata nil) (tautomata nil))
  (make-graph-spec cwd 'degre-graph-spec
		   signature automata tautomata
		   :degre degre))

(defun degre-max-spec (d &optional (cwd 0))
  (let* ((m (+ 2 d))
	 (edge-automata
	   (mapcar (lambda (i)
		     (table-edge-automaton m i m cwd))
		   (set-iota (1- m))))
	 (equality-automata
	   (mapcar
	    (lambda (pair)
	      (table-equal-automaton m (car pair) (second pair) cwd :orientation nil))
	    (distinct-pairs (set-iota (1- m)))))
	 (tautomata ()))
    (push (make-tautomaton "Edges" edge-automata) tautomata)
    (push (make-tautomaton "Equalities" equality-automata) tautomata)
    (make-degre-graph-spec
     d
     cwd
     :signature (cwd-vbits-signature cwd m)
     :automata (append edge-automata equality-automata)
     :tautomata tautomata)))

(defun load-degre-spec (d cwd)
  (set-current-spec (degre-max-spec d cwd)))

(defun test-degre-max-step1 (d cwd)
  (with-spec (degre-max-spec d cwd)
    (let ((nequalities  (mapcar #'complement-automaton
				(automata (get-tautomaton "Equalities"))))
	  (edges (automata (get-tautomaton "Edges")))
	  (m (+ 2 d))
	  a)
      (when *debug*
	(format *output-stream* "construction of ~A-degre-x-sup-~A~%" cwd d))
;;      (with-time-to-output-stream
	  (setf a (op-automata-list
		   (append edges nequalities)
		   :op 'intersection-automaton-compatible
		   :cop 'minimize-automaton))
      ;;)
      (rename-and-save (format nil "~A-degre-x~A-sup-~A-min" cwd m d) a)
      (when *debug*
	(format *output-stream* " projection ~%"))
      (setf a (vbits-projection
	       (minimize-automaton a)
	       (list (1+ d))))
      (rename-and-save (format nil "~A-degre-x-sup-~A" cwd d) a))))

(defun test-degre-max-step2-det (d cwd a)
  (unless (deterministic-p a)
    (when *debug*
      (format *output-stream* " determinized"))
    (setf a (determinize-automaton a)))
  (rename-and-save (format nil "~A-degre-x-sup-~A-det" cwd d) a))

(defun test-degre-max-step2-min (d cwd a)
  (when *debug*
    (format *output-stream* " minimizing"))
;;  (with-time-to-output-stream
  (setf a (nminimize-automaton a))
  ;;)
  (rename-and-save (format nil "~A-degre-x-sup-~A-min" cwd d) a))

(defun test-degre-max-step2 (d cwd a)
  (setf a (test-degre-max-step2-det d cwd a))
  (setf a (test-degre-max-step2-min d cwd a))
  (when *debug*
    (format *output-stream* " projection"))
;;  (with-time-to-output-stream
      (setf a (vbits-projection a))
    ;;)
  (rename-and-save (format nil "~A-exists-x-degre-sup-~A" cwd d) a))

(defun test-degre-max-step3-det (d cwd a &key fly)
  (when *debug*
    (format *output-stream* " determinized"))
;;  (with-time-to-output-stream
  (setf a (determinize-automaton-to-x a fly))
  ;;)
  (rename-and-save (format nil "~A-exists-x-degre-sup-~A-det" cwd d) a))

(defun test-degre-max-step3-min (d cwd a)
  (when *debug*
    (format *output-stream* " minimizing"))
;;  (with-time-to-output-stream
  (setf a (minimize-automaton a))
;;  )
  (rename-and-save (format nil "~A-degre-sup-~A-min" cwd d) a)
  (when *debug*
    (format *output-stream* " complemented"))
;;  (with-time-to-output-stream
  (setf a (complement-automaton a))
  ;;)
  (when *debug*
    (format *output-stream* "~%"))
  (rename-and-save (format nil "~A-degre-infe-~A-min" cwd d) a))

(defun test-degre-max-step3 (d cwd a &key (fly nil))
  (setf a (test-degre-max-step3-det d cwd a :fly fly))
  (unless fly
    (setf a (test-degre-max-step3-min d cwd a)))
  a)

(defun test-degre-max (d cwd &key (fly nil))
  (let (a)
;;    (with-time-to-output-stream
	(progn
	  (setf a (test-degre-max-step1 d cwd))
	  (setf a (test-degre-max-step2 d cwd a))
	  (setf a (test-degre-max-step3 d cwd a :fly fly)))
;;    )
    a))

(defun table-degre-max-automaton (d cwd)
  (test-degre-max d cwd :fly nil))

(defun degre-max-automaton (d &optional (cwd 0))
  (test-degre-max d cwd :fly t))
