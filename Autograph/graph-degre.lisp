;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun basic-degre0-automaton (&optional (cwd 0))
  (let* ((p5 (y1-y2-to-xj1-xj2
	      (complement-automaton
	       (basic-subset-automaton :cwd cwd :orientation nil))
	      3 3 1))
	 (p4 (intersection-automaton-compatible
	      (subgraph-singleton-automaton 3 3 :cwd cwd :orientation nil)
	      p5))
	 (p3
	   (union-automaton 
	    p4
	    (edge-automaton 3 2 3 :cwd cwd)))
	 (p2 (vbits-projection p3 '(1 2)))
	 (p1 (complement-automaton p2))
	 (a1 (complement-automaton (subgraph-empty-automaton 2 1 :cwd cwd :orientation nil)))
	 (a2 (subgraph-stable-automaton 2 1 :cwd cwd :orientation nil))
	 (a3 (x1-to-xj1-intersection-xj2
	      (subgraph-empty-automaton 1 1 :cwd cwd :orientation nil) 2 1 2))
	 (p0 (intersection-automaton-compatible-gen a1 a2 a3 p1)))
    (rename-object p0 (format nil "~A-DEG0-X1-X2" cwd))))

(defun table-basic-degre0-automaton (cwd)
  (compile-automaton (basic-degre0-automaton cwd)))

(defclass port-count-state (graph-state)
  ((port :initarg :port :reader port)
   (num :initarg :num :reader num)))

(defmethod print-object ((s port-count-state) stream)
  (format stream "~A:~A" (port-to-string (port s)) (num s)))

(defmethod compare-object ((s1 port-count-state) (s2 port-count-state))
  (and (= (port s1) (port s2)) (= (num s1) (num s2))))

(defun make-port-count-state (port num)
  (make-instance 'port-count-state :port port :num num))

(defmethod ports-of ((s port-count-state))
  (make-ports-from-port (port s)))

(defmethod graph-ren-target (a b (s port-count-state))
  (make-port-count-state (subst b a (port s)) (num s)))

(defclass port-counts-state (graph-state)
  ((port-counts :initarg :port-counts :reader port-counts)))

(defmethod print-object ((s port-counts-state) stream)
  (display-sequence (port-counts s) stream))

(defmethod ports-of ((s port-counts-state))
  (make-ports (mapcar #'port (port-counts s))))

(defgeneric degre-port-count (a port-counts-state)
  (:method (a (s port-counts-state))
    (let ((pc (find a (port-counts s) :key #'port :test #'=)))
      (if pc (num pc) 0))))

(defun compute-port-count (a port-counts)
  (let ((n (reduce
	    #'+
	    (mapcar #'num
		    (remove a port-counts :key #'port :test-not #'=) )
	    :initial-value 0)))
    (if (> n 3) 3 n)))

(defun clean-port-counts (port-counts)
  (let ((ports (ports-of port-counts)))
    (loop
      for port in (container-contents ports)
      collect (make-port-count-state
	       port (compute-port-count port port-counts)))))

(defun make-port-counts-state (port-counts)
  (make-instance
   'port-counts-state
   :port-counts
   (clean-port-counts port-counts)))

(defgeneric port-counts-union (port-counts-state1 port-counts-state2)
  (:method ((s1 port-counts-state) (s2 port-counts-state))
    (make-port-counts-state (append (port-counts s1) (port-counts s2)))))

(defclass xy-state (graph-state)
  ((x :initarg :x :reader x)
   (y :initarg :y :reader y)))

(defmethod ports-of ((s xy-state))
  (make-ports (list (x s) (y s))))

(defmethod print-object ((s xy-state) stream)
  (format stream "~A-~A" (port-to-string (x s)) (port-to-string (y s))))

(defmethod compare-object ((s1 xy-state) (s2 xy-state))
  (and (= (x s1) (x s2)) (= (y s1) (y s2))))

(defun make-xy-state (x y)
  (make-instance 'xy-state :x x :y y))

(defclass xys-state (graph-state)
  ((xys :initarg :xys :reader xys)))

(defmethod ports-of ((s xys-state))
  (ports-of (xys s)))

(defmethod print-object ((s xys-state) stream)
  (display-sequence (xys s) stream))

(defun make-xys-state (xys)
  (assert (listp xys))
  (make-instance
   'xys-state :xys
   (sort-components
    (my-remove-duplicates xys))))

(defclass xyy-state (graph-state)
  ((x :initarg :x :reader x)
   (y1 :initarg :y1 :reader y1)
   (y2 :initarg :y2 :reader y2)))

(defmethod ports-of ((s xyy-state))
  (make-ports (list (x s) (y1 s) (y2 s))))

(defun make-xyy-state (x y1 y2)
  (let ((pair (sort (list y1 y2) #'<)))
    (make-instance 'xyy-state :x x :y1 (first pair) :y2 (second pair))))

(defmethod print-object ((s xyy-state) stream)
  (format stream "~A-~A~A" (port-to-string (x s))  (port-to-string (y1 s)) (port-to-string (y2 s))))

(defmethod compare-object ((s1 xyy-state) (s2 xyy-state))
  (and (= (x s1) (x s2)) (= (y1 s1) (y1 s2)) (= (y2 s1) (y2 s2))))

(defclass xyys-state (graph-state)
  ((xyys :initarg :xyys :reader xyys)))

(defmethod print-object ((s xyys-state) stream)
  (display-sequence (xyys s) stream))

(defun make-xyys-state (xyys)
  (make-instance
   'xyys-state
   :xyys (sort-components (my-remove-duplicates xyys))))

(defmethod ports-of ((s xyys-state))
  (ports-of (xyys s)))

(defclass graph-degre2-state (graph-state)
  ((isolated-ports-x :initarg :isolated-ports-x :reader isolated-ports-x)
   (port-counts-y :initarg :port-counts-y :reader port-counts-y)
   (edges-xy :initarg :edges-xy :reader edges-xy)
   (edges-xyy :initarg :edges-xyy :reader edges-xyy)))

(defmethod print-object ((s graph-degre2-state) stream)
  (format stream "<~A|~A|~A|~A>" (isolated-ports-x s) (port-counts-y s) (edges-xy s) (edges-xyy s)))

(defmethod compare-object ((s1 graph-degre2-state) (s2 graph-degre2-state))
  (and
   (compare-object (isolated-ports-x s1) (isolated-ports-x s2))
   (compare-object (port-counts-y s1) (port-counts-y s2))
   (compare-object (edges-xy s1) (edges-xy s2))
   (compare-object (edges-xyy s1) (edges-xyy s2))))

(defun make-graph-degre2-state (isolated-ports-x port-counts-y edges-xy edges-xyy)
  (make-instance
   'graph-degre2-state
   :isolated-ports-x isolated-ports-x
   :port-counts-y port-counts-y
   :edges-xy edges-xy
   :edges-xyy edges-xyy))

(defmethod ports-of ((s graph-degre2-state))
  (reduce
   #'ports-union
   (list
    (isolated-ports-x s)
    (ports-of (port-counts-y s))
    (ports-of (edges-xy s))
    (ports-of (edges-xyy s)))
   :initial-value (make-empty-ports)))

(defmethod graph-ren-target (a b (s xy-state))
  (make-xy-state (subst b a (x s)) (subst b a (y s)) ))

(defmethod graph-ren-target (a b (s xyy-state))
  (make-xyy-state (subst b a (x s)) (subst b a (y1 s)) (subst b a (y2 s))))

(defmethod graph-ren-target (a b (s xys-state))
  (make-xys-state
   (mapcar (lambda (xy) (graph-ren-target a b xy))
	   (xys s))))

(defmethod graph-ren-target (a b (s xyys-state))
  (make-xyys-state
   (mapcar (lambda (xyy) (graph-ren-target a b xyy))
	   (xyys s))))

(defmethod graph-ren-target (a b (s port-counts-state))
  (make-port-counts-state
   (mapcar (lambda (pcs) (graph-ren-target a b pcs))
	   (port-counts s))))

(defgeneric pi-x (xstate)
  (:method ((l list))
    (ports-union-gen (mapcar #'pi-x l)))
  (:method ((s xy-state)) (make-ports (list (x s))))
  (:method ((s xyy-state)) (make-ports (list (x s))))
  (:method ((s xys-state))
    (reduce #'ports-union
	    (mapcar #'pi-x (xys s))
	    :initial-value (make-empty-ports)))
  (:method ((s xyys-state))
    (reduce #'ports-union
	    (mapcar #'pi-x (xyys s))
	    :initial-value (make-empty-ports)))
  (:method ((s graph-degre2-state))
    (reduce #'ports-union
	    (list
	     (isolated-ports-x s)
	     (pi-x (edges-xy s))
	     (pi-x (edges-xyy s))))))

(defmethod graph-ren-target (a b (s graph-degre2-state))
  (make-graph-degre2-state
   (graph-ren-target a b (isolated-ports-x s))
   (graph-ren-target a b (port-counts-y s))
   (graph-ren-target a b (edges-xy s))
   (graph-ren-target a b (edges-xyy s))))

(defgeneric new-graph-degre2-state-for-add (a b graph-degre2-state)
  (:method (a b (s graph-degre2-state))
    (let ((bc (degre-port-count b (port-counts-y s)))
	  (xyys (xyys (edges-xyy s)))
	  (xys (xys (edges-xy s))))
      (if (zerop bc)
	  s
	  (unless
	      (or
	       (= 3 bc)
	       (and
		(= 1 bc)
		(find-if
		 (lambda (xyy)
		   (and (= a (x xyy)) (/= b (y1 xyy)) (/= b (y2 xyy))))
		 xyys))
	       (and
		(= 2 bc)
		(or
		 (find-if
		  (lambda (xy) (and (= a (x xy)) (/= b (y xy))))
		  xys)
		 (find-if
		  (lambda (xyy)
		    (and
		     (= a (x xyy))
		     (or (/= b (y1 xyy)) (/= b (y2 xyy)))))
		  xyys))))
	    (let* ((edges-ay (remove-if-not
			      (lambda (xy)
				(and (= a (x xy)) (/= b (y xy))))
			      xys))
		   (edges-ay1y (remove-if-not
				(lambda (xyy)
				  (and (= a (x xyy)) (/= b (y1 xyy))))
				xyys))
		   (edges-ayy2 (remove-if-not
				(lambda (xyy)
				  (and (= a (x xyy)) (/= b (y2 xyy))))
				xyys))
		   (new-xyys
		     (append
		      (mapcar
		       (lambda (ay) (make-xyy-state a b (y ay)))
		       edges-ay)
		      (mapcar
		       (lambda (ayy) (make-xyy-state a b (y1 ayy)))
		       edges-ay1y)
		      (mapcar
		       (lambda (ayy) (make-xyy-state a b (y2 ayy)))
		       edges-ayy2)
		      xyys)))
	      (make-graph-degre2-state
	       (ports-remove a (isolated-ports-x s))
	       (port-counts-y s)
	       (make-xys-state
		(if edges-ay
		    (set-difference xys edges-ay)
		    (if (and (= bc 1) (ports-member a (isolated-ports-x s)))
			(cons (make-xy-state a b) xys)
			xys)))
	       (make-xyys-state
		(if (and (= 2 bc) (ports-member a (isolated-ports-x s)))
		    (cons (make-xyy-state a b b) new-xyys)
		    new-xyys)))))))))

(defmethod graph-add-target (a b (s graph-degre2-state))
  (let* ((ports (ports-of s))
	 (ma (ports-member a ports))
	 (mb (ports-member b ports)))
    (if (and ma mb)
	(let* ((pi-x (pi-x s))
	       (ma-x (ports-member a pi-x))
	       (mb-x (ports-member b pi-x)))
	  (unless (and ma-x mb-x)
	    (cond
	      (ma-x (new-graph-degre2-state-for-add a b s))
	      (mb-x (new-graph-degre2-state-for-add b a s)))))
	s)))

(defmethod graph-oplus-target ((s1 graph-degre2-state) (s2 graph-degre2-state))
  (make-graph-degre2-state
   (ports-union (isolated-ports-x s1) (isolated-ports-x s2))
   (port-counts-union (port-counts-y s1) (port-counts-y s2))
   (make-xys-state (append (xys (edges-xy s1)) (xys (edges-xy s2))))
   (make-xyys-state (append (xyys (edges-xyy s1)) (xyys (edges-xyy s2))))))

(defgeneric degre2-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg null))
    (let* ((name (name root))
	   (vbits (vbits root))
	   (b0 (aref vbits 0))
	   (b1 (aref vbits 1)))
      (if (= 1 b0)
	  (when (zerop b1)
	    (make-graph-degre2-state
	     (make-ports-from-port (string-to-port name))
	     (make-port-counts-state '())
	     (make-xys-state '())
	     (make-xyys-state '())))
	  (if (= 1 b1)
	      (make-graph-degre2-state
	       (make-empty-ports)
	       (make-port-counts-state
		(list (make-port-count-state (port-of root) 1)))
	       (make-xys-state '())
	       (make-xyys-state '()))
	      (make-graph-degre2-state
	       (make-empty-ports)
	       (make-port-counts-state '())
	       (make-xys-state '())
	       (make-xyys-state '()))))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'degre2-transitions-fun))
  (:method ((root (eql *empty-symbol*)) (arg null))
    (cwd-transitions-fun root arg #'degre2-transitions-fun)))

(defmethod state-final-p ((s graph-degre2-state))
  (and
   (not (ports-empty-p (pi-x s)))
   (ports-empty-p (isolated-ports-x s))
   (endp (xys (edges-xy s)))))

(defun basic-degre2-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2)
   #'degre2-transitions-fun
   :name (cwd-automaton-name "DEGRE2" cwd)))

(defun one-hole-spec (cwd)
  (let* ((m 6)
	 (degre0 (basic-degre0-automaton cwd))
	 (degre0-automata
	   (list
	    (rename-object
	     (y1-y2-to-xj-xj1-union-xj2 degre0 m 1 4 5)
	     "DEGRE0-X1-X4-U-X5")
	    (rename-object
	     (y1-y2-to-xj-xj1-union-xj2 degre0 m 2 3 4)
	     "DEGRE0-X2-X3-U-X4")))
	 (degre2 (basic-degre2-automaton cwd))
	 (degre2-automata
	   (list
	    (rename-object
	     (y1-y2-to-xj-xj1-union-xj2 degre2 m 1 2 3)
	     "DEGRE2-X1-X2-U-X3")
	    (rename-object
	     (y1-y2-to-xj-xj1-union-xj2 degre2 m 2 1 5)
	     "DEGRE2-X2-X1-U-X5")
	    (rename-object
	     (y1-y2-to-xj-xj1-union-xj2 degre2 m 5 2 4)
	     "DEGRE2-X5-X2-U-X4")
	    (rename-object
	     (y1-y2-to-xj-xj1-union-xj2 degre2 m 3 1 4)
	     "DEGRE2-X3-X1-U-X4")))
	 (edge-automata
	   (list
	    (edge-automaton m 3 4 :cwd cwd)
	    (edge-automaton m 4 5 :cwd cwd)
	    (complement-automaton (edge-automaton m 3 5 :cwd cwd))))
	 (partition
	   (partition-automaton m :cwd cwd :orientation nil))
	 (tautomata ()))
    (push (make-tautomaton "Degres0" degre0-automata) tautomata)
    (push (make-tautomaton "Degres2" degre2-automata) tautomata)
    (push (make-tautomaton "Edges" edge-automata) tautomata)
    (make-graph-spec cwd 'graph-spec
		     (cwd-vbits-signature cwd m)
		     (list partition)
		     tautomata)))

(defun load-one-hole-spec (&optional (cwd 0))
  (set-current-spec (one-hole-spec cwd)))

(defun one-hole-automaton (&optional (cwd 0))
  (with-spec (one-hole-spec cwd)
    (let ((degres0 (automata (get-tautomaton "Degres0")))
	  (degres2 (automata (get-tautomaton "Degres2")))
	  (edges (automata (get-tautomaton "Edges")))
	  (partition (automaton (current-spec)))
	  A)
      (when *debug*
	(format *output-stream* "construction of ~A-ONE-HOLE~%" cwd))
      (setf a (intersection-automata-compatible
	       (cons partition (append edges degres0 degres2))))
      (setf a (vbits-projection a 6))
      (rename-object a (format nil "~A-ONE-HOLE" cwd)))))
