;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass three-ports-state (graph-state)
  ((a1 :initarg :a1 :reader a1 :type 'ports)
   (a2 :initarg :a2 :reader a2 :type 'ports)
   (a3 :initarg :a3 :reader a3 :type 'ports)))

(defmethod state-final-p ((so three-ports-state)) t)

(defgeneric make-three-ports-state (ports1 ports2 ports3)
  (:documentation "")
  (:method ((a1 ports) (a2 ports) (a3 ports))
    (assert (container-intersection-empty-p a1 a2))
    (assert (ports-subset-p a3 (ports-union a1 a2)))
    (make-instance 'three-ports-state :a1 a1 :a2 a2 :a3 a3))
  (:method ((a1 list) (a2 list) (a3 list))
    (make-three-ports-state (make-ports a1) (make-ports a2) (make-ports a3))))

(defmethod print-object ((tpo three-ports-state) stream)
  (format stream "{~A,~A,~A}"(a1 tpo) (a2 tpo) (a3 tpo)))

(defmethod graph-oplus-target ((tpo1 three-ports-state) (tpo2 three-ports-state))
  (let* ((a11 (a1 tpo1))
	 (a12 (a1 tpo2))
	 (a21 (a2 tpo1))
	 (a22 (a2 tpo2))
	 (a31 (a3 tpo1))
	 (a32 (a3 tpo2))
	 (intersection (ports-intersection a11 a12))
	 (union (ports-union a21 a22))
	 (iuu (ports-union intersection union)))
    (make-three-ports-state 
     (ports-union (ports-difference a11 iuu) (ports-difference a12 iuu))
     iuu
     (ports-union a31 a32))))

(defmethod graph-oadd-target (a b (tpo three-ports-state))
  (let* ((a1 (a1 tpo))
	 (a2 (a2 tpo))
	 (a3 (a3  tpo))
	 (union (ports-union a1 a2)))
    (cond ((or (not (ports-member a union)) (not (ports-member b union)))
	   tpo)
	  ((and (ports-member a a1) (not (ports-member b a3)))
	   (make-three-ports-state a1 a2 (ports-adjoin b a3))))))

(defmethod graph-ren-target (a b (tpo three-ports-state))
  (let* ((a1 (a1 tpo))
	 (a2 (a2 tpo))
	 (a3 (a3 tpo))
	 (u (ports-union a1 a2))
	 (m (ports-member b u))
	 (newa3 (ports-subst b a a3))
	 newa1
	 newa2)
    (cond ((and m (ports-member a a1))
	   (setf newa1 (ports-remove b (ports-remove a a1))
		 newa2 (ports-adjoin b a2)))
	  ((and m (ports-member a a2))
	   (setf newa1 (ports-remove b a1)
		 newa2 (ports-remove a (ports-adjoin b a2))))
	  ((not m)
	   (setf newa1 (ports-subst b a a1))
	   (setf newa2 (ports-subst b a a2)))
	  (t
	   (setf newa1 a1 newa2 a2)))
    (make-three-ports-state newa1 newa2 newa3)))

(defgeneric degreinmax1-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg null))
    (make-three-ports-state (list (port-of root)) '() '()))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'degreinmax1-transitions-fun)))

(defun degreinmax1-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd :orientation t)
   (lambda (root states)
     (let ((*oriented* t))
       (degreinmax1-transitions-fun root states)))
   :name (cwd-automaton-name "DEGREIN-MAX1" cwd)))

(defun subgraph-degreinmax1-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
  (rename-object
   (nothing-to-xj (degreinmax1-automaton cwd) m j)
   (format nil "~A-DEGREIN1-MAX-X~A-~A" cwd j m)))

(defun table-degreinmax1-automaton (cwd)
  (compile-automaton (degreinmax1-automaton cwd)))

(defun test-degreinmax1 ()
  (let ((a (degreinmax1-automaton)))
    (assert (recognized-p (clique-width::cwd-term-pn 4 :oriented t) a))
    (assert (recognized-p (cwd-term-circuit 4) a))
    (assert (recognized-p (cwd-term-okn 2) a))
    (assert (not (recognized-p (cwd-term-okn 3) a)))
    (assert (not (recognized-p (cwd-term-okn 4) a)))))
