;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun depth-automaton (count ffun &optional (cwd 0))
  (count-automaton
   (lambda (n) (funcall ffun n count))
   (lambda (n) (1+ n))
   #'max
   #'1+
   #'1+
   t
   cwd))

(defun depth-inf-automaton (count &optional (cwd 0))
  (rename-object
   (depth-automaton count #'<= cwd)
   (format nil "~A-DEPTH<=~A" cwd count)))

(defun depth-=-automaton (count &optional (cwd 0))
  (rename-object
   (depth-automaton count #'= cwd)
   (format nil "~A-DEPTH=~A" cwd count)))

