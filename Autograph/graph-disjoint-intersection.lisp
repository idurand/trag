;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun basic-disjoint-intersection-automaton (m &key (cwd 0) (orientation :unoriented))
  (rename-object 
   (vbits-fun-automaton
    m
    (lambda (vbits)
      (<= (count 1 vbits) 1))
    :cwd cwd :orientation orientation)
    (format
     nil
     "~A-DISJOINT-INTERSECTION-X1-~:[~;...-~]X~A"
     cwd (> m 2) m)))

(defun table-basic-disjoint-intersection-automaton (m cwd &key (orientation :unoriented))
  (compile-automaton
   (basic-disjoint-intersection-automaton m :cwd cwd :orientation orientation)))

(defun disjoint-intersection-automaton (m j1 j2 &key (cwd 0) (orientation :unoriented))
  (rename-object
   (y1-y2-to-xj1-xj2
    (basic-disjoint-intersection-automaton m :cwd cwd :orientation orientation)
    m j1 j2)
   (format nil "~A-DISJOINT-INTERSECTION-X~A-X~A-~A" cwd j1 j2 m)))

(defun table-disjoint-intersection-automaton (m j1 j2 cwd &key (orientation :unoriented))
  (compile-automaton
   (disjoint-intersection-automaton m j1 j2 :cwd cwd :orientation orientation)))
