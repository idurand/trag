;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)
(defun basic-2singletons-same-neighbours (&key (cwd 0)) ;; theta(X1,X2,X3)
  (rename-object
   (intersection-automaton-compatible-gen
     (subgraph-singleton-automaton 3 2 :cwd cwd :orientation nil)
     (subgraph-singleton-automaton 3 3 :cwd cwd :orientation nil)
     (disjoint-intersections-automaton 3 :cwd cwd :orientation nil)
     (basic-same-neighbours :cwd cwd))
   (cwd-automaton-name "X2-X3-2SINGLETONS-SAME-NEIGHBOURS-IN-X1" cwd)))

(defun basic-dominating-automaton-from-formula (&key (cwd 0))
  (rename-object
   (intersection-automaton-compatible-gen
     (x1-to-cx1 (subgraph-stable-automaton 1 1 :cwd cwd :orientation nil))
     (complement-automaton
      (vbits-projection
       (basic-2singletons-same-neighbours :cwd cwd)
      '(1))))
   (cwd-automaton-name "X1-DOMINATING" cwd)))

(defun dominating-automaton-from-formula (m j &key (cwd 0))
  (x1-to-xj (basic-dominating-automaton-from-formula :cwd cwd) m j))

(defun dominating-test-term (&key
			     (x1 '(0))
			     (x2 '(0))
			     (x3 '(0))
			     (y1 '(0))
			     (y2 '(0))
			     (y3 '(0))
			     (y4 '(0))
			     (y5 '(0))
			     (y6 '(0))
			       (y7 '(0)))
  (cwd-term-unoriented-add
   0 3
   (cwd-term-unoriented-add
    0 2
    (cwd-term-unoriented-add
     0 1
     (cwd-term-multi-oplus
      (cwd-term-vertex 0 x3) ;; x3 10
      (cwd-term-vertex 1 y3) ;; y3 9
      (cwd-term-ren
       0 4
       (cwd-term-unoriented-add
	0 3
	(cwd-term-oplus
	 (cwd-term-vertex 3 y6) ;; y6 8
	 (cwd-term-ren
	  3 4	
	  (cwd-term-unoriented-add
	   0 3
	   (cwd-term-unoriented-add
	    0 1
	    (cwd-term-multi-oplus
	     (cwd-term-vertex 0 x2) ;; x2 7
	     (cwd-term-vertex 3 y2) ;; y2 6
	     (cwd-term-ren
	      0 4
	      (cwd-term-unoriented-add
	       0 1
	       (cwd-term-unoriented-add
		0 2
		(cwd-term-unoriented-add
		 0 3
		 (cwd-term-unoriented-add
		  0 4
		  (cwd-term-multi-oplus
		   (cwd-term-vertex 0 x1) ;; x1 5
		   (cwd-term-vertex 1 y7) ;; y7 4
		   (cwd-term-vertex 2 y5) ;; y5 3
		   (cwd-term-vertex 3 y4) ;; y4 2
		   (cwd-term-vertex 4 y1) ;; y1 1
		   )))))))))))))))))) 

(defun dominating-test-term1 ()
  (dominating-test-term :x1 '(1) :x2 '(1) :x3 '(1)))

(defun dominating-test-term2 ()
  (dominating-test-term :x1 '(1) :x2 '(1) :x3 '(1) :y1 '(1)))

(defun dominating-test-term3 ()
  (dominating-test-term :x1 '(1) :x2 '(1) :y6 '(1) :y7 '(1)))

(defun dominating-test-term4 ()
  (dominating-test-term :x1 '(1 0 0) :x2 '(1 0 0)
			:x3 '(1 0 0) :y1 '(0 1 0)
			:y2 '(0 0 1) :y3 '(0 0 0)
			:y4 '(0 0 0) :y5 '(0 0 0)
			:y6 '(0 0 0) :y7 '(0 0 0)))

(defun dominating-test-term5 ()
  (dominating-test-term :x1 '(1 0 0 0) :x2 '(1 0 0 1)
			:x3 '(1 0 0 0) :y1 '(0 1 0 0)
			:y2 '(0 0 1 0) :y3 '(0 0 0 0)
			:y4 '(0 0 0 0) :y5 '(0 0 0 0)
			:y6 '(0 0 0 0) :y7 '(0 0 0 0)))

(defun dominating-test-term6 ()
  (dominating-test-term :x1 '(1 0 0 1) :x2 '(1 0 0 0)
			:x3 '(1 0 0 0) :y1 '(0 1 0 0)
			:y2 '(0 0 1 0) :y3 '(0 0 0 0)
			:y4 '(0 0 0 0) :y5 '(0 0 0 0)
			:y6 '(0 0 0 0) :y7 '(0 0 0 0)))

(defun test-dominating ()
  ;; 2012-11-20 10.913sec
  ;; 2012-12-14 in 5.385sec
  ;; 2013-01-11 5.33s
  ;; 2013-08-15 7.264sec
  ;; 2014-02-06 7.196sec
  ;; 2014-06-27 4.767sec
  (format *standard-output* "dominating")
  (let ((f (dominating-automaton-from-formula 1 1 :cwd 5)))
    (assert (recognized-p (dominating-test-term1) f))
    (assert (recognized-p (dominating-test-term2) f))
    (assert (not (recognized-p (dominating-test-term3) f))))
  (format *standard-output* " passed~%"))
