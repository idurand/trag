;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand
(in-package :autograph)

(defclass domination-state (graph-state oriented-mixin)
  ((ports1 :type ports :initarg :ports1 :reader ports1)
   (ports2 :type ports :initarg :ports2 :reader ports2)))

(defgeneric make-domination-state (ports1 ports2 oriented)
  (:method ((ports1 ports) (ports2 ports) oriented)
    (make-instance
     'domination-state
     :ports1 ports1
     :ports2 ports2
     :oriented oriented)))

(defmethod state-size ((s domination-state))
  (+ (ports-size (ports1 s))
     (ports-size (ports2 s))))

(defmethod print-object ((ppo domination-state) stream)
  (format stream "{~A}-{~A}"
	  (ports1 ppo)
	  (ports2 ppo)))

(defmethod graph-oplus-target ((po1 domination-state) (po2 domination-state))
  (make-domination-state
   (ports-union
    (ports1 po1)
    (ports1 po2))
   (ports-union
    (ports2 po1)
    (ports2 po2))
   (oriented-p po1)))

(defmethod graph-add-target (a b (ds domination-state))
  (let ((oriented (oriented-p ds)))
    (cond
      ((and (ports-member a (ports1 ds))
	    (ports-member b (ports2 ds)))
       (make-domination-state
	(ports1 ds)
	(container-remove b (ports2 ds))
	oriented))
      ((and (not (oriented-p ds))
	    (ports-member b (ports1 ds))
	    (ports-member a (ports2 ds)))
       (make-domination-state
	(ports1 ds)
	(container-remove a (ports2 ds))
	oriented))
      (t ds))))

(defmethod graph-ren-target (a b (ppo domination-state))
  (make-domination-state
   (ports-subst b a (ports1 ppo))
   (ports-subst b a (ports2 ppo))
   (oriented-p ppo)))

(defgeneric domination-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg (eql nil)))
    (let* ((port (port-of root))
	   (vbits (vbits root))
	   (vb1 (aref vbits 0))
	   (vb2 (aref vbits 1)))
      (cond
	((= 1 (* vb1 vb2)) nil)
	((= vb1 1)
	 (make-domination-state
	  (make-ports-from-port port)
	  (make-empty-ports)
	  *oriented*))
	((= vb2 1)
	 (make-domination-state
	  (make-empty-ports)
	  (make-ports-from-port port)
	  *oriented*))
	(t (make-domination-state
	    (make-empty-ports)
	    (make-empty-ports)
	    *oriented*)))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'domination-transitions-fun)))

(defmethod state-final-p ((state domination-state))
  (ports-empty-p (ports2 state)))

(defun basic-domination-automaton (&key (cwd 0) (orientation :unoriented))
  "X1 I X2 empty and every vertex of X2 is the head of an edge with tail in X1 (X1 dominates X2)"
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2 :orientation orientation)
   #'domination-transitions-fun
   :name (cwd-automaton-name "~A-DOMINATION-X1-X2" cwd)))

(defun table-basic-domination-automaton (cwd &key (orientation :unoriented)) ; domination between X1 and X2
  (compile-automaton (basic-domination-automaton :cwd cwd :orientation orientation)))

(defun domination-automaton (m j1 j2 &key (cwd 0) (orientation :unoriented))
  (rename-object
   (y1-y2-to-xj1-xj2
    (basic-domination-automaton :cwd cwd :orientation orientation) m j1 j2)
   (format nil "~A-DOMINATION-X~A-X~A-~A" cwd j1 j2 m)))

(defun table-domination-automaton (cwd m j1 j2 &key (orientation :unoriented))
  (compile-automaton (domination-automaton m j1 j2 :cwd cwd :orientation orientation)))

(defun domination-transducer (afun &key (orientation :unoriented))
  (vbits-projection
   (attribute-automaton (domination-automaton 3 1 2 :orientation orientation) afun)))
