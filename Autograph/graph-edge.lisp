;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)
(defvar *orientation* nil)
(defvar *loops* nil)

(defclass two-ports-state (graph-state)
  ((port1 :initarg :port1 :reader port1)
   (port2 :initarg :port2 :reader port2)))

(defmethod compare-object ((s1 two-ports-state) (s2 two-ports-state))
  (and
   (compare-object (port1 s1) (port1 s2))
   (compare-object (port2 s1) (port2 s2))))

(defun make-two-ports-state (port1 port2)
  (let ((pair (opair port1 port2 *orientation*)))
    (unless *loops*
      (assert (/= port1 port2)))
    (make-instance
     'two-ports-state
     :port1 (first pair)
     :port2 (second pair))))

(defmethod print-object ((tpo two-ports-state) stream)
  (format stream "~A~A"
	  (port-to-string  (port1 tpo))
	  (port-to-string (port2 tpo))))

(defclass port-num-state (graph-state)
  ((port :initarg :port :reader port)
   (num :initarg :num :reader num)))

(defun make-port-num-state (port num)
  (make-instance
   'port-num-state
   :port port
   :num num))

(defmethod compare-object ((s1 port-num-state) (s2 port-num-state))
  (and
   (= (num s1) (num s2))
   (= (port s1) (port s2))))

(defmethod print-object ((pno port-num-state) stream)
  (format stream "~A~A" (num pno) (port-to-string (port pno))))

(defmethod graph-oplus-target ((pno1 port-num-state) (pno2 port-num-state))
  (let ((port1 (port pno1))
	(port2 (port pno2))
	(num1 (num pno1))
	(num2 (num pno2)))
    (when (and (/= num1 num2) (/= port1 port2))
      (if (= num1 1)
	  (make-two-ports-state port1 port2)
	  (make-two-ports-state port2 port1)))))

(defmethod graph-add-target (a b (sc port-num-state)) sc)

(defmethod graph-add-target (a b (tpo two-ports-state))
  (let ((pair (opair a b *orientation*)))
    (if (and (= (first pair) (port1 tpo)) (= (second pair) (port2 tpo)))
	*ok-state*
	tpo)))

(defmethod graph-ren-target (a b (pno port-num-state))
  (make-port-num-state (subst b a (port pno)) (num pno)))

(defmethod graph-reh-target ((port-mapping port-mapping)
			     (port-num-state port-num-state))
  (make-port-num-state
   (apply-port-mapping port-mapping (port port-num-state))
   (num port-num-state)))

(defmethod graph-ren-target (a b (tpo two-ports-state))
  (let ((port1 (subst b a (port1 tpo)))
	(port2 (subst b a (port2 tpo))))
    (unless (= port1 port2) (make-two-ports-state port1 port2))))

(defmethod graph-reh-target ((port-mapping port-mapping) (tpo two-ports-state))
  (let ((port1 (apply-port-mapping port-mapping (port1 tpo)))
	(port2 (apply-port-mapping port-mapping (port2 tpo))))
    (unless (= port1 port2) (make-two-ports-state port1 port2))))

(defgeneric arc-or-edge-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg (eql nil)))
    (let* ((port (port-of root))
	   (vbits (vbits root))
	   (vb1 (aref vbits 0))
	   (vb2 (aref vbits 1)))
      (cond ((= 1 (* vb1 vb2)) nil)
	    ((= vb1 1) (make-port-num-state port 1))
	    ((= vb2 1) (make-port-num-state port 2))
	    (t *neutral-state*))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'arc-or-edge-transitions-fun))
  (:method ((root annotated-symbol) (arg list))
    (let ((target (arc-or-edge-transitions-fun (symbol-of root) arg)))
      (if (typep target 'two-ports-state)
	  (if (pairs-member (list (port1 target) (port2 target)) (annotation root))
	      *ok-state*
	      nil)
	  target))))

(defun name-according-orientation (orientation)
  (case orientation ((t) "ARC") ((nil) "EDGE") (:unoriented "ARC/EDGE")))

(defun basic-arc-or-edge-automaton (&key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2 :orientation orientation)
   (let ((*orientation* orientation))
     (lambda (root states)
       (let ((*neutral-state-final-p* nil)
	     (*orientation* orientation))
	 (arc-or-edge-transitions-fun root states))))
   :name (cwd-automaton-name
	  (format nil "~A(X1,X2)" (name-according-orientation orientation))
	  cwd)))

(defun arc-or-edge-automaton (m j1 j2 &key (cwd 0) (orientation :unoriented))
  (y1-y2-to-xj1-xj2
   (basic-arc-or-edge-automaton :cwd cwd :orientation orientation) m j1 j2))

(defun table-arc-or-edge-automaton (m j1 j2 cwd &key (orientation :unoriented))
  (compile-automaton
   (arc-or-edge-automaton m j1 j2 :cwd cwd :orientation orientation)))

(defun basic-edge-automaton (&key (cwd 0))
  "automaton for EDGE(X1,X2) with m = 2"
  (basic-arc-or-edge-automaton :cwd cwd :orientation nil))

(defun edge-automaton (m j1 j2 &key (cwd 0))
  "automaton for EDGE(Xj1,Xj2) with m = M)"
  (y1-y2-to-xj1-xj2 (basic-edge-automaton :cwd cwd) m j1 j2))

(defun table-basic-edge-automaton (cwd)
  "automaton for an edge between singletons X1 and X2"
  (compile-automaton (basic-edge-automaton :cwd cwd)))

(defun table-edge-automaton (m j1 j2 cwd)
  "table automaton for an edge between singletons X1 and X2"
  (compile-automaton (edge-automaton m j1 j2 :cwd cwd)))
