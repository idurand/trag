;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric empty-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg (eql nil))) nil)
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'empty-transitions-fun))
  (:method ((root (eql *empty-symbol*)) (arg (eql nil)))
    (cwd-transitions-fun root arg #'empty-transitions-fun)))

(defun empty-automaton (&key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-signature cwd :orientation orientation)
   (lambda (root states)
     (let ((*neutral-state-final-p* t))
       (empty-transitions-fun root states)))
   :name (cwd-automaton-name "EMPTY" cwd)))

(defun subgraph-empty-automaton (m j &key (cwd 0) (orientation :unoriented))
  (assert (<= 1 j m))
  (rename-object
   (vbits-fun-automaton
    m
    (lambda (vbits)
      (zerop (aref vbits (1- j))))
    :cwd cwd :orientation orientation)
   (cwd-automaton-name (format nil "EMPTY(X~A)^~A" j m) cwd)))

(defun table-subgraph-empty-automaton (m j cwd &key (orientation :unoriented))
  (compile-automaton
   (subgraph-empty-automaton m j :cwd cwd :orientation orientation)))

(defun one-color-empty-automaton (k color &key (cwd 0) (orientation :unoriented))
  (assert (<= 1 color k))
   (nothing-to-color
    (empty-automaton :cwd cwd :orientation orientation)
    k color))

(defun color-empty-automaton (k &key (cwd 0) (orientation :unoriented))
  (nothing-to-colors-constants
   (empty-automaton :cwd cwd :orientation orientation) k))

(defun table-color-empty-automaton (k cwd &key (orientation :unoriented))
  (compile-automaton
   (color-empty-automaton k :cwd cwd :orientation orientation)))

(defun table-one-color-empty-automaton (k color cwd &key (orientation :unoriented))
  (compile-automaton
   (one-color-empty-automaton k color :cwd cwd :orientation orientation)))
