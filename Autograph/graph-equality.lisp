;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun basic-equal-automaton (&key (cwd 0) (orientation :unoriented))
  (rename-object
   (vbits-fun-automaton
    2
    (lambda (vbits)
      (=  (aref vbits 0) (aref vbits 1)))
    :cwd cwd :orientation orientation)
   (format nil "~A-EQUALITY-X1-X2" cwd)))

(defun equal-automaton (m j1 j2 &key (cwd 0) (orientation :unoriented))
  (rename-object
   (y1-y2-to-xj1-xj2
    (basic-equal-automaton :cwd cwd :orientation orientation) m j1 j2)
   (format nil "~A-EQUALITY-X~A-X~A-~A" cwd j1 j2 m)))

(defun table-basic-equal-automaton (cwd &key (orientation :unoriented))
  (compile-automaton
   (basic-equal-automaton :cwd cwd :orientation orientation)))

(defun table-equal-automaton (m j1 j2 cwd  &key (orientation :unoriented))
  (compile-automaton
   (equal-automaton m j1 j2 :cwd cwd :orientation orientation)))

(defun test-graph-equality ()
  (let ((term (input-cwd-term "oplus(a^100, oplus(a^010, a^000))"))
	(a1 (equal-automaton 3 1 2))
	(a2 (equal-automaton 3 1 3)))
    (assert 
     (recognized-p term
		   (intersection-automaton
		    (complement-automaton a1)
		    (complement-automaton a2))))))
