;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric has-circuit-ok-fun-add (a b uncasted-state)
  (:method (a b (s uncasted-state))  nil)
  (:method (a b (s rel-state))
    (and (rel-state-member (list b a) s) *ok-ok-state*)))

(defun boucle (a) (list a a))

(defgeneric has-circuit-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (make-rel-state (make-ports-relation (list (boucle (port-of root))) t)))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'has-circuit-transitions-fun)))

(defun has-circuit-automaton (&key (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd :orientation t)
   (lambda (root states)
       (let ((*ok-fun-add* #'has-circuit-ok-fun-add))
	 (has-circuit-transitions-fun root states)))
   :name (cwd-automaton-name "HAS-CIRCUIT" cwd)))

(defun table-has-circuit-automaton (cwd)
  (compile-automaton (has-circuit-automaton :cwd cwd)))

(defun subgraph-has-circuit-automaton (m j &key (cwd 0))
   (nothing-to-xj
    (has-circuit-automaton :cwd cwd) m j))

(defun table-subgraph-has-circuit-automaton (m j cwd)
  (compile-automaton (subgraph-has-circuit-automaton m j :cwd cwd)))

(defun test-has-circuits ()
  (let ((term1 (input-cwd-term  "oplus(add_a->b(add_b->a(oplus(b,a))),add_a->b(add_b->a(oplus(b,a))))"))
	(term2 (input-cwd-term "add_a->c(add_c->b(oplus(c[2],ren_c_b(add_b->c(oplus(c[3],add_b->a(oplus(b[4],a[1]))))))))"))
	(term3 (input-cwd-term "add_a->c(oplus(c[2],ren_c_b(add_b->c(oplus(c[3],add_b->a(oplus(b[4],a[1])))))))"))
	(f (has-circuit-automaton)))
    (assert (recognized-p term1 f))
    (assert (recognized-p term2 f))
    (assert (not (recognized-p term3 f)))))
