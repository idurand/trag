;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;;; first part common to nr-has-cycle and nr-ne-deg-supeg2
(defclass nr-cycle-state (graph-state)
  ((ports1 :initarg :ports1 :reader ports1 :type ports
	   :documentation "ports appearing once")
   (ports2 :initarg :ports2 :reader ports2 :type ports
	   :documentation "ports appearing at least twice")
   (degre0 :initarg :degre0 :reader degre0 :type ports
	   :documentation "ports of degree 0")
   (degre1 :initarg :degre1 :reader degre1 :type ports
	   :documentation "ports of degree 1")))

(defmethod print-object ((s nr-cycle-state) stream)
  (format stream "[~A,~A,~A,~A]"
	  (ports1 s) (ports2 s) (degre0 s) (degre1 s)))

(defmethod ports-of ((s nr-cycle-state))
  (ports-union (ports1 s) (ports2 s)))

(defmethod compare-object ((s1 nr-cycle-state) (s2 nr-cycle-state))
  (and
   (compare-object (ports1 s1) (ports1 s2))
   (compare-object (ports2 s1) (ports2 s2))
   (compare-object (degre0 s1) (degre0 s2))
   (compare-object (degre1 s1) (degre1 s2))))

(defgeneric make-nr-cycle-state (ports1 ports2 degre0 degre1 state-type)
  (:method ((ports1 ports) (ports2 ports) (degre0 ports)
	    (degre1 ports) state-type)
    (assert (container-intersection-empty-p ports1 ports2))
    (assert (ports-subset-p (ports-union degre0 degre1) (ports-union ports1 ports2)))
    (make-instance state-type
		   :ports1 ports1
		   :ports2 ports2
		   :degre0 degre0
		   :degre1 degre1)))

(defmethod graph-ren-target (a b (s nr-cycle-state))
  (let* ((ports1 (ports1 s))
	 (ports2 (ports2 s))
	 (ports (ports-union ports1 ports2)))
    (if (ports-member a ports)
	(let ((ainp1 (ports-member a ports1))
	      (binp1 (ports-member b ports1)))
	  (cond
	    ((and ainp1 binp1)
	     (setf ports1 (container-remove a (container-remove b ports1)))
	     (setf ports2 (container-adjoin b ports2)))
	    ((and ainp1 (ports-member b ports2))
	     (setf ports1 (container-remove a ports1)))
	    ((and binp1 (ports-member a ports2))
	     (setf ports1 (container-remove b ports1))))
	  (make-nr-cycle-state
	   (ports-subst b a ports1)
	   (ports-subst b a ports2)
	   (ports-subst b a (degre0 s))
	   (ports-subst b a (degre1 s))
	   (class-of s)))
	s)))

(defmethod graph-oplus-target ((s nr-cycle-state) (sp nr-cycle-state))
  (let* ((ports1 (ports1 s))
	 (ports1p (ports1 sp))
	 (ports2 (ports2 s))
	 (ports2p (ports2 sp))
	 (degre0 (degre0 s))
	 (degre0p (degre0 sp))
	 (degre1 (degre1 s))
	 (degre1p (degre1 sp))
	 (u1 (ports-union ports1 ports1p))
	 (i1 (ports-intersection ports1 ports1p))
	 (u2 (ports-union ports2 ports2p)))
    (make-nr-cycle-state
     (container-difference (container-difference u1 i1) u2)
     (ports-union u2 i1)
     (ports-union degre0 degre0p)
     (ports-union degre1 degre1p)
     (class-of s))))

(defgeneric next-degre-greater-than-one (a b nr-cycle-state)
  (:method (a b (s nr-cycle-state))
    ;; ab included in ports-of s
    (let* ((ports2 (ports2 s))
	   (ports (ports-of s))
	   (degre0 (degre0 s))
	   (degre1 (degre1 s)))
      (container-difference
       degre1
       (ports-union
	(container-remove-if
	 (lambda (c)
	   (or
	    (and (= c a) (ports-member b ports))
	    (and (= c b) (ports-member a ports))))
	 degre1)
	(container-remove-if
	 (lambda (c)
	   (or
	    (and (= c a) (ports-member b ports2))
	    (and (= c b) (ports-member a ports2))))
	 degre0))))))

(defgeneric next-degre-one (a b nr-cycle-state)
  (:method (a b (s nr-cycle-state))
    (let ((ports1 (ports1 s)))
      (container-remove-if-not
       (lambda (c)
	 (or
	  (and (= c a) (ports-member b ports1))
	  (and (= c b) (ports-member a ports1))))
       (degre0 s)))))

;;; specific to nr-has-cycle
(defclass nr-has-cycle-state (nr-cycle-state) ())

(defun make-nr-has-cycle-state (ports1 ports2 degre0 degre1)
  (make-nr-cycle-state ports1 ports2 degre0 degre1 'nr-has-cycle-state))

(defgeneric before-cycling-p (a b nr-has-cycle-state)
  (:method (a b (s nr-has-cycle-state))
    (let ((ports2 (ports2 s))
	  (degre1 (degre1 s))
	  (difference (container-difference (ports-of s) (make-ports (list a b)))))
      (and (ports-empty-p difference)
	   (or
	    (and (ports-member a ports2) (ports-member a degre1))
	    (and (ports-member b ports2) (ports-member b degre1)))))))

(defgeneric after-cycling-p (nr-has-cycle-state)
  (:method ((s nr-has-cycle-state))
    (and (not (ports-empty-p (ports-of s)))
	 (ports-empty-p (ports-union (degre0 s) (degre1 s))))))

(defmethod graph-add-target (a b (s nr-has-cycle-state))
  (let ((ab (make-ports (list a b)))
	(ports (ports-of s)))
    (if (ports-subset-p ab ports)
	(if (before-cycling-p a b s)
	    *ok-ok-state*
	    (let ((new-s
		    (make-nr-has-cycle-state
		     (ports1 s)
		     (ports2 s)
		     (container-difference (degre0 s) ports)
		     (ports-union
		      (container-difference
		       (degre1 s)
		       (next-degre-greater-than-one a b s))
		      (next-degre-one a b s)))))
	      (if (after-cycling-p new-s)
		  *ok-ok-state*
		  new-s)))
	s)))

(defgeneric nr-has-cycle-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (let ((port (port-of root)))
      (make-nr-has-cycle-state
       (make-ports-from-port port)
       (make-empty-ports)
       (make-ports-from-port port)
       (make-empty-ports))))
  (:method ((root (eql *empty-symbol*)) (arg null))
    (cwd-transitions-fun root arg #'nr-has-cycle-transitions-fun))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'nr-has-cycle-transitions-fun)))

(defun nr-has-cycle-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd)
   (lambda (root states)
     (let ((*oriented* nil)
	   (*neutral-state-final-p* nil))
       (nr-has-cycle-transitions-fun root states)))
   :name (cwd-automaton-name "NR-HAS-CYCLE" cwd)))

(defun table-nr-has-cycle-automaton (cwd)
  (compile-automaton (nr-has-cycle-automaton cwd)))

(defun subgraph-nr-has-cycle-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
  (rename-object
   (nothing-to-xj (nr-has-cycle-automaton cwd) m j)
   (format nil "~A-NR-HAS-CYCLE-X~A-~A" cwd j m)))

(defun table-subgraph-nr-has-cycle-automaton (m j cwd)
  (compile-automaton (subgraph-nr-has-cycle-automaton m j cwd)))

(defun test-has-cycle ()
  (let ((a (nr-has-cycle-automaton)))
    (assert (recognized-p (cwd-term-kn 4) a))
    (assert (recognized-p (cwd-term-oplus (cwd-term-cycle 4) (cwd-term-stable 4)) a))
    (assert (not (recognized-p (cwd-term-pn 4) a)))
    (assert (not (recognized-p (cwd-term-stable 4) a)))))
