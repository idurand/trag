;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;; since 2016-05-27 ac-oplus by default everywhere

(defun test-graph-automata1 ()
  ;; 0.02s 2011-05-19
  ;; 0.015 2013-08-05
  ;; 2014-06-27 0.02sec
  ;; 2018-03-24 0.013sec (mac)
  (format *standard-output* "test-graph-automata1~%")
  (let* ((a (table-stable-automaton 2))
	 (i (intersection-automaton-compatible a (duplicate-automaton a)))
	 (u (union-automaton a (duplicate-automaton a)))
	 (ud (disjoint-union-automaton a (duplicate-automaton a)))
	 (udg (disjoint-union-automaton-gen a (duplicate-automaton a) (duplicate-automaton a)))
	 (c (complement-automaton ud))
	 (term (cwd-term-stable 3)))
    (assert (equality-automaton i a))
    (assert (equality-automaton u a))
    (assert (equality-automaton ud a))
    (assert (equality-automaton udg a))
    (assert (recognized-p term a))
    (assert (recognized-p term i))
    (assert (recognized-p term u))
    (assert (recognized-p term ud))
    (assert (not (recognized-p term c)))))

(defun test-graph-automata2 ()
  ;; 2012-11-21 in 0.012 sec
  ;; 2013-08-05 in 0.002 sec
  ;; 2014-06-27 0.001sec
  ;; 2018-03-23 0.001sec
  (format *standard-output* "test-graph-automata2~%")
  (let ((f1 (cardinality-automaton 2 :cwd 2))
	(f2 (complement-automaton (stable-automaton :cwd 2)))
	(term (input-cwd-term "add_a_b(add_a_b(oplus(a,b)))")))
    (assert (recognized-p term f1))
    (assert (recognized-p term f2))
    (assert (recognized-p term (intersection-automaton-compatible f1 f2)))
    (assert (recognized-p term (union-automaton f1 f2)))))

(defun test-graph-degres ()
  ;; 2011-05-19 1mn 17s 
  ;; 2011-10-08 1mn 48s 
  ;; 2012-09-01 54s 
  ;; 2012-11-21 52s 
  ;; 2013-08-15 47.579sec
  ;; 2014-02-06 7.23sec
  ;; 2014-06-24 5.457sec
  ;; 2014-06-27 6.803src
  ;; 2015-10-16 5.768sec (mac 8GB)
  ;; 2016-05-27 4.0sec (shenzhou ac-oplus always)
  ;; 2018-01-11 1.724sec soyouz SBCL 1.3.13 debug ???
  (format *standard-output* "test-graph-degres~%")
  (let ((a (table-degre-max-automaton 1 2)))
    (verify-automaton (minimize-automaton a) 7 40)
    (setf a (table-degre-max-automaton 2 2))
    (verify-automaton (minimize-automaton a) 15 148)))

(defun test-paths ()
  ;; 7.86s 2011-05-19
  ;; in 6.4s 2012-01-11
  ;; 2014-02-06 3.857sec
  ;; 2014-06-24 3.512sec
  ;; 2014-06-27 3.791sec
  ;; 2018-01-11 1.032sec soyouz SBCL 1.3.13 debug
  (format *standard-output* "test-paths~%")
  (let ((a (table-basic-path-automaton 2)))
    (verify-automaton a 25 216)
    (verify-automaton (minimize-automaton a) 12 83)
    (setq a (table-basic-path-automaton 3))
    (verify-automaton a 214 7147)
    (verify-automaton (minimize-automaton a) 124 4402)
    (verify-automaton (table-basic-path-automaton-from-formula 2 :orientation nil) 12 83)))

;; (defun test-paths2 () 
;;   ;; a-t-on déjà réussi ce test?
;;   (format *standard-output* "test-paths2~%")
;;   (verify-automaton (table-basic-path-automaton-from-formula 3) 124 4402))

(defun test-colorability ()
  ;; 1mn 36s 2011-06-16
  ;; 40.412s 2012-11-21
  ;; 2013-05-14 29.193sec
  ;; 2013-08-15 26.629sec
  ;; 2014-06-24 24.486sec
  ;; 2014-06-27 28.1sec
  (format *standard-output* "test-colorability~%")
  (verify-automaton
   (minimize-automaton (table-kcolorability-automaton 2 2 :orientation nil)) 8 50)
  (verify-automaton
   (minimize-automaton (table-kcolorability-automaton 3 2 :orientation nil)) 15 147)
  (verify-automaton
   (minimize-automaton (table-kcolorability-automaton 2 3 :orientation nil)) 56 1972)
  (format *standard-output* "passed~%"))

(defun test-petersen (&key (annotation nil))
  ;; without annotation 
  ;; 2013-10-10 48.215sec
  ;; 2014-02-06 42.626sec
  ;; 2014-06-24 39.618sec
  ;; 2014-06-27 45.158sec
  ;; 2015-12-01 26.932sec SBCL 1.3 debug

  ;; with annotation 
  ;; 2013-10-11 28.087sec
  ;; 2014-02-06 23.405sec
  ;; 2014-06-24 23.097sec
  ;; 2014-06-27 25.32sec
  ;; 2015-12-01 15.414sec SBCL 1.2.14 debug (shenzhou??)
  ;; 2015-06-03 15.249sec SBCL 1.2.14 debug shenzhou
  ;; 2015-06-03 12.78sec SBCL 1.2.14 debug shenzhou
  (let ((petersen (if annotation (apetersen) (petersen))))
    (format *standard-output* "test-petersen ")
    (assert (recognized-p petersen (kcolorability-automaton 3))) ;; 1.46s
    (format *standard-output* "colorability passed~%")
    (assert
     (not (recognized-p petersen (k-acyclic-colorability-automaton 3))))
    (format *standard-output* "acyclic-colorability 3 passed~%")
    (assert
     (recognized-p petersen (k-acyclic-colorability-automaton 4)))
    (format *standard-output* "acyclic-colorability 4 passed~%")))

;; heap exhausted si on ne met pas *cast* a vrai
;; 2016-08-07 32min 40.676sec *ac-symbol* nil *cast-inside* T *cast* NIL (debug 3)
(defun test-3-acyclic-colorability (cwd-term)
  (assert
   (recognized-p cwd-term (k-acyclic-colorability-automaton 3)))
  (format *standard-output* "3-acyclic-colorability 3 passed~%"))

(defun test-mcgee-aux (amcgee)
  ;; 2013-08-12 50mn 55sec
  ;; 2014-02-04 40min 59.162sec before branch object
  (format *standard-output* "test-mcgee ")
  (assert (not
	   (recognized-p
	    amcgee
	    (kcolorability-automaton 2))))
  (format *standard-output* "2-colorability passed~%")
  (assert (enum-recognized-p amcgee (kcolorability-automaton 3)))
  (format *standard-output* "3-colorability passed~%")
  (assert
   (enum-recognized-p amcgee (k-acyclic-colorability-automaton 3)))
  (test-3-acyclic-colorability amcgee))

(defun test-mcgee ()
  ;; 2013-08-12 50mn 55sec
  ;; 2014--6-27 29mn 20sec
  ;; 2015-12-01 shenzhou 18min 42.734sec ac nil
  ;; 2017-04-11 shenzhou 16min 3.336sec
  ;; 2017-05-11 15min 43.092sec
  (let ((amcgee (amcgee)))
    (test-mcgee-aux amcgee)))

(defun test-circuits () 
  ;; 1.28sec 2011-06-16
  ;; 0.4s 2013-01-11
  ;; 2014-02-06 0.335sec
  (format *standard-output* "test-circuits")
  (let ((circuit2 (table-has-circuit-automaton 2))
	(circuit3 (table-has-circuit-automaton 3)))
    (verify-automaton circuit2 7 43)
    (assert (minimal-p circuit2))
    (verify-automaton circuit3 80 3907)
    (assert (minimal-p circuit3))
    (format *standard-output* " passed~%")))

(defun test-cycles () 
  ;; 0.29s 2011-06-16
  ;; 0.26s 2013-01-11
  ;; 2014-02-06 0.227sec
  ;; 2014-06-27 0.208sec
  (format *standard-output* "test-circuits")
  (let ((a (table-has-kcycle-automaton 3 3)))
    (verify-automaton a 46 474)
    (format *standard-output* " passed~%")))

(defun test-acyclic-colorability (&key (annotation nil))
  ;; without annotation
  ;; 2011-06-20 1.851sec
  ;; 2013-01-11 0.85s
  ;; 2014-06-27 1.1sec

  ;; with annotation
  ;; 2011-06-29 1.518s 
  ;; 2011-09-22 in 4.25sec pourquoi fait-on pire?
  ;; 2011-09-22 in 4.25sec
  ;; 2011-11-18 in 2.558src annotation in 1.427sec
  ;; 2013-01-11 0.51s 
  ;; 2014-06-27 0.634sec
  (let ((grunbaum (if annotation (agrunbaum) (grunbaum)))
	(f (k-acyclic-colorability-automaton 4)))
    (assert (not (recognized-p grunbaum f)))
    (assert (recognized-p grunbaum (k-acyclic-colorability-automaton 5)))))

(defun test-l1-colorability () 
  ;; needs that 2ports work well
  ;; 2011-10-07 0.411sec
  ;; 2013-01-11 0.3s
  ;; 2014-02-06 0.19sec
  ;; 2014-06-27 0.12sec
  (format *standard-output* "test-l1-colorability")
  (assert (not (recognized-p (cwd-term-knm 2 2)
			     (l1-colorability-automaton 4 2))))
  (assert (recognized-p (cwd-term-knm 2 2)
			(l1-colorability-automaton 5 2)))
  (assert (recognized-p (cwd-term-pn 4) (l1-colorability-automaton 4 3)))
  (format *standard-output* " passed~%"))

(defun test-graphs1 ()
  ;; 2012-12-14 1min 0.44sec
  ;; 2013-01-11 51.3s
  ;; 2014-02-06 10.924sec
  ;; 2014-06-27 10.8sec
  ;; 2017-05-11 5.132sec
  (test-graph-automata1)
  (test-graph-degres)
  (test-paths)
  (test-cycles)
  (test-circuits))

(defun test-graphs2 (&key (annotation nil))
  ;; annotation t
  ;; 2012-12-14 9min 25.313sec 
  ;; 2014-02-06 1min 35.115sec
  ;; 2014-06-27 1min 59.7sec
  ;; annotation nil
  ;; 2014-02-06 1min 53.553sec
  ;; 2014-06-27 1min 40sec
  ;; 2017-04-11 47.18sec
  ;; 2017-05-11 36.704sec
  (test-colorability)
  (test-petersen :annotation annotation)
  (test-acyclic-colorability :annotation annotation))

(defun test-graphs (&key (annotation nil))
  ;; 2011-10-08 23min 
  ;; 2011-08-16 19min with annotation 
  ;; 2012-08-25 in 12min 31.141sec
  ;; 2012-09-01 in 10mn 6.043sec
  ;; 2012-09-20 in 10min 42.1sec
  ;; 2012-09-20 in 8min 31.525sec (annotation)
  ;; 2012-09-27 in 10min 50.341sec
  ;; 2012-09-27 in 8min 11.639sec (annotation)
  ;; 2012-12-14 in 8min 13.937sec (annotation)
  ;; 2013-01-15 in 9mn 37s (without annotation)
  ;; 2013-01-15 in 7mn 17s(annotation)
  ;; 2013-08-12 in 5min 32sec
  ;; 2014-02-06 annotation nil 2min 5.909sec
  ;; 2014-02-06 annotation t 1min 46.203sec 
  ;; 2014-06-27 annotation nil 2min 8.928sec
  ;; 2014-06-27 annotation t 1min 50.848sec
  ;; 2016-05-27 59.098sec annotation nil shenzhou ac-plus (always)
  ;; 2017-05-11 38.672sec annotation, ac-oplus nil
  (test-graphs1)
  (test-graphs2 :annotation annotation))

(defun test-graphs3 ()
  ;; 2012-09-04 in 7min 1.29sec
  ;; 2012-12-08 in 3min 36.47sec
  ;; 2012-12-14 in 3min 16.823sec
  ;; 2013-01-11 3min 9.9s
  ;; 2013-09-29 1min 27.998sec
  ;; 2014-02-06 23.925sec
  ;; 2014-06-27 25.023sec
  ;; 2015-12-03 15.948sec
  ;; 2017-05-11 12.628sec
  (let* ((f (k-acyclic-colorability-automaton 4))
	 (target (compute-target-counting-runs (apetersen) f)))
    (assert (= (target-attribute target) 10800))))

(defun test-graphs3bis ()
  ;; 2017-05-11 in 13.272sec
  (let* ((f (constants-color-projection
	     (attribute-automaton
	      (acyclic-coloring-automaton 4) *count-afun*)))
	 (target (compute-target (apetersen) f)))
    (assert (= (target-attribute target) 10800))))

(defun test-graphs4 ()
  ;; 2011-11-16 in 9.2 sec
  ;; 2012-08-25 in 8.266sec
  ;; 2012-12-14 in 6.416sec
  ;; 2013-01-15 6.1s
  ;; 2014-02-06 7.177sec
  ;; 2017-05-11 1.944sec
  (test-graph-automata2)
  (test-l1-colorability)
  (test-dominating))

(defun test-graphs5 ()
  ;; 2015-12-02 shenzhou 2.141sec
  ;; 2016-05-24 shenzhou 1.359sec
  ;; 2017-05-11 2.084sec
  (let ((f (regular-automaton)))
    (assert (recognized-p (cwd-term-pn 2) f))
    (assert (not (recognized-p (cwd-term-pn 20) f)))
    (assert (recognized-p (cwd-term-cycle 20) f))
    (assert (recognized-p (cwd-term-square-grid 2) f))
    (assert (not (recognized-p (cwd-term-square-grid 20) f)))
    (assert (recognized-p (cwd-term-kn 20) f))
    (assert (recognized-p (cwd-term-stable 20) f))))

(defun test-short ()
  ;; 2013-08-15 in 8min 41.814sec
  ;; 2014-01-06 2min 37.171sec
  ;; 2014-06-24 2min 23.273sec
  ;; 2014-06-27 2min 39.043sec
  ;; 2015-04-02 1min 48.49sec (shenzhou ac-oplus t)
  ;; 2015-04-02 1min 38.757sec  (shenzhou ac-oplus nil)
  ;; 2015-11-13 1min 37.613sec (shenzhou ac-oplus t)
  ;; 2015-11-13 1min 34.996sec (shenzhou ac-oplus nil)
  ;; 2015-12-01 1min 48.884sec shenzhou ac-oplus t / nil 1min 46.747sec
  ;; 2016-05-24 1min 22.398sec shenzhou ac-plus
  ;; 2016-05-27 1min 18.466sec shenzhou ac-plus always
  ;; 2016-08-14 1min 8.671sec shenzhou ac-oplus nil
  ;; 2016-08-14 1min 8.671sec shenzhou ac-oplus nil
  ;; 2016-08-14 1min 11.476sec shenzhou ac-oplus nil
  ;; 2017-05-11 52.792sec shenzhou ac-oplus nil
  ;; 2018-03-29 29.492sec soyouz ac-oplus nil
  (test-graphs)
  (test-graphs4)
  (test-graphs5))

(defun test-all1 ()
  ;; 2013-08-13 in 60min 53.596sec
  ;; 2013-11-23 in 51m 12.23sec
  ;; 2014-02-06 31mn
  ;; 2014-02-16 29min 5.021sec
  ;; 2015-10-12 shenzhou 19min.59s cast=T SBCL 1.2.14 --dynamic-space-size 4096 --control-stack-size 32768
  ;; 2015-11-13 shenzhou 19min 44.797sec cast=nil
  ;; 2015-11-13 shenzhou  cast=t
  ;; 2015-12-01 shenzhou 19min 51.075sec SBCL 1.2.14 debug
  ;; 2017-05-22 shenzhou 17min 8.708sec SBCL 1.3.13 debug
  (test-graphs1)
  (test-graphs2)
  (test-graphs4)
  (test-graphs5)
  (test-forest)
  (test-connectedness1)
  (test-connectedness2)
  (test-graph-clique)
  (test-graph-redundant)
  (test-mcgee))

(defun test-graph-automata3 ()
  ;; 2018-02-07 soyouz 0.056sec  SBCL 1.3.13 debug
  (format *standard-output* "test-graph-automata3~%")
  (test-graph-equality)
  (test-forest)
  (test-graph-path)
  (test-graph-clique)
  (test-graph-equality)
  (test-card-cc)
  (test-dag)
  (test-stable)
  (test-graph-union-cycle)
  (test-has-cycle))

(defun test-all ()
  (test-graphs2 :annotation nil)
  (test-graphs2 :annotation t)
  (test-graph-automata1)
  (test-graph-automata2)
  (test-graph-automata3)
  (test-degreinmax1)
  (test-count-house-subgraphs)
  (test-afun))
 
(defun test-enum ()
  (let* ((f (stable-automaton :cwd 2))
	 (ud (disjoint-union-automaton f (duplicate-automaton f)))
	 (t1 (cwd-term-stable 2 '(0 1)))
	 (t2 (cwd-term-unoriented-add 0 1 t1)))
    (assert (recognized-p t1 f))
    (assert (recognized-p t1 f :enum t))
    (assert (recognized-p t1 ud))
    (assert (recognized-p t1 ud :enum t))
    (assert (not (recognized-p t1 (complement-automaton f) :enum nil)))
    (assert (not (recognized-p t1 (complement-automaton f) :enum t)))
    (assert (not (recognized-p t2 f)))
    (assert (recognized-p t2 (complement-automaton f)))))
    
 (defun test-emptiness ()
  (let ((f (stable-automaton :cwd 2))
	(f2 (kcolorability-automaton 1)))
    (assert (not (automaton-emptiness f)))
    (assert (not (automaton-emptiness (complement-automaton f))))
    (assert (not (cwd-automaton-emptiness f2 2)))
    (assert (not (cwd-automaton-emptiness (complement-automaton f2) 2)))))
    
(defun test-autograph ()
;;  "1min 50.667sec" 2020-12-28
  (test-singleton)
  (test-member)
  (test-graph-hamiltonian)
  (test-enum)
  (test-emptiness)
  (test-short)
  (test-link)
  (test-all)
  (test-graph-automata-declarative)
  (test-mso2-hamiltonian-decl))
