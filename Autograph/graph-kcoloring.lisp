;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun kcolorability-automaton (k &key (cwd 0) (orientation :unoriented))
  (rename-object
   (constants-color-projection (coloring-automaton k :cwd cwd :orientation orientation))
   (cwd-automaton-name
    (format nil "~Acolorability" k) cwd)))

(defun table-kcolorability-automaton (k cwd &key (orientation :unoriented))
  (compile-automaton (kcolorability-automaton k :cwd cwd :orientation orientation)))

(defun progressive-table-kcolorability-automaton (k cwd &optional (minimize nil))
  (let ((a (table-coloring-automaton k cwd)))
    (when (<= cwd 2)
      ;;      (format *output-stream* " minimizing~%")
      (setf a (minimize-automaton a)))
    (rename-and-save
     (format nil "~A-~A-coloring-min" cwd k)
     a)
    ;;    (format *output-stream* " Projection~%")
    (setf a (constants-color-projection a))
    (rename-and-save
     (format nil "~A-~A-colorability" cwd k)
     a)
    (when minimize 
      ;;      (format *output-stream* " Determinization~%")
      (with-time
	  (setf a (determinize-automaton a)))
      ;;      (format *output-stream* "~%")
      (rename-and-save
       (format nil "~A-~A-colorability-det" cwd k)
       a)
      ;;      (format *output-stream* " minimizing~%")
      (with-time
	  (setf a (minimize-automaton a)))
      ;;      (format *output-stream* "~%")
      (rename-and-save
       (format nil "~A-~A-colorability-min" cwd k)
       a))
    a))
