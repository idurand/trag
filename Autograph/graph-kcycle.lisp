;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;; automaton for testing the existence of a cycle of length k
;; without chords
;; in an unoriented graph
;; works on redundant or non redundant term

(defun basic-has-kcycle-automaton (len &optional (cwd 0))
  (let ((automata '()))
    (mapc
     (lambda (pair)
       (let
	   ((j1 (first pair))
	    (j2 (second pair)))
	 (if (or (and (= 1 j1) (= len j2)) (= 1 (- j2 j1)))
	     (push (edge-automaton len j1 j2 :cwd cwd) automata) ;; edge of the cycle
	     (progn
	       (push
		(complement-automaton (edge-automaton len j1 j2 :cwd cwd)) automata) ;; no edges inside the cycle
	       (push
		(complement-automaton 
		 (equal-automaton len j1 j2 :cwd cwd)) automata)))))
     (distinct-pairs (set-iota len)))
    (intersection-automata-compatible automata)))

(defun has-kcycle-automaton (len &optional (cwd 0))
  (rename-object
   (vbits-projection
    (basic-has-kcycle-automaton len cwd))
   (format nil "~A-HAS-~ACYCLE" cwd len)))
      
(defclass has-kcycle-graph-spec (graph-spec)
  ((len :type integer :initarg :len :accessor len)))

(defun make-has-kcycle-graph-spec
    (len cwd &key (signature nil) (automata nil) (tautomata nil))
  (make-graph-spec cwd 'has-kcycle-graph-spec
		   signature automata tautomata
		   :len len))

(defun has-kcycle-spec (len cwd)
  (assert (>= len 2))
  (let* ((edge-automata nil)
	 (nedge-automata nil)
	 (nequality-automata nil)
	 (tautomata ()))
    (mapc
     (lambda (pair)
       (let
	   ((j1 (first pair))
	    (j2 (second pair)))
	 (if
	  (or (and (= 1 j1) (= len j2))
	      (= 1 (- j2 j1)))
	  (push (table-edge-automaton len j1 j2 cwd) edge-automata)
	  (progn
	    (push
	     (complement-automaton (table-edge-automaton len j1 j2 cwd)) nedge-automata)
	    (push
	     (complement-automaton
	      (table-equal-automaton len j1 j2 cwd)) nequality-automata)
	    ))))
     (distinct-pairs (set-iota len)))
    (push (make-tautomaton "Edges" edge-automata) tautomata)
    (push (make-tautomaton "Nedges" nedge-automata) tautomata)
    (push (make-tautomaton "Nequalities" nequality-automata) tautomata)
    (make-has-kcycle-graph-spec
     len
     cwd
     :signature (cwd-vbits-signature cwd len)
     :automata edge-automata
     :tautomata tautomata)))

(defun load-has-kcycle-spec (len cwd)
  (set-current-spec (has-kcycle-spec len cwd)))

(defun test-has-kcycle-step1 (len cwd)
  (load-has-kcycle-spec len cwd)
  (when *debug*
    (format *output-stream* "construction of ~A-has-kcycle-~A~%" cwd len))
  (let ((edges (automata (get-tautomaton "Edges")))
	(nedges (automata (get-tautomaton "Nedges")))
	(nequalities (automata (get-tautomaton "Nequalities")))
	a)
    (with-time-to-output-stream
	(setf a
	      (op-automata-list
	       (append edges nedges nequalities)
	       :op 'intersection-automaton-compatible
	       :cop 'minimize-automaton
	       )))
    (rename-and-save (format nil "min-~A-has-kcycle-x-~A" cwd len) a)))

(defun table-basic-has-kcycle-automaton (len cwd)
  (test-has-kcycle-step1 len cwd))

(defun test-has-kcycle-step2 (len cwd a)
  (setf a (vbits-projection a))
  (rename-and-save (format nil "~A-has-kcycle-~A" cwd len) a))

(defun test-has-kcycle-step3 (len cwd a)
  (format *output-stream* "determinizing ")
  (with-time (setf a (determinize-automaton a)))
  (rename-and-save (format nil "~A-has-kcycle-~A-det" cwd len) a)
  (format *output-stream* "minimizing ")
  (with-time (setf a (minimize-automaton a)))
  (rename-and-save (format nil "~A-has-kcycle-~A-min" cwd len) a))

(defun table-has-kcycle-automaton (len cwd &optional (determinize nil))
  (assert (> len 2))
  (let ((a (test-has-kcycle-step1 len cwd)))
    (setf a (test-has-kcycle-step2 len cwd a))
    (when determinize 
      (setf a (test-has-kcycle-step3 len cwd a)))
    a))

(defun test-kcycle ()
  (let ((tok (cwd-decomposition (graph-cycle 4)))
	(tnok (cwd-decomposition
	       (graph-from-paths '((1 2 3 4 1) (2 4))
				 :oriented nil)))
	(a3 (table-has-kcycle-automaton 4 3)))
    (assert (recognized-p tok (has-kcycle-automaton 4)))
    (assert (recognized-p tok a3))
    (assert (not (recognized-p tnok (has-kcycle-automaton 4))))
    (assert (not (recognized-p tnok a3)))))
