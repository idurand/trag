;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun stable-pairs (m)
  (mapcar #'list (set-iota m) (iota (1- m) 2)))

(defun nop3-pairs (m)
  (remove-if (lambda (pair)
	       (let ((diff (- (second pair) (first pair))))
		 (< diff 2)))
	     (cartesian-product
	      (list (iota (- m 2) 1) (iota (- m 1) 2)))))

(defun l1-colorability-spec (m cwd)
  (let* ((stable (stable-automaton :cwd cwd :orientation nil))
	 (nop3 (nop3-automaton cwd))
	 (stable-automata
	   (mapcar
	    (lambda (pair)
	      (rename-object
	       (nothing-to-xj1-union-xj2
		stable m (first pair)
		(second pair))
	       (format nil
		       "~A-STABLE-X~A-U-X~A-~A"
		       cwd
		       (first pair) (second pair) m)))
	    (stable-pairs m)))
	 (nop3-automata
	   (mapcar
	    (lambda (pair)
	      (rename-object
	       (nothing-to-xj1-union-xj2
		nop3 m (first pair)
		(second pair))
	       (format nil
		       "~A-NOP3-X~A-U-X~A-~A"
		       cwd
		       (first pair) (second pair) m)))
	    (nop3-pairs m)))
	 (partition
	   (partition-automaton m :cwd cwd :orientation nil))
	 (tautomata ()))
    (push (make-tautomaton "Stables" stable-automata) tautomata)
    (push (make-tautomaton "Nop3s" nop3-automata) tautomata)
    (make-graph-spec cwd 'graph-spec
		     (cwd-vbits-signature cwd m)
		     (list partition)
		     tautomata)))

(defun load-l1-colorability-spec (k cwd)
  (set-current-spec (l1-colorability-spec k cwd)))

(defun l1-colorability-automaton (k cwd)
  (with-spec (l1-colorability-spec k cwd)
    (let* ((stables (automata (get-tautomaton "Stables")))
	   (nop3 (automata (get-tautomaton "Nop3s")))
	   (partition (automaton (current-spec)))
	   (automata
	     (cons partition (append stables nop3)))
	   a)
      (setf a (intersection-automata-compatible automata))
      (rename-object
       (vbits-projection a)
       (format nil "~A-L1COLORABILITY-~A" cwd k)))))

(defun table-l1-colorability-automaton (k cwd)
  (compile-automaton (l1-colorability-automaton k cwd)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; new
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun new-l1-colorability-spec (k cwd)
  (let* ((stable (stable-automaton :cwd cwd))
	 (stable-automata
	   (mapcar
	    (lambda (pair)
	      (rename-object
	       (nothing-to-c1-union-c2 stable k (first pair) (second pair))
	       (format nil
		       "~A-STABLE-C~A-U-C~A-~A"
		       cwd
		       (first pair) (second pair) k)))
	    (stable-pairs k)))
	 (nop3 (nop3-automaton cwd))
	 (nop3-automata
	   (mapcar
	    (lambda (pair)
	      (rename-object
	       (nothing-to-c1-union-c2 nop3 k (first pair) (second pair))
	       (format nil
		       "~A-NOP3-C~A-U-C~A-~A"
		       cwd
		       (first pair) (second pair) k)))
	    (nop3-pairs k)))
	 (tautomata ()))
    (push (make-tautomaton "Stables" stable-automata) tautomata)
    (push (make-tautomaton "Nop3s" nop3-automata) tautomata)
    (make-graph-spec cwd 'graph-spec
		     (cwd-color-signature cwd k)
		     ()
		     tautomata)))

(defun new-load-l1-colorability-spec (k cwd)
  (set-current-spec (new-l1-colorability-spec k cwd)))

(defun new-l1-colorability-automaton (k cwd)
  (with-spec (new-l1-colorability-spec k cwd)
    (let* ((stables (automata (get-tautomaton "Stables")))
	   (nop3s (automata (get-tautomaton "Nop3s")))
	   (automata (append stables nop3s))
	   a)
      ;;    (format *output-stream* "construction of fly-~A-L1COLORABILITY-~A~%" k cwd)
      ;;    (format *output-stream* "~A ~A ~%" stables nop3s)
      (setf a (intersection-automata-compatible automata))
      (rename-object
       (constants-color-projection a) (format nil "~A-L1COLORABILITY-~A" k cwd)))))

;; (defparameter *ct* (input-cwd-term "add_b_c(
;;  oplus(
;;   ren_c_b(
;;    ren_b_a(
;;     add_b_c(
;;      oplus(
;;       ren_c_b(
;;        ren_b_a(
;;         add_b_c(
;;          oplus(
;;           ren_c_b(
;;            ren_b_a(
;;             add_b_c(
;;              oplus(b^0100,c^0001)))),c^1000)))),c^0010)))),c^0010))"))
