;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun lca-spec (cwd)
  (let*
      ((m 3)
       (singleton-automata
	(list (table-subgraph-singleton-automaton m 3 :cwd cwd))))))
       
