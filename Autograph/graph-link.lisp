;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass link-state (graph-state oriented-mixin)
  ((ports1 :type ports :initarg :ports1 :reader ports1)
   (ports2 :type ports :initarg :ports2 :reader ports2)))

(defmethod compare-object ((s1 link-state) (s2 link-state))
  (and (compare-object (ports1 s1) (ports1 s2))
       (compare-object (ports2 s1) (ports2 s2))))

(defmethod print-object ((ppo link-state) stream)
  (format stream "{~A}-{~A}" (ports1 ppo) (ports2 ppo)))

(defgeneric make-link-state (ports1 ports2 oriented)
  (:method ((ports1 ports) (ports2 ports) oriented)
    (make-instance
     'link-state :ports1 ports1 :ports2 ports2 :oriented oriented)))

(defmethod graph-oplus-target ((po1 link-state) (po2 link-state))
  (make-link-state
   (ports-union (ports1 po1) (ports1 po2))
   (ports-union (ports2 po1) (ports2 po2))
   (oriented-p po1)))

(defmethod graph-add-target (a b (ppo link-state))
  (if (or (and (ports-member a (ports1 ppo))
	    (ports-member b (ports2 ppo)))
	  (and (not (oriented-p ppo))
	       (ports-member b (ports1 ppo))
	       (ports-member a (ports2 ppo))))
      *ok-ok-state*
      ppo))

(defmethod graph-ren-target (a b (ppo link-state))
  (let ((ports1 (subst b a (container-contents (ports1 ppo))))
	(ports2 (subst b a (container-contents (ports2 ppo)))))
    (make-link-state
     (make-ports ports1) (make-ports ports2) (oriented-p ppo))))

(defgeneric link-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg (eql nil)))
    (let* ((port (port-of root))
	   (vbits (vbits root))
	   (vb1 (aref vbits 0))
	   (vb2 (aref vbits 1)))
      (cond
	((= 1 (* vb1 vb2))
	 (make-link-state
	  (make-ports-from-port port)
	  (make-ports-from-port port)
	  *oriented*))
	((= vb1 1)
	 (make-link-state
	  (make-ports (list port))
	  (make-empty-ports)
	  *oriented*))
	((= vb2 1)
	 (make-link-state
	  (make-empty-ports)
	  (make-ports-from-port port) *oriented*))
	(t (make-link-state
	    (make-empty-ports)
	    (make-empty-ports)
	    *oriented*)))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'link-transitions-fun)))

(defun basic-link-automaton (&key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2 :orientation orientation)
   #'link-transitions-fun
   :name (cwd-automaton-name "LINK-X1-X2" cwd)))

(defun table-basic-link-automaton (&key (cwd 0) (orientation :unoriented))
  "link (arc or edge) between X1 and X2"
  (compile-automaton (basic-link-automaton :cwd cwd :orientation orientation)))

(defun link-automaton (m j1 j2 &key (cwd 0) (orientation :unoriented))
  "link (arc or edge) between Xj1 and Xj2 with m set-variables"
  (rename-object
   (y1-y2-to-xj1-xj2
    (basic-link-automaton :cwd cwd :orientation orientation) m j1 j2)
   (format nil "~A-LINK-X~A-X~A-~A" cwd j1 j2 m)))

(defun table-link-automaton (m j1 j2 cwd &key (orientation :unoriented))
  (compile-automaton (link-automaton m j1 j2 :cwd cwd :orientation orientation)))

(defun test-link (&optional (oriented nil))
  (let* ((*oriented* oriented)
	 (f (basic-link-automaton))
	 (term1
 	   (input-cwd-term
	    "ren_c_b(ren_b_a(add_b_c(oplus(ren_c_b(ren_b_a(add_b_c( \
              oplus(b^11,c^01)))),c^11))))"))
	 (term2
 	   (input-cwd-term
	    "add_c_b(oplus(ren_b_c(ren_c_a(add_c_b( \
              oplus(c^11,b^01)))),b^11))")))
    (assert (recognized-p term1 f))
    (assert (not (recognized-p term1 (complement-automaton f))))
    (assert (recognized-p term2 f))
    (assert (not (recognized-p term2 (complement-automaton f))))))
