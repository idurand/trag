;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defvar *port*)

(defgeneric member-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg null))
    (if (= (port-of root) *port*)
	*ok-ok-state*
	*neutral-state*))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'member-transitions-fun)))

(defun member-automaton (port &key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-signature cwd :orientation orientation)
   (lambda (root states)
     (let ((*neutral-state-final-p* nil)
	   (*port* port))
       (member-transitions-fun root states)))
   :name
   (cwd-automaton-name (format nil "MEMBER-~A" (port-to-string port)) cwd)))

(defun table-member-automaton (port cwd &key (orientation :unoriented))
  (compile-automaton
   (member-automaton port :cwd cwd :orientation orientation)))

(defun subgraph-member-automaton (port m j &key (cwd 0) (orientation :unoriented))
  (assert (<= 1 j m))
  (nothing-to-xj (member-automaton port :cwd cwd :orientation orientation) m j))

(defun table-subgraph-member-automaton (port m j cwd &key (orientation :unoriented))
  (compile-automaton
   (subgraph-member-automaton port m j :cwd cwd :orientation orientation)))

(defun test-member ()
  (let* ((f (member-automaton 0))
	 (term (input-cwd-term "add_a_b(oplus(a,b))"))
	 (f1 (subgraph-member-automaton 0 1 1))
	 (t1 (input-cwd-term "add_a_b(oplus(a^1,b^1))")))
    (assert (recognized-p term f))
    (assert (not (recognized-p term (complement-automaton f))))
    (assert (recognized-p t1 f1))
    (assert (not (recognized-p t1 (complement-automaton f1))))))
