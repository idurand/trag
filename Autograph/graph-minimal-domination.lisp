;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass b-object (graph-state) ;; B(x,X1)
  (;; labels of X2-nodes with neighour in X1
   (b-port2 :type integer :initarg :b-port2 :reader b-port2)
   ;; labels of neighbours of X2-nodes
   (b-2ports1 :type 2ports :initarg :b-2ports1 :reader b-2ports1)))

(defgeneric make-b-object (b-port2 b-2ports1)
  (:method ((b-port2 integer) (b-2ports1 2ports))
;;    (assert (not (container-empty-p b-2ports1)))
    (make-instance
     'b-object
     :b-port2 b-port2
     :b-2ports1 b-2ports1)))

(defmethod print-object ((b b-object) stream)
  (format stream "B(~A,~A)"
	  (port-to-string (b-port2 b))
	  (b-2ports1 b)))

(defmethod graph-ren-target (a b (obj b-object))
  (make-b-object (subst b a (b-port2 obj))
		 (ports-subst b a (b-2ports1 obj))))

(defmethod compare-object ((s1 b-object) (s2 b-object))
  (and
   (= (b-port2 s1) (b-port2 s2))
   (compare-object (b-2ports1 s1) (b-2ports1 s2))))

(defclass b-container (ordered-container) ()
  (:documentation "container for b-object"))

(defmethod container-order-fun ((c b-container)) #'strictly-ordered-p)

(defgeneric make-b-container (bs)
  (:method ((bs list))
    (make-container-generic bs 'b-container #'equal)))
	    
(defmethod ports-subst (new old (bc b-container))
  (container-mapcar-to-container (lambda (bo) (ports-subst new old bo)) bc))

(defclass c-object (graph-state) ; C(x,X1)
  ((port1 :type integer :initarg :port1 :reader port1)
   (ports1 :type ports :initarg :ports1 :reader ports1)
   (b-container :type b-container :initarg :b-container :reader b-container)))

(defgeneric make-c-object (port1 ports1 b-container)
  (:method ((port1 integer) (ports1 ports) (b-container b-container))
    (make-instance
     'c-object
     :port1 port1
     :ports1 ports1
     :b-container b-container)))

(defmethod print-object ((c c-object) stream)
  (format stream "C(~A,~A,~A)"
	  (port-to-string (port1 c))
	  (ports1 c)
	  (b-container c)))

(defmethod compare-object ((s1 c-object) (s2 c-object))
  (and
   (= (port1 s1) (port1 s2))
   (compare-object (ports1 s1) (ports1 s2))
   (compare-object (b-container s1) (b-container s2))))

(defmethod graph-ren-target (a b (s c-object))
  (make-c-object
   (subst b a (port1 s))
   (ports-subst b a (ports1 s))
   (container-mapcar-to-container (lambda (bo) (graph-ren-target a b bo)) (b-container s))))

(defclass c-container (ordered-container) ()
  (:documentation "container for cs"))

(defmethod container-order-fun ((c c-container)) #'strictly-ordered-p)

(defgeneric make-c-container (cs)
  (:method ((cs list))
    (make-container-generic cs 'c-container #'equal)))

(defmethod ports-subst (new old (cc c-container))
  (container-mapcar-to-container (lambda (co) (ports-subst new old co)) cc))

;; c port in X1, its neighbours in X1, its neighbours in X2
(defclass minimal-domination-state (graph-state oriented-mixin)
  ((2ports1 :type 2ports :initarg :2ports1 :reader 2ports1) ;; X1
   (ports2 :type ports :initarg :ports2 :reader ports2) ;; X2 with no neighbour in X1
   (c-container :type c-container :initarg :c-container :reader c-container)))

(defgeneric make-minimal-domination-state (2ports1 ports2 c-container oriented)
  (:method
      ((2ports1 2ports) (ports2 ports) (c-container c-container) oriented)
    (make-instance
     'minimal-domination-state
     :2ports1 2ports1
     :ports2 ports2
     :c-container c-container
     :oriented oriented)))

(defmethod print-object ((s minimal-domination-state) stream)
  (format stream "M(~A, ~A, ~A)"
	  (2ports1 s)
	  (ports2 s)
	  (c-container s)))

(defgeneric ports2-of (o)
  (:method ((b-container b-container))
    (make-ports (container-mapcar #'b-port2 b-container)))
  (:method ((c-object c-object))
    (ports2-of (b-container c-object)))
  (:method ((c-container c-container))
    (ports-union-gen (container-mapcar #'ports2-of c-container)))
  (:method ((s minimal-domination-state))
    (ports-union (ports2 s) (ports2-of (c-container s)))))

(defmethod ports-of ((s minimal-domination-state))
  (ports-union (ports-of (2ports1 s)) (ports2-of s)))

(defmethod graph-oplus-target ((s1 minimal-domination-state) (s2 minimal-domination-state))
  (make-minimal-domination-state
   (ports-union (2ports1 s1) (2ports1 s2))
   (ports-union (ports2 s1) (ports2 s2))
   (container-union (c-container s1) (c-container s2))
   (oriented-p s1)))

;; add the fact that we have an arc from a in X1 to b in X2
(defgeneric update-b-object (am b b-object)
  (:method ((am attributed-object) (b integer) (b-object b-object))
    (if (= (b-port2 b-object) b)
	(make-b-object b (container-adjoin am (b-2ports1 b-object)))
	b-object)))

;; a is in X1
(defgeneric update-b-container (am b b-container)
  (:method ((am attributed-object) (b integer) (b-container b-container))
    (container-mapcar-to-container
     (lambda (b-object) (update-b-object am b b-object))
     b-container)))

(defgeneric antecedants (b object)
  (:method ((b integer) (b-container b-container))
    (container-remove-if-not
      (lambda (b-object) (= b (b-port2 b-object)))
      b-container))
  (:method ((b integer) (c-object c-object))
    (antecedants b (b-container c-object)))
  (:method ((b integer) (c-container c-container))
    (container-union-gen
     (container-mapcar (lambda (c-object)
			 (antecedants b (b-container c-object)))
		       c-container))))

;; oriented
(defgeneric update-c-object (am b antecedants 2ports1 ports2 c-object)
  (:method ((am attributed-object) (b integer)
	    (antecedants b-container) (2ports1 2ports) (ports2 ports) (c-object c-object))
    (let ((a (object-of am))
	  (b-container (b-container c-object))
	  (port1 (port1 c-object))
	  (ports1 (ports1 c-object)))
      (when (and (= a port1) (ports-member b 2ports1))
	(setq ports1 (ports-adjoin b ports1)))
      ;; no b of X2 is connected to a
      (when (= a port1)
	(setf b-container
	      (if (ports-member b ports2)
		  (container-adjoin (make-b-object b (make-2ports '())) b-container)
		  (container-union antecedants b-container))))
      (setq b-container (update-b-container am b b-container))
      (make-c-object port1 ports1 b-container))))

;; a in X1	    
(defgeneric update-c-container (am b 2ports1 ports2 c-container)
  (:method ((am attributed-object) (b integer) (2ports1 2ports) (ports2 ports) (c-container c-container))
    (assert (ports-member (object-of am) 2ports1))
    (let ((antecedants (antecedants b c-container)))
      (setq c-container 
	    (container-mapcar-to-container
	     (lambda (c-object)
	       (update-c-object am b antecedants 2ports1 ports2 c-object))
	     c-container)))
    c-container))

(defmethod graph-add-target-oriented (a b (s minimal-domination-state))
  (let* ((2ports1 (2ports1 s))
	 (am (ports-member a 2ports1))
	 (bp (ports-member b (ports-of s))))
    (if (and am bp)
	(let ((c-container (c-container s))
	      (u-ports2 (ports2 s)))
	  (setq c-container (update-c-container am b 2ports1 u-ports2 c-container))
	  (when (ports-member b u-ports2)
	    (setq u-ports2 (container-remove b u-ports2)))
	  (make-minimal-domination-state
	   2ports1
	   u-ports2
	   c-container
	   (oriented-p s)))
	s)))

(defmethod graph-add-target (a b (s minimal-domination-state))
  (let ((new-s (graph-add-target-oriented a b s)))
    (unless (oriented-p s)
      (setq new-s (graph-add-target-oriented b a new-s)))
    new-s))

(defmethod graph-ren-target (a b (s minimal-domination-state))
  (make-minimal-domination-state
   (cwd-mixin-ren a b (2ports1 s))
   (ports-subst b a (ports2 s))
   (container-mapcar-to-container (lambda (c) (graph-ren-target a b c)) (c-container s))
   (oriented-p s)))

(defgeneric minimal-domination-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg (eql nil)))
    (let* ((port (port-of root))
	   (vbits (vbits root))
	   (vb1 (aref vbits 0))
	   (vb2 (aref vbits 1)))
      (cond
	((= 1 (* vb1 vb2)) nil) ; X1 I X2 must be empty
	((= vb1 1) ; vertex in X1
	 (make-minimal-domination-state
	  (make-2ports (list port))
	  (make-empty-ports)
	  (make-c-container
	   (list (make-c-object
		  port
		  (make-empty-ports)
		  (make-b-container '()))))
	  *oriented*))
	((= vb2 1) ; vertex in X2
	 (make-minimal-domination-state
	  (make-2ports '())
	  (make-ports-from-port port)
	  (make-c-container '())
	  *oriented*))
	(t (make-minimal-domination-state
	    (make-2ports '())
	    (make-empty-ports)
	    (make-c-container '())
	    *oriented*)))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'domination-transitions-fun)))

(defmethod state-final-p ((state minimal-domination-state))
  (and
   (ports-empty-p (ports2 state))
   (let ((c-container (c-container state)))
     (container-every
      (lambda (c-object)
	(or (ports-empty-p (ports1 c-object))
	    (container-find-if
	     (lambda (b-object)
	       (let ((b-2ports1 (b-2ports1 b-object)))
		 (and (= 1 (container-size b-2ports1))
		      (let ((p1 (container-car b-2ports1)))
			(and (= 1 (attribute-of p1))
			     (= (port1 c-object) (object-of p1)))))))
	     (b-container c-object))))
      c-container))))

(defun basic-minimal-domination-automaton (&key (cwd 0))
  "X1 I X2 empty and X1 minimally dominates X2"
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2)
   #'minimal-domination-transitions-fun
   :name (cwd-automaton-name "MINIMAL-DOMINATION-X1-X2" cwd)))

(defun table-basic-minimal-domination-automaton (cwd) ; minimal-domination of X1 on X2
  (compile-automaton (basic-minimal-domination-automaton :cwd cwd)))

(defun minimal-domination-automaton (m j1 j2 &key (cwd 0))
  (rename-object
   (y1-y2-to-xj1-xj2
    (basic-minimal-domination-automaton :cwd cwd) m j1 j2)
   (format nil "~A-MINIMAL-DOMINATION-X~A-X~A-~A" cwd j1 j2 m)))

(defun table-minimal-domination-automaton (cwd m j1 j2)
  (compile-automaton (minimal-domination-automaton m j1 j2 :cwd cwd)))

(defun minimal-domination-transducer (afun)
  (vbits-projection
   (attribute-automaton (minimal-domination-automaton 3 1 2) afun)))
