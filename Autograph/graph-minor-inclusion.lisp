;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun minor-inclusion-spec (edges cwd)
  (let* ((m (length edges))
	 (connectedness-automata
	   (mapcar (lambda (j)
		     (subgraph-connectedness-automaton m j :cwd cwd :orientation nil))
		   (set-iota m)))
	 (disjoint-automaton (basic-disjoint-intersection-automaton m :cwd cwd :orientation nil))
	 (link-automata
	   (mapcar
	    (lambda (pair)
	      (link-automaton m (first pair) (second pair) :cwd cwd :orientation nil))
	    edges))
	 (tautomata ()))
    (push (make-tautomaton "Connectedness" connectedness-automata) tautomata)
    (push (make-tautomaton "Links" link-automata) tautomata)
    (make-graph-spec cwd 'graph-spec
		     (cwd-vbits-signature cwd m)
		     (list disjoint-automaton)
		     tautomata)))

(defun load-minor-inclusion-spec (m cwd)
  (set-current-spec (minor-inclusion-spec m cwd)))

(defun minor-inclusion-automaton (edges &optional (cwd 0))
  (with-spec (minor-inclusion-spec edges cwd)
    (let* ((connectedness (automata (get-tautomaton "Connectedness")))
	   (links (automata (get-tautomaton "Links")))
	   (disjoint (automaton (current-spec)))
	   (automata (cons disjoint (append links connectedness)))
	   (a))
      (when *debug*
	(format *output-stream* "construction of ~A-MINOR-INCLUSION~A~%" cwd edges)
	(format *output-stream* "~A ~A ~A~%" links connectedness disjoint))
      (setf a (intersection-automata-compatible automata))
      (rename-object
       (vbits-projection a)
       (format nil "~A-MINOR-INCLUSION-~A" cwd edges)))))

(defun table-minor-inclusion-automaton (edges cwd)
  (compile-automaton (minor-inclusion-automaton edges cwd)))
