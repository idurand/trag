;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass ne-dse2-state (nr-cycle-state) ())

(defun make-ne-dse2-state (ports1 ports2 degre0 degre1)
  (make-nr-cycle-state ports1 ports2 degre0 degre1 'ne-dse2-state))

(defmethod state-final-p ((s ne-dse2-state))
  (and (not (ports-empty-p (ports-of s)))
       (ports-empty-p (ports-union (degre0 s) (degre1 s)))))

(defmethod graph-add-target (a b (s ne-dse2-state))
  (let ((ab (make-ports (list a b)))
	(ports (ports-of s)))
    (if (ports-subset-p ab ports)
	(make-ne-dse2-state
	 (ports1 s)
	 (ports2 s)
	 (container-difference (degre0 s) ports)
	 (ports-union
	  (container-difference
	   (degre1 s)
	   (next-degre-greater-than-one a b s))
	  (next-degre-one a b s)))
	s)))

(defgeneric nr-ne-deg-supeg2-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg null))
    (let ((port (port-of root)))
      (make-ne-dse2-state
       (make-ports-from-port port)
       (make-empty-ports)
       (make-ports-from-port port)
       (make-empty-ports))))
  (:method ((root (eql *empty-symbol*)) (arg null))
    (cwd-transitions-fun root arg #'nr-ne-deg-supeg2-transitions-fun))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'nr-ne-deg-supeg2-transitions-fun)))

(defun nr-ne-deg-supeg2-automaton (&optional (cwd 0))
  "every vertex has degree at least 2"
  (make-cwd-automaton
   (cwd-signature cwd)
   (lambda (root states)
     (let ((*oriented* nil)
	   (*neutral-state-final-p* nil))
       (nr-ne-deg-supeg2-transitions-fun root states)))
   :name (cwd-automaton-name "~A-NR-NE-DEG-SUPEG2" cwd)))

(defun table-nr-ne-deg-supeg2-automaton (cwd)
  (compile-automaton (nr-ne-deg-supeg2-automaton cwd)))

(defun subgraph-nr-ne-deg-supeg2-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
  (rename-object
   (nothing-to-xj (nr-ne-deg-supeg2-automaton cwd) m j)
   (format nil "~A-NR-NE-DEG-SUPEG2-X~A-~A" cwd j m)))

(defun table-subgraph-nr-ne-deg-supeg2-automaton (m j cwd)
  (compile-automaton (subgraph-nr-ne-deg-supeg2-automaton m j cwd)))

(defun test-nr-ne-deg-supeg2 ()
  (let ((a (nr-ne-deg-supeg2-automaton)))
    (assert (recognized-p (cwd-term-kn 4) a))
    (assert (not (recognized-p
		  (cwd-term-oplus (cwd-term-cycle 4) (cwd-term-stable 4)) a)))
    (assert (not (recognized-p (cwd-term-pn 4) a)))
    (assert (not (recognized-p (cwd-term-stable 4) a)))))
