;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric make-neighbours-object (l)
  (:method ((pairs list))
    (make-empty-pairs)))

(defclass nop3-state (graph-state)
  ((2ports :type 2ports :initarg :2ports :reader 2ports)
   (neighbours :initarg :neighbours :reader neighbours :type pairs)
   )
  (:documentation "state for automaton recognizing graphs with no p3 induced? subgraph"))

(defmethod state-final-p ((s nop3-state)) t)

(defmethod print-object ((s nop3-state) stream)
  (format stream "<~A,~A>" (2ports s) (neighbours s)))

(defun make-nop3-state (2ports neigbours)
  (make-instance 'nop3-state :2ports 2ports :neighbours neigbours))

(defgeneric nop3-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (let ((port (port-of root)))
      (make-nop3-state
       (make-2ports (list port))
       (make-neighbours-object '()))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'nop3-transitions-fun)))

;; verifier si c est necessaire ou pas
(defmethod nop3-transitions-fun ((root (eql *empty-symbol*)) (arg (eql nil)))
  (cwd-transitions-fun root arg #'nop3-transitions-fun))

(defmethod graph-oplus-target ((s1 nop3-state) (s2 nop3-state))
  (make-nop3-state
   (ports-union (2ports s1) (2ports s2))
   (pairs-union (neighbours s1) (neighbours s2))))

(defmethod graph-ren-target (a b (s nop3-state))
  (make-nop3-state
   (ports-subst b a (2ports s))
   (pairs-subst b a (neighbours s))))

(defmethod graph-add-target (a b (s nop3-state))
  (let* ((2ports (2ports s))
	 (ma (multiplicity a 2ports))
	 (mb (multiplicity b 2ports)))
    (if (and (plusp ma) (plusp mb))
	(when (and (< ma 2) (< mb 2))
	  (let ((neighbours (neighbours s)))
	    (when
		(container-empty-p
		 (pairs-remove-if
		  (lambda (pair)
		    (or (equal (list a b) pair)
			(= a (first pair))
			(= b (second pair))))
		  neighbours))
	      (make-nop3-state
	       2ports
	       (pairs-union
		neighbours
		(pairs-carre a b nil))))))
	s)))

(defun nop3-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd)
   (lambda (root states)
     (let ((*neutral-state-final-p* t))
       (nop3-transitions-fun root states)))
   :name (cwd-automaton-name "NOP3" cwd)))

(defun table-nop3-automaton (cwd)
  (compile-automaton
   (nop3-automaton cwd)))

(defun subgraph-nop3-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
  (rename-object
   (nothing-to-xj (nop3-automaton cwd) m j)
   (format nil "~A-NOP3-X~A-~A" cwd j m)))

(defun color-nop3-automaton (k color &optional (cwd 0))
  (assert (<= 1 color k))
  (rename-object
   (nothing-to-color 
    (nop3-automaton cwd)
    k color)
   (format nil "~A-NOP3-C~A-~A" cwd color k)))

(defun table-subgraph-nop3-automaton (m j cwd)
  (compile-automaton
   (subgraph-nop3-automaton m j cwd)))

(defun table-color-nop3-automaton (k color cwd)
  (compile-automaton
   (color-nop3-automaton k color cwd)))
