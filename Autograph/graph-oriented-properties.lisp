;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;; Le mieux est de tester la négation:
;; exist X, Y, edg(X,Y) et X non inclu dans Y et NON edge2(Y,X)
;; edg(X,Y) implique X et Y singletons
;; X non inclus Y : pour qu ils soient differents.

(defun basic-no3circuit-automaton (&key (cwd 0))
  "automaton for P(X1,X2) = Arc(X1,X2) and ~subset(X1,X2) and ~2Arc(X2,X1)"
  (rename-object
   (intersection-automaton-compatible-gen
    (basic-arc-automaton :cwd cwd) ;; Arc(X1,X2)
    (complement-automaton
     (let ((*oriented* t))
       (basic-equal-automaton :cwd cwd :orientation t))) ;; not subset (X1,X2)
    (complement-automaton (2arc-automaton 2 2 1 1 1 :cwd cwd)))
   (format nil "~A-no3circuit-X1-X2" cwd)))

(defun basic-exists-no3circuit-automaton (&key (cwd 0))
  (vbits-projection (basic-no3circuit-automaton :cwd cwd)))

(defun basic-forall-3circuit-automaton (&key (cwd 0))
  (rename-object
   (complement-automaton
    (basic-exists-no3circuit-automaton :cwd cwd))
   (cwd-automaton-name "forall-X1-X2-3circuit" cwd)))

(defun weakp2-automaton (&key (cwd 0))
  (basic-forall-3circuit-automaton :cwd cwd))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; propriete P2 de Sopena
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun basic-arc-noall2arcs-automaton (&key (cwd 0))
  "automaton for P(X1,X2) = Arc(X1,X2) and not Eq(X1,X2) and not all2Arcs(X1,X2)"
  (rename-object
   (intersection-automaton-compatible-gen
    (basic-arc-automaton :cwd cwd) ;; Arc(X1,X2)
    (complement-automaton
     (let ((*oriented* t))
       (basic-equal-automaton :cwd cwd :orientation t))) ;; X1 /= X2
    (complement-automaton (basic-all2arc-automaton :cwd cwd))) ;; not all2arcsx(X1,X2)
   (cwd-automaton-name "arc-noall2arcs-X1-X2" cwd)))

(defun basic-exists-noall2arcs-automaton (&key (cwd 0))
  (vbits-projection (basic-arc-noall2arcs-automaton :cwd cwd)))

(defun basic-forall-all2arcs-automaton (&key (cwd 0))
  (rename-object
   (complement-automaton
    (basic-exists-noall2arcs-automaton :cwd cwd))
   (cwd-automaton-name "forall-X1-X2-all2arcs" cwd)))

(defun p2-automaton (&key (cwd 0))
  (basic-forall-all2arcs-automaton :cwd cwd))

(defun weakp2-test-term ()
  (cwd-term-oriented-add
   1 0
   (cwd-term-oriented-add
    0 2
    (cwd-term-oplus
     (cwd-term-vertex 0)
     (cwd-term-ren
      0 2
      (cwd-term-oriented-add
       2 0
       (cwd-term-oriented-add
	2 1
	(cwd-term-oriented-add 
	 1 2
	 (cwd-term-oriented-add 0 1 (cwd-term-stable-cwd 3))))))))))

(defun test-p2-no-aux ()
  (cwd-term-oriented-add
   0 7
   (cwd-term-oriented-add
    0 6
    (cwd-term-oriented-add
     6 8
     (cwd-term-oriented-add
      0 5
      (cwd-term-oriented-add
       5 8
       (cwd-term-oriented-add
	5 7
	(cwd-term-oriented-add
	 0 4
	 (cwd-term-oriented-add
	  4 7
	  (cwd-term-oriented-add
	   4 6
	   (cwd-term-oriented-add
	    3 0
	    (cwd-term-oriented-add
	     3 8
	     (cwd-term-oriented-add
	      3 7
	      (cwd-term-oriented-add
	       3 6
	       (cwd-term-oriented-add
		3 5
		(cwd-term-oriented-add
		 0 2
		 (cwd-term-oriented-add
		  2 8
		  (cwd-term-oriented-add
		   2 7
		   (cwd-term-oriented-add
		    2 6
		    (cwd-term-oriented-add
		     2 5
		     (cwd-term-oriented-add
		      2 4
		      (cwd-term-oriented-add
		       1 8
		       (cwd-term-oriented-add
			1 7
			(cwd-term-oriented-add
			 1 6
			 (cwd-term-oriented-add
			  1 5
			  (cwd-term-oriented-add
			   1 4
			   (cwd-term-oriented-add
			    1 3
			    (cwd-term-multi-oplus
			     1 2 3 4 5 6 7 8 0))))))))))))))))))))))))))))

(defun test-p2-no ()
  (cwd-term-oriented-add 0 1 (test-p2-no-aux)))

(defun test-p2-random-no ()
  (cwd-term-oriented-add 0 1 (graph-term-randomize-orientation (test-p2-no-aux))))

;;(defparameter *f* (oprojection (p2-automaton 9)))

(defun random-search-p2 ()
  (loop
    with f = (p2-automaton)
    for i from 0
    for term = (test-p2-random-no) ;; then (test-p2-random-no)
    when (zerop (mod i 1000))
      do (format *error-output* "~A " i)
    when (recognized-p term f)
      do (return term)))

(defun search-p2 ()
  (let ((*clean-states-table* t))
    (loop
      with f = (p2-automaton)
      with e = (make-oterm-enumerator (test-p2-no))
      for i from 0
      for term = (call-enumerator e) ;; then (call-enumerator e)
      when (zerop (mod i 1000))
	do (format *error-output* "~A " i)
      when (recognized-p term f)
	do (return term))))
