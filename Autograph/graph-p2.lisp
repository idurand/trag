;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun arc-pairs ()
  '((1 3) (3 2) (1 4) (2 4) (5 1) (5 2) (6 1) (2 6)))

(defun prop2-oriented-spec (cwd)
  (let* ((*oriented* t)
	 (m 7)
	 (arcs-automata
	   (mapcar (lambda (pair)
		     (arc-automaton m (first pair) (second pair) cwd))
		   (arc-pairs)))
	 (partition
	   (rename-object
	    (partition-automaton m cwd) "Partition"))
	 (arc-automaton (arc-automaton 2 1 2 cwd))
	 (tautomata ()))
    (push (make-tautomaton "Arcs" arcs-automata) tautomata)
    (make-graph-spec cwd 'graph-spec
		     (cwd-vbits-signature cwd m)
		     (list arc-automaton partition)
		     tautomata)))

(defun load-prop2-oriented-spec (cwd)
  (set-current-spec (prop2-oriented-spec cwd)))

(defun prop2-oriented-automaton (&optional (cwd 0))
  (load-prop2-oriented-spec cwd)
  (let* ((arcs (automata (get-tautomaton "Arcs")))
	 (automata (cons (get-automaton "Partition") arcs))
	 (arc (automaton (current-spec)))
	 a b)
    ;;    (format *output-stream* "construction of PROP2-~A~%" cwd)
    ;;    (format *output-stream* "~A ~A~%" automata arc)
    (setf a (intersection-automata-compatible automata))
    (setf a (vbits-projection a '(1 2)))
    (setf a (complement-automaton a))
    (setf b (intersection-automaton-compatible a arc))
    (rename-object
     (complement-automaton 
      (vbits-projection b)
      )
     (format nil "O-PROP2-~A" cwd))))

(defun prop2-automaton (&optional (cwd 0))
  (rename-object
   (intersection-automaton-compatible
    (cardinality-sup-automaton 6 cwd)
    (oprojection (prop2-oriented-automaton cwd) ))
   (format nil "PROP2-~A" cwd)))

(defun table-prop2-automaton (cwd)
  (compile-automaton (prop2-automaton cwd)))

;; (input-cwd-term "add_b_a(add_a_b(oplus(oplus(b^000001,a^010000),oplus(oplus(a^100000,b^000010),oplus(b^000100,b^001000)))))")
