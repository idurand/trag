;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun pij-automaton (i j m &key (cwd 0) (orientation :unoriented))
  (complement-automaton
   (vbits-projection
    (intersection-automaton-compatible
     (subgraph-singleton-automaton (1+ m) (1+ m) :cwd cwd  :orientation nil)
     (intersection-automaton-compatible
      (subset-automaton (1+ m) (1+ m) i
			:cwd cwd :orientation orientation)
      (subset-automaton (1+ m) (1+ m) j
			:cwd cwd :orientation orientation)))
    (iota m 1)))) ;;; A VOIR

(defun disjoint-intersections-automaton (m &key (cwd 0) (orientation :unoriented))
  (if (= m 2)
      (pij-automaton 1 2 2 :cwd cwd :orientation orientation)
      (reduce
       #'intersection-automaton-compatible
       (mapcar
	(lambda (pair)
	  (pij-automaton (first pair) (second pair) m :cwd cwd :orientation orientation))
	(distinct-pairs (set-iota m))))))

(defun partition-automaton (m &key (cwd 0) (orientation :unoriented))
  (rename-object 
   (vbits-fun-automaton
    m (lambda (vbits) (= (count 1 vbits) 1))
    :cwd cwd :orientation orientation)
   (format nil "~A-PARTITION-X1-~:[~;...-~]X~A" cwd (> m 2) m)))
