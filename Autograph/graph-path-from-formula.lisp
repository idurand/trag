;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun path-p9 (cwd &key (orientation :unoriented))
  ;; cwd=2  58 rules 12 states (Error)
  ;; minimal  40 rules 8 states
  ;; cwd=3  201 rules 19 states (Error)
  ;; minimal 128 rules 12 states
  (rename-object
   (intersection-automaton-compatible-gen
    (table-subgraph-singleton-automaton 4 3 cwd :orientation orientation)
    (table-subgraph-singleton-automaton 4 4 cwd :orientation orientation)
    (table-subset-automaton 4 3 1 cwd :orientation orientation)
    (table-subset-automaton 4 3 2 cwd :orientation orientation)
    (table-subset-automaton 4 4 2 cwd :orientation orientation)
    (table-arc-or-edge-automaton 4 3 4 cwd :orientation orientation)
    (complement-automaton
     (table-subset-automaton 4 4 1 cwd :orientation orientation)))
   "P9(X1,X2,X3,X4)"))

(defun path-p8 (cwd &key (orientation :unoriented))
  ;; I(tb-2-SINGLETON-X3-4 tb-2-SINGLETON-X4-4 tb-2-SUBSET-X3-X1-4
  ;;   tb-2-SUBSET-X3-X2-4 tb-2-SUBSET-X4-X2-4 tb-C-(#-(2-SUBSET-X4-X1-4))
  ;;   tb-2-EDGE-X3-X4-4)-det  
  ;; cwd=2 2703 rules* 70 states + Error
  ;; minimal 92 rules 11 states (Success)
  ;; cwd=3 502470 rules* 993 states + Error
  (determinize-automaton
   (vbits-projection (path-p9 cwd :orientation orientation) '(1 2))))

(defun path-sp8 (cwd &key (orientation :unoriented))
  ;; det-of-I(I(I(I(I(I(Singleton-X3 Singleton-X4) SUBSET-X3-X1) SUBSET-X3-X2) SUBSET-X4-X2) C-(SUBSET-X4-X1)) Edge-X3-X4) 
  ;; cwd=2 107 rules* 11 states + Error
  ;; cwd=3  25432 rules??? 155 states + Error
  (let ((a (path-p8 cwd :orientation orientation)))
    (nminimize-automaton a)))

(defun path-p7 (cwd &key (orientation :unoriented))
  ;; cwd=2 minimal 92 rules* 11 states
  (complement-automaton (path-sp8 cwd :orientation orientation)))

(defun path-p7prime (cwd &key (orientation :unoriented))
  ;; C-(I(tb-2-SINGLETON-X3-4 tb-2-SINGLETON-X4-4 tb-2-SUBSET-X3-X1-4
  ;;   tb-2-SUBSET-X3-X2-4 tb-2-SUBSET-X4-X2-4 tb-C-(#-(2-SUBSET-X4-X1-4))
  ;;   tb-2-EDGE-X3-X4-4)-det)  
  ;; cwd=2 minimal 148 rules* 11 states
  ;; cwd=3 
  (vbits-cylindrification (path-p7 cwd :orientation orientation) '(3 4 5)))

(defun path-p6 (cwd &key (orientation :unoriented))
  ;; I(tb-C-(I(tb-2-SINGLETON-X3-4 tb-2-SINGLETON-X4-4 tb-2-SUBSET-X3-X1-4
  ;;   tb-2-SUBSET-X3-X2-4 tb-2-SUBSET-X4-X2-4 tb-C-(#-(2-SUBSET-X4-X1-4))
  ;;   tb-2-EDGE-X3-X4-4)-det)
  ;;   tb-2-SUBSET-X3-X1-4 tb-C-(#-(2-SUBSET-X4-X1-4)))
  ;; cwd=2 minimal 316 rules* 21 states
  (intersection-automaton-compatible-gen
   (path-p7prime cwd :orientation orientation)
   (table-subset-automaton 5 3 1 cwd :orientation orientation)
   (complement-automaton
    (table-subset-automaton 5 4 1 cwd :orientation orientation))))

;; I(I(C-(det-of-I(I(I(I(I(I(Singleton-X3 Singleton-X4) SUBSET-X3-X1) SUBSET-X3-X2) SUBSET-X4-X2) C-(SUBSET-X4-X1)) Edge-X3-X4)) SUBSET-X3-X1) C-(SUBSET-X4-X1))
;; 308 states 97288 rules 

(defun path-sp6 (cwd &key (orientation :unoriented))
  ;; I(tb-C-(I(tb-2-SINGLETON-X3-4 tb-2-SINGLETON-X4-4 tb-2-SUBSET-X3-X1-4
  ;; tb-2-SUBSET-X3-X2-4 tb-2-SUBSET-X4-X2-4 tb-C-(#-(2-SUBSET-X4-X1-4))
  ;; tb-2-EDGE-X3-X4-4)-det)
  ;; tb-2-SUBSET-X3-X1-4 tb-C-(#-(2-SUBSET-X4-X1-4)))  
  ;; 316 rules* 21 states  (orientation nil)
  (minimize-automaton (path-p6 cwd :orientation orientation)))

(defun path-p5 (cwd fly &key (orientation :unoriented))
  ;; 144 rules 14 states (Success)
  (let ((a (vbits-projection (path-sp6 cwd :orientation orientation) '(2 3 4))))
    (setf a (determinize-automaton-to-x a fly))
    (unless fly
      (nminimize-automaton a))
    a))

(defun path-p4 (cwd fly &key (orientation :unoriented))
  ;; 144 rules 13 states + Error
  (complement-automaton (path-p5 cwd fly :orientation orientation)))

(defun path-p4prime (cwd fly &key (orientation :unoriented))
  ;; 158 rules 13 states + Error
  (vbits-cylindrification (path-p4 cwd fly :orientation orientation) '(1)))

(defun path-p3 (cwd fly &key (orientation :unoriented))
  ;; 189 rules* 19 states + Error
  (op-automata-list
   (list 
    (path-p4prime cwd fly :orientation orientation)
    (table-subset-automaton 4 3 1 cwd :orientation orientation)
    (table-subset-automaton 4 4 1 cwd :orientation orientation)
    (complement-automaton (table-equal-automaton 4 3 4 cwd :orientation orientation))
    (table-subgraph-cardinality-automaton 2 4 1 cwd :orientation orientation))))

(defun path-p2 (cwd fly &key (orientation :unoriented))
  ;; 109 rules* 14 states + Error
   (let ((a (intersection-automaton-compatible-gen
	     (path-p3 cwd fly :orientation orientation)
	     (table-subgraph-singleton-automaton 4 3 cwd :orientation orientation)
	     (table-subgraph-singleton-automaton 4 4 cwd :orientation orientation))))
     (unless fly 
       (nminimize-automaton a))
     a))

(defun path-p1 (cwd fly &key (orientation :unoriented))
  ;; 83 rules* 12 states
  (let ((a (vbits-projection (path-p2 cwd fly :orientation orientation) '(1 2))))
    (setq a (determinize-automaton a))
    (unless fly 
      (nminimize-automaton a))
    a))

(defun basic-path-automaton-from-formula (&key (cwd 0) (orientation :unoriented))
  (intersection-automaton-compatible-gen
   (path-p1 cwd t :orientation orientation)
   (subset-automaton 2 1 2 :cwd cwd :orientation orientation)))

(defun table-basic-path-automaton-from-formula (cwd &key (fly nil) (orientation :unoriented))
  (intersection-automaton-compatible-gen
   (path-p1 cwd fly :orientation orientation)
   (table-subset-automaton 2 1 2 cwd :orientation orientation)))
