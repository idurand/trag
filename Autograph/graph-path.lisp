;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; PATH(X1,X2) == X1 subseteq X2 and |X1| = 2 and G[X2] links the two vertices of X1

(in-package :autograph)

(defclass port-rel-state (graph-state)
  ((ports :type 'ports :initarg :ports :reader ports)
   (ports-relation :type ports-relation :initarg :ports-relation :reader ports-relation)))

(defmethod ports-of ((s port-rel-state))
  (ports-union (ports s) (ports-of (ports-relation s))))

(defmethod print-object ((port-rel-state port-rel-state) stream)
  (format stream "1~A~A"
	  (ports port-rel-state)
	  (ports-relation port-rel-state)))

(defmethod compare-object ((po1 port-rel-state) (po2 port-rel-state))
  (and
   (compare-object (ports po1) (ports po2))
   (compare-object (ports-relation po1) (ports-relation po2))))

(defclass port-port-rel-state (graph-state)
  ((ports-relation :type ports-relation :initarg :ports-relation :reader ports-relation)
   (ports1 :type ports :initarg :ports1 :reader ports1)
   (ports2 :type ports :initarg :ports2 :reader ports2)))

(defmethod ports-of ((s port-port-rel-state))
  (ports-union
   (ports-union (ports1 s) (ports2 s))
   (ports-of (ports-relation s))))

(defmethod compare-object ((pppo1 port-port-rel-state) (pppo2  port-port-rel-state))
  (and
   (compare-object (ports-relation pppo1) (ports-relation pppo2))
   (compare-object (ports1 pppo1) (ports1 pppo2))
   (compare-object (ports2 pppo1) (ports2 pppo2))))

(defmethod print-object ((pppo port-port-rel-state) stream)
  (format stream "2~A-~A~A"
	  (ports1 pppo)
	  (ports2 pppo)
	  (ports-relation pppo)))

(defgeneric make-port-rel-state (ports rel-state)
  (:method ((ports ports) (ports-relation ports-relation))
    (make-instance
     'port-rel-state
     :ports ports
     :ports-relation ports-relation)))

(defgeneric make-port-port-rel-state (ports1 ports2 ports-relation)
  (:method ((ports1 ports) (ports2 ports) (ports-relation ports-relation))
    (let ((ordered (strictly-ordered-p ports1 ports2)))
      (unless ordered
	(psetf ports1 ports2 ports2 ports1))
      (make-instance
       'port-port-rel-state
       :ports1 ports1
       :ports2 ports2
       :ports-relation ports-relation))))

(defmethod graph-ren-target (a b (ppo port-rel-state))
  (make-port-rel-state
   (ports-subst b a (ports ppo))
   (ports-relation-subst b a (ports-relation ppo))))

(defmethod graph-ren-target (a b (pppo port-port-rel-state))
  (make-port-port-rel-state
   (ports-subst b a (ports1 pppo))
   (ports-subst b a (ports2 pppo))
   (ports-relation-subst b a (ports-relation pppo))))

(defmethod ports-of ((pppo port-port-rel-state))
  (merge-containers
   (list
    (ports1 pppo)
    (ports2 pppo)
    (ports-of (ports-relation pppo)))))

(defmethod graph-add-target (a b (pppo port-port-rel-state))
  (let ((ports1 (ports1 pppo))
	(ports2 (ports2 pppo))
	 (ports (ports-of pppo)))
    (if (and (ports-member a ports) (ports-member b ports))
	(let* ((ports-relation (ports-relation pppo))
	       (pairs (pairs ports-relation))
	       (oriented (oriented-p ports-relation)))
	  (make-port-port-rel-state
	   (ports-union
	    ports1
	    (container-point-point ports1 (pairs-carre a b oriented) pairs))
	   (ports-union
	    ports2
	    (container-point-point ports2 (pairs-carre a b oriented) pairs))
	   (new-ports-relation-for-add a b ports-relation)))
	pppo)))

(defmethod graph-oadd-target (a b (pppo port-port-rel-state))
  (graph-add-target a b pppo))

(defmethod graph-ren-target (a b (ppo port-rel-state))
  (make-port-rel-state
   (ports-subst b a (ports ppo))
   (ports-relation-subst b a (ports-relation ppo))))

(defmethod graph-ren-target (a b (pppo port-port-rel-state))
  (make-port-port-rel-state
   (ports-subst b a (ports1 pppo))
   (ports-subst b a (ports2 pppo))
   (ports-relation-subst b a (ports-relation pppo))))

(defmethod graph-add-target (a b (ppo port-rel-state))
  (let ((ports (ports-of ppo)))
    (if (and (ports-member a ports) (ports-member b ports))
	(let ((ports1 (ports ppo))
	      (ports-relation (ports-relation ppo)))
	  (make-port-rel-state
	   (ports-union
	    ports1 (container-point
		    (container-point ports1 (pairs-carre a b (oriented-p ports-relation)))
		    (pairs ports-relation)))
	   (new-ports-relation-for-add a b ports-relation)))
	ppo)))

(defmethod graph-oplus-target ((p1 ok-state) (p2 port-rel-state))
  (declare (ignore p1 p2))
  nil)

(defmethod graph-oplus-target ((p1 port-rel-state) (p2 ok-state))
  (declare (ignore p1 p2))
  nil)

(defmethod graph-oplus-target ((p1 port-rel-state) (p2 port-port-rel-state))
  nil)

(defmethod graph-oplus-target ((p1 port-port-rel-state) (p2 port-rel-state))
  nil)

(defmethod graph-oplus-target ((p1 port-port-rel-state) (p2 port-port-rel-state))
  nil)

(defmethod graph-oplus-target ((p1 rel-state) (p2 port-rel-state))
  (make-port-rel-state
   (ports p2)
   (ports-relation-union (ports-relation p1) (ports-relation p2))))

(defmethod graph-oplus-target ((p1 port-rel-state) (p2 rel-state))
  (make-port-rel-state
   (ports p1)
   (ports-relation-union (ports-relation p1) (ports-relation p2))))

(defmethod graph-oplus-target ((p1 port-rel-state) (p2 port-rel-state))
  (make-port-port-rel-state
   (ports p1)
   (ports p2)
   (ports-relation-union (ports-relation p1) (ports-relation p2))))

(defmethod graph-oplus-target ((p1 rel-state) (p2 port-port-rel-state))
  (make-port-port-rel-state
   (ports1 p2)
   (ports2 p2)
   (ports-relation-union (ports-relation p1) (ports-relation p2))))

(defmethod graph-oplus-target ((p1 port-port-rel-state) (p2 rel-state))
  (make-port-port-rel-state
   (ports1 p1)
   (ports2 p1)
   (ports-relation-union (ports-relation p1) (ports-relation p2))))

(defgeneric path-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg (eql nil)))
    (let* ((vbits (vbits root))
	   (vbit1 (aref vbits 0))
	   (vbit2 (aref vbits 1)))
      (if
       (and (zerop vbit1) (zerop vbit2))
       (make-rel-state (make-empty-ports-relation *oriented*))
       (let ((port (port-of root)))
	 (cond
	   ((and (zerop vbit1) (= 1 vbit2))
	    (make-rel-state (make-ports-relation (list (list port port)) *oriented*)))
	   ((and (= vbit1 1) (= 1 vbit2))
	    (make-port-rel-state
	     (make-ports (list port))
	     (make-ports-relation (list (list port port)) *oriented*)))
	   (t nil))))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'path-transitions-fun)))

(defgeneric path-ok-fun-add (a b uncasted-state)
  (:method (a b (s uncasted-state))
    (declare (ignore a))
    (declare (ignore b))
    (declare (ignore s))
    nil)
  (:method (a b (s port-port-rel-state))
    (let* ((ports (ports-of s))
	   (ports1 (ports1 s))
	   (ports2 (ports2 s))
	   (pao (ports-relation s))
	   (oriented (oriented-p pao)))
      (when
	  (and
	   (ports-member a ports)
	   (ports-member b ports)
	   (or
	    (and (ports-member a ports1) (ports-member b ports2))
	    (and (not oriented)
		 (ports-member a ports2) (ports-member b ports1))
	    (and
	     (ports-member a ports1)
	     (ports-member a ports2)) ;; (b,b) necessarily in pao
	    (and
	     (ports-member b ports1)
	     (ports-member b ports2)))) ;; (b,b) necessarily in pao
	*ok-state*))))

(defun basic-path-automaton (&key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2)
   (lambda (root states)
     (let ((*ok-fun-add* #'path-ok-fun-add)
	   (*oriented* (eq t orientation)))
       (path-transitions-fun root states)))
   :name (cwd-automaton-name "PATH-X1-X2" cwd)))

(defun table-basic-path-automaton (cwd &key (orientation :unoriented))
  (compile-automaton (basic-path-automaton :cwd cwd :orientation orientation)))

(defun test-graph-path ()
  (let ((a (basic-path-automaton))
	(tok (input-cwd-term "add_a_b(oplus(a^11,oplus(b^01,a^11)))"))
	(tok2 (input-cwd-term "oplus(add_a_b(oplus(a^11,b^01)),add_c_d(oplus(c^01, d^11))) "))
	(tnok (input-cwd-term "add_a_b(oplus(a^11,oplus(b^00,a^11)))")))
    (assert (recognized-p tok a))
    (assert (not (recognized-p tok2 a)))
    (assert (recognized-p (cwd-term-unoriented-add 1 2 tok2) a))
    (assert (not (recognized-p tok (complement-automaton a))))
    (assert (not (recognized-p tnok a)))
    (assert (recognized-p tnok (complement-automaton a)))))
