;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass port-state (graph-state)
  ((ports :type ports :initarg :ports :reader ports)))

(defmethod ports-of ((port-state port-state))
  (ports port-state))

(defmethod compare-object ((po1 port-state) (po2 port-state))
  (compare-object (ports po1) (ports po2)))

(defmethod print-object ((port-state port-state) stream)
  (format stream "~A" (ports port-state)))

(defgeneric make-port-state (ports)
  (:method ((ports ports)) (make-instance 'port-state :ports ports))
  (:method ((ports list))
    (make-instance 'port-state :ports (make-ports ports))))

(defun make-empty-port-state ()
  (make-port-state '()))

(defmethod graph-add-target (a b (s port-state)) s)

(defmethod graph-oplus-target ((s1 port-state) (s2 port-state))
  (make-port-state (ports-union (ports s1) (ports s2))))

(defmethod graph-ren-target (a b (po port-state))
  (make-port-state (ports-subst b a (ports po))))

(defgeneric port-transitions-fun (root states)
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'port-transitions-fun))
  (:method ((root annotated-symbol) (arg list))
    (port-transitions-fun (symbol-of root) arg))
  (:method ((root (eql *empty-symbol*)) (arg (eql nil)))
    (cwd-transitions-fun root arg #'port-transitions-fun))
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (make-port-state (list (port-of root)))))

(defun port-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd :orientation :unoriented)
   #'port-transitions-fun
   :name (cwd-automaton-name "PORT" cwd)))

(defun table-port-automaton (cwd)
  (compile-automaton (port-automaton cwd)))

(defun color-port-automaton (k &optional (cwd 0))
  (nothing-to-colors-constants (port-automaton cwd) k))

(defun subgraph-port-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
  (nothing-to-xj (port-automaton cwd) m j))
  
(defun table-subgraph-port-automaton (cwd m j)
  (compile-automaton (subgraph-port-automaton m j cwd)))
