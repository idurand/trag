;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass prefix-order-state (graph-state)
  ((num :initarg :num :initform 0 :reader num)))

(defun make-prefix-order-state (num)
  (make-instance 'prefix-order-state :num num))

(defmethod state-final-p ((s prefix-order-state)) nil)

(defmethod print-object ((s prefix-order-state) stream)
  (format stream "q~A" (num s)))

(defmethod graph-oplus-target ((s1 prefix-order-state) (s2 prefix-order-state))
  (let ((num1 (num s1))
	(num2 (num s2)))
    (when (zerop num1)
      (return-from graph-oplus-target s2))
    (when (zerop num2)
      (return-from graph-oplus-target s1))
    (when (< num1 num2)
      *ok-state*)))

(defmethod graph-add-target (a b (s prefix-order-state)) s)

(defmethod graph-ren-target (a b (s prefix-order-state)) s)

(defgeneric prefix-order-transitions-fun (root arg)
  (:method ((root vbits-constant-symbol) (arg (eql nil)))
    (let* ((vbits (vbits root))
	   (vb1 (aref vbits 0))
	   (vb2 (aref vbits 1)))
      (unless (= 1 (* vb1 vb2))
	(cond
	  ((= 1 vb1) 
	   (make-prefix-order-state 1))
	  ((= 1 vb2)
	   (make-prefix-order-state 2))
	  (t *neutral-state*)))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'prefix-order-transitions-fun)))

(defun basic-prefix-order-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2)
   (lambda (root states)
     (let ((*neutral-state-final-p* nil))
       (prefix-order-transitions-fun root states)))
   :name (cwd-automaton-name "~A-X1-<-X2" cwd)))

(defun prefix-order-automaton (m j1 j2 &optional (cwd 0))
  "automaton for Xj1 < Xj2 with m bits)"
  (rename-object
   (y1-y2-to-xj1-xj2
    (basic-prefix-order-automaton cwd) m j1 j2)
   (format nil "~A-X~A-<-X~A-~A" cwd j1 j2 m)))

(defun table-prefix-order-automaton (m j1 j2 cwd) 
  (compile-automaton (prefix-order-automaton m j1 j2 cwd)))

(defun table-basic-prefix-order-automaton (cwd) 
  (compile-automaton (basic-prefix-order-automaton cwd)))
