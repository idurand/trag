;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;; partition X1, X2, ... , Xm none of them empty

(defun proper-partition-automaton (m &key (cwd 0) (orientation :unoriented))
  (rename-object
   (intersection-automata
    (cons
     (vbits-fun-automaton
      m
      (lambda (vbits)
	(= (count 1 vbits) 1))
      :cwd cwd :orientation orientation)
     (mapcar (lambda (i)
	       (complement-automaton
		(subgraph-empty-automaton m i :cwd cwd :orientation orientation)))
	     (set-iota m))))
   (format
    nil
    "~A-PROPER-PARTITION-X1-~:[~;...-~]X~A"
    cwd (> m 2) m)))

(defun color-proper-partition-automaton (k &key (cwd 0) (orientation :unoriented))
  (rename-object
   (intersection-automata
    (mapcar (lambda (color)
	      (complement-automaton (one-color-empty-automaton k color :cwd cwd :orientation orientation)))
	    (color-iota k)))
   (format
    nil
    "~A-COLOR-PROPER-PARTITION-C0-~:[~;...-~]C~A"
    cwd (> k 1) k)))

(defun table-proper-partition-automaton (m cwd &key (orientation :unoriented))
  (compile-automaton
   (proper-partition-automaton m :cwd cwd :orientation orientation)))
