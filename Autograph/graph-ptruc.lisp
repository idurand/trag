;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun ptruc-spec (m truc &optional (cwd 0))
  (let* ((name (symbol-name truc))
	 (truc
	   (funcall
	    (find-symbol
	     (format nil "~A-AUTOMATON" (string-upcase truc))) cwd))
	 (truc-automata
	   (mapcar
	    (lambda (j)
	      (rename-object
	       (nothing-to-xj truc m j)
	       (format nil
		       "~A-~A-X~A-~A"
		       cwd name j m)))
	    (set-iota m)))
	 (partition
	   (partition-automaton m :cwd cwd))
	 (tautomata ()))
    (push (make-tautomaton "Trucs" truc-automata) tautomata)
    (make-graph-spec cwd 'graph-spec
		     (cwd-vbits-signature cwd m)
		     (list partition)
		     tautomata)))

(defun load-ptruc-spec (m truc &optional (cwd 0))
  (set-current-spec (ptruc-spec m truc cwd)))

(defun ptruc-automaton (m truc &optional (cwd 0))
  (load-ptruc-spec m truc cwd)
  (let ((name (symbol-name truc))
	(trucs (automata (get-tautomaton "Trucs")))
	(partition (automaton (current-spec)))
	a)
    (when *debug*
      (format *output-stream*
	      "construction of ~A-P~A-~A~%" cwd name m)
      (format *output-stream* "~A ~A~%" trucs partition))
    (setf a (intersection-automata-compatible (cons partition trucs)))
    (setf a (vbits-projection a))
    (rename-object a (format nil "~A-P~A-~A" cwd name m))))

(defun pforest-automaton (m &optional (cwd 0))
  (ptruc-automaton m 'forest cwd))

(defun pclique-automaton (m &optional (cwd 0))
  (ptruc-automaton m 'clique cwd))
