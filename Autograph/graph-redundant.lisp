;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass redundant-state (graph-state)
  ((isolated-ports :reader isolated-ports :initarg :isolated-ports
		   :type ports)
   (edges :reader edges :initarg :edges :type ports-relation)))

(defmethod state-final-p ((s redundant-state)) nil)

(defmethod compare-object ((s1 redundant-state) (s2 redundant-state))
  (and
   (compare-object (isolated-ports s1) (isolated-ports s2))
   (compare-object (edges s1) (edges s2))))

(defmethod print-object ((nrs redundant-state) stream)
  (format stream "[~A|~A]" (isolated-ports nrs) (edges nrs)))

(defun make-redundant-state (isolated-ports edges)
  (make-instance
   'redundant-state :isolated-ports isolated-ports :edges edges))

(defmethod ports-of ((nrs redundant-state))
  (ports-union (isolated-ports nrs) (ports-of (edges nrs))))

(defmethod graph-oplus-target ((nrs1 redundant-state) (nrs2 redundant-state))
  (make-redundant-state
   (ports-union (isolated-ports nrs1) (isolated-ports nrs2)) 
   (ports-relation-union (edges nrs1) (edges nrs2))))

(defmethod graph-add-target (a b (nrs redundant-state))
  (let ((edges (edges nrs))
	(connected-ports (make-ports (list a b)))
	(ports (ports-of nrs)))
    (if (ports-relation-member (list a b) edges)
	*ok-ok-state*
	(if (ports-subset-p connected-ports ports)
	    (make-redundant-state
	     (container-difference (isolated-ports nrs) connected-ports)
	     (ports-relation-adjoin (list a b) edges))
	    nrs))))

(defmethod graph-ren-target (a b (nrs redundant-state))
  (make-redundant-state
   (ports-subst b a (isolated-ports nrs))
   (ports-relation-subst b a (edges nrs))))

(defgeneric redundant-transitions-fun (root arg)
  (:method ((root (eql *empty-symbol*)) (arg (eql nil)))
    (cwd-transitions-fun root arg #'redundant-transitions-fun))
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (make-redundant-state
     (make-ports-from-port (port-of root))
     (make-empty-ports-relation *oriented*)))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'redundant-transitions-fun)))

(defun redundant-automaton (oriented &key (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd :orientation oriented)
   (lambda (root states)
     (let ((*oriented* oriented))
       (redundant-transitions-fun root states)))
   :name (cwd-automaton-name "REDUNDANT" cwd)))

(defun non-redundant-automaton (oriented &key (cwd 0))
  (complement-automaton (redundant-automaton oriented :cwd cwd)))

(defun table-redundant-automaton (oriented cwd)
  (compile-automaton (redundant-automaton oriented :cwd cwd)))

(defun table-non-redundant-automaton (oriented cwd)
  (complement-automaton (table-redundant-automaton oriented cwd)))

(defun subgraph-non-redundant-automaton (oriented m j &key (cwd 0))
  (assert (<= 1 j m))
  (nothing-to-xj (non-redundant-automaton oriented :cwd cwd) m j))

(defun table-subgraph-non-redundant-automaton (oriented m j cwd)
  (compile-automaton
   (subgraph-non-redundant-automaton oriented m j :cwd cwd)))

(defun test-graph-redundant-non-oriented ()
  (let* ((a (redundant-automaton nil))
	 (tnok (cwd-term-pn 4))
	 (tok (cwd-term-unoriented-add 0 1 tnok)))
    (assert (recognized-p tok a))
    (assert (not (recognized-p tnok a)))))

(defun test-graph-redundant-oriented ()
  (let* ((a (redundant-automaton t))
	 (tok (cwd-term-pn 4 :oriented t))
	 (tnok (cwd-term-oriented-add 0 1 tok)))
    (assert (not (recognized-p tok a)))
    (assert (recognized-p tnok a))))

(defun test-graph-redundant ()
  (test-graph-redundant-non-oriented)
  (test-graph-redundant-oriented))
