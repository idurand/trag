;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;;; automaton for regularity: every vertex has the same degree
;;; for non redundant terms only

(in-package :autograph)

(defclass regular-state (graph-state)
  ((count-fun :initarg :count-fun :reader count-fun)
   (degre-fun :initarg :degre-fun :reader degre-fun)))

(defun regular-state-p (state)
  (typep state 'regular-state))

(defmethod print-object ((s regular-state) stream)
  (format stream "<~A|~A>" (count-fun s) (degre-fun s)))

(defmethod compare-object ((s1 regular-state) (s2 regular-state))
  (and (compare-object (count-fun s1) (count-fun s2))
       (compare-object (degre-fun s1) (degre-fun s2))))

(defun make-regular-state (count-fun degre-fun)
  (make-instance 'regular-state :count-fun count-fun :degre-fun degre-fun))

(defmethod state-final-p ((state regular-state))
  (regular-degre-fun-p (degre-fun state)))

(defmethod graph-oplus-target ((s1 regular-state) (s2 regular-state))
  (let ((degre-fun (merge-degre-fun (degre-fun s1) (degre-fun s2))))
    (when degre-fun
      (make-regular-state
       (merge-count-fun (count-fun s1) (count-fun s2))
       degre-fun))))

(defmethod graph-ren-target (a b (s regular-state))
  (let* ((count-fun (ren-count-fun a b (count-fun s)))
	 (nb (get-port-value b count-fun)))
    (if (plusp nb)
	(let ((degre-fun (ren-degre-fun a b (degre-fun s))))
	  (when degre-fun
	    (make-regular-state count-fun degre-fun)))
	s)))

(defmethod graph-add-target (a b (s regular-state))
  (let ((na (get-port-value a (count-fun s)))
	(nb (get-port-value b (count-fun s))))
    (if (and (plusp na) (plusp nb))
	(let ((new-degre-fun (copy-port-fun (degre-fun s))))
	  (incf-port-value nb a new-degre-fun)
	  (incf-port-value na b new-degre-fun)
	  (make-regular-state
	   (count-fun s)
	   new-degre-fun))
	s)))

(defgeneric regular-transitions-fun (root arg))

(defmethod regular-transitions-fun ((root cwd-constant-symbol) (arg (eql nil)))
  (let ((port (port-of root)))
    (let ((count-fun (make-count-fun))
	  (degre-fun (make-degre-fun)))
      (incf-port-value 1 port degre-fun)
      (incf-port-value 1 port count-fun)
      (make-regular-state
       count-fun
       degre-fun))))

(defmethod regular-transitions-fun ((root abstract-symbol) (arg list))
  (cwd-transitions-fun root arg #'regular-transitions-fun))

(defgeneric regular-applicable-annotation-p (root arg))
(defmethod regular-applicable-annotation-p
    ((root annotated-symbol) (arg list))
  (and 
   (oplus-symbol-p root)
   ;; the connected ports must be in differente states
   ;; otherwise the failure would have been detected earlier
   (let ((ports (loop
		  for state in arg
		  when (regular-state-p state)
		    collect (ports state))))
     (and ports
	  (let ((relation (annotation root)))
	    (some (lambda (edge)
		    (let ((a (first edge))
			  (b (second edge)))
		      (some
		       (lambda (pair)
			 (edge-connecting-ports-p
			  a b (first pair) (second pair)))
		       (cartesian-product (list ports ports)))))
		  relation))))))

;; optimization using add-annotation
(defmethod regular-transitions-fun ((root annotated-symbol) (arg list))
  (if (add-symbol-p root) ;; the add has already been treated in oplus below
      (car arg)
      (let ((applicable-annotation (regular-applicable-annotation-p root arg)))
	;;    (when applicable-annotation
	;;      (format *error-output* "applying annotation ~A ~A ~%" root arg))
	(unless applicable-annotation
	  (regular-transitions-fun (symbol-of root) arg)))))

(defmethod regular-transitions-fun ((root (eql *empty-symbol*)) (arg (eql nil)))
  (cwd-transitions-fun root arg #'regular-transitions-fun))

(defun regular-automaton (&optional (cwd 0))
  (when (= 1 cwd)
    (return-from regular-automaton
      (stable-automaton :cwd 1)))
  (make-cwd-automaton
   (cwd-signature cwd)
   (lambda (root states)
     (let ((*neutral-state-final-p* t))
       (regular-transitions-fun root states)))
   :sink-state (if (= 1 cwd) *success-state* nil)
   :name (cwd-automaton-name "REGULAR" cwd)
   :precondition-automaton (non-redundant-automaton nil :cwd cwd)))

(defun table-regular-automaton (cwd)
  (compile-automaton (regular-automaton cwd)))

(defun subgraph-regular-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
  (nothing-to-xj (regular-automaton cwd) m j))

(defun color-regular-automaton (k color &optional (cwd 0))
  (assert (and (<= 0 color) (< color k)))
   (nothing-to-color (regular-automaton cwd) k color))

(defun table-subgraph-regular-automaton (m j cwd)
  (compile-automaton (subgraph-regular-automaton m j cwd)))

(defun table-color-regular-automaton (k color cwd)
  (compile-automaton (color-regular-automaton k color cwd)))

(defun test-regular (&optional (n 4))
  (let* ((f (regular-automaton n))
	 (stable (cwd-term-stable n))
	 (stablen (cwd-term-stable-cwd n))
	 (cn (cwd-term-cycle n))
	 (kn (cwd-term-kn n))
	 (pn (cwd-term-pn n))
	 (cf (complement-automaton f)))
    (assert (recognized-p stable f))
    (assert (recognized-p kn f))
    (assert (not (recognized-p kn cf)))
    (when (> n 2)
      (assert (recognized-p pn cf)))
    (assert (not (recognized-p stable cf)))
    (assert (recognized-p stablen f))
    (assert (not (recognized-p stablen cf)))
    (when (>= n 4)
      (assert (recognized-p cn f))
      (assert (not (recognized-p cn cf))))))
