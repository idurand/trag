;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; from formula
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun basic-same-neighbours (&key (cwd 0))
  (rename-object
   (complement-automaton
    (vbits-projection
     (intersection-automaton-compatible-gen
      (subgraph-singleton-automaton 4 4 :cwd cwd :orientation nil)
      (subset-automaton 4 4 1 :cwd cwd :orientation nil)
      (union-automaton
       (intersection-automaton-compatible
	(edge-automaton 4 2 4 :cwd cwd)
	(complement-automaton (edge-automaton 4 3 4 :cwd cwd)))
       (intersection-automaton-compatible
	(edge-automaton 4 3 4 :cwd cwd)
	(complement-automaton (edge-automaton 4 2 4 :cwd cwd)))))
     '(1 2 3)))
   (cwd-automaton-name "X2-X3-SAME-NEIGHBOURS-IN-X1" cwd)))

(defun basic-same-neighbours-test ()
  (cwd-term-oplus
   (cwd-term-vertex 0 '(1 0 0))
   (cwd-term-unoriented-add
    0 1
    (cwd-term-multi-oplus
     (cwd-term-vertex 0 '(1 0 0))
     (cwd-term-vertex 0 '(1 0 0))
     (cwd-term-vertex 1 '(0 1 0))
     (cwd-term-vertex 1 '(0 0 1))))))

(defun basic-same-neighbours-test-not ()
  (cwd-term-unoriented-add
   2 3
   (cwd-term-oplus
    (cwd-term-vertex 3 '(0 0 1))
    (cwd-term-unoriented-add
     1 2
     (cwd-term-oplus
      (cwd-term-vertex 2 '(1 0 0))
      (cwd-term-unoriented-add
       0 1
       (cwd-term-oplus
	(cwd-term-vertex 0 '(1 0 0))
	(cwd-term-vertex 1 '(0 1 0)))))))))

(defun basic-same-neighbours-test-not4 ()
  (cwd-term-unoriented-add
   2 3
   (cwd-term-oplus
    (cwd-term-vertex 3 '(0 0 1 0))
    (cwd-term-unoriented-add
     1 2
     (cwd-term-oplus
      (cwd-term-vertex 2 '(1 0 0 0))
      (cwd-term-unoriented-add
       0 1
       (cwd-term-oplus
	(cwd-term-vertex 0 '(1 0 0 1))
	(cwd-term-vertex 1 '(0 1 0 0)))))))))
