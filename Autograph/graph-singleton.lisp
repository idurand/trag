;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric singleton-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg null)) *ok-state*)
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'singleton-transitions-fun))
  (:method ((root (eql *empty-symbol*)) (arg null))
    (cwd-transitions-fun root arg #'singleton-transitions-fun)))

(defun singleton-automaton (&key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-signature cwd :orientation orientation)
   (lambda (root states)
     (let ((*neutral-state-final-p* nil))
       (singleton-transitions-fun root states)))
   :name (cwd-automaton-name "SINGLETON" cwd)))

(defun table-singleton-automaton (cwd &key (orientation :unoriented))
  (compile-automaton
   (singleton-automaton :cwd cwd :orientation orientation)))

(defun subgraph-singleton-automaton (m j &key (cwd 0) (orientation :unoriented))
  (nothing-to-xj
   (singleton-automaton :cwd cwd :orientation orientation)  m j))

(defun table-subgraph-singleton-automaton (m j cwd &key (orientation :unoriented))
  (compile-automaton
   (subgraph-singleton-automaton m j :cwd cwd :orientation orientation)))

(defun test-singleton ()
  (let* ((f (subgraph-singleton-automaton 1 1))
	 (of (subgraph-singleton-automaton 1 1 :orientation t))
	 (term (input-cwd-term "add_a_b(add_a_b(ren_c_b(oplus(a^1,oplus(b^0,c^0)))))"))
	 (oterm (input-cwd-term "add_a->b(add_a->b(ren_c_b(oplus(a^0,oplus(b^1,c^0)))))")))
    (assert (recognized-p term f))
    (assert (recognized-p oterm of))
    (assert (not (recognized-p term (complement-automaton f))))
    (assert (not (recognized-p oterm (complement-automaton of))))))
