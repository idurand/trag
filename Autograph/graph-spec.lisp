;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;; a factoriser avec spec de autowrite
;; pas possible pour l'instant car il faudrait
;; faire une auto-spec dans termauto?

(defclass graph-spec (termauto-spec)
  ((cwd :initform 2 :initarg :cwd :accessor cwd)))

(defgeneric fill-graph-spec (graph-spec &key automata tautomata))
(defmethod fill-graph-spec ((spec graph-spec) &key (automata nil) (tautomata nil))
  (loop for automaton in automata
	do (add-automaton automaton spec))
  (loop for tautomaton in tautomata
	do (add-tautomaton tautomaton spec))
  spec)

(defun make-graph-spec (cwd type-spec signature automata tautomata
			&rest keyword-args
			&key
			&allow-other-keys)
  (let ((spec (apply
	       #'make-instance
	       type-spec
	       :cwd cwd
	       :name (format nil "~A-~A" cwd (symbol-name type-spec))
	       :signature signature
	       :automaton (car automata)
	       :tautomaton (car tautomata)
	       keyword-args)))
    (fill-termauto-spec spec :automata automata :tautomata tautomata)))
