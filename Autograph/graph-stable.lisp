;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass stable-state (graph-state)
  ((ports :type ports :initarg :ports :reader ports)))

(defmethod state-size ((s stable-state))
  (ports-size (ports s)))

(defun stable-state-p (state)
  (typep state 'stable-state))

(defmethod print-object ((so stable-state) stream)
  (format stream "~A" (ports so)))

(defgeneric make-stable-state (ports)
  (:method ((ports ports))
    (make-instance 'stable-state :ports ports)))

(defmethod state-final-p ((so stable-state)) t)

(defmethod graph-ren-target (a b (so stable-state))
  (make-stable-state (ports-subst b a (ports so))))

(defmethod graph-reh-target ((port-mapping port-mapping)
			     (stable-state stable-state))
  (make-stable-state
   (apply-port-mapping port-mapping (ports stable-state))))

(defmethod graph-add-target (a b (so stable-state))
  (let ((ports (ports so)))
    (unless (and (ports-member a ports) (ports-member b ports))
      so)))

(defmethod graph-oplus-target ((so1 stable-state) (so2 stable-state))
  (make-stable-state (ports-union (ports so1) (ports so2))))

(defgeneric stable-applicable-annotation-p (annotated-symbol arg)
  (:method ((root annotated-symbol) (arg list))
  ;; the connected ports must be in different states
  ;; otherwise the failure would have been detected earlier
  (and (oplus-symbol-p root)
       (let ((ports
	       (loop
		 for state in arg
		 when (stable-state-p state)
		   collect (ports state))))
	 (and
	  ports
	  (let ((pairs (annotation root)))
	    (container-find-if
	     (lambda (edge)
	       (let ((a (first edge))
		     (b (second edge)))
		 (some
		  (lambda (pair)
		    (edge-connecting-ports-p a b (first pair) (second pair)))
		  (cartesian-product (list ports ports)))))
	     pairs)))))))

(defgeneric stable-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (make-stable-state (make-ports-from-port (port-of root))))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'stable-transitions-fun))
  (:method ((root annotated-symbol) (arg list)) ;; optimization using add-annotation
    ;;  (format *error-output* "stable-transitions-fun on annotated-symbol ~A~%" root)
    (if (add-symbol-p root) ;; the add has already been treated in oplus below
	(car arg)
      (let ((applicable-annotation (stable-applicable-annotation-p root arg)))
	(unless applicable-annotation
	  (stable-transitions-fun (symbol-of root) arg)))))
  (:method ((root (eql *empty-symbol*)) (arg (eql nil)))
    (cwd-transitions-fun root arg #'stable-transitions-fun)))

(defun stable-automaton (&key (cwd 0) (orientation :unoriented))
  (if (= 1 cwd)
      (universal-graph-automaton :cwd 1 :orientation orientation)
      (make-cwd-automaton
       (cwd-signature cwd :orientation orientation)
       (lambda (root states)
	 (let ((*neutral-state-final-p* t))
	   (stable-transitions-fun root states)))
       :name (cwd-automaton-name "STABLE" cwd))))

(defun table-stable-automaton (cwd &key (orientation :unoriented))
  (compile-automaton
   (stable-automaton :cwd cwd :orientation orientation)))

(defun subgraph-stable-automaton (m j &key (cwd 0) (orientation :unoriented))
  (assert (<= 1 j m))
   (nothing-to-xj (stable-automaton :cwd cwd :orientation orientation) m j))

(defun one-color-stable-automaton (k color &key (cwd 0) (orientation :unoriented))
  (assert (<= 1 color  k))
   (nothing-to-color (stable-automaton :cwd cwd :orientation orientation) k color))

(defun color-stable-automaton (k &key (cwd 0) (orientation :unoriented))
  (nothing-to-colors-constants (stable-automaton :cwd cwd :orientation orientation) k))

(defun table-subgraph-stable-automaton (m j cwd &key (orientation :unoriented))
  (compile-automaton
   (subgraph-stable-automaton m j :cwd cwd :orientation orientation)))

(defun table-color-stable-automaton (k cwd &key (orientation :unoriented))
  (compile-automaton
   (color-stable-automaton k :cwd cwd :orientation orientation)))

(defun table-one-color-stable-automaton (k color cwd &key (orientation :unoriented))
  (compile-automaton
   (one-color-stable-automaton k color :cwd cwd :orientation orientation)))

(defun test-stable (&optional (n 4))
  (let* ((f (stable-automaton :cwd n))
	 (stable (cwd-term-stable n))
	 (stablen (cwd-term-stable-cwd n))
	 (pn (cwd-term-pn n))
	 (cf (complement-automaton f))
	 (a (compile-automaton f))
	 (ca (complement-automaton a)))
    (assert (equality-automaton ca (complement-automaton f)))
    (when (= 1 n)
      (assert (equality-automaton ca (empty-automaton :cwd n))))
    (assert (recognized-p stable f))
    (assert (not (recognized-p stable cf)))
    (assert (recognized-p stablen f))
    (assert (not (recognized-p stablen cf)))
    (when (> n 1)
      (assert (recognized-p pn cf))
      (assert (not (recognized-p pn f))))
    (assert (recognized-p stable (stable-automaton :orientation t)))
    (assert (recognized-p stable (stable-automaton :orientation nil)))))
