;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)
;;; should be compiled instead of graph-coloring when a strict coloring
;;; is needed

(defclass vertex-colors-state (colors-state)
  ((vertex-colors :type list :accessor vertex-colors :initform '() :initarg :vertex-colors)))

(defmethod print-object ((colors-state vertex-colors-state) stream)
  (let ((color-fun (color-fun colors-state)))
    (print-port-fun color-fun stream)
    (print-vertex-colors (vertex-colors colors-state) stream)))

(defmethod compare-object ((co1 vertex-colors-state) (co2 vertex-colors-state))
  (and (call-next-method)
       (compare-object (color-fun co1) (color-fun co2))))

(defmethod state-final-p ((co vertex-colors-state)) t)

(defgeneric make-vertex-colors-state (color-fun vertex-colors))
(defmethod make-vertex-colors-state ((color-fun color-fun) (vertex-colors list))
  ;; vertex-colors should be sorted according to the car (vertex number)
  (make-instance 'vertex-colors-state :color-fun color-fun :vertex-colors vertex-colors))

(defmethod coloring-transitions-fun ((root annotated-symbol) (arg list))
  (let ((symbol (symbol-of root)))
    (if (symbol-constant-p root)
	(let ((color (color symbol))
	      (vertex-number (annotation root)))
	  (when (vertex-color-restrictions-p)
	    (let ((imposed-color (imposed-color vertex-number)))
	      (when (and imposed-color (/= imposed-color color))
		(return-from coloring-transitions-fun))))
	  (unless (and (= vertex-number 1) (/= color 1))
	    (make-vertex-colors-state
	     (make-port-color-fun 
	      color 
	      (port-of symbol))
	     (list (cons vertex-number color)))))
	(cond
	  ((add-symbol-p root) ;; the add has already been treated in oplus below
	   (car arg))
	  ((or (reh-symbol-p root) (ren-symbol-p root))
	   (coloring-transitions-fun (symbol-of root) arg))
	  (t
	   (let ((applicable-annotation
		   (coloring-applicable-annotation-p root arg)))
	     (unless applicable-annotation
	       (coloring-transitions-fun (symbol-of root) arg))))))))

(defmethod graph-oplus-target ((s1 vertex-colors-state) (s2 vertex-colors-state))
  (let* ((color-fun1 (color-fun s1))
	 (color-fun2 (color-fun s2))
	 (color-fun (merge-color-fun color-fun1 color-fun2))
	 (vertex-colors (sort (union (vertex-colors s1) (vertex-colors s2)) #'< :key #'car)))
    (when (correct-vertex-colors-p vertex-colors)
      (make-vertex-colors-state color-fun vertex-colors))))

(defmethod graph-ren-target (a b (vertex-colors-state vertex-colors-state))
  (make-vertex-colors-state (ren-color-fun a b (color-fun vertex-colors-state)) (vertex-colors vertex-colors-state)))

(defmethod graph-oplus-target-gen ((s1 vertex-colors-state) (s2 vertex-colors-state) (states list))
  (reduce #'graph-oplus-target (cons s1 (cons s2 states))))

(defgeneric count-strict-colorings (cwd-term k))

(defmethod count-strict-colorings ((cwd-term term) k)
  (compute-final-value (compute-coloring-annotations cwd-term) (count-coloring-transducer k)))
