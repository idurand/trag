;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun basic-subset-automaton (&key (cwd 0) (orientation :unoriented))
  (rename-object
   (vbits-fun-automaton
    2
    (lambda (vbits)
      (<= (aref vbits 0) (aref vbits 1)))
    :cwd cwd :orientation orientation)
   (format nil "~A-SUBSET(X1,X2)" cwd)))

(defun subset-automaton (m j1 j2 &key (cwd 0) (orientation :unoriented))
   (y1-y2-to-xj1-xj2
    (basic-subset-automaton :cwd cwd :orientation orientation)
    m j1 j2))

(defun table-basic-subset-automaton (cwd &key (orientation :unoriented))
  (compile-automaton
   (basic-subset-automaton :cwd cwd :orientation orientation)))

(defun table-subset-automaton (m j1 j2 cwd &key (orientation :unoriented))
  (compile-automaton
   (subset-automaton m j1 j2 :cwd cwd :orientation orientation)))

