;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass union-cycle-state (graph-state)
  ((d0-kports :type kports :initarg :d0-kports :reader d0-kports)
   (d1-kports :type kports :initarg :d1-kports :reader d1-kports)
   (d2-ports :type ports :initarg :d2-ports :reader d2-ports)))

(defmethod print-object ((co union-cycle-state) stream)
  (format stream "<~A,~A,~A>" (d0-kports co) (d1-kports co) (d2-ports co)))

(defmethod ports-of ((co union-cycle-state))
  (ports-union
   (ports-of (d0-kports co))
   (ports-union (ports-of (d1-kports co)) (ports-of (d2-ports co)))))

(defmethod compare-object ((co1 union-cycle-state) (co2 union-cycle-state))
  (and (compare-object (d0-kports co1) (d0-kports co2))
       (compare-object (d1-kports co1) (d1-kports co2))
       (compare-object (d2-ports co1) (d2-ports co2))))

(defgeneric union-cycle-state-conditions (d0-kports d1-kports d2-ports)
  (:method ((d0-kports kports) (d1-kports kports) (d2-ports ports))
    (and (ports-empty-p (ports-intersection d2-ports (ports-of d1-kports)))
	 (ports-empty-p (ports-intersection d2-ports (ports-of d0-kports)))
	 (ports-empty-p (ports-intersection (ports-of d1-kports) 
					    (ports-of d0-kports))))))

(defgeneric make-union-cycle-state (d0-kports d1-kports d2-ports)
  (:method ((d0-kports kports) (d1-kports kports) (d2-ports ports))
    (make-instance 'union-cycle-state
		   :d0-kports d0-kports
		   :d1-kports d1-kports
		   :d2-ports d2-ports)))

(defgeneric conditional-make-union-cycle-state (d0-kports d1-kports d2-ports)
  (:method ((d0-kports kports) (d1-kports kports) (d2-ports ports))
    (when (union-cycle-state-conditions d0-kports d1-kports d2-ports)
      (make-union-cycle-state d0-kports d1-kports d2-ports))))

;; is a single node a cycle??
(defmethod state-final-p ((co union-cycle-state))
  (and (container-empty-p (d0-kports co))
       (container-empty-p (d1-kports co))))

(defmethod graph-ren-target (a b (co union-cycle-state))
  (let ((new-d0 (ports-subst b a (d0-kports co))))
    (unless (> (kports-max-multiplicity new-d0) 2)
      (let ((new-d1 (ports-subst b a (d1-kports co))))
	(unless (> (kports-max-multiplicity new-d1) 2)
	  (make-union-cycle-state
	   new-d0 new-d1 (ports-subst b a (d2-ports co))))))))

(defmethod graph-oplus-target ((co1 union-cycle-state) (co2 union-cycle-state))
  (let ((d0-kports (ports-union (d0-kports co1) (d0-kports co2)))
	(d1-kports (ports-union (d1-kports co1) (d1-kports co2)))
	(d2-ports (ports-union (d2-ports co1) (d2-ports co2))))
    (conditional-make-union-cycle-state d0-kports d1-kports d2-ports)))

(defgeneric other-error-cases (a b d0-kports d1-2ports)
  (:method (a b (d0-kports kports) (d1-kports kports))
    (let ((a0 (container-member a d0-kports))
	  (a1 (container-member a d1-kports))
	  (b0 (container-member b d0-kports))
	  (b1 (container-member b d1-kports)))
      (or (and a0 b1 (= 2 (attribute-of a0)))
	  (and a1
	       (or (and (= 1 (attribute-of a1))
			(or (and b0 (= 2 (attribute-of b0)))
			    (and b1 (= 2 (attribute-of b1)))))
		   (and (= 2 (attribute-of a1))
			(or (and b0 (= 2 (attribute-of b0)))
			    b1))))))))

(defmethod graph-add-target (a b (co union-cycle-state))
  ;;  (format *trace-output* "graph-add-target union-cycle-state ~A ~A ~A~%" a b co)
  (let ((ports (ports-of co)))
    (when (or (not (ports-member a ports))
	      (not (ports-member b ports)))
      (return-from graph-add-target co))
    (let ((d0-kports (d0-kports co))
	  (d1-kports (d1-kports co))
	  (d2-ports (d2-ports co)))
      (unless
	  (or
	   ;; vertex of degree 3!
	   (container-member a d2-ports)
	   (container-member b d2-ports)
	   (other-error-cases a b d0-kports d1-kports))
	(let ((a0 (container-member a d0-kports))
	      (a1 (container-member a d1-kports))
	      (b0 (container-member b d0-kports))
	      (b1 (container-member b d1-kports))
	      (new-d0 (container-copy d0-kports))
	      (new-d1 (container-copy d1-kports))
	      (new-d2 (container-copy d2-ports)))
	  ;; transfer ports that have not anymore degree 0
	  (when a0 (container-delete a0 new-d0))
	  (when b0 (container-delete b0 new-d0))
	  (if a0
	      (if (or (and b0 (= 2 (attribute-of b0)))
		      (and b1 (= 2 (attribute-of b1))))
		  (container-nadjoin a new-d2)
		  (container-nadjoin a0 new-d1))
	      ;; a1
	      (container-nadjoin a new-d2))
	  (if b0
	      (if (or (and a0 (= 2 (attribute-of a0)))
		      (and a1 (= 2 (attribute-of a1))))
		  (container-nadjoin b new-d2)
		  (container-nadjoin b0 new-d1))
	      ;; b1
	      (container-nadjoin b new-d2))
	  ;; transfer ports that have not anymore degree 1
	  (when a1
	    (container-delete a1 new-d1)
	    (container-nadjoin a new-d2))
	  (when b1
	    (container-delete b1 new-d1)
	    (container-nadjoin b new-d2))
	  (conditional-make-union-cycle-state new-d0 new-d1 new-d2))))))

(defgeneric union-cycle-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (make-union-cycle-state
     (make-kports (list (port-of root)))
     (make-kports '())
     (make-empty-ports)))

  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'union-cycle-transitions-fun))

  (:method  ((root (eql *empty-symbol*)) (arg (eql nil)))
    (cwd-transitions-fun root arg #'union-cycle-transitions-fun)))

(defun union-cycle-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-signature cwd)
   (lambda (root states)
     (let ((*neutral-state-final-p* nil))
       (union-cycle-transitions-fun  root states)))
   :name (cwd-automaton-name "UNION-CYCLE" cwd)
   :precondition-automaton (non-redundant-automaton nil :cwd cwd)))

(defun table-union-cycle-automaton (cwd)
  (compile-automaton
   (union-cycle-automaton cwd)))

(defun subgraph-union-cycle-automaton (m j &optional (cwd 0))
  (assert (<= 1 j m))
   (nothing-to-xj (union-cycle-automaton cwd) m j))

(defun color-union-cycle-automaton (k color &optional (cwd 0))
  "for non redundant terms only"
  (assert (<= 1 color k))
  (nothing-to-color (union-cycle-automaton cwd) k color))

(defun table-subgraph-union-cycle-automaton (m j cwd)
  (compile-automaton (subgraph-union-cycle-automaton m j cwd)))

(defun table-color-union-cycle-automaton (k color cwd)
  (compile-automaton (color-union-cycle-automaton k color cwd)))

(defun test-graph-union-cycle ()
  ;; to complete
  (let ((a (union-cycle-automaton))
	(tok (cwd-term-oplus (cwd-term-kn 3) (cwd-term-cycle 4)))
	(tnok1 (cwd-term-pn 3))
	(tnok2 (input-cwd-term "add_a_b(oplus(a, oplus(b, oplus(b,b))))")))
    (assert (recognized-p tok a))
    (assert (not (recognized-p tnok1 a)))
    (assert (not (recognized-p tnok2 a)))))
