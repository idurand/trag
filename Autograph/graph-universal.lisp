;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun universal-graph-automaton (&key (cwd 0) (orientation :unoriented))
  (rename-object
   (complement-automaton (empty-automaton :cwd cwd :orientation orientation))
   (cwd-automaton-name "UNIVERSAL" cwd)))

(defun table-universal-graph-automaton (cwd &key (orientation :unoriented))
  (compile-automaton
   (universal-graph-automaton :cwd cwd :orientation orientation)))

(defun test-universal ()
  (assert (automaton-emptiness (complement-automaton (table-universal-graph-automaton 2))))
  (assert (equality-automaton (complement-automaton (empty-automaton :cwd 2)) (universal-graph-automaton :cwd 2))))


(defgeneric universal-transitions-explicit-fun (root states))
(defmethod universal-transitions-explicit-fun ((root cwd-constant-symbol) (arg (eql nil)))
  *ok-ok-state*)
(defmethod universal-transitions-explicit-fun ((root (eql *empty-symbol*)) (arg null))
  *neutral-state*)

(defmethod universal-transitions-explicit-fun ((root abstract-symbol) (arg list))
  (cwd-transitions-fun root arg #'universal-transitions-explicit-fun))

(defun universal-graph-automaton-explicit (&key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
    (add-empty-symbols
      (cwd-signature cwd :orientation orientation))
    #'universal-transitions-explicit-fun
    :name (cwd-automaton-name "UNIVERSAL" cwd)))
