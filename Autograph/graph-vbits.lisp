;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defvar *vbits-fun*)

(defgeneric vbits-transitions-fun (symbol arg))

(defmethod vbits-transitions-fun ((root vbits-constant-symbol) (arg (eql nil)))
  (let ((vbits (vbits root)))
    (when (funcall *vbits-fun* vbits)
      *ok-ok-state*)))

(defmethod vbits-transitions-fun
    ((root abstract-symbol) (arg list))
  (cwd-transitions-fun root arg #'vbits-transitions-fun))

(defun vbits-fun-automaton (m fun &key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-vbits-signature cwd m :orientation orientation)
   (lambda (root states)
     (let ((*vbits-fun* fun))
       (vbits-transitions-fun root states)))
   :name
   (cwd-automaton-name (format nil "VBITS-FUN-X1-~:[~;...-~]X~A" (> m 2) m)  cwd)))
