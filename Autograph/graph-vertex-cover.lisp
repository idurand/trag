;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;; Vertex-Cover (X1) = Stable(V-X1)

(defun vertex-cover-automaton (&key (cwd 0) (orientation :unoriented))
  (rename-object
   (x1-to-cx1
    (subgraph-stable-automaton 1 1 :cwd cwd :orientation orientation))
   (format nil "~A-VERTEX-COVER-X1" cwd)))

(defun table-vertex-cover-automaton (cwd &key (orientation :unoriented))
  (compile-automaton
   (vertex-cover-automaton :cwd cwd :orientation orientation)))

(defun k-vertex-cover-automaton (k &key (cwd 0) (orientation :unoriented))
  (rename-object
   (vbits-projection
    (intersection-automaton-compatible
     (subgraph-cardinality-automaton k 1 1 :cwd cwd :orientation orientation)
     (vertex-cover-automaton :cwd cwd :orientation orientation)))
   (format nil "~A-EXISTS-~A-VERTEX" cwd k)))

(defun table-k-vertex-cover-automaton (k cwd  &key (orientation :unoriented))
  (compile-automaton
   (k-vertex-cover-automaton k :cwd cwd :orientation orientation)))
