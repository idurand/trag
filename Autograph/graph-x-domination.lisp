;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun basic-x-domination-automaton (&key (cwd 0) (orientation :unoriented))
  (rename-object
   (xi-eq-cxj
    (basic-domination-automaton :cwd cwd :orientation orientation)
    2 1 2)
   (cwd-automaton-name "X1-DOMINATION" cwd)))
  
(defun table-basic-x-domination-automaton (cwd &key (orientation :unoriented))
  "X1 dominates C(X1)"
  (compile-automaton
   (basic-x-domination-automaton :cwd cwd :orientation orientation)))

(defun x-domination-automaton (m j1 &key (cwd 0) (orientation :unoriented))
  (rename-object
   (x1-to-xj
    (basic-x-domination-automaton :cwd cwd :orientation orientation) m j1)
   (format nil "~A-X-DOMINATION-X~A-~A" cwd j1 m)))

(defun table-x-domination-automaton (cwd m j1 &key (orientation :unoriented))
  (compile-automaton
   (x-domination-automaton m j1 :cwd cwd :orientation orientation)))

(defun x-domination-transducer (afun &key (orientation :unoriented))
  (vbits-projection
   (attribute-automaton
    (x-domination-automaton 2 1 :orientation orientation) afun)))

(defun basic-inf-k-x-domination-automaton
    (count &key (cwd 0) (orientation :unoriented))
  (intersection-automaton
   (subgraph-cardinality-inf-automaton count 1 1 :cwd cwd :orientation orientation)
   (basic-x-domination-automaton :cwd cwd :orientation orientation)))
			   
(defun inf-k-x-domination-automaton (count m j1 &key (cwd 0) (orientation :unoriented))
  (rename-object
   (x1-to-xj
    (basic-inf-k-x-domination-automaton count :cwd cwd :orientation orientation) m j1)
   (format nil "~A-X-INF-~A-DOMINATION-X~A-~A" cwd count j1 m)))

(defun inf-k-x-domination-transducer (count afun &key (cwd 0) (orientation :unoriented))
  (vbits-projection
   (attribute-automaton (basic-inf-k-x-domination-automaton count :cwd cwd :orientation orientation) afun)))

(defun inf-k-x-domination-enumerator (term count afun &key (orientation :unoriented))
  (final-value-enumerator
   term
   (inf-k-x-domination-transducer count afun :orientation orientation)))

(defun terms-with-inf-k-x-domination (term k)
  (termauto::compute-assigned-terms term (basic-inf-k-x-domination-automaton k)))

(defun graphs-with-inf-k-dominating-configurations (term count)
  (mapcar #'cwd-term-to-graph (terms-with-inf-k-x-domination term count)))

(defun graphs-with-inf-n/2-dominating-configurations (term k)
  (mapcar #'cwd-term-to-graph (terms-with-inf-k-x-domination term k)))
