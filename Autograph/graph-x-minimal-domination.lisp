;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun x-minimal-domination-automaton (&key (cwd 0))
  (rename-object
   (xi-eq-cxj
    (basic-minimal-domination-automaton :cwd cwd)
    2 1 2)
   (cwd-automaton-name "X1-MINIMAL-DOMINATION" cwd)))
  
(defun table-x-minimal-domination-automaton (cwd)
  "X1 dominates C(X1)"
  (compile-automaton (x-minimal-domination-automaton :cwd cwd)))

(defun x-minimal-domination-transducer (afun)
  (vbits-projection
   (attribute-automaton (x-minimal-domination-automaton) afun)))

(defun x-minimal-domination-enumerator (term afun)
  (final-value-enumerator
   term
   (x-minimal-domination-transducer afun)))

(defun x-minimal-domination-sets-enumerator (term)
  (x-minimal-domination-enumerator term *assignment-afun*))

(defun nodes-in-x (assigned-term)
  (mapcar #'term-eti
	  (remove-if
	   (lambda (leaf) (zerop (aref (vbits (root leaf)) 0)))
	   (leaves-of-term assigned-term))))

(defun terms-with-minimal-configurations (term)
  (termauto::compute-assigned-terms term (x-minimal-domination-automaton)))

(defun graphs-with-minimal-configurations (term)
  (mapcar #'cwd-term-to-graph (terms-with-minimal-configurations term)))

(defun count-minimal-configurations (term &key (enum nil))
  (compute-value term (x-minimal-domination-transducer *count-afun*) :enum enum))
  
(defun minimal-configurations (term)
  (let ((mc
	  (mapcar (lambda (l) (sort l #'<))
		  (mapcar #'nodes-in-x
			  (terms-with-minimal-configurations term)))))
    (setq mc (sort mc (lambda (l1 l2) (< (length l1) (length l2)))))
    (values mc (length mc))))

