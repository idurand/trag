;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass homogeneous-orientation-state (graph-state)
  ((orientation :reader orientation :initarg :orientation)))

(defmethod print-object ((so homogeneous-orientation-state) stream)
  (format stream "O:~A" (orientation so)))

(defmethod compare-object
    ((s1 homogeneous-orientation-state) (s2 homogeneous-orientation-state))
  (eq (orientation s1) (orientation s2)))
									 
(defun make-homogeneous-orientation-state (orientation)
  (make-instance 'homogeneous-orientation-state :orientation orientation))

(defmethod state-final-p ((state homogeneous-orientation-state)) t)

(defmethod graph-ren-target (a b (state homogeneous-orientation-state))
  (declare (ignore a b))
  state)

(defmethod graph-reh-target ((port-mapping port-mapping)
			     (state homogeneous-orientation-state))
  (declare (ignore port-mapping))
  state)

(defmethod graph-oadd-target (a b (state homogeneous-orientation-state))
  (unless (null (orientation state))
    (make-homogeneous-orientation-state t)))

(defmethod graph-add-target (a b (state homogeneous-orientation-state))
  (unless (eq t (orientation state))
    (make-homogeneous-orientation-state nil)))

(defmethod graph-oplus-target ((state1 homogeneous-orientation-state) (state2 homogeneous-orientation-state))
  (when (eq (orientation state2) (orientation state2))
    state1))

(defgeneric homogeneous-orientation-transitions-fun (root arg)
  (:method ((root cwd-constant-symbol) (arg null))
    (make-homogeneous-orientation-state *orientation*))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'homogeneous-orientation-transitions-fun))
  (:method ((root (eql *empty-symbol*)) (arg null))
    (cwd-transitions-fun root arg #'homogeneous-orientation-transitions-fun)))

(defun homogeneous-orientation-automaton (&key (cwd 0) (orientation :unoriented))
  (make-cwd-automaton
   (cwd-signature cwd :orientation orientation)
   (lambda (root states)
     (let ((*orientation* orientation))
       (homogeneous-orientation-transitions-fun root states)))
   :name (cwd-automaton-name "HOMOGENEOUS-ORIENTATION" cwd)))

(defun table-homogeneous-orientation-automaton (cwd &key (orientation :unoriented))
  (compile-automaton
   (homogeneous-orientation-automaton :cwd cwd :orientation orientation)))

(defun test-homogeneous-orientation ()
  (let* ((uf (homogeneous-orientation-automaton :cwd 3))
	 (of (homogeneous-orientation-automaton  :cwd 3 :orientation t))
	 (f (homogeneous-orientation-automaton  :cwd 3 :orientation nil))
	 (pn (cwd-term-pn 4))
	 (opn (cwd-term-pn 4 :oriented t))
	 (cf (complement-automaton f))
	 (a (compile-automaton f))
	 (ca (complement-automaton a)))
    (assert (equality-automaton ca cf))
    (assert (recognized-p pn uf))
    (assert (recognized-p pn f))
    (assert (not (recognized-p pn of)))
    (assert (recognized-p opn of))
    (assert (recognized-p opn uf))))
    
