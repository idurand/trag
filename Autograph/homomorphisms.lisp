;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric cwd-nothing-to-x1 (abstract-automaton)
  (:method ((a abstract-automaton))
    (let ((parts (decompose-string (name a) #\-))
	  (cwd (clique-width::clique-width (signature a))))
      (when (digit-char-p (aref (car parts) 0))
	(setq parts (cdr parts)))
      (rename-object
       (nothing-to-x1 a)
       (autograph::cwd-automaton-name
	(format nil "~A-X1" (car parts)) cwd)))))

(defgeneric cwd-nothing-to-xj (abstract-automaton m j)
  (:method ((a abstract-automaton) (m integer) (j integer))
    (let ((parts (decompose-string (name a) #\-))
	  (cwd (clique-width::clique-width (signature a))))
      (when (digit-char-p (aref (car parts) 0))
	(setq parts (cdr parts)))
      (rename-object
       (nothing-to-xj a m j)
       (autograph::cwd-automaton-name
      (format nil "~A-X~A-~A" (car parts) j m) cwd)))))

(defgeneric cwd-xj-to-cxj (abstract-automaton m j)
  (:method ((a abstract-automaton) (m integer) (j integer))
    (rename-object
     (xj-to-cxj a m j)
     (compose-string 
      (subst (format nil "~~X~A" j) (format nil "X~A" j) (decompose-string (name a) #\-) :test #'equal)
      #\-))))

(defgeneric cwd-y1-y2-to-xj1-xj2 (abstract-automaton m j1 j2)
  (:method ((a abstract-automaton) (m integer) (j1 integer) (j2 integer))
    (let ((parts (decompose-string (name a) #\-))
	  (cwd (clique-width::clique-width (signature a))))
      (when (digit-char-p (aref (car parts) 0))
	(setq parts (cdr parts)))
      (rename-object
       (y1-y2-to-xj1-xj2 a m j1 j2)
       (autograph::cwd-automaton-name
	(format nil "~A-X~A-X~A-~A" (car parts) j1 j2 m) cwd)))))

(defgeneric cwd-constants-color-projection (abstract-automaton)
  (:method ((a abstract-automaton))
    (let ((parts (decompose-string (name a) #\-)))
      (rename-object
       (constants-color-projection a)
       (reduce #'compose-name (nbutlast parts))))))
	

