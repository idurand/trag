;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun init-autograph ()
  (init-directories)
  (setq input-output::*data-relative-directory* "Data/")
  (init-termauto)
  (init-graph)
  (rename-object (make-autograph-spec (make-signature nil)) "empty")
  (set-current-spec (rename-object
		     (make-autograph-spec (make-signature nil))
		     "current"))
  (init-parser))
