;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;(asdf:operate 'asdf:load-op :autograph)
(in-package :autograph)

(defparameter *amcgee* (amcgee))
(defparameter *f* (color-k-acyclic-colorability-automaton 3))

;;(with-time (defparameter *res* (compute-target *amcgee* *f*)))
;; (with-time (enum-recognized-p *amcgee* *f*))
;; (with-time (defparameter *res* (enum-compute-target *amcgee* *f*)))

;; 2014-03-05
;; (defparameter *amcgee* (amcgee))
;; (defparameter *f* (color-k-acyclic-colorability-automaton 3))
;; AUTOGRAPH> (with-time 
;; 	       (defparameter *target*
;; 		 (compute-target-counting-runs
;; 		  *amcgee* *f*)))
;;  in 26min 40.144sec
;; *TARGET*
;; AUTOGRAPH> (target-attribute *target*)
;; 57024
;; T
