;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

;;; for multi-sets of connected-components

(defclass multi-components (cwd-mixin ordered-mixin multi-max-container) ()
  (:documentation "to represent a multi-set of connected-components"))

(defmethod container-order-fun ((container multi-components)) #'strictly-ordered-p)

(defmethod ports-of ((multi-components multi-components))
  (let ((ports (make-empty-ports)))
    (container-map
     (lambda (cc)
       (ports-nunion ports (ports-of cc)))
     multi-components)
    ports))

(defgeneric make-multi-components (components))
(defmethod make-multi-components ((components list))
  (make-multi-max-container-generic
   (mapcar (lambda (cc) (if (attributed-p cc)
			    cc
			    (make-object-multi cc)))
	   components)
   'multi-components #'equal 2))

(defgeneric multi-component-copy (multi-component))
(defmethod multi-component-copy ((multi-component attributed-object))
  (make-object-multi (object-of multi-component)
		     (attribute-of multi-component)))
  
(defgeneric multi-components-copy (multi-components))
(defmethod multi-components-copy ((multi-components multi-components))
  (container-mapcar-to-container
   #'multi-component-copy
   multi-components))

(defgeneric multi-components-ren (a b multi-components))
(defmethod multi-components-ren ((a integer) (b integer) (multi-components multi-components))
  (container-mapcar-to-container
   (lambda (cc) (make-object-multi (component-ren a b (object-of cc)) (attribute-of cc)))
   multi-components))

(defgeneric multi-components-reh (port-mapping  multi-components))
(defmethod multi-components-reh ((port-mapping port-mapping) (multi-components multi-components))
  (container-mapcar
   (lambda (cc)
     (apply-port-mapping port-mapping cc))
   multi-components))

(defgeneric find-multi-component-with (a multi-components))
(defmethod find-multi-component-with (a (multi-components multi-components))
  (container-find-if
   (lambda (c)
     (ports-member a (ports-of c)))
   multi-components))

(defmethod sharp (a (multi-components multi-components))
  (cond
    ((not (ports-member a (ports-of multi-components)))
     0)
    ((container-find-if
      (lambda (cc)
	(ports-relation-member (list a a) (all-paths (object-of cc))))
      multi-components)
     2)
    (t
     (let ((count 0))
       (container-map (lambda (cc)
			(when (ports-member a (ports-of cc)) 
				     (incf count (attribute-of cc))))
		      multi-components)
       count))))

