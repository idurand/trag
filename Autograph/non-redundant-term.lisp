;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric cwd-term-non-redundant-p (cwd-term)
  (:documentation "T if CWD-TERM is non redundant")
  (:method ((cwd-term term))
    (not (recognized-p cwd-term (redundant-automaton (oriented-p cwd-term))))))

(defgeneric cwd-term-redundant-p (cwd-term)
  (:method ((term term))
    (recognized-p term (redundant-automaton (oriented-p term)))))

(defgeneric annotated-to-non-redundant (annotated-term)
  (:documentation "non redundant version of ANNOTATED-TERM")
  (:method ((term term))
    (let* ((arg (mapcar #'annotated-to-non-redundant (arg term)))
	   (root (root term))
	   (sym (symbol-of root)))
      (if (and (add-symbol-p sym) (pairs-member (symbol-ports sym) (annotation root)))
	  (car arg)
	  (set-term-eti (build-term sym arg) (term-eti term))))))

(defgeneric cwd-term-to-non-redundant-term (cwd-term)
  (:documentation "non redundant version of CWD-TERM")
  (:method ((cwd-term term))
    (annotated-to-non-redundant (compute-add-annotation cwd-term))))
