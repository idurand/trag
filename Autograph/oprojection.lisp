;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric oprojection-f (o))
(defgeneric oprojection-if (o))
(defgeneric oprojection-h ())
(defgeneric oprojection-ih ())

(defgeneric oprojection (o))

(defmethod oprojection-f ((s abstract-symbol))
  (list s))

(defmethod oprojection-f ((s non-constant-mixin))
  (list
   (if (add-symbol-p s)
       (apply #'add-symbol (sorted-pair-from-pair (symbol-ports s)))
       s)))

(defmethod oprojection-f ((s annotated-symbol))
  (warn "oprojection-f on annotated-symbol ~A" s))

(defmethod oprojection-if ((s abstract-symbol))
  (list s))

(defmethod oprojection-if ((s non-constant-mixin))
  (if (add-symbol-p s)
      (let ((ports (symbol-ports s)))
	(list
	 (apply #'add-symbol ports)
	 (apply #'add-symbol (reverse ports))))
      (list s)))

(defmethod oprojection-ih ()
  (lambda (s) (oprojection-if s)))

(defmethod oprojection-h ()
  (lambda (s) (oprojection-f s)))

(defmethod oprojection ((signed-object signed-object))
  (homomorphism
   signed-object (oprojection-h)
   (oprojection-ih)
   nil))

(defmethod oprojection ((s non-constant-mixin))
  (list
   (if (add-symbol-p s)
       (let ((ports (symbol-ports s)))
	 (apply #'add-symbol (sorted-pair-from-pair ports)))
       s)))

(defun pair-random-ordered (pair)
  (let ((n (random 2)))
    (if (zerop n)
	pair
	(reverse pair))))

(defgeneric symbol-randomize-orientation (abstract-symbol))

(defmethod symbol-randomize-orientation ((s abstract-symbol))
  s)

(defmethod symbol-randomize-orientation ((s non-constant-mixin))
  (if (add-symbol-p s)
      (let ((ports (symbol-ports s)))
	(apply #'add-symbol (pair-random-ordered ports)))
      s))

(defgeneric graph-term-randomize-orientation (term))
(defmethod graph-term-randomize-orientation ((term term))
  (term-subst-symbols term #'symbol-randomize-orientation))
