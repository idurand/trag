;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun basic-arc-automaton (&key (cwd 0))
  "automaton for ARC(X1,X2) with m = 2"
  (basic-arc-or-edge-automaton :cwd cwd :orientation t))

(defun arc-automaton (m j1 j2 &key (cwd 0))
  "automaton for ARC(Xj1, Xj2) with m = M)"
  (y1-y2-to-xj1-xj2 (basic-arc-automaton :cwd cwd) m j1 j2))

(defun table-basic-arc-automaton (cwd)
  "automaton for an arc between singletons X1 and X2"
  (compile-automaton (basic-arc-automaton :cwd cwd)))

(defun table-arc-automaton (m j1 j2 cwd)
  "table automaton for an arc between singletons X1 and X2"
  (compile-automaton (arc-automaton m j1 j2 :cwd cwd)))
