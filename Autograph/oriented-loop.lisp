;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass loop-state (graph-state)
  ((loop-isolated-ports :reader loop-isolated-ports :initarg :loop-isolated-ports
		   :type ports)
   (loop-arcs :reader loop-arcs :initarg :loop-arcs :type ports-relation)))

(defmethod compare-object ((s1 loop-state) (s2 loop-state))
  (and (compare-object (loop-isolated-ports s1) (loop-isolated-ports s2))
       (compare-object (loop-arcs s1) (loop-arcs s2))))

(defmethod print-object ((s loop-state) stream)
  (format stream "[~A|~A]" (loop-isolated-ports s) (loop-arcs s)))

(defun make-loop-state (isolated-ports arcs)
  (make-instance
   'loop-state :loop-isolated-ports isolated-ports :loop-arcs arcs))

(defmethod ports-of ((s loop-state))
  (ports-union (loop-isolated-ports s) (ports-of (loop-arcs s))))

(defmethod graph-oplus-target ((s1 loop-state) (s2 loop-state))
  (make-loop-state
   (ports-union (loop-isolated-ports s1) (loop-isolated-ports s2)) 
   (ports-relation-union (loop-arcs s1) (loop-arcs s2))))

(defmethod graph-add-target (a b (s loop-state))
  (let ((arcs (loop-arcs s))
	(connected-ports (make-ports (list a b)))
	(ports (ports-of s)))
    (if (ports-relation-member (list b a) arcs)
	*ok-ok-state*
	(if (ports-subset-p connected-ports ports)
	    (make-loop-state
	     (container-difference (loop-isolated-ports s) connected-ports)
	     (ports-relation-adjoin (list a b) arcs))
	    s))))

(defmethod graph-ren-target (a b (s loop-state))
  (make-loop-state
   (ports-subst b a (loop-isolated-ports s))
   (ports-relation-subst b a (loop-arcs s))))

(defgeneric loop-transitions-fun (root arg)
  (:method ((root (eql *empty-symbol*)) (arg (eql nil)))
    (cwd-transitions-fun root arg #'loop-transitions-fun))
  (:method ((root cwd-constant-symbol) (arg (eql nil)))
    (make-loop-state
     (make-ports-from-port (port-of root))
     (make-empty-ports-relation t)))
  (:method ((root abstract-symbol) (arg list))
    (cwd-transitions-fun root arg #'loop-transitions-fun)))

(defun oriented-loop-automaton (&key (cwd 0))
  "to detect a loop in an oriented graph"
  (make-cwd-automaton
   (cwd-signature cwd :orientation t)
   (lambda (root states)
     (loop-transitions-fun root states))
   :name (cwd-automaton-name "MSO2-HAS-LOOP" cwd)))

(defun test-oriented-loop ()
  (let ((tok (input-cwd-term "ren_b_a(add_a->b(add_b->c(oplus(b,add_b->a(oplus(a,oplus(b,c)))))))"))
	(tnok (input-cwd-term "ren_b_a(add_b->c(add_b->c(oplus(b,add_b->a(oplus(a,oplus(b,c)))))))")))
    (assert (recognized-p tok (oriented-loop-automaton)))
    (assert (not (recognized-p tnok (oriented-loop-automaton))))))
	
