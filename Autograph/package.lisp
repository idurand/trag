;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :autograph
  (:use :common-lisp
   :general :input-output
   :vbits :object :color :container
   :symbols :terms :clique-width :state
   :termauto :enum :graph :decomp :tptp-syntax)
  (:export
   #:compile-cwd-automaton
   #:cardinality-automaton
   #:subgraph-cardinality-automaton
   #:color-cardinality-automaton
   #:counting-automaton
   #:stable-automaton
   #:subgraph-stable-automaton
   #:color-stable-automaton
   #:one-color-stable-automaton
   #:arc-automaton
   #:basic-arc-automaton
   #:table-basic-arc-automaton
   #:arc-automaton
   #:basic-equal-automaton
   #:table-basic-equal-automaton
   #:equal-automaton
   #:table-equal-automaton
   #:basic-edge-automaton
   #:basic-path-automaton
   #:table-basic-edge-automaton
   #:singleton-automaton
   #:edge-automaton
   #:subset-automaton
   #:basic-subset-automaton
   #:table-edge-automaton
   #:degreinmax-automaton
   #:table-degreinmax-automaton
   #:clique-automaton
   #:subgraph-clique-automaton
   #:color-clique-automaton
   #:table-clique-automaton
   #:table-subgraph-clique-automaton
   #:table-color-clique-automaton
   #:kcolorability-automaton
   #:coloring-automaton
   #:acyclic-coloring-automaton
   #:k-acyclic-colorability-automaton
   #:table-kcolorability-automaton
   #:forest-automaton
   #:nr-forest-automaton
   #:subgraph-forest-automaton
   #:connectedness-automaton
   #:unoriented-hamiltonian-automaton
   #:oriented-hamiltonian-automaton
   #:subgraph-connectedness-automaton
   #:basic-2oarc-automaton
   #:2oarc-automaton
   #:autograph-spec
   #:set-current-graph
   #:init-autograph
   #:change-automaton-cwd
   #:remove-graph
   #:graph-graph

   #:single-coloring-grid-edges
   #:single-coloring-grid-graph
   #:single-coloring-grid-term-sequential
   #:single-coloring-grid-term-parallel

   #:cwd-automaton-emptiness
   #:cwd-non-emptiness-witness
   #:cwd-y1-y2-to-xj1-xj2
   #:cwd-xj-to-cxj
   #:cwd-nothing-to-x1
   #:cwd-nothing-to-xj
   #:cwd-constant-color-projection

   #:clique-width
   #:circuit-automaton
   #:has-circuit-automaton
   
   ))
