;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass rel-state (graph-state)
  ((ports-relation :type ports-relation :initarg :ports-relation :reader ports-relation))
  (:documentation "state to store arcs connected by par -*- "))

(defmethod oriented-p ((rel-state rel-state))
  (oriented-p (ports-relation rel-state)))

(defmethod compare-object ((o1 rel-state) (o2 rel-state))
  (compare-object (ports-relation o1) (ports-relation o2)))

(defgeneric make-rel-state (ports-relation)
  (:method ((ports-relation ports-relation))
    (make-instance 'rel-state :ports-relation ports-relation)))

(defun make-empty-rel-state (oriented)
  (make-rel-state (make-empty-ports-relation oriented)))

(defmethod strictly-ordered-p ((po1 rel-state) (po2 rel-state))
  (strictly-ordered-p (ports-relation po1) (ports-relation po2)))

(defmethod print-object ((rel-state rel-state) stream)
  (format stream "[~A]" (ports-relation rel-state)))

(defmethod ports-of ((rel-state rel-state))
  (ports-of (ports-relation rel-state)))

(defgeneric rel-state-empty-p (rel-state)
  (:method ((rel-state rel-state))
    (ports-relation-empty-p (ports-relation rel-state))))

(defgeneric rel-state-subsetp (rel-state1 rel-state2)
  (:method ((p1 rel-state) (p2 rel-state))
    (ports-relation-subset-p (ports-relation p1) (ports-relation p2))))

(defgeneric rel-state-member (pair rel-state)
  (:method ((pair list) (p rel-state))
    (unless (oriented-p p)
      (assert (opair-p pair)))
    (ports-relation-member pair (ports-relation p))))

(defgeneric rel-state-union (rel-state1 rel-state2)
  (:method ((p1 rel-state) (p2 rel-state))
    (make-rel-state
     (ports-relation-union (ports-relation p1) (ports-relation p2)))))

(defgeneric rel-state-subst (a b rel-state)
  (:method (a b (p rel-state))
    (make-rel-state
     (ports-relation-subst a b (ports-relation p)))))

(defgeneric rel-state-difference (rel-state1 rel-state2)
  (:method ((p1 rel-state) (p2 rel-state))
    (make-rel-state
     (ports-relation-difference (ports-relation p1) (ports-relation p2)))))

(defgeneric rel-state-remove (pair rel-state)
  (:method ((pair list) (p rel-state))
    (make-rel-state (ports-relation-remove pair (ports-relation p)))))

(defmethod graph-oplus-target ((p1 rel-state) (p2 ok-state)) p2)

(defmethod graph-oplus-target ((p1 rel-state) (p2 rel-state))
  (rel-state-union p1 p2))

(defmethod graph-ren-target (a b (s rel-state))
  (make-rel-state
   (ports-relation-subst b a (ports-relation s))))

(defmethod graph-add-target (a b (rel-state rel-state))
  (let ((oriented (oriented-p rel-state))
	(ports (ports-of rel-state)))
    (unless oriented
      (assert (<= a b)))
    (if (and (ports-member a ports)
	     (ports-member b ports))
	(make-rel-state
	 (new-ports-relation-for-add a b (ports-relation rel-state)))
	rel-state)))

(defmethod graph-oadd-target (a b (rel-state rel-state))
  (graph-add-target a b rel-state))
