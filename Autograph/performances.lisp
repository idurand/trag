;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun eval-time-aut-term (term automaton &key (fois 1) (enum t))
  (evaluate-time
      (assert (recognized-p term automaton :enum enum)) fois))

(defun mouline-stream
    (automaton n stream cwd-term-fun &key (start 1) (step 1) (fois 1) (enum t))
  (loop
    for i from start
    for k from start by step
    repeat n
    do (let* ((term (funcall cwd-term-fun k))
	      (time (eval-time-aut-term term automaton :fois fois :enum enum)))
	 (format stream "~3D ~10,5F~%" k time))))

(defun mouline-file (automaton n file cwd-term-fun
		     &key (start 1) (step 1) (fois 1) (enum t))
  (with-open-file (stream file :direction :output :if-does-not-exist :create :if-exists :supersede)
    (mouline-stream
     automaton n stream cwd-term-fun
     :start start :step step :fois fois :enum enum)))

(defun 3colorability-on-grids (n &key (start 1) (step 1) (fois 1) (enum t))
  (let ((f (uncast-automaton (kcolorability-automaton 3))))
    (mouline-stream
     f n t
     #'cwd-term-square-grid
     :step step :start start :fois fois :enum enum)))

(defun mouline-aut1-vs-aut2-stream
    (aut2 aut1
     i j aut2-stream aut1-stream cwd-term-fun &key (step 1) (fois 1) (enum t))
  (loop
    for k from i to j by step
    do (let* ((term (funcall cwd-term-fun k))
	      (aut2-time (eval-time-aut-term term aut2 :fois fois :enum enum))
	      (aut1-time (eval-time-aut-term term aut1 :fois fois :enum enum)))
	 (format aut2-stream "~3D ~10,5F~%" k aut2-time)
	 (format aut1-stream "~3D ~10,5F~%" k aut1-time))))

(defun mouline-next-aut1-vs-aut2-stream 
    (aut1 aut2 i j aut1-out aut2-out term next-term-fun 
     &key (step 1) (aut1-times 1) (aut2-times 1) (fact 1) (enum t))
  (loop
    with next-fun = (apply-k-next step next-term-fun)
    for k from i to j by step
    ;;    do (format *trace-output* "~A " k)
    do (format
	aut1-out
	"~3D ~8,5F~%" k
	(* fact (eval-time-aut-term term aut1 :fois aut1-times :enum enum)))
    do (format
	aut2-out
	"~3D ~8,5F~%" k
	(* fact (eval-time-aut-term term aut2 :fois aut2-times)))
    do (setf term (funcall next-fun term))
       (format aut1-out "~%")
       (format aut2-out "~%")))

(defun mouline-next-aut1-vs-aut2-file
    (aut1 aut2
     i j aut1-file aut2-file
     term next-term-fun
     &key (step 1) (aut1-times 10) (aut2-times 10) (fact 1) (enum t))
  (with-open-file (aut1-out aut1-file :direction :output :if-does-not-exist :create :if-exists :supersede)
    (with-open-file (aut2-out aut2-file :direction :output :if-does-not-exist :create :if-exists :supersede)
      (mouline-next-aut1-vs-aut2-stream
       aut1 aut2 i j aut1-out aut2-out term next-term-fun
       :step step :aut1-times aut1-times :aut2-times aut2-times :fact fact :enum enum))))

(defun mouline-next-stream
    (automaton n stream initial-term next-term-fun  &key (step 1) (start 1) (fois 1) (enum t))
  (loop
    with next-fun = (apply-k-next step next-term-fun)
    repeat n
    for i from start
    for k from start by step
    for term = initial-term then (funcall next-fun term)
    do (format stream "~3D ~A~%" k (eval-time-aut-term
				    term automaton :fois fois :enum enum))))
(defun mouline-next-file
    (automaton n file initial-term next-term-fun  &key (step 1) (start 1) (fois 1) (enum t))
  (with-open-file (stream (date-string file)
			  :direction :output :if-does-not-exist :create :if-exists :supersede)
    (mouline-next-stream automaton n stream initial-term next-term-fun :step step :start start :fois fois :enum enum)))

(defun mouline-enum-vs-parallel-stream (automaton n stream cwd-term-fun &key (step 1) (start 1) (fois 1))
  (format stream "#      enum      parallel~%")
  (loop
    for i from start
    for k from start by step
    repeat n
    do (let* ((term (funcall cwd-term-fun k))
	      (time-enum (eval-time-aut-term term automaton :fois fois :enum t))
	      (time-parallel (eval-time-aut-term term automaton :fois fois :enum nil)))
	 (format stream "~3D ~10,5F ~10,5F~%" k time-enum time-parallel))))

(defun mouline-next-enum-vs-parallel-stream
    (automaton n stream initial-term next-term-fun  &key (step 1) (start 1) (fois 1))
  (format stream "#      enum      parallel~%")
  (loop
    with next-fun = (apply-k-next step next-term-fun)
    repeat n
    for i from start
    for k from start by step
    for term = initial-term then (funcall next-fun term)
    do (format stream "~3D ~10,5F ~10,5F~%" 
	       k 
	       (eval-time-aut-term term automaton :fois fois :enum t)
	       (eval-time-aut-term term automaton :fois fois :enum nil))))

(defun mouline-next-enum-vs-parallel-file
    (automaton n file initial-term next-term-fun  &key (step 1) (start 1) (fois 1))
  (with-open-file (stream (date-string file)
			  :direction :output :if-does-not-exist :create :if-exists :supersede)
    (mouline-next-enum-vs-parallel-stream automaton n stream initial-term next-term-fun :step step :start start :fois fois)))

(defun mouline-aut1-vs-aut2-file
    (aut2 aut1 i j aut2-file aut1-file cwd-term-fun
     &key (step 1) (fois 1) (enum t))
  (with-open-file (aut2-stream aut2-file :direction :output :if-does-not-exist :create :if-exists :supersede)
    (with-open-file (aut1-stream aut1-file :direction :output :if-does-not-exist :create :if-exists :supersede)
      (mouline-aut1-vs-aut2-stream
       aut2 aut1
       i j aut2-stream aut1-stream cwd-term-fun :step step :fois fois :enum enum))))

(defun mouline-aut1-vs-aut2-one-stream
    (aut2 aut1
     i j stream cwd-term-fun &key (step 1) (fois 1) (enum t))
  (loop
    for k from i to j by step
    do (let* ((term (funcall cwd-term-fun k))
	      (aut2-time (eval-time-aut-term
			  term aut2 :fois fois :enum enum))
	      (aut1-time (eval-time-aut-term
			  term aut1 :fois fois :enum enum)))
	 (format t "~3D ~10,5F ~10,5F~%" k aut2-time aut1-time)
	 (format stream "~3D ~10,5F ~10,5F~%" k aut2-time aut1-time))))

(defun mouline-aut1-vs-aut2-one-file
    (aut2 aut1 i j file cwd-term-fun
     &key (step 1) (fois 1) (enum t))
  (with-open-file (stream file :direction :output :if-does-not-exist :create :if-exists :supersede)
    (mouline-aut1-vs-aut2-one-stream
     aut2 aut1
     i j stream cwd-term-fun :step step :fois fois :enum enum)))

(defun aut1-vs-aut2-stream
    (aut2-stream aut1-stream
     aut2 aut1
     cwd-term-fun next-term-fun
     n ;; n computations
     &key (start 1) (step 1) (fois 1))
  (loop
    with enum = (not (deterministic-p aut2))
    with next-term-fun = (apply-k-next step next-term-fun)
    repeat n
    for i from start by step
    for term = (funcall cwd-term-fun start) then (funcall next-term-fun term)
    do (let ((aut2-time (eval-time-aut-term term aut2 :fois fois :enum enum))
	     (aut1-time (eval-time-aut-term term aut1 :fois fois :enum enum)))
	 (format t "~3D ~10,5F ~10,5F~%" i aut2-time aut1-time)
	 (unless (eq aut2-stream t)
	   (if (eq aut2-stream aut1-stream)
	       (format aut2-stream "~3D ~10,5F ~10,5F~%" i aut2-time aut1-time)
	       (progn
		 (format aut2-stream "~3D ~10,5F~%" i aut2-time)
		 (format aut1-stream "~3D ~10,5F~%" i aut1-time)))))))

(defun aut1-vs-aut2-file
    (aut1-file aut2-file
     aut1 aut2
     cwd-term-fun next-term-fun
     n
     &key  (start 1) (step 1) (fois 1))
  (with-open-file (aut2-stream aut2-file :direction :output :if-does-not-exist :create :if-exists :supersede)
    (with-open-file (aut1-stream aut1-file :direction :output :if-does-not-exist :create :if-exists :supersede)
      (aut1-vs-aut2-stream
       aut1-stream aut2-stream
       aut1 aut2
       cwd-term-fun next-term-fun
       n
       :start start :step step :fois fois))))

(defun connectedness-on-pn-streams (s1 s2 n &key (start 1) (step 1) (fois 1))
  (let* ((f (uncast-automaton (connectedness-automaton :cwd 3)))
	 (a (compile-automaton f)))
    (aut1-vs-aut2-stream
     s1 s2
     a f 
     #'cwd-term-pn
     #'next-pn
     n
     :start start
     :step step
     :fois fois)))

(defun connectedness-on-pn-files (f1 f2 n &key (start 1) (step 1) (fois 1))
  (with-open-file (s1 (date-string f1) :direction :output :if-does-not-exist :create :if-exists :supersede)
    (with-open-file (s2 (date-string f2)  :direction :output :if-does-not-exist :create :if-exists :supersede)
      (connectedness-on-pn-streams
       s1 s2
       n
       :start start :step step :fois fois))))

(defun 3colorability-on-rgrids (n m &key (start 1) (step 1) (fois 1) (enum t))
  (let ((f (uncast-automaton (kcolorability-automaton 3))))
    (mouline-next-stream
     f n t
     (cwd-term-rgrid start m)
     (lambda (term) (clique-width::grid-next-row term m))
     :step step :start start :fois fois :enum enum)))

(defun 3colorability-on-rgrids-enum-vs-parallel (n m &key (start 1) (step 1) (fois 1))
  (let ((f (uncast-automaton (kcolorability-automaton 3))))
    (mouline-next-enum-vs-parallel-stream
     f n t
     (cwd-term-rgrid start m)
     (lambda (term) (clique-width::grid-next-row term m))
     :step step :start start :fois fois)))

(defun 3colorability-on-grids-enum-vs-parallel (n &key (start 1) (step 1) (fois 1))
  (let ((f (uncast-automaton (kcolorability-automaton 3))))
    (mouline-enum-vs-parallel-stream
     f n t
     #'cwd-term-square-grid
     :step step :start start :fois fois)))
