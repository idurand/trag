;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun random-term-graph (term-size nleaves cwd proba)
  (with-open-stream
      (stream
       (sb-ext:process-output
	(sb-ext:run-program
	 "/Users/idurand/bin/gGen"
	 (mapcar (lambda (n) (format nil "~A" n))
		 (list term-size nleaves cwd proba))
	 :output :stream)))
    (read-line stream)
    (read-line stream)
    (let ((*char-ports* nil)
	  (*warn-new-symbols nil))
      (parse-term stream))))
      
(with-open-stream (files
     (sb-ext:process-output
      (sb-ext:run-program "/usr/bin/cat" '("/path/to/file")
       :output :stream)))
  (loop :for line = (read-line files nil nil)
 :while (and
  (my-mega-condition-p line))
	line) :do (princ line) (terpri))

