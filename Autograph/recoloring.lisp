;; Labels and colours are zero-based numbers
;; Coloring is store as two arrays of sorted lists, label->colors, color->labels
;; Term is (:op (parameters…) args…)
(defpackage :recoloring (:use :common-lisp :clique-width :general))

(in-package :recoloring)

(defun make-empty-coloring (labels colors)
  (cons
   (make-array labels :initial-element nil)
   (make-array colors :initial-element nil)))

(defun make-vertex-coloring (label labels colors)
  (let* ((res (make-empty-coloring labels colors)))
    (push 0 (elt (car res) label))
    (push label (elt (cdr res) 0))
    res))

(defun recolor (coloring permutation)
  (let* ((labels (length (car coloring)))
	 (colors (length (cdr coloring)))
	 (res (make-empty-coloring labels colors)))
    (loop for k from 0 to (1- colors) do
      (setf (elt (cdr res) (elt permutation k)) (elt (cdr coloring) k)))
    (loop for k from 0 to (1- labels) do
      (setf (elt (car res) k)
	    (loop for c in (elt (car coloring) k)
		  collect (elt permutation c))))
    res))

(defun merge-lists (&rest ls)
  (remove-adjacent-duplicates (sort (copy-list (reduce 'append ls)) '<)))

(defun coloring-rename-label (coloring a b)
  (let* ((labels (length (car coloring)))
	 (colors (length (cdr coloring)))
	 (res (make-empty-coloring labels colors)))
    (setf (car res) (subseq (car coloring) 0))
    (setf (elt (car res) b) (merge-lists (elt (car res) a) (elt (car res) b)))
    (setf (elt (car res) a) nil)
    (loop
      for j from 0 to (1- colors) do
	(setf (elt (cdr res) j)
	      (merge-lists
	       (set-difference (elt (cdr coloring) j) (list a b))
	       (when (intersection (list a b) (elt (cdr coloring) j)) (list b))
	       ))
      )
    res))

(defun merge-colorings (c1 c2)
  (let* ((labels (length (car c1)))
	 (colors (length (cdr c1)))
	 (res (make-empty-coloring labels colors)))
    (loop for k from 0 to (1- labels) do
      (setf (elt (car res) k)
            (merge-lists
	     (elt (car c1) k)
	     (elt (car c2) k))))
    (loop for k from 0 to (1- colors) do
      (setf (elt (cdr res) k)
            (merge-lists
	     (elt (cdr c1) k)
	     (elt (cdr c2) k))))
    res))

(defun matchings (minimal-size allowed-pairs &key id1 id2)
  (loop with l := (length allowed-pairs)
	with max-match :=
		       (reduce 'max (or (reduce 'append allowed-pairs) '(0)))
	with stack := (list (list 0 nil nil nil))
	with res := nil
	with sibling-used :=
			  (make-array (1+ max-match) :initial-element nil)
	with id2 :=
		 (or id2 
		     (map
		      'vector 'identity 
		      (loop for j from 0 to max-match collect j)))
	with id1 :=
		 (or id1
		     (map 'vector 'identity
			  (loop for j from 0 to l collect j)))
	for top := (pop stack)
	for k := (first top)
	for start := (second top)
	for used := (third top)
	for refused := (fourth top)
	for refused-local := nil
	unless top return res
	  do
	     (cond
	       ((= k l) 
		(when (>= (length start) minimal-size)
		  (push (reverse start) res)))
	       (t
		(loop for j from 0 to max-match
		      do (setf (elt sibling-used j) nil))
		(loop
		  for e in (elt allowed-pairs k)
		  unless (or (find e used)
			     (when
				 (find (list (elt id1 k) (elt id2 e))
				       refused :test 'equalp)
					;(format t "~s~%" (list :refused k e (elt id1 k) (elt id2 e)))
			       t)
			     (elt sibling-used (elt id2 e)))
		    do
		       (progn
			 (setf (elt sibling-used (elt id2 e)) t)
			 (push (list (elt id1 k) (elt id2 e))
			       refused-local)
			 (push
			  (list
			   (1+ k) (cons (list k e) start)
			   (cons e used) refused)
			  stack)))
		(unless
		    (< (+ (length used) (- l k 1)) minimal-size)
		  (push (list (1+ k) start used (append refused-local refused))
			stack))))))

(defun permutation-from-matching (n l1 l2 m)
  (let*
      (
       (res  (make-array n :initial-element nil))
       (used (make-array n :initial-element nil))
       (min-unused l1)
       )
    (loop for p in m do
      (progn
	(setf (elt res (second p)) (first p))
	(setf (elt used (first p)) t)
	))
    (loop for k from 0 to (1- l2)
	  unless (elt res k)
	    do
	       (progn
		 (loop
		   while (elt used min-unused)
		   do (incf min-unused))
		 (setf (elt res k) min-unused)
		 (setf (elt used min-unused) t)
		 (incf min-unused)
		 ))
    (setf min-unused 0)
    (loop for k from l2 to (1- n)
	  do
	     (progn
	       (loop
		 while (elt used min-unused)
		 do (incf min-unused))
	       (setf (elt res k) min-unused)
	       (setf (elt used min-unused) t)
	       (incf min-unused)
	       ))
    res))


(defun acceptable-merges (c1 c2 adds &key forbid-identification)
  (loop
    with colors := (length (cdr c1))
    with n1 := (or (position nil (cdr c1)) colors)
    with n2 := (or (position nil (cdr c2)) colors)
    with min-match := (- (+ n1 n2) colors)
    with compatible-colors :=
			   (loop
			     for k1 from 0 to (1- n1)
			     collect
			     (loop
			       for k2 from 0 to (1- n2)
			       unless
			       (loop
				 for l1 in (elt (cdr c1) k1)
				 when
				 (loop
				   for l2 in (elt (cdr c2) k2)
				   when (elt (elt adds l1) l2)
				     return t)
				 return t)
			       collect k2))
    with label-set-ht := (make-hash-table :test 'equalp)
    with id2 :=
	     (unless forbid-identification
	       (loop for j from 0 to (1- n2) do 
		 (setf 
		  (gethash (elt (cdr c2) j) label-set-ht)
		  (gethash (elt (cdr c2) j) label-set-ht j)))
	       (map
		'vector 'identity
		(loop for j from 0 to (1- n2)
		      collect (gethash (elt (cdr c2) j) label-set-ht j)
		      )))
    with id1 :=
	     (unless forbid-identification
	       (loop for j from 0 to (1- n1) do 
		 (setf 
		  (gethash (elt (cdr c1) j) label-set-ht)
		  (gethash (elt (cdr c1) j) label-set-ht j)))
	       (map
		'vector 'identity
		(loop for j from 0 to (1- n1)
		      collect (gethash (elt (cdr c1) j) label-set-ht j)
		      )))
    with matchings :=
		   (matchings min-match compatible-colors
			      :id1 id1 :id2 id2)
    for m in matchings
    for p := (permutation-from-matching colors n1 n2 m)
    for c2-prime := (recolor c2 p)
    collect (list 1 (merge-colorings c1 c2-prime))
    ))

(defun make-labels-annotations (term)
  (let*
      ((op (first term))
       (params (second term))
       (args (rest (rest term)))
       (mapped-args (mapcar 'make-labels-annotations args))
       (labels
	   (reduce 'merge-lists
		   (loop
		     for a in mapped-args
		     collect (getf (second a) :labels))))
       )
    (case op
      ((:vertex)
       `(,op
	 (:labels (,(getf params :label)) 
	   :max-label ,(getf params :label)
	   ,@params)))
      ((:ren)
       (let*
	   ((from (getf params :from))
	    (to   (getf params :to  ))
	    (new-labels (merge-lists
			 (set-difference labels (list from to))
			 (when (intersection labels (list from to))
			   (list to)))))
	 `(,op
	   (:labels ,new-labels :inner-labels ,labels 
	     :max-label
	     ,(reduce 'max
		      (list
		       from to
		       (getf (second (first mapped-args)) :max-label)))
	     ,@params)
	   ,@ mapped-args)))
      ((:add :oplus)
       `(,op
	 (:labels ,labels 
	   :max-label
	   ,(reduce 'max (loop for a in  mapped-args 
			       collect (getf (second a) :max-label)))
	   ,@params)
	 ,@ mapped-args)))))


(defun make-add-annotations (term &optional edges)
  (unless (getf (second term) :labels)
    (return-from make-add-annotations
      (make-add-annotations
       (make-labels-annotations term))))
  (let*
      ((op (first term))
       (params (second term))
       (args (rest (rest term)))
       (labels (getf params :labels))
       (edges (loop 
		for e in edges 
		when (and (find (first e) labels)
			  (find (second e) labels))
		  collect e))
       )
    (case op
      ((:vertex)
       `(,op
	 (:add ,edges ,@params)))
      ((:oplus)
       `(,op
	 (:add ,edges ,@params)
	 ,@(loop for a in args
		 collect (make-add-annotations a edges))))
      ((:add)
       (let*
	   ((from (getf params :from))
	    (to   (getf params :to  )) 
	    (edge (sort (list from to) '<))
	    (edges (cond
		     ((find edge edges :test 'equal) edges)
		     ((and (find from labels) (find to labels))
		      (cons edge edges))
		     (t edges))))
	 `(,op
	   (:add ,edges ,@params)
	   ,@(loop for a in args
		   collect (make-add-annotations a edges)))))
      ((:ren)
       (let*
	   ((from (getf params :from))
	    (to   (getf params :to  )) 
	    (inner-labels (getf params :inner-labels))
	    (inner-edges
	      (loop 
		for e in edges
		for a := (first e)
		for b := (second e)
		when
		(and
		 (find a inner-labels)
		 (find b inner-labels))
		collect (list a b)
		when 
		(and (= a to)
		     (find from inner-labels)
		     (find b inner-labels))
		collect (sort (list from b) '<)
		when 
		(and (= b to)
		     (find a inner-labels)
		     (find from inner-labels))
		collect (sort (list a from) '<)))
	    )
	 `(,op
	   (:add ,edges :inner-add ,inner-edges ,@params)
	   ,@(loop for a in args
		   collect (make-add-annotations a inner-edges)))))
      )))

(defun canonical-colorings (term n-colors &key n-labels forbid-identification)
  (when
      (eq (getf (second term) :add t) t)
    (return-from canonical-colorings
      (canonical-colorings (make-add-annotations term)
			   n-colors :n-labels n-labels
				    :forbid-identification forbid-identification)))
  (let*
      ((op (first term))
       (params (second term))
       (args (rest (rest term)))
       (edges (getf params :add))
       (n-labels (or n-labels (1+ (getf params :max-label))))
       )
    (case op
      ((:vertex)
       (list
	(list 1 (make-vertex-coloring
		 (getf params :label) n-labels n-colors))))
      ((:add) (canonical-colorings (first args) n-colors :n-labels n-labels
							 :forbid-identification forbid-identification))
      ((:oplus)
       (let*
	   ((adds 
	      (map 
	       'vector 'identity
	       (loop
		 for x from 0 to (1- n-labels)
		 collect (make-array n-labels :initial-element nil))))
	    (ht (make-hash-table :test 'equalp))
	    (res nil))
	 (loop
	   for e in edges
	   for a := (first e)
	   for b := (second e)
	   do (setf (elt (elt adds a) b) t)
	   do (setf (elt (elt adds b) a) t))
	 (loop
	   for c1 in
		  (canonical-colorings
		   (first args) n-colors :n-labels n-labels
					 :forbid-identification forbid-identification
					 )
	   do
	      (loop
		for c2 in
		       (canonical-colorings
			(second args) n-colors :n-labels n-labels
					       :forbid-identification forbid-identification
					       )
		do
		   (loop
		     for c in (acceptable-merges (second c1) (second c2) adds
						 :forbid-identification forbid-identification)
		     do
			(incf (gethash (second c) ht 0) (* (first c1) (first c2) (first c))))))
	 (maphash
	  (lambda (k v) (push (list v k) res))
	  ht)
	 res))
      ((:ren)
       (let*
	   (
	    (ht (make-hash-table :test 'equalp))
	    (res nil)
	    )
	 (loop
	   for c in
		 (canonical-colorings
		  (first args) n-colors :n-labels n-labels
					:forbid-identification forbid-identification)
	   do
	      (incf
	       (gethash (coloring-rename-label
			 (second c) (getf params :from) (getf params :to)) ht 0)
	       (first c)))
	 (maphash
	  (lambda (k v) (push (list v k) res))
	  ht)
	 res
	 )))))

(defun convert-from-autograph-web-term (term)
  (if
   (stringp term) `(:vertex (:label ,(parse-port term)))
   (let* ((op (intern (string-upcase (first term)) :keyword))
	  (args (cdddr term))
	  (params (second term)))
     (case op
       (:ren
	`(,op (
	       :from ,(parse-port (first params))
	       :to ,(parse-port (second params)))
	      ,(convert-from-autograph-web-term (first args))))
       (:add
	`(,op (
	       :from ,(parse-port (first params))
	       :to ,(parse-port (second params)))
	      ,(convert-from-autograph-web-term (first args))))
       (:oplus
	`(,op ()
	      ,(convert-from-autograph-web-term (first args))
	      ,(convert-from-autograph-web-term (second args))))))))


(defun convert-from-cwd-term (term)
  (let* ((root (terms:root term))
         (root-name (object:name root))
         (ports (clique-width:symbol-ports root))
         (args (terms:arg term)))
    (cond
      ((null args)
       `(:vertex (:label ,(parse-port root-name))))
      ((equalp root-name "oplus")
       `(:oplus ()
                ,@(mapcar 'convert-from-cwd-term args)))
       ((clique-width:add-symbol-p root)
	`(:add (
	       :from ,(first ports)
	       :to ,(second ports))
	      ,(convert-from-cwd-term (first args))))
       ((clique-width:ren-symbol-p root)
	`(:ren (
	       :from ,(first ports)
	       :to ,(second ports))
	      ,(convert-from-cwd-term (first args)))))))

(defun convert-from-graph-term (term)
  (convert-from-cwd-term
   (autograph::input-cwd-term (format nil "~a" term))))

(defun make-canonical-colorings-annotations
    (term n-colors &key n-labels forbid-identification)
  (when (eq (getf (second term) :add t) t)
    (return-from make-canonical-colorings-annotations
      (make-canonical-colorings-annotations
       (make-add-annotations term)
       n-colors :n-labels n-labels
		:forbid-identification forbid-identification)))
  (let* ((op (first term))
	 (params (second term))
	 (args (rest (rest term)))
	 (edges (getf params :add))
	 (n-labels (or n-labels (1+ (getf params :max-label))))
	 (mapped-args
	   (loop for a in args
		 collect 
		 (make-canonical-colorings-annotations
		  a n-colors :n-labels n-labels
			     :forbid-identification forbid-identification
			     )))
	 (arg-colorings
	   (loop for a in mapped-args
		 collect (getf (second a) :colorings)))
	 (colorings
	   (case op
	     ((:vertex)
	      (list
	       (list 1 (make-vertex-coloring
			(getf params :label) n-labels n-colors))))
	     ((:add) (first arg-colorings))
	     ((:oplus)
	      (let* ((adds 
		       (map 
			'vector 'identity
			(loop
			  for x from 0 to (1- n-labels)
			  collect (make-array n-labels :initial-element nil))))
		     (ht (make-hash-table :test 'equalp))
		     (res nil))
		(loop for e in edges
		      for a := (first e)
		      for b := (second e)
		      do (setf (elt (elt adds a) b) t)
		      do (setf (elt (elt adds b) a) t))
		(loop for c1 in
			     (first arg-colorings)
		      do
			 (loop for c2 in
				      (second arg-colorings)
			       do
				  (loop for c in (acceptable-merges (second c1) (second c2) adds
								    :forbid-identification forbid-identification)
					do
					   (incf (gethash (second c) ht 0) (* (first c1) (first c2) (first c))))))
		(maphash
		 (lambda (k v) (push (list v k) res))
		 ht)
		res))
	     ((:ren)
	      (let* ((ht (make-hash-table :test 'equalp))
		     (res nil))
		(loop for c in (first arg-colorings)
		      do (incf
			  (gethash (coloring-rename-label
				    (second c) (getf params :from) (getf params :to)) ht 0)
			  (first c)))
		(maphash
		 (lambda (k v) (push (list v k) res))
		 ht)
		res)))))
    `(,op (:colorings ,colorings ,@params) ,@ mapped-args)))

(defun dump (fn obj &key (format "~s~%"))
  (with-open-file (f fn :direction :output :if-exists :supersede)
    (let ((*print-right-margin* 999999)) (format f format obj))))

;; ; rlwrap sbcl --dynamic-space-size 4096 --load setup.lisp --eval "(mapcar 'asdf:make '(:autograph-web :autotree))" --load local-projects/trag/Autograph/recoloring.lisp  --load local-projects/trag/Autograph/brute-force-colouring.lisp --load local-projects/trag/Autograph/generate-flower-term.lisp --load local-projects/trag/Autograph/graph-coloring-to-glucose.lisp
;; ; (sb-ext:gc :full t) (let* ((c 3) (n 3) (term (autograph::stage-petal-k n)) (gm (graph-matrix-from-graph-term term)) (mt (convert-from-graph-term term)) (at (autograph::compute-add-annotation term)) (ct (autograph::compute-coloring-annotations term)) (ca (autograph::kcolorability-automaton c))) (list (time (reduce '+ (mapcar 'first (canonical-colorings mt c)))) (time (ignore-errors (list (autograph::recognized-p ct ca)))) (time (ignore-errors (list (autograph::recognized-p at ca)))) (time (ignore-errors (list (autograph::enum-recognized-p at ca)))) (time (ignore-errors (list (autograph::enum-recognized-p ct ca)))) (time (ignore-errors (list (autograph::run-count at ca)))) (time (1- (length (k-col-direct gm c))))))
;; ; (sb-ext:gc :full t) (let* ((c 3) (n 6) (term (autograph::stage-petal-k n)) (gm (graph-matrix-from-graph-term term)) (mt (convert-from-graph-term term)) (at (autograph::compute-add-annotation term)) (ct (autograph::compute-coloring-annotations term)) (ca (autograph::kcolorability-automaton c)) (gcnf (graph-term-to-glucose term c))) (dump "/tmp/test.cnf" gcnf :format "~a") (list (time (reduce '+ (mapcar 'first (canonical-colorings mt c))))  (time (ignore-errors (list (autograph::recognized-p at ca))))  (time (1- (length (k-col-direct gm c))))))

;; ; (loop for g in (remove-if (lambda (x) (> (graph::nodes-size (graph::graph-nodes x)) 50)) (remove nil (loop for p in (append (directory "local-projects/trag/Decomp/Data/*.col") (directory "local-projects/trag/Decomp/Data/*/*.col")) do (format t "~s~%" p) collect (ignore-errors (graph::load-dimacs-graph-absolute (namestring p) nil))))) for term := (decomp::cwd-decomposition g) for myterm := (ignore-errors (convert-from-graph-term term)) do (format t "~s~%" g) do (loop for c upfrom 1 for my-ok := (> (length (time (canonical-colorings myterm c))) 0) for ok := (time (autograph::recognized-p term (autograph::kcolorability-automaton c))) do (format t "~s ~s ~s ~s~%~%----~%~%" g c my-ok ok) do (assert (equal ok my-ok)) while (not my-ok))))

#+nil
(let* ((ag-term (autograph::cwd-term-rgrid 40 4))
       (my-term (recoloring::convert-from-cwd-term ag-term))
       (ann-term (recoloring::make-add-annotations
                   (recoloring::make-labels-annotations my-term)))
       (colorings 
         (time (recoloring::canonical-colorings
                 ann-term 3 :forbid-identification t)))
       (counts (mapcar 'car colorings))
       (ag-count (time (autograph::compute-count
                         ag-term (autograph::kcolorability-automaton 3)))))
  (list (reduce '+ counts) ag-count))
