;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defgeneric multiple-add-add-p (annotated-symbol rel-state))
(defmethod multiple-add-add-p ((as annotated-symbol) (rel-state rel-state))
  (and
   (not (oplus-symbol-p as))
   (let* ((root (symbol-of as))
	  (ports (symbol-ports root)))
     (and (add-symbol-p root)
	  (rel-state-member
	   (list (first ports) (second ports))
	   rel-state)))))

;; input: annotated-term with add-annotation
;; output: non redundant term with identity operations
(defgeneric remove-multiple-edges-rec (term))
(defmethod remove-multiple-edges-rec ((term term))
  (if (term-zeroary-p term)
      term
      (let* ((root (root term))
	     (annotation (annotation root)))
	(build-term
	 (if (multiple-add-add-p root annotation)
	     (identity-symbol)
	     (symbol-of root))
	 (mapcar
	  #'remove-multiple-edges-rec
	  (arg term))))))

(defgeneric remove-multiple-edges (term))
(defmethod remove-multiple-edges ((term term))
  (let ((aterm (compute-add-annotation term)))
    (remove-identity-symbols 
     (remove-annotation
      (remove-multiple-edges-rec aterm)))))

(defgeneric no-effect-op-p (term))
(defmethod no-effect-op-p ((term term))
  (let ((root (root term)))
    (and
     (not (oplus-symbol-p root))
     (let* ((ports (container-contents (graph-ports-of (car (arg term)))))
	    (symbol-ports (symbol-ports root))
	    (a (first symbol-ports))
	    (b (second symbol-ports)))
       (or
	(and (add-symbol-p root)
	     (or (not (ports-member a ports))
		 (not (ports-member b ports))))
	(and (ren-symbol-p root)
	     (not (ports-member a ports))))))))

(defgeneric remove-no-effect-op-rec (term))
(defmethod remove-no-effect-op-rec ((term term))
  (if (term-zeroary-p term)
      term
      (build-term
       (if (no-effect-op-p term)
	   (identity-symbol)
	   (root term))
       (mapcar
	#'remove-no-effect-op-rec
	(arg term)))))

(defgeneric remove-no-effect-op (term))
(defmethod remove-no-effect-op ((term term))
  (remove-identity-symbols
   (remove-no-effect-op-rec term)))
