;; Unfinished

(defun bottom-up-value (term funs)
  (destructuring-bind (op &rest args) term
    (case op
      (cwd-term-vertex
       (destructuring-bind
	   (label &rest modifiers) args
	 (apply (getf funs :fun-vertex) label :allow-other-keys t modifiers)))
      (cwd-term-ren
       (destructuring-bind
	   (from to subterm &rest modifiers) args
         (apply (getf funs :fun-ren)
                from to (bottom-up-value subterm funs)
                :allow-other-keys t modifiers)))
      (cwd-term-unoriented-add
       (destructuring-bind
	   (from to subterm &rest modifiers) args
         (apply (or
		 (getf funs :fun-unoriented-add)
		 (getf funs :fun-add))
                from to (bottom-up-value subterm funs)
                :allow-other-keys t modifiers)))
      (cwd-term-add
       (destructuring-bind
	   (from to subterm &rest modifiers) args
         (apply
	  (getf funs :fun-add)
	  from to (bottom-up-value subterm funs)
	  :allow-other-keys t modifiers)))
      (cwd-term-oplus
       (destructuring-bind
	   (left right &rest modifiers) args
         (apply (getf funs :fun-oplus)
                (bottom-up-value left funs)
                (bottom-up-value right funs)
                :allow-other-keys t modifiers)))
      (t (error "Unknown operation ~s." op)))))

(defun bottom-up-run (term funs &key (result-key :result) drop-modifiers)
  (destructuring-bind (op &rest args) term
    (case op
      (cwd-term-vertex
       (destructuring-bind
	   (label &rest modifiers) args
         (let*
	     ((result
		(apply (getf funs :fun-vertex) label :allow-other-keys t modifiers)))
           (values
	    `(,op ,label ,result-key ,result
		  ,@(unless drop-modifiers modifiers))
	    result))))
      (cwd-term-ren
       (destructuring-bind
	   (from to subterm &rest modifiers) args
         (multiple-value-bind
	       (subimage subresult)
	     (bottom-up-run
	      subterm funs
	      :result-key result-key
	      :drop-modifiers :drop-modifiers)
           (let*
	       ((result
		  (apply (getf funs :fun-ren)
			 from to subresult
			 :allow-other-keys t modifiers)))
             (values
	      `(,op ,from ,to ,subimage
		    ,result-key ,result
		    ,@(unless drop-modifiers modifiers))
	      result)))))
      (cwd-term-unoriented-add
       (destructuring-bind
	   (from to subterm &rest modifiers) args
         (multiple-value-bind
	       (subimage subresult)
	     (bottom-up-run
	      subterm funs
	      :result-key result-key
	      :drop-modifiers :drop-modifiers)
           (let*
	       ((result
		  (apply
		   (or
                    (getf funs :fun-unoriented-add)
                    (getf funs :fun-add))
		   from to subresult
		   :allow-other-keys t modifiers)))
             (values
	      `(,op ,from ,to ,subimage
		    ,result-key ,result
		    ,@(unless drop-modifiers modifiers))
	      result)))))
      (cwd-term-add
       (destructuring-bind
	   (from to subterm &rest modifiers) args
         (multiple-value-bind
	       (subimage subresult)
	     (bottom-up-run
	      subterm funs
	      :result-key result-key
	      :drop-modifiers :drop-modifiers)
           (let*
	       ((result
		  (apply
		   (getf funs :fun-add)
		   from to subresult
		   :allow-other-keys t modifiers)))
             (values
	      `(,op ,from ,to ,subimage
		    ,result-key ,result
		    ,@(unless drop-modifiers modifiers))
	      result)))))
      (cwd-term-oplus
       (destructuring-bind
	   (left right &rest modifiers) args
         (multiple-value-bind
	       (leftimage leftresult)
	     (bottom-up-run
	      left funs
	      :result-key result-key
	      :drop-modifiers :drop-modifiers)
           (multiple-value-bind
		 (rightimage rightresult)
	       (bottom-up-run
		right funs
		:result-key result-key
		:drop-modifiers :drop-modifiers)
             (let*
		 ((result
		    (apply
		     (getf funs :fun-oplus)
		     leftresult rightresult
		     :allow-other-keys t modifiers)))
               (values
		`(,op ,leftimage ,rightimage
		      ,result-key ,result
		      ,@(unless drop-modifiers modifiers))
		result))))))
      (t (error "Unknown operation ~s." op)))))

(defun top-down-run (term state funs &key (result-key :result) drop-modifiers)
  (destructuring-bind (op &rest args) term
    (case op
      (cwd-term-vertex
       (destructuring-bind
	   (label &rest modifiers) args
         (multiple-value-bind
	       (result substate)
	     (apply
	      (getf funs :fun-vertex)
	      state label
	      modifiers)
           substate
           `(,op ,label ,result-key ,result
                 ,@(unless drop-modifiers modifiers)))))
      (cwd-term-ren
       (destructuring-bind
	   (from to subterm &rest modifiers) args
         (multiple-value-bind
	       (result substate)
	     (apply (getf funs :fun-ren)
		    state from to
		    modifiers)
           `(,op ,from ,to
                 ,(top-down-run
		   subterm substate funs
		   :result-key result-key
		   :drop-modifiers drop-modifiers)
                 ,result-key ,result
                 ,@(unless drop-modifiers modifiers)))))
      (cwd-term-unoriented-add
       (destructuring-bind
	   (from to subterm &rest modifiers) args
         (multiple-value-bind
	       (result substate)
	     (apply
	      (or
               (getf funs :fun-unoriented-add)
               (getf funs :fun-add))
	      state from to
	      modifiers)
           `(,op ,from ,to
                 ,(top-down-run
		   subterm substate funs
		   :result-key result-key
		   :drop-modifiers drop-modifiers)
                 ,result-key ,result
                 ,@(unless drop-modifiers modifiers)))))
      (cwd-term-add
       (destructuring-bind
	   (from to subterm &rest modifiers) args
         (multiple-value-bind
	       (result substate)
	     (apply
	      (getf funs :fun-add)
	      state from to
	      modifiers)
           `(,op ,from ,to
                 ,(top-down-run
		   subterm substate funs
		   :result-key result-key
		   :drop-modifiers drop-modifiers)
                 ,result-key ,result
                 ,@(unless drop-modifiers modifiers)))))
      (cwd-term-oplus
       (destructuring-bind
	   (left right &rest modifiers) args
         (multiple-value-bind
	       (result substate)
	     (apply
	      (getf funs :fun-oplus)
	      state modifiers)
           `(,op
	     ,(top-down-run
	       left substate funs
	       :result-key result-key
	       :drop-modifiers drop-modifiers)
	     ,(top-down-run
	       right substate funs
	       :result-key result-key
	       :drop-modifiers drop-modifiers)
	     ,result-key ,result
	     ,@(unless drop-modifiers modifiers)))))
      (t (error "Unknown operation ~s." op)))))

(defparameter
    *vertex-count-funs*
  (list
   :fun-vertex (constantly 1)
   :fun-oplus (lambda (x y &key) (+ x y))
   :fun-add (lambda (x y n &key) x y n)
   :fun-ren (lambda (x y n &key) x y n)))

(assert
 (=
  (bottom-up-value
   `(cwd-term-ren
     1 0
     (cwd-term-unoriented-add
      0 1
      (cwd-term-oplus
       (cwd-term-vertex 0)
       (cwd-term-vertex 1))))
   *vertex-count-funs*)
  2))
(assert
 (=
  (getf
   (cddddr
    (bottom-up-run
     `(cwd-term-ren 
       1 0 
       (cwd-term-unoriented-add
	0 1
	(cwd-term-oplus
	 (cwd-term-vertex 0)
	 (cwd-term-vertex 1))))
     *vertex-count-funs*))
   :result)
  2))

(defun universal-< (x y)
  (cond
    ((and (null x) (null y)) nil)
    ((null x) t)
    ((null y) nil)
    ((and (realp x) (realp y)) (< x y))
    ((realp x) t)
    ((realp y) nil)
    ((and (numberp x) (numberp y))
     (universal-< (list (realpart x) (imagpart x))
                  (list (realpart y) (imagpart y))))
    ((numberp x) t)
    ((numberp y) nil)
    ((and (stringp x) (stringp y)) (string< x y))
    ((stringp x) t)
    ((stringp y) nil)
    ((and (characterp x) (characterp y)) (char< x y))
    ((characterp x) t)
    ((characterp y) nil)
    ((and (symbolp x) (symbolp y))
     (universal-<
      (list (package-name (symbol-package x)) (symbol-name x))
      (list (package-name (symbol-package y)) (symbol-name y))))
    ((symbolp x) t)
    ((symbolp y) nil)
    ((and (pathnamep x) (pathnamep y))
     (string< (namestring x) (namestring y)))
    ((pathnamep x) t)
    ((pathnamep y) nil)
    ((and (vector x) (vectorp y))
     (loop
       for j from 0 to (1- (min (length x) (length y)))
       when (universal-< (elt x j) (elt y j)) return t
	 when (universal-< (elt y j) (elt x j)) return nil
	   finally (return (< (length x) (length y)))))
    ((vectorp x) t)
    ((vectorp y) nil)
    ((and (consp x) (consp y) (universal-< (car x) (car y))) t)
    ((and (consp x) (consp y) (universal-< (car y) (car x))) nil)
    ((and (consp x) (consp y)) (universal-< (cdr x) (cdr y)))
    ((consp x) t)
    ((consp y) nil)
    (t
     (< (sxhash x) (sxhash y)))))

(defun merge-sorted-lists (x y &key (test #'equal))
  (let
      ((res nil) (match nil))
    (loop
      while (and x y)
      for fx := (pop x)
      for fy := (pop y)
      do
	 (cond
	   ((funcall test fx fy) (push fx res) (setf match t))
	   ((universal-< fx fy)
	    (progn (push fx res) (push fy y)))
	   (t (progn (push fy res) (push fx x)))))
    (loop for e in x do (push e res))
    (loop for e in y do (push e res))
    (values (reverse res) match)))

(defun merge-hash-tables (x y &key (test #'equal))
  (let
      ((res (make-hash-table :test test)) (match t))
    (maphash
     (lambda (k v)
       (setf (gethash k res) v))
     x)
    (maphash
     (lambda (k v)
       (when (gethash k res) (setf match t))
       (setf (gethash k res) v))
     y)
    (values res match)))

(defun copy-hash-table (x &key (test (hash-table-test x)))
  (let ((res (make-hash-table :test test)))
    (maphash
     (lambda (k v) (setf (gethash k res) v))
     x)
    res))

(defun add-hash-tables (x y &key (test (hash-table-test x)))
  (let ((res (make-hash-table :test test)))
    (maphash
     (lambda (k v) (incf (gethash k res 0) v))
     x)
    (maphash
     (lambda (k v) (incf (gethash k res 0) v))
     y)
    res))

(defparameter
    *make-add-annotations*
  (list
   :fun-vertex (lambda (edges label &key) label edges)
   :fun-oplus (lambda (edges &key) (values edges edges))
   :fun-ren (lambda (edges from to &key)
	      (let
		  (fe te oe)
		(loop
		  for e in edges
		  do
		     (if (equal (first e) to)
			 (if (equal (second e) from)
			     nil
			     (push (list from (second e)) fe))
			 (if (equal (second e) to)
			     (if (equal (first e) from)
				 nil
				 (push (list (first e) from) te))
			     (push e oe))))
		(let
		    ((result 
		       (merge-sorted-lists 
                        oe (merge-sorted-lists fe te))))
		  (values result result))))
   :fun-add (lambda (old-edges from to &key)
	      (let
		  ((result
		     (merge-sorted-lists 
                      (list (list from to)) old-edges)))
		(values result result)))
   ))

(assert
 (top-down-run
  `(cwd-term-ren
    1 0
    (cwd-term-unoriented-add
     0 1
     (cwd-term-oplus
      (cwd-term-vertex 0)
      (cwd-term-vertex 1))))
  () *make-add-annotations*))

(defun clique-term (n)
  (loop
    for k from 1 to n
    for g := `(cwd-term-vertex 0)
      then
    `(cwd-term-ren 
      1 0 
      (cwd-term-unoriented-add
       0 1 
       (cwd-term-oplus 
	(cwd-term-vertex 1)
	,g)))
    finally (return g)))

(defun clique-term-flip (n)
  (loop
    for k from 1 to n
    for g := `(cwd-term-vertex 0)
      then
    `(cwd-term-ren 
      1 0 
      (cwd-term-unoriented-add
       0 1 
       (cwd-term-oplus
	,g
	(cwd-term-vertex 1))))
    finally (return g)))

(assert
 (top-down-run
  (clique-term 3)
  ()
  *make-add-annotations*))

(defun kcolorability-funs (k)
  (list
   :fun-vertex
   (lambda (label &key)
     (loop
       for c from 0 to (1- k)
       collect `(,label (,c))))
   :fun-ren
   (lambda (from to colorings &key)
     (remove-adjacent-duplicates
      (loop for cc in colorings
	    collect (let ((cc (copy-list cc)))
		      (setf (getf cc to)
			    (merge-sorted-lists
			     (getf cc from)
			     (getf cc to)))
		      (setf (getf cc from) nil)
		      cc))
      :test #'equal))
   :fun-add
   (lambda (from to colorings &key)
     (loop
       for cc in colorings
       for collision := 
		     (second
		      (multiple-value-list
		       (merge-sorted-lists (getf cc from) (getf cc to))))
       unless collision collect cc))
   :fun-oplus
   (lambda (left right &key)
     (let
	 ((res nil))
       (loop
	 for ccl in left do
	   (loop
	     for ccr in right
	     do
		(let
		    ((cc nil) (ccl ccl) (ccr ccr))
		  (loop
		    while (and ccl ccr)
		    for cl := (pop ccl)
		    for cr := (pop ccr)
		    do
		       (cond
			 ((= cl cr)
			  (push cl cc) 
			  (push (merge-sorted-lists
				 (pop ccl) (pop ccr)) cc))
			 ((< cl cr) (push cr ccr)
			  (push cl cc)
			  (push (pop ccl) cc))
			 (t (push cl ccl)
			    (push cr cc)
			    (push (pop ccr) cc))
			 )
		    )
		  (loop for x in ccl do (push x cc))
		  (loop for x in ccr do (push x cc))
		  (push (reverse cc) res)
		  )
	     )
	 )
       (remove-adjacent-duplicates (sort res 'universal-<) :test #'equal)))
   ))

(bottom-up-run
 (clique-term 3)
 (kcolorability-funs 2))

(bottom-up-run
 (clique-term 3)
 (kcolorability-funs 3))

(bottom-up-run
 (fourth (fourth (clique-term 2)))
 (kcolorability-funs 2))

(bottom-up-run
 (clique-term 2)
 (kcolorability-funs 2))

(bottom-up-value
 (clique-term 3)
 (kcolorability-funs 3))

(bottom-up-value
 (clique-term 3)
 (kcolorability-funs 2))


#|

(time
 (bottom-up-value
  (clique-term 12)
  (kcolorability-funs 12)))

(time
 (bottom-up-value
  (clique-term-flip 12)
  (kcolorability-funs 12)))

(time
 (termauto::compute-target
  (autograph::cwd-term-kn 12)
  (autograph::kcolorability-automaton 12)))

|#

(defparameter *root-presence-funs*
  (list
   :fun-vertex (lambda (label &key root) label root)
   :fun-ren (lambda (from to root &key) from to root)
   :fun-add (lambda (from to root &key) from to root)
   :fun-oplus (lambda (left right &key) (or left right))))

(bottom-up-run
 `(cwd-term-add
   0 1
   (cwd-term-add
    0 2
    (cwd-term-oplus
     (cwd-term-oplus
      (cwd-term-oplus
       (cwd-term-vertex 0)
       (cwd-term-vertex 3))
      (cwd-term-vertex 2 :root t))
     (cwd-term-vertex 1))))
 *root-presence-funs*)

(defparameter *nr-forest-funs*
  (list
   :fun-vertex (lambda (label &key)
		 (let ((ht (make-hash-table)))
		   (setf (gethash label ht) 1)
		   (list ht)))
   :fun-ren (lambda (from to comps &key)
	      (mapcar
	       (lambda (cc)
		 (let ((res (copy-hash-table cc)))
		   (incf (gethash to res 0)
			 (gethash from res 0))
		   (remhash from res)
		   (when (= 0 (gethash to res))
		     (remhash to res))
		   res))
	       comps))
   :fun-add (lambda (from to comps &key)
	      (block
		  add
		(let
		    ((from-comps nil)
		     (to-comps nil)
		     (neither-comps nil)
		     (from-count 0)
		     (to-count 0)
		     (from-max 0)
		     (to-max 0))
		  (loop
		    for cc in comps
		    for fc := (gethash from cc)
		    for tc := (gethash to cc)
		    do
		       (cond
			 ((and fc tc) (return-from add 0))
			 (fc (incf from-count fc)
			     (push cc from-comps)
			     (setf from-max (max from-max fc)))
			 (tc (incf to-count tc)
			     (push cc to-comps)
			     (setf to-max (max to-max tc)))
			 (t (push cc neither-comps))))
		  (cond
		    ((= from-count 0) (append to-comps neither-comps))
		    ((= to-count 0) (append from-comps neither-comps))
		    ((and (> from-count 1) (> to-count 1)) nil)
		    ((or (> from-max 1) (> to-max 1)) nil)
		    (t (let ((merged (make-hash-table)))
			 (loop
			   for cc in from-comps
			   do (setf merged (add-hash-tables merged cc)))
			 (loop
			   for cc in to-comps
			   do (setf merged (add-hash-tables merged cc)))
			 (cons merged neither-comps)))
		    ))))
   :fun-oplus (lambda (left right &key) (and left right (append left right)))))

(assert
 (not
  (bottom-up-value
   (clique-term 3)
   *nr-forest-funs*)))

