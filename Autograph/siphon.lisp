;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defclass siphon-state (graph-state)
  ((siphon-cports :type 'ports :initarg :siphon-cports :reader siphon-cports)
   (ports-in :type 'ports :initarg :ports-in :reader ports-in) ;; come in cannot go out
   (ports-out :type 'ports :initarg :ports-out :reader ports-out) ;; can come out
   ))

(defmethod print-object ((s siphon-state) stream)
  (format stream "~A~A~A"
	  (siphon-cports s)
	  (ports-in s)
	  (ports-out s)))

(defmethod compare-object ((s1 siphon-state) (s2 siphon-state))
  (and
   (compare-object (siphon-cports s1) (siphon-cports s2))
   (compare-object (ports-in s1) (ports-in s2))
   (compare-object (ports-out s1) (ports-out s2))))

(defmethod state-final-p ((s siphon-state))
  (and
   (not (ports-empty-p (siphon-cports s)))
   (ports-empty-p (ports-in s))))

(defgeneric make-siphon-state (cports ports-in ports-out))
(defmethod make-siphon-state ((siphon-cports ports) (ports-in ports) (ports-out ports))
  (make-instance
   'siphon-state
   :siphon-cports siphon-cports
   :ports-in ports-in
   :ports-out ports-out))

(defmethod graph-ren-target (a b (s siphon-state))
  (make-siphon-state
   (ports-subst b a (siphon-cports s))
   (ports-subst b a (ports-in s))
   (ports-subst b a (ports-out s))))

(defmethod ports-of ((s siphon-state))
  (ports-union
   (ports-union
    (siphon-cports s)
    (ports-in s))
   (ports-out s)))

(defmethod graph-add-target (a b (s siphon-state))
  (let* ((siphon-cports (siphon-cports s))
	 (ports-in (ports-in s))
	 (ports-out (ports-out s))
	 (dports (ports-union ports-in ports-out)))
    (if (and (ports-member a siphon-cports)
	     (ports-member b dports))
	(make-siphon-state
	 siphon-cports
	 (ports-remove b ports-in)
	 (ports-remove b ports-out))
	(if (and (ports-member a dports) (ports-member b siphon-cports))
	    (make-siphon-state
	     siphon-cports
	     (ports-adjoin a ports-in)
	     (ports-remove a ports-out))
	    s))))

(defmethod graph-oplus-target ((p1 siphon-state) (p2 siphon-state))
  (make-siphon-state
   (ports-union (siphon-cports p1) (siphon-cports p2))
   (ports-union (ports-in p1) (ports-in p2))
   (ports-union (ports-out p1) (ports-out p2))))

(defgeneric siphon-transitions-fun (root arg))

(defmethod siphon-transitions-fun ((root vbits-constant-symbol) (arg (eql nil)))
  (let ((port (port-of root)))
    (if (= 1 (aref (vbits root) 0)) ;; in C
	(make-siphon-state
	 (make-ports (list port))
	 (make-empty-ports)
	 (make-empty-ports))
	(make-siphon-state
	 (make-empty-ports)
	 (make-empty-ports)
	 (make-ports (list port))))))

(defmethod siphon-transitions-fun ((root abstract-symbol) (arg list))
  (cwd-transitions-fun root arg #'siphon-transitions-fun))

(defun siphon-automaton (&optional (cwd 0))
  (make-cwd-automaton
   (cwd-vbits-signature cwd 1 :orientation t)
   (lambda (root states)
     (let ((*oriented* t))
       (siphon-transitions-fun root states)))
   :name (cwd-automaton-name "SIPHON-X1" cwd)))

(defgeneric basic-siphon-transitions-fun (root arg))

(defmethod basic-siphon-transitions-fun ((root vbits-constant-symbol) (arg (eql nil)))
  (let ((port (port-of root)))
    (let* ((vbits (vbits root))
	   (vbit1 (aref vbits 0))
	   (vbit2 (aref vbits 1)))
      (when (>= vbit1 vbit2) ;; X2 included in X1
	(if (= 1 vbit2) ;; in X2
	    (make-siphon-state
	     (make-ports (list port))
	     (make-empty-ports)
	     (make-empty-ports))
	    (make-siphon-state
	     (make-empty-ports)
	     (make-empty-ports)
	     (make-ports (list port))))))))

(defmethod basic-siphon-transitions-fun ((root abstract-symbol) (arg list))
  (cwd-transitions-fun root arg #'basic-siphon-transitions-fun))

(defun basic-siphon-automaton (&optional (cwd 0))
  "X1 = P, X2 included in X1 and siphon"
  (make-cwd-automaton
   (cwd-vbits-signature cwd 2 :orientation t)
   (lambda (root states)
     (let ((*oriented* t))
       (basic-siphon-transitions-fun root states)))
   :name (cwd-automaton-name "SIPHON-X1-X2" cwd)))

(defun basic-siphon-card-inf-automaton (count &optional (cwd 0))
  (rename-object
   (intersection-automata
    (list
     (subgraph-cardinality-inf-automaton count 2 2 :cwd cwd :orientation nil)
     (basic-siphon-automaton cwd)))
   (format nil "~A-SIPHON-CARD-INF-~A" cwd count)))

(defun basic-siphon-card-automaton (count &optional (cwd 0))
  (rename-object
   (intersection-automata
    (list
     (subgraph-cardinality-automaton count 2 2 :cwd cwd :orientation nil)
     (basic-siphon-automaton cwd)))
   (format nil "~A-SIPHON-CARD-~A" cwd count)))

(defun table-siphon-automaton (cwd)
  (compile-automaton (siphon-automaton cwd)))

(defun subgraph-siphon-automaton (m j &optional (cwd 0))
  (assert (< 1 j))
  (assert (<= j m))
  (rename-object
   (y1-y2-to-xj1-xj2 (basic-siphon-automaton cwd) m 1 j)
   (format nil "~A-SIPHON-X1-X~A-~A" cwd j m)))

(defun table-subgraph-siphon-automaton (m j cwd)
  (compile-automaton (subgraph-siphon-automaton m j cwd)))

(defun subgraph-siphon-card-inf-automaton (count m j &optional (cwd 0))
  (assert (< 1 j))
  (assert (<= j m))
  (rename-object
   (y1-y2-to-xj1-xj2
    (basic-siphon-card-inf-automaton count cwd) m 1 j)
   (format nil "~A-SIPHON-CARD-INF-~A-X~A-~A" cwd count j m)))

(defun exists-siphon-card-inf-automaton (count &optional (cwd 0))
  (vbits-projection (basic-siphon-card-inf-automaton count cwd) '(1)))

(defun exists-siphon-card-automaton (count &optional (cwd 0))
  (vbits-projection (basic-siphon-card-automaton count cwd) '(1)))

(defun exists-siphon-automaton (&optional (cwd 0))
  (vbits-projection (basic-siphon-automaton cwd) '(1)))

(defun siphons-transducer ()
  (vbits-projection
   (attribute-automaton
    (basic-siphon-automaton)
    *assignment-afun*)
   '(1)))

(defun siphons-of-size-transducer (n)
  (vbits-projection
   (attribute-automaton
    (basic-siphon-card-automaton n)
    *assignment-afun*)
   '(1)))

(defun siphon-sizes-transducer ()
  (vbits-projection
   (attribute-automaton
    (basic-siphon-automaton)
    (card-i-afun 2))
   '(1)))

(defgeneric siphons (term))
(defmethod siphons ((term term))
  (compute-final-value term (siphons-transducer)))

(defgeneric siphons-enumerator (term))
(defmethod siphons-enumerator ((term term))
  (final-value-enumerator term (siphons-transducer)))

(defgeneric siphons-of-size-enumerator (term size))
(defmethod siphons-of-size-enumerator ((term term) (size integer))
  (final-value-enumerator term (siphons-of-size-transducer size)))

(defgeneric siphon-sizes-enumerator (term))
(defmethod siphon-sizes-enumerator ((term term))
  (final-value-enumerator term (siphon-sizes-transducer)))
(defun siphon-size (siphon)
  (count-if (lambda (pa)
	      (= 1 (aref (termauto::pa-assignment pa) 1)))
	    siphon))

(defgeneric siphon-sizes (term))
(defmethod siphon-sizes ((term term))
  (sort 
   (delete-duplicates
    (compute-final-value term (siphon-sizes-transducer)))
   #'<))

(defgeneric siphon-sizes-verify (term))
(defmethod siphon-sizes-verify ((term term))
  (when (recognized-p term (exists-siphon-automaton))
    (siphon-sizes term)))

(defgeneric minimal-siphon-size (term))
(defmethod minimal-siphon-size ((term term))
  (let ((siphon-sizes (siphon-sizes term)))
    (reduce #'min siphon-sizes)))

(defgeneric minimal-siphons-enumerator (term))
(defmethod minimal-siphons-enumerator ((term term))
  (let ((min (minimal-siphon-size term)))
    (make-funcall-enumerator 
     (lambda (positions-assignment)
       (positions-assignment-to-etis 
	term 
	positions-assignment
	(lambda (vbits) (= (aref vbits 1) 1))))
     (siphons-of-size-enumerator term min))))

(defgeneric minimal-siphons (term))
(defmethod minimal-siphons ((term term))
  (mapcar (lambda (siphon) (sort siphon #'<))
	  (collect-enum (minimal-siphons-enumerator term))))
