;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defvar *loadable* nil)

(defclass autograph-spec (termauto-spec)
  ((graph :initform nil :initarg :graph :accessor graph)
   (graphs :initform (make-graphs) :accessor graphs)))

(defun add-graph (graph spec)
  (setf (gethash (name graph) (graphs-table (graphs spec))) graph))

(defun add-current-graph ()
  (add-graph (graph (current-spec)) (current-spec)))

(defgeneric remove-graph (a spec))
(defmethod remove-graph ((a abstract-transducer) (spec termauto-spec))
  (remhash (name a) (graphs-table (automata spec))))

(defun fill-autograph-spec (spec &key (graphs nil) (termsets nil) (automata nil) (tautomata nil) (terms nil))
  (loop for graph in graphs
     do (add-graph graph spec))
  (loop for termset in termsets
     do (add-termset termset spec))
  (loop for automaton in automata
     do (add-automaton automaton spec))
  (loop for tautomaton in tautomata
     do (add-tautomaton tautomaton spec))
  (loop for term in terms
     do (add-term term spec))
  spec)
  
(defun make-autograph-spec
    (signature
     &key (graphs nil) (termsets nil) (automata nil) (tautomata nil) (terms nil))
  (setf graphs (mapcar #'make-graph-context graphs))
  (let ((spec (make-instance 'autograph-spec
			     :name "unknown"
		 :signature signature
		 :graph (car graphs)
		 :term (car terms)
		 :termset (car termsets)
		 :automaton (car automata)
		 :tautomaton (car tautomata))))
    (fill-autograph-spec spec :graphs graphs :termsets termsets :automata automata :tautomata tautomata :terms terms)))

(defun write-graphs (spec stream)
  (maphash (lambda (key value)
	     (declare (ignore key))
	     (format stream "~A" value))
	   (graphs-table (graphs spec))))

(defmethod write-spec :after ((spec autograph-spec) stream)
  (write-graphs spec stream)
  (format stream "~%")
  )

(defun save-spec (file)
  (back-up-file (absolute-data-filename file))
  (with-open-file (foo (absolute-data-filename file) :direction :output)
    (and foo (progn (write-current-spec foo) t))))

(defun set-current-graph (graph)
  (setf (graph (current-spec)) graph)
  (add-current-graph))

