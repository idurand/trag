;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defparameter *house-edges* '((1 2) (1 3) (2 3) (2 4) (3 5) (4 5)))
(defparameter *house-noedges* '((1 5) (1 4) (2 5) (3 4)))
(defparameter *house-graph* (graph-from-earcs '((1 2) (1 3) (2 3) (2 4) (3 5) (4 5))))
(defparameter *thouse* (decomp::cwd-decomposition *house-graph*))

(defparameter *house-automaton* (graph-from-edges-automaton *house-edges*))
(defparameter *graph-paths*
  '((1 2 3 5 6 7 8 1) (2 4) (4 6) (6 8) (2 8)))

(defparameter *graph* (graph::graph-from-paths *graph-paths*))
(defparameter *tgraph* (decomp::cwd-decomposition *graph*))

(defparameter *carres-paths*
  '((1 2 3 4 1 5)))

(defparameter *carres* (graph::graph-from-paths *carres-paths*))
(defparameter *tcarres* (decomp::cwd-decomposition *carres*))

(defparameter *af* (attribute-automaton *house-automaton* *assignment-afun*))
(defparameter *p* (vbits-projection *af*))
(defparameter *e* (final-value-enumerator *tgraph* *p*))
