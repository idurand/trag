(in-package :autograph)

(defun trace-cwd-automaton (term automaton &key (prefix "") (step "|  "))
  (loop for a in (arg term)
        do (trace-cwd-automaton a automaton :step step
                                :prefix (concatenate 'string prefix step)))
  (let* ((*print-right-margin* (expt 10 10))
         (g (cwd-term-to-graph term))
         (isolated (nodes-contents (graph::graph-isolated-nodes g)))
         (edges (if (oriented-p g)
                  (graph::arcs-contents (graph-arcs g))
                  (graph::arcs-contents (graph-edges g))))
         (edges (loop for e in edges collect
                      (list (origin e) (extremity e))))
         (paths
           (labels ((describe-node
                      (n)
                      (remove nil (list (num n) (graph::label n) (weight n))))
                    (describe-path (p) (mapcar #'describe-node p)))
             (mapcar #'describe-path (append (mapcar #'list isolated)
                                             edges))))
         (paths (sort paths #'strictly-ordered-p))
         (state (compute-target term automaton)))
    (format t "~aTerm: ~a~%~aGraph: ~s~%~aState: ~s~%~aFinal: ~a~%"
            prefix term
            prefix paths
            prefix state
            prefix (final-state-p state automaton))
    state))
