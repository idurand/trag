;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;; If we use this annotation we double the execution time
;; for 4-acyclic-colorability of Petersen
;; probably because the graph-add-target is costly

(defmethod forest-transitions-fun ((root annotated-symbol) (arg list))
  (let ((symbol (symbol-of root)))
    (let ((state (forest-transitions-fun symbol arg)))
      (when (oplus-symbol-p symbol)
	(loop
	  while state
	  for pair in (container-contents (annotation root))
	  do (setq
	      state
	      (graph-add-target (first pair) (second pair) state))))
      state)))

