;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defun change-cwd-name (cwd-name old-cwd new-cwd)
  (if (= old-cwd new-cwd)
      cwd-name
      (if (zerop old-cwd)
	  (cwd-automaton-name cwd-name new-cwd)
	  (let ((parts (decompose-string cwd-name #\-)))
	    (cwd-automaton-name
	     (compose-string (cdr parts) #\-)
	     new-cwd)))))
	
(defgeneric nchange-automaton-cwd (automaton cwd)
  (:method ((automaton fly-automaton) (cwd integer))
  (let* ((signature (signature automaton))
	 (old-cwd (clique-width signature)))
    (when (= old-cwd cwd) (return-from nchange-automaton-cwd automaton))
    (setf (signature (transitions-of automaton))
	  (clique-width::fix-signature-cwd (signature automaton) cwd))
    (rename-object automaton (change-cwd-name (name automaton) old-cwd cwd)))))

(defgeneric change-automaton-cwd (automaton cwd)
  (:method ((automaton fly-automaton) (cwd integer))
    (nchange-automaton-cwd (duplicate-automaton automaton) cwd))
  (:method ((automaton table-automaton) (cwd integer))
    (warn "Cannot change the cwd of a table-automaton")
    automaton))
