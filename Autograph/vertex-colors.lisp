;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autograph)

(defvar *vertex-color-restrictions*
  "list of pairs (vertex-number . color) to fix the color of some vertices")

(setq *vertex-color-restrictions* nil)

(defun vertex-color-restrictions-p ()
  (endp *vertex-color-restrictions*))

(defun imposed-color (vertex)
  (cdr (assoc vertex *vertex-color-restrictions*)))

(defun print-vertex-colors (vertex-colors stream)
  (when vertex-colors
    (let ((vertex-color (pop vertex-colors)))
      (format stream "~A:~A" (car vertex-color) (cdr vertex-color))
      (loop 
	while vertex-colors
	do (let ((vertex-color (pop vertex-colors)))
	     (format stream ",~A:~A" (car vertex-color) (cdr vertex-color)))))))

(defun correct-color-sequence-p (colors)
  (assert colors)
  (loop
    with max = (pop colors)
    while colors
    do (let ((color (pop colors)))
	 (when (> color max)
	   (if (> (- color max) 1)
	       (return-from correct-color-sequence-p)
	       (setq max color))))
    finally (return t)))

(defun correct-vertex-colors-p (vertex-colors)
  (or
   (= 1 (caar vertex-colors))
   (correct-color-sequence-p (mapcar #'cdr vertex-colors))))
