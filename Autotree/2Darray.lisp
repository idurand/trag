;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHORS: Irène Durand

(in-package :autotree)

(defun max-width (a nl nc width legend)
  (let* ((max-value
	   (loop 
	     for i from 0 below nl
	     maximize
	     (loop for j from 0 below nc maximize (aref a i j))))
	 (max (if legend (max nl nc max-value) max-value)))
    (max width (1+ (length (format nil "~a" max))))))

(defun legend-string (maxwidth formatter nc)
  (concatenate 
   'string (string #\Newline)
   (make-string maxwidth :initial-element #\.)
   (loop
     with line := ""
     for i from 0 below nc
     do (setf line (concatenate 'string line (format nil formatter i)))
     finally (return line))
   (string #\Newline)))
    
;;; 2D arrays
(defun format-2d-natural-array (a &key (width 0) (legend nil))
  (let* ((d (array-dimensions a))
	 (nl (first d))
	 (nc (second d))
	 (maxwidth (max-width a nl nc width legend))
	 (formatter (format nil "~~~d,' d" maxwidth))
	 (res (if legend 
		  (legend-string maxwidth formatter nc)
		  (string #\Newline))))
    (loop
      for i from 0 below nl
      for leader := (if legend (format nil formatter i) "")
      for line :=
      (loop
        with s := ""
        for j from 0 below nc
        for value := (format nil formatter (aref a i j))
        do (setf s (concatenate 'string s value))
        finally (return s))
      do (setf res (concatenate 'string res leader line (string #\Newline))))
    res))

(defun ftd (a)
  (format-2d-natural-array a))

(defun extend-array (array dim)
  (loop
    with ad = (array-dimensions array)
    with nl  = (first ad)
    with nc = (second ad)
    with a = (make-array (list (max nl (first dim)) (max nc (second dim))))
    for i from 0 below nl
    do (loop
	 for j from 0 below nc
	 do (setf (aref a i j) (aref array i j)))
    finally (return a)))

(defun array-bin-op (array1 array2 bin-op)
  (let* ((ad1 (array-dimensions array1))
	 (ad2 (array-dimensions array2))
	 (nl1 (first ad1))
	 (nl2 (first ad2))
	 (nc1 (second ad1))
	 (nc2 (second ad2))
	 (nl (max nl1 nl2))
	 (nc (max nc1 nc2))
	 (dim (list nl nc)))
    (setq array1 (extend-array array1 dim))
    (setq array2 (extend-array array2 dim))
    (loop
      with new-array = (make-array dim)
      for k from 0 below nl
      do
	 (loop
	   for i from 0 below nc
	   do (setf (aref new-array k i)
		    (funcall bin-op 
			     (aref array1 k i) 
			     (aref array2 k i))))
      finally (return new-array))))

(defun array-column-sum (array i)
  (loop
    with nl = (first (array-dimensions array))
    for k from 0 below nl
    sum (aref array k i)))

(defun array-sum (array1 array2)
  (array-bin-op array1 array2 #'+))

(defun array-up (array)
  (loop
    with d = (array-dimensions array)
    with nl = (first d)
    with nc = (second d)
    with new-array = (make-array (list (1+ nl) nc))
    for l from 0 below nl
    do
       (loop
	 for c from 0 below nc
	 do (setf (aref new-array l c)
		  (aref array l c)))
    finally (return new-array)))

(defun array-down (array)
  (loop
    with d = (array-dimensions array)
    with nl = (first d)
    with nc = (second d)
    with new-array = (make-array (list (1+ nl) nc))
    for l from 1 to nl
    do
       (loop
	 for c from 0 below nc
	 do (setf (aref new-array l c)
		  (aref array (1- l) c)))
    finally (return new-array)))

(defun array-right (array)
  (loop
    with d = (array-dimensions array)
    with nl = (first d)
    with nc = (second d)
    with new-array = (make-array (list nl (1+ nc)))
    for l from 0 below nl
    do
       (loop
	 for c from 1 below nc
	 do (setf (aref new-array l c)
		  (aref array l (1- c))))
    finally (return new-array)))

(defun array-left (array)
  (loop
    with d = (array-dimensions array)
    with nl = (first d)
    with nc = (second d)
    with new-array = (make-array (list nl (1+ nc)))
    for l from 0 below nl
    do
       (loop
	 for c from 0 below nc
	 do (setf (aref new-array l c)
		  (aref array l c)))
    finally (return new-array)))

(defun array-vertical-symmetry (array)
  (loop
    with d = (array-dimensions array)
    with nl = (first d)
    with nc = (second d)
    with new-array = (make-array (list nl nc))
    for l from 0 below nl
    do (loop
	 for c from 0 below nc
	 do (setf (aref new-array l (- nc c 1))
		  (aref array l c)))
	 finally (return new-array)))

(defun array<= (array1 array2)
  (loop
    with d = (array-dimensions array1)
    with nl = (first d)
    with nc = (second d)
    for l from 0 below nl
    do (loop
	 for c from 0 below nc
	 when (> (aref array1 l c) (aref array2 l c))
	   do (return-from array<=))
    finally (return t)))
	     
(defun array< (array1 array2)
  (and (not (array= array1 array2))
       (array<= array1 array2)))

(defun array= (array1 array2)
  (equalp array1 array2))

(defun array-abs-diff (array1 array2)
  (array-bin-op 
		array1 array2 (lambda (e1 e2) (abs (- e1 e2)))))
