(in-package :autotree)

;;; 2D arrays of coefficients obtained from atriples
(defvar *factorial-cache* (make-hash-table :test 'equal))

(defun factorial (n)
  (let
    ((cached (gethash n *factorial-cache*)))
    (or cached
        (setf (gethash n *factorial-cache*)
              (if (<= n 1) 1
                (* n (factorial (1- n)))))))) 

(defvar *binomial-cache* (make-hash-table :test 'equal))

(defun binomial (n k)
  (let
    ((cached (gethash (list n k) *binomial-cache*)))
    (or cached
        (setf (gethash (list n k) *binomial-cache*)
              (or
                (loop
                  for j from 1 to k
                  for l downfrom n
                  for res := (/ l j) then (* res (/ l j))
                  finally (return res))
                1)))))
