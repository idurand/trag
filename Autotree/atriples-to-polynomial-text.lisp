(in-package :autotree)

(defun atriples-to-polynomial-text (at &rest names)
  (format
    nil "0 ~{+(~{~a~{*(~{(~a)^(~a)~})~}~})~}"
    (loop
      for entry in at
      collect
      (list
        (attribute-of entry)
        (map 'list 'list names (object-of entry))))))
