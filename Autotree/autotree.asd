;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Autotree.
(in-package :asdf-user)

(defsystem :autotree
  :description "Autotree: Automata and Graphs"
  :name "autotree"
  :version "6.0"
  :author "Irene Durand <idurand@labri.fr>"
  :depends-on (:tree :termauto :cl-who)
  :serial t
  :components
  (
   (:file "package") 
   (:file "tree-symbols") 
   (:file "tree-signature") 
   (:file "tree-term")
   (:file "tree-terms")
   (:file "triples")
   (:file "tittman-afun")
   (:file "tree-automata")
   (:file "tittman-automata")
   (:file "arithmetic")
   (:file "2Darray")
   (:file "coefficients-array")
   (:file "transducer")
   (:file "q-automata")
   (:file "r-automata")
   (:file "carray")
   (:file "enumeration")
   (:file "experimentation")
   (:file "b1-b2-b3")
   (:file "extract-graph-properties")
   (:file "atriples-to-polynomial-text")
   (:file "report")
   )
  :serial t)

(pushnew :autotree *features*)
