;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

(defparameter *tb1* (tittman-b1-transducer))
(defparameter *tb2* (tittman-b2-transducer))
(defparameter *tb3* (tittman-b3-transducer))

(defun test-b1 (n)
  (multiple-value-bind (tree1 tree2)
	    (trees-not-root-equivalent n)
	 (let* ((term1 (tree-to-tree-term tree1))
		(term2 (tree-to-tree-term tree2))
		(v1 (compute-final-value term1 *tb1*))
		(v2 (compute-final-value term1 *tb2*)))
	   (values tree1 tree2 term1 term2 (container-difference v1 v2) (container-difference v2 v1)))))

(defun test-b1-b2-b3 (n &optional (nb-times 1000))
  (loop
     repeat nb-times
     do (multiple-value-bind (tree1 tree2)
	    (trees-not-root-equivalent n)
	 (let ((term1 (tree-to-tree-term tree1))
	       (term2 (tree-to-tree-term tree2)))
	   (when (and
		  (container-equal-p
		   (compute-final-value term1 *tb1*)
		   (compute-final-value term2 *tb1*))
		  (container-equal-p
		   (compute-final-value term1 *tb2*)
		   (compute-final-value term2 *tb2*))
		  (container-equal-p
		   (compute-final-value term1 *tb3*)
		   (compute-final-value term2 *tb3*)))
	     (setq *tree1* tree1)
	     (setq *tree2* tree2)
	     (setq *term1* term1)
	     (setq *term2* term2)
	     (return-from test-b1-b2-b3))))
     do (format *error-output* ".")))

