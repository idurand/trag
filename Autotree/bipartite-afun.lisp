;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

(defclass bipartite-container (ordered-multi-container)
  ())

(defun make-bipartite-container (lvbits)
  (make-multi-container-generic
   lvbits
   'bipartite-container
   #'equalp
   #'+))

(defmethod container-order-fun ((container bipartite-container)) #'vector<)

(defgeneric bipartite-symbol-fun-body (f-edge attributes))
(defmethod bipartite-symbol-fun-body ((f-edge integer) (attributes list))
  (let ((attribute
	  (attributes-symbol-fun-body attributes (multi-object-fun #'vector-sum #'*))))
    (if (zerop f-edge)
	attribute
	(let ((v (make-triple 3 (list 0 0 1))))
	  (container-mapcar-to-container
	   (lambda (melement)
	     (make-object-multi
	      (vector-sum (object-of melement) v)
	      (attribute-of melement)))
	   attribute)))))
  
(defgeneric bipartite-symbol-fun (s))
(defmethod bipartite-symbol-fun ((s color-non-constant-symbol))
  (lambda (&rest attributes)
    (bipartite-symbol-fun-body (color s) attributes)))

(defmethod bipartite-symbol-fun ((s color-constant-symbol))
  (let* ((color (color s))
	 (x (boolean-to-bit (= 1 color)))
	 (y (boolean-to-bit (= 2 color))))
    (lambda ()
      (make-bipartite-container
       (list
	(make-triple (list x y 0)))))))

(defvar *bipartite-afun*)
(setq *bipartite-afun*
      (make-afuns #'bipartite-symbol-fun (container-union-fun #'make-bipartite-container
							      "Bipartite")))
