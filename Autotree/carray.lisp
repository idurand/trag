(in-package :autotree)

(defun copy-variable-carray (carray)
  (loop
    with nv = (carray-nv carray)
    with c = (make-carray nv)
    for k from 0 to (1- nv)
    do (loop for i from 0 to nv
	     do (setf (aref c k i)
		      (copy-tree (aref carray k i))))
    finally (return c)))

(defun evaluable (tree)
  (if (atom tree)
      (numberp tree)
      (every #' evaluable (cdr tree))))

(defun nreplace-var (carray value variable)
  (loop 
    for k from 0 below (carray-nv carray)
    do (loop for i from 0 to (carray-nv carray)
	     do (let ((new (subst value variable (aref carray k i))))
		  (when (evaluable new) (setq new (eval new)))
		  (setf (aref carray k i) new)))
    finally (return carray)))

(defun replace-var (carray value variable)
  (nreplace-var (copy-variable-carray carray) value variable))

(defun replace-x (carray value)
  (replace-var carray value 'x))

(defun replace-vars (carray env)
  (loop
    with c = (copy-variable-carray carray)
    for pair in env
    do (nreplace-var c (cdr pair) (car pair))
    finally (return c)))

(defgeneric carray-from (tree transducer))

(defmethod carray-from ((tree tree) transducer)
  (carray-from (tree-to-tree-term tree) transducer))

(defmethod carray-from ((tree-term term) transducer)
  (atriples-to-carray (transducer-atriples tree-term transducer)))

(defmethod carray-from ((sexpr list) transducer)
  (carray-from (sexpr-to-tree sexpr) transducer))

(defgeneric tree-rxs (tree))
(defmethod tree-rxs ((tree tree))
  (loop
    with pairs = '()
    for n from 1 to (tree-nb-nodes tree)
    do (let* ((tt (tree::root-change tree n))
	      (rx (carray-from tt (rx-transducer)))
	      (pair (assoc rx pairs :test #'equalp)))
	 (if pair
	     (push tt (second pair))
	     (push (list rx (list tt)) pairs)))
    finally (return pairs)))

(defmethod tree-rxs ((sexpr list))
  (tree-rxs (tree::sexpr-to-tree sexpr)))

(defparameter *t8-1* (sexpr-to-tree-term '(1 (2 3 4 5 6) 7 8)))
(defparameter *t7-1* (sexpr-to-tree-term '(1 (2 3 4 5 6) 7))) 
(defparameter *t8-2* (sexpr-to-tree-term '(1 (2 3 4) 5 6 7 8)))
(defparameter *t7-2* (sexpr-to-tree-term '(1 (2 3 4) 5 6 7)))
(defparameter *rxa1* (carray-from *t7-1* (rx-transducer)))
(defparameter *rxa2* (carray-from *t7-2* (rx-transducer)))
(defparameter
	*prax*
  (make-array
   '(7 8) :initial-contents 
   '(
     (0 0 0 0 0 0 0 1)
     ( 0 0    0      1      0      0    5  0 )
     ( 0  0  x  0  (- 5 x)   10    0  0 )
     ( 0  1   0    (+ x 4)   10    (- 5 x)  0  0 )
     ( 0  0  (- 5 x)    5    (+ x 4)    0    1  0 )
     ( 0  0    1    (- 5 x)    0      x    0  0 )
     ( 0  0    0      0      1      0    0  0 ))))

(defparameter
	*car*
  (make-array
   '(7 8) :initial-contents 
   '(
     (0 0 0 0 0 0 0 1)
     (0 0 X0 0 (- 1 X0) 0 5 0)
     (0 X2 0 (+ X0 3) 0 (- 12 X2 X0) 0 0)
     (0 0 (+ (- (+ X2 4) X1)) 0 (+ X1 15) 0 (- 1 X2) 0)
     (0 (- 1 X2) 0 (- 12 X1) 0 (+ X2 X1 2) 0 0)
     (0 0 (- (+ X1 2) X2 X0) 0 (- (+ X0 4) X1) 0 X2 0)
     (0 0 0 (- X1 X0) 0 (- (+ X0 1) X1) 0 0))))

(defparameter
	*car-x2-1*
  (make-array
   '(7 8) :initial-contents 
   '(
     (0 0 0 0 0 0 0 1)
     (0 0 X0 0 (- 1 X0) 0 5 0)
     (0 0 0 (+ X0 3) 0 (- 12 0 X0) 0 0)
     (0 0 2 0 17 0 1 0)
     (0 1 0 10 0 4 0 0)
     (0 0 (- (+ 2 2) 0 X0) 0 (- (+ X0 4) 2) 0 0 0)
     (0 0 0 (- 2 X0) 0 (- (+ X0 1) 2) 0 0))))

;; (replace-vars *car-x2-0* '((X0 . 0) (X1 . 0))) good t7-2
(defparameter
	*car-X2-0*
  (make-array
   '(7 8) :initial-contents 
   '((0 0 0 0 0 0 0 1)
     (0 0 X0 0 (- 1 X0) 0 5 0)
     (0 0 0 (+ X0 3) 0 (- 12 0 X0) 0 0)
     (0 0 (+ (- 0 X1) 4) 0 (+ X1 15) 0 1 0)
     (0 1 0 (- 12 X1) 0 (+ 0 X1 2) 0 0)
     (0 0 (- (+ X1 2) 0 X0) 0 (- (+ X0 4) X1) 0 0 0)
     (0 0 0 (- X1 X0) 0 (- (+ X0 1) X1) 0 0))))
