;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

;;; 2D arrays of coefficients obtained from atriples
(defun make-carray (nv) (make-array (list nv (1+ nv))))

(defun carray-nv (ca) (1- (second (array-dimensions ca))))

(defun atriples-to-carray (rat)
  (let*
      ((nv (1+ (loop for x in rat maximize (elt (object-of x) 2))))
       (res (make-carray nv)))
    (select-atriples (u v e n) rat t
      (setf (aref res e u) n))
    res))

(defun array-to-atriples (carray)
  (loop
    with nv = (carray-nv carray)
    for k from 0 to (1- nv)
    nconc 
    (loop for i from 0 to nv
	  when (plusp (aref carray k i))
	    collect (make-attributed-object
		     (make-triple (list i (- nv i) k))
		     (aref carray k i)))))

(defun carray-shift (carray l c)
  (loop
    with nv = (carray-nv carray)
    with new-carray = (make-carray (1+ nv))
    for k from l to (- nv l)
    do (loop for i from c to (- nv c)
	     do (setf (aref new-carray k i)
		      (aref carray (- k l) (- i c))))
    finally (return new-carray)))

(defun minimize-two-carrays (ca1 ca2)
  (let* ((nv (carray-nv ca1))
	 (ca (make-carray nv)))
    (loop
      for i from 0 to (1- nv)
      do (loop
	   for j from 0 to nv
	   do (setf (aref ca i j) (min (aref ca1 i j) (aref ca2 i j)))))
    ca))
      
(defun minimize-carrays-list (cas)
  (reduce #'minimize-two-carrays cas))

(defun minimize-carrays-list-bis (cas)
  (let* ((ca1 (first cas))
	 (nv (carray-nv ca1))
	 (ca (make-carray nv)))
    (loop
      for i from 0 to (1- nv)
      do (loop
	   for j from 0 to nv
	   do (setf (aref ca i j)
		    (loop for carray in cas
			  minimize (aref carray i j)))))
    ca))
    
(defun rx-to-ry (rx) (array-vertical-symmetry rx))

(defun carray-sum (carray1 carray2)
  (array-sum carray1 carray2))

(defun rx-to-r (rx)
  (carray-sum rx (rx-to-ry rx)))

(defun rxa-to-rx (rxa)
  (carray-sum (carray-shift rxa 1 0) (carray-shift rxa 0 1)))

(defun rxa-to-ry (rxa)
  (rx-to-ry (rxa-to-rx rxa)))

(defun rxa-to-r (rxa)
  (rx-to-r (rxa-to-rx rxa)))

(defun sum-diago1 (carray j)
  (loop
    with nv = (carray-nv carray)
    for k from j to (1- nv)
    sum (aref carray k (- k j))))

(defun sum-diago2 (carray j)
  (loop
    with nv = (carray-nv carray)
    for k from j to (1- nv) 
    sum (aref carray k (+ (- nv k) j) )))

(defun carray-sum-line (carray k)
  (loop
    for i from 0 to (carray-nv carray)
    sum (aref carray k i)))

(defun carray-sum-column (carray i)
  (loop
    for k from 0 below (carray-nv carray)
    sum (aref carray k i)))

(defun check-carray-line (carray k)
  (= (carray-sum-line carray k)
     (binomial (carray-nv carray) k)))

(defun check-carray-column (carray i)
  (= (carray-sum-column carray i) (binomial (carray-nv carray) i)))

(defun carray-line-list (carray k)
  (loop for i from 0 to (carray-nv carray)
	collect (aref carray k i)))

(defun cx-max-degree (cx)
  (let ((nv (carray-nv cx)))
    (loop for k from (1- nv) downto 0
	  when (or (plusp (aref cx k 1)) (plusp (aref cx k (- nv 1))))
	    do (return k))))

(defun rr-max-degree (rr)
  (let ((nv (carray-nv rr)))
    (loop for k from (1- nv) downto 0
	  when (plusp (aref rr k 1))
	    do (return k))))

(defun rr-l (rr)
  (let ((nv (carray-nv rr)))
    (loop for i from 1 to (floor (/ nv 2))
	  when (plusp (aref rr (1- nv) i))
	    do (return i))))

(defun rx-l (rx)
  (let ((nv (carray-nv rx)))
    (loop for i from 1 to nv
	  when (= 1 (aref rx (1- nv) i))
	    do (return i))))

(defun cx-nb-leaves (cx)
  (aref cx 1 (1- (carray-nv cx))))
	  
(defun cx-root-degree (cx)
  (let ((nv (carray-nv cx)))
    (loop for k from (1- nv) downto 0
	  when (plusp (aref cx k 1))
	    do (return k))))

(defun cx-lx (cx)
  (let ((nv (carray-nv cx)))
    (loop for i from 0 to nv
	  when (plusp (aref cx (1- nv) i))
	    do (return i))))

(defun carray-product (ca1 ca2)
  (let* ((nv1 (carray-nv ca1))
	 (nv2 (carray-nv ca2))
	 (nv (+ nv1 nv2))
	 (ca (make-carray nv))
	 (ne1 (1- (first (array-dimensions ca1))))
	 (ne2 (1- (first (array-dimensions ca2)))))
    (loop
      for k from 0 to (1- nv)
      do (loop
	   for i from 0 to nv
	   do (setf 
	       (aref ca k i)
	       (loop
		 for l from (max 0 (- k ne2)) to (min ne1 k)
		 sum
		 (loop
		   for j from (max 0 (- i nv2)) to (min nv1 i)
		   sum (* (aref ca1 l j) (aref ca2 (- k l) (- i j)))))))
      finally (return ca))))

(defun format-xki (k i ca1 ca2)
  (let* ((nv1 (carray-nv ca1))
	 (nv2 (carray-nv ca2))
	 (ne1 (1- (first (array-dimensions ca1))))
	 (ne2 (1- (first (array-dimensions ca2)))))
  (loop
    for l from (max 0 (- k ne2)) to (min ne1 k)
    do
       (print l)
    (loop
      for j from (max 0 (- i nv2)) to (min nv1 i)
      do (format t "x^a_~A_~A * x^b_~A_~A + " l j (- k l) (- i j))))))

(defun rx-to-z (rx)
  (array-down (rx-to-ry rx)))

(defun rx-add-edge (rx)
  (array-sum (array-up rx) (rx-to-z rx)))

;;(array-down (rx-to-ry rx))))

(defun edge-rx-product (rxa rxb)
  (carray-product rxa (rx-add-edge rxb)))

(defun node-rx-product (rxa rxb)
  (edge-rx-product
   (edge-rx-product #2A((0 1)) rxa)
   rxb))

(defun node-rx-product-gen (rxs)
  (assert rxs)
  (if (endp (cdr rxs))
      (car rxs)
      (let ((r (node-rx-product (pop rxs) (pop rxs))))
	(if rxs
	    (edge-rx-product
	     r
	     (node-rx-product-gen rxs))
	    r))))
