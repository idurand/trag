;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

(defun apply-square-enumerator (e1 e2)
  (make-binary-diagonal-product-enumerator e1 e2
   :fun
   (lambda (t1 t2)
     (tree-term-square nil t1 t2))))

(defun tree-term-enumerator1 (nb-leaves)
  (if (= 1 nb-leaves)
      (make-list-enumerator
       (list (tree-term-vertex nil)))
      (let ((eis (make-array nb-leaves)))
	(loop
	  for i from 1 below nb-leaves
	  do (setf (aref eis i) (tree-term-enumerator1 i)))
	(make-sequential-enumerator
	 (loop
	   for i from 1 below nb-leaves
	   when (<= i (- nb-leaves i))
	   collect (apply-square-enumerator (aref eis i) (aref eis (- nb-leaves i))))))))

(defun tree-term-enumerator2 (nb-leaves)
  (if (= 1 nb-leaves)
      (make-list-enumerator
       (list (tree-term-vertex nil)))
      (let ((eis (make-array nb-leaves)))
	(loop
	  for i from 1 below nb-leaves
	  do (setf (aref eis i) (tree-term-enumerator2 i)))
	(make-sequential-enumerator
	 (loop
	   for i from 1 below nb-leaves
	   when (>= i (- nb-leaves i))
	     collect (apply-square-enumerator (aref eis i) (aref eis (- nb-leaves i))))))))

(defun tree-term-enumerator (nb-nodes)
  "enumerates all terms corresponding to all trees with nb-nodes"
  (if (= 1 nb-nodes)
      (make-list-enumerator
       (list (tree-term-vertex nil)))
      (let ((eis (make-array nb-nodes)))
	(loop
	  for i from 1 below nb-nodes
	  do (setf (aref eis i) (tree-term-enumerator i)))
	(make-sequential-enumerator
	 (loop
	   for i from 1 below nb-nodes
	   collect (apply-square-enumerator (aref eis i) (aref eis (- nb-nodes i))))))))

(defun tree-terms-enumerator (nb-leaves)
  (make-filter-enumerator
   (make-binary-diagonal-product-enumerator
     (tree-term-enumerator nb-leaves)	     
     (tree-term-enumerator nb-leaves))
   (lambda (pair)
     (let ((term1 (first pair))
	   (term2 (second pair)))
       (and
	(not (compare-object term1 term2))
	(let ((tr1 (tree-term-to-tree term1))
	      (tr2 (tree-term-to-tree term2)))
	  (tree::ds-equivalent-p tr1 tr2)))))))

(defun unrooted-tree-term-enumerator (nb-leaves)
  (make-no-duplicates-enumerator (tree-term-enumerator nb-leaves) :test #'root-change-equivalent-p))

(defun unordered-tree-enumerator (nb-leaves)
  (make-no-duplicates-enumerator
   (make-funcall-enumerator
    (lambda (tree-term)
       (tree-term-to-tree tree-term))
    (tree-term-enumerator nb-leaves))
   :test #'tree::utree=))

(defun unrooted-unordered-tree-enumerator (nb-leaves)
   (make-no-duplicates-enumerator 
    (unordered-tree-enumerator nb-leaves)
    :test #'root-change-equivalent-p))

(defun balanced-tree-enumerator (nb-leaves)
  (make-no-duplicates-enumerator
   (make-funcall-enumerator
   (lambda (tree-term)
     (tree::standardize-tree
      (tree::balance-tree (tree-term-to-tree tree-term))))
   (unrooted-tree-term-enumerator nb-leaves))
   :test #'root-change-equivalent-p))

(defun balanced-utree-enumerator (nb-leaves)
  (make-no-duplicates-enumerator
   (make-funcall-enumerator
   (lambda (tree-term)
     (tree::balance-tree (tree-term-to-tree tree-term)))
   (unrooted-tree-term-enumerator nb-leaves))
   :test #'tree::utree=))

;;; change so that it does not enumerate the initial tree!
(defgeneric root-change-tree-term-enumerator (tree))
(defmethod root-change-tree-term-enumerator ((tree-term term))
  (make-no-duplicates-enumerator
   (make-funcall-enumerator
    #'tree-to-tree-term
    (root-change-tree-enumerator (tree-term-to-tree tree-term)))))

