;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)


(defvar *tested-terms*)
(setq *tested-terms* nil)
(defvar *nb-tested-terms*)
(setq *nb-tested-terms* 0)

(defvar *save-terms*)
(setq *save-terms* t)

(defgeneric compute-term-value (tree-term transducer))
(defmethod compute-term-value ((tree-term term) transducer)
  (when *save-terms*
    (assert (not (member tree-term *tested-terms* :test #'compare-object)))
    (push tree-term *tested-terms*))
  (compute-final-value tree-term transducer))

(defgeneric test-one-term (tree-term transducer))
(defmethod test-one-term ((tree-term term) transducer)
  (unless (member tree-term *tested-terms* :test #'compare-object)
    (let ((res (compute-term-value tree-term transducer)))
      (loop
	with e = (root-change-tree-term-enumerator tree-term)
	while (next-element-p e)
	for tr2 = (next-element e) ; then (next-element e)
	unless (or (compare-object tree-term tr2) (member tree-term *tested-terms* :test #'compare-object))
	  do 
	     (unless (container-equal-p 
		      res
		      (compute-term-value tr2 transducer))
	       (return-from test-one-term (list tree-term tr2 ))))
      (incf *nb-tested-terms*))))


(defun test-all-terms (nb-leaves transducer)
  (loop
    with e1 = (tree-term-enumerator nb-leaves)
    while (next-element-p e1)
    do (let* ((tree-term (next-element e1)))
	 (let ((res (test-one-term tree-term transducer)))
	   (when res
	     (return-from test-all-terms res))))))

(defun test-one-random-term (nb-leaves tt)
  (test-one-term (tree-term-random nb-leaves) tt))

(defun test-random-tittman (nb-leaves tt &key (nb-times 10000) (save nil))
  (let ((*nb-tested-terms* 0)
	(*save-terms* save))
    (loop
      repeat nb-times
      do (let ((res (test-one-random-term nb-leaves tt)))
	   (when res (return-from test-random-tittman res))))
    *nb-tested-terms*))

(defun stream-load-tree-terms (stream)
  (loop
    for line = (read-line stream nil)
    until (null line)
    collect (input-term line)))

(defun load-tree-terms (nb-nodes)
  (let* ((directory (concatenate 'string (home-directory) "/trag/Autotree/Tested-Terms/"))
	 (file (absolute-filename 
		directory
		(format nil "~A-tested-terms.lisp" nb-nodes))))
    (with-open-file (in file)
      (stream-load-tree-terms in))))

(defun stream-save-tree-terms (terms stream)
  (loop
    for term in terms
    do (format stream "~a~%" term)))

(defun save-tree-terms (terms &optional nb-nodes)
  (unless nb-nodes
    (setq nb-nodes (term-nb-leaves (car terms))))
  (let* ((directory (concatenate 'string (home-directory) "/trag/Autotree/Tested-Terms/"))
	 (file  (absolute-filename 
		 directory
		 (format nil "~A-tested-terms.lisp" nb-nodes)))
	 (previous (load-tree-terms nb-nodes)))
    (setq terms (union terms previous :test #'compare-object))
    (with-open-file (in file :direction :output :if-does-not-exist :create :if-exists :rename-and-delete)
      (stream-save-tree-terms terms in))))

(defparameter *tt* (tittman-transducer))
(defparameter *tittman-basic* (tittman-basic-automaton))
(defparameter *r* nil)
(defun test-nb-leaves (nb-leaves)
  (with-time 
      (let
	  ((*tested-terms* nil)
	   (*nb-tested-terms* nil)
	   (*r* (test-all-terms nb-leaves *tt*)))
	(save-tree-terms *tested-terms* nb-leaves)
	(if *r* 'eureka (length *tested-terms*)))))

(defun test-nb-leaves-dont-save (nb-leaves tt)
  (with-time
      (let ((*tested-terms* nil)
	    (*save-terms* nil)
	    (*nb-tested-terms* 0))
	(setq *r* (test-all-terms nb-leaves tt))
	(or *r* *nb-tested-terms*))))

(defparameter *term1* nil)
(defparameter *term2* nil)
(defparameter *tree1* nil)
(defparameter *tree2* nil)

(defun test-tittman (n tt &optional (nb-times 1000))
  (loop
    repeat nb-times
    do (multiple-value-bind (tree1 tree2)
	   (trees-not-root-equivalent n)
	 (let ((term1 (tree-to-tree-term tree1))
	       (term2 (tree-to-tree-term tree2)))
	 (when (container-equal-p
		(compute-final-value term1 tt)
		(compute-final-value term2 tt))
	   (setq *tree1* tree1)
	   (setq *tree2* tree2)
	   (setq *term1* term1)
	   (setq *term2* term2)
	   (return-from test-tittman (values term1 term2)))))
    do (format *error-output* ".")))

(defun caterpillar-tittman (n tt &optional (nb-times 1000))
  (loop
    repeat nb-times
    do (multiple-value-bind (tree1 tree2)
	   (caterpillars-not-root-equivalent n)
	 (let ((term1 (tree-to-tree-term tree1))
	       (term2 (tree-to-tree-term tree2)))
	 (when (container-equal-p
		(compute-final-value term1 tt)
		(compute-final-value term2 tt))
	   (setq *tree1* tree1)
	   (setq *tree2* tree2)
	   (setq *term1* term1)
	   (setq *term2* term2)
	   (return-from caterpillar-tittman (values term1 term2)))))
    do (format *error-output* ".")))
