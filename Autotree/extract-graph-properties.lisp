(in-package :autotree)

(defun degree-sequence (qat)
  (let*
    ((single-red (select-atriples (u v e n) qat (= u 1)))
     (max-degree (loop for x in single-red maximize (elt (object-of x) 1)))
     (monomial-sequence (make-array (1+ max-degree) :initial-element 0))
     (degree-sequence (make-array (1+ max-degree) :initial-element 0)))
    (loop
      for x in single-red
      for deg := (elt (object-of x) 1)
      for count := (attribute-of x)
      do (setf (elt monomial-sequence deg) count))
    (loop
      for deg downfrom max-degree to 1
      for count := (elt monomial-sequence deg)
      for shadow :=
      (loop
        for j from (1+ deg) to max-degree
        sum (* (binomial j deg) (elt degree-sequence j)))
      do (assert (>= count shadow))
      do (setf (elt degree-sequence deg) (- count shadow)))
    degree-sequence))

(defun lowest-sn (qat)
  (let*
    ((connected-sequence 
       (sort
         (select-atriples 
           (u v e n) qat 
           (and
             (<= (+ u v) (1+ e))
             (>= u v))
           (if (and (= u v) (> u 0))
             (list (+ u v) (/ n 2))
             (list (+ u v) n)))
         '> :key 'first))
     (graph-size (first (first connected-sequence)))
     (connected-vector (make-array (1+ graph-size) :initial-element 0))
     (degree-sequence (degree-sequence qat))
     (leaf-count (elt degree-sequence 1)))
    (loop
      for x in connected-sequence
      do (incf (elt connected-vector (first x)) (second x)))
    (loop
      for k from 0 to graph-size
      unless (= k 1)
      do (assert (not (zerop (elt connected-vector k)))))
    (loop
      for s downfrom graph-size
      for n := (elt connected-vector s)
      for leaf-trim := (binomial leaf-count (- graph-size s))
      when (> n leaf-trim)
      return (values (- graph-size s 1) (- n leaf-trim)))))

(defun leaf-colour-split (qat)
  (let*
    ((degree-sequence (degree-sequence qat))
     (graph-size (reduce '+ degree-sequence))
     (max-red
       (first
         (select-atriples
           (u v e n) qat
           (and
             (= (+ u v) graph-size)
             (= (1+ e) graph-size)
             (>= u v))
           (progn 
             (assert (= n (if (= u v) 2 1))) 
             u))))
     (min-blue-max-red
       (reduce 'min
               (select-atriples
                 (u v e n) qat
                 (and
                   (= u max-red)
                   (= (1+ e) (+ u v)))
                 v)))
     (blue-leaves (- graph-size max-red min-blue-max-red))
     (red-leaves (- (elt degree-sequence 1) blue-leaves)))
    (if (= graph-size 2) (values 1 1)
      (values red-leaves blue-leaves))))

(defun transducer-summary-from (s transducer &key legend name)
  (format 
    nil "~a~a~%~a~%"
    (if name (format nil "~a: " name) "")
    s 
    (format-2d-natural-array
      (carray-from s transducer)
      :legend legend)))

(defun r-summary-for-sexpr (s &key legend name)
  (transducer-summary-from s (r-transducer) :legend legend :name name))

(defun tree-term-to-dot-svg (term &key dot-parameters)
  (with-output-to-string (svg)
    (with-input-from-string
      (dot
        (with-output-to-string (write-dot)
          (graph-to-dot
            (graph-from-tree-term term)
            write-dot)))
      (uiop:run-program
        (append
          (list "dot" "-Tsvg")
          dot-parameters)
        :input dot
        :output svg
        :error-output t))))

(defun html (tag attributes &rest content)
  (format
    nil
    "<~a~{ ~{~a=\"~a\"~}~}>~{~a~#[~:; ~]~}</~a>"
    tag attributes
    content
    tag))

(defun svg-initial-width (svg)
  (multiple-value-bind
	(ok w)
      (cl-ppcre:scan-to-strings
       "width=\"([0-9]+[.]?[0-9]*)pt\"" svg)
    (and ok (read-from-string (elt w 0)))))

(defun compute-svg (tree-term svg-scale)
  (let* ((svg (tree-term-to-dot-svg tree-term))
	 (svg-initial-width (svg-initial-width svg))
	 (svg-target-width (and svg-initial-width svg-scale (* svg-scale svg-initial-width))))
    (when svg-target-width
      (setq svg
            (cl-ppcre:regex-replace
              "( (width|height)=\"[0-9.]+pt\")+"
              svg
              (format nil " width=\"~fpt\"" svg-target-width))))
    svg))

(defun html-transducer-summary-from (s transducer &key (name "") file minwidth top-style (skip-svg) (svg-scale))
  (let*
    (
     (term (if (typep s 'term) s (sexpr-to-tree-term s)))
     (svg (compute-svg term svg-scale))
     (a-full (carray-from term transducer))
     (d (array-dimensions a-full))
     (arrays (list a-full))
     (ne (1- (first d)))
     (nv2 (1- (second d)))
     (html
       (html
         :div `(,@(when top-style `((:style ,top-style))))
         (if skip-svg
           ""
           (html 
             :div `((:style "float: left;")) 
             svg))
         (apply
           'concatenate 'string
           (loop
             for a-current in arrays
             collect
             (html 
               :div `((:style "float: left; padding: 0.5em; ")) 
               (html
                 :table `((:rules "all") (:border "all"))
                 (html
                   :thead ()
                   (html :th () name)
                   (apply
                     'concatenate 'string
                     (loop
                       for j from 0 to nv2
                       collect
                       (html :th `(,@(when minwidth
                                       `((:style ,(format nil "min-width: ~a" minwidth))))) j)))
                   )
                 (html
                   :tbody ()
                   (apply
                     'concatenate 'string
                     (loop
                       for i from 0 to ne
                       collect
                       (html
                         :tr ()
                         (html 
                           :td ()
                           (html :b () i))
                         (apply
                           'concatenate 'string
                           (loop
                             for j from 0 to nv2
                             collect
                             (html :td () (aref a-current i j))
                             )))
                       )))
                 ))))
         ))
     )
    (when file
      (with-open-file (f file :direction :output :if-exists :supersede)
        (format f "~a" html)))
    html))

(defun html-r-summary-for-sexpr (s &key (name "") file minwidth top-style (skip-svg) (svg-scale))
  (html-transducer-summary-from s (r-transducer) 
				     :name name
				     :file file
				     :minwidth minwidth
				     :top-style top-style
				     :skip-svg skip-svg
				     :svg-scale svg-scale))

(defun html-transducer-multi-summary (named-entries transducer &key attributes file)
  (let*
    ((res
       (apply
         'html :div ()
         (loop
           for e in named-entries
           collect
           (apply 'html-transducer-summary-from
                  (first e) transducer :name (second e) 
                  (append
                    (let
                      ((s (third e)))
                      (cond
                        ((not s) nil)
                        ((numberp s) (list :svg-scale s))
                        (t (list :skip-svg t))))
                    attributes))))))
    (if file
      (with-open-file (f file :direction :output :if-exists :supersede)
        (format f "~a" res))
      res)))

(defun html-r-multi-summary (named-entries &key attributes file)
  (html-transducer-multi-summary named-entries (r-transducer) :attributes attributes :file file))
