(in-package :autotree)

(defun verify-first-line-rx (rx)
  (let* ((nv (carray-nv rx))
	 (middle (floor (/ nv 2))))
    (loop 
      for i from 1 below middle
      unless (zerop (aref rx 1 i))
	do (return))
    (if (evenp nv)
	(<= (aref rx 1 middle) 1)
	(<= (aref rx 1 middle) 2))))

(defun test-rr (n)
  (let ((e (balanced-tree-enumerator n)))
    (with-open-file (*html-stream* "/tmp/r.html" :direction :output :if-exists :supersede :if-does-not-exist :create)
      (loop
	do (multiple-value-bind (tree found) (call-enumerator e)
	     (unless found (return t))
	     (html-tree tree (list (r-transducer) (rx-transducer)) "" nil 0.3)
	     (unless (verify-first-line-rx (carray-from tree (rx-transducer)))
	       (html-tree tree (list (r-transducer) (rx-transducer)) "" nil 0.3)
	       (return)))))))

(defun test-extrem-lines (tree)
  (let ((rtrees (collect-enum 
		 (make-no-duplicates-enumerator
		  (make-funcall-enumerator
		   #'tree::balance-tree
		   (root-change-tree-enumerator tree))
		  :test #'tree::utree=))))
    (= (length rtrees)
       (length 
	(remove-duplicates
	 rtrees :test #'compare-rx-extrem-lines)))))

(defun test-lines (n)
  (let ((urtrees (collect-enum (balanced-utree-enumerator n))))
    (loop
      for urtree1 in urtrees
      do (loop
	   for urtree2 in urtrees
	   unless (eq urtree1 urtree2)
	     do (when (compare-rx-extrem-lines urtree1 urtree2)
		  (return (values urtree1 urtree2)))))))

(defun compare-rx-extrem-lines (tree1 tree2)
  (let ((rx1 (carray-from tree1 (rx-transducer)))
	(rx2 (carray-from tree2 (rx-transducer))))
     (and (= (rx-l rx1) (rx-l rx2))
	  (equal (carray-line-list rx1 1) (carray-line-list rx2 1)))))

(defun test-pred (n pred)
  (let ((urtrees (collect-enum (unordered-tree-enumerator n))))
    (loop
      for urtree in urtrees
      do (when (funcall pred urtree)
	   (return-from test-pred (values urtree))))))

(defun enum-pred (n pred)
  (make-filter-enumerator (unrooted-unordered-tree-enumerator n)
			  pred))

(defun p2-characteristic-p (ca)
  (loop
    for i from 1 below (carray-nv ca)
    unless (= 2 (aref ca 1 i)) 
      do (return)
    finally (return t)))

(defun test-binary-pred (n pred)
  (let ((urtrees (collect-enum (unordered-tree-enumerator n))))
    (loop
      for urtree1 in urtrees
      do (loop
	   for urtree2 in urtrees
	   unless (eq urtree1 urtree2)
	     do (when (funcall pred urtree1 urtree2)
		  (return-from test-binary-pred (values urtree1 urtree2)))))))

;; (defun product-zone-k (k ca1 ca2)
;;   (let (
;;     (values
;;      (max 0 (- k ne2)) (min ne1 k)))))

(defun product-zone-ki (k i ca1 ca2)
  (let* ((ne1 (1- (first (array-dimensions ca1))))
	 (ne2 (1- (first (array-dimensions ca2))))
	 (nv1 (carray-nv ca1))
	 (nv2 (carray-nv ca2)))
    (values
     (max 0 (- k ne2)) (min ne1 k)
     (max 0 (- i nv2)) (min nv1 i))))

(defun show-product-zones (k i ca1 ca2)
  (let ((a1 (make-array (array-dimensions ca1)))
	(a2 (make-array (array-dimensions ca2))))
    (multiple-value-bind (k1 k2 i1 i2) (product-zone-ki k i ca1 ca2)
	(loop
	  for l from k1 to k2
	  do (loop
	       for j from i1 to i2
	       do (progn
		    (setf (aref a1 l j) 1)
		    (setf (aref a2 (- k l) (- i j)) 1))))
      (values (ftd a1) (ftd a2)))))

(defun test-either-dds-or-lx (n)
  (let ((trees (collect-enum (unordered-tree-enumerator n))))
    (loop
      for tree1 in trees
      do (loop
	   for tree2 in trees
	   unless (eq tree1 tree2)
	     do
		(when (compare-rx-extrem-lines tree1 tree2)
		  (return (values tree1 tree2)))))))

(defun r-to-tree (rr)
  (let* ((nv (carray-nv rr))
	 (nv-div-2 (floor nv 2))
	 (l (r-l rr)))
    (cond
      ((= 1 l)
       (tree-from-sexpr (iota nv 1)))
      ((= nv-div-2 l)
       (if (p2-characteristic-p rr)
	   (tree-term-to-tree (graph-to-tree-term (graph-pn nv t)))
	   nil))
      (t nil))))
