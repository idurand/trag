(in-package :autotree)

(defun array-header (array)
  (format t "\\begin{array}")
  (format t "{|c||")
  (let ((nv (first (array-dimensions array))))
    (loop
      for c from 0 to nv
      do (format t "c|")
      finally (format t "}~%"))
    (format t "\\hline~%")
    (loop for column from 0 to nv
	  do (bf-item column)
	  finally (format t "\\\\\\hline~%"))))
   
(defun bf-item (v)
  (format t "& \\bf{~A} " v))
  
(defun array-footer ()
  (format t "}"))
			   
(defun array-line (line array)
  (format t "\\bf{~A} " line)
  (loop
    for i from 0 to (first (array-dimensions array))
    do (format t " & ~A " (aref array line i)))
  (format t "\\\\\\hline~%"))
  
(defun array-to-latex (array)
  (loop
    initially (array-header array)
    for line from 0 below (first (array-dimensions array))
    do (array-line line array)
    finally (format t "\\end{array}")))

(defun sexpr-to-tikz-expr (sexpr)
  (if (atom sexpr)
      (format t "[~A]" sexpr)
      (progn 
;;	(format t "[~A " (car sexpr))
	(mapc 
	 #'sexpr-to-tikz-expr (cdr sexpr))
	(format t "]")))
  (values))

(defun sexpr-to-tikz (sexpr)
  (format t "\\begin{forest}~%")
  (format t  "for tree={draw=green!50!black,fill=green!15,edge = {-},circle,
      rounded corners=4pt,minimum size=5mm,inner sep=1.5pt,draw,
      math content,anchor=center,l=1cm},~%")
  (sexpr-to-tikz-expr sexpr)
  (format t "\\end{forest}~%"))
  
