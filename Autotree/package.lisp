;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :autotree
  (:use :common-lisp
   :general :tree :input-output
   :vbits :object :color :container :symbols :terms :state
   :termauto :enum :graph)
  (:export
   ))
