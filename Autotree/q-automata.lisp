;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)
;; constants *
;; color 1 = X
;; color 2 = Y
;; color 3 = neither in X or in Y

;; internal nodes #
;; color 0 = edge not in F
;; color 1 = edge in F

;; il
;; 10 = root in X ok below
;; 21 = root in Y connected to some edge in F
;; 20 = root in Y not connected to some edge in F
;; 30 = root not in X U Y and not the end of any edge in F

;; state-final-p

(defgeneric i-of (q-state))
(defgeneric l-of (q-state))

(defclass q-state (uncasted-state) 
  ((i :initarg :i :reader i-of)
   (l :initarg :l :reader l-of)))

(defmethod state-final-p ((s q-state))
  (or (= (i-of s) 3)
      (= (l-of s) 1)))

(defmethod print-object ((s q-state) stream)
  (format stream "[i:~A l:~A]" (i-of s) (l-of s)))

(defun make-q-state (i l)
  (assert (<= 1 i 3))
  (assert (<= 0 l 1))
  (make-instance 'q-state :i i :l l))

(defgeneric q-transitions-fun (root arg))

(defmethod q-transitions-fun ((root color-constant-symbol) (arg (eql nil)))
  (make-q-state (color root) 0))
  
(defmethod q-transitions-fun ((root color-non-constant-symbol) (arg list))
  (let* ((s1 (first arg))
	 (s2 (second arg))
	 (i1 (i-of s1))
	 (i2 (i-of s2))
	 (l1 (l-of s1)))
    (if (zerop (color root))
	(unless (and (= (l-of s2) 0) (or (= i2 1) (= i2 2)))
	  ;; only a vertex with color 3 can be unconnected
	  (make-q-state i1 l1))
	(when (or (and (= i1 1) (= i2 2))
		  (and (= i1 2) (= i2 1)))
	  (make-q-state i1 (max l1 1))))))

(defun q-basic-automaton ()
  (make-fly-automaton
   (tree-signature 3)
   (lambda (root states)
     (q-transitions-fun root states))
   :name (format nil "Q-C")))

(defun q-table-basic-automaton ()
  (minimize-automaton
   (compile-automaton
    (q-basic-automaton))))

(defun q-automaton ()
  (tree-projection (q-table-basic-automaton)))

(defun sup-q-transducer (count)
  (tree-projection
   (attribute-automaton
    (intersection-automaton
     (tree-sup-leaves-automaton count)
     (q-basic-automaton))
    *tittman-afun*)))

(defun q-transducer-from-automaton (basic-automaton)
  (tree-projection
   (attribute-automaton basic-automaton *tittman-afun*)))

(defun q-transducer ()
  (q-transducer-from-automaton (q-table-basic-automaton)))

(defun q1-transducer ()
  (let* ((tb (q-table-basic-automaton))
	 (transitions (transitions-of tb))
	 (b11-state (find-casted-state (make-q-state 1 1) transitions))
	 (transducer (q-transducer-from-automaton tb)))
    (setf (output-fun transducer)
	  (lambda (target)
	    (attribute-of
	     (container-member b11-state target))))
    transducer))

(defun q2-transducer ()
  (let* ((tb (q-table-basic-automaton))
	 (transitions (transitions-of tb))
	 (b21-state (find-casted-state (make-q-state 2 1) transitions))
	 (transducer (q-transducer-from-automaton tb)))
    (setf (output-fun transducer)
	  (lambda (target)
	    (attribute-of
	     (container-member b21-state target))))
    transducer))

(defun q0-transducer ()
  (let* ((tb (q-table-basic-automaton))
	 (transitions (transitions-of tb))
	 (b30-state (find-casted-state (make-q-state 3 0) transitions))
	 (transducer (q-transducer-from-automaton tb)))
    (setf (output-fun transducer)
	  (lambda (target)
	    (attribute-of
	     (container-member b30-state target))))
    transducer))

(defun q-sat-transducer ()
  (tree-projection
   (attribute-automaton
     (q-basic-automaton)
     *assignment-afun*)))

(defun q-atriples (tree-term)
  (transducer-atriples tree-term (q-transducer)))

(defun q0-atriples (tree-term)
  (transducer-atriples tree-term (q0-transducer)))

(defun q11-atriples (tree-term)
  (transducer-atriples tree-term (q1-transducer)))

(defun q21-atriples (tree-term)
  (transducer-atriples tree-term (q2-transducer)))

;; AUTOTREE> (compute-final-value *tp4* (q0-transducer))
;; {#(0 0 0):1 #(1 1 1):2}A
;; T
;; AUTOTREE> (compute-final-value *tp4* (q1-transducer))
;; {#(1 1 1):2 #(1 2 2):1 #(2 1 2):1 #(2 2 2):2 #(2 2 3):1}A
;; T
;; AUTOTREE> (compute-final-value *tp4* (q2-transducer))
;; {#(1 1 1):2 #(1 2 2):1 #(2 1 2):1 #(2 2 2):2 #(2 2 3):1}A
;; T
;; AUTOTREE> (compute-final-value *tp4* (q-transducer))
;; {#(0 0 0):1 #(1 1 1):6 #(1 2 2):2 #(2 1 2):2 #(2 2 2):4 #(2 2 3):2}A
;; T
