;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)
;; constants *
;; color 1 = X
;; color 2 = Y

;; internal nodes #
;; color 0 = edge not in F
;; color 1 = edge in F

;; il
;; 10 = root in X not connected to some edge in F
;; 11 = root in X connected to some edge in F
;; 20 = root in Y not connected to some edge in F
;; 21 = root in Y connected to some edge in F

;; state-final-p

(defgeneric i-of (r-state))

(defclass r-state (uncasted-state) 
  ((i :initarg :i :reader i-of)))

(defmethod state-final-p ((s r-state)) t)

(defmethod print-object ((s r-state) stream)
  (format stream "[i:~A]" (i-of s)))

(defun make-r-state (i)
  (assert (<= 1 i 2))
  (make-instance 'r-state :i i))

(defgeneric r-transitions-fun (root arg))

(defmethod r-transitions-fun ((root color-constant-symbol) (arg (eql nil)))
  (make-r-state (color root)))
  
(defmethod r-transitions-fun ((root color-non-constant-symbol) (arg list))
  (let* ((s1 (first arg))
	 (s2 (second arg))
	 (i1 (i-of s1))
	 (i2 (i-of s2))
	 (no-edge (zerop (color root))))
    (when (or (and (= i1 i2) no-edge) (and (/= i1 i2) (not no-edge)))
      (make-r-state i1))))

(defun r-basic-automaton ()
  (make-fly-automaton 
   (tree-signature 2)
   (lambda (root states)
     (r-transitions-fun root states))
   :name (format nil "R-C")))

(defmethod rx-state-final-p ((s r-state))
  (= 1 (i-of s)))

(defun rx-basic-automaton ()
  (make-fly-automaton 
   (tree-signature 2)
   (lambda (root states)
     (r-transitions-fun root states))
   :final-state-fun #'rx-state-final-p
   :name (format nil "RX-C")))

(defun ry-basic-automaton ()
  (make-fly-automaton 
   (tree-signature 2)
   (lambda (root states)
     (r-transitions-fun root states))
   :final-state-fun (lambda (s) (= 2 (i-of s)))
   :name (format nil "RY-C")))

(defun r-table-basic-automaton ()
  (compile-automaton
   (r-basic-automaton)))

(defun rx-table-basic-automaton ()
  (compile-automaton
   (rx-basic-automaton)))

(defun ry-table-basic-automaton ()
  (compile-automaton
   (ry-basic-automaton)))

(defun r-automaton ()
  (tree-projection (r-table-basic-automaton)))

(defun rx-automaton ()
  (tree-projection (rx-table-basic-automaton)))

(defun ry-automaton ()
  (tree-projection (ry-table-basic-automaton)))

(defgeneric r-symbol-fun-body (f attributes))
(defmethod r-symbol-fun-body ((f integer) (attributes list))
  (let ((attribute (attributes-symbol-fun-body attributes (multi-object-fun #'vector-sum #'*))))
    (if (zerop f)
	attribute
	(let ((v (make-triple (list 0 0 1))))
	  (container-mapcar-to-container
	   (lambda (melement)
	     (make-object-multi
	      (vector-sum (object-of melement) v)
	      (attribute-of melement)))
	   attribute)))))

(defgeneric r-symbol-fun (s))
(defmethod r-symbol-fun ((s color-non-constant-symbol))
  (let ((f (color s)))
    (lambda (&rest attributes)
      (r-symbol-fun-body f attributes))))

(defmethod r-symbol-fun ((s color-constant-symbol))
  (let* ((color (color s))
	 (u (boolean-to-bit (= 1 color)))
	 (v (boolean-to-bit (= 2 color))))
    (lambda ()
      (make-tittman-container
       (list
	(make-triple (list u v 0)))))))

(defvar *r-afun*)
(setq *r-afun*
      (make-afuns #'r-symbol-fun (container-union-fun #'make-tittman-container) "R"))

(defun r-transducer-from-automaton (basic-automaton)
  (tree-projection
   (attribute-automaton basic-automaton *r-afun*)))

(defun r-transducer ()
  (rename-object (r-transducer-from-automaton (r-table-basic-automaton)) "RT"))

(defun rx-transducer ()
  (rename-object (r-transducer-from-automaton (rx-table-basic-automaton)) "RX"))

(defun ry-transducer ()
  (rename-object (r-transducer-from-automaton (ry-table-basic-automaton)) "RY"))

(defun r-sat-transducer ()
  (tree-projection
   (attribute-automaton
     (r-basic-automaton)
     *assignment-afun*)
   2))

(defun r-atriples (tree-term &optional k)
  (let ((atriples (transducer-atriples tree-term (r-transducer))))
    (when k
      (setq atriples
	    (remove-if (lambda (triple) (/= k (aref (object-of triple) 2))) atriples)))
    atriples))

(defun rr1-atriples (tree-term &optional k)
  (let ((atriples
	  (mapcar 
	   (lambda (atriple)
	     (let ((triple (object-of atriple)))
	       (if (= (aref triple 0) (aref triple 1))
	  	   (make-atriple triple (/ (attribute-of atriple) 2))
	  	   atriple)))
	   (remove-if (lambda (atriple)
			(let ((triple (object-of atriple)))
			  (> (aref triple 0) (aref triple 1))))
		      (r-atriples tree-term)))))
    (when k
      (setq atriples
	    (remove-if (lambda (triple) (/= k (aref (object-of triple) 2))) atriples)))
    atriples))
	
(defun tree-print-rrk (tree)
  (let ((nb-edges (tree-nb-edges tree))
	(tree-term (tree-to-tree-term tree)))
    (loop for i from 0 to nb-edges
	  do (print (rr1-atriples tree-term i)))))

(defun tree-term-print-rrk (tree-term)
  (tree-print-rrk (tree-term-to-tree tree-term)))
  
(defun rx-atriples (tree-term)
  (transducer-atriples tree-term (rx-transducer)))

(defun ry-atriples (tree-term)
  (transducer-atriples tree-term (ry-transducer)))
