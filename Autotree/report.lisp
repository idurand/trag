(in-package :autotree)

(defvar *html-stream* *standard-output*)

(defmacro with-html-stream-output (expr)
  `(cl-who:with-html-output (*html-stream* nil)
     ,expr))

(defmacro with-html-stream-output-newline (expr)
  `(cl-who:with-html-output (*html-stream* nil :indent t)
     ,expr))

(defun html-newline ()
  (format *html-stream* "<div style='clear: both'></div>~%"))

(defun html-hr ()
  (format *html-stream* "<hr>~%"))

(defun html-br ()
  (format *html-stream* "<br>~%"))

(defun html-td (i &key &allow-other-keys)
  (with-html-stream-output (:td :align "right" (cl-who:str i))))

(defparameter *html-td-generator* #'html-td)
(defparameter *html-transducer-hook* nil)

(defun html-th (i)
  (with-html-stream-output
      (:th :align "right"
	   (cl-who:str i))))

(defun html-line (array line nv)
  (with-html-stream-output-newline
      (:tr
       (html-th line)
       (loop for c from 0 to nv
	     do (funcall *html-td-generator* (aref array line c) :row line :column c)))))

(defun html-svg (tree-term svg-scale)
  (let ((svg (compute-svg tree-term svg-scale)))
    (with-html-stream-output
	(:div :style "float: left;" (cl-who:str svg)))))

(defun html-atriples-array (array term-name transducer-name)
  (let* ((dimensions (array-dimensions array))
	 (ne (1- (first dimensions)))
	 (nv (1- (second dimensions))))
    (with-html-stream-output-newline
	(:div :style "float: left; padding: 1em;"
	      (:table :style "float: left; padding: 1em;"
		      :rules "all" :border "all" :cellpadding 4
		      (:thead term-name
			      (:tr (:th (cl-who:str term-name)) (:th :colspan (format nil "~d" (1+ nv)) (cl-who:str transducer-name)))
			      (:tr
			       (html-th "k \\ i")
			       (loop for column from 0 to nv
				     do (html-th column)))
			      (loop for line from 0 to ne do
				(html-line array line nv))))))))

(defun html-transducer-array (ttd transducer name)
  (when *html-transducer-hook*
    (funcall *html-transducer-hook* :transducer transducer))
  (let ((atriples-array (carray-from ttd transducer)))
    (html-atriples-array atriples-array name (name transducer))))

(defgeneric designator-to-tree-term (tree-designator))
(defmethod designator-to-tree-term ((tree-term term))
  tree-term)

(defmethod designator-to-tree-term ((tree tree))
  (tree-to-tree-term tree))

(defmethod designator-to-tree-term ((sexpr list))
  (sexpr-to-tree-term sexpr))

(defun html-tree (ttd transducers name skip-svg svg-scale)
  (let ((tree-term (designator-to-tree-term ttd)))
    (with-html-stream-output-newline
	(:div
	 :style "float: left; padding: 1em;"
	 (unless skip-svg
	   (html-svg tree-term svg-scale))
	 (when (atom transducers)
	   (setq transducers (list transducers)))
	 (loop for transducer in transducers
	       do (html-transducer-array tree-term transducer name))))))

(defun report-stream (s transducers &key name svg-scale skip-svg)
  (with-html-stream-output-newline
      (:html
       :xmlns "http://www.w3.org/1999/xhtml" :xml\:lang "en" :lang "en"
       (:head (:meta :http-equiv "Content-Type" :content "text/html;charset=utf-8")
	      (:title "title"))
       (:body
	(html-tree s transducers name skip-svg svg-scale)))))

(defun report (sexpr-or-sexprs transducers &key (file "/tmp/report.html") (svg-scale 0.3) skip-svg name)
  (unless (listp sexpr-or-sexprs)
    (setq sexpr-or-sexprs (list sexpr-or-sexprs)))
  (with-open-stream 
      (*html-stream*
       (if file
	   (open file :direction :output :if-exists :supersede)
	   (make-synonym-stream 
	    '*standard-output*)))
    (loop for sexpr in sexpr-or-sexprs
	  do (report-stream sexpr transducers
			    :name name
			    :svg-scale svg-scale :skip-svg skip-svg))))

(defun multi-report-stream (named-entries transducers)
  (with-html-stream-output-newline
      (:html
       :xmlns "http://www.w3.org/1999/xhtml" :xml\:lang "en" :lang "en"
       (:head (:meta :http-equiv "Content-Type" :content "text/html;charset=utf-8")
	      (:title "title"))
       (:body
	(loop for named-entry in named-entries
	      do (let* ((scaling (third named-entry))
			(skip-svg (eq scaling t))
			(scale-factor (if skip-svg 1 scaling)))
		   (html-tree
		    (first named-entry)
		    transducers (second named-entry) skip-svg scale-factor))))))
  (mapcar 'second named-entries))

(defun multi-report (named-entries transducers &key file)
  (with-open-stream (*html-stream*
		     (if file
			 (open file :direction :output :if-exists :supersede)
			 (make-synonym-stream '*standard-output*)))
    (multi-report-stream named-entries transducers)))

(defun multi-report-referenced (named-entries transducers &key file)
  (let*
      ((*html-td-generator* *html-td-generator*)
       (first-entry (designator-to-tree-term (first (first named-entries))))
       (*html-transducer-hook* 
	 (lambda (&key transducer &allow-other-keys)
	   (setf
	    *html-td-generator* 
	    (let*
		((reference-array
		   (carray-from first-entry transducer)))
	      (lambda (value &key row column &allow-other-keys)
		(let*
		    ((reference-value
		       (or (ignore-errors (aref reference-array row column)) 0))
		     (style
		       (cond
			 ((= 0 value reference-value) "background-color: #DDDDDD")
			 ((= value reference-value) "background-color: #DDFFDD")
			 ((< value reference-value) "background-color: #FFDDDD")
			 ((> value reference-value) "background-color: #DDDDFF")
			 (t (error "Arithmetics is broken")))))
		  (with-html-stream-output
		      (:td :align "right" :style style (cl-who:str value))))))))))
    (multi-report named-entries transducers :file file)))

(defun output-arrays (carrays name)
  (with-open-file (*html-stream* "/tmp/c.html" 
				 :direction :output :if-exists :supersede)
    (loop
      for carray in carrays
      do (html-atriples-array carray "" name))))

(with-open-file (*html-stream* "/tmp/c.html" 
			       :direction :output :if-exists :supersede)
  (loop
    for x from 0 to 5
    do (let* ((rax (replace-x *prax* x))
	      (ray (rx-to-ry rax)))
	 (html-atriples-array rax (format nil "~A" x) "RAX")
	 (html-atriples-array ray(format nil "~A" x) "RAY")
	 (html-atriples-array (array-sum rax ray) (format nil "~A" x) "RA")
	 (html-newline))))

(defun two-terms-report (tree-terms-enumerator)
  (let* ((terms (call-enumerator tree-terms-enumerator))
	 (term1 (first terms))
	 (term2 (second terms)))
    (print terms)
  (with-open-stream 
    (*html-stream* 
     (open "/tmp/tt.html" :direction :output :if-exists :supersede))
	  
    (report-stream term1 (list (r-transducer) (rx-transducer))
		   :svg-scale 0.4 :name "T1")
    (report-stream term2 (list (r-transducer) (rx-transducer)) 
		   :svg-scale 0.4 :name "T2"))))

(defun carrays-comparative-report-stream (carrays)
  (let*
    ((*html-td-generator* 
       (let* ((reference-carray (first carrays)))
	 (lambda (value &key row column &allow-other-keys)
	   (let*
	       ((reference-value
		  (or (ignore-errors (aref reference-carray row column)) 0))
		(style
		  (cond
		    ((= 0 value reference-value) "background-color: #DDDDDD")
		    ((= value reference-value) "background-color: #DDFFDD")
		    ((< value reference-value) "background-color: #FFDDDD")
		    ((> value reference-value) "background-color: #DDDDFF")
		    (t (error "Arithmetics is broken")))))
	     (with-html-stream-output
		 (:td :align "right" :style style (cl-who:str value))))))))
    (with-html-stream-output-newline
	(:html
	 :xmlns "http://www.w3.org/1999/xhtml" :xml\:lang "en" :lang "en"
	 (:head (:meta :http-equiv "Content-Type" :content "text/html;charset=utf-8")
		(:title "title"))
	 (:body
	  (loop for carray in carrays
		do (html-atriples-array carray "T" "RX")))))))

(defun carrays-comparative-report (carrays &key file)
  (with-open-stream (*html-stream*
		     (if file
			 (open file :direction :output :if-exists :supersede)
			 (make-synonym-stream '*standard-output*)))
    (carrays-comparative-report-stream carrays)))
