;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

(defclass tittman-container (ordered-multi-container)
  ())

(defun make-tittman-container (triples)
  (make-multi-container-generic
   triples
   'tittman-container
   #'equalp
   #'+))

(defmethod container-order-fun ((container tittman-container)) #'vector<)

(defgeneric tittman-symbol-fun-body (f attributes))
(defmethod tittman-symbol-fun-body ((f integer) (attributes list))
  (let ((attribute (attributes-symbol-fun-body attributes (multi-object-fun #'vector-sum #'*))))
    (if (zerop f)
	attribute
	(let ((v (make-triple (list 0 0 1))))
	  (container-mapcar-to-container
	   (lambda (melement)
	     (make-object-multi
	      (vector-sum (object-of melement) v)
	      (attribute-of melement)))
	   attribute)))))
  
(defgeneric tittman-symbol-fun (s))
(defmethod tittman-symbol-fun ((s color-non-constant-symbol))
  (let ((f (color s)))
    (lambda (&rest attributes)
      (tittman-symbol-fun-body f attributes))))

(defmethod tittman-symbol-fun ((s color-constant-symbol))
  (let* ((color (color s))
	 (y (boolean-to-bit (= 1 color)))
	 (w (boolean-to-bit (= 2 color))))
    (lambda ()
      (make-tittman-container
       (list
	(make-triple (list y w 0)))))))

(defvar *tittman-afun*)
(setq *tittman-afun*
      (make-afuns
       #'tittman-symbol-fun (container-union-fun #'make-tittman-container) "Tittmann"))



