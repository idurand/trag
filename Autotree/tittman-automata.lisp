;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

;; state-final-p
(defgeneric i-of (tittman-state))
(defgeneric l-of (tittman-state))

(defclass tittman-state (uncasted-state) 
  ((i :initarg :i :reader i-of)
   (l :initarg :l :reader l-of)))

(defmethod state-final-p ((s tittman-state))
  (or (/= (i-of s) 2) (= (l-of s) 1)))

(defmethod print-object ((s tittman-state) stream)
  (format stream "[i:~A l:~A]" (i-of s) (l-of s)))

(defun make-tittman-state (i l)
  (assert (<= 1 i 3))
  (assert (<= 0 l 1))
  (make-instance 'tittman-state :i i :l l))

(defgeneric tittman-transitions-fun (root arg))

(defmethod tittman-transitions-fun ((root color-constant-symbol) (arg (eql nil)))
  (make-tittman-state (color root) 0))
  
(defmethod tittman-transitions-fun ((root color-non-constant-symbol) (arg list))
  (let* ((s1 (first arg))
	 (s2 (second arg))
	 (i1 (i-of s1))
	 (i2 (i-of s2))
	 (l1 (l-of s1)))
    (if (zerop (color root))
	(unless (and (= i2 2) (= (l-of s2) 0))
	  (make-tittman-state i1 l1))
	(when (or (and (= i1 1) (= i2 2))
		  (and (= i1 2) (= i2 1)))
	  (make-tittman-state i1 (max l1 1))))))

(defun tittman-basic-automaton ()
  (make-fly-automaton
   (tree-signature 3)
   (lambda (root states)
     (tittman-transitions-fun root states))
   :name (format nil "TITTMAN-C")))

(defun tittman-table-basic-automaton ()
  (minimize-automaton
   (compile-automaton
    (tittman-basic-automaton))))

(defun tittman-automaton ()
  (tree-projection (tittman-table-basic-automaton)))

(defun sup-tittman-transducer (count)
  (tree-projection
   (attribute-automaton
    (intersection-automaton
     (tree-sup-leaves-automaton count)
     (tittman-basic-automaton))
    *tittman-afun*)))

(defun tittman-transducer-from-automaton (basic-automaton)
  (tree-projection
   (attribute-automaton basic-automaton *tittman-afun*)))

(defun tittman-transducer ()
  (tittman-transducer-from-automaton (tittman-table-basic-automaton)))

(defun tittman-b1-transducer ()
  (let* ((tb (tittman-table-basic-automaton))
	 (transitions (transitions-of tb))
	 (b10-state (find-casted-state (make-tittman-state 1 0) transitions))
	 (transducer (tittman-transducer-from-automaton tb)))
    (setf (output-fun transducer)
	  (lambda (target)
	    (attribute-of
	     (container-member b10-state target))))
    transducer))

(defun tittman-b2-transducer ()
  (let* ((tb (tittman-table-basic-automaton))
	 (transitions (transitions-of tb))
	 (b10-state (find-casted-state (make-tittman-state 1 0) transitions))
	 (b30-state (find-casted-state (make-tittman-state 3 0) transitions))
	 (transducer (tittman-transducer-from-automaton tb)))
    (setf (output-fun transducer)
	  (lambda (target)
	    (target-attribute
	     (container-remove b30-state (container-remove b10-state target)))))
    transducer))

(defun tittman-b3-transducer ()
  (let* ((tb (tittman-table-basic-automaton))
	 (transitions (transitions-of tb))
	 (b10-state (find-casted-state (make-tittman-state 1 0) transitions))
	 (b20-state (find-casted-state (make-tittman-state 2 0) transitions))
	 (transducer (tittman-transducer-from-automaton tb)))
    (setf (output-fun transducer)
	  (lambda (target)
	    (target-attribute
	     (container-remove b10-state (container-remove b20-state target)))))
    transducer))

(defun tittman-sat-transducer ()
  (tree-projection
   (attribute-automaton
     (tittman-basic-automaton)
     *assignment-afun*)))

(defun tittman-atriples (tree-term)
  (transducer-atriples tree-term (tittman-transducer)))

