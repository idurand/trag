(in-package :autotree)

(defgeneric transducer-atriples (tree transducer))

(defmethod transducer-atriples ((tree-term term) transducer)
  (container-contents (compute-final-value tree-term transducer)))

(defmethod transducer-atriples ((tree tree) transducer)
  (transducer-atriples (tree-to-tree-term tree)))

(defmethod transducer-atriples ((sexpr list) transducer)
  (transducer-atriples (sexpr-to-tree-term sexpr)))

(defun transducer-atriples-k (tree transducer k)
  (remove-if (lambda (triple) (/= k (aref (object-of triple) 2)))
	     (transducer-atriples tree transducer)))


