;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

(defun tree-sup-leaves-automaton (count)
  (nothing-to-colors
   (sup-leaves-automaton count (tree-signature 3))
   3))
