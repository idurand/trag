;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

(defun fly-tree-signature ()
  (make-fly-signature
   (lambda (sym)
     (or
      (star-symbol-p sym)
      (square-symbol-p sym)))))

(defun star-symbols (nb-colors)
  (mapcar
   (lambda (i)
     (make-star-symbol i))
   (color-iota nb-colors)))

(defun square-symbols ()
  (mapcar
   (lambda (j)
     (make-square-symbol j))
   (iota 2)))

(defun tree-symbols (nb-colors)
  (nconc
   (star-symbols nb-colors)  
   (square-symbols)))

(defun tree-signature (nb-colors)
  (make-signature
   (tree-symbols nb-colors)))
