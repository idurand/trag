;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

(defvar *star-string* "*")
(defvar *square-string* "#")

(defvar *square-symbol*)

(defvar *star-symbol*)

(defun init-tree-symbols ()
  (init-symbols)
  (setq *star-symbol*
	(make-constant-symbol *star-string*))
  (setq *square-symbol*
	(make-parity-symbol *square-string* 2)))

(init-tree-symbols)

(defgeneric star-symbol-p (abstract-symbol)
  (:documentation "is ABSTRACT-SYMBOL a star (constant) symbol"))

(defmethod star-symbol-p ((s abstract-symbol))
  (eq s *star-symbol*))

(defmethod star-symbol-p ((s decorated-symbol))
  (star-symbol-p (symbol-of s)))

(defgeneric square-symbol-p (abstract-symbol)
  (:documentation "is ABSTRACT-SYMBOL a star (constant) symbol"))

(defmethod square-symbol-p ((s abstract-symbol))
  (eq s *star-symbol*))

(defmethod square-symbol-p ((s decorated-symbol))
  (square-symbol-p (symbol-of s)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; square symbol
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun make-star-symbol (i)
  (if i
      (make-color-symbol *star-symbol* i)
      *star-symbol*))

(defun make-square-symbol (j)
  (if j
      (make-color-symbol *square-symbol* j)
      *square-symbol*))

(defun make-random-square-symbol ()
  (make-square-symbol (random 2)))

(defun make-random-star-symbol (&optional (node-colors 3))
  (make-star-symbol (1+ (random node-colors))))

(defgeneric tree-symbol-p (symbol))

(defgeneric tree-projection-f (tree-symbol node-colors))
(defmethod tree-projection-f ((s color-symbol) node-colors)
  (list (symbol-of s)))

(defgeneric tree-projection-if (tree-symbol node-colors))
(defmethod tree-projection-if ((s constant-symbol) node-colors)
  (mapcar
   (lambda (color)
     (make-star-symbol color))
   (color-iota node-colors)))

(defmethod tree-projection-if ((s parity-symbol) node-colors)
  (mapcar
   (lambda (color)
     (make-square-symbol color))
   (iota 2)))

(defun tree-projection-h (node-colors)
  (lambda (s)
    (tree-projection-f s node-colors)))

(defgeneric tree-projection-ih (node-colors))

(defmethod tree-projection-ih (node-colors)
  (lambda (s)
    (tree-projection-if s node-colors)))

(defgeneric tree-projection (signed-object &optional node-colors)
  (:documentation "tree-projection with node-colors 2 colors"))

(defmethod tree-projection ((signed-object signed-object) &optional (node-colors 3))
  (homomorphism
   signed-object
   (tree-projection-h node-colors)
   (tree-projection-ih node-colors)
   nil))
