;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autotree)

(defun tree-term-vertex (n &optional i)
  (set-term-eti
   (build-term (make-star-symbol i))
   n))

(defun tree-term-square (n t1 t2 &optional (j nil))
  (set-term-eti
   (build-term (make-square-symbol j) (list t1 t2))
   n))

(defvar *t2*)
(setq *t2*
  (tree-term-square
   2
   (tree-term-vertex 1)
   (tree-term-vertex 2) 
   ))

(defvar *t3*)
(setq *t3* (tree-term-square
	    3
	    (tree-term-square 
	     2
	     (tree-term-vertex 1)
	     (tree-term-vertex 2))
	    (tree-term-vertex 3)))

(defun boolean-to-bit (e)
  (if e 1 0))

(defun compute-i (root w y)
  (cond
    ((member root w) 1)
    ((member root y) 2)
    (t 3)))

(defun tree-to-term (tree w y f)
  (assert (endp (intersection w y)))
  (let* ((root1 (tree-root tree))
	 (subtrees (tree-subtrees tree)))
    (if (endp subtrees)
	(tree-term-vertex root1 (compute-i root1 w y))
	(let* ((t1 (make-tree root1 (butlast subtrees)))
	       (t2 (car (last subtrees)))
	       (root2 (tree-root t2))
	       (term1 (tree-to-term t1 w y f))
	       (term2 (tree-to-term t2 w y f)))
	  (tree-term-square root2 (boolean-to-bit (member root2 f)) term1 term2)))))

(defgeneric tree-term-vertices (term))
(defmethod tree-term-vertices ((term term))
  (mapcar #'term-eti (leaves-of-term term)))

(defgeneric tree-term-edges (term))
(defmethod tree-term-edges ((term term))
  (let ((edges '()))
     (labels
	 ((intern-get (subterm)
	    (unless (term-zeroary-p subterm)
	      (let* ((arg (arg subterm))
		     (t1 (first arg))
		     (t2 (second arg)))
		(intern-get t1)
		(intern-get t2)
		(push (list (tree-term-root t1) (term-eti subterm)) edges)))))
       (intern-get term))
    (nreverse edges)))

(defgeneric tree-term-num-edges (tree-term))
(defmethod tree-term-num-edges ((tree-term term))
  (if (term-constant-p tree-term)
      tree-term
      (let* ((arg (arg tree-term))
	     (t1 (first arg))
	     (t2 (second arg)))
	(set-term-eti tree-term (tree-term-root t2))
	(tree-term-num-edges t1)
	(tree-term-num-edges t2)
	tree-term)))

(defgeneric tree-term-num (tree-term)
  (:documentation "number leaves (vertices) and internal nodes (edges)")
  (:method ((tree-term term))
    (tree-term-num-edges (term-num-leaves tree-term))))

(defgeneric graph-from-tree-term (tree-term))
(defmethod graph-from-tree-term ((tree-term term))
  (setq tree-term (tree-term-num tree-term))
  (graph-from-earcs-and-enodes (tree-term-edges tree-term)
			       (tree-term-vertices tree-term) :oriented t))

(defgeneric tree-term-to-graph (tree-term))
(defmethod tree-term-to-graph ((tree-term term))
  (graph-from-tree-term tree-term))

;; (defgeneric tree-term-to-tree (tree-term))
;; (defmethod tree-term-to-tree ((tree-term term))
;;   (setq tree-term (tree-term-num tree-term))
;;   (make-tree (tree-term-edges tree-term)))
(defgeneric tree-term-edges (term))
(defmethod tree-term-edges ((term term))
  (let ((edges '()))
     (labels
	 ((intern-get (subterm)
	    (unless (term-zeroary-p subterm)
	      (let* ((arg (arg subterm))
		     (t1 (first arg))
		     (t2 (second arg)))
		(intern-get t1)
		(intern-get t2)
		(push (list (tree-term-root t1) (term-eti subterm)) edges)))))
       (intern-get term))
    (nreverse edges)))

(defgeneric tree-term-to-tree (tree-term))
(defmethod tree-term-to-tree ((tree-term term))
  (setq tree-term (tree-term-num tree-term))
  (if (term-constant-p tree-term)
      (make-leaf-tree (term-eti tree-term))
      (let* ((arg (arg tree-term))
	     (tree1 (tree-term-to-tree (first arg)))
	     (tree2 (tree-term-to-tree (second arg))))
	(make-tree (tree-root tree1)
		   (append (tree-subtrees tree1)
			   (list tree2))))))

(defun sexpr-to-tree-term (sexpr)
  (tree-to-tree-term (sexpr-to-tree sexpr)))

;; il faut trouver un moyen de numeroter les aretes et noeuds a partir du tree-term

(defgeneric tree-term-show (tree-term))
(defmethod tree-term-show ((tree-term term))
  (graph-show (graph-from-tree-term tree-term)))

(defgeneric tree-show (tree))
(defmethod tree-show ((tree tree))
  (graph-show (graph-from-tree-term (tree-to-tree-term tree))))

(defmethod tree-show ((sexpr list))
  (graph-show (graph-from-tree-term (tree-to-tree-term sexpr))))

;; leftmost leaf is the root of the tree

;; (defgeneric tree-to-tree-term (tree))
;; (defmethod  tree-to-tree-term ((tree tree))
;;   (let* ((root (tree-root tree))
;; 	 (sons (tree-sons root tree)))
;;     (if (endp sons)
;; 	(tree-term-vertex root)
;; 	(let* ((son2 (car (last sons)))
;; 	       (tree2 (node-tree son2 tree))
;; 	       (t2 (tree-to-tree-term tree2))
;; 	       (nodes2 (tree-nodes tree2))
;; 	       (nodes1 (set-difference (tree-nodes tree) nodes2))
;; 	       (tree1 (tree-filter tree nodes1))
;; 	       (t1 (tree-to-tree-term tree1)))
;; 	  (tree-term-square son2 t1 t2)))))

(defgeneric tree-to-tree-term (tree))
(defmethod  tree-to-tree-term ((tree tree))
  (let* ((root (tree-root tree))
	 (subtrees (tree-subtrees tree)))
    (if (endp subtrees)
	(tree-term-vertex root)
	(let* ((tree2 (car (last subtrees)))
	       (term2 (tree-to-tree-term tree2))
	       (tree1 (tree-node-cut (tree-root tree2) tree))
	       (term1 (tree-to-tree-term tree1)))
	  (tree-term-square
	   (tree-root tree2) term1 term2)))))

(defmethod  tree-to-tree-term ((sexpr list))
  (tree-to-tree-term (sexpr-to-tree sexpr)))

(defgeneric edges-to-tree-term (edges &optional nodes))

(defmethod edges-to-tree-term ((edges list) &optional nodes)
  (tree-to-tree-term (make-tree edges nodes)))

(defgeneric nothing-to-random-wyf-body (symbol))
(defmethod nothing-to-random-wyf-body ((symbol (eql *star-symbol*)))
  (list (make-random-star-symbol)))

(defmethod nothing-to-random-wyf-body ((symbol (eql *square-symbol*)))
  (list (make-random-square-symbol)))

(defgeneric nothing-to-random-wyf-if (symbol))
(defmethod nothing-to-random-wyf-if ((s color-symbol))
  (list (symbol-of s)))

(defun nothing-to-random-wyf-ih ()
  (lambda (s)
    (nothing-to-random-wyf-if s)))

(defun nothing-to-random-wyf-h ()
  (lambda (s)
    (nothing-to-random-wyf-body s)))

(defgeneric nothing-to-random-wyf (tree-term))
(defmethod nothing-to-random-wyf ((signed-object signed-object))
  (homomorphism
     (add-empty-symbols signed-object)
     (nothing-to-random-wyf-h)
     (nothing-to-random-wyf-ih)))

(defgeneric tree-term-root (tree-term))
(defmethod tree-term-root ((tree-term term))
  (if (term-constant-p tree-term)
      (term-eti tree-term)
      (tree-term-root (car (arg tree-term)))))

(defmethod degree-sequence-of ((tree-term term))
  (degree-sequence-of (tree-term-to-tree tree-term)))

(defun tree-term-random (nb-leaves)
  (case nb-leaves
    (1 (tree-term-vertex nil))
    (2 (tree-term-square nil (tree-term-vertex nil) (tree-term-vertex nil)))
    (t
     (let ((i (1+ (random (1- nb-leaves)))))
       (tree-term-square
	nil
	(tree-term-random i)
	(tree-term-random (- nb-leaves i)))))))

(defun graph-to-tree-term (graph)
  (tree-to-tree-term (tree::graph-to-tree graph)))

(defun test-tree-to-tree-term (tree)
  (assert
   (tree-equivalent-p tree (tree-term-to-tree (tree-to-tree-term tree)))))

(defmethod root-change-equivalent-p ((tree-term1 term) (tree-term2 term))
  (root-change-equivalent-p
   (tree-term-to-tree tree-term1)  
   (tree-term-to-tree tree-term2)))
