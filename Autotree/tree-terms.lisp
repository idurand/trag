(in-package :autotree)

(defparameter *s0* (graph-to-tree-term (graph-sn 0 t)))
(defparameter *s1* (graph-to-tree-term (graph-sn 1 t)))
(defparameter *s2* (graph-to-tree-term (graph-sn 2 t)))
(defparameter *s3* (graph-to-tree-term (graph-sn 3 t)))
(defparameter *s4* (graph-to-tree-term (graph-sn 4 t)))
(defparameter *s5* (graph-to-tree-term (graph-sn 5 t)))
(defparameter *s6* (graph-to-tree-term (graph-sn 6 t)))
(defparameter *s7* (graph-to-tree-term (graph-sn 7 t)))

(defparameter *p1* (graph-to-tree-term (graph-pn 1 t)))
(defparameter *p2* (graph-to-tree-term (graph-pn 2 t)))
(defparameter *p3* (graph-to-tree-term (graph-pn 3 t)))
(defparameter *p4* (graph-to-tree-term (graph-pn 4 t)))
(defparameter *p5* (graph-to-tree-term (graph-pn 5 t)))
(defparameter *p6* (graph-to-tree-term (graph-pn 6 t)))
(defparameter *p7* (graph-to-tree-term (graph-pn 7 t)))
(defparameter *p8* (graph-to-tree-term (graph-pn 8 t)))

(defparameter *c22* (sexpr-to-tree-term '(1 (2 (3 4) 5))))
(defparameter *c32* (sexpr-to-tree-term '(1 (2 (3 4 5) 6 7))))
(defparameter *c33* (sexpr-to-tree-term '(1 (2 (3 (4 5 6)) 7 8) 9 10)))
(defparameter *f22* (sexpr-to-tree-term '(1 (2 3 4) (5 6 7))))

(defparameter *s3-2* (sexpr-to-tree-term '(1 (2 5) 3 4)))
(defparameter *s3-23* (sexpr-to-tree-term '(1 (2 5) (3 6) 4)))
(defparameter *s4-2* (sexpr-to-tree-term '(1 (2 6) 3 4 5)))
(defparameter *s5-23* (sexpr-to-tree-term '(1 (2 7) (3 8) 4 5 6)))
(defparameter *P3-2* (sexpr-to-tree-term '(1 (2 3 4))))
(defparameter *P3-12* (sexpr-to-tree-term '(1 (2 3 4) 5)))
(defparameter *P4-2* (sexpr-to-tree-term '(1 (2 (3 4) 5))))
(defparameter *P4-23* (sexpr-to-tree-term '(1 (2 (3 4 5) 6))))
(defparameter *P4-123* (sexpr-to-tree-term '(1 (2 (3 4 5) 6) 7)))
(defparameter *P5-24* (sexpr-to-tree-term '(1 (2 (3 (4 5 6)) 7))))
(defparameter *P5-124* (sexpr-to-tree-term '(1 (2 (3 (4 5 6)) 7) 8)))
(defparameter *P5-2* (sexpr-to-tree-term '(1 (2 (3 (4 5)) 6))))
(defparameter *P5-3* (sexpr-to-tree-term '(1 (2 (3 (4 5) 6)))))

; (remove-if (lambda (triple) (/= 2 (aref (object-of triple) 2)))(q-atriples *p6*))

(defparameter *t1*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 (3 (4 (5 6)) 7) 8)))))

(defparameter *t2*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 (3 (4 (5 6) 7)) 8)))))
  
(defparameter *p6-2*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 (3 (4 (5 6))) 7)))))

(defparameter *p6-3*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 (3 (4 (5 6)) 7))))))

(defparameter *t12*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 3 (4 (5 6))) (7 8 9) (10 11) 12))))

(defparameter *st3-2*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 3) (4 5) (6 7)))))

(defparameter *st4-2*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 3) (4 5) (6 7) (8 9)))))

(defparameter *st3-3*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 (3 4)) (5 (6 7)) (8 (9 10))))))


(defparameter *t7*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 3) (4 5) 6 7))))


(defparameter *t7b*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 3 5) (5 6) 7))))

(defparameter *t7c*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 3 4) (5 6 7)))))

(defparameter *t8*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 3 4) 5 (6 7 8)))))

(defparameter *t10*
  (tree-to-tree-term (sexpr-to-tree '(1 (2 3 4) (5 6 7) (8 9 10)))))

(defparameter *trees-inf-4*
   '(
     ((1 (2 3)) "S2" t)
     ((1 (2 (3 4))) "P4" t)
     ((1 2 3 4) "S3" t)))

(defparameter *trees-5*
  ;; 5 nodes
  '(
    ((1 2 3 4 5) "S4" t)
    ((1 (2 (3 (4 5)))) "P5" t)
    ((1 (2 (3 4) 5)) "P4-2" 0.4)
    ))

(defparameter *trees-6*
  ;; 6 nodes
  '(
    ((1 2 3 4 5) "S5" t)
    ((1 (2 (3 (4 (5 6))))) "P6" t)
    ((1 (2 (3 (4 5)) 6)) "P5-2" 0.4)
    ((1 (2 (3 (4 5) 6))) "P5-3" 0.4)
    ((1 (2 (3 4 5) 6)) "P4-23" 0.4)
    ((1 (2 6) 3 4 5) "S4-2" 0.4)))

(defparameter *trees-7*
     ;; 7 nodes
  '(
    ((1 2 3 4 5 6) "S6" t)

    ((1 (2 (3 (4 (5 (6 7)))))) "P7" t)
    ((1 (2 (3 (4 (5 6))) 7)) "P6-2" 0.3)
    ((1 (2 (3 (4 (5 6)) 7))) "P6-3" 0.3)
    ((1 (2 5) (3 6) 4) "S4-23" 0.3)
    ((1 (2 3) 4 5 6 7) "S5-2" 0.3)
    ((1 (2 3) (4 5) (6 7)) "St2-3" 0.3)
    ((1 (2 3 4 5) 6 7) "C33" 0.3)
    ((1 (2 3) (4 5) 6 7) "T7" 0.3)
    ((1 (2 3 4) (5 6) 7) "T7b" 0.3)
    ((1 (2 3 4) (5 6 7)) "T7c" 0.3)
    ((1 (2 3 4) (5 6) 7) "T7d" 0.3)
    ((1 (2 (3 4)) 5 6 7) "T7e" 0.3)))

(defparameter *trees-8*
     ;; 8 nodes
  '(
    ((1 2 3 4 5 6 7 8) "S7" t)
    ((1 (2 (3 (4 (5 (6 (7 8))))))) "P8" t)
    ((1 (2 (3 (4 5 6)) 7) 8) "P5-124" 0.3)
    ((1 (2 7) (3 8) 4 5 6) "S5-23" 0.3)
    ((1 (2 3 4) 5 (6 7 8)) "T8" 0.3)))

(defparameter *trees-9*
     ;; 9 nodes
  '(
    ((1 2 3 4 5 6 7 8 9) "S8" t)
    ((1 (2 (3 (4 (5 (6 (7 (8 9)))))))) "P9" t)
    ((1 (2 3) (4 5) (6 7) (8 9)) "St4-2" 0.3)
    ((1 (2 3 4) (5 6) (7 8) 9) "T9" 0.3)
    ((1 (2 3 4 5) (6 7) (8 9)) "T9b" 0.3)
    ((1 (2 (3 4) 5 6) (7 8) 9) "T9c" 0.3)
    ((1 (2 (3 4 5) 6 7) (8 9)) "T9d" 0.3)))
  
(defparameter *trees-10*
     ;; 10 nodes
  '(
    ((1 (2 3 4) (5 6 7) (8 9 10)) "T10" 0.3)))

(defparameter *trees-12*
     ;; 12 nodes
  '(((1 (2 3 (4 (5 6))) (7 8 9) (10 11) 12) "T12" 0.3))
  )

(defparameter *trees* (append *trees-inf-4* *trees-5* *trees-6* *trees-7* *trees-8* *trees-9* *trees-10* *trees-12*))

(defparameter *t13* '(1 (2 3 4) (5 6 7) (8 9 10) (11 12 13)))

(defparameter *tt* 
  '(
    ((1 (2 (3 (4 (5 6))))) "P6" 0.3)
    ((1 (2 3) (4 5) (6 7)) "t" 0.3)
    ((1 (2 3) (4 5) (6 7) (8 9) (10 11)) "t" 0.3)
    ((1 (2 3 4) (5 6 7) (8 9 10)) "t" 0.3)
    ((1 (2 3 4) (5 6 7) (8 9 10) (11 12 13)) "t" 0.3)
    ((1 (2 3 4) (5 6 7) (8 9 10) (11 12 13)) "t" 0.3)
    ((1 (2 3) (4 5) (6 7) (8 9)) "t" 0.3)
    ((1 (2 (3 4)) (5 (6 7)) (8 (9 10)) (11 (12 13))) "t" 0.3)
    ((1 (2 (3 (4 5))) (6 (7 (8 9))) (10 (11 (12 13))) (14 (15 (16 17)))) "t" 0.3)
    ((1 (2 (3 (4 5))) (6 (7 (8 9))) (10 (11 12)) (13 (14 15))) "t" 0.3)
    ((1 (2 (3 (4 (5 6)))) 
      (7 (8 (9 (10 11)))) (12 (13 (14 (15 16)))) (17 (18 (19 (20 21))))) "t" 0.3)
    ))
