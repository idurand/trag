(in-package :autotree)

(defmacro select-atriples ((u v e n) from where &optional result)
  (let*
    ((x (gensym))
     (o (gensym)))
    `(loop
       for ,x in ,from
       for ,o := (object-of ,x)
       for ,n := (attribute-of ,x)
       for ,u := (elt ,o 0)
       for ,v := (elt ,o 1)
       for ,e := (elt ,o 2)
       when ,where
       collect ,(or result x))))

(defun atriples-to-triples (atriples)
  (mapcar #'object-of atriples))

(defun q-triples (tree-term)
  (atriples-to-triples (q-atriples tree-term)))

(defun make-triple (l)
  (make-array 3 :initial-contents l))

(defun make-atriple (l a)
  (make-attributed-object (make-triple l) a))

(defun copy-atriple (atriple)
  (make-attributed-object (make-triple (object-of atriple)) (attribute-of atriple)))

(defun compare-atriples (atriple1 atriple2)
  (and
   (equalp (object-of atriple1)  (object-of atriple2))
   (= (attribute-of atriple1)  (attribute-of atriple2))))

(defun difference-atriple-list (atriples1 atriples2)
  (values
   (set-difference atriples1 atriples2 :test #'compare-atriples)
   (set-difference atriples2 atriples1 :test #'compare-atriples)))

(defun triples-nb-edges (triples)
  (reduce #'max (mapcar (lambda (triple) (aref triple 2)) triples)))

(defun modify-atriple (atriple triple-fun)
  (let ((new-atriple (copy-atriple atriple)))
    (funcall triple-fun (object-of new-atriple))
    new-atriple))

(defun modify-atriples (atriples triple-fun)
  (mapcar (lambda (atriple) (modify-atriple atriple triple-fun))
	  atriples))

(defun multiply-atriples (atriples index)
  (modify-atriples atriples (lambda (triple) (incf (aref triple index)))))

(defun atriples-nb-edges (atriples)
  (triples-nb-edges (atriples-to-triples atriples)))

(defun triples-nb-nodes (triples)
  (reduce #'max (mapcar (lambda (triple)
			    (+ (aref triple 0) (aref triple 1)))
			triples)))
(defun atriples-nb-nodes (atriples)
  (triples-nb-nodes (atriples-to-triples atriples)))

(defun triples-degree-max (triples)
  (setq triples (remove-if (lambda (triple) (/= (aref triple 0) 1)) triples))
  (reduce #'max (mapcar (lambda (triple) (aref triple 2)) triples)))

(defun atriples-degree-max (atriples)
  (aref (object-of (car (atriples-with-degree-max atriples))) 1))

(defun atriples-with-degree-max (atriples)
  (setq atriples (remove-if-not
		  (lambda (atriple) (= (aref (object-of atriple) 0) 1))
		  atriples))
  (let ((degree-max
	  (reduce
	   #'max atriples
	   :key (lambda (atriple)
		  (aref (object-of atriple) 1)))))
    (remove-if-not
     (lambda (atriple)
       (= degree-max (aref (object-of atriple) 1)))
     atriples)))

(defun atriples-full-edges (atriples)
  (let ((nb-edges (atriples-nb-edges atriples)))
    (remove-if-not
     (lambda (atriple)
       (let* ((triple (object-of atriple))
	      (w (aref triple 2)))
	 (= nb-edges w)))
     atriples)))

(defun atriples-full-nodes (atriples)
  "all the nodes are colored but not all edges"
  (let ((nb-nodes (atriples-nb-nodes atriples))
	(nb-edges (atriples-nb-edges atriples)))
    (remove-if
     (lambda (atriple)
       (let* ((triple (object-of atriple))
	      (u (aref triple 0))
	      (v (aref triple 1))
	      (w (aref triple 2))
	      )
	 (or (= w nb-edges) (/= nb-nodes (+ u v)))))
     atriples)))

(defun atriples-uncomplete (atriples)
  (let ((nb-edges (atriples-nb-edges atriples))
	(nb-nodes (atriples-nb-nodes atriples)))
    (remove-if
     (lambda (atriple)
       (let* ((triple (object-of atriple))
	      (u (aref triple 0))
	      (v (aref triple 1))
	      (w (aref triple 2)))
	 (or (= nb-edges w) (= nb-nodes (+ u v)))))
     atriples)))
  

(defun full-red-triples (atriples)
  (let* ((triples (atriples-to-triples atriples))
	 (nb-edges (triples-nb-edges triples)))
    (remove-if-not
     (lambda (atriple)
       (and (= 1 (attribute-of atriple))
	    (let* ((triple (object-of atriple))
		   (u (aref triple 0))
		   (v (aref triple 1))
		   (w (aref triple 2)))
	      (and (= nb-edges w)
		   (> u v)))))
     atriples)))
