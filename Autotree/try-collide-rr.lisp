					; (require :autotree)

(in-package :autotree)

(let ((counter 0))
  (defun reset-v () (setf counter 0))
  (defun mk-v () (incf counter))
  (defun mk-sn (n)
    (loop for k from 0 to n collect (mk-v)))
  (defun mk-pn (n)
    (loop for k from 1 to n
          for tail := (list (mk-v)) then (list (mk-v) tail)
          finally (return tail)))
  (defun mk-chain (&rest subtrees)
    (loop
      for s in subtrees
      for tail := s then (append s (list tail))
      finally (return tail)))
  (defun mk-bintree (&rest subtrees)
    (let ((n (length subtrees)))
      (cond 
        ((= n 0) ())
        ((= n 1) (first subtrees))
        (t (list (mk-v)
                 (apply 'mk-bintree (subseq subtrees 0 (truncate n 2)))
                 (apply 'mk-bintree (subseq subtrees (truncate n 2) n)))))))
  (defun mk-tree (&rest subtrees) (cons (mk-v) subtrees))
  (defun mk-sn-bunch (&rest ns) (apply 'mk-tree (mapcar 'mk-sn ns))))

(format-2d-natural-array (carray-from '(1 (2 3) (4 (5 (6 (7 8)) (9 (10 11))) (12 13 14))) (r-transducer)))

(format-2d-natural-array (carray-from '(1 (2 3) (4 (5 (6 (7 8)) (9 10 11)) (12 (13 14)))) (r-transducer)))

(multi-report-referenced
 '(
   (
    (1 (2 13 (5 6 7 12)) (4 (8 (9 10 11))))
    "TN1"
    0.5
    )
   (
    (1 (2 13 (5 (6 7 11))) (4 (8 9 10 12)))
    "iso-TN1"
    0.5
    )
   )
 (list (r-transducer))
 :file "/tmp/deg-seq-collision.html")

(multi-report-referenced
 '(
   (
    (1 2 3 4 5 6 7 8 9)
    "S9"
    0.5
    )
   (
    (1 (2 (3 (4 (5 (6 (7 (8 9))))))))
    "P9"
    0.5
    )
   (
    (1 (2 3 4 5) (6 7 8 9))
    "S3+S3"
    0.5
    )
   (
    (1 (2 3 4) (5 6 7) 8 9)
    "S2+S2+S0+S0"
    0.5
    )
   )
 (list (r-transducer) (rx-transducer))
 :file "/tmp/test-rr.html")

(multi-report-referenced
 '(
   (
    (1 (2 3 4) (5 6) 7 8)
    "S2+S1+S0+S0"
    0.5
    )
   (
    (1 (2 3 4) (5 6) 7)
    "S2+S1+S0"
    0.5
    )
   )
 (list (r-transducer) (rx-transducer))
 :file "/tmp/test-rr.html")

(multi-report-referenced
 '(
   (
    (1 2 (3 4 5 6) (7 8 9))
    "TN"
    0.5
    )
   (
    (1   (3 4 5 6) (7 8 9))
    "TN-1"
    0.5
    )
   )
 (list (r-transducer))
 :file "/tmp/diag-sum.html")

(+ 1 6 18 36 46 33 11 1)
(+ 1   5 12 19 2   1 9 19 26 6 1)

(reset-v)
(multi-report-referenced
 (let*
     (
      )
   (labels
       (
	(bottom () (mk-sn 2))
	(cap-1 () (mk-tree (mk-sn 3) (mk-sn 4)))
	(cap-2 () (mk-tree (mk-sn 6) (mk-sn 1)))
	(marker-1 () (mk-tree (mk-pn 3) (mk-sn 1)))
	(marker-2 () (mk-tree (mk-pn 2) (mk-sn 2)))
	)
     `(
       (
	,(mk-chain (bottom) (cap-1) (marker-1) (mk-sn 0) (marker-2) (cap-2) (bottom))
	"TN"
	0.5
	)
       (
	,(mk-chain (bottom) (cap-1) (marker-2) (mk-sn 0) (marker-1) (cap-2) (bottom))
	"iso-TN"
	0.5
	)
       )))
 (list (r-transducer) (rx-transducer))
 :file "/tmp/test-rr.html")

(reset-v)
(multi-report-referenced
 (let*
     (
      )
   (labels
       (
	)
     `(
       (
	,(mk-bintree (mk-sn 2) (mk-sn 5) (mk-sn 3) (mk-chain (mk-pn 2)(mk-sn 2) ))
	"TN"
	0.5
	)
       (
	,(mk-bintree (mk-sn 2) (mk-sn 5) (mk-chain (mk-pn 2)(mk-sn 1)) (mk-sn 4))
	"iso-TN"
	0.5
	)
       )))
 (list (r-transducer) (rx-transducer))
 :file "/tmp/test-rr.html")

(reset-v)
(multi-report-referenced
 (let*
     (
      )
   (labels
       (
	)
     `(
       (
	,(mk-tree (mk-sn-bunch 2 4 5) (mk-sn-bunch 3 3))
	"TN"
	0.4
	)
       (
	,(mk-tree (mk-sn-bunch 3 3 5) (mk-sn-bunch 2 4))
	"iso-TN"
	0.4
	)
       )))
 (list (r-transducer) (rx-transducer))
 :file "/tmp/test-rr.html")

(reset-v)
(multi-report-referenced
 (let*
     (
      )
   (labels
       (
	)
     `(
       (
	,(mk-chain (mk-sn 5) (mk-sn 0) (mk-sn 0))
	"TN"
	0.5
	)
       (
	,(mk-chain (mk-sn 0) (mk-sn 0) (mk-sn 5))
	"iso-TN"
	0.5
	)
       (
	,(mk-tree (mk-sn 0) (mk-sn 5))
	"iso-2-TN"
	0.5
	)
       )))
 (list (r-transducer) (rx-transducer))
 :file "/tmp/test-rr.html")

(reset-v)
(multi-report-referenced
 (let*
     (
      )
   (labels
       (
	)
     `(
       (
	,(apply 'mk-bintree (mapcar 'mk-pn '(1 3 1 2 2)))
	"TN"
	0.4
	)
       (
	,(apply 'mk-bintree (mapcar 'mk-pn '(2 2 1 1 3)))
	"iso-TN"
	0.4
	)
       )))
 (list (r-transducer) (rx-transducer))
 :file "/tmp/test-rr.html")

(reset-v)
(multi-report-referenced
 (let*
     (
      )
   (labels
       (
	)
     `(
       (
	,(apply 'mk-bintree (mapcar 'mk-pn '(1 4 1 2 3)))
	"TN"
	0.4
	)
       (
	,(apply 'mk-bintree (mapcar 'mk-pn '(2 3 1 1 4)))
	"iso-TN"
	0.4
	)
       )))
 (list (r-transducer) (rx-transducer))
 :file "/tmp/test-rrc.html")
