; (load "local-projects/trag/Autotree/try-factor.lisp")

(in-package :autotree)

(defun maxima::maxima_q_from_string (s)
  (maxima::mfuncall
   'maxima::$eval_string
   (atriples-to-polynomial-text (q-atriples (sexpr-to-tree-term (read-from-string s))) "u" "v" "e")))

(defun maxima::maxima_q0_from_string (s)
  (maxima::mfuncall
   'maxima::$eval_string
   (atriples-to-polynomial-text
    (q0-atriples
     (sexpr-to-tree-term (read-from-string s))) "u" "v" "e")))

(defun maxima::maxima_q11_from_string (s)
  (maxima::mfuncall
   'maxima::$eval_string
   (atriples-to-polynomial-text
    (q11-atriples
     (sexpr-to-tree-term (read-from-string s))) "u" "v" "e")))

(defun maxima::maxima_q21_from_string (s)
  (maxima::mfuncall
   'maxima::$eval_string
   (atriples-to-polynomial-text
    (q21-atriples
     (sexpr-to-tree-term (read-from-string s))) "u" "v" "e")))

(defun maxima::maxima_r_from_string (s)
  (maxima::mfuncall
   'maxima::$eval_string
   (atriples-to-polynomial-text (r-atriples (sexpr-to-tree-term (read-from-string s))) "u" "v" "e")))

(defun maxima::maxima_rx_from_string (s)
  (maxima::mfuncall
   'maxima::$eval_string
   (atriples-to-polynomial-text
    (rx-atriples
     (sexpr-to-tree-term (read-from-string s))) "u" "v" "e")))

(defun maxima::maxima_factorized_qs_for_size (n)
  (apply
   'maxima::mfuncall 'maxima::mlist
   (mapcar
    (lambda (f) (maxima::mfuncall 'maxima::$factor f))
    (mapcar
     (lambda (s) (maxima::mfuncall 'maxima::$eval_string s))
     (mapcar
      (lambda (x) (atriples-to-polynomial-text x "u" "v" "e"))
      (mapcar 
       'q-atriples
       (collect-enum (tree-term-enumerator n))))))))

(defun maxima-factor (f) (maxima::mfuncall 'maxima::$factor f))

(defun maxima-factor-irreducible (f)
  (find 'maxima::irreducible (first (maxima-factor f))))

(defun maxima-eval-string (s) (maxima::mfuncall 'maxima::$eval_string s))
(defun maxima-to-string (x) (maxima::mfuncall 'maxima::$string x))

(defun maxima-polynomial (term atriples-function &rest names)
  (cond
    ((stringp term)
     (apply 'maxima-polynomial 
            (read-from-string term) atriples-function names))
    ((listp term)
     (apply 'maxima-polynomial 
            (sexpr-to-tree-term term) atriples-function names))
    (t 
     (maxima-eval-string
      (apply 'atriples-to-polynomial-text 
	     (funcall atriples-function term) names)))))

#|

(collect-enum
  (make-mapping-enumerator
    (constantly nil)
    (lambda (x y) x (maxima-to-string y))
    (make-filter-enumerator
      (make-mapping-enumerator 
        (constantly nil)
        (lambda (x y) x
          (maxima::mfuncall
            'maxima::$ratsimp
            (maxima::mfuncall
              'maxima::mtimes
              (maxima-polynomial y 'rx-atriples "u" "v" "e")
              (maxima-eval-string "1/u"))))
        (tree-term-enumerator 5))
      (lambda (x) (maxima-factor-irreducible x)))))

(collect-enum
  (make-mapping-enumerator
    (constantly nil)
    (lambda (x y) x (maxima-to-string y))
    (make-filter-enumerator
      (make-mapping-enumerator 
        (constantly nil)
        (lambda (x y) x
          (maxima::mfuncall
            'maxima::$ratsimp
            (maxima::mfuncall
              'maxima::mtimes
              (maxima-polynomial y 'rx-atriples "u" "v" "e")
              (maxima-eval-string "1/u"))))
        (tree-term-enumerator 5))
      (lambda (x) (not (maxima-factor-irreducible x))))))

(collect-enum
  (make-mapping-enumerator
    (constantly nil)
    (lambda (x y) x (maxima-to-string (maxima-factor y)))
    (make-filter-enumerator
      (make-mapping-enumerator 
        (constantly nil)
        (lambda (x y) x
          (maxima::mfuncall
            'maxima::$ratsimp
            (maxima::mfuncall
              'maxima::$rx_subtree_factor
              (maxima-polynomial y 'rx-atriples "u" "v" "e"))))
        (tree-term-enumerator 10))
      (lambda (x) (not (maxima-factor-irreducible x))))))

(in-package :autotree)

(loop
  with n-hits := 0
  for n-try from 1 to 10000 do
  (let*
    ((term (tree-term-random 6))
     (r (maxima-polynomial term 'r-atriples "u" "v" "e"))
     (rx (maxima-polynomial term 'rx-atriples "u" "v" "e"))
     (rx-stf (maxima::mfuncall 'maxima::$rx_subtree_factor rx))
     (q (maxima-polynomial term 'q-atriples "u" "v" "e"))
     (q0 (maxima-polynomial term 'q0-atriples "u" "v" "e"))
     (rx-stf-irr (maxima-factor-irreducible rx-stf))
     (q-irr (maxima-factor-irreducible q))
     (q0-irr (maxima-factor-irreducible q0))
     )
    (when
      (not
        (and q-irr q0-irr rx-stf-irr))
      (incf n-hits)
      (format t "Term:~%~s~%" term)
      (format t "~s~%" (list rx-stf-irr q-irr q0-irr))
      (format t "Q0: ~a~%" (maxima-to-string (maxima-factor q0))))
    (when (= 0 (mod n-try 250))
      (format t "~a hits of ~a tries~%" n-hits n-try))
    )
  )

|#
