;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin, Irène Durand

(asdf:defsystem
  :autowrite-web
  :description "A web UI for Autowrite"
  :version "0.0"
  :author "Michael Raskin <raskin@mccme.ru>, Irène Durand <irene.durand@u-bordeaux.fr>"
  :depends-on (
               :autowrite :trag-web
               :hunchentoot :cl-emb :smackjack
               :cl-ppcre :esrap-peg :cl-json :alexandria
               :external-program :trivial-backtrace
               :bordeaux-threads :local-time
               )
  :components
  (
   (:file "package")
   (:file "util" :depends-on ("package"))
   (:file "js-definitions" :depends-on ("package"))
   (:file "js-functions" :depends-on ("package" "js-definitions"))
   (:file "lisp-operations" :depends-on ("package" "util"))
   (:file "js-commands" :depends-on ("package" "js-definitions" "js-functions"))
   (:file "commands" :depends-on ("package" "js-definitions" "js-functions" "lisp-operations" "js-commands"))
   (:file "menus" :depends-on ("package" "js-definitions"))
   (:file "page" :depends-on ("package" "js-definitions" "js-functions" "commands" "menus"))
   ))
