(in-package :autowrite-web)

(defcommand-with-backend
  (aw-ids::cwd-automaton-recognizes-p aw-ids::string *autowrite-web-js* :js-name "Recognize term")
  () (term automaton)
  (term-and-automaton-compatible-p)
  (if (termauto:recognized-p term automaton)
    "Recognized" "Not recognized")
  (set-pane-current "result"
                    (create-op-id (get-pane-current "term")
                                  (get-pane-current "automaton"))))
  
(defcommand-with-backend
    (aw-ids::load-trs aw-ids::trs *autowrite-web-js* :js-name "Load trs")
    ((data 'prompt "TRS file" 'allow-upload t))
    ()
    t
    (with-input-from-string (input data) (autowrite::parse-trs input))
    (set-pane-current
     "trs"
     (create-op-id (create-immediate-identifier 'string data))))
