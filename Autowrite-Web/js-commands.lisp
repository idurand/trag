(in-package :autowrite-web)

(add-parenscript-definitions (*autowrite-web-js*)
  ;; Commands
  
  (defcommand "Input the term" ((value 'prompt "Term: "))
    (set-pane-current
     "term"
     (create-immediate-identifier :term value)
     (lambda ()
       (when (get-pane-current "term")
	 (set-pane-current "result" nil nil)))))
  
  (defcommand "NF automaton" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :nf-automaton (list))))
  
  (defcommand "Union" ((second 'prompt "The name of the second automaton"
			       'completion-options (kind-names "automaton")))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :union
      (list (get-pane-current "automaton") second))))
  
  (defcommand "Intersection"
      ((second 'prompt "The name of the second automaton"
	       'completion-options (kind-names "automaton")))
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :intersection (list (get-pane-current "automaton") second))))
  
  (defcommand "Complement" ()
    (set-pane-current
     "automaton"
     (create-operation-identifier
      :cwd-automaton :complement (list (get-pane-current "automaton")))))
  
  (defcommand "Deterministic?" ()
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :cwd-automaton-deterministic-p (list (get-pane-current "automaton")))))

  (defcommand "Non emptiness witness" ((cwd 'prompt "Fix the cwd"))
    (set-pane-current
     "result"
     (create-operation-identifier
      :cwd-term
      :non-emptiness-witness
      (list (get-pane-current "automaton")
	    (create-immediate-identifier aw-ids::integer cwd)))))

  (defcommand "Emptiness" ((cwd 'prompt "Fix the cwd"))
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :automaton-emptiness
      (list (get-pane-current "automaton")))))
  
  (defcommand "Compute the automaton target state on the term" ()
    (set-pane-current
     "result"
     (create-operation-identifier
      :string :cwd-automaton-target-state
      (list (get-pane-current "term") (get-pane-current "automaton")))))
  
  (defcommand "Force-send state to server" () (resend-session-to-server))
  
  (defcommand "Cancel computation" ()
    (server-funcall :session-cancel-request (json-args)
		    (lambda () (default-state)
		      (show-text *message-holder* "Succesfully cancelled"))))
  
  (defcommand "Reset session" ()
    (server-funcall :session-destroy (json-args)
		    (lambda () (default-state) (load-session-top-panes)
			    (show-text *message-holder* "Succesfully cleared"))))
  
  (defcommand "Export session" () (session-export))
  
  (defcommand "Import session" ((data 'prompt "Session data: "
				      'allow-expand t 'allow-upload t))
    (session-import data))
  
  (defcommand "List session names" ()
    (show-text (ps:chain (aref *known-panes* "result") content)
	       (loop for kind in (object-keys *named-objects*)
		     append
		     (append
		      (list (+ "Of kind " kind ": "))
		      (object-keys (aref *named-objects* kind))
		      (list ""))))))
