(in-package :autowrite-web)

(defparameter *uri* "/autowrite")
(pushnew *uri* trag-web:*uris* :test 'equal)

(defvar *ag-session-table* (make-session-table))

(add-parenscript-definitions (*autowrite-web-js* t)
  ;; Panes for displaying data
  (define-pane "trs" "Current TRS ")
  (define-pane "term" "Current term: ")
  (define-pane "automaton" "Current automaton: ")
  (define-pane "result" "Latest result: "))
  
