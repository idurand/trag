(in-package :autowrite-web)

(add-parenscript-definitions (*autowrite-web-js*)
  (defun term-and-automaton-compatible-p ()
    (and
      (get-pane-current "term")
      (get-pane-current "automaton")
      ;; add something
  )))
