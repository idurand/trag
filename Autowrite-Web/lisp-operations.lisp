(in-package :autowrite-web)

(define-object-identifier-operations (aw-ids::term)
  (aw-ids::compute-normal-form (tt trs) (termwrite::compute-normal-form tt trs)))

(define-object-identifier-operations (aw-ids::trs)
  (aw-ids::load-trs (data) (with-input-from-string (input data)
				(termwrite::load-trs-stream input))))

(define-object-identifier-operations (aw-ids::automaton)
  (aw-ids::union (&rest parameters) (termauto:union-automata parameters))
  (aw-ids::intersection (&rest parameters) (termauto:intersection-automata parameters))
  (aw-ids::complement (a) (termauto:complement-automaton a)))

(define-object-identifier-operations (aw-ids::string)
  (aw-ids::cwd-automaton-target-state
   (term automaton)
   (format nil "Target state: ~a" (termauto:compute-final-target term automaton)))

  (aw-ids::cwd-automaton-deterministic-p
   (automaton)
   (if (termauto:deterministic-p automaton) "Deterministic" "Not deterministic"))

  (aw-ids::automaton-emptiness
   (automaton)
   (if (autowrite:automaton-emptiness automaton) "Empty" "Not empty")))
