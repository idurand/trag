;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin, Irène Durand

(in-package :autowrite-web)

(add-parenscript-definitions (*autowrite-web-js*)
  ;; Menus

  (defmenu "Terms menu"
    (menu-command-entry "Input" "Input the term"))

  (defmenu "Automata menu"
    ;;      (menu-submenu-entry "Current automaton" "Current automaton menu")
    (menu-submenu-entry "Application" "Application menu"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-command-entry "Determistic?" "Deterministic?"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-command-entry "Emptiness" "Emptiness"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-command-entry "Non emptiness witness" "Non emptiness witness"
			nil (lambda () (not (get-pane-current "automaton"))))
    (menu-submenu-entry "Standard automata" "Standard automata menu")
    (menu-submenu-entry "Operations" "Automaton operations"
			nil (lambda () (not (get-pane-current "automaton")))))
  
  (defmenu "Application menu"
    (menu-command-entry "Recognize" "Recognize term")
    (menu-command-entry "Target state" "Compute the automaton target state on the term"))

  (defmenu "Standard automata menu"
    (menu-command-entry "NF" "NF automaton"))
  
  (defmenu "Automaton operations"
    (menu-command-entry "Union" "Union")
    (menu-command-entry "Complement" "Complement")
    (menu-command-entry "Intersection" "Intersection"))

  (defmenu "Named objects menu"
    (menu-submenu-entry "Term" "Named term")
    (menu-submenu-entry "Trs" "Named trs")
    (menu-submenu-entry "Automaton" "Named automaton"))
  
  (defmenu "Session menu"
    (menu-command-entry "Cancel computation" "Cancel computation")
    (menu-command-entry "Reset" "Reset session")
    (menu-command-entry "Force to server" "Force-send state to server")
    (menu-command-entry "Export" "Export session")
    (menu-command-entry "Import" "Import session")))
