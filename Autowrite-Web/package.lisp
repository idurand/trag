(defpackage :autowrite-web
  (:use :common-lisp :trag-web)
  (:export))

(defpackage :autowrite-web-ids
  (:use)
  (:nicknames :aw-ids)
  (:import-from :keyword
                :integer :string)
  (:import-from :trag-web-demo-ids))

(defpackage :autowrite-web-ajax
  (:use :trag-web-ajax-handlers)
  (:nicknames :aw-js))
