(in-package :autowrite-web)

(add-parenscript-definitions (*autowrite-web-js*)
  (setf *top-menus*
	(list
	 (menu-submenu-entry "Session" "Session menu")
	 (menu-submenu-entry "Trs" "Trss menu")
	 (menu-submenu-entry "Terms" "Terms menu")
	 (menu-submenu-entry "Automata" "Automata menu")
	 (menu-submenu-entry "Named objects" "Named objects menu")
	 ))

  ;; Initialization to perform on load
  (defun initialize ()))

(define-pane-page
    :uri *uri* :parenscript-code *autowrite-web-js* :ajax-package :aw-js
  :object-identifier-package :aw-ids :remote-session-table *ag-session-table*
  :command-line-help "Enter a command; use Ctrl-Space for completion"
  :page-bottom-message 
  "
  <br/><br/><div style=\"font-size: 0.9em; padding: 0.7em;\">
  This is a <a href=\"https://bitbucket.org/idurand/TRAG/\">TRAG</a> demo.
  This site uses session cookies to maintain correspondence between the client
  and the server side of computations.
  We reserve the right, but do not promise, to collect, store and process
  the constructions entered into the web application, including the operations
  performed on their combinations, and publish the results of such analysis.
  Please do not submit any personally identifiable data or data that should
  not become public.
  </span>
  ")
