;;; As S02 but with extended signature
(defparameter sign2 '((A 0) (B 0) (F 4) (G 1) (M 1) (I 1)))

(defparameter lhs2 (mapcar #'left_handside s2))
(defparameter aut-b2 (make_automaton lhs2 sign2 0))
(defparameter aut-a2 (make_nf_automaton lhs2 sign2 1))
(defparameter aut-c2 (disjoint_union_automaton aut-b2 aut-a2))
(defparameter aut-b2-prime (make_bprime_automaton lhs2 sign2 2))
(defparameter gaut-c2 (g_transform_automaton aut-c2))
(defparameter gaut-c2 (g_saturation s2 gaut-c2))
(defparameter gaut-b2-prime (g_transform_automaton aut-b2-prime))
(defparameter gaut-c2-prime (disjoint_union_automaton gaut-b2-prime gaut-c2))
(defparameter gaut-c2-prime (g_fold_constant_states gaut-c2-prime))
(defparameter gaut-c2-prime
  (g_make_states_equivalent gaut-c2-prime (var_state 0) (var_state 2)))
(defparameter stat 
 (append *firststates*
	 (my_setdifference
	  (get_states gaut-c2-prime) *firststates*)))
(defparameter gaut-c2-prime (change_states_to_automaton stat gaut-c2-prime))
(defparameter gaut-c2-ren (g_renaming gaut-c2-prime))   
(length (get_states gaut-c2-prime))
(length (get_rules gaut-c2-prime))
; 15 states 803 rules
(defparameter gaut-d2 (g_make_d_automaton gaut-c2-ren)) 
(get_finalstates gaut-d2)
(build_one_term_to_state (nth 0 (get_finalstates gaut-d2)) (get_rules gaut-d2) '())
(length (get_states gaut-d2))
(length (get_rules gaut-d2))
; version2 etats regles
