(length s2)
; 151
(defparameter gaut-c2-ren (c_automate sign2 s2))   
(length (get_states gaut-c2-ren))
(length (get_rules gaut-c2-ren))
;  15 states 823 rules
(defparameter gaut-d2 (g_make_d_automaton gaut-c2-ren)) 
(get_finalstates gaut-d2)
; should be NIL
;  etats regles
