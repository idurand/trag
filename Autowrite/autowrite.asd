;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Autowrite.
(in-package :asdf-user)

(defsystem :autowrite
  :description "Autowrite: Term rewriting and Automata"
  :name "autowrite"
  :version "6.0"
  :author "Irene Durand <idurand@labri.fr>"
  :depends-on (:termwrite :trs)
  :serial t
  :components
  (
   (:file "package")
   (:file "specification")
   (:file "parser")
   (:file "input")
   (:file "init-autowrite")
   (:file "com-term")
   (:file "com-termset")
   (:file "com-trs")
   (:file "com-reduction")
   (:file "com-approximation")
   (:file "jeu-de-tests")
   ))

(pushnew :autowrite *features*)
