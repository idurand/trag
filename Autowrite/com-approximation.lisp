;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autowrite)

(defmacro update-approximation (approximation)
  `(progn 
    (setf *approximation* ,approximation)
    (when (trs (current-spec))
      (seqsys-change-approx (trs (current-spec)) ,approximation))))

(defun arbitrary ()
  (let ((trs (trs (current-spec))))
    (when trs
      (or
       (find-if #'arbitrary-rule-p (rules-list (get-approx-rules trs)))
       (and
	(find-if #'collapsing-rule-p (rules-list (get-approx-rules trs)))
	(progn
	  (seqsys-change-aut-t
	   trs
	   (aut-termset (make-termset "Extra" (list (extra-term)))))
	  (let* ((aut1  (automaton-without-extra-symbol (seqsys-aut-c-t trs)))
		 (aut2
		  (let ((signature (signature aut1))
			(lhs  (get-lhs trs)))
		    (make-b-automaton lhs signature :finalstates t))))
	    (prog1
		(multiple-value-bind (res witness)
		    (intersection-emptiness aut1 aut2)
		  (and (not res) witness))
	      (seqsys-change-aut-t trs nil)))))))))

