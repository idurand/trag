;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autowrite)

(defun reduction-step (rp &key (tr t))
  (when tr
    (format-output
     "contracting redex at position ~A~%"
     (position-to-string rp)))
  (let ((new-term (reduction
		   (term (current-spec))
		   rp (rules-of
		       (trs (current-spec))))))
    (setf (previous-term (current-spec)) (term (current-spec)))
    (setf (term (current-spec)) new-term)))

(defun parallel-reduction-step (positions &key (tr t))
  (when tr
    (format-output
	    "contracting redex(es) at position(s) ~A~%" (positions-to-string positions)))
  (let ((initial-term (term (current-spec))))
    (dolist (r positions (term (current-spec)))
      (reduction-step r :tr nil))
    (setf (previous-term (current-spec)) initial-term)))



