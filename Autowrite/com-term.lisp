;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autowrite)

(defun normalizable (term sys &key (extra nil))
    (let* ((signature-term (signature term))
	   (bullet (in-signature-bullet signature-term))
	   (extra-p (in-signature-extra signature-term)))
      (let ((recognized
		 (recognized-p
		  term
		  (seqsys-aut-c sys :extra (or extra extra-p) :bullet bullet))))
	(format-output "~A " term)
	(unless recognized
	  (format-output "not "))
	(format-output
	 "~A-normalizable" (get-approx-name (seqsys-approx sys)))
	(when extra (format-output "@")))))
  
(defmacro term-check-property (property)
  `(unless (null (term (current-spec)))
    (format-output
      "term ~A~A.~%"
      (if (,property (term (current-spec))) "" "not ")
      (string-downcase (symbol-name ',property)))))

(defun term-needed-redexes (term sys &key (extra nil))
    (when term
    (if (merge-signature (signature sys) (signature term))
	(let ((rp (needed-redex-positions term sys :extra extra)))
	  (if (null rp)
	      (format-output
	       "no needed-redex")
	      (format-output
	       "needed redex positions: ~A" (positions-to-string rp))))
	(format t "incompatible signatures"))))

(defun set-current-term (term)
  (setf (previous-term (current-spec)) nil)
  (setf (term (current-spec)) term)
  (add-current-term))
