;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autowrite)

(defun list-termsets ()
  (list-keys (termsets-table (termsets (current-spec)))))

(defun accessibility ()
  (when (and (trs (current-spec)) (termset (current-spec)))
    (let ((signature-term (signature (term (current-spec)))))
      (if (and (termset-merge-signature (termset (current-spec)) signature-term)
	       (seqsys-change-aut-t (trs (current-spec)) (aut-termset (termset (current-spec)))))
	  (progn
	    (format *standard-output* "termset ~A " (name (termset (current-spec))))
	    (if (recognized-p (term (current-spec)) (seqsys-aut-c-t (trs (current-spec))) )
		(prog1 t
		  (format-output "accessible from term ~A" (term (current-spec))))
		(format-output
			"not accessible from term ~A" (term (current-spec)))))
	  (format-output "incompatible signatures~%")))))

