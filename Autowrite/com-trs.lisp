;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autowrite)

(defmacro trs-check-property (property)
  `(progn
    (when (trs (current-spec))
      (format-output
       "~A is ~A~A~%"
       (name (trs (current-spec)))
       (if (,property (rules-of (trs (current-spec)))) "" "not ")
       (string-downcase (symbol-name ',property))))))

(defun display-sequential (res sys &key (extra nil))
  (format-output
   "trs ~A ~Ain CBN~A-~A"
   (name sys)
   (if res "" "not ")
   (if extra "@" "")
   (get-approx-name (seqsys-approx sys))))

(defun display-sequentiality-results (sys aut-d &key extra)
  (let ((res (container-empty-p (get-finalstates aut-d))))
    (display-sequential res sys :extra extra)
    (unless res
      (format-output " free term: ~A" (non-emptiness-witness aut-d)))))

(defun try-to-check-seq-with-properties (ctx)
  (cond
    ((and
      (orthogonal-p (left-handsides (rules-of ctx)))
      (rules-closed-p (get-approx-rules ctx)))
     (values t t))
    (t (values nil ctx))))

(defun trs-check-collapsing (sys)
  (when sys
    (let ((collapsing (collapsing-p (rules-of sys))))
      (format-output "~A is " (name sys))
      (if collapsing
	  (format-output
		  "collapsing witnessed by ~A~%" collapsing)
	  (format-output "not collapsing~%")))))

(defun trs-check-constructor (sys)
  (when sys
    (multiple-value-bind  (constructor witness)
	(constructor-p (rules-of sys))
      (format-output "~A is " (name sys))
      (if constructor
	  (format-output "constructor~%")
	  (format-output "not constructor witnessed by ~A~%" witness)))))

(defun trs-check-forward-branching (sys &key (def nil))
  (when sys
    (multiple-value-bind (fb witness)
	(if def
	    (forward-branching-def sys)
	    (forward-branching-index-tree sys))
      (format-output "~A " (name sys))
      (if fb
	  (format-output "is forward-branching~%")
	  (format-output "not forward-branching ~A~%" witness)))))

(defun trs-check-orthogonal (sys)
  (when (trs (current-spec))
    (let ((orthogonal (and (not (overlapp  (get-lhs sys)))
			   (left-linear (rules-of sys)))))
      (format-output "~A is~:[ not~;~] orthogonal~%" (name sys) orthogonal))))

(defun trs-check-growing (sys)
  (when (trs (current-spec))
    (let ((growing (growing sys)))
      (format-output "~A is~:[ not~;~] growing~%" (name sys) growing))))

(defun trs-check-overlapping (sys)
  (when (trs (current-spec))
    (let ((overlapp (overlapp (get-lhs sys))))
      (format-output
	      "~A is~A overlapping" (name sys) (if overlapp "" " not"))
      (if overlapp
	  (format-output " witnessed by ~A~%" overlapp)
	  (format-output "~%")))))

(defun list-trss ()
  (list-keys (trss-table (trss (current-spec)))))

(defun set-current-trs (trs)
  (setf (trs (current-spec)) trs)
  (add-current-trs))

(defun seqsys-from-rules (name rules)
  (make-seqsys name rules :signature (signature (current-spec))))

(defun nf-empty ()
  (when (trs (current-spec))
    (multiple-value-bind (res term)
	(automaton-emptiness (seqsys-aut-nf (trs (current-spec))))
      (if res
	  (format-output "NF empty")
	  (progn
	    (format-output "NF not empty witnessed by ~A" term))))))

(defun enf-empty ()
  (when (trs (current-spec))
    (multiple-value-bind (res term)
	(inclusion-automaton
	 (seqsys-aut-nf (trs (current-spec)))
	 (make-matching-automaton (get-lhs (trs (current-spec))) (seqsys-signature (trs (current-spec))) :strict t :finalstates t))
      (if res
	  (format-output "ENF empty")
	  (format-output "ENF not empty witnessed by ~A" term)))))
