;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autowrite)

(defun init-autowrite ()
  (setq input-output::*data-relative-directory* "Data/Autowrite/")
  (init-directories)
  (init-termauto)
  (set-current-spec (empty-spec))
  (reset-aux-variables)
  (init-parser)
  (init-variables))
