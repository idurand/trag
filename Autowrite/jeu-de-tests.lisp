;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autowrite)

(defun test-nv-jacquemard (sys)
  (format t "test-nv-jacquemard ")
  (verify-automaton (seqsys-aut-c sys :deterministic nil :bullet t) 4 36)
  (let ((aut-d (seqsys-aut-d sys :deterministic nil :partial t)))
    (assert (container-empty-p (get-finalstates aut-d)))
    (assert (= 17 (container-size (get-states aut-d))))
    (assert (= 7003 (number-of-transitions (transitions-of aut-d))))
    (setf aut-d (seqsys-aut-d  sys :deterministic nil :extra t :partial t))
    (assert (container-contents (get-finalstates (seqsys-aut-d-extra-ndet sys))))
    (format t "passed~%")))

(defun test-nv-toyama (sys)
  (format t "test-nv-toyama ")
  (let ((aut-d (seqsys-aut-d sys)))
    (assert (container-empty-p (get-finalstates aut-d)))
    (verify-automaton (seqsys-aut-d sys :partial t) 14 2238)
    (setf aut-d (seqsys-aut-d sys :extra t :partial t))
    (assert (container-contents (get-finalstates aut-d)))
    (format t "passed~%")))

(defun test-nv () 
  ;; 10.06sec 2011-05-26
  ;; 2014-06-27 22.838sec
  ;; 2018-03-24 20.4sec Mac
  (with-spec (read-autowrite-spec-from-path (absolute-data-filename "S04.txt"))
    (let ((sys (trs (current-spec))))
      (update-approximation 'nv-approximation)
      (test-nv-jacquemard sys)
      (test-nv-toyama sys))))

(defun test-g-toyama (sys)
  (format t "test-g-toyama ")
  (let ((aut-d (seqsys-aut-d sys)))
    (assert (container-empty-p (get-finalstates aut-d)))
    (setf aut-d (seqsys-aut-d sys :extra t :partial t))
    (assert (not (container-empty-p (get-finalstates aut-d))))
    (format t "passed~%")))

(defun test-g () 
  ;; 23.85sec 2011-05-26
  ;; 2014-06-27 37.086sec
  (with-spec (read-autowrite-spec-from-path (absolute-data-filename "S05.txt"))
    (let ((sys (trs (current-spec))))
      (update-approximation 'growing-approximation)
      (test-g-toyama sys))))

(defun test-s3-toyama ()
  (with-spec (read-autowrite-spec-from-path (absolute-data-filename "S03.txt"))
    (let ((sys (trs (current-spec))))
      (update-approximation 'growing-approximation)
      ;;      (defparameter termwrite::*sys* sys)
      (format t "test-s3-toyama ")
      (let ((aut-c (seqsys-aut-c sys :deterministic t)))
	(verify-automaton aut-c 4 6)
	(let ((aut-d (seqsys-aut-d sys :deterministic t)))
	  (assert (container-empty-p (get-finalstates aut-d)))
	  (verify-automaton aut-d 17 49)))
      (format t "passed~%"))))

(defun test-call-by-need () 
  ;; 2011-05-26 33.89sec
  ;; 2014-06-27 1min 0.24sec
  (test-s3-toyama)
  (test-nv)
  (test-g))

(defun test-forward-branching () ;;
  (format t "test forward-branching ~%")
  (let* ((spec (read-autowrite-spec-from-path (absolute-data-filename "fb.txt")))
	 (trs (trs spec)))
    (assert (forward-branching-def trs))
    (assert (forward-branching-index-tree trs))))

(defun test-automaton ()
  (let* ((spec (read-autowrite-spec-from-path (absolute-data-filename "S05.txt")))
	 (trs (trs spec)))
    (make-redex-automaton  (get-lhs trs) (signature trs))))

(defun test-automata () 
  ;;  4min 17.0sec 2011-06-16
  ;; 2014-06-27 30.284sec
  (format t "test automata ~%")
  (let ((a (test-automaton)))
    (verify-automaton a 17 23)
    (verify-automaton (minimize-automaton a) 4 278)
    (format t " emptiness ~% ")
    (assert (not (automaton-emptiness a)))
    (format t " disjoint-union ~%")
    (let ((ud (disjoint-union-automaton a (duplicate-automaton a))))
      (verify-automaton ud 33 46)
      (format t " determinize ~%")
      (let ((d (determinize-automaton a)))
 	(verify-automaton d 16 65618)
 	(format t " minimize ~%")
 	(let ((s (minimize-automaton d)))
 	  (verify-automaton s 4 278)
 	  (format t " intersection emptiness ~%")
 	  (assert (intersection-emptiness s (complement-automaton s)))
	  (format t " intersection ~%")
	  (let ((i (intersection-automaton a (duplicate-automaton a))))
	    (verify-automaton i 47 53)
	    (verify-automaton (minimize-automaton i) 4 278))))))
  (format t "passed~%"))

(defun test-autowrite () 
  ;; 9min 42sec 2011-08-16
  ;; 1min 12.46sec ??
  ;; 37.592sec 2013-08-05
  ;; 2014-06-27 1min 31.315sec
  ;; 2016-05-27 53.953sec (shenzhou ac)
  ;; 2018-02-14 26.18sec (soyouz SBCL 1.3.13 debug 3)
  (init-autowrite)
  (test-call-by-need)
  (test-automata)
  (test-forward-branching))
