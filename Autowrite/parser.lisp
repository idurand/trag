;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autowrite)
;; <spec> ::= <ops> [<vars>] <truc>*
;; <var> ::= <memorized-name>
;; <truc> ::= <trs> | <automaton> | <termset> | <termdef>
;; <set> ::= Termset <name> <term>*
;; <termdef> ::= Term <term>
;; <term> ::= <memorized-name> [<arglist>]
;; <flat-term> ::= <memorized-name> [<state arglist>]
;; <arglist> ::= ( <arg> <args> )
;; <args> ::= <empty> | , <arg> <args>
;; classes : kwd sym separator string

;; constants : +sep-open-par+ +sep-closed-par+ +sep-colon+ +sep-arrow+ +sep-comma+ +sep-open-bracket+ +sep-closed-bracket+

(defun parse-autowrite-spec (stream)
  (init-parser)
  (let ((termsets '())
	(tautomata ())
	(terms ())
	(trss ()))
    (parse-symbols stream)
    (when *current-token*
      (when (my-keyword-p "Vars")
	(lexmem stream)
	(parse-var stream)
	(parse-vars stream)
	))
    (do ()
	((or (my-keyword-p "Eof")
	     (not (eq (type-of *current-token*) 'kwd)))
	 (make-autowrite-spec (make-signature (all-symbols))
			      :trss (nreverse trss)
			      :automata (nreverse *automata*)
			      :tautomata (nreverse tautomata)
			      :termsets (nreverse termsets)
			      :terms (nreverse terms)))
      ;;      (format *error-output* "current token ~A type ~A~%" *current-token* (type-of *current-token*))
      (cond
	((my-keyword-p "TRS") (push (parse-trs stream) trss))
	((my-keyword-p "Automaton") (push (scan-automaton stream) *automata*))
	((my-keyword-p "Tautomaton") (push (scan-tautomaton stream) tautomata))
	((my-keyword-p "Termset") (push (parse-named-termset stream) termsets))
	((my-keyword-p "Term") (push (parse-term stream) terms))
	(t (error 'parser-error :token *current-token* "unknown keyword"))))))

(defun parse-trs-vars (stream)
  (lexmem stream)
  (when (my-keyword-p "Vars")
    (lexmem stream)
    (loop
      while (symbol-or-string-p)
      do (progn
	   (parse-var stream)
	   (when (eq *current-token* +sep-comma+)
	     (lexmem stream))))))

(defun parse-trs-spec (stream)
  (init-parser)
  (parse-trs-vars stream)
  (if (my-keyword-p "TRS")
      (let* ((*warn-new-symbol* nil)
	     (trs (parse-trs stream)))
	(rename-object
	 (make-autowrite-spec (signature trs) :trss (list trs))
	 (format nil "~A-spec" (name trs))))
      (empty-spec)))

(defun parse-aut-spec (stream)
  (init-parser)
  (let ((*warn-new-symbol* nil)
	(aut (parse-automaton stream)))
    (rename-object
     (make-autowrite-spec (signature aut) :automata (list aut))
     (format nil "~A-spec" (name aut)))))
