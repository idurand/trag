;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :autowrite)

(defvar *loadable* nil)

(defclass autowrite-spec (termauto-spec)
  ((vars :initform nil :initarg :vars :accessor vars)
   (trs :initform nil :initarg :trs :accessor trs)
   (trss :initform (make-trss) :accessor trss)
   ))

(defun add-trs (trs spec)
  (setf (gethash (name trs) (trss-table (trss spec))) trs))

(defun add-current-trs ()
  (add-trs (trs (current-spec)) (current-spec)))

(defun fill-autowrite-spec (spec &key (trss nil) (termsets nil) (automata nil) (tautomata nil) (terms nil))
  (loop for trs in trss
     do (add-trs trs spec))
  (loop for termset in termsets
     do (add-termset termset spec))
  (loop for automaton in automata
     do (add-automaton automaton spec))
  (loop for tautomaton in tautomata
     do (add-tautomaton tautomaton spec))
  (loop for term in terms
     do (add-term term spec))
  spec)
  
(defun make-autowrite-spec
    (signature
     &key (trss nil) (termsets nil) (automata nil) (tautomata nil) (terms nil))
  (setf trss (mapcar #'make-seqsys-from-trs trss))
  (let ((spec (make-instance 'autowrite-spec
			     :name "unknown"
		 :signature signature
		 :trs (car trss)
		 :term (car terms)
		 :termset (car termsets)
		 :automaton (car automata)
		 :tautomaton (car tautomata))))
    (fill-autowrite-spec spec :trss trss :termsets termsets :automata automata :tautomata tautomata :terms terms)))

(defun empty-spec ()
  (rename-object
   (make-autowrite-spec (make-signature '()))
   "empty"))

(defun load-autowrite-spec (stream)
  (handler-case
      (parse-autowrite-spec stream)
    (parser-error (c)
      (prog1 nil (format *error-output* "load-autowrite-spec parse error ~A: ~A~%" (parser-token c) (parser-message c))))
    (error (c)
      (prog1 nil (format *error-output* "error ~A in loading spec~%" c)))))

(defun read-autowrite-spec-from-path (path)
  (let ((spec (load-file-from-absolute-filename path #'load-autowrite-spec)))
    (when spec
      (name-spec spec (file-namestring path)))))

(defun write-trs (trs stream)
    (format stream "TRS ")
    (write-name (name trs) stream)
    (format stream "~%")
    (format stream "~A~%" (rules-of trs)))

(defun write-trss (spec stream)
  (maphash (lambda (key value)
	     (declare (ignore key))
	     (write-trs value stream))
	   (trss-table (trss spec))))

(defmethod write-spec :after ((spec autowrite-spec) stream)
  (when (trs spec)
    (let ((vars (vars-of (rules-of (trs spec)))))
      (when vars
	(format stream "Vars ")
	(display-sequence vars stream)
	(format stream "~%~%"))))
  (write-trss spec stream)
  (format stream "~%")
  )

(defun back-up-file (file)
  (when (probe-file file)
    (let ((nfile (concatenate 'string file ".bak")))
	(back-up-file nfile)
	(rename-file file nfile))))

(defun save-spec (file)
  (back-up-file (absolute-data-filename file))
  (with-open-file (foo (absolute-data-filename file) :direction :output)
    (and foo (progn (write-current-spec foo) t))))
