(in-package :clique-width)

;;; optimal decomposition cwd=8

(defun dodecahedron ()
  (input-cwd-term 
   "add_d_g(add_b_f(oplus(ren_g_d(ren_f_c(ren_h_b(add_c_g(add_e_f(add_b_f(oplus(ren_g_a(ren_f_a(add_b_f(add_c_g(oplus(ren_c_a(ren_b_a(add_b_h(add_b_d(add_b_c(oplus(ren_b_a(add_b_h(oplus(ren_h_g(add_d_h(oplus(ren_d_a(add_d_e(add_e_g(add_b_f(add_d_f(oplus(oplus(ren_f_a(ren_e_a(add_f_g(oplus(ren_g_e(add_d_g(add_b_e(add_e_g(add_f_g(add_c_e(add_a_f(add_a_c(add_a_h(oplus(oplus(oplus(oplus(oplus(oplus(oplus(a[1],e[4]),g[8]),d[12]),h[14]),c[15]),f[16]),b[20])))))))))),g[7])))),f[6]),e[11])))))),d[9]))),h[18]))),b[10])))))),oplus(b[2],c[3])))))),add_f_g(oplus(f[5],g[19]))))))))),add_f_g(oplus(f[13],g[17])))))"))
