(in-package :clique-width)

;; optimal cwd=8

;;add_5_6(add_2_4(add_0_3(oplus*(oplus*(oplus*(ren_4_1(ren_5_1(ren_3_1(add_4_7(add_3_5(oplus*(ren_7_2(ren_5_1(add_2_5(oplus*(ren_2_1(add_1_7(oplus*(ren_7_2(add_2_7(oplus*(add_5_6(add_4_5(add_3_4(add_2_3(add_1_2(add_0_6(add_0_1(oplus*(oplus*(oplus*(oplus*(oplus*(oplus*(0-1,1-2),2-3),3-4),4-11),5-12),6-24)))))))),add_1_4(add_4_6(add_6_7(add_5_7(add_0_5(add_1_3(add_0_3(oplus*(oplus*(oplus*(oplus*(oplus*(oplus*(3-6,0-7),5-14),7-15),6-16),4-17),1-18))))))))))),7-19))),2-13)))),add_2_6(add_0_2(add_5_6(oplus*(oplus*(add_0_7(oplus*(oplus*(5-5,0-9),7-10)),2-21),6-22)))))))))),3-8),4-20),5-23))))

(defun mcgee ()
  (cwd-term-unoriented-add
   5 6
   (cwd-term-unoriented-add
    2 4
    (cwd-term-unoriented-add
     0 3
     (cwd-term-multi-oplus 
      (cwd-term-ren
       4 1
       (cwd-term-ren
	5 1
	(cwd-term-ren
	 3 1
	 (cwd-term-unoriented-add
	  4 7
	  (cwd-term-unoriented-add
	   3 5 
	   (cwd-term-oplus 
	    (cwd-term-ren
	     7 2
	     (cwd-term-ren
	      5 1
	      (cwd-term-unoriented-add
	       2 5 
	       (cwd-term-oplus 
		(cwd-term-ren
		 2 1
		 (cwd-term-unoriented-add
		  1 7 
		  (cwd-term-oplus 
		   (cwd-term-ren
		    7 2
		    (cwd-term-unoriented-add
		     2 7 
		     (cwd-term-oplus 
		      (cwd-term-unoriented-add
		       5 6
		       (cwd-term-unoriented-add
			4 5
			(cwd-term-unoriented-add
			 3 4
			 (cwd-term-unoriented-add
			  2 3
			  (cwd-term-unoriented-add
			   1 2
			   (cwd-term-unoriented-add
			    0 6
			    (cwd-term-unoriented-add
			     0 1 
			     (cwd-term-multi-oplus-list
			      (mapcar #'cwd-term-num-vertex '(0 1 2 3 4 5 6) '(1 2 3 4 11 12 24))))))))))
		      (cwd-term-unoriented-add
		       1 4
		       (cwd-term-unoriented-add
			4 6
			(cwd-term-unoriented-add
			 6 7
			 (cwd-term-unoriented-add
			  5 7
			  (cwd-term-unoriented-add
			   0 5
			   (cwd-term-unoriented-add
			    1 3
			    (cwd-term-unoriented-add
			     0 3 
			     (cwd-term-multi-oplus
			      (cwd-term-num-vertex 3 6)
			      (cwd-term-num-vertex 0 7)
			      (cwd-term-num-vertex 5 14)
			      (cwd-term-num-vertex 7 15)
			      (cwd-term-num-vertex 6 16)
			      (cwd-term-num-vertex 4 17)
			      (cwd-term-num-vertex 1 18))))))))))))
		   (cwd-term-num-vertex 7 19))))
		(cwd-term-num-vertex 2 13)))))
	    (cwd-term-unoriented-add
	     2 6
	     (cwd-term-unoriented-add
	      0 2
	      (cwd-term-unoriented-add
	       5 6 
	       (cwd-term-multi-oplus
		(cwd-term-unoriented-add
		 0 7 
		 (cwd-term-multi-oplus
		  (cwd-term-num-vertex 5 5)
		  (cwd-term-num-vertex 0 9)
		  (cwd-term-num-vertex 7 10)))
		(cwd-term-num-vertex 2 21)
		(cwd-term-num-vertex 6 22)))))))))))
      (cwd-term-num-vertex 3 8) 
      (cwd-term-num-vertex 4 20) 
      (cwd-term-num-vertex 5 23))))))
  
;; 2013-11-13
