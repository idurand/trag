(in-package :clique-width)

;; optimal cwd=5
(defun petersen ()
  (let* ((t1 (cwd-term-unoriented-add 1 2 (cwd-term-unoriented-add
			       0 1
			       (cwd-term-multi-oplus
				(cwd-term-num-vertex 0 1)
				(cwd-term-num-vertex 1 3)
				(cwd-term-num-vertex 2 5)))))
	   (t2 (cwd-term-unoriented-add 1 4 (cwd-term-unoriented-add
			       1 3
			       (cwd-term-multi-oplus
				(cwd-term-num-vertex 3 2)
				(cwd-term-num-vertex 4 6)
				(cwd-term-num-vertex 1 7)))))
	   (t3 (cwd-term-ren
		4 2
		(cwd-term-ren
		 3 0
		 (cwd-term-unoriented-add 2 3 (cwd-term-unoriented-add 0 4 (cwd-term-oplus t1 t2))))))
	   (t4 (cwd-term-unoriented-add 2 4 (cwd-term-unoriented-add 2 3
					      (cwd-term-multi-oplus
					       (cwd-term-num-vertex 3 4)
					       (cwd-term-num-vertex 4 8)
					       (cwd-term-num-vertex 2 9)))))
	   (t5 (cwd-term-ren 4 3 (cwd-term-unoriented-add 1 4 (cwd-term-unoriented-add 0 3 (cwd-term-oplus t3 t4))))))
      (cwd-term-unoriented-add 2 4 (cwd-term-oplus t5 (cwd-term-num-vertex 4 10)))))
