;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)
;;; pairs don't handle orientation
;;; in the oriented case, we may store (0 1) and/or (1 0)
;;; in the non-oriented case, we should store (0 1) and (1 0)
;;; orientation will be handled in the ports-relation

(defclass pairs (ordered-container) ()
  (:documentation "container for ordered-pairs"))

(defmethod container-order-fun ((pairs pairs)) #'pair<)
(defmethod container-value-test ((pairs pairs)) #'equal)

(defmethod ports-of ((pairs pairs))
  (let ((ports '()))
    (container-map (lambda (pair)
		     (push (first pair) ports)
		     (push (second pair) ports))
		   pairs)
    (make-ports ports)))

(defgeneric make-pairs (pairs))
(defmethod make-pairs ((pairs list))
  (make-container-generic pairs 'pairs #'equal))

(defgeneric make-empty-pairs ())
(defmethod make-empty-pairs ()
  (make-pairs '()))

(defgeneric pairs-edges (pairs))
(defmethod pairs-edges ((pairs pairs))
  (let ((edges '()))
    (container-map
     (lambda (pair)
       (push (sorted-pair-from-pair pair) edges))
     pairs)
    (make-pairs edges)))

(defun pair-to-string (pair oriented)
  (format nil "~A~A~A"
	  (port-to-string (first pair))
	  (port-to-string (second pair))
	  (if (or oriented (not *print-object-readably*)) "" "*")))

;; dans le cas non oriente, on met dans un pairs à la fois (a b) et (b a).
;; mais a l'impression on compacte 
(defgeneric pairs-to-string (pairs oriented))
(defmethod pairs-to-string ((pairs pairs) oriented)
  (unless oriented
    (setf pairs (pairs-edges pairs)))
  (let ((s ""))
    (mapc
     (lambda (pair)
       (setf s
	     (if (equal s "")
		 (pair-to-string pair oriented)
		 (format nil "~A-~A" s (pair-to-string pair oriented)))))
       (container-contents pairs))
    (format nil "{~A}" s)))

(defun print-pairs (pairs stream)
  (format stream "~A" (pairs-to-string pairs *oriented*)))

(defmethod print-object :around ((pairs pairs) stream)
  (if *char-ports*
      (print-pairs pairs stream)
      (call-next-method)))

(defgeneric pairs-subsetp (pairs1 pairs2))
(defmethod pairs-subsetp ((pairs1 pairs) (pairs2 pairs))
  (container-subset-p pairs1 pairs2))

(defgeneric pairs-remove (pair pairs))
(defmethod pairs-remove ((pair cons) (pairs pairs))
  (container-remove pair pairs))

(defgeneric pairs-delete (pair pairs))
(defmethod pairs-delete ((pair cons) (pairs pairs))
  (container-delete pair pairs))

(defgeneric pairs-empty-p (pairs))
(defmethod pairs-empty-p ((pairs pairs))
  (container-empty-p pairs))

(defgeneric pairs-nunion (pairs1 pairs2))
(defmethod pairs-nunion ((pairs1 pairs) (pairs2 pairs))
  (container-nunion pairs1 pairs2))

(defgeneric pairs-union (pairs1 pairs2))
(defmethod pairs-union ((pairs1 pairs) (pairs2 pairs))
  (container-union pairs1 pairs2))

(defgeneric pairs-difference (pairs1 pairs2))
(defmethod pairs-difference ((pairs1 pairs) (pairs2 pairs))
  (container-difference pairs1 pairs2))

(defgeneric pairs-intersection (pairs1 pairs2))
(defmethod pairs-intersection ((pairs1 pairs) (pairs2 pairs))
  (container-intersection pairs1 pairs2))

(defgeneric pairs-subset-p (pairs1 pairs2))
(defmethod pairs-subset-p ((pairs1 pairs) (pairs2 pairs))
  (container-subset-p pairs1 pairs2))

(defgeneric pairs-member (pair pairs))
(defmethod pairs-member ((pair cons) (pairs pairs))
  (container-member pair pairs))

(defgeneric pairs-nadjoin (pair pairs))
(defmethod pairs-nadjoin ((pair cons) (pairs pairs))
  (container-nadjoin pair pairs))

(defgeneric pairs-subst (new old pairs))
(defmethod pairs-subst (new old (pairs pairs))
  (let ((newpairs (make-empty-pairs)))
    (container-map
     (lambda (pair)
       (container-nadjoin
	(subst new old pair)
	newpairs))
     pairs)
    newpairs))

(defgeneric pairs-copy (pairs))
(defmethod pairs-copy ((pairs pairs))
  (container-mapcar-to-container
   #'copy-list
   pairs))

(defgeneric pairs-adjoin (pair pairs))
(defmethod pairs-adjoin ((pair cons) (pairs pairs))
  (pairs-nadjoin pair (pairs-copy pairs)))

;;  (container-copy pairs))

(defgeneric pairs-remove-if (pred pairs))
(defmethod pairs-remove-if (pred (pairs pairs))
  (container-remove-if pred pairs))

(defgeneric pairs-remove-if-not (pred pairs))
(defmethod pairs-remove-if-not (pred (pairs pairs))
  (container-remove-if-not pred pairs))

(defun carre (a b oriented)
  (if oriented
      (list (list a b))
      (list (list a b) (list b a))))

(defun pairs-carre (a b oriented)
  (make-pairs (carre a b oriented)))

(defun point (alpha pairs)
  (remove-if-not
   (lambda (x)
     (some
      (lambda (couple)
	(let ((a (first couple))
	      (b (second couple)))
	  (and (member a alpha :test #'=) (= b x))))
      pairs))
   (mapcar #'second pairs)))

(defgeneric container-point (ports arcs))
(defmethod container-point ((alpha ports) (arcs pairs))
  (make-ports
   (point
    (container-unordered-contents alpha)
    (container-unordered-contents arcs))))

(defgeneric container-point-point (ports arcs1 arcs2))
(defmethod container-point-point ((ports ports) (arcs1 pairs)
				  (arcs2 pairs))
  (make-ports
   (point
    (point (container-unordered-contents ports) (container-unordered-contents arcs1))
    (container-unordered-contents arcs2))))

(defgeneric pairs-nadjoin-according-to-oriented (pair pairs oriented))
(defmethod pairs-nadjoin-according-to-oriented ((pair list) (pairs pairs) oriented)
  (pairs-nadjoin pair pairs)
  (unless oriented
    (pairs-nadjoin (reverse pair) pairs)))

(defgeneric pairs-adjoin-according-to-oriented (pair pairs oriented))
(defmethod pairs-adjoin-according-to-oriented ((pair list) (pairs pairs) oriented)
  (let ((new-pairs (pairs-adjoin pair pairs)))
    (unless oriented
      (pairs-nadjoin (reverse pair) pairs))
    new-pairs))

(defgeneric make-pairs-according-to-oriented (pairs oriented))
(defmethod make-pairs-according-to-oriented ((pairs list) oriented)
  (let ((c (make-empty-pairs)))
    (loop for pair in pairs
	  do (pairs-nadjoin-according-to-oriented pair c oriented))
    c))

(defgeneric brond (arcs1 arcs2 oriented))
(defmethod brond ((arcs1 pairs) (arcs2 pairs) oriented)
  (let ((arcs (make-empty-pairs)))
    (container-map
     (lambda (couple1)
       (let ((origin (first couple1))
	     (extrem (second couple1)))
	 (container-map
	  (lambda (couple2)
	    (when (= extrem (first couple2))
	      (let ((arc (list origin (second couple2))))
		(pairs-nadjoin-according-to-oriented arc arcs oriented))))
	  arcs2)))
     arcs1)
    arcs))

(defgeneric new-pairs-for-add (a b pairs oriented)
  (:documentation "new paths between ports after addition of arc(edge) a--b "))

(defmethod new-pairs-for-add (a b (arcs pairs) oriented)
  (let* ((carre (pairs-carre a b oriented))
	 (newarcs (brond carre arcs oriented)))
    (pairs-nunion newarcs arcs)
    (pairs-nadjoin-according-to-oriented (list a b) newarcs oriented)
    newarcs))
