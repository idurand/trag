;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defun cycle-chromatic-polynomial (k n)
  (+ (expt (1- k) n)
     (*
      (if (evenp n) 1 -1)
      (1- k))))

(defun tree-chromatic-polynomial (k n)
  (* k (expt (1- k) (1- n))))

(defun kn-chromatic-polynomial (k n)
  (loop
    for p = k then (* p (decf k))
    repeat (1- n)
    finally (return p)))

(defun brinkmann-chromatic-polynomial (k)
  (let* ((k2 (* k k))
	 (k3 (* k2 k))
	 (k4 (* k3 k))
	 (k5 (* k4 k))
	 (k6 (* k5 k))
	 (a (+ k3 (- k2) (- (* 2 k) 1)))
	 (b (+ k6 (* 3 k5) (- (* 8 k4) (- (* 21 k3)) (* 27 k2) (* 38 k) -41))))
    (*
     (- k 4)
     (- k 2)
     (+ k 2)
     (* a a)
  (* b b))))
