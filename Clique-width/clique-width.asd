;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Clique-Width.
(in-package :asdf-user)

(defsystem :clique-width
  :description "clique-width terms"
  :name "clique-width"
  :version "6.0"
  :author "Irene Durand <idurand@labri.fr>"
  :depends-on (:terms)
  :serial t
  :components
  ((:file "package")
   (:file "port")
   (:file "ports")
   (:file "port-mixin")
   (:file "port-mapping")
   (:file "graph-objects")
   (:file "beta-rel")
   (:file "ports-relation")
   (:file "cwd-symbols")
   (:file "cwd-signature")
   (:file "cwd-term")
   (:file "compute-cwd-terms")
   (:file "recycle")
   (:file "light-cwd-term")
   (:file "port-fun")
   (:file "colors")
;;   (:file "fixed-color")
;;   (:file "fixed-colors")
   (:file "color-fun")
   (:file "port-int-fun")
   (:file "count-fun")
   (:file "degre-fun")
   (:file "input")
   (:file "output")
   (:file "chromatic-polynomial")
   (:file "petersen")
   (:file "Data/petersen")
   (:file "Data/mcgee")
   (:file "Data/dodecahedron")
   (:file "bipartite")
   (:file "parse-port")
   (:file "incidence-ports")
  ))

(pushnew :clique-width *features*)
