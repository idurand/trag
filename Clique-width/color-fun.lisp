;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass color-fun (port-fun) ())

(defmethod compare-object ((color-fun1 color-fun) (color-fun2 color-fun))
  (hash-table-equal-p (hfun color-fun1) (hfun color-fun1) #'compare-object))

(defun make-color-fun (&optional (color-fun-type 'color-fun))
  (make-port-fun color-fun-type))

(defgeneric add-color-to-port (color port color-fun))
(defmethod add-color-to-port (color (port integer) (color-fun color-fun))
  (set-port-value
   (colors-nadjoin color (get-port-value port color-fun))
   port
   color-fun)
  color-fun)

(defgeneric make-port-color-fun (color port))
(defmethod make-port-color-fun ((color integer) (port integer))
  (add-color-to-port color port (make-color-fun 'color-fun)))

(defmethod set-port-value :before ((colors colors) (port integer) (color-fun color-fun))
  (setq colors (colors-copy colors)))  
	  
(defmethod default-value ((color-fun color-fun))
  (make-empty-colors))

(defgeneric ren-color-fun (a b color-fun))
(defmethod ren-color-fun (a b (color-fun color-fun))
  (let ((new-color-fun (copy-port-fun color-fun)))
    (set-port-value
     (colors-union
      (get-port-value a new-color-fun)
      (get-port-value b color-fun))
     b
     new-color-fun)
    (reset-port-value a new-color-fun)
    new-color-fun))

(defun nmerge-strict-color-fun (color-fun1 color-fun2)
  (maphash
   (lambda (port2 colors2)
     (set-port-value
      (colors-union (get-port-value port2 color-fun1) colors2)
      port2
      color-fun1))
   (hfun color-fun2))
  color-fun1)

(defun nmerge-color-fun (color-fun1 color-fun2)
  (maphash
   (lambda (port2 colors2)
     (set-port-value
      (colors-union (get-port-value port2 color-fun1) colors2)
      port2
      color-fun1))
   (hfun color-fun2))
  color-fun1)

(defun merge-color-fun (color-fun1 color-fun2)
  (nmerge-color-fun (copy-port-fun color-fun1) color-fun2))

(defun merge-color-fun-gen (color-funs)
  (assert color-funs)
  (loop
    with color-fun1 = (copy-port-fun (pop color-funs))
    for color-fun2 in color-funs
    do (nmerge-color-fun color-fun1 color-fun2)
    finally (return color-fun1)))

(defgeneric colors-of (color-fun))
(defmethod colors-of ((color-fun color-fun))
  (container-contents (container-union-gen (list-values (hfun color-fun)))))

(defmethod apply-port-mapping ((port-mapping port-mapping) (color-fun color-fun))
  (let ((new-color-fun (copy-port-fun color-fun)))
    (loop
      for pair in (mapping-pairs port-mapping)
      do 
	 (let ((a (car pair))
	       (b (cdr pair)))
	   (set-port-value
	    (colors-union
	     (get-port-value a new-color-fun)
	     (get-port-value b color-fun))
	    b
	  new-color-fun)
	 (reset-port-value a new-color-fun)))
    new-color-fun))

