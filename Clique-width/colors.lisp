;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass colors (ordered-container) ())

(defmethod print-object ((colors colors) stream)
  (display-sequence (container-contents colors) stream :sep ","))

(defgeneric make-colors (colors))

(defmethod make-colors ((colors list))
  (make-container-generic colors 'colors #'eql))

(defgeneric make-empty-colors ())
(defmethod make-empty-colors ()
  (make-colors '()))

(defgeneric colors-intersection (colors1 colors2))
(defmethod colors-intersection ((colors1 colors) (colors2 colors))
  (container-intersection colors1 colors2))

(defgeneric colors-intersection-empty-p (colors1 colors2))

(defmethod colors-intersection-empty-p ((colors1 colors) (colors2 colors))
  (container-intersection-empty-p colors1 colors2))

(defgeneric colors-empty-p (colors))
(defmethod colors-empty-p ((colors colors))
  (container-empty-p colors))

(defgeneric colors-nadjoin (color colors))
(defmethod colors-nadjoin (color (colors colors))
  (container-nadjoin color colors))

(defgeneric colors-union (colors1 colors2))
(defmethod colors-union ((colors1 colors) (colors2 colors))
  (container-union colors1 colors2))

(defgeneric colors-union-gen (colorss))
(defmethod colors-union-gen ((colorss list))
  (container-union-gen colorss))

(defgeneric colors-nunion (colors1 colors2))
(defmethod colors-nunion ((colors1 colors) (colors2 colors))
  (container-nunion colors1 colors2))

(defgeneric colors-copy (colors))
(defmethod colors-copy ((colors colors))
  (container-copy colors))
