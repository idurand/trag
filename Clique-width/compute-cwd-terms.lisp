;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defun cwd-term-vertex (i &optional vbits)
  (let ((s (make-cwd-constant-symbol i)))
    (when vbits
      (setq s (make-vbits-symbol s vbits)))
    (build-term s)))

(defun cwd-term-num-vertex (i num &optional vbits)
  (set-term-eti (cwd-term-vertex i vbits) num))

(defun vertex (term-or-num)
  (if (integerp term-or-num)
      (cwd-term-vertex term-or-num)
      term-or-num))

(defun only-oplus-in (term)
  (endp (cdr (signature-symbols (non-constant-signature (signature term))))))

(defun normalize-oplus (term)
  (if (only-oplus-in term)
      (reduce #'cwd-term-oplus
	      (sort (leaves-of-term term) #'<
		    :key (lambda (c) (port-of (root c)))))
      term))

(defun cwd-term-oplus (term1 term2)
  (let ((subterms (mapcar #'vertex (list term1 term2))))
    (cond
      ((cwd-term-empty-p term1) term2)
      ((cwd-term-empty-p term2) term1)
      (t (build-term (oplus-symbol) subterms)))))

(defun cwd-term-ac-oplus (terms)
  (cond ((endp terms)
	 (cwd-term-empty-graph))
	((endp (cdr terms))
	 (car terms))
	(t (if *ac-oplus*
	       (build-term (oplus-symbol) terms)
	       (reduce #'cwd-term-oplus terms)))))

(defun cwd-term-binary-oplus (terms)
  (assert terms)
  (if (null (cdr terms))
      (car terms)
      (reduce (lambda (t1 t2)
		(cwd-term-oplus t1 t2))
	      terms)))

(defun cwd-term-multi-oplus-list (terms)
  (assert (every (lambda (term) (or (abstract-term-p term) (integerp term)))
		 terms))
  (assert terms)
  (cond
    ((null (cdr terms))
     (first terms))
    ((null (cddr terms))
     (cwd-term-oplus (first terms) (second terms)))
    (t
     (cwd-term-ac-oplus terms))))

(defun cwd-term-multi-oplus (&rest terms)
  (cwd-term-multi-oplus-list terms))

(defun apply-oplus (&rest terms)
  (setf terms (remove nil terms))
  (and terms
       (if (null (cdr terms))
	   (car terms)
	   (if (null (cddr terms))
	       (apply #'cwd-term-oplus terms)
	       (cwd-term-multi-oplus-list terms)))))

(defun cwd-term-stable (n &optional (l (make-list n :initial-element 0)))
  (cwd-term-multi-oplus-list (mapcar #'cwd-term-vertex l)))

(defun cwd-term-stable-cwd (n)
  (cwd-term-stable n (port-iota n)))

(defun apply-build-term (root &rest terms)
  (build-term root terms))

(defun cwd-term-add (x y term oriented)
  (if (and *simplify-cwd-term*
	   (or
	    (let ((ports (graph-ports-of term)))
	      (or (not (ports-member x ports)) (not (ports-member y ports))))
	    (and (add-symbol-p (root term))
	     (symbol-equal (root term) (add-symbol x y :oriented oriented)))))
      term
      (build-term (add-symbol x y :oriented oriented) (list term))))

(defun cwd-term-oriented-add (x y term)
  (cwd-term-add x y term t))

(defun cwd-term-unoriented-add (x y term)
  (cwd-term-add x y term nil))

(defun cwd-term-ren (x y term)
  (build-term (ren-symbol x y) (list term)))

(defun cwd-term-conditional-ren (x y term)
  (if (= x y)
      term
      (let ((ports (graph-ports-of term)))
	(if (ports-member x ports)
	    (if (and *simplify-cwd-term* (only-oplus-in term))
		;; only oplus (no adds or rens
		;;(term-constant-p term))
		(apply-port-mapping (make-port-mapping (list (cons x y))) term)
		(cwd-term-ren x y term))
	    term))))

(defgeneric cwd-term-reh (port-mapping term)
  (:method ((port-mapping  port-mapping) (term term))
    (when *simplify-cwd-term*
      (setq port-mapping (restrict-port-mapping port-mapping (graph-ports-of term))))
    (when (port-mapping-identity-p port-mapping)
      (return-from cwd-term-reh term))
    (when *simplify-cwd-term*
      (let ((mapping-pairs (mapping-pairs port-mapping)))
	(when (endp (cdr mapping-pairs))
	  (let ((pair (first mapping-pairs)))
	    (return-from cwd-term-reh (cwd-term-ren (car pair) (cdr pair) term)))))
      (when (term-constant-p term)
	(return-from cwd-term-reh (apply-port-mapping port-mapping term))))
    (build-term (reh-symbol port-mapping) (list term))))

(defun cwd-term-pn-ter (n &key (origin 0) (oriented nil))
  ;; 1--2--3--
  (loop
    with term = (cwd-term-vertex origin)
    for i from (1+ origin) to n
    do (setf term (cwd-term-add
		   (1- i) i
		   (cwd-term-oplus
		    (cwd-term-vertex i)
		    term)
		   oriented))
    finally (return term)))

(defun next-pn (term &key (oriented nil))
  (cwd-term-ren
   2 1
   (cwd-term-ren
    1 0
    (cwd-term-add
     1 2
     (cwd-term-oplus term (cwd-term-vertex 2))
     oriented))))

(defun cwd-term-p1 ()
  (cwd-term-vertex 1))

(defun apply-k-times-to-term (k next-fun start-term)
  (loop
    for term = start-term then (funcall next-fun term)
    repeat k
    finally (return term)))

(defun apply-k-next (k next-fun)
  (lambda (term)
    (apply-k-times-to-term k next-fun term)))

(defun cwd-term-pn (n &key (oriented nil))
  (assert (>= n 1))
  (apply-k-times-to-term (1- n) (lambda (term) (next-pn term :oriented oriented)) (cwd-term-p1)))

(defun cwd-term-pn-bis (n &key (origine 0) (oriented nil))
  (assert (plusp n))
  (when (< n 2) (warn "cwd-term-pn-bis of size ~A" n))
  (when (= n 1)
    (return-from cwd-term-pn-bis 
      (cwd-term-stable n)))
  (when (= n 2)
    (return-from cwd-term-pn-bis 
      (cwd-term-add 0 1 (cwd-term-stable 2 (iota 2)) oriented)))
  ;;  (assert (>= n 2))
  (let* ((milieu (1+ origine))
	 (extremite (1+ milieu))
	 (new (1+ (max origine milieu extremite)))
	 (gnew (cwd-term-vertex new))
	 (term (cwd-term-edge origine extremite :oriented oriented)))
    (loop
      repeat (- n 2)
      do (setf
	  term
	  (cwd-term-ren
	   new extremite
	   (cwd-term-ren
	    extremite milieu
	    (cwd-term-add
	     extremite new
	     (cwd-term-oplus term gnew)
	     oriented)))))
    term))

(defgeneric cwd-term-cycle (integer))

(defmethod cwd-term-cycle :before ((n integer))
  (when (< n 3)
    (warn "cycle of size ~A" n)))
					;  (assert (>= n 3)))

(defmethod cwd-term-cycle ((n integer))
  (cwd-term-ren
   1 0
   (cwd-term-ren
    2 0
    (cwd-term-unoriented-add 0 2 (cwd-term-pn-bis n)))))

(defgeneric cwd-term-circuit (integer)
  (:method ((n integer))
    (assert (>= n 2))
    (let ((path (cwd-term-pn-bis n :oriented t)))
      (if (= n 2)
	  (cwd-term-oriented-add 1 0 path)
	  (cwd-term-ren
	   1 0
	   (cwd-term-ren
	    2 0
	    (cwd-term-oriented-add 2 0 path)))))))

(defun cwd-term-kn (n)
  (assert (>= n 1))
  (let ((term (cwd-term-vertex 0))
	(g1 (cwd-term-vertex 1)))
    (loop
      repeat (1- n)
      do (setf term 
	       (cwd-term-ren
	        1 0
		(cwd-term-unoriented-add 
		 0 1
		 (cwd-term-oplus
		  term
		  g1)))))
    term))

(defun cwd-term-okn (n)
  (assert (>= n 1))
  (let ((term (cwd-term-vertex 0)))
    (loop
      repeat (1- n)
      do (setf term 
	       (cwd-term-ren
	        1 0
		(cwd-term-oriented-add 
		 1 0
		 (cwd-term-oriented-add 
		  0 1
		  (cwd-term-oplus
		   term
		   (cwd-term-vertex 1)))))))
    term))

(defun cwd-term-knm (n m)
  (assert (>= n 1))
  (assert (>= m 1))
  (cwd-term-unoriented-add
   0 1
   (cwd-term-oplus
    (cwd-term-stable n)
    (cwd-term-stable m (make-list m :initial-element 1)) )))

(defun cwd-term-kn-vbits (n &optional vbits)
  (assert (>= n 1))
  (let ((term (cwd-term-vertex 0 (pop vbits))))
    (loop
      repeat (1- n)
      do (setf
	  term 
	  (cwd-term-ren
	   1 0
	   (apply
	    #'cwd-term-unoriented-add
	    (append
	     (if (zerop (random 2)) '(0 1) '(1 0))
	     (list
	      (cwd-term-oplus
	       term
	       (cwd-term-vertex 1 (or (pop vbits) '(0 0))))))))))
    term))

(defun foo (n)
  (if (zerop n)
      0
      (1+ (foo (1- n)))))

(defun cwd-term-edge (i j &key (oriented nil))
  (unless oriented
    (assert (< i j)))
  (cwd-term-add
   i j
   (cwd-term-oplus
    (cwd-term-vertex i)
    (cwd-term-vertex j))
   oriented))

(defun cwd-term-sn (n &key (oriented nil))
  (assert (plusp n))
  (apply
   #'apply-oplus
   (loop
     with i = -1
     repeat n
     collect (cwd-term-add
	      (+ i 1) (+ i 2)
	      (cwd-term-oplus
	       (cwd-term-vertex (incf i))
	       (cwd-term-vertex (incf i)))
	      oriented))))

(defun cwd-term-zn (n &key (oriented nil))
  (assert (plusp n))
  (let ((term (cwd-term-sn n)))
    (loop
      with i = 1
      repeat (1- n)
      do (setf term
	       (cwd-term-add i (incf i 2) term oriented)))
    term))

(defun ladder (n &key (oriented nil))
  (assert (plusp n))
  (let
      ((term (cwd-term-empty-graph))
       (de (cwd-term-edge 3 4)))
    (loop
      repeat n
      do (setf
	  term
	  (cwd-term-ren
	   4 2
	   (cwd-term-ren
	    3 1
	    (cwd-term-ren 
	     2 0
	     (cwd-term-ren 
	      1 0
	      (cwd-term-add
	       2 4
	       (cwd-term-add
		1 3
		(cwd-term-oplus term de)
		oriented)
	       oriented)))))))
    term))

(defun ladder-with-one-diag (n &key (oriented nil))
  (assert (plusp n))
  (let
      ((term (cwd-term-empty-graph))
       (de (cwd-term-edge 3 4)))
    (loop
      repeat n
      do (setf
	  term
	  (cwd-term-ren
	   4 2
	   (cwd-term-ren
	    3 1
	    (cwd-term-ren 
	     2 0
	     (cwd-term-ren 
	      1 0
	      (cwd-term-add
	       2 4
	       (cwd-term-add
		1 3
		(cwd-term-oplus term de)
		oriented)
	       oriented)))))))
    term))

(defun g3 (&key (oriented t))
  (let*((t1 (cwd-term-add
	     0 2
	     (cwd-term-add
	      0 1
	      (cwd-term-multi-oplus
	       (cwd-term-vertex 0)
	       (cwd-term-vertex 1)
	       (cwd-term-vertex 2))
	      oriented)
	     oriented))
	(t2 (cwd-term-oplus 
	     (cwd-term-ren
	      2 1
	      (cwd-term-ren
	       3 0
	       (cwd-term-add
		2 3
		(cwd-term-multi-oplus t1 t1 (cwd-term-vertex 3))
		oriented)))
	     (cwd-term-vertex 2))))
    (cwd-term-add
     1 2
     (cwd-term-oplus
      (cwd-term-ren
       2 0 (cwd-term-add 1 2 t2 oriented))
      (cwd-term-vertex 2))
     oriented)))

(defun cwd-term-square-stairs (n) ;; with n+2 labels
  (assert (plusp n))
  (if (= 1 n)
      (cwd-term-vertex 1)
      (let* ((term (cwd-term-square-stairs (1- n)))
	     (label (1+ n))
	     (c (cwd-term-vertex label)))
	(setf term (cwd-term-unoriented-add
		    (1- n) n
		    (cwd-term-oplus term (cwd-term-vertex n))))
	(loop for i from (1- n) downto 2
	      do (setf
		  term
		  (cwd-term-ren
		   label i
		   (cwd-term-ren
		    i 0
		    (cwd-term-unoriented-add
		     (1- i) label
		     (cwd-term-unoriented-add
		      i label
		      (cwd-term-oplus c term)))))))
	(setf term (cwd-term-ren
		    label 1
		    (cwd-term-ren
		     1 0
		     (cwd-term-unoriented-add
		      1 label (cwd-term-oplus term c)))))
	term)))

(defun cwd-term-square-stairs-it (n) ;; with n+2 labels
  (assert (plusp n))
  (loop
    with term = (cwd-term-vertex 1)
    for k from 2 to n 
    do (loop
	 initially
	    (setq
	     term
	     (cwd-term-unoriented-add
	      (1- k) k
	      (cwd-term-oplus term (cwd-term-vertex k))))
	 for i from (1- k) downto 2
	 do (setq
	     term
	     (cwd-term-ren
	      (1+ k) i
	      (cwd-term-ren
	       i 0
	       (cwd-term-unoriented-add
		(1- i) (1+ k)
		(cwd-term-unoriented-add
		 i (1+ k)
		 (cwd-term-oplus (cwd-term-vertex (1+ k)) term))))))
	 finally (setq term
		       (cwd-term-ren
			(1+ k) 1
			(cwd-term-ren
			 1 0
			 (cwd-term-unoriented-add
			  1 (1+ k) (cwd-term-oplus term (cwd-term-vertex  (1+ k))))))))
    finally (return term)))

(defun cwd-term-stairs (n m) ;; with m+2 labels
  (assert (>= n m))
  (let ((term (cwd-term-square-stairs-it m))) ;; m+2
    (when (= n m) (return-from cwd-term-stairs term))
    (loop
      for i from (1+ m) to n
      do (loop
	   for j from m downto 1
	   do (setf term (cwd-term-ren j (1+ j) term)))
      do (loop
	   for j from 1 to (1- m)
	   do (setf term
		    (cwd-term-ren
		     (1+ j) 0
		     (cwd-term-unoriented-add
		      j (+ 2 j)
                      (cwd-term-unoriented-add
		       j (1+ j)
                       (cwd-term-oplus (cwd-term-vertex j) term))))))
      do (setf term  (cwd-term-ren
		      (1+ m) 0
		      (cwd-term-unoriented-add
		       m (1+ m)
		       (cwd-term-oplus (cwd-term-vertex m) term)))))
    term))

(defun cwd-term-square-grid (n)
  (assert (plusp n))
  (when (= n 1)
    (return-from cwd-term-square-grid (cwd-term-vertex 0)))
  (let* ((stairs (cwd-term-square-stairs-it (- n 1)))
	 (cn (cwd-term-vertex n))
	 (term (cwd-term-oplus stairs stairs)))
    (setf 
     term 
     (cwd-term-ren 
      n 0
      (cwd-term-unoriented-add 
       1 n 
       (cwd-term-oplus term cn))))
    (loop
      for i from 2 to (1- n)
      do (setf 
          term
          (cwd-term-ren n 0
			(cwd-term-unoriented-add
			 (1- i) n
			 (cwd-term-unoriented-add
			  i n
			  (cwd-term-oplus term cn))))))
    (setf 
     term 
     (cwd-term-ren 
      n 0
      (cwd-term-unoriented-add 
       (1- n) n 
       (cwd-term-oplus term cn))))
    term))

(defun shift-diagonal (term m)
  (loop for i from m downto 1
	do (setf term (cwd-term-ren i (1+ i) term)))
  term)

(defun cwd-term-forest (n)
  (let ((pn (cwd-term-pn n)))
    (cwd-term-multi-oplus-list
     (make-list n :initial-element pn))))

(defun cwd-term-grid-irene (n m)
  (assert (plusp n))
  (assert (plusp m))
  (when (> m n)
    (return-from cwd-term-grid-irene (cwd-term-grid-irene m n)))
  (when (= n m)
    (return-from cwd-term-grid-irene (cwd-term-square-grid n)))
  (let* ((stairs1 (cwd-term-stairs (1- n) m)) ;; uses m+2 labels 
	 (stairs2 (shift-diagonal
		   (cwd-term-square-stairs-it (1- m)) (1- m))) ;; uses m labels
	 (label (+ 1 m))
	 (c (cwd-term-vertex label))
	 (term (cwd-term-oplus stairs1 stairs2)))
    (loop
      for i from 1 to (1- m)
      do (setf 
          term
          (cwd-term-ren 
	   label 0
	   (cwd-term-unoriented-add
	    i label
	    (cwd-term-unoriented-add
	     (1+ i) label
	     (cwd-term-oplus term c))))))
    (setf 
     term 
     (cwd-term-unoriented-add 
      m label
      (cwd-term-oplus term c)))
    term))

(defun grid-next-row (term m &optional (variant nil))
  (loop
    with port = (1+ m)
    with term = (cwd-term-ren 1 0 (cwd-term-unoriented-add 1 port (cwd-term-oplus (cwd-term-vertex port) term)))
    for i from 2 to m
    when variant
      do (setf term (cwd-term-unoriented-add i port term))
    do (setf term
	     (cwd-term-unoriented-add
	      (1- i) port
	      (cwd-term-ren
	       i 0
	       (cwd-term-unoriented-add
		i port
		(cwd-term-oplus
		 (cwd-term-vertex port)
		 (cwd-term-ren
		  port (1- i)
		  term))))))
    finally (return (cwd-term-ren port m term))))

(defun cwd-term-rgrid (n m)
  (apply-k-times-to-term
   (1- n)
   (lambda (term)
     (grid-next-row term m))
   (cwd-term-pn-ter m :origin 1)))

(defun cwd-term-grid (n m)
  (assert (plusp n))
  (assert (plusp m))
  (when (> m n)
    (return-from cwd-term-grid (cwd-term-grid m n)))
  (when (= n m)
    (return-from cwd-term-grid (cwd-term-square-grid n)))
  (loop
    with term = (cwd-term-pn-ter m :origin 1)
    repeat (1- n)
    do (setf term (grid-next-row term m))
    finally (return term)))

(defun cwd-term-grid-variant1 (n m)
  (assert (> n 1))
  (assert (plusp m))
  (loop with term = (grid-next-row (cwd-term-pn-ter m :origin 1) m t)
	repeat (- n 2)
	do (setf term (grid-next-row term m))
	finally (return term)))

(defun cwd-term-grid-variant2 (n m)
  (assert (> n 1))
  (assert (plusp m))
  (loop with term = (cwd-term-pn-ter m :origin 1)
	repeat (- n 2)
	do (setf term (grid-next-row term m))
	finally (return (grid-next-row term m t))))


(defun wiki ()
  (cwd-term-unoriented-add
   0 1
   (cwd-term-multi-oplus
    (cwd-term-vertex 0)
    (cwd-term-vertex 1)
    (cwd-term-ren
     1 0
     (cwd-term-unoriented-add
      0 1
      (cwd-term-oplus
       (cwd-term-vertex 0)
       (cwd-term-vertex 1)))))))

(defun otournoi5 ()
  (let ((term (cwd-term-stable 5 (reverse (port-iota 5)))))
    (setf term (cwd-term-unoriented-add 1 0 term))
    (loop
      for i from 1 to 4
      do (setf term (cwd-term-unoriented-add i (mod (+ 1 i) 5) term)))
    (loop
      for i from 0 to 4
      do (setf term (cwd-term-unoriented-add i (mod (+ 2 i) 5) term)))
    term))

(defun otournoi7 ()
  (cwd-term-unoriented-add
   2 1
   (cwd-term-unoriented-add
    0 2
    (cwd-term-oplus
     (cwd-term-vertex 2)
     (cwd-term-ren
      2 1
      (cwd-term-ren
       4 0
       (cwd-term-ren
	3 0
	(cwd-term-unoriented-add
	 2 4
	 (cwd-term-unoriented-add
	  0 4
	  (cwd-term-unoriented-add
	   4 3
	   (cwd-term-unoriented-add
	    4 1
	    (cwd-term-oplus (cwd-term-vertex 4)
			    (cwd-term-ren
			     4 2
			     (otournoi5))))))))))))))

(defun grid-next-row-with-diag (term m d)
  (loop
    with port = (1+ m)
    with term = (cwd-term-ren
		 1 0
		 (cwd-term-unoriented-add
		  1 port (cwd-term-oplus (cwd-term-vertex port) term)))
    for i from 2 to m
    do (setf term (cwd-term-unoriented-add
		   i port
		   (cwd-term-oplus
		    (cwd-term-vertex port)
		    (cwd-term-ren
		     port (1- i)
		     term))))
    when (= i d) 
      do (setf term (cwd-term-unoriented-add (1- i) i term))
    do (setf term
	     (cwd-term-unoriented-add
	      (1- i) port
	      (cwd-term-ren
	       i 0 term)))
    finally (return (cwd-term-ren port m term))))

(defun cwd-term-rgrid-with-diag (n m)
  (assert (>= n 1))
  (loop with term = (cwd-term-pn-ter m :origin 1)
	for d from n downto 2
	do (setf term (grid-next-row-with-diag term m d))
	finally (return term)))

(defun cwd-term-rgrid-with-one-internal-diag (n m)
  (assert (>= n 4))
  (assert (>= m 4))
  (loop with term = (grid-next-row-with-diag 
		     (grid-next-row 
		      (cwd-term-pn-ter m :origin 1) m)
		     m
		     3)
	repeat (- n 3)
	do (setf term (grid-next-row term m))
	finally (return (remove-top-ren term))))

(defun grunbaum ()
  (let* ((t1 (cwd-term-ren
	      1 0
	      (cwd-term-unoriented-add
	       0 1
	       (cwd-term-multi-oplus
		(cwd-term-vertex 0)
		(cwd-term-vertex 1)
		(cwd-term-vertex 0)
		(cwd-term-vertex 1)))))
	 (t2 (cwd-term-unoriented-add
	      0 1
	      (cwd-term-oplus t1 (cwd-term-vertex 1)))))
    (cwd-term-unoriented-add
     0 2
     (cwd-term-oplus (cwd-term-vertex 2) t2))))


(defun grunbaum-bis ()
  (cwd-term-unoriented-add
   0 1
   (apply-oplus
    (cwd-term-vertex 1)
    (cwd-term-ren
     1 0
     (cwd-term-unoriented-add
      0 4
      (cwd-term-ren
       3 0
       (cwd-term-unoriented-add
	0 3
	(cwd-term-unoriented-add
	 1 4
	 (cwd-term-ren
	  2 1
	  (cwd-term-unoriented-add
	   0 1
	   (cwd-term-unoriented-add
	    1 2
	    (cwd-term-unoriented-add
	     2 3
	     (cwd-term-multi-oplus-list (mapcar #'cwd-term-vertex (iota 5)))))))))))))))
