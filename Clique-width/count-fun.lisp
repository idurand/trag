;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass count-fun (port-int-fun) ()
  (:documentation "port-fun yielding integers"))

(defun make-count-fun () (make-port-int-fun 'count-fun))

(defgeneric ren-count-fun (a b count-fun))
(defmethod ren-count-fun (a b (count-fun count-fun))
  (let ((new-count-fun (copy-port-fun count-fun)))
    (incf-port-value (get-port-value a count-fun) b new-count-fun)
    (reset-port-value a new-count-fun)
    new-count-fun))

(defgeneric merge-count-fun (count-fun1 count-fun2))
(defmethod merge-count-fun ((count-fun1 count-fun) (count-fun2 count-fun))
  (let ((count-fun (copy-port-fun count-fun1)))
    (maphash
     (lambda (port value)
       (incf-port-value value port count-fun))
     (hfun count-fun2))
    count-fun))
