;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defvar *reh* "T if we want to use reh-symbols")
(setq *reh* nil)

(defun toggle-reh () (setq *reh* (not *reh*)))

(defun add-couples (ports oriented)
  (let ((couples (if oriented 
		     (distinct-couples ports)
		     (distinct-pairs ports))))
    (when (some #'minusp ports)
      (setq couples (delete-if 
    		     (lambda (couple)
    		       (or (every #'minusp couple)
			   (every (lambda (x) (>= x 0)) couple)))
    		     couples)))
    couples))

(defun reh-mappings (ports)
  (mapcar
   #'make-port-mapping
   (delete-if ;; we remove mappings with one pair (-> ren-symbols)
    (lambda (pairs)
      (endp (cdr pairs)))
    (cdr (powerset (distinct-cons ports))))))

(defun reh-symbols (ports)
  (mapcar
   (lambda (port-mapping)
     (reh-symbol port-mapping))
   (reh-mappings ports)))

(defun add-symbols (ports oriented)
  (mapcar
   (lambda (pair-or-couple)
     (add-symbol (first pair-or-couple) (second pair-or-couple) :oriented oriented))
   (add-couples ports oriented)))

(defun add-symbols-orientation (ports orientation)
  (if (eq orientation :unoriented)
      (nconc (add-symbols ports nil) (add-symbols ports t))
      (add-symbols ports orientation)))

(defun ren-symbols (ports)
  (mapcar
   (lambda (couple)
     (ren-symbol (first couple) (second couple)))
   (distinct-couples ports)))

(defun vbits-constant-cwd-symbols (cwd m)
  (let ((ports (port-iota cwd)))
    (mapcar
     (lambda (l)
       (make-cwd-vbits-constant-symbol (car l) (cdr l)))
     (distribute-set
      (if (endp ports)
	  (list (string-to-port (name *empty-symbol*)))
	  ports)
      (lvbits m)))))

(defun normal-constant-cwd-symbols (cwd)
  (mapcar #'make-cwd-constant-symbol (port-iota cwd)))

(defun non-constant-cwd-symbols (cwd orientation)
  (let ((ports (port-iota cwd)))
    (nconc
     (add-symbols-orientation ports orientation)
     (ren-symbols ports)
     (list (oplus-symbol))
     (when *reh* (ren-symbols ports)))))

(defun vbits-cwd-symbols (cwd orientation m)
  (nconc
   (vbits-constant-cwd-symbols cwd m)
   (non-constant-cwd-symbols cwd orientation)))

(defun normal-cwd-symbols (cwd orientation)
  (nconc
   (normal-constant-cwd-symbols cwd)
   (non-constant-cwd-symbols cwd orientation)))

(defgeneric cwd-add-symbol-p (sym cwd orientation)
  (:method ((sym abstract-symbol) (cwd integer) orientation) nil)
  (:method((sym annotated-symbol) (cwd integer) orientation)
    (cwd-add-symbol-p (symbol-of sym) cwd orientation))
  (:method ((sym cwd-add-symbol) (cwd integer) orientation)
    (let ((oriented (oriented-p sym)))
      (and (or (eq orientation :unoriented)
	       (eq orientation oriented))
	 (let* ((symbol-ports (symbol-ports sym))
		(a (first symbol-ports))
		(b (second symbol-ports)))
	   (and
	    (or oriented (< a b))
	    (or (zerop cwd)
		(and (correct-port-p a cwd)
		     (correct-port-p b cwd)))))))))

(defun cwd-correct-ports-p (sym cwd)
  (or (zerop cwd) (every (lambda (port) (correct-port-p port cwd))
			 (symbol-ports sym))))

(defun cwd-ren-symbol-p (sym cwd)
  (and (ren-symbol-p sym) (cwd-correct-ports-p sym cwd)))

(defun cwd-reh-symbol-p (sym cwd)
  (and (reh-symbol-p sym) (cwd-correct-ports-p sym cwd)))

(defun cwd-non-constant-symbol-p (sym cwd orientation)
  (or
   (cwd-add-symbol-p sym cwd orientation)
   (cwd-ren-symbol-p sym cwd)
   (oplus-symbol-p sym)
   (cwd-reh-symbol-p sym cwd)
   (identity-symbol-p sym)))

(defun cwd-normal-symbol-p (sym cwd orientation)
  (or (cwd-constant-symbol-p sym cwd)
      (cwd-non-constant-symbol-p sym cwd orientation)))

(defun cwd-color-symbol-p (sym cwd orientation k)
  (or (cwd-color-constant-symbol-p sym cwd k)
      (cwd-non-constant-symbol-p sym cwd orientation)))

(defun cwd-vbits-symbol-p (sym cwd orientation m)
  (or (cwd-vbits-constant-symbol-p sym cwd m)
      (cwd-non-constant-symbol-p sym cwd orientation)))

(defun signature-plist (cwd orientation max-cwd m k)
  (list (cons :nb-set-variables m)
	(cons :nb-colors k)
	(cons :cwd cwd)
	(cons :oriented orientation)
	(cons :max-cwd max-cwd)))

(defun fly-cwd-signature (cwd orientation max-cwd m k)
  (assert (not (and (plusp m) (plusp k))))
  (make-fly-signature
   (cond
     ((plusp k)
      (lambda (sym) (cwd-color-symbol-p sym cwd orientation k)))
     ((plusp m)
      (lambda (sym) (cwd-vbits-symbol-p sym cwd orientation m)))
     (t
      (lambda (sym) (cwd-normal-symbol-p sym cwd orientation))))
   (signature-plist cwd orientation max-cwd m k)))
  
(defun table-normal-cwd-signature (cwd orientation max-cwd)
  (make-signature
   (normal-cwd-symbols cwd orientation)
   (signature-plist cwd orientation max-cwd 0 0)))

(defun table-vbits-cwd-signature (cwd m orientation max-cwd)
  (make-signature
   (vbits-cwd-symbols cwd orientation m)
   (signature-plist cwd orientation max-cwd m 0)))

(defgeneric max-clique-width (signature)
  (:method ((signed-object signed-object))
    (max-clique-width (signature signed-object)))
  (:method ((signature abstract-signature))
    (symbols::plist-value :cwd (plist signature) 0)))

(defgeneric compile-cwd-signature (fly-signature &optional cwd)
  (:method ((fly-signature fly-signature) &optional cwd)
    (let ((cwd (or cwd (clique-width fly-signature))))
      (assert (plusp cwd))
      (let ((m (signature-nb-set-variables fly-signature))
	    (k (signature-nb-colors fly-signature))
	    (oriented (oriented-p fly-signature))
	    (max-cwd (max-clique-width fly-signature)))
	(cond
	  ((plusp m)
	   (table-vbits-cwd-signature cwd m oriented max-cwd))
	  ((plusp k)
	   (table-color-cwd-signature cwd k oriented max-cwd))
	  (t (table-normal-cwd-signature cwd oriented max-cwd)))))))

(defun cwd-signature (cwd &key (orientation nil) (m 0) (max-cwd 0) (k 0))
  (assert (or (zerop cwd) (zerop max-cwd) (< cwd max-cwd)))
  (let ((fly-signature (fly-cwd-signature cwd orientation max-cwd m k)))
    (if (zerop cwd)
	fly-signature
	(compile-cwd-signature fly-signature cwd))))

(defun cwd-color-constant-symbols (n k)
  (mapcar
   (lambda (pair)
     (make-cwd-color-constant-symbol (car pair) (cdr pair)))
   (distribute-set (port-iota n) (color-iota k))))

(defun cwd-color-symbols (cwd k orientation)
  (nconc
   (cwd-color-constant-symbols cwd k)
   (non-constant-cwd-symbols cwd orientation)))

(defun table-color-cwd-signature (cwd k orientation max-cwd)
  (make-signature
   (cwd-color-symbols cwd k orientation)
   (list (cons :nb-colors k)
	 (cons :cwd cwd)
	 (cons :oriented orientation)
	 (cons :max-cwd max-cwd))))

(defun cwd-color-signature (cwd k &key (orientation nil) (max-cwd 0))
  (cwd-signature cwd :k k :orientation orientation :max-cwd max-cwd))

(defun cwd-vbits-signature (cwd m &key (orientation nil) (max-cwd 0))
  (cwd-signature cwd :m m :orientation orientation :max-cwd max-cwd))

(defgeneric clique-width (cwd-object)
  (:method ((cwd-object signed-object))
    (clique-width (signature cwd-object)))
  (:method ((cwd-signature fly-signature))
    (cdr (assoc :cwd (plist cwd-signature))))
  (:method ((signature signature))
    (ports-size (ports-of (signature-symbols signature)))))

(defgeneric fix-signature-cwd (fly-cwd-signature cwd)
  (:method ((s fly-signature) (cwd integer))
    (if (= (clique-width s) cwd)
	s
	(let ((m (signature-nb-set-variables s))
	      (orientation (oriented-p s))
	      (k (signature-nb-colors s)))
	  (cond
	    ((plusp k) (cwd-color-signature cwd k :orientation orientation))
	    ((plusp m) (cwd-signature cwd :m m :orientation orientation))
	    (t (cwd-signature cwd :orientation orientation)))))))
