;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defgeneric port-mapping-of (ren-or-reh-symbol)
  (:method  ((s decorated-symbol)) (port-mapping-of (symbol-of s))))

(defgeneric port-of (constant-symbol)
  (:documentation "port number associated with the constant symbol"))

(defgeneric symbol-ports (cwd-symbol)
  (:documentation "list of ports with a cwd-symbol")
  (:method ((s abstract-symbol)) '())
  (:method ((s decorated-symbol)) (symbol-ports (symbol-of s))))

(defgeneric oplus-symbol-p (abstract-symbol)
  (:documentation "is ABSTRACT-SYMBOL an oplus symbol"))

(defgeneric add-symbol-p (abstract-symbol)
  (:documentation "is ABSTRACT-SYMBOL an add symbol"))

(defgeneric ren-symbol-p (abstract-symbol)
  (:documentation "is ABSTRACT-SYMBOL a ren symbol"))

(defgeneric renaming-symbol-p (abstract-symbol)
  (:documentation "is ABSTRACT-SYMBOL a renaming symbol (ren or reh)"))

(defmethod renaming-symbol-p ((s abstract-symbol))
  (or (ren-symbol-p s) (reh-symbol-p s)))

(defvar *ac-oplus*)
(setq *ac-oplus* nil)
(defvar *ren-string* "ren")
(defvar *reh-string* "reh")
(defvar *add-string* "add")
(defvar *oplus-string* "oplus")
(defvar *identity-string* "I")
(defvar *oplus-symbol*)
(defvar *oplus-ac-symbol*)
(setq *oplus-symbol* (make-commutative-symbol *oplus-string*))
(setq *oplus-ac-symbol* (make-ac-symbol *oplus-string*))

(defvar *oplus-symbol* (make-ac-symbol *oplus-string*))
(defvar *identity-symbol* (make-parity-symbol *identity-string* 1))

(defmethod apply-port-mapping ((port-mapping port-mapping) (s (eql *oplus-symbol*)))
  s)

(defmethod apply-port-mapping ((port-mapping port-mapping) (s (eql *oplus-ac-symbol*)))
  s)

(defmethod apply-port-mapping ((port-mapping port-mapping) (s (eql *empty-symbol*)))
  s)

(defmethod apply-port-mapping ((port-mapping port-mapping) (s (eql *identity-symbol*)))
  s)

(defmethod apply-port-mapping ((port-mapping port-mapping) (s vbits-symbol))
  (make-vbits-symbol (apply-port-mapping port-mapping (symbol-of s)) (vbits s)))

(defmethod apply-port-mapping ((port-mapping port-mapping) (s color-symbol))
  (make-color-symbol (apply-port-mapping port-mapping (symbol-of s)) (color s)))

(defmethod ports-of ((s abstract-symbol))
  (make-empty-ports))

(defmethod ports-of ((s decorated-symbol))
  (ports-of (symbol-of s)))

(defmethod port-of ((s decorated-symbol))
  (port-of (symbol-of s)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; constant symbols
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass cwd-constant-symbol (abstract-constant-symbol) 
  ((port :initarg :port :reader port-of)))

(defmethod ports-of ((sym cwd-constant-symbol))
  (make-ports (list (port-of sym))))

(defmethod name ((s cwd-constant-symbol))
  (port-to-string (port-of s)))

(defmethod print-object ((s cwd-constant-symbol) stream)
  (format stream "~A" (name s)))

(defun make-cwd-constant-symbol (port)
  (make-casted-symbol 'cwd-constant-symbol :port port))

(defmethod compare-object ((s1 cwd-constant-symbol) (s2 cwd-constant-symbol))
  (= (port-of s1) (port-of s2)))

(defgeneric cwd-symbol-constant-p (abstract-symbol)
  (:documentation "is ABSTRACT-SYMBOL a constant port symbol")
  (:method ((sym abstract-symbol)) nil)
  (:method ((sym cwd-constant-symbol)) t)
  (:method ((sym annotated-symbol)) (cwd-symbol-constant-p (symbol-of sym))))

(defmethod apply-port-mapping ((port-mapping port-mapping) (s cwd-constant-symbol))
  (make-cwd-constant-symbol (apply-port-mapping port-mapping (port-of s))))

(defgeneric make-cwd-vbits-constant-symbol (port vbits)
  (:method ((port integer) (vbits list))
    (make-cwd-vbits-constant-symbol port (make-vbits vbits)))
  (:method ((port integer) (vbits vector))
    (make-vbits-symbol (make-cwd-constant-symbol port) vbits)))

(defun correct-port-p (port cwd)
  (< port cwd))

(defgeneric cwd-constant-symbol-p (sym cwd)
  (:method ((sym abstract-symbol) (cwd integer))
    (and (symbol-constant-p sym)
	 (or
	  (zerop cwd)
	  (correct-port-p (port-of sym) cwd))))
  (:method ((sym annotated-symbol) (cwd integer))
    (cwd-constant-symbol-p (symbol-of sym) cwd))
  (:method ((sym color-symbol) (cwd integer))
    (cwd-constant-symbol-p (symbol-of sym) cwd)))

(defgeneric cwd-vbits-constant-symbol-p (sym cwd m)
  (:method ((sym abstract-symbol) (cwd integer) (m integer))
    (declare (ignore sym))
    (declare (ignore m))
    (declare (ignore cwd))
    nil)
  (:method ((sym vbits-constant-symbol) (cwd integer) (m integer))
    (and
     (= m (length (vbits sym)))
     (cwd-symbol-constant-p (symbol-of sym)))))

(defun make-cwd-correct-constant-symbol (port &optional (vbits nil))
  (if vbits
      (make-cwd-vbits-constant-symbol port vbits)
      (make-cwd-constant-symbol port)))

(defgeneric make-cwd-color-constant-symbol (port color)
  (:method ((port integer) (color integer))
    (make-color-symbol (make-cwd-constant-symbol port) color)))

(defgeneric cwd-color-constant-symbol-p (symbol cwd k)
  (:method ((s abstract-symbol) (cwd integer) (k integer)) nil)
  (:method ((s annotated-symbol) (cwd integer) (k integer))
    (cwd-color-constant-symbol-p (symbol-of s) cwd k))
  (:method ((s abstract-symbol) (cwd integer) (k integer)) nil)
  (:method ((s color-constant-symbol) (cwd integer) (k integer))
    (and
     (or (zerop k) (<= (color s) k))
     (cwd-symbol-constant-p (symbol-of s)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; oplus symbol
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun oplus-symbol () (if *ac-oplus* *oplus-ac-symbol* *oplus-symbol*))
(defmethod oplus-symbol-p ((s annotated-symbol))
  (oplus-symbol-p (symbol-of s)))
(defmethod oplus-symbol-p ((s abstract-symbol)) nil) 
(defmethod oplus-symbol-p ((s (eql *oplus-symbol*))) t)
(defmethod oplus-symbol-p ((s (eql *oplus-ac-symbol*))) t)

(defmethod print-object ((s (eql *oplus-symbol*)) stream)
  (format stream "~A" (name s)))

(defmethod print-object ((s (eql *oplus-ac-symbol*)) stream)
  (format stream "~A" (name s)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; unary symbols (add, ren, reh)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dont inherit from named-symbol because we
;; dont want to compare their names
;; the names are just for printing
(defgeneric cwd-symbol-name (cwd-symbol)
  (:documentation "the name (string) of the cwd-symbol"))

(defclass cwd-unary-symbol (abstract-non-constant-symbol) ())
(defmethod arity ((s cwd-unary-symbol)) 1)
(defmethod print-object ((s cwd-unary-symbol) stream)
  (format stream "~A" (name s)))

(defmethod ports-of ((s cwd-unary-symbol))
  (make-ports (symbol-ports s)))

(defclass cwd-2ports-symbol (cwd-unary-symbol)
  ((port1 :initarg :port1 :reader port1 :type integer)
   (port2 :initarg :port2 :reader port2 :type integer)))

(defmethod symbol-ports ((s cwd-2ports-symbol))
  (list (port1 s) (port2 s)))

(defmethod compare-object ((s1 cwd-2ports-symbol) (s2 cwd-2ports-symbol))
  (and
   (eq (cwd-symbol-name s1) (cwd-symbol-name s2))
   (= (port1 s1) (port1 s2))
   (= (port2 s1) (port2 s2))))

(defmethod name ((s cwd-2ports-symbol))
  (format nil "~A_~A_~A" (cwd-symbol-name s)
	  (port-to-string (port1 s))
	  (port-to-string (port2 s))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; add symbol
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass cwd-add-symbol (oriented-mixin cwd-2ports-symbol) ())

(defmethod name ((s cwd-add-symbol))
  (format nil "~A_~A~:[_~;->~]~A" (cwd-symbol-name s)
	  (port-to-string (port1 s))
	  (oriented-p s)
	  (port-to-string (port2 s))))

(defmethod cwd-symbol-name ((s cwd-add-symbol)) *add-string*)

(defmethod compare-object ((s1 cwd-add-symbol) (s2 cwd-add-symbol))
  (and
   (call-next-method)
   (eq (oriented-p s1) (oriented-p s2))))

(defgeneric add-symbol (port1 port2 &key oriented)
  (:method ((port1 integer) (port2 integer) &key (oriented *oriented*))
    (unless oriented
      (when (> port1 port2)
	(psetf port1 (min port1 port2)
	       port2 (max port1 port2))))
    (make-casted-symbol 'cwd-add-symbol :port1 port1 :port2 port2 :oriented oriented)))

(defmethod apply-port-mapping ((port-mapping port-mapping) (s cwd-add-symbol))
  (let ((ports (apply-port-mapping port-mapping (symbol-ports s))))
    (apply #'add-symbol (opair-from-pair ports *oriented*))))

(defmethod add-symbol-p ((sym abstract-symbol)) nil)
(defmethod add-symbol-p ((sym decorated-symbol))
  (add-symbol-p (symbol-of sym)))

(defmethod add-symbol-p ((s cwd-add-symbol)) t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ren symbol
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass cwd-ren-symbol (cwd-2ports-symbol) ())

(defmethod cwd-symbol-name ((s cwd-ren-symbol)) *ren-string*)

(defmethod ren-symbol-p ((sym abstract-symbol)) nil)
(defmethod ren-symbol-p ((s cwd-ren-symbol)) t)

(defmethod ren-symbol-p ((s decorated-symbol))
  (ren-symbol-p (symbol-of s)))

(defgeneric ren-symbol (port1 port2))
(defmethod ren-symbol ((port1 integer) (port2 integer)) 
  (make-casted-symbol 'cwd-ren-symbol :port1 port1 :port2 port2))

(defmethod apply-port-mapping ((port-mapping port-mapping) (s cwd-ren-symbol))
  (apply #'ren-symbol (apply-port-mapping port-mapping (symbol-ports s))))

(defmethod port-mapping-of ((s cwd-ren-symbol))
  (make-port-mapping (list (apply #'cons (symbol-ports s)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; identity symbol
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric identity-symbol-p (abstract-symbol)
  (:documentation "is ABSTRACT-SYMBOL the identity symbol")
  (:method ((s abstract-symbol)) nil)
  (:method ((s (eql *identity-symbol*))) t))

(defun identity-symbol () *identity-symbol*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; reh symbol
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass cwd-reh-symbol (cwd-unary-symbol)
  ((port-mapping :initarg :port-mapping :reader port-mapping-of :type port-mapping)))

(defmethod cwd-symbol-name ((s cwd-reh-symbol)) *reh-string*)

(defmethod compare-object ((s1 cwd-reh-symbol) (s2 cwd-reh-symbol))
  (compare-object (port-mapping-of s1) (port-mapping-of s2)))

(defmethod name ((s cwd-reh-symbol))
  (format nil "~A_~A" *reh-string* (port-mapping-of s)))

(defgeneric reh-symbol-p (port-mapping)
  (:method ((sym abstract-symbol)) nil)
  (:method ((s cwd-reh-symbol)) t)
  (:method ((s decorated-symbol)) (reh-symbol-p (symbol-of s))))

(defmethod symbol-ports ((s cwd-reh-symbol))
  (loop for pair in (mapping-pairs (port-mapping-of s))
	nconc (list (car pair) (cdr pair))))

(defgeneric reh-symbol (port-mapping)
  (:method ((port-mapping port-mapping))
    (make-casted-symbol 'cwd-reh-symbol :port-mapping port-mapping)))

(defmethod apply-port-mapping ((port-mapping port-mapping) (s cwd-reh-symbol))
  (let ((map (compose-port-mapping port-mapping (port-mapping-of s))))
    (if (port-mapping-identity-p map)
	*identity-symbol*
	(reh-symbol map))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
