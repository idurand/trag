;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defgeneric cwd-term-to-incidence-rec (cwd-term)
  (:documentation
   "works for terms which add on edge for each add_a_b operation \
returns a term corresponding to the incidence graph of the graph corresponding to cwd-term"))

(defgeneric cwd-term-to-incidence (cwd-term)
  (:documentation
   "works for terms which add on edge for each add_a_b operation \
returns a term corresponding to the incidence graph of the graph corresponding to cwd-term"))

(defmethod cwd-term-to-incidence ((cwd-term term))
  (let ((eti-max (reduce #'max (decomp::eti-list cwd-term))))
    (labels
	((aux (term)
	   (let ((root (root term)))
	     (if (symbol-constant-p root)
		 term
		 (let ((new-arg (mapcar #'aux (arg term))))
		   (if (add-symbol-p root)
		       (let* ((ports (symbol-ports root))
			      (a (first ports))
			      (b (second ports)))
			 (cwd-term-ren
			  -2 -1
			  (cwd-term-add
			   -2 b
			    (cwd-term-add
			     a -2
			     (cwd-term-oplus (car new-arg) (cwd-term-num-vertex -2 (incf eti-max)))))))
		       (build-term root new-arg)))))))
	   (aux cwd-term))))
      
    
