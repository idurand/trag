;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defvar *simplify-cwd-term*)
;;(setq *simplify-cwd-term* t)
(setq *simplify-cwd-term* nil)

(defgeneric port-mapping-function (port-mapping)
  (:method ((port-mapping port-mapping))
    (lambda (s) (apply-port-mapping port-mapping s))))

(defmethod apply-port-mapping ((port-mapping port-mapping) (term term))
  (if (port-mapping-identity-p port-mapping)
      term
      (term-subst-symbols term (port-mapping-function port-mapping))))

(defun ports-of-terms (terms)
  (loop
    with ports = (make-empty-ports)
    for candidates = terms then (loop for term in candidates
				      do (ports-nunion ports (ports-of (root term)))
				      append (arg term))
    while candidates
    finally (return ports)))

(defmethod ports-of ((term term))
  (ports-of-terms (list term)))

(defgeneric graph-ports-of (cwd-term)
  (:method ((cwd-term term))
    ;;  (trivial-backtrace:print-backtrace-to-stream *error-output*)
    (let ((root (root cwd-term)))
      (if (symbol-constant-p root)
	  (ports-of root)
	  (let ((ports (graph-ports-of (arg cwd-term))))
	    (cond
	      ((or (oplus-symbol-p root) (add-symbol-p root))
	       ports)
	      ((renaming-symbol-p root)
	       (apply-port-mapping (port-mapping-of root) ports))
	      (t (warn "unknowm cwd symbol")))))))
  (:method ((cwd-terms list))
    (ports-union-gen
     (mapcar #'graph-ports-of cwd-terms))))

(defgeneric remove-identity-symbols (term)
  (:documentation "new term wihtout identity nodes")
  (:method ((term term))
    (if (term-zeroary-p term)
	term
	(let ((root (root term)))
	  (if (identity-symbol-p root)
	      (remove-identity-symbols (car (arg term)))
	      (build-term root (mapcar #'remove-identity-symbols (arg term))))))))

(defgeneric remove-top-ren (term)
  (:method ((term abstract-term))
    (if (or (endp (arg term)) (not (ren-symbol-p (root term))))
	term
	(remove-top-ren (car (arg term))))))

(defun skeleton (cwd-term depth)
  (loop
    with term = cwd-term
    until (= (term-depth term) depth)
    while term
    do (setq term
	     (let ((arg (arg term)))
	       (when arg
		 (let ((arg1 (pop arg)))
		   (if (endp arg)
		       arg1
		       (let ((arg2 (pop arg)))
			 (if (> (term-depth arg1) (term-depth arg2))
			     arg1
			     arg2)))))))
    finally (return term)))

(defun cwd-term-empty-graph (&optional (m 0))
  (if (zerop m)
      (build-term *empty-symbol*)
      (build-term
       (make-vbits-symbol *empty-symbol* '(0)))))

(defgeneric cwd-term-empty-p (term)
  (:method ((term term))
    (symbol-equal (root term) *empty-symbol*)))

(defgeneric add-edge-vbits (term i j)
  (:method ((term term) i j)
    (let ((root (root term)))
      (if (term-constant-p term)
	  (let ((eti (term-eti term)))
	    (set-term-eti
	     (build-term
	      (make-vbits-symbol
	       (symbol-of root)
	       (make-vbits 
		(cond
		  ((= eti i) (make-vbits '(1 0)))
		  ((= eti j) (make-vbits '(0 1)))
		  (t (make-vbits '(0 0)))))))
	     eti))
	  (build-term root 
		      (mapcar
		       (lambda (st) 
			 (add-edge-vbits st i j))
		       (arg term)))))))

(defgeneric color-term-with-vertex-colors (term vertex-colors)
  (:method ((term abstract-term) (vertex-colors list))
    (let ((arg (arg term))
	  (root (root term)))
      (if (endp arg)
	  (let ((eti (term-eti term)))
	    (set-term-eti
	     (build-term
	      (make-color-symbol root (cdr (assoc eti vertex-colors))))
	     eti))
	  (build-term root (mapcar (lambda (st) (color-term-with-vertex-colors st vertex-colors))
				   arg))))))

(defgeneric cwd-term-reh-to-ren (term)
  (:method ((cwd-term term))
    (let ((root (root cwd-term)))
      (if (symbol-constant-p root)
	  cwd-term
	  (let ((arg (mapcar #'cwd-term-reh-to-ren (arg cwd-term))))
	    (if (reh-symbol-p root)
		(let ((st (car arg))
		      (pairs (sequentialize-mapping (mapping-pairs (port-mapping-of root)) (graph-ports-of cwd-term))))
		  (loop
		    for pair in pairs
		    do (setq st (cwd-term-ren (car pair) (cdr pair) st))
		    finally (return st)))
		(build-term root arg)))))))

(defmethod oriented-p ((cwd-term term))
  (let* ((add-symbols (remove-if-not #'add-symbol-p (symbols-from cwd-term)))
	 (oadd (find-if #'oriented-p add-symbols))
	 (uadd (find-if-not #'oriented-p add-symbols)))
    (cond
      ((or (endp add-symbols) (and oadd uadd)) :unoriented)
      (oadd t)
      (uadd nil))))

(defgeneric cwd-term-unorient-oriented (cwd-term)
  (:method ((cwd-term term))
    (let ((root (root cwd-term)))
      (if (symbol-constant-p root)
	  cwd-term
	  (let ((arg (mapcar #'cwd-term-unorient-oriented (arg cwd-term))))
	    (if (add-symbol-p root)
		(let ((ports (container-contents (ports-of root))))
		  (let ((*simplify-cwd-term* t))
		    (cwd-term-add (first ports) (second ports) (car arg) nil)))
		(build-term root arg)))))))

(defgeneric cwd-term-unorient (cwd-term)
  (:method ((cwd-term term))
    (if (oriented-p cwd-term)
	(cwd-term-unorient-oriented cwd-term)
	cwd-term)))
