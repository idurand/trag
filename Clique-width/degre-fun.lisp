;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass degre-fun (port-int-fun) ())

;; indicated the degre of a port
;; -1 if the port does not exist

(defmethod default-value ((degre-fun degre-fun))
  -1)

(defun make-degre-fun () (make-port-fun 'degre-fun))

(defgeneric ren-degre-fun (a b degre-fun))
(defmethod ren-degre-fun (a b (degre-fun degre-fun))
  (let ((da (get-port-value a degre-fun))
	(db (get-port-value b degre-fun)))
    (unless (and (/= da db)
		 (not (or (= -1 da) (= -1 db))))
      (let ((new-degre-fun (copy-port-fun degre-fun)))
	(set-port-value da b new-degre-fun)
	(reset-port-value a new-degre-fun)
	new-degre-fun))))

(defgeneric regular-degre-fun-p (degre-fun))
(defmethod regular-degre-fun-p ((degre-fun degre-fun))
  (let ((degre nil))
    (maphash
     (lambda (port d)
       (declare (ignore port))
       (unless degre
	 (setq degre d))
       (when (/= degre d)
	 (return-from regular-degre-fun-p)))
     (hfun degre-fun))
    t))

(defgeneric merge-degre-fun (degre-fun1 degre-fun2))
(defmethod merge-degre-fun ((degre-fun1 degre-fun) (degre-fun2 degre-fun))
  (let ((degre-fun (copy-port-fun degre-fun1)))
    (maphash
     (lambda (port d2)
       (let ((d1 (get-port-value port degre-fun)))
	 (if (= -1 d1)
	     (set-port-value d2 port degre-fun)
	     (unless (= d1 d2)
	       (return-from merge-degre-fun)))))
     (hfun degre-fun2))
    degre-fun))
