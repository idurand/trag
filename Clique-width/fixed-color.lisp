;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass vertices (ordered-container) ())

(defun make-vertices (vertices)
  (make-container-generic vertices 'vertices #'eql))

(defclass fixed-color (attributed-object) ()
  (:documentation "a color object with an attribute which if the set (ordered-container) of vertices having that color"))

(defgeneric color-of (fixed-color)
  (:documentation "the color of a fixed-color"))

(defmethod color-of ((fixed-color fixed-color))
  (object-of fixed-color))

(defgeneric vertices-of (fixed-color))
(defmethod vertices-of ((fixed-color fixed-color))
  (attribute-of fixed-color))

(defun make-fixed-color (color vertices)
  (make-instance 'fixed-color :object color :attribute (make-vertices vertices)))

(defmethod print-object ((fixed-color fixed-color) stream)
  (format stream "~A" (color-of fixed-color)))
