;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defgeneric ports-translation (term k))
(defmethod ports-translation ((term term) (k integer))
  (let ((cwd (clique-width term)))
    (loop
      for i from (1- cwd) downto 0
      do (setf term (cwd-term-ren i (+ i k) term))
      finally (return term))))

(defun graph-scale (n)
  (loop
    with ti = (cwd-term-unoriented-add 0 1 (cwd-term-oplus (cwd-term-vertex 0) (cwd-term-vertex 1)))
    for i from 1 below n
    do (setf ti
	     (cwd-term-ren
	      (+ 2 i) i
	     (cwd-term-ren
	      i (1- i)
	      (cwd-term-unoriented-add
	       (1- i) (1+ i)
	       (cwd-term-unoriented-add
		i (+ 2 i)
		 (cwd-term-oplus
		  ti
		  (cwd-term-unoriented-add (+ 2 i) (1+ i) (cwd-term-oplus (cwd-term-vertex (+ 2 i)) (cwd-term-vertex (1+ i))))))))))
       finally (return (cwd-term-ren i (1- i) ti))))
	
(defun graph-grammar (n)
  (loop
    with g = (cwd-term-ren 0 2 (graph-scale 2))
    with h = (ports-translation (graph-scale 3) 4)
    repeat n
    do (setf g
	     (cwd-term-ren
	      6 2
	      (cwd-term-ren
	       4 1
	       (cwd-term-ren
		5 0
		(cwd-term-ren
		 3 0
		 (cwd-term-ren
		  2 0
		  (cwd-term-ren
		   1 0
		   (cwd-term-unoriented-add
		    4 1
		    (cwd-term-unoriented-add
		     5 2
		     (cwd-term-unoriented-add
		      6 3
		      (cwd-term-multi-oplus h g (cwd-term-ren 1 3 g))))))))))))
    finally (return g)))

;; to use dag optimisation you must load Termauto/dag.lisp

;; (defparameter *t* (clique-width::graph-grammar 2))
;; (defparameter *f* (kcolorability-automaton 4))
;; (with-time (recognized-p *t* *f* :save-run t))

;; 2013-05-24
;; G_i 4-colorable
;;-------------------
;; i: pas dag / dag
;;-------------------
;; 0:  0.02s / 0.02s
;; 1:   9.8s / 10s
;; 2:  31.4s / 20s
;; 3: 1mn 13s/ 32s
;; 4:  2mn   / 42s
;; 5:  5mn   / 54s
;; 6: 11mn   / 1mn 6s
;; 7: 22mn   / 1mn 16s
;; 8: 44mn   / 1mn 27s
;; 9: 88mn   / 1mn 32s
;;10:        / 1mn 49s
;;11:        / 2mn 1s
;;12:        / 2mn 14s
;;13:        / 2mn 22s
;;14:        / 2mn 34s
;;15:        / 2mn 50s
;;16:        / 2mn 59s
;;17:        / 3mn 9s
;;18:        / 3mn 21s
;;19:        / 3mn 35s
;;20:        / 3mn 54s
;;21:        / 4mn 12s
;;22:        / 4mn 41s
;;23:        / 5mn 20s
;;24:        / 6mn 39s
;;25:        / 8mn53s
;;26:        /13mn 31s
;;27:        /22mn 35s
;;28         /40mn 15s
;;29         /76mn 1s
;;30         /146mn 11s
;;-------------------

;; s_1 = 14
;; a_1 = 31
;; s_{n+1} = 6 + 2 s_n
;; a_{n+1} = 23 + 2 a_n
;; s_n = 10 2^n - 6
;; a_n = 27 2^n - 23
