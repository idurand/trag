;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defgeneric arc-connecting-ports-p (a b ports1 ports2))

(defmethod arc-connecting-ports-p (a b (s1 ports) (s2 ports))
  (and (ports-member a s1) (ports-member b s2)))

(defgeneric edge-connecting-ports-p (a b ports1 ports2))

(defmethod edge-connecting-ports-p (a b (s1 ports) (s2 ports))
  (or
   (arc-connecting-ports-p a b s1 s2)
   (arc-connecting-ports-p b a s1 s2)))
