;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(cwd-signature 26)
(cwd-vbits-signature 26 1)
(cwd-vbits-signature 26 2)
(cwd-vbits-signature 26 3)
(cwd-vbits-signature 26 4)

;; (defparameter *path*
;;   (input-cwd-term 
;; "
;; add_c_d(add_b_d(oplus(d[v1],ren_d_b(add_a_d(oplus(d[v2],add_c_e(oplus(add_a_b(add_b_c(oplus(a[v3],oplus(b[v4],c[v5])))),add_a_b(add_b_e(oplus(a[v6],oplus(b[v7],e[v8]))))))))))))"))

(defparameter *path1*
  ;; path property yes
  (input-cwd-term 
   "add_c_d(
     add_b_d(
      oplus(
       d^01,
       ren_d_b(
        add_a_d(
         oplus(
          d^00,
          add_c_e(
           oplus(
            add_a_b(
             add_b_c(
              oplus(
               a^11,
               oplus(b^01,c^00)))),
            add_a_b(
             add_b_e(
              oplus(
               a^00,
               oplus(b^01,e^11))))))))))))"))

(defparameter *ppath1* (vbits-projection *path1*))

(defparameter *path2* ;; no
  (input-cwd-term "
add_c_d(add_b_d(oplus(d^01,ren_d_b(add_a_d(oplus(d^00,add_c_e(oplus(add_a_b(add_b_c(oplus(a^11,oplus(b^01,c^00)))),add_a_b(add_b_e(oplus(a^00,oplus(b^00,e^11))))))))))))"))

(defparameter
  *term*
  (input-cwd-term
"add_a_b(
 oplus(
  oplus(
   ren_b_a(add_a_b(oplus(a^00,b^00))),
   ren_b_a(add_a_b(oplus(a^00,b^00)))),
  oplus(
   ren_b_a(add_a_b(oplus(a^00,b^00))),
   ren_a_b(oplus(a^00,oplus(a^00,add_a_b(oplus(a^00,b^00))))))))"))

(defparameter
    *term1* ;; non car card(X1) =\= 2
  (input-cwd-term
   "add_a_b(oplus(oplus(oplus(ren_b_a(add_a_b(oplus(a^11,b^11))),ren_b_a(add_a_b(oplus(a^11,b^00)))),ren_b_a(add_a_b(oplus(a^00,b^00)))),ren_a_b(oplus(a^01,oplus(a^00,add_a_b(oplus(a^00,b^00)))))))"))

(defparameter
    *term2*
  (input-cwd-term
"add_a_b(
  oplus(
   oplus(
    oplus(
     ren_b_a(
      add_a_b(
       oplus(a^11,b^11))),
     ren_b_a(add_a_b(oplus(a^01,b^00)))),
    ren_b_a(add_a_b(oplus(a^00,b^00)))),
   ren_a_b(
    oplus(
     a^01,
     oplus(a^00,add_a_b(oplus(a^00,b^00)))))))"))

(defparameter
    *term3*
  (input-cwd-term
   "add_a_b(oplus(oplus(oplus(ren_b_a(add_a_b(oplus(a^11,b^00))),ren_b_a(add_a_b(oplus(a^11,b^00)))),ren_b_a(add_a_b(oplus(a^00,b^00)))),ren_a_b(oplus(a^01,oplus(a^00,add_a_b(oplus(a^00,b^00)))))))"))

(defparameter
    *term4*
  (input-cwd-term
   "add_a_b(oplus(oplus(oplus(ren_b_a(add_a_b(oplus(a^11,b^11))),ren_b_a(add_a_b(oplus(a^00,b^00)))),ren_b_a(add_a_b(oplus(a^00,b^00)))),ren_a_b(oplus(a^00,oplus(a^00,add_a_b(oplus(a^00,b^00)))))))"))

(defparameter
    *term5*
  (input-cwd-term
   "add_a_b(oplus(oplus(oplus(ren_b_a(add_a_b(oplus(a^11,b^11))),ren_b_a(add_a_b(oplus(a^00,b^00)))),ren_b_a(add_a_b(oplus(a^01,b^01)))),ren_a_b(oplus(a^00,oplus(a^00,add_a_b(oplus(a^00,b^00)))))))"))

(defparameter
    *term6*
  (input-cwd-term
   "add_a_b(oplus(oplus(oplus(ren_b_a(add_a_b(oplus(a^11,b^00))),ren_b_a(add_a_b(oplus(a^11,b^00)))),ren_b_a(add_a_b(oplus(a^01,b^01)))),ren_a_b(oplus(a^00,oplus(a^00,add_a_b(oplus(a^00,b^00)))))))"))

(defparameter
    *term7*
  (input-cwd-term
   "add_a_b(oplus(oplus(oplus(ren_b_a(add_a_b(oplus(a^00,b^00))),ren_b_a(add_a_b(oplus(a^00,b^00)))),ren_b_a(add_a_b(oplus(a^00,b^01)))),ren_a_b(oplus(a^11,oplus(a^11,add_a_b(oplus(a^00,b^00)))))))"))


(defparameter
    *term8*
  (input-cwd-term
   "add_a_b(oplus(oplus(oplus(ren_b_a(add_a_b(oplus(a^00,b^00))),ren_b_a(add_a_b(oplus(a^00,b^01)))),ren_b_a(add_a_b(oplus(a^00,b^00)))),ren_a_b(oplus(a^11,oplus(a^11,add_a_b(oplus(a^00,b^00)))))))"))

(defparameter
    *exp1*
  (input-cwd-term
  "oplus(ren_b_a(add_a_b(oplus(a,b))),b)"))

(defparameter *i1*
  (input-cwd-term
   "add_a_b(oplus(ren_b_a(add_a_b(oplus(a^11,oplus(b^01,b^01)))),b^11))"))
  
(defparameter *i2*
  (input-cwd-term
   "add_a_b(oplus(ren_b_a(add_a_b(oplus(a^11,oplus(b^00,b^00)))),b^11))"))

(defparameter *i3*
  (input-cwd-term
   "add_a_b(oplus(add_a_b(oplus(a^11,b^01)),add_a_b(oplus(a^11,b^01))))"))

(defparameter *i4*
  (input-cwd-term
   "add_a_b(oplus(add_a_b(oplus(a^11,b^00)),add_a_b(oplus(a^11,b^01))))"))

(defparameter *i5*
  (input-cwd-term
   "add_a_b(oplus(add_a_b(oplus(a^11,b^00)),add_a_b(oplus(a^11,b^00))))"))

 (defparameter *i6*
  (input-cwd-term
   "add_a_b(oplus(add_a_b(oplus(a^11,b^01)),add_a_b(oplus(a^11,b^00))))"))

 (defparameter *i7*
  (input-cwd-term
   "add_a_b(oplus(b^11,oplus(b^11,ren_b_a(add_a_b(oplus(a^01,b^01))))))"))

(defparameter *i8* ;; reconnu par *p* mais pas par celui de Bruno!
  (input-cwd-term 
"add_a_b(oplus(ren_b_a(oplus(ren_a_b(oplus(add_a_b(oplus(a^01,b^01)),oplus(oplus(a^01,b^01),ren_a_b(oplus(oplus(a^01,b^01),ren_b_a(oplus(oplus(a^01,b^01),oplus(b^11,b^01)))))))),ren_a_b(oplus(add_a_b(oplus(a^01,b^01)),oplus(oplus(a^01,b^01),ren_a_b(oplus(oplus(a^01,b^01),ren_b_a(oplus(oplus(a^01,b^01),oplus(b^11,b^01)))))))))),add_a_b(oplus(a^01,b^01))))
"))

(defparameter *c*
  (input-cwd-term
   "oplus(c^01,add_b_c(oplus(c^11,ren_c_a(add_a_c(oplus(c^11,oplus(add_a_b(oplus(a^01,b^01)),add_a_b(oplus(a^01,b^01)))))))))"))

;;; graph pour colorablility
(defparameter *term*
  (input-cwd-term
   "oplus(add_a_b(oplus(a,a)),add_a_b(oplus(a,a)))"))


(defparameter *pas-3-3-coloriable*
  (input-cwd-term
   "add_a_b(
      oplus(
        b,
        ren_b_c(
          add_b_c(
            oplus(
              add_a_b(oplus(b,ren_b_a(add_a_b(oplus(a,b))))),
              ren_b_c(add_a_b(oplus(b,ren_b_a(add_a_b(oplus(a,b)))))))))))"))

;; t = add_a_b(oplus*(b,w))
;; w = ren_b_c(add_b_c(oplus*(u,v)))
;; v = ren_b_c(u)
;; u = add_a_b(oplus*(b,ren_b_a(add_b_a(oplus*(a,b)))))

;; v = ren_b_c(add_a_b(oplus*(b,ren_b_a(add_b_a(oplus*(a,b))))))

(defparameter
    *3-3-coloriable*
  (input-cwd-term
   "add_a_c(
      oplus(
        ren_c_b(
          add_b_c(
            oplus(
              ren_b_c(add_b_c(oplus(b,add_a_c(oplus(a,c))))),
              add_a_b(oplus(a,b))))) ,
        ren_a_c(add_a_c(oplus(a,c)))))"))

;; t = add_a_c(oplus(v,ren_a_c(add_a_c(oplus(a,c)))))
;; v = ren_c_b(add_b_c(oplus(u,add_a_b(oplus(a,b)))))
;; u = ren_b_c(add_b_c(oplus(b,add_a_c(oplus(a,c)))))

(defparameter *c4*
  (input-cwd-term 
   "add_a_b(
     oplus(
      add_a_b(
       oplus(a,b)),
      add_a_b(
       oplus(a,b)))))))))"))

(defparameter *c5*
  (input-cwd-term
   "add_b_c(
     oplus(
      c,
      ren_c_a(
       add_a_c(
        oplus(
         c,
         oplus(
          add_a_b(
           oplus(a,b)),
          add_a_b(
           oplus(a,b))))))))"))

(defparameter
  *ncn*
  (input-cwd-term
   "add_a_c(
      oplus(
       oplus(
        oplus(
         ren_b_c(
          ren_a_d(
           add_b_c(
            add_a_d(
             oplus(
              c,
              oplus(
               d,
               oplus(add_a_b(oplus(a,b)),add_a_b(oplus(a,b))))))))),
         add_a_d(oplus(a,d))),
        add_b_d(oplus(b,d))),
       add_b_c(oplus(c,add_a_b(oplus(a,b))))))"))

(defparameter
  *cn*
  (cwd-term-unoriented-add 0 1 *ncn*))

;; a b c d e f g h i j k  l  m  n  o  p  q  r
;; 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
(defparameter *add_a_b* (add-symbol 0 1))
(defparameter *add_a_c* (add-symbol 0 2))
(defparameter *add_a_d* (add-symbol 0 3))
(defparameter *add_a_j* (add-symbol 0 9))
(defparameter *add_d_f* (add-symbol 3 5))
(defparameter *add_g_i* (add-symbol 6 8))
(defparameter *add_j_l* (add-symbol 9 11))
(defparameter *add_m_o* (add-symbol 12 14))
(defparameter *add_p_r* (add-symbol 15 17))
(defparameter *ren_d_a* (ren-symbol 3 0))
(defparameter *ren_d_e* (ren-symbol 3 4))
(defparameter *ren_e_d* (ren-symbol 4 3))
(defparameter *ren_f_a* (ren-symbol 5 0))


(defparameter *A-B* (input-cwd-term "add_a_b(oplus(a,b))"))
(defparameter *B-C* (input-cwd-term "add_b_c(oplus(b,c))"))
(defparameter *C-D* (input-cwd-term "add_c_d(oplus(c,d))"))
(defparameter *D-E* (input-cwd-term "add_d_e(oplus(d,e))"))
(defparameter *E-F* (input-cwd-term "add_e_f(oplus(e,f))"))
(defparameter *F-G* (input-cwd-term "add_f_g(oplus(f,g))"))
(defparameter *G-H* (input-cwd-term "add_g_h(oplus(g,h))"))
(defparameter *H-I* (input-cwd-term "add_h_i(oplus(h,i))"))
(defparameter *I-J* (input-cwd-term "add_i_j(oplus(i,j))"))
(defparameter *J-K* (input-cwd-term "add_j_k(oplus(j,k))"))
(defparameter *K-L* (input-cwd-term "add_k_l(oplus(k,l))"))
(defparameter *L-M* (input-cwd-term "add_l_m(oplus(l,m))"))
(defparameter *M-N* (input-cwd-term "add_m_n(oplus(m,n))"))
(defparameter *N-O* (input-cwd-term "add_n_o(oplus(n,o))"))
(defparameter *O-P* (input-cwd-term "add_o_p(oplus(o,p))"))
(defparameter *P-Q* (input-cwd-term "add_p_q(oplus(p,q))"))
(defparameter *Q-R* (input-cwd-term "add_q_r(oplus(q,r))"))


(defparameter *A-C* (input-cwd-term "add_a_c(oplus(a,c))"))
(defparameter *A-D* (input-cwd-term "add_a_d(oplus(a,d))"))
(defparameter *B-D* (input-cwd-term "add_b_d(oplus(b,d))"))

;;; cwd = 4
(defparameter *S4* (cwd-term-oplus
		    (input-cwd-term "oplus(b,d)")
		    (cwd-term-oplus *B-D* *C-D*)))


(defparameter *T4* (cwd-term-multi-oplus *A-B* *A-C* *A-D* *B-C*))

(defparameter *w* (cwd-term-oplus *S4* *T4*))
(defparameter *u* (cwd-term-oplus (input-cwd-term "c") *W* ))
(defparameter *v4* (build-term *add_a_b*
			      (list
			       (build-term
				*ren_d_a*
				(list *u*))))) ;; (non connexe)

(defparameter *Z4*
  (build-term
   *add_a_b*
   (list
    (build-term *ren_d_a* (list *W*)))))    ;; (connexe)

;;;;;;;;;;;;;;;;;;;
;; Cwd = 5
;; On ajoute e
(defparameter *C-E* (input-cwd-term "add_c_e(oplus(c,e))"))
(defparameter *t5* *t4*)
(defparameter *S5*
  (cwd-term-oplus
    (input-cwd-term "oplus(b,e)")
    (cwd-term-oplus *B-D* *C-E*)))

(defparameter *W5* (cwd-term-oplus *S5* *T5*))
(defparameter *U5*
  (build-term
   *ren_e_d*
   (list
    (cwd-term-oplus (input-cwd-term "c") *W5*))))
(defparameter
    *V5*
  (build-term *add_a_b* (list (build-term *ren_d_a* (list *U5*))))) ;;   (non connexe)

(defparameter *z5*
  (build-term
   *add_a_b*
   (list (build-term *ren_d_a* (list (build-term *ren_e_d* (list *W5*))))))) 
;;    (connexe)

;;;;;;;;;;;;
;; Cwd = 6
;; On ajoute f

(defparameter *F-B* (input-cwd-term "add_b_f (oplus(f,b))"))
(defparameter *F-D* (input-cwd-term "add_d_f (oplus(f,d))"))
(defparameter *T6* (cwd-term-multi-oplus *F-B* *A-C* *F-D* *B-C*))

(defparameter *S6* *S5*)
(defparameter *w6* (cwd-term-oplus *S6* *T6*))
(defparameter *u6*
  (build-term *ren_f_a*
	      (list
	       (build-term
		*ren_e_d*
		(list
		 (cwd-term-oplus (input-cwd-term "c") *W5*))))))

(defparameter *V6*
  (build-term
   *add_a_b*
   (list
    (build-term *ren_d_a*  (list *U6*))))) ;; (non connexe)

(defparameter *Z6*
  (build-term
   *add_a_b*
   (list
    (build-term
     *ren_d_a*
     (list
      (build-term
       *ren_f_a*
       (list
	(build-term
	 *ren_e_d* (list  *W5*)))))))))    ;;; (connexe)

(defparameter *t18*
  (cwd-term-multi-oplus
    *A-B* *B-C* *C-D* *D-E* *E-F* *F-G* *G-H* *H-I* *I-J*
    *J-K* *K-L* *L-M* *M-N* *N-O* *O-P* *P-Q* *Q-R*))

(defparameter *v18prime*
  (build-term
   *add_m_o*
   (list *t18*)))

(defparameter *v18*
  (build-term
   *add_a_c*
   (list
    (build-term
     *add_d_f*
     (list
      (build-term
       *add_g_i*
       (list
	(build-term
	 *add_j_l*
	 (list
	  (build-term
	   *add_m_o*
	   (list *t18*)))))))))))

(defparameter *z18*
  (build-term *add_p_r* (list *v18*)))


(defparameter *tprime*
  (cwd-term-unoriented-add ;; d f
   3 5
   (cwd-term-unoriented-add ;; g i
    6 8
    (cwd-term-unoriented-add ;; a c
     0 2
     (cwd-term-multi-oplus
      *A-B* *B-C* *C-D* *D-E* *E-F* *F-G* *G-H* *H-I* *I-J* )))))

(defparameter *sprime*
  (apply-build-term
   *add_j_l*
   (apply-oplus
    (list
     *tprime*
     *J-K*
     *K-L*
     (apply-build-term
      *add_m_o*
      (cwd-term-multi-oplus *L-M* *M-N* *N-O* *O-P* *P-Q* *Q-R*))))))

(defparameter *wprime* (apply-build-term *add_p_r* *sprime*))

(defparameter *foret*
  (cwd-term-multi-oplus
    (build-term (make-cwd-constant-symbol 0))
    (apply-build-term
     (add-symbol 1 2)
     (cwd-term-oplus
      (build-term (make-cwd-constant-symbol 1))
      (build-term (make-cwd-constant-symbol 2))))
    (apply-build-term
     (add-symbol 1 2)
     (cwd-term-oplus
      (apply-build-term
       (ren-symbol 1 0)
       (apply-build-term
	(add-symbol 1 2)
	(cwd-term-oplus
	 (apply-build-term
	  (add-symbol 0 1)
	  (cwd-term-oplus
	   (apply-build-term (make-cwd-constant-symbol 0))
	   (apply-build-term (make-cwd-constant-symbol 1))))
	 (apply-build-term (make-cwd-constant-symbol 2)))))
      (apply-build-term (make-cwd-constant-symbol 2))))))

(defparameter *g*
  (cwd-term-oplus
   (cwd-term-vertex 0)
   (cwd-term-oplus
    (cwd-term-vertex 1)
    (cwd-term-oplus
     (cwd-term-vertex 3)
     (cwd-term-unoriented-add
      1 2
      (cwd-term-oplus (cwd-term-vertex 1) (cwd-term-vertex 2)))))))

(defparameter *h*
  (cwd-term-oplus
   (cwd-term-vertex 0)
   (cwd-term-oplus
    (cwd-term-vertex 3)
    (cwd-term-oplus
     (cwd-term-unoriented-add
      1 2
      (cwd-term-oplus (cwd-term-vertex 1) (cwd-term-vertex 2)))
     (cwd-term-unoriented-add
      1 2
      (cwd-term-oplus (cwd-term-vertex 1) (cwd-term-vertex 2)))))))

(defparameter *foret1*
  (apply-build-term
   (add-symbol 0 1)
   (apply-build-term
    (add-symbol 2 3)
    *g*)))
    
(defparameter *cycle1*
  (apply-build-term
   (add-symbol 0 1)
   (apply-build-term
    (add-symbol 2 3)
    *h*)))

(defparameter *foret2*
  (input-cwd-term
"ren_d_a(
  add_a_d(
   oplus(
    add_c_d(oplus(d,oplus(c,c)))
    add_a_b(
     oplus(
      b,
      add_a_d(oplus(a,oplus(d,d))))))))"))

(defparameter *cycle2*
  (cwd-term-unoriented-add 1 2 *foret2*))

(defparameter *foret3*
  (input-cwd-term
   "add_b_c(oplus(b,add_a_b(add_b_d(oplus(a,oplus(b,d))))))"))

(defparameter *cycle3*
  (apply-build-term
   (add-symbol 0 3)
   *foret3*))

(defparameter *t1*
  (input-cwd-term
" ren_g_a(add_b_c(add_b_e(add_b_g(
oplus(b,oplus(c,oplus(e,add_a_g(oplus(a,g))))))))) "))

(defparameter *t2* (input-cwd-term
"
ren_g_a(
 add_b_f(
  add_b_g(
   oplus(
    b,
    add_c_f(
     oplus(
      oplus(c,f),
      add_a_g(oplus(a,g))))))
"))

(defparameter *t3*
  (input-cwd-term
   "add_a_b(add_b_d(add_b_g(oplus(a,oplus(b,oplus(d,g))))))"))

(defparameter *t4* 
  (apply-build-term
   (ren-symbol 1 2)
   (apply-build-term
    (ren-symbol 0 7)
    *t3*)))

(defparameter *t5* 
  (cwd-term-ren
   3 8
   (cwd-term-ren
    6 10
    (cwd-term-ren 7 11 *t4*))))

(defparameter *t*
  (cwd-term-multi-oplus
    *t1* *t2* *t3* *t4* *t5*
    (cwd-term-vertex 0)
    (cwd-term-vertex 1)
    (cwd-term-vertex 2)
    (cwd-term-vertex 2)))

(defparameter *tprime* (cwd-term-ren 1 5 *t*))

(defparameter *tt* (input-cwd-term "add_b_c(add_c_d(oplus(b,oplus(c,oplus(d,oplus(d,d))))))"))

(defun ff1 (term)
  (cwd-term-unoriented-add 0 1 (cwd-term-ren 2 4 (cwd-term-ren 3 4 (cwd-term-unoriented-add 2 3 term)))))

(defparameter *t*
  (ff1
   (cwd-term-oplus
    *tt*
    (ff1
     (cwd-term-oplus
      *tt*
      (ff1
       (cwd-term-oplus *tt* (ff1 (cwd-term-oplus *tt* (cwd-term-vertex 0))))))))))


(defparameter *star*
  (cwd-term-unoriented-add
   0 2
   (cwd-term-pn-bis 5)))

(defparameter *graph-c5*
  (cwd-term-unoriented-add
   0 2
   (cwd-term-oplus
    (cwd-term-ren
     2 1
     (cwd-term-unoriented-add
      1 2
      (cwd-term-oplus
       (cwd-term-unoriented-add
	0 1
	(cwd-term-oplus
	 (cwd-term-vertex 0)
	 (cwd-term-vertex 1)))
       (cwd-term-unoriented-add
	0 2
	(cwd-term-oplus 
	 (cwd-term-vertex 0)
	 (cwd-term-vertex 2))))))
    (cwd-term-vertex 2))))

(defparameter *c5p*
  (input-cwd-term
   "add_a_c(oplus(a^000001,add_a_c(oplus(ren_c_b(add_b_c(oplus(add_a_b(oplus(a^100000,b^001000)),
add_a_c(oplus(c^000100,a^000010))))),c^010000))))"))

;; (car (arg *c5p*)) reconnu par DEGRE2-X1-X2-U-X3 mais pas *c5p*
(defparameter *c5bis*
  (cwd-term-unoriented-add
   0 2
   (apply-oplus
    *graph-c5*
    (cwd-term-vertex 0))))

(defparameter *c7p*
  (input-cwd-term
   "add_a_c(ren_d_c(ren_c_b(add_c_d(oplus(ren_d_c(ren_c_b(add_c_d(oplus(ren_d_c(ren_c_b(add_c_d(oplus(ren_d_c(ren_c_b(add_c_d(oplus(ren_d_c(ren_c_b(add_c_d(oplus(add_a_c(oplus(a^100000,c^001000)),d^000100)))),d^000010)))),d^010000)))),d^100000)))),d^010000)))))"))

(defparameter *c7* (cwd-term-cycle 7))

(defun cwd-term-grid-clique (n &optional (connected t))
  (let* ((grid (cwd-term-square-grid n))
	 (clique (cwd-term-kn n))
	 (term (cwd-term-oplus grid clique)))
      (if connected
	  (cwd-term-unoriented-add 0 1 term)
	  term)))

(defparameter *tarticle1*
  (input-cwd-term
   "add_b_c(oplus(add_a_b(oplus(a,b)),ren_b_c(add_a_b(oplus(a,b)))))"))

(defparameter *tarticle2*
  (input-cwd-term
   "add_c_d(ren_b_d(ren_a_c(add_a_b(oplus(a,b)))))"))

(defparameter *tarticle3*
  (input-cwd-term
   "ren_b_d(ren_a_c(add_a_b(oplus(a,b))))"))

(defparameter *tarticle4*
  (input-cwd-term
   "ren_a_c(add_a_b(oplus(a,c)))"))

(defun cgraph ()
  (let ((ab (cwd-term-pn 2))
	(bc (cwd-term-unoriented-add 1 2 (cwd-term-stable 2 '(1 2))))
	(ac (cwd-term-pn-bis 2))
	(abc (cwd-term-unoriented-add 1 2 (cwd-term-unoriented-add 0 1 (cwd-term-stable 3 '(0 1 2))))))
    (cwd-term-unoriented-add 0 1
     (cwd-term-multi-oplus
      ab ab ab bc bc ac ac abc))))
