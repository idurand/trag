;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defun incidence-port-p (port)
  (minusp port))

(defgeneric incidence-p (signed-object)
  (:method ((o signed-object))
    (in-signature (make-cwd-constant-symbol -1) (constant-signature o))))
