;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defgeneric symbol-parts (name)
  (:documentation 
   "for a symbol op_a_b, returns the list (op pa pb o) \
where of is a the name op and pa pb are the ports (integers) \
associated with a and b \
for add_a_b (add pa pb nil) \
for add_a->b (add pa pb t)"))

(defun decompose-oriented (string)
  (let ((strings (decompose-string string #\-)))
    (list (first strings) (subseq (second strings) 1)))) ;; to skip >

;; "op" a b [oriented]
(defmethod symbol-parts ((name string))
  (let ((strings (decompose-string name #\_))
	(oriented nil))
    (when (= (length strings) 2)
      (setq strings (cons (car strings) (decompose-oriented (second strings)))
	    oriented t))
    (list (car strings)
	  (string-to-port (second strings))
	  (string-to-port (third strings))
	  oriented)))

;; ;; "op" a b [oriented]
;; (defmethod symbol-parts ((name string))
;;   (let ((strings (name-parts name)))
;;     (cons (car strings) (mapcar #'string-to-port (cdr strings)))))

(defgeneric symbol-to-cwd-symbol (s)
  (:method ((s abstract-symbol))
    (cond
      ((= (arity s) 1)
       (let* ((parts (symbol-parts (name s)))
	      (op (first parts))
	      (a (second parts))
	      (b  (third parts))
	      (oriented (fourth parts)))
	 (cond
	   ((equal *add-string* op) (add-symbol a b :oriented oriented))
	   ((equal *ren-string* op) (ren-symbol a b)))))
      ((equal *oplus-string* (name s))
       (oplus-symbol))
      (t (constant-to-cwd-symbol s)))))

(defgeneric constant-to-cwd-symbol (s)
  (:method ((s color-constant-symbol))
    (make-color-symbol
     (constant-to-cwd-symbol (symbol-of s))
     (color s)))
  (:method ((s constant-symbol))
    (make-cwd-constant-symbol (string-to-port (name s))))
  (:method ((s vbits-constant-symbol))
    (make-vbits-symbol
     (constant-to-cwd-symbol (symbol-of s))
     (vbits s))))

(defgeneric term-to-cwd-term (term)
  (:method ((term term))
    (term-subst-symbols term #'symbol-to-cwd-symbol)))

(defun input-cwd-term (string)
  (term-to-cwd-term (input-term string)))

(defun input-cwd-multi-term (string)
  (term-to-cwd-term (input-multi-term string)))

(defun read-cwd-term (stream)
  (term-to-cwd-term
   (read-term stream)))

(defgeneric load-cwd-term (name)
  (:method ((name string))
    (term-to-cwd-term (load-term name "decomp"))))
