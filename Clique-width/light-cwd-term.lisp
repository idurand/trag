;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defun try-move-ren-down (root arg)
  ;; arg already simplified
  (assert (ren-symbol-p root))
  (let* ((st (car arg))
	 (ports (symbol-ports root))
	 (a (first ports))
	 (ports (graph-ports-of st)))
    (cond
      ((not (ports-member a ports))
       st)
      ((symbol-constant-p root)
       (apply-port-mapping (port-mapping-of root) st))
      ((oplus-symbol-p root)
       (cwd-term-ac-oplus 
	(mapcar (lambda (s)
		  (light-cwd-term-rec
		   (build-term root (list s))))
		(arg st))))
      ((and (add-symbol-p root)
	    (not (ports-member a (ports-of root))))
       ;; inverse ren and add
       (try-move-add-down
	root
	(list
	 (try-move-ren-down root (arg st)))))
      (t (build-term root arg)))))

(defun try-move-add-down (root arg)
  (assert (add-symbol-p root))
  (let* (
	 (st (car arg))
	 (rst (root st))
	 (ports (symbol-ports root))
	 (a (first ports))
	 (b (second ports)))
    (cond
      ((oplus-symbol-p rst)
       (multiple-value-bind (a-or-b no-ab)
	   (split-list (arg st)
		       (lambda (st)
			 (let ((ports (graph-ports-of st)))
			   (or (ports-member a ports) (ports-member b ports)))))
	 (let ((ports-a-or-b (ports-union-gen (mapcar (lambda (s) (graph-ports-of s)) a-or-b))))
	   (if (and (ports-member a ports-a-or-b) (ports-member b ports-a-or-b)) ;; we need an add
	       (let* ((subterm
			(if (endp (cdr a-or-b))
			    (car a-or-b)
			    (cwd-term-ac-oplus a-or-b)))
		      (add-term (build-term root (list subterm))))
		 (cwd-term-ac-oplus (cons add-term no-ab)))
	       (cwd-term-ac-oplus (append a-or-b no-ab)))))) 		      ;; no add
      ((ren-symbol-p rst)
       (let* ((sp (symbol-ports rst))
	      (c (first sp))
	      (d (second sp)))
	 ;;		   (format t "a=~A, b=~A, c=~A, d=~A~%" a b c d)
	 (if (or (= c a) (= c b) (= d a) (= d b))
	     (build-term root arg)
	     (build-term rst (list (light-cwd-term-rec (build-term root (list (car (arg st))))))))))
      (t (build-term root arg)))))  

(defgeneric light-cwd-term-rec (term))
(defmethod light-cwd-term-rec ((term term))
  (let ((root (root term)))
    (if (symbol-constant-p root)
	term
	(let ((arg (mapcar #'light-cwd-term-rec (arg term))))
	  (cond
	    ((add-symbol-p root) ;; try to move add down
	     (try-move-add-down root arg))
	    ((ren-symbol-p root)
	     (try-move-ren-down root arg))
	    ((oplus-symbol-p root) (build-term root arg))
	    (t (warn "unkown cwd-symbol: ~A~%" root)))))))

(defgeneric light-cwd-term (term))
(defmethod light-cwd-term ((term term))
  (let ((nt (light-cwd-term-rec term)))
;;    (assert (decomp::cwd-term-equivalent-p term nt))
    (remove-top-ren nt)))

(defgeneric cwd-term-simplify-rec (term)
  (:method ((cwd-term term))
    (let ((root (root cwd-term)))
      (if (symbol-constant-p root)
	  cwd-term
	  (let ((arg (mapcar #'cwd-term-simplify-rec (arg cwd-term))))
	    (cond ((oplus-symbol-p root) (normalize-oplus (build-term root arg)))
		  ((reh-symbol-p root) (warn "todo"))
		  (t (let* ((ports (symbol-ports root))
			    (a (first ports))
			    (b (second ports))
			    (subterm (car arg)))
		       (if (eq (root subterm) root)
			   subterm
			   (cond
			     ((add-symbol-p root)
			      (cwd-term-add a b subterm (oriented-p root)))
			     ((ren-symbol-p root)
			      (cwd-term-conditional-ren a b subterm))
			     (t (error "unknown operation ~A" root))))))))))))
    
(defgeneric cwd-term-simplify (term)
  (:method ((cwd-term term))
    (light-cwd-term (cwd-term-simplify-rec cwd-term))))
    
;; (defparameter *e* 
;; 	     (make-no-duplicates-enumerator 
;; 	      (make-funcall-enumerator #'clique-width::cwd-term-simplify (termauto::terms-depth=-enumerator (cwd-signature 2) 3))
;; 	      :test #'compare-term))
