;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defun mcgee2 ()
  (cwd-term-unoriented-add
   5 8
   (cwd-term-unoriented-add
    3 9
    (cwd-term-unoriented-add
     3 8
     (cwd-term-unoriented-add
      2 6
      (cwd-term-unoriented-add
       1 4 
       (cwd-term-unoriented-add
	1 9
	(cwd-term-unoriented-add
	 3 8
	 (cwd-term-unoriented-add
	  2 5
	  (cwd-term-unoriented-add
	   4 6
	   (cwd-term-unoriented-add
	    4 7 
	    (cwd-term-oplus
	     (cwd-term-vertex 4)
	     (cwd-term-ren
	      4 0
	      (cwd-term-unoriented-add
	       4 5 ; 4 libre
	       (cwd-term-oplus
		(cwd-term-vertex 5)
		(cwd-term-ren
		 5 0
		 (cwd-term-unoriented-add
		  2 5 ; 5 libre
		  (cwd-term-oplus
		   (cwd-term-vertex 2)
		   (cwd-term-ren
		    2 0
		    (cwd-term-unoriented-add
		     2 3 ; 2 libre
		     (cwd-term-oplus
		      (cwd-term-vertex 3)
		      (cwd-term-ren
		       3 0
		       (cwd-term-unoriented-add
			3 8 ; 3 libre
			(cwd-term-oplus
			 (cwd-term-vertex 8)
			 (cwd-term-ren
			  8 0 (cwd-term-unoriented-add
			       1 8 ; 8 libre
			       (cwd-term-oplus
				(cwd-term-vertex 1)
				(cwd-term-ren
				 1 0
				 (cwd-term-unoriented-add
				  1 9 ; 1 libre
				  (cwd-term-oplus
				   (cwd-term-vertex 9)
				   (cwd-term-ren
				    9 0
				    (cwd-term-unoriented-add
				     6 9 ; 9 libre
				     (cwd-term-oplus
				      (cwd-term-vertex 6)
				      (cwd-term-unoriented-add
				       3 7
				       (cwd-term-unoriented-add
					4 8
					(cwd-term-unoriented-add
					 2 9
					 (cwd-term-unoriented-add
					  1 5
					  (cwd-term-ren
					   6 0
					   (cwd-term-unoriented-add
					    6 7 ; 6 libre
					    (cwd-term-oplus
					     (cwd-term-vertex 7)
					     (cwd-term-ren
					      7 0
					      (cwd-term-unoriented-add
					       4 7 ; 7 libre
					       (cwd-term-oplus
						(cwd-term-vertex 4)
						(cwd-term-ren
						 4 0
						 (cwd-term-unoriented-add
						  4 5 ; 4 libre
						  (cwd-term-oplus
						   (cwd-term-vertex 5)
						   (cwd-term-ren
						    5 0
						    (cwd-term-unoriented-add
						     2 5 ; 5 libre
						     (cwd-term-oplus
						      (cwd-term-vertex 2)
						      (cwd-term-ren
						       2 0
						       (cwd-term-unoriented-add
							2 3 ; 2 libre
							(cwd-term-oplus
							 (cwd-term-vertex 3)
							 (cwd-term-ren
							  3 0 (cwd-term-unoriented-add
							       3 8 ; 3 libre
							       (cwd-term-oplus
								(cwd-term-vertex 8)
								(cwd-term-ren
								 8 0
								 (cwd-term-unoriented-add
								  1 8 ; 8 libre
								  (cwd-term-oplus
								   (cwd-term-vertex 1)
								   (cwd-term-ren
								    1 0
								    (cwd-term-unoriented-add
								     1 9 ; 1 libre
								     (cwd-term-oplus
								      (cwd-term-vertex 9)
								      (cwd-term-unoriented-add
								       1 8
								       (cwd-term-unoriented-add
									7 8
									(cwd-term-unoriented-add
									 6 7
									 (cwd-term-unoriented-add
									  5 6 (cwd-term-unoriented-add
									       4 5
									       (cwd-term-unoriented-add
										3 4
										(cwd-term-unoriented-add
										 2 3
										 (cwd-term-unoriented-add
										  1 2
										  (cwd-term-multi-oplus-list
										   (mapcar #'cwd-term-vertex (iota 8 1))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


;; (defparameter *f* (k-acyclic-colorability-automaton 3 10))

;; 2011-06-19 avec annotations sur faucon

;; (with-time (print (setf *res* (recognized-p (amcgee) *f*))))
;; T  in 778min 24.99sec
;; NIL

;; 2011-08-15
;; * *max-number-of-states*

;; 2013-08-03
;; AUTOGRAPH> (with-time (enum-recognized-p *amcgee* (k-acyclic-colorability-automaton 3)))
;; in 1.208sec
;; T

;; 100000
;; * (with-time (defparameter *target* (compute-target-and-attributes (amcgee) *f* :afun ???)
;; in 554min 13.873sec

;; NIL
;; * (run-count *target*) 57024
;; * (max-run-count *target*) 2

;; 2011-08-18
;;  in 1030min 18.781sec
;;  STATES-TABLE     = #<HASH-TABLE :TEST EQUAL :COUNT 3111735 {101DEE3881}>

;; in 572min 30.745sec

;; * (run-count *target*)

;; 57024
;; * (max-run-count *target*)

;; 2

;; 2011-10-09
;; (defparameter *3-ac-10* (k-acyclic-colorability-automaton 3 10))
;; (with-time (defparameter *target* (compute-target-counting-runs (amcgee) *3-ac-10*)))
;; in 629mn
;; 2011-10-11
;; AUTOGRAPH> (defparameter *3-ac-10* (k-acyclic-colorability-automaton 3 10))
;; *3-AC-10*
;; AUTOGRAPH> (let ((*clean-states-table* t))
;; 	     (with-time 
;; 	      (defparameter *target* (compute-target-counting-runs (amcgee) *3-ac-10*))))
;;  in 526min 36.691sec
;; NIL
;; AUTOGRAPH> (target-attribute *target*)
;; 56808

;; AUTOGRAPH> (let ((*clean-states-table* t))
;; 	     (with-time 
;; 	      (defparameter *target* (compute-target-counting-runs (amcgee) *3-ac-10*))))
;;  in 626min 36.691sec 2012-06-12
;; T

;; 2012-06-08
;; 1178min 44.48sec(defparameter *f* (k-acyclic-colorability-automaton 3 10))
;; *F*
;; AUTOGRAPH> (with-time (recognized-p *mcgee* *f*)) 
;; sans annotation!(defparameter *f* (k-acyclic-colorability-automaton 3 10))

;;(defparameter *f* (color-k-acyclic-colorability-automaton 3))
;; (defparameter *amcgee* (amcgee))
;; *clean-states-table*
;; T
;; (with-time (defparameter *res* (compute-target *amcgee* *f*)))
;; 2012-11-17 in 362min 45.941sec
;; 2012-12-14 in 217min 5.64sec
;; 2013-04-23 in 167min 23.332sec

;; 2013
;; fait en 32 min mais pas possible à reproduire

;; AUTOGRAPH> (with-time 
;;  	     (defparameter *target* 
;;  	       (compute-target-counting-runs *amcgee* *f*)))
;; 2012-12-14 in 586min 52.335sec
;; 2013-04-23 in 352min 33.586sec
;; AUTOGRAPH> (target-attribute *target*)
;; 57024
;; 56808 (fausse valeur septembre 2012)


;; 2013-08-07
;; (defparameter *f* (color-k-acyclic-colorability-automaton 3))
;; (defparameter *amcgee* (amcgee))
;; (with-time (defparameter *res* (compute-target *amcgee* *f*)))

;; 2013-08-08
;; (with-time (defparameter *target* (compute-target-counting-runs (amcgee) (color-k-acyclic-colorability-automaton 3))))
;;  in 52min 56.272sec
;; (termauto::target-attribute *target*)
;; 57024
;; T
;; (defparameter *f* (color-acyclic-coloring-automaton 3))
;; (defparameter *af* (attribute-automaton *f* *count-afun*))
;; (defparameter *p* (constants-color-projection *af*))
;; (with-time (defparameter *target* (compute-target (amcgee) *p*)))
;;  in 51min 47.719sec
;; (termauto::target-attribute *target*)
;; 57024
;; T

