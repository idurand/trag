;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)
(defgeneric save-cwd-term (term name))

(defmethod save-cwd-term ((term term) (name string))
  (save-term term name "decomp"))
