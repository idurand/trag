;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Mikhail Raskin

(in-package :clique-width)

(defun parse-port (x &optional default)
  (or
   (and (integerp x) x)
   (ignore-errors (parse-integer x))
   (and (stringp x) (= (length x) 1)
	(<= 0 (- (char-code (elt x 0)) (char-code #\a)) 29)
	(- (char-code (elt x 0)) (char-code #\a)))
   (and (= (length x) 0) (or default 0))
   (error "cannot parse value: ~a" x)))
