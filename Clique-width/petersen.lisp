;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defun petersen-chromatic-polynomial (k)
  (let* ((k2 (* k k))
	 (k3 (* k k2))
	 (k4 (* k k3))
	 (k5 (* k k4))
	 (k6 (* k k5))
	 (k7 (* k k6)))
  (*
   k
   (1- k)
   (- k 2)
   (-
    (+ k7 (* 67 k5) (* 529 k3) (* 775 k))
    (* 12 k6) 
    (* 230 k4) 
    (* 814 k2)
    352))))

;; *clean-states-table* nil
;; AUTOGRAPH> (defparameter *4-ac-7* (k-acyclic-colorability-automaton 4 7))
;; *4-AC-7*
;; AUTOGRAPH> (with-time (defparameter *target* (compute-target-counting-runs (petersen) *4-ac-7*)))
;; in 10min 43.954sec

;; 2012-01-04
;; *clean-states-table* t
;; AUTOGRAPH> (with-time (defparameter *target* (compute-target-counting-runs (apetersen) *4-ac-7*)))
;;  in 5min 25.8sec
;; AUTOGRAPH> (target-attribute *target*)
;; 10800

;; 2012-05-04
;; AUTOGRAPH> (defparameter *e* (final-target-enumerator *t* *f*))
;; *E*
;; AUTOGRAPH> (with-time (call-enumerator *e*))
;;  in 50.11sec
;; !<!Okk,!#f,!<{aef}>,!<{abd}>,!<{acg}>,![@a,@e,@f],![@a,@b,@d],![@a,@c,@g],![@d,<{ab-ae-be-af},{aa-ab-ae-af-bf-ef}>],![@e,<{aa-cf-ag-fg},{ac-af-ag-cg}>],![@g,<{aa-bc-ad-cd},{ab-ac-ad-bd}>]>
;; AUTOGRAPH> (with-time (call-enumerator *e*))
;;  in 0.024sec
;; !<!Okk,!<{g}>,!<{aef}>,!<{abd}>,!<{ac}>,![@e,<{ag-fg},{af}>],![@a,@b,@d,@g],![@a,@c,@g],![@d,<{ab-ae-be-af},{aa-ab-ae-af-bf-ef}>],![@e,<{aa},{}>,<{cf},{}>],![<{aa-bc-ad-cd},{ab-ac-ad-bd}>]>
;; AUTOGRAPH> (with-time (call-enumerator *e*))
;;  in 0.015sec
;; !<!Okk,!<{f}>,!<{ae}>,!<{abd}>,!<{acg}>,![@a,@e,@f],![@b,@d,<{af},{}>],![@a,<{cf-fg},{cg}>],![@d,<{ab-ae-be},{aa-ab-ae}>],![@c,@e,<{aa-ag},{ag}>],![@g,<{aa-bc-ad-cd},{ab-ac-ad-bd}>]>
;; AUTOGRAPH> (defparameter *e* (final-target-enumerator (petersen) *f*))
;; *E*
;; AUTOGRAPH> (with-time (call-enumerator *e*))
;;  in 52.04sec
;; !<!Okk,!<{g}>,!<{aef}>,!<{abd}>,!<{ac}>,![@e,<{ag-fg},{af}>],![@a,@b,@d,@g],![@a,@a,<{cg},{}>],![@d,<{ab-ae-be-af},{aa-ab-ae-af-bf-ef}>],![<{aa},{}>,<{af},{}>,<{ce},{}>],![<{aa-ab-ad-cd},{aa-ab-ac-bc-ad-bd}>]>
;; AUTOGRAPH> (with-time (call-enumerator *e*))
;;  in 0.028sec
;; !<!Okk,!<{f}>,!<{ae}>,!<{abdg}>,!<{ac}>,![@a,@e,@f],![@b,@d,<{af-fg},{ag}>],![@a,@c,<{af},{}>],![@d,<{ab-ae-be-ag},{aa-ab-ae-ag-bg-eg}>],![@a,<{aa},{}>,<{ce},{}>],![<{aa-ab-ad-cd-cg},{aa-ab-ac-bc-ad-bd-ag-bg-dg}>]>
;; AUTOGRAPH> (with-time (length (enum::collect-enum-set *e*)))
;;  in 52min 40.891sec
;; 10800
;; AUTOGRAPH> (with-time (length (enum::collect-enum-set *e*))) 2012-06-11
;;  in 1min 34.657sec
10800
;; AUTOGRAPH> (with-time (recognized-p (petersen) *f*))
;; in 7min 7.218sec
;; T
;; AUTOGRAPH> (with-time (recognized-p (apetersen) *f*)) 2012-06-11
;;  in 3min 2.771sec
;; T

(defun petersen6 ()
  (input-cwd-term
"
add_b_c(
 oplus(c,
  ren_d_b(
   ren_c_a(
    add_c_d(
     oplus(d,
      ren_e_c(
       ren_d_a(
        add_d_e(
         oplus(e,
          add_a_c(
           oplus(c,
            ren_f_d(
             ren_e_b(
              ren_c_a(
               add_e_f(
                add_c_f(
                 oplus(f,
                  add_b_c(
                   oplus(c,
                    add_b_d(
                     oplus(b,
                      add_a_d(
                       oplus(d,
                        add_a_e(
                         oplus(a,e))))))))))))))))))))))))))
"))
