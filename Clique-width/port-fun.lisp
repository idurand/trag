;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

;; to factorize port-int-fun and color-fun

;; a port-fun is a function from the set of ports to any domain
;; represented by a hash-table

(defgeneric copy-port-fun (port-fun))
(defgeneric default-value (port-fun))
(defgeneric reset-port-value (port port-fun))
(defgeneric get-port-value (port port-fun))
(defgeneric set-port-value (value port port-fun))
(defgeneric print-port-fun (port-fun stream))
(defgeneric hfun (port-fun))

(defclass port-fun ()
  ((hfun :type hash-table :reader hfun :initform (make-hash-table))))

(defun make-port-fun (class)
  (make-instance class))

(defmethod ports-of ((port-fun port-fun))
  (make-ports (list-keys (hfun port-fun))))

(defmethod reset-port-value ((port integer) (port-fun port-fun))
  (remhash port (hfun port-fun)))

(defmethod set-port-value (value (port integer) (port-fun port-fun))
  (setf (gethash port (hfun port-fun)) value))

(defmethod get-port-value ((port integer) (port-fun port-fun))
  (multiple-value-bind (value found) (gethash port (hfun port-fun))
    (if found
	value
	(default-value port-fun))))

(defmethod print-port-fun ((port-fun port-fun) stream)
  (when *print-object-readably* (format stream "["))
  (let ((hfun (hfun port-fun)))
    (when (plusp (hash-table-count hfun))
      (let* ((ints (sort (list-keys hfun) #'<))
	     (int (car ints))
	     (value (gethash int hfun)))
	(when value
	  (format stream "~A:~A" 
		  (port-to-string int) (gethash (car ints) hfun)))
	(loop 
	   for int in (cdr ints)
	   do (setq value (gethash int hfun))
	   when value
	     do (format stream " ~A:~A" (port-to-string int) value)))))
  (when *print-object-readably*  (format stream "]")))

(defmethod print-object ((port-fun port-fun) stream)
  (print-port-fun port-fun stream))

(defmethod copy-port-fun ((port-fun port-fun))
  (let ((new-port-fun (make-port-fun (class-of port-fun))))
    (maphash
     (lambda (port value)
       (set-port-value value port new-port-fun))
     (hfun port-fun))
    new-port-fun))

(defmethod map-port-fun (fun (port-fun port-fun))
  (let ((new-port-fun (make-port-fun (class-of port-fun))))
    (maphash
     (lambda (port value)
       (set-port-value (funcall fun value) port new-port-fun))
     (hfun port-fun))
    new-port-fun))

(defmethod compare-object ((port-fun1 port-fun) (port-fun2 port-fun))
  (hash-table-equal-p (hfun port-fun1) (hfun port-fun1) #'compare-object))

(defgeneric port-in-port-fun-p (port port-fun)
  (:method ((port integer) (port-fun port-fun))
    (multiple-value-bind (v exists) (gethash port port-fun)
      (declare (ignore v))
      exists)))
