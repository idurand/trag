;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass port-int-fun (port-fun) ())

(defmethod default-value ((port-int-fun port-int-fun)) 0)

(defmethod compare-object ((pf1 port-int-fun) (pf2 port-int-fun))
  (hash-table-equal-p (hfun pf1) (hfun pf2) #'=))

(defun make-port-int-fun (class)
  (make-port-fun class))

(defgeneric incf-port-value (inc port port-fun))
(defmethod incf-port-value (inc (port integer) (port-fun port-fun))
  (set-port-value (+ (get-port-value port port-fun) inc) port port-fun))
