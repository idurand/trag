;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defgeneric apply-port-mapping (port-mapping cwd-mixin))


(defun ppair-to-string (pair)
  (format nil "~A-~A"
	  (port-to-string (car pair))
	  (port-to-string (cdr pair))))

(defgeneric port-mapping-empty-p (port-mapping))

(defclass port-mapping ()
  ((mapping-pairs :initarg :pairs :reader mapping-pairs)))

(defmethod port-mapping-empty-p ((port-mapping port-mapping))
  (endp (mapping-pairs port-mapping)))

(defmethod print-object ((port-mapping port-mapping) stream)
  (loop
    initially (format stream "[")
    for pair in (mapping-pairs port-mapping)
    do (format stream "~A " (ppair-to-string pair))
    finally (format stream "]")))

(defmethod ports-of ((port-mapping port-mapping))
  (ports-of (mapping-pairs port-mapping)))

(defun clean-mapping (mapping)
  (setq mapping (remove-if (lambda (p) (= (car p) (cdr p))) mapping))
  (sort (remove-duplicates mapping :test #'equal) #'cons<))

(defun mapping-extremities (mapping)
  (mapcar #'cdr mapping))

(defun mapping-origins (mapping)
  (mapcar #'car mapping))

(defgeneric port-mapping-origins ( port-mapping))
(defmethod port-mapping-origins ((port-mapping port-mapping))
  (make-ports
   (mapping-origins  (mapping-pairs port-mapping))))

(defun make-port-mapping (pairs)
  (make-instance 'port-mapping :pairs (clean-mapping pairs)))

(defgeneric restrict-port-mapping (port-mapping ports))
(defmethod restrict-port-mapping ((port-mapping port-mapping) (ports ports))
  (when (ports-empty-p ports)
    (return-from restrict-port-mapping port-mapping))
  (make-port-mapping
   (remove-if-not (lambda (p) (ports-member (car p) ports))
		  (mapping-pairs port-mapping))))
  
(defgeneric port-mapping-injective-p (port-mapping))
(defmethod port-mapping-injective-p ((mapping-pairs list))
  (let ((extremities (mapcar #'cdr mapping-pairs)))
    (= (length extremities) (length (remove-duplicates extremities :test #'=)))))

(defmethod port-mapping-injective-p ((port-mapping port-mapping))
  (port-mapping-injective-p (mapping-pairs port-mapping)))

(defgeneric port-mapping-identity-p (port-mapping))
(defmethod port-mapping-identity-p ((port-mapping port-mapping))
  (every (lambda (pair) (= (car pair) (cdr pair))) (mapping-pairs port-mapping)))

(defgeneric port-to-new-port (port port-mapping))
(defmethod port-to-new-port ((port integer) (port-mapping port-mapping))
  (let ((found (assoc port (mapping-pairs port-mapping))))
    (if found
	(cdr found)
	port)))

(defmethod apply-port-mapping (port-mapping cwd-mixin))
(defmethod apply-port-mapping ((port-mapping port-mapping) (port integer))
  (port-to-new-port port port-mapping))

(defmethod apply-port-mapping ((port-mapping port-mapping) (l list))
  (mapcar (lambda (x) (apply-port-mapping port-mapping x)) l))

(defmethod apply-port-mapping ((port-mapping port-mapping) (ports ports))
  (make-ports (apply-port-mapping port-mapping (container-contents ports))))

(defgeneric inverse-port-mapping (port-mapping))
(defmethod inverse-port-mapping ((port-mapping port-mapping))
  (assert (port-mapping-injective-p port-mapping))
  (make-port-mapping
   (mapcar (lambda (pair) (cons (cdr pair) (car pair)))
	   (mapping-pairs port-mapping))))

(defgeneric compose-port-mapping (port-mapping1 port-mapping2))
(defmethod compose-port-mapping ((m1 list) (m2 list))
  (when (endp m2)
      (return-from compose-port-mapping m1))
  (when (endp m1)
      (return-from compose-port-mapping m2))
  (remove nil 
	  (loop
	    for pair in m1
	    collect (cons (car pair) (port-to-new-port (cdr pair) m2))) :key #'cdr))

(defmethod compose-port-mapping ((p1 port-mapping) (p2 port-mapping))
  (let* ((m1 (mapping-pairs p1))
	 (origins1 (mapping-origins m1))
	 (m2 (mapping-pairs p2))
	 (pairs))
    (loop for pair1 in m1
	  do (let ((p (port-to-new-port (cdr pair1) p2)))
	       (when p (push (cons (car pair1) p) pairs))))
    (loop for pair2 in m2
	  do (let ((o2 (car pair2)))
	       (unless (member o2 origins1)
		 (let ((p (port-to-new-port o2 p2)))
		   (when p (push (cons o2 p) pairs))))))
    (make-port-mapping pairs)))

(defgeneric sequentialize-mapping (mapping-pairs reserved-ports))

(defmethod sequentialize-mapping ((mapping-pairs list) (reserved-ports ports))
  (let* ((origins (mapping-origins mapping-pairs))
	 (extremities (mapping-extremities mapping-pairs))
	 (inter (intersection origins extremities :test #'=))
	 (ports (ports-union (ports-of mapping-pairs) reserved-ports))
	 (pairs '())
	 (tmp '()))
    (loop
      for pair in mapping-pairs
      do (let ((a (car pair))
	       (b (cdr pair)))
	   (if (member b inter)
	       (let ((c (car (rassoc b tmp))))
		 (unless c
		   (setq c (next-port ports))
		   (ports-nadjoin c ports))
		 (push (cons c b) tmp)
		 (push (cons a c) pairs))
	       (push (cons a b) pairs)))
	  finally (return (nconc pairs tmp)))))

(defmethod sequentialize-mapping ((port-mapping port-mapping) (reserved-ports ports))
  (make-port-mapping
   (sequentialize-mapping (mapping-pairs port-mapping) reserved-ports)))
