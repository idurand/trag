;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass port-mixin (cwd-mixin)
  ((port :initarg :port :reader port)))

(defmethod ports-of ((port-mixin port-mixin))
  (make-ports (list (port port-mixin))))
