;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass cwd-mixin () ())

(defgeneric cwd-mixin-ren (a b cwd-mixin)
  (:method ((a integer) (b integer) (port integer))
    (subst b a port)))

(defvar *char-ports* "if T we output a port as a character otherwise as an integer")
(setq *char-ports* t)

(defun toggle-char-ports ()
  "negate the *char-ports* variable"
  (setf *char-ports* (null *char-ports*)))

(defun char-to-number (c)
  "#\a -> 0, #\b -> 1, ..."
  (- (char-int c) 97))

(defun number-to-char (i &optional (lower-case t))
  ;;  (assert (<= i 25))
  "0 -> #\a, 1 -> #\b, ..."
  (code-char (+ (if lower-case i (- i 32)) 97)))

(defun string-to-number (s)
  " \"a\" -> 0, \"b\" -> 1, ..."
  (let ((port (char-to-number (aref s 0))))
    (if (= 2 (length s))
	(- port)
	port)))

(defun number-to-string (i &optional (lower-case t))
  "0 -> \"a\", 1 -> \"b\", ..."
  (if (and *char-ports* (<= -25 i 25))
    (let ((string
            (format nil "~A" (number-to-char (abs i) lower-case))))
      (if (minusp i)
        (format nil "~A#" string)
        string))
    (format nil "~a" i)))

(defun char-to-port (char)
  (char-to-number char))

(defun string-to-port (name)
  "\"a\" -> 0, \"b\" -> 1, ..."
  (if (or (char<= #\0 (elt name 0) #\9) (equal (elt name 0) #\-))
      (parse-integer name)
      (string-to-number name)
      ))

(defun port-iota (cwd) (iota cwd))

(defun port-to-string (number)
  (if *char-ports*
      (number-to-string number)
      (format nil "~A" number)))

