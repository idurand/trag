;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass ports-relation (oriented-mixin)
  ((pairs :type pairs :initarg :pairs :reader pairs))
  (:documentation "pour stocker couples de ports connectes par -*- "))

(defgeneric ports-relation-to-string (ports-relation))
(defmethod ports-relation-to-string ((ports-relation ports-relation))
  (pairs-to-string (pairs ports-relation) (oriented-p ports-relation)))

(defmethod compare-object ((o1 ports-relation) (o2 ports-relation))
  (when *debug* (assert (eq (oriented-p o1)
			    (oriented-p o2))))
  (compare-object (pairs o1) (pairs o2)))

(defgeneric make-ports-relation (pairs oriented))
(defmethod make-ports-relation ((pairs pairs) oriented)
  (unless oriented
    (assert
     (container-every (lambda (pair) (container-member (reverse pair) pairs)) pairs)))
  (make-instance
   'ports-relation
   :pairs pairs :oriented oriented))

(defmethod make-ports-relation ((pairs list) oriented)
  (make-ports-relation
   (make-pairs-according-to-oriented pairs oriented) oriented))

(defun make-empty-ports-relation (oriented)
  (make-ports-relation (make-empty-pairs) oriented))

(defgeneric ports-relation-copy (ports-relation))
(defmethod ports-relation-copy ((p ports-relation))
  (make-ports-relation (pairs-copy (pairs p)) (oriented-p p)))

(defmethod strictly-ordered-p ((po1 ports-relation) (po2 ports-relation))
  (strictly-ordered-p (pairs po1) (pairs po2)))

(defmethod print-object ((ports-relation ports-relation) stream)
  (format stream "R~A" (pairs-to-string (pairs ports-relation) (oriented-p ports-relation))))

(defmethod ports-of ((ports-relation ports-relation))
  (ports-of (pairs ports-relation)))

(defgeneric ports-relation-empty-p (ports-relation))
(defmethod ports-relation-empty-p ((ports-relation ports-relation))
  (zerop (container-size (pairs ports-relation))))


(defgeneric ports-relation-subset-p (ports-relation1 ports-relation2))
(defmethod ports-relation-subset-p ((p1 ports-relation) (p2 ports-relation))
  (pairs-subset-p (pairs p1) (pairs p2)))

(defgeneric ports-relation-member (pair ports-relation))
(defmethod ports-relation-member ((pair list) (p ports-relation))
   (pairs-member pair (pairs p)))

(defgeneric ports-relation-union (ports-relation1 ports-relation2))
(defmethod ports-relation-union ((p1 ports-relation) (p2 ports-relation))
  (make-ports-relation
   (pairs-union (pairs p1) (pairs p2))
   (oriented-p p1)))

(defgeneric ports-relation-subst (a b ports-relation))
(defmethod ports-relation-subst (a b (ports-relation ports-relation))
  (make-ports-relation (pairs-subst a b (pairs ports-relation)) (oriented-p ports-relation)))

(defgeneric ports-relation-difference (ports-relation1 ports-relation2))

(defmethod ports-relation-difference ((p1 ports-relation) (p2 ports-relation))
  (make-ports-relation
   (pairs-difference (pairs p1) (pairs p2)) (oriented-p p1)))

(defgeneric ports-relation-remove (pair ports-relation))
(defmethod ports-relation-remove ((pair list) (p ports-relation))
  (let ((oriented (oriented-p p))
	(new-pairs (pairs-remove pair (pairs p))))
    (unless oriented
      (pairs-delete (reverse pair) new-pairs))
  (make-ports-relation new-pairs oriented)))

(defgeneric ports-relation-remove-if-not (pair ports-relation))
(defmethod ports-relation-remove-if-not (pred (p ports-relation))
  (let* ((oriented (oriented-p p))
	 (new (make-empty-ports-relation oriented)))
    (container-map (lambda (pair)
		     (when (funcall pred pair)
		       (pairs-nadjoin pair (pairs new)))
		     (unless oriented
		       (setq pair (reverse pair))
		       (when (funcall pred pair)
			 (pairs-nadjoin pair (pairs new)))))
		   (pairs p))
    new))

(defgeneric ports-relation-nadjoin (pair ports-relation))
(defmethod ports-relation-nadjoin ((pair list) (p ports-relation))
  (pairs-nadjoin pair (pairs p))
  (unless (oriented-p p)
    (pairs-nadjoin (reverse pair) (pairs p)))
  p)

(defgeneric ports-relation-adjoin (pair ports-relation))
(defmethod ports-relation-adjoin ((pair list) (p ports-relation))
  (ports-relation-nadjoin pair (ports-relation-copy p)))

(defgeneric new-ports-relation-for-add (a b arcs))
(defmethod new-ports-relation-for-add (a b (arcs ports-relation))
  (make-ports-relation
   (new-pairs-for-add a b (pairs arcs) (oriented-p arcs))
   (oriented-p arcs)))

(defgeneric ports-relation-union-gen (ports-relations oriented))
(defmethod ports-relation-union-gen ((ports-relations list) oriented)
  (loop
    with new-ports-relation = (make-empty-ports-relation oriented)
    for ports-relation in ports-relations
    do (pairs-nunion (pairs new-ports-relation) (pairs ports-relation))
    finally (return new-ports-relation)))

(defgeneric ports-relation-intersection (ports-relation1 ports-relation2))
(defmethod ports-relation-intersection ((p1 ports-relation) (p2 ports-relation))
  (make-ports-relation
   (pairs-intersection (pairs p1) (pairs p2))
   (oriented-p p1)))

(defgeneric ports-relation-intersection-empty-p (ports-relation1 ports-relation2))
(defmethod ports-relation-intersection-empty-p ((ports-relation1 ports-relation) (ports-relation2 ports-relation))
  (pairs-empty-p (pairs-intersection (pairs ports-relation1) (pairs ports-relation2))))
