;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defclass abstract-ports (cwd-mixin) ()
  (:documentation "to indicate a set or multiset of ports"))

(defclass ports (abstract-ports ordered-container) ()
  (:documentation "ordered set of ports"))

(defclass mports (abstract-ports ordered-multi-max-container) ()
  (:documentation "multiset of ports with max multiplicity"))

(defclass 2ports (mports) ()
  (:documentation "multiset of ports with max multiplicity 2"))

(defclass kports (abstract-ports ordered-multi-container) ()
  (:documentation "multiset of ports"))

(defgeneric kports-max-multiplicity (kports)
  (:method ((kports kports))
    (container-reduce #'max kports :key #'attribute-of :initial-value 0)))

(defgeneric make-ports (ports)
  (:method ((ports list))
    (make-container-generic ports 'ports #'eql)))

(defgeneric make-empty-ports ()
  (:method () (make-ports '())))

(defgeneric make-ports-from-port (port)
  (:method ((port integer))
    (make-ports (list port))))

(defgeneric make-2ports (ports)
  (:method ((ports list))
    (make-multi-max-container-generic ports '2ports #'eql 2)))

(defgeneric make-kports (ports)
  (:method ((ports list))
    (make-multi-container-generic ports 'kports #'eql #'+)))

(defgeneric make-mports (m ports)
  (:method ((m integer) (ports list))
    (make-multi-max-container-generic ports 'mports #'eql m)))

(defun ports-to-string (alpha)
  (if (null alpha)
      "" 
      (let ((s (port-to-string (car alpha))))
	(loop for e in (cdr alpha)
	      do (setf s
		       (format nil "~A~A"  s (port-to-string e))))
	s)))

(defgeneric print-ports (ports stream)
  (:method ((ports list) stream)
    (format stream "{~A}" (ports-to-string ports))))

(defmethod print-object :around ((ports mports) stream)
  (if *char-ports*
      (print-ports (multi-container-list ports) stream)
      (call-next-method)))

(defmethod print-object :around ((ports ports) stream)
  (if *char-ports*
      (print-ports (container-contents ports) stream)
      (call-next-method)))

(defgeneric ports-subst (new old abstract-ports)
  (:method (new old (abstract-ports abstract-ports))
    (container-subst new old abstract-ports)))

(defmethod cwd-mixin-ren ((a integer) (b integer) (abstract-ports abstract-ports))
  (ports-subst b a abstract-ports))

(defgeneric ports-union (ports1 ports2)
  (:method ((ports1 abstract-ports) (ports2 abstract-ports))
    (container-union ports1 ports2)))

(defgeneric ports-intersection (ports1 ports2)
  (:method ((ports1 abstract-ports) (ports2 abstract-ports))
    (container-intersection ports1 ports2)))

(defgeneric ports-difference (ports1 ports2)
  (:method ((ports1 abstract-ports) (ports2 abstract-ports))
    (container-difference ports1 ports2)))

(defgeneric ports-subset-p (ports1 ports2)
  (:method ((ports1 abstract-ports) (ports2 abstract-ports))
    (container-subset-p ports1 ports2)))

(defgeneric ports-equal-p (ports1 ports2)
  (:method ((ports1 abstract-ports) (ports2 abstract-ports))
    (container-equal-p ports1 ports2)))

(defgeneric ports-member (port abstract-ports)
  (:method ((port integer) (ports abstract-ports))
    (container-member port ports)))

(defgeneric ports-adjoin (port ports)
  (:method ((port integer) (abstract-ports abstract-ports))
    (container-adjoin port abstract-ports)))

(defgeneric ports-nadjoin (port ports)
  (:method ((port integer) (ports abstract-ports))
    (container-nadjoin port ports)))

(defgeneric ports-nunion (ports1 ports2)
  (:method ((ports1 abstract-ports) (ports2 abstract-ports))
    (container-nunion ports1 ports2)))

(defgeneric ports-remove (port ports)
  (:method ((port integer) (ports abstract-ports))
    (container-remove port ports)))

(defgeneric ports-remove-if (pred ports)
  (:method (pred (ports abstract-ports))
    (container-remove-if pred ports)))

(defgeneric ports-remove-if-not (pred ports)
  (:method (pred (ports abstract-ports))
    (container-remove-if-not pred ports)))

(defgeneric ports-empty-p (ports)
  (:method ((ports ports))
    (container-empty-p ports)))

(defgeneric ports-size (ports)
  (:method ((ports ports))
    (container-size ports)))

(defgeneric ports-union-gen (portss)
  (:method ((portss list))
    (reduce #'ports-union portss :initial-value (make-empty-ports))))

(defgeneric 2ports-union-gen (portss)
  (:method ((portss list))
    (reduce #'ports-union portss :initial-value (make-2ports '()))))

(defvar *reserved-ports*)

(defgeneric next-port (reserved-ports)
  (:documentation "smallest port not belonging to reserved-ports")
  (:method ((reserved-ports ports))
    (loop
      with port = 0
      while (ports-member port reserved-ports)
      do (incf port)
      finally (return port))))

(defgeneric port-count (port abstract-ports)
  (:method ((port integer) (abstract-ports abstract-ports))
    (let ((e (container-member port abstract-ports)))
      (if e
	  (attribute-of e)
	  0))))

(defgeneric ports-of (cwd-mixin)
  (:documentation "ports (container) containing the ports of CWD-MIXIN")
  (:method (cwd-object)
    (warn "port-of undefined ~A~%" (type-of cwd-object)))
  (:method ((port integer)) (make-ports (list port)))
  (:method ((s (eql nil))) (make-empty-ports))
  (:method ((p cons)) (ports-union (ports-of (car p)) (ports-of (cdr p))))
  (:method ((l list)) (merge-containers (mapcar #'ports-of l)))
  (:method ((ports ports)) ports)
  (:method ((attributed-object attributed-object))
    (ports-of (object-of attributed-object)))
  (:method ((abstract-ports abstract-ports))
    (make-ports (container-mapcar #'object-of abstract-ports)))
  (:method ((mports mports)) (make-ports (container-mapcar #'object-of mports)))
  (:method ((container abstract-container))
    (reduce #'ports-union
	    (container-mapcar #'ports-of container)
	    :initial-value (make-empty-ports))))
