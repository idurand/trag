;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defgeneric ren-injective-mapping (h1 p1 graph-ports))
(defmethod ren-injective-mapping ((h1 port-mapping) (p1 ports) (graph-ports ports))
  (let ((hupairs  '())
	(inter (ports-intersection graph-ports p1))
	(reserved-ports (make-empty-ports)))
    (loop for port in (container-contents inter) ;; surviving ports
	  do (let ((newp (port-to-new-port port h1)))
	       (push (cons port newp) hupairs)
	       (ports-nadjoin newp reserved-ports)))
    (loop
      for port in (container-contents (ports-difference graph-ports p1)) ;; appearing ports (one for ren)
      do (let ((newp (next-port reserved-ports)))
	   (push (cons port newp) hupairs)
	   (ports-nadjoin newp reserved-ports)))
    (assert (port-mapping-injective-p hupairs))
    (make-port-mapping hupairs)))

(defgeneric oplus-injective-mapping (h1 h2 p1 p2))
(defmethod oplus-injective-mapping ((h1 port-mapping) (h2 port-mapping) (p1 ports) (p2 ports))
  (let ((hpairs '())
	(reserved-ports (make-empty-ports)))
    (loop 
      for port in (container-contents p1) ;; we keep h1
      do (let ((newp (port-to-new-port port h1)))
	   (push (cons port newp) hpairs)
	   (container-nadjoin newp reserved-ports)))
    (loop ;; now p2\p1
	  for port in (container-contents (ports-difference p2 p1))
	  do (let* ((oldp (port-to-new-port port h2))
		    (newp (if (or (ports-member oldp reserved-ports)
				  (>= oldp (container-size (ports-union p1 p2))))
			      (next-port reserved-ports)
			      oldp)))
	       (container-nadjoin newp reserved-ports)
	       (push (cons port newp) hpairs)))
    (assert (port-mapping-injective-p hpairs))
    (make-port-mapping hpairs)))

(defgeneric recycle-ports (term))

(defgeneric recycle-ports-rec (term))
(defmethod recycle-ports-rec ((term term))
  (let ((root (root term)))
    (when (symbol-constant-p root)
      (let ((port (port-of root)))
	(if (zerop port)
	    (return-from recycle-ports-rec (values term (make-port-mapping '())))
	    (return-from recycle-ports-rec (values (cwd-term-num-vertex 0 (term-eti term))
						   (make-port-mapping (list (cons port 0))))))))
    (let* ((arg (arg term))
	   (a1 (car arg))
	   (p1 (graph-ports-of a1)))
      (multiple-value-bind (t1 h1) (recycle-ports-rec a1) ;; first arg
	(cond
	  ((add-symbol-p root)
	   (if (ports-subset-p (ports-of root) p1)
	       (values (build-term (apply-port-mapping h1 root) (list t1)) h1)
	       (values t1 h1)))
	  ((renaming-symbol-p root)
	   (let ((h (port-mapping-of root))
		 (graph-ports (graph-ports-of term)))
	     (if (port-mapping-empty-p h)
		 (values t1 h1)
		 (let* ((hu (ren-injective-mapping h1 p1 graph-ports))
			(hp (compose-port-mapping 
			     (inverse-port-mapping h1)
			     (compose-port-mapping h hu))))
		   (values (cwd-term-reh hp t1) hu)))))
	  ((oplus-symbol-p root)
	   (let ((a2 (if (endp (cddr arg)) (cadr arg) (build-term root (cdr arg)))))
	     (multiple-value-bind (t2 h2) (recycle-ports-rec a2)
	       (let* ((hu (oplus-injective-mapping h1 h2 p1 (graph-ports-of a2)))
		      (map (compose-port-mapping (inverse-port-mapping h2) hu)))
		 (values (cwd-term-oplus t1 (cwd-term-reh map t2)) hu))))))))))

(defmethod recycle-ports ((term term))
  (multiple-value-bind (t1 h1) (recycle-ports-rec term)
    (cwd-term-reh (inverse-port-mapping h1) t1)))


