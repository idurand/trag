;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defun interval-p (l)
  (let ((min (car l))
	(max (car (last l))))
    (= (length l) (1+ (- max min)))))

(defun ordered-list-p (l)
  (equal l (sort (copy-list l) #'<)))

(defgeneric compatible-color-funs-p (color-fun1 color-fun2))
(defmethod compatible-color-funs-p ((color-fun1 color-fun) (color-fun2 color-fun))
  (let ((colors1 (colors-of color-fun1))
	(colors2 (colors-of color-fun2)))
    (<= (car (last colors2))
	(1+ (car (last colors1))))))

(defgeneric color-fun-min-color (color-fun))
(defmethod color-fun-min-color ((color-fun color-fun))
  (car (colors-of color-fun)))
   
(defgeneric vertex-fixed-color (vertex-number fixed-color-fun))
(defmethod vertex-fixed-color ((vertex-number integer) (fixed-color-fun fixed-color-fun))
  (loop
    for fixed-color in (colors-of fixed-color-fun)
    when (container-member vertex-number (clique-width::vertices-of fixed-color))
	 do (return-from vertex-fixed-color (color-of fixed-color))))

(defgeneric vertices-fixed-color (fixed-colors))
(defmethod vertices-fixed-color ((fixed-colors fixed-colors))
  (vertices-fixed-color (container-contents fixed-colors)))

(defmethod vertices-fixed-color ((fixed-colors list))
  (loop
    with vertices-color = '()
    for fixed-color in fixed-colors
    do
       (let ((color (color-of fixed-color)))
	 (setq vertices-color
	       (nconc
		vertices-color
		(container-mapcar
		 (lambda (vertex)
		   (cons vertex color))
		 (vertices-of fixed-color)))))
    finally (return (sort vertices-color #'< :key #'car))))


