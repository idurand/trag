;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defparameter *cf1* (make-color-fun))
(defparameter *cf2* (make-color-fun))
(defparameter *cf3* (make-color-fun))

(add-color-to-port 1 0 *cf1*)
(add-color-to-port 2 0 *cf1*)
(add-color-to-port 3 1 *cf1*)

(add-color-to-port 2 0 *cf2*)
(add-color-to-port 3 1 *cf2*)
(add-color-to-port 2 1 *cf2*)

(add-color-to-port 1 0 *cf3*)
(add-color-to-port 2 0 *cf3*)
(add-color-to-port 3 1 *cf3*)

(add-color-to-port 2 0 *cf3*)
(add-color-to-port 3 1 *cf3*)
(add-color-to-port 2 1 *cf3*)


(assert (compare-object (merge-color-fun *cf1* *cf2*) *cf3*))

(add-color-to-port 4 2 *cf3*)

(assert  (compare-object
	       (add-color-to-port 1 0
				  (add-color-to-port 4 0 *cf2*))
	       (ren-color-fun 2 0 *cf3*)))

(defun big-cf (n)
  (let ((cf (make-color-fun)))
    (loop for port in (port-iota n)
       do (set-port-value (make-colors (color-iota n)) port cf))
    cf))

(defun profile-compare-object (k cf1 cf2)
  (loop
     repeat k
     do (compare-object cf1 cf2)))
	 
