;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defparameter *cf1* (make-count-fun))
(defparameter *cf2* (make-count-fun))

(incf-port-value 2 0 *cf1*)
(incf-port-value 3 2 *cf1*)

(incf-port-value 2 2 *cf2*)
(incf-port-value 3 0 *cf2*)

(assert (compare-object (merge-count-fun *cf1* *cf2*)
			(merge-count-fun *cf2* *cf1*)))

(assert (compare-object (ren-count-fun 0 2 *cf1*)
			(ren-count-fun 0 2 *cf2*)))

