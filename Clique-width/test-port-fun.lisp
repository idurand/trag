;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :clique-width)

(defparameter *f1* (make-port-int-fun))
(defparameter *f2* (make-port-int-fun))

(assert (compare-object (copy-port-fun *f1*) *f1*))
(assert (= 0 (get-port-value 0 *f1*)))

(set-port-value 3 1 *f1*)
(set-port-value 3 0 *f2*)
(set-port-value 3 1 *f2*)
(set-port-value 3 0 *f1*)

(assert (compare-object *f1* *f2*))

(incf-port-value 2 0 *f1*)
(incf-port-value 2 0 *f2*)

(assert (compare-object *f1* *f2*))

(defun big-pf (n)
  (let ((pf (make-port-int-fun)))
    (loop for port in (port-iota n)
       do (loop
	       for value from 0 below 5
	       do (set-port-value value port pf)))
    pf))

(defun profile-compare-object (k pf1 pf2)
  (loop
     repeat k
     do (compare-object pf1 pf2)))
	 
