;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Color.
(in-package :asdf-user)

(defsystem :color
  :description "color mixin"
  :name "color"
  :version "6.0"
  :author "Irene Durand <idurand@labri.fr>"
  :serial t
  :depends-on (:general)
  :components
  ((:file "package")
   (:file "color")))

(pushnew :color *features*)
