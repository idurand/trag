;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :color)

(defclass color-mixin ()
  ((color :type integer :initarg :color :reader color)))

(defun random-color (k)
  (1+ (random k)))

(defun color-iota (k)
  (iota k 1))
