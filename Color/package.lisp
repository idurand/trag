;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :color
  (:use :common-lisp :general)
  (:export
   #:color-mixin
   #:color
   #:random-color
   #:color-iota
   ))
