;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :container)
;; homogeneous containers
;; all objects must have the same type

(defgeneric container-contents (container))
(defgeneric container-unordered-contents (container))
(defgeneric container-order-fun (ordered-mixin))
(defgeneric container-copy (container))
(defgeneric container-subst (new old container))
(defgeneric container-ndifference (container1 container2))
(defgeneric container-difference (container1 container2))
(defgeneric container-find-if (fun container))
(defgeneric container-find-if-not (fun container))
(defgeneric container-count-if (fun container))
(defgeneric container-count-if-not (fun container))
(defgeneric container-car (container))
(defgeneric container-pop (container))
(defgeneric container-member (object container))
(defgeneric container-empty (container)
  (:documentation "removes all objects from container"))
(defgeneric container-size (container))
(defgeneric container-adjoin (object container))
(defgeneric container-nadjoin (object container))
(defgeneric container-map (fun container))
(defgeneric container-reduce (fun container &key key initial-value))
(defgeneric container-mapcar (fun container))
(defgeneric container-mapcan (fun container))
(defgeneric container-mapcar-to-container (fun container))
(defgeneric container-every (fun container))
(defgeneric container-delete (object container))
(defgeneric container-remove (object container))
(defgeneric container-nunion (container1 container2))
(defgeneric container-union (container1 container2))
(defgeneric container-union-gen (containers))
(defgeneric container-nunion-gen (containers))
(defgeneric container-intersection (container1 container2))
(defgeneric container-delete-if (pred container))
(defgeneric container-delete-if-not (pred container))
(defgeneric container-remove-if (pred container))
(defgeneric container-remove-if-not (pred container))
(defgeneric container-subset-p (container1 container2))
(defgeneric container-equal-p (container1 container2))
(defgeneric container-intersection-empty-p (container1 container2))
(defgeneric container-intersection-not-empty-p (container1 container2))
(defgeneric make-container-generic (objects container-class test &optional initargs))
(defgeneric container-empty-copy (container))
(defgeneric setup-container-contents (container clean))
(defgeneric container-nmerge-containers (container containers))
(defgeneric container-merge-containers (container containers))
(defgeneric merge-containers (containers))
(defgeneric container-empty-p (container))
(defgeneric singleton-container-p (container))
(defgeneric container-value-test (container))
(defgeneric container-test (container))
(defun container-p (object)
  (typep object 'abstract-container))

(defclass abstract-container (key-mixin) ()
  (:documentation "set objects"))

(defmethod print-object ((container abstract-container) stream)
  (when *print-object-readably*
    (format stream "{"))
  (display-sequence (container-contents container) stream :sep " ")
  (when *print-object-readably*
    (format stream "}")))

(defmethod container-adjoin (object (container abstract-container))
  (container-nadjoin object (container-copy container)))

(defmethod container-empty-p ((container abstract-container))
  (zerop (container-size container)))

(defmethod singleton-container-p ((container abstract-container))
  (let ((container-contents (container-contents container)))
    (and container-contents (null (cdr container-contents)))))

(defmethod container-nmerge-containers ((container abstract-container) (containers list))
  (dolist (c containers container)
    (container-nunion container c)))

(defmethod container-merge-containers ((container abstract-container) (containers list))
  (container-nmerge-containers (container-copy container) containers))

(defmethod merge-containers (containers)
  (assert containers)
  (container-merge-containers (car containers) (cdr containers)))

(defmethod container-union ((container1 abstract-container)
			    (container2 abstract-container))
  (container-nunion (container-copy container1) container2))

(defmethod container-union-gen ((containers list))
  (assert containers)
  (container-nunion-gen (cons (container-copy (car containers)) (cdr containers))))

(defmethod container-subset-p ((container1 abstract-container) (container2 abstract-container))
  (container-empty-p (container-difference container1 container2)))

(defmethod container-equal-p ((container1 abstract-container) (container2 abstract-container))
  (and (container-subset-p container1 container2) (container-subset-p container2 container1)))

(defmethod container-remove (object (container abstract-container))
  (container-delete object (container-copy container)))

(defmethod container-pop ((container abstract-container))
  (assert (not (container-empty-p container)))
  (let ((object (container-car container)))
    (container-delete object container)
    object))

(defmethod container-value-test ((abstract-container abstract-container))
  #'compare-object)

(defmethod compare-object ((container1 abstract-container) (container2 abstract-container))
  (let ((value-test (container-value-test container1)))
    (if (null value-test)
	(and (= (container-size container1)
		(container-size container2))
	     (container-equal-p container1 container2))
	(let ((table1 (container-table container1))
	      (table2 (container-table container2)))
	  (if (eq value-test #'equalp)
	      (equalp table1 table2)
	      (hash-table-equal-p table1 table2 value-test))))))
