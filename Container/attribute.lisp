;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :container)

(defgeneric combine-fun (container))

(defclass combine-mixin ()
  ((combine-fun :initarg :combine-fun :reader combine-fun)))

(defclass attributed-container (combine-mixin container) ())

(defmethod print-object :after ((container combine-mixin) stream)
  (format stream "A"))
;;  (format stream ":~A" (combine-fun container)))

(defgeneric attributed-container-set (container))
(defmethod attributed-container-set ((container combine-mixin))
  (container-mapcar #'object-of container))

(defmethod container-member ((ao attributed-object) (container combine-mixin))
;;  (format *error-output* "container-member attributed-object combine-mixin ~A ~A~%" ao container)
  (container-member (object-of ao) container))

(defmethod container-nadjoin ((ao attributed-object) (container combine-mixin))
;;  (format *error-output* "container-nadjoin attributed-object combine-mixin~%")
  (let ((found (container-member (object-of ao) container)))
    (if found
	(attributed-object-combine found (attribute-of ao) (combine-fun container))
	(call-next-method))
  container))

(defgeneric container-attribute (container))
(defmethod container-attribute ((container combine-mixin))
  (let* ((combine-fun (combine-fun container))
	 (attribute (funcall combine-fun)))
    (maphash (lambda (k v)
	       (declare (ignore k))
	       (setf attribute (funcall combine-fun attribute (attribute-of v))))
	     (container-table container))
    attribute))

(defmethod container-empty-copy ((container combine-mixin))
  (make-empty-container-generic
   (type-of container)
   (hash-table-test (container-table container))
   (list :combine-fun (combine-fun container))))
