;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Container.
(in-package :asdf-user)

(defsystem :container
  :description "container"
  :name "container"
  :version "6.0"
  :author "Irene Durand <idurand@labri.fr>"
  :serial t
  :depends-on (:object)
  :components
   (
    (:file "package")
    (:file "abstract-container")
    (:file "hash-container")
    (:file "ordered")
    (:file "key")
    (:file "attribute")
    (:file "multi")
    ))

(pushnew :container *features*)
