;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :container)

;; todo improve the comparison of containers
;; when values are equalp comparable
;; use equalp for comparing the hash=tables!!

(defgeneric container-table (container))

(defclass container (abstract-container)
  ((container-table :type hash-table :reader container-table))
  (:documentation "container with hash-table"))

(defmethod container-unordered-contents ((container container))
  (list-values (container-table container)))

(defmethod container-contents ((container container))
  (container-unordered-contents container))

(defmethod container-test ((container container))
  (hash-table-test (container-table  container)))

(defmethod container-car ((container container))
  (let ((table (container-table container)))
    (if (zerop (hash-table-count table))
	(values nil nil)
	(maphash 
	 (lambda (k v)
	   (declare (ignore k))
	   (return-from container-car (values v t)))
	 table))))

(defmethod container-member (object (container container))
;;  (format *error-output* "container-member t container ~A ~A~%" (type-of object) (type-of container))
  (gethash object (container-table container)))

(defun make-empty-container-generic 
    (container-type test &optional initargs)
  (let ((container (if initargs
		       (apply #'make-instance container-type initargs)
		       (make-instance container-type))))
    (setf (slot-value container 'container-table )
	  (make-hash-table :test test))
    container))
 
(defmethod container-nadjoin (object (container container))
;;  (format *error-output* "container-nadjoin object container~%")
  (setf (gethash object (container-table container) object) object)
  container)

(defgeneric container-nadds (objects container-container))
(defmethod container-nadds ((objects list) (container abstract-container))
  (dolist (object objects container)
    (container-nadjoin object container)))

(defmethod make-container-generic ((objects list) container-type test &optional initargs)
  (let ((container (make-empty-container-generic container-type test initargs)))
    (container-nadds objects container)))

(defmethod container-delete (object (container container))
  (remhash object (container-table container))
  container)

(defmethod container-size ((container container))
  (hash-table-count (container-table container)))

(defmethod container-copy ((container container))
  (let* ((oldtable (container-table container))
	 (new (container-empty-copy container))
	 (table (container-table new)))
    (maphash
     (lambda (k v)
       (setf (gethash k table) (object-copy v)))
     oldtable)
    new))

(defmethod container-empty-copy ((container container))
  (make-empty-container-generic
   (type-of container)
   (container-test container)))

(defmethod container-ndifference ((container1 container) (container2 container))
  (maphash
   (lambda (k object2)
     (declare (ignore k))
     (container-delete object2 container1))
   (container-table container2))
  container1)

(defmethod container-difference ((container1 container) (container2 container))
  (container-ndifference (container-copy container1) container2))

(defmethod container-subset-p ((container1 container) (container2 container))
  (maphash
   (lambda (k v)
     (declare (ignore k))
     (unless (container-member v container2)
       (return-from container-subset-p nil)))
   (container-table container1))
  t)
   
(defmethod container-intersection ((container1 container) (container2 container))
  (let* ((table2 (container-table container2))
	 (c (container-empty-copy container1))
	 (table (container-table c)))
    (maphash
     (lambda (k1 object1)
       (when (gethash k1 table2)
	 (setf (gethash k1 table) object1)))
     (container-table container1))
    c))

(defmethod container-intersection-empty-p
    ((container1 container) (container2 container))
  (let ((table2 (container-table container2)))
    (maphash
     (lambda (k1 object1)
       (declare (ignore object1))
       (when (gethash k1 table2)
	 (return-from container-intersection-empty-p)))
     (container-table container1))
    t))

(defmethod container-intersection-not-empty-p
    ((container1 container) (container2 container))
  (let ((table2 (container-table container2)))
    (maphash
     (lambda (k1 object1)
       (declare (ignore object1))
       (when (gethash k1 table2)
	 (return-from container-intersection-not-empty-p t)))
     (container-table container1))
    nil))

(defmethod container-subst (new old (container container))
  (let ((table (container-table container))
	(c (container-empty-copy container)))
    (maphash
     (lambda (k v)
       (declare (ignore k))
       (container-nadjoin (object-subst new old v) c))
     table)
    c))

(defmethod container-nunion ((container1 container)
			     (container2 container))
  (maphash
   (lambda (k object2)
     (declare (ignore k))
     (container-nadjoin object2 container1))
   (container-table container2))
  container1)

(defmethod container-nunion-gen ((containers list))
  (loop
    with container1 = (pop containers)
    for container2 in containers
    do (container-nunion container1 container2)
       finally (return container1)))

(defmethod container-remove-if-not
    (predicate (container container))
  (let* ((table (container-table container))
	 (new (container-empty-copy container))
	 (ht (container-table new)))
    (maphash (lambda (k v)
;;	       (when (object-funcall predicate v)
	       (when (funcall predicate v)
		 (setf (gethash k ht) v)))
	     table)
    new))

(defmethod container-remove-if (predicate (container container))
  (let* ((table (container-table container))
	 (new (container-empty-copy container))
	 (ht (container-table new)))
    (maphash (lambda (k v)
;;	       (unless (object-funcall predicate v)
	       (unless (funcall predicate v)
		 (setf (gethash k ht) v)))
	     table)
    new))

(defmethod container-delete-if-not (predicate (container container))
  (let ((ht (container-table container)))
    (maphash
     (lambda (k v)
       (unless (funcall predicate v)
	 (remhash k ht)))
     (container-table container))
    container))

(defmethod container-delete-if
    (predicate (container container))
  (let ((ht (container-table container)))
    (maphash (lambda (k v)
	       (when (funcall predicate v)
		 (remhash k ht)))
	     (container-table container))
    container))

(defmethod container-find-if (fun (container container))
  (maphash
   (lambda (k v)
     (declare (ignore k))
     (when (funcall fun v)
       (return-from container-find-if v)))
   (container-table container)))

(defmethod container-count-if (fun (container container))
  (let ((count 0))
    (maphash
     (lambda (k v)
       (declare (ignore k))
       (when (funcall fun v)
	 (incf count)))
     (container-table container))
    count))

(defmethod container-count-if (fun (container container))
  (let ((count 0))
    (maphash
     (lambda (k v)
       (declare (ignore k))
       (unless (funcall fun v)
	 (incf count)))
     (container-table container))
    count))

(defmethod container-find-if-not (fun (container container))
  (maphash
   (lambda (k v)
     (declare (ignore k))
     (unless (funcall fun v)
       (return-from container-find-if-not v)))
   (container-table container)))

(defmethod container-map (fun (container container))
  (maphash (lambda (k v)
	     (declare (ignore k))
	     (funcall fun v))
	   (container-table container)))

(defmethod container-reduce (fun (container container) &key (key #'identity) initial-value)
  (let ((vv initial-value))
    (container-map 
     (lambda (object)
       (setq vv (funcall fun vv (funcall key object))))
     container)
    vv))

(defmethod container-mapcar-to-container (fun (container container))
  (let ((oldtable (container-table container))
	(new-container (container-empty-copy container)))
    (maphash
     (lambda (k v)
       (declare (ignore k))
       (container-nadjoin (funcall fun (object-copy v)) new-container))
     oldtable)
    new-container))

(defmethod container-mapcar (fun (container container))
  (let ((l '()))
    (maphash (lambda (k v)
	       (declare (ignore k))
	       (push (funcall fun v) l))
	     (container-table container))
    l))

(defmethod container-mapcan (fun (container container))
  (let ((l '()))
    (maphash (lambda (k v)
	       (declare (ignore k))
	       (setq l (nconc (funcall fun v) l)))
	     (container-table container))
    l))

(defmethod container-every (pred (container container))
  (maphash (lambda (k v)
	     (declare (ignore k))
	     (unless (funcall pred v)
	       (return-from container-every)))
	   (container-table container))
  t)

(defmethod container-empty ((container abstract-container))
  (clrhash (container-table container)))
