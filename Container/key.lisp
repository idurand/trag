;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :container)

(defmethod container-member ((key-mixin key-mixin) (container container))
;;  (format *error-output* "container-member key-mixin ~A ~A~%" (type-of key-mixin)  (type-of container))
  (gethash (object-key key-mixin)
	   (container-table container)))

(defmethod container-nadjoin ((key-mixin key-mixin) (container container))
;;  (format *error-output* "container-nadjoin key-mixin ~A ~A~%" (type-of key-mixin)  (type-of container))
  (setf (gethash
	 (object-key key-mixin)
	 (container-table container))
	key-mixin)
  container)

(defgeneric container-key-member (key container))
(defmethod container-key-member (key (container container))
  (gethash key (container-table container)))
(defmethod container-delete ((key-mixin key-mixin) (container container))
  (remhash (object-key key-mixin) (container-table container))
  container)
