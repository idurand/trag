;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :container)

(defclass multi-mixin () ())
(defclass multi-container (multi-mixin attributed-container) ())

(defclass multi-max-mixin (multi-mixin) ())

(defgeneric multi-max (multi-max-mixin))

(defclass multi-max-container (multi-max-mixin attributed-container) ())
(defclass ordered-multi-max-container (multi-max-mixin ordered-mixin attributed-container) ())

(defun make-empty-multi-container-generic (type test combine-fun)
  (make-empty-container-generic type test (list :combine-fun combine-fun)))

(defun make-multi-container-generic (elements type test combine-fun)
  (make-container-generic elements type test (list :combine-fun combine-fun)))

(defun make-empty-multi-max-container-generic (type test max)
  (make-empty-container-generic type test (list :combine-fun 
						(lambda (a1 a2)
						  (min max (+ a1 a2))))))

(defun make-multi-max-container-generic (elements type test max)
  (make-container-generic elements type test
			  (list 
			   :combine-fun
			   (lambda (a1 a2)
			     (min max (+ a1 a2))))))

(defgeneric container-cardinality (multi-mixin))
(defmethod container-cardinality ((multi-mixin multi-mixin))
  (let ((count 0))
    (container-map (lambda (em)
		     (incf count (attribute-of em)))
		   multi-mixin)
    count))

(defclass ordered-multi-container (multi-mixin ordered-mixin attributed-container) ())


(defgeneric multi-container-list (container))
(defmethod multi-container-list ((container multi-mixin))
  (mapcan
   (lambda (em)
     (make-list (attribute-of em) :initial-element (object-of em)))
   (container-contents container)))

(defgeneric multiplicity (object multi-mixin))
(defmethod multiplicity (object (container multi-mixin))
  (let ((found (container-member object container)))
    (if found
	(attribute-of found)
	0)))

(defmethod container-member ((ao attributed-object) (container multi-mixin))
;;  (format t "container-member attributed-object multi-mixin")
  (let ((found (call-next-method)))
    (and found (<= (attribute-of ao) (attribute-of found)) found)))

;; when applied to a multi-element does multi-delete
(defmethod container-delete ((ao attributed-object) (container multi-mixin))
  (let ((found (container-member ao container)))
    (unless found
      (return-from container-delete container))
    (let ((multiplicity (- (attribute-of found) (attribute-of ao))))
      (if (plusp multiplicity)
	  (setf (attribute-of found) multiplicity)
	  (setq container (container-delete (object-of found) container))))
    container))
    
;; when applied to a simple object does normal delete
(defmethod container-delete (object (container multi-mixin))
  (call-next-method))

(defmethod container-nadjoin (object (container multi-mixin))
  (container-nadjoin (make-object-multi object) container))
