;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :container)

(defclass ordered-mixin () ())

(defclass ordered-container (ordered-mixin container) ()
  (:documentation "ordered container"))

(defmethod container-order-fun ((container ordered-mixin)) #'<)

(defmethod container-contents ((container ordered-mixin))
  (sort (call-next-method) (container-order-fun container) :key #'object-key))

(defmethod strictly-ordered-p ((container1 ordered-container) (container2 ordered-container))
  (list< (container-contents container1) 
	 (container-contents container2)
	 :test (container-order-fun container1)))
