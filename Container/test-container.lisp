;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :container)

(defun test-container (n container-type test)
  (let ((c1 (make-container-generic (iota n) container-type test))
	(c2 (make-container-generic (nreverse (iota n)) container-type test)))
    (container-union c1 c2)))

;; 2013-09-23
;; (with-time (defparameter *c* (test-container 10000 'container #'eql)))
;; in 0.018s

(defparameter *c* (make-multi-container-generic 
		   '(1 2 3 1 2 3) 'multi-container #'eql
		    #'+))

(defun test-multi ()
  (let ((c (make-multi-container-generic 
		   '(1 2 3 1 2 3) 'multi-container #'eql
		   #'+))
	(o3 (make-object-multi 3 1)))
    
    (assert (container-equal-p c c))
    (assert (container-equal-p (container-adjoin o3 (container-remove o3 c))
			       (container-remove o3
						 (container-adjoin o3 c))))))
