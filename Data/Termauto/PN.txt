Ops O:1 X:1 m:0

Automaton R1
States q qO qX qf
Final States qf qO
Transitions
m -> q
O(q) -> qO
X(q) -> qX
O(qO) -> qf
X(qX) -> qf
O(qf) -> qf
X(qf) -> qf

Automaton R2
States q qO qOO qX qXO qXOX qXX qXXO qf
Final States qf qOO qXX
Transitions
m -> q

O(q) -> q
X(q) -> q

O(q) -> qO
X(q) -> qX

O(qO) -> qOO
O(qX) -> qXO
X(qX) -> qXX

O(qOO) -> qf
X(qXO) -> qXOX
O(qXX) -> qXXO
X(qXX) -> qf

X(qXOX) -> qf
X(qXXO) -> qf

O(qf) -> qf
X(qf) -> qf

;; (defparameter *a* (automaton (read-autowrite-spec-from-path "/usr/lab (concatenate 'string (initial-data-directory) "Termauto/PN.txt")))))ri/idurand/trag/Data/Termauto/PN.txt")))
