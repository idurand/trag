;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defvar *place-node-nums*)
(setq *place-node-nums* '())

(defgeneric place-p (num)
  (:method ((num integer))
    (member num *place-node-nums* :test #'=)))

(defgeneric bipartite-term-to-term (term place-node-nums)
   (:method ((term term) (place-node-nums list))
      (term-add-vbits-by-eti-lists 
        term
        (list place-node-nums)
        :inverted (list t))))

(defun place-node-nums (bipartition one-transition)
  (nodes-num
   (if one-transition (second bipartition) (first bipartition))))

(defgeneric cwd-decomposition-bipartition (graph bipartition
					   &key one-transition optimal)
  (:method ((g graph) (bipartition list) &key one-transition optimal)
    (let ((*place-node-nums* (place-node-nums bipartition one-transition))
	  (*oriented* (oriented-p g)))
      (bipartite-term-to-term (cwd-decomposition g optimal) *place-node-nums*))))

(defgeneric cwd-decomposition-bipartite (graph &key one-transition optimal)
  (:method (graph &key one-transition (optimal nil))
    (if (graph-empty-p graph)
	(cwd-term-empty-graph 1)
	(let ((bipartition (graph-bipartite-p graph)))
	  (assert bipartition)
	  (cwd-decomposition-bipartition
	   graph bipartition :one-transition one-transition :optimal optimal)))))

(defgeneric cwd-optimal-decomposition-bipartite (graph &key one-transition)
  (:method ((graph graph) &key one-transition)
    (cwd-decomposition-bipartite graph :one-transition one-transition :optimal t)))

(defgeneric cwd-decomposition-incidence (graph &key optimal)
  (:documentation "decomposition of the incidence graph of GRAPH")
  (:method ((graph graph) &key (optimal nil))
    (cwd-decomposition-bipartite
     (incidence-graph-of graph) :optimal optimal)))

(defgeneric cwd-optimal-decomposition-incidence (graph)
  (:documentation "optiomal decomposition of the incidence graph of GRAPH")
  (:method ((graph graph))
    (cwd-decomposition-incidence graph :optimal t)))
