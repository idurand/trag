;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defgeneric constraint-node (constraint))
(defgeneric constraint-port (constraint))

(defclass constraint ()
  ((node :initarg :node :reader constraint-node)
   (port :initarg :port :reader constraint-port)))

(defmethod print-object ((constraint constraint) stream)
  (format stream "~A:~A"
	  (constraint-node constraint)
	  (constraint-port constraint)))

(defgeneric make-constraint (node port)
  (:method ((node node) (port integer))
    (make-instance 'constraint :node node :port port)))

(defclass constraints ()
  ((constraints-table :initform (make-hash-table) :reader constraints-table)))

(defgeneric constraints-constraints (constraints)
  (:method ((constraints constraints))
    (list-values (constraints-table constraints))))

(defmethod print-object ((constraints constraints) stream)
  (format stream "~A"
	  (constraints-constraints constraints)))

(defun make-constraints () (make-instance 'constraints))

(defgeneric add-constraint (constraint constraints)
  (:method ((constraint constraint) (constraints constraints))
    (setf (gethash (num (constraint-node constraint)) (constraints-table constraints))
	  constraint)
    constraints))

(defgeneric node-constraint (node constraints)
  (:method ((node node) (constraints constraints))
    (gethash (num node) (constraints-table constraints))))

(defgeneric node-port (node constraints)
  (:method ((node node) (constraints constraints))
    (let ((node-constraint (node-constraint node constraints)))
      (and
       node-constraint
       (constraint-port node-constraint)))))

(defgeneric add-node-port-constraint (node port constraints)
  (:method ((node node) (port integer) (constraints constraints))
    (add-constraint (make-constraint node port) constraints)))

(defgeneric make-one-constraint-constraints (node port)
  (:method ((node node) (port integer))
    (add-node-port-constraint node port (make-constraints))))

(defgeneric filter-constraints (nodes constraints)
  (:method ((nodes nodes) (constraints constraints))
    (let ((new-constraints (make-constraints)))
      (maphash
       (lambda (num-node constraint)
	 (declare (ignore num-node))
	 (let ((node (constraint-node constraint)))
	   (when (nodes-member node nodes)
	     (add-constraint constraint new-constraints))))
       (constraints-table constraints))
      new-constraints)))

(defgeneric filter-constraints-not (nodes constraints)
  (:method ((nodes nodes) (constraints constraints))
    (let ((new-constraints (make-constraints)))
      (maphash
       (lambda (num-node constraint)
	 (declare (ignore num-node))
	 (let ((node (constraint-node constraint)))
	   (unless (nodes-member node nodes)
	     (add-constraint constraint new-constraints))))
       (constraints-table constraints))
      new-constraints)))

(defgeneric filtered-constraints-ports (nodes constraints)
  (:method ((nodes nodes) (constraints constraints))
    (let ((ports (make-ports '())))
      (maphash
       (lambda (num-node constraint)
	 (declare (ignore num-node))
	 (when (nodes-member (constraint-node constraint) nodes)
	   (ports-nadjoin (constraint-port constraint) ports)))
       (constraints-table constraints))
      ports)))

(defgeneric constraints-ports-except-node (constraints node)
  (:method ((constraints constraints) (node node))
    (let ((ports (make-ports '())))
      (maphash
       (lambda (num-node constraint)
	 (declare (ignore num-node))
	 (unless (eq node (constraint-node constraint))
	   (ports-nadjoin (constraint-port constraint)  ports)))
       (constraints-table constraints))
      ports)))

(defgeneric delete-constraint (node constraints)
  (:method ((node node) (constraints constraints))
    (remhash (num node) (constraints-table constraints))))

(defgeneric remove-constraint (node constraints)
  (:method ((num-node integer) (constraints constraints))
    (let ((new-constraints (make-constraints)))
      (maphash
       (lambda (nn constraint)
	 (unless (= nn num-node)
	   (add-node-port-constraint (constraint-node constraint)
				     (constraint-port constraint) new-constraints)))
       (constraints-table constraints))
      new-constraints))
  (:method  ((node node) (constraints constraints))
    (remove-constraint (num node) constraints)))

(defgeneric constraints-nodes (constraints)
  (:method ((constraints constraints))
    (make-nodes (mapcar #'constraint-node (constraints-constraints constraints)))))

(defgeneric constraints-ports (constraints)
  (:method ((constraints constraints))
    (make-ports (mapcar #'constraint-port
			(constraints-constraints constraints)))))

(defgeneric merge-independant-constraints (constraints1 constraints2)
  (:method ((constraints1 constraints) (constraints2 constraints))
    (let ((constraints (make-constraints)))
      (maphash
       (lambda (num-node constraint)
	 (declare (ignore num-node))
	 (add-constraint constraint constraints))
       (constraints-table constraints1))
      (maphash
       (lambda (num-node constraint)
	 (declare (ignore num-node))
	 (add-constraint constraint constraints))
       (constraints-table constraints2))
      constraints)))

(defgeneric ports-renaming (term constraints num-ports)
  (:method ((term term) (constraints constraints) (num-ports constraints))
    (let ((pairs '()))
      (maphash
       (lambda (num-node constraint)
	 (declare (ignore num-node))
	 (let* ((n (constraint-node constraint))
		(p (constraint-port constraint))
		(port (node-port n num-ports)))
	   (when (and p (not (eql p port)))
	     (push (cons port p) pairs)
	     (add-node-port-constraint n p num-ports))))
       (constraints-table constraints))
      (loop
	while pairs
	do (let ((pair (find-if-not
			(lambda (pair)
			  (member (cdr pair)
				  (mapcar #'car pairs)))
			pairs)))
	     (setf pairs (delete pair pairs :test #'equal))
	     (setf term (cwd-term-ren (car pair) (cdr pair) term)))
	finally (return (values term num-ports))))))

(defgeneric make-initial-constraints (nodes)
  (:method  ((nodes nodes))
    (let ((constraints (make-constraints)))
      (nodes-map
       (lambda (node)
	 (setf constraints (add-node-port-constraint node 0 constraints)))
       nodes)
      constraints)))

(defgeneric term-satisfies-constraints (term constraints)
  (:method ((term term) (constraints constraints))
    (maphash
     (lambda (k v)
       (declare (ignore k))
       (unless (= (constraint-port v) (node-final-port (num (constraint-node v)) term))
       	 (return-from term-satisfies-constraints)))
     (constraints-table constraints))
    t))
