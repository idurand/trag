;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defun equal-arcs (edge1 edge2)
  (let ((ori1 (first edge1))
	(ext1 (second edge1))
	(ori2 (first edge2))
	(ext2 (second edge2)))
    (and (term-eti= ori1 ori2) (term-eti= ext1 ext2))))

(defun equal-edges (edge1 edge2 oriented)
  (if oriented
      (equal-arcs edge1 edge2)
      (or (equal-arcs edge1 edge2)
	  (equal-arcs edge1 (reverse edge2)))))

(defun vertices-from-edges (edges)
  (remove-duplicates (flatten edges) :test #'term-eti=))

(defun eq-port-v (port v) (= port (port-of (root v))))

(defun connect-ports (ports edges vertices oriented)
  (let* ((port1 (first ports))
	 (port2 (second ports))
	 (all-vertices
	   (union (vertices-from-edges edges)
		  vertices :test #'term-eti=))
	 (vertices1 (remove-if-not
		     (lambda (v) (eq-port-v port1 v))
		     all-vertices))
	 (vertices2 (remove-if-not
		     (lambda (v) (eq-port-v port2 v))
		     all-vertices)))
    (if (and vertices1 vertices2)
	(let ((new-edges
		(cartesian-product (list vertices1 vertices2)))
	      (remaining-vertices
		(remove-if
		 (lambda (v)
		   (or (eq-port-v port1 v) (eq-port-v port2 v)))
		 vertices)))
	  (list (union edges new-edges :test (lambda (e1 e2) (equal-edges e1 e2 oriented)))
		remaining-vertices))
	(list
	 edges
	 vertices))))

(defgeneric edges-and-isolated-vertices (term)
  (:documentation
   "list of edges and isolated vertices of the graph corresponding to this term \
    if leaves are not numbered we number them \
    the edges are couples of constant terms, the vertices constant terms")
  (:method ((term term))
    (let ((oriented (oriented-p term))
	  (root (root term)))
      (when (identity-symbol-p root) (setq term (car (arg term))))
      (setq term (term-num-leaves term))
      (if (term-constant-p term)
	  (list () (list term))
	  (let ((arg (arg term)))
	    (if (oplus-symbol-p root)
		(let ((eis (mapcar (lambda (subterm) (edges-and-isolated-vertices subterm)) arg)))
		  (list (reduce #'append (mapcar #'first eis))
			(reduce #'append (mapcar #'second eis))))
		(let ((l1 (edges-and-isolated-vertices (first arg)))
		      (ports (symbol-ports root)))
		  (cond
		    ((add-symbol-p root)
		     (connect-ports ports (first l1) (second l1) oriented))
		    ((ren-symbol-p root)
		     (apply-port-mapping (port-mapping-of root) l1))
		    ((reh-symbol-p root)
		     (apply-port-mapping (port-mapping-of root)  l1))
		    (t (error "~A not a graph symbol~%" root))))))))))

(defgeneric edges-and-isolated-vertices-numbers (term)
  (:documentation "edges and isolated vertices given with their numbers")
  (:method ((term term))
    (let ((edges-and-isolated-vertices (edges-and-isolated-vertices term)))
      (list
       (mapcar (lambda (edge)
		 (mapcar #'term-eti edge))
	       (car edges-and-isolated-vertices))
       (mapcar #'term-eti (second edges-and-isolated-vertices))))))

(defgeneric term-to-enode (term)
  (:method ((term term))
    (list (term-eti term)
	  (let ((terms::*show-term-eti* nil))
	    (format nil "~A" term)))))

(defgeneric edges-and-isolated-vertices-number-label (term)
  (:documentation "")
  (:method ((term term))
    (let ((edges-and-isolated-vertices (edges-and-isolated-vertices term)))
      (list
       (mapcar
	(lambda (edge)
	  (mapcar #'term-to-enode edge))
	(first edges-and-isolated-vertices))
       (mapcar #'term-to-enode (second edges-and-isolated-vertices))))))

(defgeneric graph-from-cwd-term (term)
  (:documentation
   "graph corresponding to the cwd-term (with no incidence ports)")
  (:method ((term term))
    (let ((ev (edges-and-isolated-vertices-number-label term)))
      (graph-from-earcs-and-enodes
       (first ev) (second ev) :oriented (oriented-p term)))))

(defgeneric igraph-from-cwd-term (cwd-term)
  (:documentation "incidence graph corresponding to CWD-TERM")
  (:method ((cwd-term term))
    (incidence-graph-of (graph-from-cwd-term cwd-term))))

(defgeneric graph-from-cwd-iterm (cwd-iterm)
  (:documentation "graph corresponding to cwd-iterm")
  (:method ((cwd-iterm term))
    (incidence-graph-to-graph (graph-from-cwd-term cwd-iterm))))

(defgeneric cwd-iterm-to-graph (cwd-iterm)
  (:documentation "graph corresponding to cwd-iterm")
  (:method ((cwd-iterm term))
    (graph-from-cwd-iterm cwd-iterm)))

(defgeneric cwd-term-to-graph (cwd-term)
  (:documentation "graph corresponding to CWD-TERM")
  (:method ((cwd-term term))
    (graph-from-cwd-term cwd-term)))

(defun node-color (c)
  (case c
    (0 "blue")
    (1 "red")
    (2 "green")
    (3 "yellow")
    (t "black")))

(defmethod color ((s abstract-symbol)) -1)

(defgeneric cwd-term-to-dot (term &optional stream)
  (:method ((term term) &optional (stream t))
    (let* ((*show-term-eti* t)
	   (oriented (oriented-p term)))
      (setq term (term-num-leaves term))
      (let ((l (edges-and-isolated-vertices term)))
	(format stream "~:[~;di~]graph ~S {~%" oriented (name term))
	(loop for edge in (first l)
	      do (let ((ori (first edge))
		       (ext (second edge)))
		   (format stream "~S -~:[-~;>~] ~S;~%"
			   (format nil "~A" ori)
			   oriented
			   (format nil "~A" ext)
			   )))
	(loop for v in (append (second l) (vertices-from-edges (first l)))
	      do (format
		  stream
		  "~S[color=~A];~%"
		  (format nil "~A" v)
		  (node-color (color (root v)))))
	(format stream "}~%")))))

(defgeneric cwd-term-to-dot-file (term name)
  (:method ((term term) (name string))
    (with-open-file (stream (format nil "/tmp/~A.dot" name)
			    :direction :output :if-exists :supersede :if-does-not-exist :create)
      (cwd-term-to-dot term stream))))

(defgeneric cwd-term-to-dot-string (term)
  (:method ((term term))
    (with-output-to-string (stream) (cwd-term-to-dot term stream))))

(defgeneric cwd-term-to-svg-string (term)
  (:method ((term term))
    (graph:dot-to-svg-string (cwd-term-to-dot-string term))))

(defun cwd-term-graph-show (term)
  (graph-show (cwd-term-to-graph term)))
  ;; (cwd-term-to-dot-file term name)
  ;; (dot-to-pdf name)
  ;; (graph::tmp-graph-display name))

(defgeneric labeled-nodes (cwd-term)
  (:method ((cwd-term term))
    (setq cwd-term (term-num-leaves cwd-term))
    (let ((eiv (edges-and-isolated-vertices cwd-term)))
      (append (vertices-from-edges (car eiv)) (second eiv)))))

(defgeneric number-of-nodes-and-edges (cwd-term)
  (:documentation
   "list containing the number of nodes and edges of the corresponding graph")
  (:method ((cwd-term term))
    (setq cwd-term (term-num-leaves cwd-term))
    (let* ((eiv (edges-and-isolated-vertices cwd-term))
	   (edges (first eiv))
	   (nb-nodes (length (leaves-of-term cwd-term)))
	   (nb-nodes2  (+
			(length (labeled-nodes cwd-term))
			(length (second eiv)))))
      (assert (= nb-nodes nb-nodes2))
      (values nb-nodes (length edges)))))

(defgeneric eti-list (cwd-term)
  (:method ((cwd-term term))
    (remove-duplicates
     (mapcar #'car (car (edges-and-isolated-vertices-numbers cwd-term)))
     :test #'=)))

(defgeneric node-final-port (node-number cwd-term)
  (:method ((node-number integer) (cwd-term term))
    (port-of (root (car (member node-number (labeled-nodes cwd-term)
				:key #'term-eti :test #'=))))))

(defgeneric cwd-term-equivalent-to-graph-p (cwd-term graph)
  (:documentation "is the graph corresponding to CWD-TERM the same as GRAPH")
  (:method ((cwd-term term) (graph graph))
    (assert (term-leaves-eti-p cwd-term))
    (graph-equivalent-p
     (cwd-term-to-graph cwd-term)
     graph)))

(defgeneric cwd-term-equivalent-p (cwd-term1 cwd-term2)
  (:documentation "cwd-term1 and cwd-term2 represent the same graph")
  (:method ((cwd-term1 term) (cwd-term2 term))
    (assert (term-leaves-eti-p cwd-term1))
    (assert (term-leaves-eti-p cwd-term2))
    (graph-equivalent-p (cwd-term-to-graph cwd-term1) (cwd-term-to-graph cwd-term2))))
 
