(in-package :decomp)

(defvar *answer*)
(defun print-answers (tmp first)
  (loop
    repeat (nb-levels)
    do (loop
	 repeat (nb-groups)
	 do (format t "~d" (aref *answer* (incf tmp))))
      do (format t "~%"))
  (when first (format t "~%"))
  tmp)

(defun print-all-answers ()
  (let ((tmp (print-answers 0 t)))
    (print-answers tmp nil)))

(defun component-from-nodes (nodes level)
  (loop
    with classes = (list (list (pop nodes)))
    while nodes
    do (let* ((v (pop nodes))
	     (class
	       (find-if
		(lambda (class)
		  (find-if (lambda (u)
			     (aref *answer* (var-code (group-var u v level))))
			   class))
		classes)))
	 (if class 
	     (nconc class (list v))
	     (push (list v) classes)))
       finally (return (make-component (mapcar #'make-nodes-class (nreverse classes))))))
  
(defun level-components (level)
  (loop
    with nodes = (iota *sat-nb-nodes* 1)
    while nodes
    for u = (pop nodes) then (pop nodes)
    collect
    (loop
      with u-nodes = (list u)
      with no-u = '()
      while nodes
      for v = (pop nodes) then (pop nodes)
      if (aref *answer* (var-code (component-var u v level)))
	do (push v u-nodes)
      else
	do (push v no-u)
      finally (progn
		(setq nodes (nreverse no-u))
		(return (component-from-nodes (nreverse u-nodes) level))))))

(defun extract-derivation (literals)
  (setq *answer* (literals-vector literals))
  (make-derivation
   (nreverse
   (loop
     for level from 0 to (nb-levels)
     collect (make-deriv-step (level-components level))))))
  
(defun decode-graph-literals (graph literals)
  (setq *sat-nb-nodes* (graph-nb-nodes graph))
  (extract-derivation literals))

(defun decode-to-derivation (graph solution-file)
  (let ((literals (load-literals solution-file)))
    (let ((*sat-debug* t))
      (save-literals literals (extend-name solution-file "debug")))
    (decode-graph-literals graph literals)))

(defun decode (graph solution-file decomp-file)
  (let ((derivation (decode-to-derivation graph solution-file)))
    (with-open-file  (stream decomp-file
			     :direction :output :if-exists :supersede)
     (output-derivation derivation stream))))
