;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defun verify-cwd (g cwd &optional (test #'=))
  (let ((term (cwd-decomposition g))
	(nb-nodes (graph-nb-nodes g))
	(nb-edges (graph-nb-edges g)))
    (assert (funcall test (clique-width term) cwd))
    (let ((gg (graph-from-cwd-term term)))
      (assert (funcall test nb-nodes (graph-nb-nodes gg)))
      (assert (funcall test nb-edges (graph-nb-edges gg))))))
  
(defparameter *petersen* (graph-petersen))
(defparameter *mcgee* (graph-mcgee))
(defun decomp-min-test ()
;; 2013-07-20 in 36.637sec
;; 2013-07-24 in 34.111sec
;; 2013-07-30 in 11.18sec
;; 2013-08-16 in 8.342sec
;; 2015-11-12 in 0.427sec shenzhou 
  (verify-cwd (graph-square-grid 5) 7)
  (verify-cwd (graph-rgrid 5 8) 7)
  (verify-cwd *petersen* 6)
  (verify-cwd *mcgee* 11)
  (verify-cwd (graph-pn 30) 3)
  (verify-cwd (graph-kn 20) 2)
  (verify-cwd (graph-cycle 100) 4)
  (verify-cwd (graph-binary-tree 32) 3))

(defun decomp-any-test ()
;; 2013-07-27 in 33.405sec
;; 2013-07-30 in 10.509sec
;; 2013-10-03 in 7.682sec
;; 2015-11-12 in 0.443sec shenzhou 
  (let ((*min-node* nil))
    (verify-cwd (graph-square-grid 5) 7)
    (verify-cwd (graph-rgrid 5 8) 7)
    (verify-cwd *petersen* 6)
    (verify-cwd *mcgee* 11)
    (verify-cwd (graph-pn 30) 3)
    (verify-cwd (graph-kn 20) 2)
    (verify-cwd (graph-cycle 100) 4)
    (verify-cwd (graph-binary-tree 32) 3)))
