;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Decomp.
(in-package :asdf-user)

(defsystem :decomp
  :description "Decomp: library of graph decompositions"
  :name "decomp"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:graph :clique-width :sat-gen)
  :serial t
  :components ((:file "package")
	       (:file "constraint")
	       (:file "unoriented-decomp") 
	       (:file "oriented-decomp")
	       (:file "decomp")
	       (:file "cwd-term-to-graph")
	       (:file "decomp-test")
	       (:file "sat-decomposition")
	       (:file "decomposition")
	       (:file "bipartite-decomposition")
	       (:file "test-decomposition")
	       (:file "encode")
	       (:file "decode")
	       (:file "derivations")
	       (:file "input-sat-derivation")
	       (:file "derivation-to-term")
	       (:file "some-derivations")
	       ))

(pushnew :decomp *features*)
