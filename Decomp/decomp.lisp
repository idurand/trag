;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defgeneric node-info (node graph constraints)
  (:method ((node node) (graph graph) (constraints constraints))
    (if (oriented-p graph)
	(oriented-node-info node graph constraints)
	(unoriented-node-info node constraints))))

(defgeneric port-attribution (port constraints)
  (:method ((port integer) (constraints constraints))
    ;; keep node-port if not reserved
    ;; otherwise get a new port (next-port)
    (let ((reserved-ports (constraints-ports constraints)))
      (if (ports-member port reserved-ports)
	  (next-port reserved-ports)
	  port))))

(defgeneric select-any-node (graph constraints)
  (:method ((graph graph) (constraints constraints))
    (let ((node (nodes-car (graph-nodes graph))))
      (node-info node graph constraints))))

;; le ports des enodes ayant un port apparaissant dans les onodes
;; devront etres changés pour que les add_a_b ne rajoutent pas des arcs
;; vers les onodes
;; les autres ports n'ont pas a etre synchronises
(defgeneric select-min-node (graph constraints))
(defmethod select-min-node ((g graph) (constraints constraints))
  (let* ((nodes (container-contents (graph-nodes g)))
	 (node (pop nodes))
	 (node-info (node-info node g constraints))
	 (min-weight (info-weight node-info)))
    (if nodes
	(let ((nodes-infos (list node-info)))
	  (mapc
	   (lambda (node)
	     (let* ((info (node-info node g constraints))
		    (weight (info-weight info)))
	       (when (zerop weight)
		 (return-from select-min-node info))
	       (when (< weight min-weight)
		 (setf min-weight weight)
		 (push info nodes-infos))))
	   nodes)
	  (find-if
	   (lambda (info)
	     (= (info-weight info) min-weight))
	   nodes-infos))
	node-info)))

;; penser a rajouter de sortir des qu' on a min 0

(defvar *min-node* t)
(defvar *selection-function*)
(setq *selection-function* #'select-min-node)

(defvar *elimination-ordering*)

(defgeneric select-next-node (graph constraints)
  (:method ((g graph) (constraints constraints))
    (let* ((num (pop *elimination-ordering*))
	   (node (find-node-with-num num g)))
      (node-info node g constraints))))

(defgeneric select-node (graph constraints)
  (:method ((g graph) (constraints constraints))
    (if *min-node*
	(select-min-node g constraints)
	(select-any-node g constraints))))

(defgeneric node-place-p (node)
  (:method ((node node))
    (place-p (num node))))

(defgeneric isolated-node (node constraints)
  (:method ((node node) (constraints constraints))
    (let ((p (or (node-port node constraints)
		 (next-port (constraints-ports constraints)))))
      (list (set-term-eti (cwd-term-vertex p) (num node))
	    (make-one-constraint-constraints node p)))))

(defgeneric connected-cwd-decomp (graph constraints selection-function)
  (:method ((g graph) (constraints constraints) (selection-function function))
    (if (oriented-p g)
	(connected-cwd-oriented-decomp g constraints selection-function)
	(connected-cwd-unoriented-decomp g constraints selection-function))))

(defgeneric cwd-decomp (graph constraints selection-function)
  (:method ((g graph) (constraints constraints) (selection-function function))
    (let ((components (graph-component-ndecomposition g)))
      (if (null (cdr components))
	  (connected-cwd-decomp g constraints selection-function)
	  (let ((cc (mapcar
		     (lambda (component)
		       (let ((nodes (get-nodes component)))
			 (cwd-decomp
			  component
			  (filter-constraints nodes constraints)
			  selection-function)))
		     components)))
	    (list
	     (cwd-term-binary-oplus (mapcar #'car cc))
	     (reduce #'merge-independant-constraints (mapcar #'cadr cc))))))))

(defgeneric cwd-approx-ndecomposition (graph &optional selection-function)
  (:method ((g graph) &optional (selection-function #'select-min-node))
    (remove-top-ren 
     (car
      (cwd-decomp g (make-initial-constraints (get-nodes g)) selection-function)))))

(defgeneric cwd-approx-decomposition (graph &optional selection-function)
  (:documentation "heuristic decomposition of GRAPH oriented or not")
  (:method ((graph graph) &optional (selection-function #'select-min-node))
    (if (graph-empty-p graph)
	(cwd-term-empty-graph)
	(cwd-approx-ndecomposition (graph-copy graph) selection-function))))

(defun decomp-test-twd (ntimes nb-nodes &optional (ratio 2))
  (loop
    repeat ntimes
    with g
    with cwd
    with term
    do (progn
	 (setf g (twd-two-random-graph nb-nodes (floor (/ nb-nodes ratio))))
	 (setf term (cwd-approx-ndecomposition g #'select-min-node))
	 (setf cwd (clique-width term))
	 (when (> cwd 6)
	   (print cwd)
	   (defparameter *term* term)))
    collect cwd))
