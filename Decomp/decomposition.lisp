;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defgeneric cwd-decomposition (graph &optional optimal)
  (:method (graph &optional (optimal nil))
    (when (graph-has-loop-p graph)
      (warn
       "~A has loops; currently we don't handle loops completely"
       graph))
    (if optimal
	(cwd-optimal-decomposition graph)
	(progn
	  (when (graph-has-loop-p graph)
	    (warn
	     "loops of ~A will be lost thru heuristic decomposition" graph))
	  (cwd-approx-decomposition graph #'select-min-node)))))


