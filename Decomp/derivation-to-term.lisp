;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defgeneric ren-operations (term component old-component)
  (:method ((term term) (component component) (old-component component))
    (loop for class in (classes component)
	  do (let* ((label (nodes-class-label class))
		    (node (nodes-class-representative class))
		    (old-class (node-class node old-component))
		    (old-label (nodes-class-label old-class)))
	       (when (/= old-label label)
		 (setf term (cwd-term-ren label old-label term))))
	  finally (return term))))

(defgeneric component-nodes (component)
  (:method ((component component))
    (make-nodes-class
     (reduce #'append (mapcar #'nodes (classes component))))))

(defgeneric add-operations (term component arcs)
  (:method ((term term) (component component) (arcs container))
    (let ((component-nodes (component-nodes component))
	  (add-pairs '())
	  (added-arcs '()))
;;      (format t "arcs ~A~%" arcs)
      (container-map
       (lambda (arc)
	 (let ((origin (num (origin arc)))
	       (extremity (num (extremity arc))))
	   (when (and (nodes-class-member origin component-nodes)
		      (nodes-class-member extremity component-nodes))
	     (push arc added-arcs)
	     (let* ((a (component-node-label origin component))
		    (b (component-node-label extremity component))
		    (pair (list a b)))
;;	       (format t "origin ~A[~A] extremity ~A[~A] ~%" a origin b extremity)
;;	       (format t "pair:~A, add_pairs:~A~%" pair add-pairs)
	       (unless (member pair add-pairs :test #'equal)
		 (push pair add-pairs)
		 (setf
		  term
		  (cwd-term-oriented-add (first pair) (second pair) term)))))))
       arcs)
      (loop for arc in added-arcs
	    do  (container-delete arc arcs)))
    term))

(defgeneric derivation-to-term-rec (component derivation arcs)
  (:method ((component component) (derivation derivation) (arcs container))
    (if (derivation-empty-p derivation)
	(let* ((class (car (classes component)))
	       (node (nodes-class-representative class))
	       (label (nodes-class-label class)))
	  (cwd-term-num-vertex label node))
	(let* ((deriv-steps (deriv-steps derivation))
	       (deriv-step (car deriv-steps))
	       (nderivation (make-derivation (cdr deriv-steps)))
	       (term (cwd-term-empty-graph)))
	  (loop
	    for c in (components deriv-step)
	    do (let* ((cc (set-component-labels c component))
		      (nodes-class (component-nodes cc))
		      (subterm
			(derivation-to-term-rec 
			 cc
			 (filter-derivation cc nderivation nodes-class) arcs)))
		 (setf subterm (ren-operations subterm cc component))
		 (setf term (cwd-term-oplus term subterm))
		 ;; homogeneiser les labels dans les classes for c
		 ))
	  ;; adds
	  (setf term (add-operations term component arcs))
	  term))))

(defun next-free-label-list (l)
  (loop
    for label from 0
    unless (find label l)
      do (return label)))

(defgeneric set-component-labels (new-component old-component)
  (:method ((new-component component) (old-component component))
    (let* ((nodes (component-nodes new-component))
	   (c (filter-component-with-nodes old-component nodes))
	   (old-labels (remove nil (mapcar #'nodes-class-label (classes c)))))
      (loop
	with reused-labels = '()
	with new-labels = '()
	for new-class in (classes new-component)
	do (let* ((node (nodes-class-representative new-class))
		  (old-class (node-class node old-component))
		  (label (nodes-class-label old-class)))
	     (if (member label reused-labels)
		 (progn
		   (setf label (next-free-label-list (union old-labels new-labels)))
		   (push label new-labels))
		 (push label reused-labels))
	     (setf (nodes-class-label new-class) label)))
      new-component)))

(defgeneric init-component-labels (component)
  (:method ((component component))
    (loop
      with label = -1
      for class in (classes component)
      do (setf (nodes-class-label class) (incf label)))
    component))

(defgeneric derivation-to-term (derivation graph)
  (:method ((derivation derivation) (graph graph))
    (let* ((deriv-steps (deriv-steps derivation))
	   (deriv-step (car deriv-steps))
	   (components (components deriv-step))
	   (component (copy-component (car components))))
      (init-component-labels component)
      (remove-top-ren
       (derivation-to-term-rec
	component
	(make-derivation (cdr deriv-steps))
	(if (oriented-p graph)
	    (graph-arcs graph)
	    (graph-edges graph)))))))
