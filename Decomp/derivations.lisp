;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defgeneric nodes-class-label (nodes-class))

(defclass nodes-class ()
  ((%nodes :initarg :nodes :reader nodes)
   (%label :initarg :label :initform nil :accessor nodes-class-label)))

(defmethod print-object ((nodes-class nodes-class) stream)
  (when (nodes-class-label nodes-class)
    (format stream "~A" (port-to-string (nodes-class-label nodes-class))))
  (format stream "|")
  (display-sequence (nodes nodes-class) stream :sep ",")
  (format stream "|"))

(defgeneric output-class (nodes-class stream)
  (:method ((nodes-class nodes-class) stream)
    (format stream "{")
    (display-sequence (nodes nodes-class) stream :sep ",")
    (format stream "}")))

(defun make-nodes-class (nodes &optional (label nil))
  (make-instance 'nodes-class :label label :nodes (sort (copy-list nodes) #'<)))

(defgeneric copy-nodes-class (class)
  (:method ((class nodes-class))
    (make-nodes-class (copy-list (nodes class)) (nodes-class-label class))))

(defgeneric nodes-class-empty-p (nodes-class)
  (:method ((nodes-class nodes-class))
    (endp (nodes nodes-class))))

(defgeneric nodes-class-member (node class)
  (:method ((node integer) (nodes-class nodes-class))
    (member node (nodes nodes-class) :test #'=)))

(defgeneric nodes-class-intersection (nodes-class1 nodes-class2)
  (:method ((c1 nodes-class) (c2 nodes-class))
    (make-nodes-class (intersection (nodes c1) (nodes c2) :test #'=)
		      (nodes-class-label c1))))

(defgeneric nodes-class-union (nodes-class1 nodes-class2)
  (:method ((c1 nodes-class) (c2 nodes-class))
    (make-nodes-class (union (nodes c1) (nodes c2) :test #'=))))

(defgeneric nodes-class-difference (nodes-class1 nodes-class2)
  (:method ((c1 nodes-class) (c2 nodes-class))
    (make-nodes-class (set-difference (nodes c1) (nodes c2) :test #'=))))

(defgeneric nodes-class-representative (nodes-class)
  (:method ((nodes-class nodes-class))
    (first (nodes nodes-class))))

(defclass component ()
  ((%classes :initarg :classes :reader classes)))

(defmethod print-object ((component component) stream)
  (format stream "{")
  (display-sequence (classes component) stream)
  (format stream "}"))

(defgeneric output-component (component stream)
  (:method ((component component) stream)
    (format stream "{")
    (loop for class in (classes component)
	  do (output-class class stream))
    (format stream "}")))
  
(defgeneric copy-component (component)
  (:method ((component component))
    (make-component (mapcar #'copy-nodes-class (classes component)))))

(defun make-component (classes)
  (make-instance 'component :classes classes))

(defun input-component (external-component)
  (make-component (mapcar #'make-nodes-class external-component)))

(defgeneric node-class (node component)
  (:method ((node integer) (component component))
    (find-if (lambda (class) (nodes-class-member node class))
	     (classes component))))

(defclass deriv-step ()
  ((%components :initarg :components :accessor components)))

(defmethod print-object ((deriv-step deriv-step) stream)
  (format stream "[")
  (display-sequence (components deriv-step) stream)
  (format stream "]"))

(defgeneric output-deriv-step (deriv-step stream)
  (:method ((deriv-step deriv-step) stream)
    (loop
      for component in (components deriv-step)
      do (output-component component stream))))

(defun make-deriv-step (components)
  (make-instance 'deriv-step :components components))

(defun input-deriv-step (external-deriv-step)
  (make-deriv-step (mapcar #'input-component external-deriv-step)))

(defclass derivation ()
  ((%deriv-steps :initarg :deriv-steps :accessor deriv-steps)))

(defmethod print-object ((derivation derivation) stream)
  (format stream "<")
  (loop
    for deriv-step in (deriv-steps derivation)
    do (format stream "~A " deriv-step))
  (format stream ">"))

(defgeneric output-derivation (derivation stream)
  (:method ((derivation derivation) stream)
    (loop
      for deriv-step in (reverse (deriv-steps derivation))
      do (output-deriv-step deriv-step stream)
      do (terpri stream))))

(defun make-derivation (deriv-steps)
  (make-instance 'derivation
		 :deriv-steps deriv-steps))

(defun input-derivation (external-derivation)
  (make-derivation (mapcar #'input-deriv-step (reverse external-derivation))))

(defgeneric component-node-label (node component)
  (:method ((node integer) (component component))
    (nodes-class-label (node-class node component))))

(defgeneric trivial-derivation-p (derivation)
  (:method ((derivation derivation))
    (endp (cdr (deriv-steps derivation)))))

(defgeneric derivation-empty-p (derivation)
  (:method ((derivation derivation))
    (endp (deriv-steps derivation))))

(defgeneric nodes-class= (class1 class2)
  (:method ((class1 nodes-class) (class2 nodes-class))
    (equal (nodes class1) (nodes class2))))

(defgeneric component= (c1 c2)
  (:method ((c1 component) (c2 component))
    (let ((classes1 (classes c1))
	  (classes2 (classes c2)))
      (and (= (length classes1) (length classes2))
	   (subsetp classes1 classes2 :test #'nodes-class=)
	   (subsetp classes2 classes1 :test #'nodes-class=)))))

(defgeneric trim-derivation-with-component (component derivation)
  (:method ((component component) (derivation derivation))
    ;; destructive for derivation
    (loop
      while (component= component (car (components (car (deriv-steps derivation)))))
      until (trivial-derivation-p derivation)
      do (pop (deriv-steps derivation))
      finally (return derivation))))

(defgeneric component-empty-p (component)
  (:method ((component component))
    (endp (classes component))))

(defgeneric filter-derivation-with-nodes (derivation nodes)
  (:method ((derivation derivation) (nodes-class nodes-class))
    ;;  (assert (not (trivial-nodes-class-p nodes-class)))
    (make-derivation
     (mapcar (lambda (components)
	       (filter-deriv-step-with-nodes components nodes-class))
	     (deriv-steps derivation)))))

(defgeneric filter-component-with-nodes (component nodes-class)
  (:method ((component component) (nodes-class nodes-class))
    (let ((classes (classes component)))
      (make-component
       (remove-if
	#'nodes-class-empty-p
	(mapcar
	 (lambda (class)
	   (nodes-class-intersection class nodes-class))
	 classes))))))

(defgeneric filter-deriv-step-with-nodes (deriv-step nodes-class)
  (:method ((deriv-step deriv-step) (nodes-class nodes-class))
    (let ((components (components deriv-step)))
      (make-deriv-step
       (remove-if
	#'component-empty-p
	(mapcar (lambda (component)
		  (filter-component-with-nodes component nodes-class))
		components))))))

(defgeneric filter-derivation (component derivation nodes)
  (:method ((component component) (derivation derivation) (nodes nodes-class))
    (if (derivation-empty-p derivation)
	derivation
	(trim-derivation-with-component
	 component
	 (filter-derivation-with-nodes derivation nodes)))))

(defgeneric nodes-class-size (nodes-class)
  (:method ((nodes-class nodes-class))
    (length (nodes nodes-class))))

(defgeneric component-width (component)
  (:documentation "number of classes in COMPONENT")
  (:method ((component component))
    (length (classes component))))

(defgeneric derivation-cwd (derivation)
  (:documentation "max width of a component if DERIVATION")
  (:method ((derivation derivation))
    (reduce
     #'max
     (mapcar
      (lambda (step)
	(reduce #'max 
		(mapcar #'component-width (components step))))
      (deriv-steps derivation)))))

(defgeneric save-derivation (derivation filename)
  (:method ((derivation derivation) filename)
    (with-open-file (out filename :direction :output :if-exists :supersede)
      (output-derivation derivation out))))
