(in-package :decomp)

(defclass key ()
  ((name :initarg :name
	 :accessor key-name)
   (nColors :initarg :nColors
	    :accessor key-nColors)))

(defun key-create (name nColors)
  (make-instance 'key :name name :nColors nColors))

(defgeneric key-equal (k1 k2)
  (:documentation "Return T if the key k1 is equal to the key k2, else return NIL"))

(defgeneric key-print (key)
  (:documentation "Print the Key"))

(defmethod key-equal ((k1 key) k2)
  (if (typep k2 'key)
      (and (string= (key-name k1) (key-name k2))
	   (= (key-nColors k1) (key-nColors k2)))))

(defmethod key-print ((k key))
  (format t "~d : ~d " (key-name k) (key-nColors k)))
	


(defclass dictionary ()
  ((size :initform 0
	 :accessor dict-size)
   (key-list :initform NIL
	     :accessor dict-key-list)
   (nClauses-list :initform NIL
		  :accessor dict-nClauses-list)))

(defun dict-create ()
  (make-instance 'dictionary))

(defgeneric dict-add-elem (dict key nClauses)
  (:documentation "Add an elem and his key to the dict. If the key is already in the dict, add it again. Return T if there were no problems, NIL else"))

(defgeneric dict-remove-elem-from-key (dict key)
  (:documentation "Remove the first occurence which as key : key. Return T if not problems, NIL else"))

(defgeneric dict-get-elem-from-key (dict key)
  (:documentation "Return the element in the dict which have key as key if it exist. If not, return NIL"))

(defgeneric dict-for-all-key (dict fun)
  (:documentation "Apply the function fun to all the key in the dict"))

(defgeneric dict-for-all-nclauses (dict fun)
  (:documentation "Apply the function fun to all the nclauses in the dict"))

(defgeneric dict-for-all-elem (dict fun)
  (:documentation "Apply the function fun to all the Element in the dict"))

(defgeneric dict-print (dict)
  (:documentation "Print the dictionary"))

(defmethod dict-add-elem ((dict dictionary) key nClauses)
  (when (and (typep key 'key) (typep nClauses 'integer))
      (if (= (dict-size dict) 0)
	  (progn
	    (setf (dict-key-list dict) (cdr (list 0 key)))
	    (setf (dict-nClauses-list dict) (cdr (list 0 nClauses))))
	  (progn
	    (setf (dict-key-list dict) (append (dict-key-list dict) (list key)))
	    (setf (dict-nClauses-list dict) (append (dict-nClauses-list dict) (list nClauses)))))
      (incf (dict-size dict) 1))
  T)

(defmethod dict-remove-elem-from-key ((dict dictionary) key)
  (if (typep key 'key)
      (do ((i 0 (1+ i)))
	  ((>= i (dict-size dict)))
	(when (key-equal (nth i (dict-key-list dict)) key)
	  (setf (dict-key-list dict) (remove-nth i (dict-key-list dict)))
	  (setf (dict-nClauses-list dict ) (remove-nth i (dict-nClauses-list dict)))
	  (decf (dict-size dict) 1)
	  (return-from dict-remove-elem-from-key T))))
  NIL)

(defmethod dict-get-elem-from-key ((dict dictionary) key)
  (if (typep key 'key)
      (do ((i 0 (1+ i)))
	  ((>= i (dict-size dict)))
	(if (key-equal (nth i (dict-key-list dict)) key)
	    (return-from dict-get-elem-from-key 
	      (nth i (dict-nClauses-list dict))))))
  NIL)

(defmethod dict-for-all-key ((dict dictionary) fun)
  (mapcar fun (dict-key-list dict)))

(defmethod dict-for-all-nclauses ((dict dictionary) fun)
  (mapcar fun (dict-nclauses-list dict)))

(defmethod dict-for-all-elem ((dict dictionary) fun)
  (do ((i 0 (1+ i)))
      ((>= i (dict-size dict)))
    (funcall fun (nth i (dict-key-list dict)) (nth i (dict-nclauses-list dict)))))

(defmethod dict-print ((dict dictionary))
  (dict-for-all-elem
   dict
   (lambda (k n)
     (key-print k)
     (format t "--> ~d~%" n))))


(defun remove-nth (n list)
  (if (or (= 0 n) (endp list))
    (cdr list)
    (cons (car list) (remove-nth (1- n) (cdr list)))))
    
