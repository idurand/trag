(in-package :decomp)

(defclass square-grid () 
  ((size :initarg :size :initform 0
	 :accessor sg-size)
   (grid :initform NIL
	 :accessor sg-grid)))


(defun sg-create (&optional size)
  (if size
      (make-instance 'square-grid :size size)
      (make-instance 'square-grid)))


(defgeneric sg-init (sg &optional size)
  (:documentation "Initialize the SG"))

(defgeneric sg-set-value-grid (sg u v val)
  (:documentation "Set the value val in the case (u,v) in sg's grid"))

(defgeneric sg-get-value-grid (sg u v)
  (:documentation "Return the value in the case (u,v) in sg's grid"))

(defgeneric sg-is-value-grid-equal (sg u v val)
  (:documentation "Test if the value in the case (u,v) in sg's grid is equal to val"))

(defgeneric sg-is-not-value-grid-equal  (sg u v val)
  (:documentation "Test if the value in the case (u,v) in sg's grid isn't equal to val"))

(defgeneric sg-does-value-grid-exist (sg u v)
  (:documentation "Test if the case (u,v) in sg's grid does have a value"))

(defgeneric sg-does-not-value-grid-exist (sg u v)
  (:documentation "Test if the case (u,v) in sg's grid doesn't have a value"))

(defmethod sg-init ((sg square-grid) &optional size)
  (if size
      (setf (sg-size sg) size))
  (setf (sg-grid sg) (make-array (list (sg-size sg) (sg-size sg)))))

(defmethod sg-set-value-grid ((sg square-grid) u v val)
  (setf (aref (sg-grid sg) u v) val))

(defmethod sg-get-value-grid ((sg square-grid) u v)
  (aref (sg-grid sg) u v))

(defmethod sg-is-value-grid-equal ((sg square-grid) u v val)
  (eq (sg-get-value-grid sg u v) val))

(defmethod sg-is-not-value-grid-equal ((sg square-grid) u v val)
  (not (sg-is-value-grid-equal sg u v val)))

(defmethod sg-does-value-grid-exist ((sg square-grid) u v)
  (sg-is-not-value-grid-equal sg u v 0))

(defmethod sg-does-not-value-grid-exist ((sg square-grid) u v)
  (not (sg-does-value-grid-exist sg u v)))
