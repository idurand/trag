(in-package :decomp)

(defun make-clause-vars ()
  (init-clause-vars)
  (make-component-vars)
  (make-group-vars)
  (make-representative-vars)
  (make-s-vars))

(defun arc-property-clauses () ;; len = 2
  (loop
    for s from 1 to (nb-levels)
    nconc (loop
	    for u from 1 to *sat-nb-nodes*
	    nconc (loop
		    for v from (1+ u) to *sat-nb-nodes*
		    when (or (is-arc u v) (is-arc v u))
		      collect (component-group-clause u v (1- s) u v s)))))

(defun neighbor-clauses (u v w level)
  (let ((clauses '()))
    (when (is-arc u v)
      (unless (is-arc u w)

	(push
	 (component-group-clause
	  (min u v) (max u v) (1- level)
	  (min v w) (max v w) level) clauses))
      (unless (is-arc w v)
	(push
	 (component-group-clause
	  (min u v) (max u v) (1- level)
	  (min u w) (max u w) level) clauses)))
    clauses))

(defun level-neighbor-clauses (level)
  (remove-duplicates
   (loop
     for u from 1 to *sat-nb-nodes*
     nconc
     (loop
       for v from 1 to *sat-nb-nodes*
       nconc
       (loop
	 for w from 1 to *sat-nb-nodes*
	 unless (or (= u w) (= v w))
	   nconc (neighbor-clauses u v w level))))
   :test #'clause= :from-end t))

(defun neighbor-property-clauses () ;; len = 2
   (loop
     for level from 1 to (nb-levels)
     nconc (level-neighbor-clauses level)))

(defun path-clause (u v x w level)
  (make-clause-from-literals
   (make-literal (component-var u v (1- level)))
   (make-literal (group-var (min u x) (max u x) level) t)
   (make-literal (group-var (min v w) (max v w) level) t)))

;; path property clauses

;; Pour tous 1 < = i < = t
;; Pour tous uv dans E
;; Pour tous x,w différents , et différents de u et v :
;; Si uv dans E ET 
;; ( uw dans E [ OU wu dans E] ) ET 
;; ( xv dans E [ OU vx dans E] ) ET 
;; (xw non dans E [ET wx non dans E])  :
;; c_m(u,v),M(u,v),i-1 -g_m(u,x),M(u,x),i  -g_m(v,w),M(v,w),i  x0

(defun u-v-path-clauses (level u v)
  (when (is-arc u v)
    (loop
      for w from 1 to *sat-nb-nodes*
      when (and (/= u w) (is-arc u w))
	nconc (loop
		for x from 1 to *sat-nb-nodes*
		when (and
		      (/= u v x w)
		      (is-arc x v)
		      (not (is-arc x w)))
		  collect (path-clause u v x w level)))))

(defun u-path-clauses (level u)
  (loop
    for v from (1+ u) to *sat-nb-nodes*
    nconc (u-v-path-clauses level u v)))

(defun level-path-clauses (level)
  (loop
    for u from 1 to *sat-nb-nodes*
    nconc (u-path-clauses level u)))
    
(defun path-property-clauses () ;; len = 3
  (loop
    for level from 1 to (nb-levels)
    nconc (level-path-clauses level)))

(defun arc-dependent-clauses ()
  (nconc
   (arc-property-clauses) ;; len = 2
   (neighbor-property-clauses) ;; len = 2
   (path-property-clauses)) ;; len = 3
  )

(defun init-globals (graph cwd)
  (assert (oriented-p graph))
  (init-sat-gen-vars graph cwd)
  (make-clause-vars))

(defun cwd-k-clauses (graph cwd)
  (unless (oriented-p graph)
    (setq graph (graph-orient graph)))
  (init-globals graph cwd)
  (nconc
   (arc-independent-clauses)
   (arc-dependent-clauses)))

(defun cwd-encode (graph cwd)
  (when (= cwd 1)
    (if (zerop (graph-nb-arcs graph))
	(format t "p cnf 0 0")
	(format t "p cnf 0 1"))
    (return-from cwd-encode 0))
  (let ((clauses (cwd-k-clauses graph cwd)))
    (display-clauses clauses)
    (format *error-output* "nb-vars: ~A, nb-clauses: ~A~%"
	  (nb-vars) (length clauses))))

(defun cwd-encode-graph-to-stream (graph cwd stream)
  (let ((*standard-output* stream))
    (cwd-encode graph cwd)))

(defun cwd-encode-graph-to-file (cnf-file graph cwd)
  (with-open-file (stream cnf-file :direction :output :if-exists :supersede)
    (cwd-encode-graph-to-stream graph cwd stream)))
