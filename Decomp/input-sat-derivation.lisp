;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defparameter *sat-readtable* (copy-readtable *readtable*))
(set-syntax-from-char #\, #\Space *sat-readtable*)
(set-syntax-from-char #\} #\) *sat-readtable*)

(set-macro-character
 #\{
 (lambda (stream char)
   (declare (ignore char))
   (read-delimited-list #\} stream t))
 t
 *sat-readtable*)

(defun parse-component (stream)
  (let ((*readtable* *sat-readtable*))
    (loop
      for component = (read stream nil nil) ;; then (read stream nil nil)
      while component
      collect component)))

(defun skip-to-{ (stream)
  (loop
    for char = (read-char stream nil nil) ;; then (read-char stream nil nil)
    while char
    until (eql #\{ char)
    finally (unread-char char stream)))

(defun load-sat-derivation-stream (stream)
;;  (skip-to-{ stream)
  (input-derivation
  (loop
    for line = (read-line stream nil nil) ;; then (read-line stream nil nil)
    while line
    collect (with-input-from-string (input line) (parse-component input))
    )))

(defun load-sat-derivation (filename)
  (with-open-file (stream filename :if-does-not-exist nil)
    (unless stream (return-from load-sat-derivation nil))
    (load-sat-derivation-stream stream)))
