;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defclass oriented-node-info (node-info)
  ((info-out-nodes :reader info-out-nodes :initarg :info-out-nodes :type constraints)
   (info-io-nodes :reader info-io-nodes :initarg :info-io-nodes :type constraints)
   (info-ports-other-in :reader info-ports-other-in :initarg :info-ports-other-in :type ports)
   (info-ports-other-out :reader info-ports-other-out :initarg :info-ports-other-out :type ports)
   (info-ports-other-io :reader info-ports-other-io :initarg :info-ports-other-io :type ports)
   (info-ports-in-out :reader info-ports-in-out :initarg :info-ports-in-out :type ports)
   (info-ports-in-io :reader info-ports-in-io :initarg :info-ports-in-io :type ports)
   (info-ports-out-io :reader info-ports-out-io :initarg :info-ports-out-io :type ports)
   ))

(defmethod print-object ((oni oriented-node-info) stream)
  (with-slots (info-node info-other-nodes info-in-nodes info-out-nodes info-io-nodes) oni
    (format stream "~A:~A:~A:~A:~A~%"
	    info-node info-other-nodes info-in-nodes info-out-nodes info-io-nodes)))

(defun make-oriented-node-info 
    (node other-nodes in-nodes out-nodes io-nodes ports-other-in ports-other-out ports-other-io ports-in-out ports-in-io ports-out-io)
  (make-instance 'oriented-node-info
		 :info-node node
		 :info-other-nodes other-nodes
		 :info-in-nodes in-nodes
		 :info-out-nodes out-nodes
		 :info-io-nodes io-nodes
		 :info-ports-other-in ports-other-in
		 :info-ports-other-out ports-other-out
		 :info-ports-other-io ports-other-io
		 :info-ports-in-out ports-in-out
		 :info-ports-in-io ports-in-io
		 :info-ports-out-io ports-out-io
		 ))

(defmethod info-weight ((node-info oriented-node-info))
  (+
   (container-size (info-ports-other-in node-info))
   (container-size (info-ports-other-out node-info))
   (container-size (info-ports-other-io node-info))
   (container-size (info-ports-in-out node-info))
   (container-size (info-ports-in-io node-info))
   (container-size (info-ports-out-io node-info))))

;; incident-nodes dont la classe contient un o-node

(defgeneric oriented-node-info (node graph constraints))
;; node is still in the constraints
(defmethod oriented-node-info ((node node) (graph graph) (constraints constraints))
  (let* ((all-nodes (nodes-delete node (constraints-nodes constraints)))
	 (out-nodes (node-out-nodes node))
	 (in-nodes (node-in-nodes node graph))
	 (other-nodes
	  (nodes-difference (nodes-ndifference all-nodes out-nodes) in-nodes))
	 (io-nodes (nodes-intersection out-nodes in-nodes)))
    (setf out-nodes (nodes-difference out-nodes io-nodes))
    (setf in-nodes (nodes-difference in-nodes io-nodes))
    (let ((in-nodes-constraints (filter-constraints in-nodes constraints))
	  (out-nodes-constraints (filter-constraints out-nodes constraints))
	  (io-nodes-constraints (filter-constraints io-nodes constraints))
	  (other-nodes-constraints (filter-constraints other-nodes constraints)))
      (make-oriented-node-info
       node
       other-nodes-constraints 
       in-nodes-constraints 
       out-nodes-constraints 
       io-nodes-constraints
       (ports-intersection
	(constraints-ports other-nodes-constraints)
	(constraints-ports in-nodes-constraints))
       (ports-intersection
	(constraints-ports other-nodes-constraints)
	(constraints-ports out-nodes-constraints))
       (ports-intersection
	(constraints-ports other-nodes-constraints)
	(constraints-ports io-nodes-constraints))
       (ports-intersection
	(constraints-ports in-nodes-constraints)
	(constraints-ports out-nodes-constraints))
       (ports-intersection
	(constraints-ports in-nodes-constraints)
	(constraints-ports io-nodes-constraints))
       (ports-intersection
	(constraints-ports out-nodes-constraints)
	(constraints-ports io-nodes-constraints))
       ))))

(defgeneric oriented-ports-attribution (node-info constraints))
;; node is still in the constraints
(defmethod oriented-ports-attribution ((node-info oriented-node-info) (constraints constraints))
  (let* ((node-to-remove (info-node node-info))
	 (in-nodes-constraints (info-in-nodes node-info))
	 (out-nodes-constraints (info-out-nodes node-info))
	 (io-nodes-constraints (info-io-nodes node-info))
	 (ports-other-in (info-ports-other-in node-info))
	 (ports-other-out (info-ports-other-out node-info))
	 (ports-other-io (info-ports-other-io node-info))
	 (ports-in-out (info-ports-in-out node-info))
	 (ports-in-io (info-ports-in-io node-info))
	 (ports-out-io (info-ports-out-io node-info))
	 (reserved-ports (constraints-ports-except-node constraints node-to-remove))
	 (to-out (ports-union ports-other-out ports-in-out))
	 (to-io (ports-union ports-other-io (ports-union ports-in-io ports-out-io)))
	 (in-pairs (mapcar
		    (lambda (p)
		      (let ((newp (next-port reserved-ports)))
			(prog1 (cons p newp)
			  (ports-nadjoin newp reserved-ports))))
		    (container-contents ports-other-in)))
	 (out-pairs (mapcar
		     (lambda (p)
		       (let ((newp (next-port reserved-ports)))
			 (prog1 (cons p newp)
			   (ports-nadjoin newp reserved-ports))))
		     (container-contents to-out)))
	 (io-pairs (mapcar
		    (lambda (p)
		      (let ((newp (next-port reserved-ports)))
			(prog1 (cons p newp)
			  (ports-nadjoin newp reserved-ports))))
		    (container-contents to-io)))
	 (new-constraints (make-constraints)))
    (maphash (lambda (num-node constraint)
	       (declare (ignore num-node))
	       (let ((node (constraint-node constraint)))
		 (unless (eq node node-to-remove)
		   (let* ((port (constraint-port constraint))
			  (newport port))
		     (if (and (node-constraint node in-nodes-constraints)
			      (ports-member port ports-other-in))
			 (setf newport (cdr (assoc port in-pairs)))
			 (if (and (node-constraint node out-nodes-constraints)
				   (ports-member port to-out))
			     (setf newport (cdr (assoc port out-pairs)))
			     (when (and (node-constraint node io-nodes-constraints)
					 (ports-member port to-io))
			       (setf newport (cdr (assoc port io-pairs))))))
		     (add-node-port-constraint node newport new-constraints)))))
	     (constraints-table constraints))
    new-constraints))

(defgeneric connected-cwd-oriented-decomp (graph constraints selection-function)
  (:method ((g graph) (constraints constraints) (selection-function function))
  ;;  (assert (graph-connected-p g))
  ;;  (graph-show g)
  (let* ((nodes (graph-nodes g))
	 (nb-nodes (nodes-size nodes)))
    (when (= 1 nb-nodes)
      (return-from connected-cwd-oriented-decomp (isolated-node (nodes-car nodes) constraints)))
    ;; more than one node: selection of a node
    ;; (print nb-nodes)
    (let* ((node-info (funcall selection-function g constraints))
	   (node (info-node node-info))
	   (in-constraints (info-in-nodes node-info))
	   (out-constraints (info-out-nodes node-info))
	   (io-constraints (info-io-nodes node-info))
	   (other-constraints (info-other-nodes node-info)))
      (graph-delete-node node g) ;; destructive on g and nodes
      (let* ((new-constraints (oriented-ports-attribution node-info constraints))
	     (decomp (cwd-decomp g new-constraints selection-function))
	     (term (first decomp))
	     (num-ports (second decomp)))
;;	(assert (term-satisfies-constraints term new-constraints))
	;; on peut deja renomer les non extremity et recupere les ports 
	;; qui peuvent etre utilises pour node
	;; modifies num-ports
	(setq term (ports-renaming term other-constraints num-ports))
	(let ((out-ports-to-connect (constraints-ports (filter-constraints (constraints-nodes out-constraints) num-ports)))
	      (in-ports-to-connect (constraints-ports (filter-constraints (constraints-nodes in-constraints) num-ports)))
	      (io-ports-to-connect (constraints-ports (filter-constraints (constraints-nodes io-constraints) num-ports)))
	      (p (port-attribution (node-port node constraints) num-ports)))
	  (setq term (cwd-term-oplus
		      (set-term-eti
		       (cwd-term-vertex p) (num node))
		      term))
	  (setq num-ports (add-node-port-constraint node p num-ports))
	  (container-map (lambda (port)
			   (setf term
				 (cwd-term-oriented-add p port term)))
			 out-ports-to-connect)
	  (container-map (lambda (port)
			   (setf term
				 (cwd-term-oriented-add port p term)))
			 in-ports-to-connect)
	  (container-map (lambda (port)
			   (setf term
				 (cwd-term-oriented-add p port (cwd-term-oriented-add port p term))))
			 io-ports-to-connect)
	  (setq term (ports-renaming term out-constraints num-ports));; modifies num-ports
	  (setq term (ports-renaming term io-constraints num-ports));; modifies num-ports
	  (setq term 
		(ports-renaming
		 term
		 (add-node-port-constraint node
					   (node-port node constraints)
					   in-constraints)
		 num-ports)) ;; modifies num-ports

	  ;;	(format t "term ~A~%"  term)
	  (assert (term-satisfies-constraints term constraints))
	  (list term num-ports)))))))

(defun oriented-decomp-test (g n)
  (loop
    repeat n
    for og = (graph-randomize-orientation g)
    unless (graph-equivalent-p og (cwd-term-to-graph (cwd-decomposition og)))
      do (error "~A ~%" (graph-to-earcs og))))
