;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defun cwd-encode-file-to-file (dimacs-file cwd cnf-file)
  (cwd-encode-graph-to-file
   cnf-file
   (load-dimacs-graph dimacs-file)
   cwd))

(defgeneric try-cwd-sat-decomposition (graph-or-file cwd)
  (:documentation "returns satisfied literals")
  (:method ((graph graph) (cwd integer))
    (unless (oriented-p graph)
      (setq graph (graph-orient graph)))
    (let ((name (name graph)))
      (glucose (cwd-k-clauses graph cwd) :name (cwd-name name cwd))))
  (:method ((dimacs-file string) (cwd integer))
    (try-cwd-sat-decomposition (load-dimacs-graph dimacs-file) cwd)))

(defgeneric cwd-k-p (graph-or-file cwd)
  (:method :around ((graph graph) (cwd integer))
    (or (<= (graph-nb-nodes graph) cwd)
	(call-next-method)))
  (:method ((graph graph) (cwd integer))
    (let ((name (name graph)))
      (unless (oriented-p graph)
	(setq graph (graph-orient graph)))
      (glucose (cwd-k-clauses graph cwd) :name name)))
  (:method ((dimacs-file string) (cwd integer))
      (cwd-k-p (load-dimacs-graph dimacs-file) cwd)))

(defgeneric graph-cwd (graph)
  (:method ((graph graph))
    (let ((cwd 1)
	  literals)
      (loop
	with ok = nil
	do (format t " Trying ~A~%" (1+ cwd))
	do (multiple-value-bind (res lit) (try-cwd-sat-decomposition graph (incf cwd))
	     (setq ok res)
	     (setq literals lit))
	until ok)
      (values cwd literals))))

(defgeneric sat-derivation (graph)
  (:method ((graph graph))
    (multiple-value-bind (cwd literals) (graph-cwd graph)
      (values (decode-graph-literals graph literals) cwd))))

(defgeneric sat-decomposition (graph)
  (:method ((graph graph))
    (let ((oterm (derivation-to-term (sat-derivation graph) graph)))
      (if (oriented-p graph)
	  oterm
	  (clique-width::cwd-term-unorient oterm))))
  (:method ((file string))
    (sat-decomposition (load-dimacs-graph file))))
 
(defgeneric cwd-optimal-decomposition (graph)
  (:method ((g graph))
    (if (graph-empty-p g)
	(cwd-term-empty-graph)
	(if (zerop (graph-nb-edges g))
	    (cwd-approx-decomposition g)
	    (sat-decomposition g))))
  (:method ((file string))
    (cwd-optimal-decomposition (load-dimacs-graph file))))
