;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defgeneric oplus-term-to-derivation (term))
(defmethod oplus-term-to-derivation ((term term))
  (if (term-constant-p term)
      (make-derivation
       (list
	(make-deriv-step (make-component (list (make-nodes-class (term-eti term)))))))
      (let ((deriv-steps (mapcar #'oplus-term-to-derivation (arg term))))
	

(defgeneric term-to-derivation (term))
(defmethod term-to-derivation ((term term))
  (loop
       
  (if (term-constant-p root)
	(make-derivation
	 (list
	  (make-deriv-step (make-component (list (make-nodes-class (term-eti term)))))))
	(let ((root (root term))
	      (arg (mapcar #'term-to-derivation (arg term))))
	  (cond 
	    ((oplus-symbol-p root)
	     (make-derivation
	      (cons
	       (combine-steps arg)
	       
	    (build-term root arg))
	(car arg))))


  (oplus-term-to-derivation (term-to-oplus-term term)))

;; remark one could do this directly without going through the oplus-term
