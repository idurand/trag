;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)

(defgeneric test-cwd-optimal-decomposition (graph)
  (:method ((graph graph))
    (format t "graph:~A~%" graph)
    (let* ((term (cwd-optimal-decomposition graph))
	   (new-graph (cwd-term-to-graph term)))
      (format t "cwd=~A~%" (clique-width term))
      (assert (graph-equivalent-p graph new-graph))
      term)))

(defgeneric test-cwd-heuristic-decomposition (graph)
  (:method ((graph graph))
    (format t "graph:~A~%" graph)
    (let* ((oriented (oriented-p graph))
	   (ograph (if oriented graph (graph-orient graph)))
	   (oterm (cwd-optimal-decomposition ograph))
	   (new-ograph (cwd-term-to-graph oterm))
	   (new-graph (if oriented new-ograph (graph-unorient new-ograph))))
      (format t "cwd=~A~%" (clique-width oterm))
      (assert (graph-equivalent-p graph new-graph)))))

(defgeneric test-cwd-decomposition (graph &key optimal)
  (:method ((graph graph) &key (optimal t))
    (format t "graph:~A~%" graph)
    (if optimal
	(test-cwd-optimal-decomposition graph)
	(test-cwd-heuristic-decomposition graph))))
