;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(with-time 
    (defparameter *term*
      (decomp::cwd-approx-ndecomposition (graph::variant-rgrid 6 45))))
;;  in 6.655 sec 2013-09-21
;; in 5.47sec 2014-11-21 shenzhou

(defparameter *f* (autograph::coloring-automaton 3))
(defparameter *af* (termauto::attribute-automaton *f* termauto::*count-afun*))
(defparameter *p* (termauto::constants-color-projection *af*))
(with-time (defparameter *res* (termauto::compute-value *term* *p*)))
;; in 18.876sec quand???

;; 23101395830675291023890851302878144452589681340533204
;; in 57.115sec !!! 2014-11-21 shenzhou

(with-time 
    (defparameter *term*
      (decomp::cwd-approx-ndecomposition (graph::variant-rgrid 6 47))))
;; in 74min 57.69sec
;; 2013-09-26 8.756sec
;; 2014-11-21 7.287sec
AUTOGRAPH> (with-time (setf *res* (compute-value *term* *p*)))
;;  in 20.544sec
;; 6207322297165539249834010416928260214944062283965874990

					;6x48 in 82min 24.72sec
;; 2013-09-26 8.793sec

;; in 21.101sec 101750682830347094091147670935880468812899673705005596014
;; 49sec 2013-09-26 pourquoi pire?

(with-time 
    (defparameter *term*
      (decomp::cwd-approx-ndecomposition (graph::variant-rgrid 6 50))))
;; 2014-11-21 8.602sec shenzhou
(with-time (defparameter *res* (termauto::compute-value *term* *p*)))

;; 2014-11-21 Heap exhausted shenzhou
