;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :decomp)
(defclass node-info ()
  ((info-node :reader info-node :initarg :info-node)
   ;; ports both in incident and others
   (info-other-nodes :reader info-other-nodes :type constraints :initarg :info-other-nodes)
   (info-in-nodes :reader info-in-nodes :type constraints :initarg :info-in-nodes)))
;; incident-nodes dont la classe contient un o-node

(defclass unoriented-node-info (node-info)
  ((info-ports :reader info-ports :initarg :info-ports)))

(defmethod print-object ((node-info node-info) stream)
  (with-slots (info-node info-ports info-other-nodes info-in-nodes) node-info
    (format stream "~A:~A:~A:~A" info-node info-ports info-other-nodes info-in-nodes)))

(defun make-unoriented-node-info (node ports other-nodes in-nodes)
  (make-instance 'unoriented-node-info
		 :info-node node
		 :info-ports ports
		 :info-other-nodes other-nodes
		 :info-in-nodes in-nodes))

(defgeneric info-weight (node-info)
  (:method ((node-info unoriented-node-info))
    (container-size (info-ports node-info))))

(defgeneric unoriented-node-info (node constraints)
(:method ((node node) (constraints constraints))
;; node is still in the constraints
  (let* ((all-nodes (nodes-delete node (constraints-nodes constraints)))
	 (in-nodes (node-out-nodes node))
	 (in-nodes-constraints (filter-constraints in-nodes constraints))
	 (other-nodes-constraints (filter-constraints (nodes-ndifference all-nodes in-nodes) constraints))
	 (ports 
	  (ports-intersection
	   	 (constraints-ports in-nodes-constraints)
	   	 (constraints-ports other-nodes-constraints))))
    (make-unoriented-node-info node ports other-nodes-constraints in-nodes-constraints))))

(defgeneric unoriented-ports-attribution (node-info constraints)
  ;; node is still in the constraints
  (:method ((node-info node-info) (constraints constraints))
    (let* ((node-to-remove (info-node node-info))
	   (in-nodes-constraints (info-in-nodes node-info))
	   (ports (info-ports node-info))
	   (reserved-ports (constraints-ports-except-node constraints node-to-remove))
	   (in-pairs (mapcar
		      (lambda (p)
			(let ((newp (next-port reserved-ports)))
			  (prog1 (cons p newp)
			    (ports-nadjoin newp reserved-ports))))
		      (container-contents ports)))
	   (new-constraints (make-constraints)))
      (maphash (lambda (num-node constraint)
		 (declare (ignore num-node))
		 (let ((node (constraint-node constraint)))
		   (unless (eq node node-to-remove)
		     (let* ((port (constraint-port constraint))
			    (newport port)
			    (node-constraint
			      (node-constraint node in-nodes-constraints)))
		       (when node-constraint
			 (let ((port (constraint-port node-constraint)))
			   (when (ports-member port ports)
			     (setf newport (cdr (assoc port in-pairs))))))
		       (add-node-port-constraint node newport new-constraints)))))
	       (constraints-table constraints))
      new-constraints)))

(defgeneric connected-cwd-unoriented-decomp (graph constraints selection-function)
  (:method ((g graph) (constraints constraints) (selection-function function))
    ;;  (assert (graph-connected-p g))
    ;;  (graph-show g)
    (let ((nodes (graph-nodes g)))
      (when (= 1 (nodes-size nodes))
	(return-from connected-cwd-unoriented-decomp
	  (isolated-node (nodes-car nodes) constraints)))
    ;; more than one node: selection of a node
    (let* ((node-info (funcall selection-function g constraints))
	   (node (info-node node-info))
	   (incident-nodes-constraints (info-in-nodes node-info)))
      (graph-delete-node node g) ;; destructive on g and nodes
      (let* ((other-constraints (info-other-nodes node-info))
	     (new-constraints (unoriented-ports-attribution node-info constraints))
	     (decomp (cwd-decomp g new-constraints selection-function))
	     (term (car decomp))
	     (num-ports (cadr decomp)))
	;; on peut deja renomer les non extremity et recuperer les ports 
	;; qui peuvent etre utilises pour node
	;; modifies num-ports
	(setf term (ports-renaming term other-constraints num-ports)) 
	(let ((in-ports-to-connect
		(constraints-ports
		 (filter-constraints
		  (constraints-nodes incident-nodes-constraints)
		  num-ports)))
	      (p (port-attribution
		  (node-port node constraints) num-ports)))
	  (setf term (cwd-term-oplus
		      (set-term-eti
		       (cwd-term-vertex p) (num node))
		      term))
	  (setf num-ports (add-node-port-constraint node p num-ports))
	  (container-map (lambda (port)
			   (setq term (cwd-term-unoriented-add p port term)))
			 in-ports-to-connect)
	  (setf term 
		(ports-renaming
		 term
		 (add-node-port-constraint node
					   (node-port node constraints)
					   incident-nodes-constraints)
		 num-ports)) ;; modifies num-ports
	  ;;	(format t "term ~A~%"  term)
	  (list term num-ports)))))))

