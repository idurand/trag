(in-package :enum)

;; (defun iterated-binary-micha (e1 e2 &rest more)
;;   (if (endp more)
;;       (make-diagonal-steps-enumerator e1 e2)
;;       (make-diagonal-steps-enumerator
;;        e1
;;        (apply #'iterated-binary-micha e1 (car more) (cdr more))
;;        :fun #'cons)))

(defun iterated-binary-ways (e)
  (if (typep e 'enum::product-mixin)
      (cons
       (way e)
       (cons
	(enum::way (enum::enum1 e))
	(iterated-binary-ways (enum::enum2 e))))
      (list (enum::way e))))

(defgeneric binary-ways (e)
  (:method ((e way-product-mixin))
    (list
     (way e)
     (list
      (way (enum1 e))
      (binary-ways (enum2 e)))))
  (:method ((e bidirectional-enumerator))
    (way e)))

(defun way-to-char (way) (if (plusp way) #\+ #\-))
(defun way-to-string (way) (make-string 1 :initial-element (way-to-char way)))

(defun ways-to-string (ways)
  (if (atom ways)
      (way-to-string ways)
      (format nil "~A(~A, ~A)" (way-to-string (first ways))
	      (ways-to-string (car (second ways)))
	      (ways-to-string (cadr (second ways))))))

(defun multi-ways-to-string (ways)
  (coerce (mapcar #'way-to-char ways) 'string))

(defun ways-string (e) (ways-to-string (binary-ways e)))

(defun show-enum-ways (e)
  (init-enumerator e)
  (loop while (next-element-p e)
	for elem = (next-element e)
	do (format t "~A ~A~%" elem (ways-to-string (binary-ways e)))))

(defun show-n-enum-ways (e n)
  (init-enumerator e)
  (loop repeat n
	for elem = (next-element e)
	do (format t "~A ~A~%" elem (ways-to-string (binary-ways e)))))

(defparameter *e0* (make-list-enumerator '()))
(defparameter *e1* (make-list-enumerator '(0)))
(defparameter *e2* (make-list-enumerator '(0 1)))
(defparameter *e3* (make-list-enumerator '(0 1 2)))
(defparameter *e4* (make-list-enumerator '(0 1 2 3)))
(defparameter *e5* (make-list-enumerator '(0 1 2 3 4)))
(defparameter *en* (make-inductive-enumerator 0 #'1+))

(assert (equal '() (collect-enum (make-binary-diagonal-product-enumerator *e0* *e3*))))
(assert (equal '() (collect-enum (make-binary-diagonal-product-enumerator *e0* *e3*))))
(assert (equal 
	 '((0 0) (1 0) (0 1) (0 2) (1 1) (2 0) (2 1) (1 2) (2 2))
	 (collect-enum (make-binary-diagonal-product-enumerator *e3* *e3*))))

(assert (equal '((0 0) (1 0) (0 1) (1 1) (2 0) (2 1))
	       (collect-enum (make-binary-diagonal-product-enumerator *e3* *e2*))))

(defparameter *b33*  (make-binary-diagonal-product-enumerator
		      *e3*
		      (make-binary-diagonal-product-enumerator *e3* *e3*)
		      :fun #'cons))
(assert
 (equal
  '((0 0 0) (1 0 0) (0 1 0) (0 0 1) (1 1 0) (2 0 0) (2 1 0) (1 0 1) (0 0 2)
    (0 1 1) (1 0 2) (2 0 1) (2 0 2) (1 1 1) (0 2 0) (0 2 1) (1 2 0) (2 1 1)
    (2 2 0) (1 2 1) (0 1 2) (0 2 2) (1 1 2) (2 2 1) (2 1 2) (1 2 2) (2 2 2))
  (collect-enum *b33*)))

(defparameter *d33*  (make-binary-diagonal-steps-enumerator
		      *e3*
		      (make-binary-diagonal-steps-enumerator *e3* *e3*)
		      :fun #'cons))

(defparameter *d22* (make-binary-diagonal-steps-enumerator *e2* *e2*))

(assert
 (equal
  '((0 0 0) (1 0 0) (0 1 0) (0 0 1) (0 0 2) (0 1 1) (0 2 0) (1 1 0) (1 0 1)
    (2 0 0) (2 1 0) (2 0 1) (1 0 2) (1 1 1) (1 2 0) (0 2 1) (0 1 2) (0 2 2)
    (1 2 1) (1 1 2) (2 0 2) (2 1 1) (2 2 0) (2 2 1) (2 1 2) (1 2 2) (2 2 2))
  (collect-enum *d33*)))

(defparameter *m33* (make-multi-diagonal-enumerator (list *e3* *e3* *e3*)))

(assert
 (equal
  '((0 0 0) (0 0 1) (0 1 0) (1 0 0) (2 0 0) (1 1 0) (0 2 0) (0 1 1) (1 0 1)
    (0 0 2) (0 1 2) (1 0 2) (2 0 1) (1 1 1) (0 2 1) (1 2 0) (2 1 0) (2 2 0)
    (1 2 1) (2 1 1) (2 0 2) (1 1 2) (0 2 2) (1 2 2) (2 1 2) (2 2 1) (2 2 2))
  (collect-enum *m33*)))

(defparameter *d44* (make-nary-diagonal-steps-enumerator (list  *e4* *e4* *e4* *e4*)))
(assert
 (equal
  '((0 0 0 0) (1 0 0 0) (0 1 0 0) (0 0 1 0) (0 0 0 1) (0 0 0 2) (0 0 1 1)
    (0 0 2 0) (0 1 1 0) (0 1 0 1) (0 2 0 0) (1 1 0 0) (1 0 1 0) (1 0 0 1)
    (2 0 0 0) (3 0 0 0) (2 1 0 0) (2 0 1 0) (2 0 0 1) (1 0 0 2) (1 0 1 1)
    (1 0 2 0) (1 1 1 0) (1 1 0 1) (1 2 0 0) (0 3 0 0) (0 2 1 0) (0 2 0 1)
    (0 1 0 2) (0 1 1 1) (0 1 2 0) (0 0 3 0) (0 0 2 1) (0 0 1 2) (0 0 0 3)
    (0 0 1 3) (0 0 2 2) (0 0 3 1) (0 1 3 0) (0 1 2 1) (0 1 1 2) (0 1 0 3)
    (0 2 0 2) (0 2 1 1) (0 2 2 0) (0 3 1 0) (0 3 0 1) (1 3 0 0) (1 2 1 0)
    (1 2 0 1) (1 1 0 2) (1 1 1 1) (1 1 2 0) (1 0 3 0) (1 0 2 1) (1 0 1 2)
    (1 0 0 3) (2 0 0 2) (2 0 1 1) (2 0 2 0) (2 1 1 0) (2 1 0 1) (2 2 0 0)
    (3 1 0 0) (3 0 1 0) (3 0 0 1) (3 0 0 2) (3 0 1 1) (3 0 2 0) (3 1 1 0)
    (3 1 0 1) (3 2 0 0) (2 3 0 0) (2 2 1 0) (2 2 0 1) (2 1 0 2) (2 1 1 1)
    (2 1 2 0) (2 0 3 0) (2 0 2 1) (2 0 1 2) (2 0 0 3) (1 0 1 3) (1 0 2 2)
    (1 0 3 1) (1 1 3 0) (1 1 2 1) (1 1 1 2) (1 1 0 3) (1 2 0 2) (1 2 1 1)
    (1 2 2 0) (1 3 1 0) (1 3 0 1) (0 3 0 2) (0 3 1 1) (0 3 2 0) (0 2 3 0)
    (0 2 2 1) (0 2 1 2) (0 2 0 3) (0 1 1 3) (0 1 2 2) (0 1 3 1) (0 0 3 2)
    (0 0 2 3) (0 0 3 3) (0 1 3 2) (0 1 2 3) (0 2 1 3) (0 2 2 2) (0 2 3 1)
    (0 3 3 0) (0 3 2 1) (0 3 1 2) (0 3 0 3) (1 3 0 2) (1 3 1 1) (1 3 2 0)
    (1 2 3 0) (1 2 2 1) (1 2 1 2) (1 2 0 3) (1 1 1 3) (1 1 2 2) (1 1 3 1)
    (1 0 3 2) (1 0 2 3) (2 0 1 3) (2 0 2 2) (2 0 3 1) (2 1 3 0) (2 1 2 1)
    (2 1 1 2) (2 1 0 3) (2 2 0 2) (2 2 1 1) (2 2 2 0) (2 3 1 0) (2 3 0 1)
    (3 3 0 0) (3 2 1 0) (3 2 0 1) (3 1 0 2) (3 1 1 1) (3 1 2 0) (3 0 3 0)
    (3 0 2 1) (3 0 1 2) (3 0 0 3) (3 0 1 3) (3 0 2 2) (3 0 3 1) (3 1 3 0)
    (3 1 2 1) (3 1 1 2) (3 1 0 3) (3 2 0 2) (3 2 1 1) (3 2 2 0) (3 3 1 0)
    (3 3 0 1) (2 3 0 2) (2 3 1 1) (2 3 2 0) (2 2 3 0) (2 2 2 1) (2 2 1 2)
    (2 2 0 3) (2 1 1 3) (2 1 2 2) (2 1 3 1) (2 0 3 2) (2 0 2 3) (1 0 3 3)
    (1 1 3 2) (1 1 2 3) (1 2 1 3) (1 2 2 2) (1 2 3 1) (1 3 3 0) (1 3 2 1)
    (1 3 1 2) (1 3 0 3) (0 3 1 3) (0 3 2 2) (0 3 3 1) (0 2 3 2) (0 2 2 3)
    (0 1 3 3) (0 2 3 3) (0 3 3 2) (0 3 2 3) (1 3 1 3) (1 3 2 2) (1 3 3 1)
    (1 2 3 2) (1 2 2 3) (1 1 3 3) (2 0 3 3) (2 1 3 2) (2 1 2 3) (2 2 1 3)
    (2 2 2 2) (2 2 3 1) (2 3 3 0) (2 3 2 1) (2 3 1 2) (2 3 0 3) (3 3 0 2)
    (3 3 1 1) (3 3 2 0) (3 2 3 0) (3 2 2 1) (3 2 1 2) (3 2 0 3) (3 1 1 3)
    (3 1 2 2) (3 1 3 1) (3 0 3 2) (3 0 2 3) (3 0 3 3) (3 1 3 2) (3 1 2 3)
    (3 2 1 3) (3 2 2 2) (3 2 3 1) (3 3 3 0) (3 3 2 1) (3 3 1 2) (3 3 0 3)
    (2 3 1 3) (2 3 2 2) (2 3 3 1) (2 2 3 2) (2 2 2 3) (2 1 3 3) (1 2 3 3)
    (1 3 3 2) (1 3 2 3) (0 3 3 3) (1 3 3 3) (2 2 3 3) (2 3 3 2) (2 3 2 3)
    (3 3 1 3) (3 3 2 2) (3 3 3 1) (3 2 3 2) (3 2 2 3) (3 1 3 3) (3 2 3 3)
    (3 3 3 2) (3 3 2 3) (2 3 3 3) (3 3 3 3))
  (collect-enum *d44*)))

(defparameter *m44* (make-multi-diagonal-enumerator (list *e4* *e4* *e4* *e4*)))
(defparameter *m34* (make-multi-diagonal-enumerator (list *e4* *e4* *e4*)))
(defparameter *m24* (make-multi-diagonal-enumerator (list *e4* *e4*)))

;; (equal
;;  (mapcar #'reverse (enum:collect-enum (iterated-binary-micha *e3* *e3* *e3*)))
;;  (enum:collect-enum
;;   (enum::make-multi-diagonal-enumerator (list *e3* *e3* *e3*))))

;; (assert
;;  (equal
;;   (mapcar #'reverse (collect-enum (iterated-binary-micha *e4* *e4* *e4* *e4*)))
;;   (collect-enum
;;    (make-multi-diagonal-enumerator (list *e4* *e4* *e4* *e4*)))))

(defun multi-ways (e)
  (map 'list #'way (underlying-enumerators e)))

(loop with e = (make-multi-diagonal-enumerator (list *e3* *e3* *e3*))
      while (enum:next-element-p e)
      for elem := (enum:next-element e)
      do
	 (print (list elem (multi-ways-to-string (multi-ways e)))))

(loop with e = (make-nary-diagonal-steps-enumerator (list *e3* *e3* *e3*))
      while (enum:next-element-p e)
      for elem := (enum:next-element e)
      do
	 (print (list elem
		      (reverse elem)
		      (ways-string e)
		      (multi-ways-to-string (iterated-binary-ways e)))))

;; (assert
;;  (equal
;;   (mapcar 'reverse
;;           (enum:collect-enum
;; 	   (iterated-binary-micha *e3* *e3* *e3*)))
;;   (enum:collect-enum
;;    (enum::make-multi-diagonal-enumerator (list *e3* *e3* *e3*)))))

;; (assert
;;  (equal
;;   (mapcar 'reverse
;;           (enum:collect-enum
;; 	   (iterated-binary-micha *e3* *e3* *e3* *e3*)))
;;   (enum:collect-enum
;;    (make-multi-diagonal-enumerator (list *e3* *e3* *e3* *e3*)))))

;; (assert
;;  (equal
;;   (mapcar 'reverse
;;           (enum:collect-enum
;; 	   (iterated-binary-micha *e4* *e4* *e4* *e4*)))
;;   (enum:collect-enum
;;    (make-multi-diagonal-enumerator (list *e4* *e4* *e4* *e4*)))))

(defparameter *p22* (make-binary-diagonal-steps-enumerator *e2* *e2*))
(assert (equal (collect-enum *d22*) (collect-enum *p22*)))

(defparameter *p33*  (make-binary-diagonal-steps-enumerator
		      *e3*
		      (make-binary-diagonal-steps-enumerator *e3* *e3*)
		      :fun #'cons))

(defparameter *p34*  (make-binary-diagonal-steps-enumerator
		      *e4*
		      (make-binary-diagonal-steps-enumerator *e4* *e4*)
		      :fun #'cons))

(loop with e = (init-enumerator *p34*)
      while (enum:next-element-p e)
      for elem := (enum:next-element e)
      do
	 (print (list elem
		      (ways-string e))))

(defparameter *p44*
  (make-binary-diagonal-steps-enumerator
   *e4*
   (make-binary-diagonal-steps-enumerator
    *e4*
    (make-binary-diagonal-steps-enumerator *e4* *e4*) :fun #'cons)
   :fun #'cons))

(defparameter *p3n*
  (make-binary-diagonal-steps-enumerator
   *en*
   (make-binary-diagonal-steps-enumerator
    *en* *en*)
   :fun #'cons))

(defparameter *p4n*
  (make-binary-diagonal-steps-enumerator
   *en*
   (make-binary-diagonal-steps-enumerator
    *en*
    (make-binary-diagonal-steps-enumerator
     *en* *en*)
    :fun #'cons)
   :fun #'cons))

;; (defparameter *d34*  (make-diagonal-steps-enumerator
;; 		      *e4*
;; 		      (make-diagonal-steps-enumerator *e4* *e4*)
;; 		      :fun #'cons))

(assert (equal (collect-enum *d33*) (collect-enum *p33*)))

;; (defparameter *d35*  (make-diagonal-steps-enumerator
;; 		      *e5*
;; 		      (make-diagonal-steps-enumerator *e5* *e5*)
;; 		      :fun #'cons))

(assert (equal
	 (collect-enum *p34*)
	 '((0 0 0) (1 0 0) (0 1 0) (0 0 1) (0 0 2) (0 1 1) (0 2 0) (1 1 0) (1 0 1)
	   (2 0 0) (3 0 0) (2 1 0) (2 0 1) (1 0 2) (1 1 1) (1 2 0) (0 3 0) (0 2 1)
	   (0 1 2) (0 0 3) (0 1 3) (0 2 2) (0 3 1) (1 3 0) (1 2 1) (1 1 2) (1 0 3)
	   (2 0 2) (2 1 1) (2 2 0) (3 1 0) (3 0 1) (3 0 2) (3 1 1) (3 2 0) (2 3 0)
	   (2 2 1) (2 1 2) (2 0 3) (1 1 3) (1 2 2) (1 3 1) (0 3 2) (0 2 3) (0 3 3)
	   (1 3 2) (1 2 3) (2 1 3) (2 2 2) (2 3 1) (3 3 0) (3 2 1) (3 1 2) (3 0 3)
	   (3 1 3) (3 2 2) (3 3 1) (2 3 2) (2 2 3) (1 3 3) (2 3 3) (3 3 2) (3 2 3)
	   (3 3 3))))
