;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; accu-mixin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass accu-mixin ()
  ((accu :accessor accu :initform '()))
  (:documentation "enumerator with infinite memory"))

(defmethod show-enumerator :after ((e accu-mixin))
  (format t "accu: ~A~%" (accu e)))

(defmethod print-object :after ((e accu-mixin) stream)
  (format stream "~A" (accu e)))

(defmethod init-enumerator :after ((e accu-mixin))
  (setf (accu e) '()))

(defmethod next-element :around ((e accu-mixin))
  (let ((element (call-next-method)))
    (push element (accu e))
    element))
