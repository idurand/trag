;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; append enumerator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; slot next-elements is bound with the list currently enumerated
;;; unbound when the next list is not yet fetched
;;; NIL when there is no more list to fetch

(defclass append-enumerator (unary-relying-enumerator)
  ((next-elements :accessor next-elements))
  (:documentation "the unary operator must enumerate lists"))

(defmethod show-enumerator :after ((e nthcdr-enumerator))
  (format t "next-elements: ~A~%" (next-elements e)))

(defgeneric make-append-enumerator (abstract-enumerator)
  (:method ((e abstract-enumerator))
    (init-enumerator
     (make-instance 'append-enumerator :enum (copy-enumerator e)))))

(defmethod copy-enumerator ((e append-enumerator))
  (make-append-enumerator (enum e)))

(defgeneric skip-to-next-list (append-enumerator)
  (:method ((e append-enumerator))
    (setf (next-elements e) '())
    (loop
      until (next-elements e)
      while (next-element-p (enum e))
      do (setf (next-elements e) (next-element (enum e))))))

(defmethod init-enumerator :after ((e append-enumerator))
  (slot-makunbound e 'next-elements))

(defmethod next-element-p ((e append-enumerator))
  (unless (slot-boundp e 'next-elements) (skip-to-next-list e))
  (not (null (next-elements e))))

(defmethod next-element ((e append-enumerator))
  (prog1 (pop (next-elements e))
    (unless (next-elements e) (slot-makunbound e 'next-elements))))
