;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defclass bidirectional-leveled-enumerator (way-product-mixin binary-relying-enumerator) ())

(defgeneric make-binary-bidirectional-leveled-enumerator (enum1 enum2 &key fun)
  (:method ((enum1 abstract-enumerator) (enum2 abstract-enumerator) &key (fun #'list))
    (init-enumerator
     (make-instance 'bidirectional-leveled-enumerator
		    :fun fun
		    :enum1 (make-bidirectional-enumerator enum1)
		    :enum2 (make-bidirectional-enumerator enum2))))
  (:method ((enum1 abstract-enumerator) (enum2 bidirectional-leveled-enumerator) &key (fun #'list))
    (init-enumerator
     (make-instance 'bidirectional-leveled-enumerator
  		    :fun fun
  		    :enum1 (make-bidirectional-enumerator enum1)
  		    :enum2 (copy-enumerator enum2)))))

(defmethod copy-enumerator ((enum bidirectional-leveled-enumerator))
  (make-binary-bidirectional-leveled-enumerator (enum1 enum) (enum2 enum) :fun (fun enum)))

(defun sliding-step (enum1 enum2 way)
  (assert (or (next-element-p enum1) (next-element-p enum2)))
  
  (if (next-element-p enum2)   ;; the one which can move moves
      ;;      (progn (assert (= (way enum2) way)) (way-next-element way enum2))
      (way-next-element way enum2)
      (way-next-element way enum1))
  (invert-way enum1) ;; everybody changes way
  (invert-way enum2))

(defun corner-step (enum1 enum2 way)
  (when (plusp (* way (way enum1)))
    ;; put in enum1 the one that goes in direction -way
    (psetf enum1 enum2 enum2 enum1))
  ;; enum1 is now the one that goes in direction -way
  (invert-way enum1) ;; enum1 will move in direction way
  (next-element enum1)   ;; enum2 will move in direction -way
  (invert-way enum2))

(defmethod way-next-element ((way integer) (enum bidirectional-leveled-enumerator))
  (assert (way-next-element-p way enum))
  (let* ((enum1 (enum1 enum))
	 (enum2 (enum2 enum))
	 (next1 (next-element-p enum1))
	 (next2 (next-element-p enum2)))
    (cond
      ((not (initialized-p enum))
       (next-element enum1)
       (next-element enum2)
       (if (minusp way) (invert-way enum1) (invert-way enum2))
       (setf (initialized-p enum) t))
      ((and next2 (minor-step-p enum2)) ;; lower-level minor step
       (next-element enum2))
      ((and next2 next1) ;; minor-step on level
       (next-element enum1) (next-element enum2))
      ((not (or next1 next2)) ;; major step
       (corner-step enum1 enum2 way))
      (t (sliding-step enum1 enum2 way))))
  (latest-element enum))

(defgeneric make-nary-recursive-bidirectional-leveled-enumerator (enumerators &key fun)
  (:method ((enums null) &key fun)
    (declare (ignore fun))
    (make-bidirectional-enumerator (make-enumerator-nil)))
  (:method ((enumerators list) &key (fun #'cons))
    (if (endp (cdr enumerators))
	(make-funcall-enumerator #'list (car enumerators))
	(if (endp (cddr enumerators))
	    (make-binary-bidirectional-leveled-enumerator
	     (car enumerators) (cadr enumerators) :fun #'list)
	    (make-binary-bidirectional-leveled-enumerator
	     (car enumerators)
	     (make-nary-recursive-bidirectional-leveled-enumerator (cdr enumerators) :fun fun)
	     :fun fun)))))

(defmethod previous-element-p ((enum bidirectional-leveled-enumerator))
  (flip-way enum)
  (prog1 (next-element-p enum)
    (flip-way enum)))

(defmethod previous-element ((enum bidirectional-leveled-enumerator))
  (flip-way enum)
  (prog1
      (next-element enum)
    (flip-way enum)))
