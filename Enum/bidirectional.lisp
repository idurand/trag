;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand, Michael Raskin

(in-package :enum)

(defclass bidirectional-enumerator (way-mixin unary-relying-enumerator)
  ((latest-element :initform nil :accessor latest-element)
   (latest-element-p :initform nil :accessor latest-element-p)
   (past-elements :initform nil :accessor past-elements)
   (future-elements :initform nil :accessor future-elements)))

(defmethod show-enumerator :after ((enum bidirectional-enumerator))
  (format
   t
   "past-elements: ~A~%future-elements: ~A~%latest-element: ~A~%latest-element-p: ~A~%"
   (past-elements enum) (future-elements enum)
   (latest-element enum) (latest-element-p enum)))

(defmethod init-enumerator :after ((enum bidirectional-enumerator))
  (setf (way enum) 1
	(latest-element enum) nil
	(latest-element-p enum) nil
	(past-elements enum) nil
	(future-elements enum) nil)
  )

(defgeneric make-bidirectional-enumerator (enum)
  (:method ((enum way-mixin))
    (make-bidirectional-enumerator (enum enum)))
  (:method ((enum bidirectional-enumerator))
    (make-bidirectional-enumerator (enum enum)))
  (:method ((enum abstract-enumerator))
    (init-enumerator
     (make-instance
      'bidirectional-enumerator :enum (copy-enumerator enum)))))

(defgeneric bidirectional-p (enumerator)
  (:method ((e abstract-enumerator)) nil)
  (:method ((e way-mixin)) t)
  (:documentation "true if E is bidirectional"))

(defmethod copy-enumerator ((e bidirectional-enumerator))
  (make-bidirectional-enumerator (enum e)))

(defgeneric way-next-element-p (way bidirectional-enumerator)
  (:method ((way integer) (enum bidirectional-enumerator))
    (case way
      (-1 (not (endp (past-elements enum))))
      (1 (or
	  (not (endp (future-elements enum)))
	  (next-element-p (enum enum)))))))

(defgeneric way-next-element (way bidirectional-enumerator)  
  (:method ((way integer) (enum bidirectional-enumerator))
    (let ((element
	    (case way
	      (-1 (pop (past-elements enum)))
	      (1 (if (future-elements enum)
		     (pop (future-elements enum))
		     (next-element (enum enum)))))))
      (if (latest-element-p enum)
	  (if (minusp way)
	      (push (latest-element enum) (future-elements enum))
	      (push (latest-element enum) (past-elements enum)))
	  (setf (latest-element-p enum) t))
      (setf (latest-element enum) element)
      element)))

(defgeneric visualize-enumerator-state (enum)
  (:method ((enum bidirectional-enumerator))
    (format t "~a~a~a"
	    (if (past-elements enum) "." "|")
	    (if (latest-element-p enum)
		(case (signum (way enum))
		  (-1 "<")
		  (+1 ">"))
		(case (signum (way enum))
		  (-1 "{")
		  (+1 "}")))
	    (if (or (future-elements enum)
		    (next-element-p (enum enum)))
		"." "|"))))

(defmethod show-enumerator :after ((enum bidirectional-enumerator))
  (format t "latest-element: ~A~%" (latest-element enum))
  (format t "latest-element-p: ~A~%" (latest-element-p enum))
  (format t "past-elements: ~A~%" (past-elements enum))
  (format t "future-elements: ~A~%" (future-elements enum)))

(defun test-bidirectional-enumerator ()
  (let ((e (make-bidirectional-enumerator (make-inductive-enumerator 0 #'1+))))
    (assert (= 0 (next-element e)))
    (assert (= 1 (latest-element e)))
    (assert (latest-element-p e))))
