;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; cartesian product (non diagonal)
;;; if one of the enumerators is empty we get stuck in it
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass enumerator-cartesian (nary-relying-enumerator fun-mixin) ())

(defgeneric make-cartesian-enumerator (enums &key fun)
  (:method ((enums null) &key fun)
    (declare (ignore fun))
    (make-empty-enumerator))
  (:method :around ((enums list) &key fun)
    (if (null (cdr enums))
	(make-funcall-enumerator fun (car enums))
	(call-next-method)))
  (:method ((enums list) &key (fun #'list))
    (let ((v (map 'vector #'make-memo-enumerator enums)))
      (setf (aref v (1- (length v))) (enum (aref v (1- (length v)))))
      ;; no memo for the last one
      (init-enumerator
       (make-instance 'enumerator-cartesian :underlying-enumerators v :fun fun)))))

(defmethod copy-enumerator ((e enumerator-cartesian))
  (let ((enums (coerce (underlying-enumerators e) 'list)))
    (make-cartesian-enumerator
     (append (mapcar #'enum (butlast enums)) (last enums))
     :fun (fun e))))

(defmethod next-element-p ((e enumerator-cartesian))
  (every #'next-element-p (underlying-enumerators e)))

(defmethod next-element ((e enumerator-cartesian))
  (let ((enums (underlying-enumerators e)))
    (prog1 (apply (fun e) (map 'list (lambda (ei) (next-element ei)) enums))
      (let ((index (1- (length enums))))
	(unless (next-element-p (enum-i e index))
	  ;; if none loop until we can start again
	  (loop until (next-element-p (enum-i e index))
		until (zerop index)
		do (init-enumerator (enum-i e index))
		do (unset-memo-element (enum-i e (decf index)))))))))
