;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defclass nthcdr-enumerator (unary-relying-enumerator)
  ((nb-cdr :type integer :reader nb-cdr :initarg :nb-cdr))
  (:documentation
   "enumerator that enumerates the same elements \
    as its underlying enumerator except that it skips \
    the nb-cdr first values"))

(defgeneric make-nthcdr-enumerator (nb-cdr enumerator)
  (:method ((nb-cdr integer) (e abstract-enumerator))
    (init-enumerator
     (make-instance
      'nthcdr-enumerator :enum (copy-enumerator e) :nb-cdr nb-cdr)))
  (:method ((nb-cdr integer) (e list-enumerator))
    (make-list-enumerator (nthcdr nb-cdr (initial-list e)))))

(defmethod copy-enumerator ((e nthcdr-enumerator))
  (make-nthcdr-enumerator (nb-cdr e) (enum e)))

(defmethod init-enumerator :after((e nthcdr-enumerator))
  (let ((se (enum e)))
    (init-enumerator se)
    (loop
      repeat (nb-cdr e)
      while (next-element-p se)
      do (next-element se))))

(defgeneric make-cdr-enumerator (enumerator)
  (:method ((e abstract-enumerator)) (make-nthcdr-enumerator 1 e)))
