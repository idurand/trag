;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defgeneric make-k-arrangement-enumerator (set k)
  (:documentation "enumerator of arrangements (order matters) of k elements of SET")
  (:method ((set list) (k integer))
  (if (= 1 k)
      (make-list-enumerator (mapcar #'list set))
      (make-sequential-enumerator
       (mapcar
	(lambda (x)
	  (make-funcall-enumerator
	   (lambda (y) (cons x y))
	   (make-k-combination-enumerator (remove x set) (1- k))))
	set)))))

(defgeneric make-k-combination-enumerator (set k)
  (:documentation "enumerator of combinations of k elements of SET")
  (:method ((set list) (k integer))
    (let ((n (length set)))
      (cond
	((> k n) (make-empty-enumerator))
	((= n k) (make-one-value-enumerator set))
	((= 1 k) (make-list-enumerator (mapcar #'list set)))
	(t
	 (let ((first (first set))
	       (rest (rest set)))
	   (make-sequential-enumerator
	    (list
	     (make-funcall-enumerator
	      (lambda (y) (cons first y))
	      (make-k-combination-enumerator rest (1- k)))
	     (make-k-combination-enumerator rest k)))))))))
