;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; enumerating pairs and lists
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun make-enumerator-nil (&key (n 1))
  (make-list-enumerator (make-list n)))

(defun make-enumerator-cons (enum1 enum2)
  (make-binary-diagonal-product-enumerator enum1 enum2 :fun #'cons))
;;  (make-binary-bidirectional-leveled-enumerator enum1 enum2 :fun #'cons))

(defun make-enumerator-list (&rest enums)
  (case (length enums)
    (0 (make-enumerator-nil))
    (1 (make-funcall-enumerator #'list (first enums)))
    (t (make-nary-diagonal-product-enumerator enums))))

;; (t (make-nary-recursive-bidirectional-leveled-enumerator enums))))
;; leveled-enumerators take too much time
;; should be used when we have infinite enumerators?

