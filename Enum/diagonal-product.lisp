;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; basic binary diagonal: no way
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass diagonal-product-enumerator (binary-relying-enumerator product-mixin) ())

(defgeneric make-binary-diagonal-product-enumerator (enum1 enum2 &key fun)
  (:method ((enum1 abstract-enumerator) (enum2 abstract-enumerator) &key (fun #'list))
    (init-enumerator
     (make-instance 'diagonal-product-enumerator
		    :fun fun
		    :enum1 (make-bidirectional-enumerator enum1)
		    :enum2 (make-bidirectional-enumerator enum2)))))

(defmethod copy-enumerator ((enum diagonal-product-enumerator))
  (make-binary-diagonal-product-enumerator
   (enum1 enum) (enum2 enum) :fun (fun enum)))

(defun bp-sliding-step (enum1 enum2)
  (assert (or (true-next-element-p enum1) (true-next-element-p enum2)))
  (if (next-element-p enum2)    ;; the one which can move moves
      (true-next-element enum2)
      (true-next-element enum1))
  (invert-way enum1)
  (invert-way enum2))

(defun bp-corner-step (enum1 enum2)
  (when (plusp (way enum1))
    ;; put in enum1 the one that goes in direction -1
      (psetf enum1 enum2 enum2 enum1))
  ;; enum1 is now the one that goes in direction -1
  (invert-way enum1) ;; enum1 will move in direction 1
  (next-element enum1)   ;; enum2 will move in direction -1
  (invert-way enum2))


(defmethod next-element ((enum diagonal-product-enumerator))
;;  (print '(next-element diagonal-product-enumerator))
  (assert (next-element-p enum))
  (let* ((enum1 (enum1 enum))
	 (enum2 (enum2 enum))
	 (next1 (next-element-p enum1))
	 (next2 (next-element-p enum2)))
    (cond
      ((not (initialized-p enum))
       (setf (initialized-p enum) t)
       (next-element enum1)
       (next-element enum2)
       (invert-way enum2))
      ((and next2 (minor-step-p enum2)) ;; lower-level minor step
       (next-element enum2))
      ((and next1 next2) ;; minor-step on level
       (next-element enum1) (next-element enum2))
      ((not (or next1 next2)) ;; major step
       (bp-corner-step enum1 enum2))
      (t (bp-sliding-step enum1 enum2))))
  (latest-element enum))

(defgeneric make-nary-diagonal-product-enumerator (enumerators)
  (:method ((enums null)) (make-enumerator-nil))
  (:method ((enumerators list))
    (if (endp (cdr enumerators))
	(car enumerators)
	(if (endp (cddr enumerators))
	    (make-binary-diagonal-product-enumerator (car enumerators) (cadr enumerators))
	    (make-binary-diagonal-product-enumerator
	     (car enumerators)
	     (make-nary-diagonal-product-enumerator (cdr enumerators))
	     :fun #'cons)))))
