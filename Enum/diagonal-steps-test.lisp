;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand, Michael Raskin

(in-package :enum)

(defun test-diagonal-enumerator-creator
    (creator-function list-lengths
     &key function-extra-args skip-output output-cleaner)
  (let* ((lists (loop for n in list-lengths
		      collect
		      (loop for i from 0 to (1- n) collect i)))
	 (enumerators (loop for l in lists collect
					   (make-list-enumerator l)))
	 (multi-enumerator
	  (copy-enumerator  (apply creator-function enumerators function-extra-args)))
	 (raw-output (collect-enum multi-enumerator))
	 (output (if output-cleaner
		     (mapcar output-cleaner raw-output) raw-output))
	 (uniq-output (remove-duplicates output :test 'equal))
	 (duplicate-count (- (length output) (length uniq-output)))
	 (sums (mapcar (lambda (l) (reduce '+ l)) output))
	 (volume (reduce '* list-lengths)))
    (when (> duplicate-count 0) (error "Found ~a duplicates." duplicate-count))
    (loop
      for previous := -1 then x
      for previous-tuple := nil then tuple
      for x in sums
      for tuple in output
      unless (<= previous x)
	do (error "Sum decreased from ~a to ~a when going from ~a to ~a."
		  previous x previous-tuple tuple))
    (unless (= (length output) volume)
      (error "Listed only ~a tuples out of ~a." (length output) volume))
    (if skip-output t raw-output)))

;; (enum::test-diagonal-enumerator-creator (labels ((create (a b &rest more) (enum::make-diagonal-steps-enumerator a (if more (apply #'create b more) b) :fun (if more 'cons 'list)))) (lambda (args) (apply #'create args))) (list 3 3 3))
;; (enum::test-diagonal-enumerator-creator 'enum::make-multi-diagonal-enumerator (list 3 3 3))
