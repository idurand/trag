;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand, Michael Raskin

(in-package :enum)

(defclass diagonal-steps-enumerator (way-product-mixin binary-relying-enumerator)
  ())

;; This enumerator is expected to be a part of multidimensional cartesian product
;; It requires bidirectional enumerators at the leaf level
;; It has two kinds of steps: minor and major
;; Minor steps are steps that do not change the sum of indices in the underlying
;; enumerators (i.e. 1,1,0 -> 1,0,1 or 1,1,0 -> 2,0,0 would be minor steps)
;; Major steps do change the sum of indices

;; Like in cons, we suppose that the first argument is a single thing and the
;; second argument might be a tail of things
;; So we consider all the steps of enum1 major steps, but ask enum2 whether
;; a step is a minor or major step

(defgeneric make-diagonal-steps-enumerator (enum1 enum2 &key fun)
  (:method ((enum1 abstract-enumerator) (enum2 abstract-enumerator) &key (fun #'list))
    (init-enumerator
     (make-instance 'diagonal-steps-enumerator
		    :fun fun
		    :enum1 (make-bidirectional-enumerator enum1)
		    :enum2 (make-bidirectional-enumerator enum2))))
  (:method ((enum1 abstract-enumerator) (enum2 diagonal-steps-enumerator) &key (fun #'list))
    (init-enumerator
     (make-instance 'diagonal-steps-enumerator
  		    :fun fun
  		    :enum1 (make-bidirectional-enumerator enum1)
  		    :enum2 (copy-enumerator enum2)))))

(defmethod copy-enumerator ((enum diagonal-steps-enumerator))
  (make-diagonal-steps-enumerator (enum1 enum) (enum2 enum) :fun (fun enum)))

;; Changing direction means we change direction of both the major and the minor
;; steps
;; we change both top-level direction and the direction of underlying enumerators

;; A minor step means either a minor step in an underlying enumerator, or
;; major steps in the opposite direction
(defmethod minor-step-p ((enum diagonal-steps-enumerator))
  (and (next-element-p (enum2 enum))
       (or (next-element-p (enum1 enum)) (minor-step-p (enum2 enum)))))

;; If we already know that both sub-enumerators are non-empty, it is enough
;; if one of them has a (true) next element to make a major step.
;; Otherwise we need to make sure both of them are non-empty.

(defmethod way-next-element-p ((way integer) (enum diagonal-steps-enumerator))
  (if (initialized-p enum)
      (or
       (with-way ((enum1 enum) way) (next-element-p (enum1 enum)))
       (with-way ((enum2 enum) way) (next-element-p (enum2 enum))))
      (and
       (with-way ((enum1 enum) way) (next-element-p (enum1 enum)))
       (with-way ((enum2 enum) way) (next-element-p (enum2 enum))))))

;; We are blocked in a corner (bottom-right or top-left)
;; Both enumerators are blocked w.r.t. their current direction, but they are 
;; headed in the opposite direction, so exactly one of them should be able to
;; go in the global direction «way»
;;
;; Then they both change the direction to do the minor steps on the new level
;; There is a subtlety for the second enumerator (the one capable of minor
;; steps itself): if it has moved, only its top-level direction is changed,
;; because on the new level it is ready to do the minor steps;; otherwise it is
;; truly flipped, so that it can first go back through all the minor steps on
;; the current level
(defun diagonal-steps-enumerator-blocked-corner-step (enum1 enum2 way)
  (if (with-way (enum2 way) (next-element-p enum2))
      (progn
	(with-way (enum2 way) (next-element enum2))
	;; new level, need to go through the new minor steps before
	;; going back
	(invert-way enum2))
      (progn
	(with-way (enum1 way)
	  (next-element enum1))
	;; old level, go through the old minor steps in the reverse order
	(flip-way enum2)))
  (flip-way enum1))

;; One of the two enumerators is blocked, but not the other, we have hit a wall
;; at 45 degree angle, we just «slip along the wall» for one step
;;
;; Note that we go in the true forward direction, even if the unblocked
;; enumerator was currently going backward
;;
;; See diagonal-steps-enumerator-blocked-corner-step for the explanation of the
;; reversal of enum2
(defun diagonal-steps-enumerator-slipping-step (enum1 enum2 way)
  (if (next-element-p enum2)
      (progn
	(with-way (enum2 way) (next-element enum2))
					; new level, need to go through the new minor steps before
					; going back
	(invert-way enum2))
      (progn
	(with-way (enum1 way) (next-element enum1))
					; old level, go through the old minor steps in the reverse order
	(flip-way enum2)))
  (flip-way enum1))

(defmethod latest-element ((enum diagonal-steps-enumerator))
  (funcall (fun enum) (latest-element (enum1 enum)) (latest-element (enum2 enum))))

;; If there is no next element, we will ask an underlying enumerator to move
;; in an impossible direction, which will probably signal an error
(defmethod way-next-element ((way integer) (enum diagonal-steps-enumerator))
  (let* ((enum1 (enum1 enum))
	 (enum2 (enum2 enum))
	 (next1 (next-element-p enum1))
	 (next2 (next-element-p enum2)))
    (cond ; We need to calculate initial elements
      ((not (initialized-p enum))
       (next-element enum1)
       (next-element enum2)
       (if (minusp way) (flip-way enum1) (flip-way enum2))
       (setf (initialized-p enum) t))
      ;; We are not going anywhere
      ((zerop way))
      ;; There is a lower-level minor step
      ((and next2 (minor-step-p enum2))
       (next-element enum2))
      ;; We can do a minor step on our own level
      ((and next2 next1) (next-element enum1) (next-element enum2))
                ;;;;;;;;;;;;;;;;;;;;;;;
      ;; We are out of trivial cases, we need to do a major step
                ;;;;;;;;;;;;;;;;;;;;;;;
      ;; We are blocked in a the corner (neither enumerator can go forward)
      ;; (0 1 2) x (0 1 2), the current state is (2,0) going (up,down)
      ((not (or next2 next1))
       (diagonal-steps-enumerator-blocked-corner-step enum1 enum2 way))
      (t (diagonal-steps-enumerator-slipping-step enum1 enum2 way)))
    (latest-element enum)))

(defgeneric make-nary-diagonal-steps-enumerator (enumerators &key fun)
  (:method ((enums null) &key fun)
    (declare (ignore fun))
    (make-bidirectional-enumerator (make-enumerator-nil)))
  (:method ((enumerators list) &key (fun #'cons))
    (if (endp (cdr enumerators))
	(car enumerators)
	(if (endp (cddr enumerators))
	    (make-diagonal-steps-enumerator
	     (car enumerators) (cadr enumerators) :fun #'list)
	    (make-diagonal-steps-enumerator
	     (car enumerators)
	     (make-nary-diagonal-steps-enumerator (cdr enumerators) :fun fun)
	     :fun fun)))))
