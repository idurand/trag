;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Enum.

(in-package :asdf-user)

(defsystem :enum
  :description "Enum: enumerators of sequences"
  :name "enum"
  :version "6.0"
  :author "Irène Durand"
  :serial t
  :components
  ((:file "package")
   (:file "enum")
   (:file "one-value")
   (:file "simple")
   (:file "vector")
   (:file "sequence")
   (:file "inductive")
   (:file "relying")
   (:file "cdr")
   (:file "append")
   (:file "lazy")
   (:file "sequential")
   (:file "union")
   (:file "parallel")
   (:file "map")
   (:file "accu")
   (:file "memo")
   (:file "cartesian-product")
   (:file "init-mixin")
   (:file "way-mixin")
   (:file "bidirectional")
   (:file "product-mixin")
   (:file "diagonal-steps")
   (:file "diagonal-steps-test")
   (:file "multi-diagonal-steps")
   (:file "diagonal-product")
   (:file "bidirectional-leveled")
   (:file "product")
   (:file "memoize")
   (:file "filter")
   (:file "no-duplicates")
   (:file "cons")
   (:file "series")
   (:file "flatten")
   (:file "combination")
   (:file "stream")
   (:file "file")))

(pushnew :enum *features*)
