;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defclass abstract-enumerator () ()
  (:documentation "class for all enumerators"))

(defgeneric next-element-p (enumerator)
  (:documentation "T if there is a next element"))

(defgeneric true-next-element-p (enumerator)
  (:documentation "T if there is a next element in the positive direction")
  (:method ((enumerator abstract-enumerator))
    (next-element-p enumerator)))

(defgeneric true-next-element (enumerator)
  (:documentation "next element in the positive direction")
  (:method ((enumerator abstract-enumerator))
    (next-element enumerator)))
  
(defgeneric next-element (enumerator)
  (:documentation "the next element"))

(defgeneric init-enumerator (enumerator)
  (:documentation "reinitialize and return ENUMERATOR")
  (:method ((e abstract-enumerator)) e))

(defgeneric copy-enumerator (enumerator)
  (:documentation "a re-initialized copy of ENUMERATOR"))

(defgeneric snapshot-enumerator (enumerator)
  (:documentation "a copy of ENUMERATOR in its current state"))

(defgeneric call-enumerator (enumerator)
  (:documentation
   "return as first value the next element produced by ENUMERATOR
    if it exists NIL otherwise
    as second value T if element was produced")
  (:method ((e abstract-enumerator))
    (if (next-element-p e)
	(values (next-element e) t)
	(values nil nil))))

(defclass fun-mixin () ((fun :initarg :fun :reader fun)))

(defgeneric show-enumerator (enum)
  (:method ((enum abstract-enumerator)) (format t "~A~%" enum)))

(defgeneric enumerator-nonempty-p (enum)
  (:method ((enum abstract-enumerator))
    (true-next-element-p (copy-enumerator enum))))

(defgeneric enumerator-empty-p (enum)
  (:method ((enum abstract-enumerator))
    (not (enumerator-nonempty-p enum))))

(defgeneric latest-element-p (enum)
  (:method ((enum abstract-enumerator)) nil)
  (:documentation "not the first element"))

(defgeneric latest-element (enum)
  (:method :before ((enum abstract-enumerator))
    (assert (latest-element-p enum)))
  (:documentation "the latest element"))

(defclass empty-enumerator (abstract-enumerator) ()
  (:documentation "empty enumerator"))

(defvar *empty-enumerator* (make-instance 'empty-enumerator))

(defun make-empty-enumerator () *empty-enumerator*)

(defmethod next-element-p ((e empty-enumerator)) nil)

(defmethod copy-enumerator ((e empty-enumerator)) e)
  
(defmethod next-element ((e empty-enumerator))
  (error "should not call next-element on empty-enumerator"))
