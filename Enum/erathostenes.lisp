;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; erathostenes sieve
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass erathostenes (unary-relying-enumerator)
  ((enum :type abstract-enumerator :accessor enum
	 :initform (make-inductive-enumerator 2 #'1+))))

(defmethod next-element ((e erathostenes))
  (let ((prime (next-element (enum e))))
    (setf
     (enum e)
     (make-instance
      'filter-enumerator
      :enum (enum e) :fun (lambda (n) (plusp (mod n prime)))))
    prime))

(defun make-erathostenes-enumerator ()
  (make-instance 'erathostenes))

