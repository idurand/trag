;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defun fibonacci-words-enumerator ()
  (make-funcall-enumerator
   #'first
   (make-inductive-enumerator
    '((a) (bc))
    (lambda (couple)
      (destructuring-bind (u v) couple
	(list v (append u v)))))))

(defun fibonacci-enumerator ()
  (make-funcall-enumerator
   #'first
   (make-inductive-enumerator
    '(0 1)
    (lambda (couple)
      (destructuring-bind (u v) couple
	(list v (+ u v)))))))
