;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defclass file-enumerator (stream-enumerator)
  ((filename :initarg :filename :reader filename)))

(defmethod init-enumerator ((e file-enumerator))
  (when (e-stream e)
    (close (e-stream e)))
  (setf (e-stream e) (open (filename e) :if-does-not-exist nil))
  (move-to-next-line e)
  e)

(defun make-file-enumerator (filename)
  (init-enumerator
   (make-instance 'file-enumerator :filename filename)))

