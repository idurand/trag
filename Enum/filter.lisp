;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; remove-if-not
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass filter-enumerator (unary-relying-enumerator fun-mixin)
  ((good-element :accessor good-element)))

(defgeneric skip-to-next (filter-enumerator)
  (:method ((e filter-enumerator))
    (let ((fun (fun e))
	  (enum (enum e)))
      (loop
	while (next-element-p enum)
	until (slot-boundp e 'good-element)
	do (let ((element (next-element enum)))
	     (when (funcall fun element)
	       (setf (good-element e) element)))))))

(defmethod next-element-p ((e filter-enumerator))
  (unless (slot-boundp e 'good-element)
    (skip-to-next e))
  (slot-boundp e 'good-element))
  
(defmethod next-element ((e filter-enumerator))
  (assert (next-element-p e))
  (prog1
      (good-element e)
    (slot-makunbound e 'good-element)))

(defgeneric make-filter-enumerator (enumerator filter-fun)
  (:method ((e abstract-enumerator) filter-fun)
    (init-enumerator 
     (make-instance
      'filter-enumerator
      :enum e
      :fun filter-fun))))

(defmethod copy-enumerator ((e filter-enumerator))
  (make-filter-enumerator
   (copy-enumerator (enum e))
   (fun e)))
