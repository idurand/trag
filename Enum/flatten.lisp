;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; concrete flatten enumerator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass flatten-enumerator (unary-relying-enumerator)
  ((current-enumerator :accessor current-enumerator :initarg :current-enumerator
:initform nil))
  (:documentation
   "enumerates sequentially the elements enumerated by the enumerators enumerated by the underlying
    enumerator"))

(defgeneric next-enumerator (flatten-enumerator))
(defmethod next-enumerator ((e flatten-enumerator))
  (setf (current-enumerator e)
	(if (next-element-p (enum e))
	    (init-enumerator (next-element (enum e)))
	    nil)))

(defgeneric move-to-next-enumerator (flatten-enumerator))
(defmethod move-to-next-enumerator ((e flatten-enumerator))
  (loop
    do (next-enumerator e)
    until 
    (or (null (current-enumerator e))
	(next-element-p (current-enumerator e))))
  (current-enumerator e))
 
(defun make-flatten-enumerator (enum)
  (init-enumerator
   (make-instance
    'flatten-enumerator
    :enum (copy-enumerator enum))))

(defmethod init-enumerator :after ((e flatten-enumerator))
  (move-to-next-enumerator e))

(defmethod copy-enumerator ((e flatten-enumerator))
  (make-flatten-enumerator
   (enum e)))

(defmethod next-element-p ((e flatten-enumerator))
  (let ((ce (current-enumerator e)))
    (and ce (next-element-p ce))))

(defmethod next-element ((e flatten-enumerator))
  (let ((ce (current-enumerator e)))
    (prog1
	(next-element ce)
      (unless (next-element-p ce)
	(move-to-next-enumerator e)))))
