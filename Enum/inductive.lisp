;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Inductive enumerators
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass inductive-enumerator (simple-enumerator fun-mixin)
  ((initial-value :initarg :initial-value
		  :accessor initial-value)
   (current-value :initarg :current-value
		  :accessor current-value)))

(defmethod show-enumerator :after ((e inductive-enumerator))
  (format t "current-value: ~A~%" (current-value e)))

(defmethod next-element-p ((e inductive-enumerator)) t)

(defmethod next-element ((e inductive-enumerator))
  (current-value e))

(defmethod next-element :after ((e inductive-enumerator))
  (setf (current-value e) (funcall (fun e) (current-value e))))

(defmethod init-enumerator :after ((e inductive-enumerator))
  (setf (current-value e) (initial-value e)))

(defun make-inductive-enumerator (initial-value fun &key current-value)
  (unless current-value (setf current-value initial-value))
  (make-instance 'inductive-enumerator
		 :fun fun
		 :initial-value initial-value
		 :current-value current-value))

(defmethod copy-enumerator ((e inductive-enumerator))
  (make-inductive-enumerator (initial-value e) (fun e)))

(defmethod snapshot-enumerator ((e inductive-enumerator))
  (make-inductive-enumerator
   (initial-value e)
   (fun e)
   :current-value (current-value e)))

(defclass mod-inductive-enumerator (inductive-enumerator)
  ((mod-fun :initarg :mod-fun :reader mod-fun)))

(defmethod next-element :after ((e mod-inductive-enumerator))
  (setf (current-value e) (funcall (mod-fun e) (current-value e))))

(defmethod init-enumerator :after ((e mod-inductive-enumerator))
  (setf (current-value e) (funcall (mod-fun e) (initial-value e))))

(defun make-mod-inductive-enumerator (initial-value fun mod-fun)
  (init-enumerator
   (make-instance
    'mod-inductive-enumerator
    :fun fun
    :mod-fun mod-fun
    :initial-value initial-value)))

(defun naturals-enumerator () (make-inductive-enumerator 0 #'1+))

