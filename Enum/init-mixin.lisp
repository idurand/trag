;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand, Michael Raskin

(in-package :enum)

(defclass init-mixin ()
  ((initialized-p :initform nil :accessor initialized-p)))

(defmethod show-enumerator :after ((e init-mixin))
  (format t "initialized-p:~A~%" (initialized-p e)))

(defmethod init-enumerator :after ((e init-mixin))
  (setf (initialized-p e) nil))
