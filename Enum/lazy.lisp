;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defun force (del-expr) (funcall del-expr))

(defclass lazy-enumerator (unary-relying-enumerator)
  ((enumerator-call :reader enumerator-call :initarg :enumerator-call)))

(defmethod init-enumerator :around ((e lazy-enumerator))
  (if (slot-boundp e 'enum)
      (call-next-method)
      e))

(defgeneric make-lazy-enumerator (enumerator-call)
  (:method (enumerator-call)
    (make-instance 'lazy-enumerator :enumerator-call enumerator-call)))

(defgeneric force-enumerator (lazy-enumerator)
  (:method ((e lazy-enumerator))
    (unless (slot-boundp e 'enum)
      (setf (slot-value e 'enum) (force (enumerator-call e)))
      (slot-makunbound e 'enumerator-call))))

(defmethod next-element-p :before ((e lazy-enumerator))
  (force-enumerator e))

(defmethod copy-enumerator ((e lazy-enumerator))
  (if (slot-boundp e 'enum)
      (copy-enumerator (enum e))
      (make-lazy-enumerator (enumerator-call e))))

(defmethod underlying-enumerators ((e lazy-enumerator))
  (if (slot-boundp e 'enum) (list (enum e)) '()))
