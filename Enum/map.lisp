;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; map-enumerator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass mapping-enumerator (unary-relying-enumerator fun-mixin)
  ((map-fun :initarg :map-fun :reader map-fun)))

(defun make-mapping-enumerator (fun map-fun e)
  (init-enumerator
   (make-instance
    'mapping-enumerator :fun fun :map-fun map-fun :enum (copy-enumerator e))))

(defmethod copy-enumerator ((e mapping-enumerator))
  (make-mapping-enumerator (fun e) (map-fun e) (enum e)))

(defmethod next-element-p ((e mapping-enumerator))
  (next-element-p (enum e)))

(defmethod next-element ((e mapping-enumerator))
  (funcall (map-fun e) (fun e) (next-element (enum e))))

(defun make-funcall-enumerator (fun e)
  (make-mapping-enumerator fun #'funcall e))

(defun make-apply-enumerator (fun e)
  (make-mapping-enumerator fun #'apply e))
