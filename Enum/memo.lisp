;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; memo enumerator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; if its state is defined returns always this state
;; otherwise calls the underlying enumerator
;; to set its state

(defclass memo-enumerator (unary-relying-enumerator)
  (memo-element)
  (:documentation "enumerator with one memory"))

(defmethod show-enumerator :after ((e memo-enumerator))
  (format t "memo-element: ~A~%"
	  (if (slot-boundp e 'memo-element) "unbound" (slot-value e 'memo-element))))

(defgeneric memo-element (memo-enumerator)
  (:method ((e memo-enumerator))
    (unless (slot-boundp e 'memo-element)
      (setf (slot-value e 'memo-element) (next-element (enum e))))
    (slot-value e 'memo-element)))

(defgeneric memo-element-p (memo-enumerator)
  (:method ((e memo-enumerator))
    (slot-boundp e 'memo-element)))

(defgeneric make-memo-enumerator (abstract-enumerator)
  (:method ((e abstract-enumerator))
    (init-enumerator (make-instance 'memo-enumerator :enum (copy-enumerator e)))))

(defmethod init-enumerator :after ((e memo-enumerator))
  (slot-makunbound e 'memo-element))

(defmethod next-element-p ((e memo-enumerator))
  (or (slot-boundp e 'memo-element) (next-element-p (enum e))))

(defmethod next-element ((e memo-enumerator))
  (unless (slot-boundp e 'memo-element)
    (setf (slot-value e 'memo-element) (next-element (enum e))))
  (slot-value e 'memo-element))

(defmethod copy-enumerator ((e memo-enumerator))
  (make-memo-enumerator (enum e)))

(defgeneric unset-memo-element (memo-enumerator)
  (:method ((e memo-enumerator))
    (slot-makunbound e 'memo-element)))
