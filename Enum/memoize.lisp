;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; memoize enumerator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; accumulates the values in a list
;; when done works as a list enumerator

(defclass memoize-enumerator (unary-relying-enumerator)
  ((done-p :initform nil :accessor done-p)
   (elements :accessor elements :initform nil))
  (:documentation "enumerator with unbounded memory"))

(defgeneric make-memoize-enumerator (abstract-enumerator))

(defmethod make-memoize-enumerator ((e list-enumerator))
  (warn "should not memoize a list-enumerator"))

(defmethod make-memoize-enumerator ((e abstract-enumerator))
  (init-enumerator
   (make-instance
    'memoize-enumerator
    :enum (copy-enumerator e))))

(defmethod init-enumerator :after ((e memoize-enumerator))
  (when (done-p e)
    (setf (slot-value e 'enum) (make-list-enumerator (nreverse (elements e)))))
  (setf (elements e) nil))
  
(defmethod next-element ((e memoize-enumerator))
  (let ((element (next-element (enum e))))
    (unless (done-p e)
      (push element (elements e)))
    element))

(defmethod next-element-p ((e memoize-enumerator))
  (let ((next (next-element-p (enum e))))
    (unless next
      (unless (done-p e)
	(setf (done-p e) t)))
    next))

(defmethod copy-enumerator ((e memoize-enumerator))
  (if (done-p e)
      (copy-enumerator (enum e))
      (make-memoize-enumerator (enum e))))
