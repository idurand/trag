;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand, Michael Raskin

(in-package :enum)

;; The code in this file is a kind of manual unrolling of iterating
;; the product in diagonal-steps.lisp

(defclass multi-diagonal-enumerator (way-product-mixin nary-relying-enumerator) ())

(defgeneric vector-product-enumerators (enums)
  (:method ((enums list))
    (coerce (loop for e in enums
                  for way := (expt -1 (1+ (length enums))) then (- way)
		  collect (make-bidirectional-enumerator e))
	    'vector)))

(defgeneric make-multi-diagonal-enumerator (enums &key fun)
  (:method ((enums list) &key (fun #'list))
    (when (endp enums)
      (return-from make-multi-diagonal-enumerator (make-empty-enumerator)))
    (when (endp (cdr enums))
      (return-from make-multi-diagonal-enumerator (copy-enumerator (car enums))))
    (init-enumerator
     (make-instance
      'multi-diagonal-enumerator
      :underlying-enumerators (vector-product-enumerators enums)
      :fun fun)))
  (:method ((enums vector) &key (fun #'list))
    (make-multi-diagonal-enumerator (coerce enums 'list) :fun fun)))

(defmethod copy-enumerator ((e multi-diagonal-enumerator))
  (make-multi-diagonal-enumerator (underlying-enumerators e) :fun (fun e)))

(defmethod next-element-p ((enum multi-diagonal-enumerator))
  (some
   (lambda (e) (with-way (e (way enum)) (next-element-p e)))
   (underlying-enumerators enum)))

(defmethod latest-element ((enum multi-diagonal-enumerator))
  (apply (fun enum) (map 'list #'latest-element (underlying-enumerators enum))))

;; Here we try to find out if a minor step is possible
;; We prefer the larger component of the minor step to be minimal,
;; and the smaller one to be maximal below the larger one
;; (this is just unrolling the logic of minor step search in diagonal-step)
;;
;; This is the function to find the earliest free-to-move enumerator
;; which has a smaller enumerator able to move in the opposite direction
;;
;; Note that the earlier enumerator may currently go in the opposite direction,
;; but during a major step inside the minor step this can be overriden
;;
;; We return multiple values: the index of the chosen enumerator
;; (or the count of enumerators in case of failure),
;; the direction needed (opposite of the found enumerator or initial one)
;; the smallest enumerator index that can move forward,
;; the smallest enumerator index that can move backward
(defun multi-diagonal-enumerator-find-for-minor (way enums)
  (loop with first-can-forward := -1
        with first-can-backward := -1
        with len := (length enums)

        for k from 0 to (1- len)
        for e := (elt enums k)
        for w := (way e)

	;; Can go in true directions (criterion for major steps)
        for this-can-forward := (true-next-element-p e)
        for this-can-backward := (true-previous-element-p e)

	;; Free to go in the current direction
        for free := (if (plusp w) this-can-forward this-can-backward)

	;; Record the earliest that can be forced to go in the true directions
        when (and (= -1 first-can-forward) this-can-forward)
	  do (setf first-can-forward k)
        when (and (= -1 first-can-backward) this-can-backward)
	  do (setf first-can-backward k)
	     
	     ;; If we are free to move and we can do a major step to the left,
	     ;; we have found our minor step
        when (and free
                  (< -1 (if (plusp w) first-can-backward first-can-forward) k))
	  return (values k (- w) first-can-forward first-can-backward)
	;; Failure, need to do a global major step
        finally (return (values len way first-can-forward first-can-backward))))

;; Find one or two positions of enumerators to advance
;; If a minor step can be made, we find the minimal possible largest component
;; of a minor step and perform a major step to the left of it
;; Otherwise we just perform a major step
(defun multi-diagonal-enumerator-find-step-pair (way enums)
  (let ((res nil))
    (multiple-value-bind (start-position force-way first-can-forward first-can-backward)
	(multi-diagonal-enumerator-find-for-minor way enums)
					; In case of globally-minor step, record the larger component
      (unless (= (length enums) start-position) (push start-position res))
      (loop
        for k downfrom (1- start-position)
        for e := (elt enums k)
        for way := (way e)

	;; more or less: we have not yet exhausted our options
        for left-not-finished := (> k (if (plusp force-way)
					  first-can-forward first-can-backward))

        for free := (next-element-p e)
	;; If the enumerator is free to move, this must be the desired
	;; direction: in the minor step case it would replace the larger
	;; component otherwise, in the slipping-along-the-wall case we can
	;; always move the non-blocked enumerator
	;;
	;; If this is our last chance, we just have to grab it
        for perform-move := (or free (not left-not-finished))

	;; return just to break the loop, but semantics if that we produce the
	;; result of the loop via push
        when perform-move return (push k res))
      (reverse res))))

;; Apply the decision to make a major or a minor step
(defun multi-diagonal-enumerator-do-steps (way enums positions)
  (if (= (length positions) 1)
    ;; a globally major step, we know that the enumerator should do
    ;; a true forward step
    (let ((e (elt enums (first positions))))
      (with-way (e way) (next-element e)))
    ;; a minor step, the first enumerator goes where it wants, the second
    ;; has to compensate
    (let ((e1 (elt enums (first positions)))
          (e2 (elt enums (second positions))))
      (next-element e1)
      (with-way (e2 (- (way e1))) (next-element e2))))
  ;; A major step requires a change in direction of everything,
  ;; a minor step only changes directions below the large enumerator
  (loop for k from 0 to (if (second positions)
                          (1- (first positions))
                          (1- (length enums)))
        for e := (elt enums k)
        do (flip-way e)))

(defmethod next-element ((enum multi-diagonal-enumerator))
  (let ((enums (underlying-enumerators enum)))
    (block nil
      ;; simple cases first
      (unless (initialized-p enum)
        (map nil
	     (lambda (e)
	       (next-element e))
	     enums)
        (setf (initialized-p enum) t)
	(return))
      ;; calculate the necessary steps,
      ;; then perform them
      (multi-diagonal-enumerator-do-steps
       1 enums (multi-diagonal-enumerator-find-step-pair 1 enums))))
  (latest-element enum))

(defun test-multi-diagonal-enumerator (list-lengths &key state-inspector (separator "###"))
  (test-diagonal-enumerator-creator
   #'make-multi-diagonal-enumerator
   list-lengths
   :function-extra-args
   (when state-inspector
     (list
      (lambda (&rest l)
	(let ((n (/ (length l) 2)))
	  (append
	   (subseq l 0 n)
	   (list separator)
	   (mapcar state-inspector (subseq l n)))))))
   :output-cleaner
   (if state-inspector
       (lambda (l) (subseq l 0 (truncate (length l) 2)))
       #'identity)))
