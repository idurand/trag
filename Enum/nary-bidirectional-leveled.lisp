;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;; The code in this file is a kind of manual unrolling of iterating
;; the product in bidirectional-leveled.lisp
;; it is unfinished and not to be used

(defclass nary-bidirectional-leveled-enumerator (way-product-mixin nary-relying-enumerator)
  ((ways :type array :initarg :ways :reader ways)
   (major-step-index :type integer :initform 0 :accessor major-step-index)))

(defgeneric make-nary-bidirectional-leveled-enumerator (enums &key fun)
  (:method ((enum null) &key (fun #'identity))
    (declare (ignore fun))
    (make-bidirectional-enumerator (make-enumerator-nil)))
  (:method ((enums list) &key (fun #'identity))
    (if (endp (cdr enums))
	(car enums)
	(let ((bidirectionals
		(coerce
		 (loop
		   for enum in enums
		   collect (make-bidirectional-enumerator enum))
		'vector))
	      (ways (make-array (length enums) :initial-element -1)))
      (setf (aref ways 0) 1)
      (init-enumerator
       (make-instance
	'nary-bidirectional-leveled-enumerator
	:underlying-enumerators bidirectionals
	:ways ways
	:fun fun))))))

(defmethod way-next-element-p ((way integer) (enum nary-bidirectional-leveled-enumerator))
  (let ((enums (underlying-enumerators enum)))
    (or
     (and (plusp way) (every #'next-element-p enums))
     (and (minusp way) (every #'previous-element-p enums))
     (some
      (lambda (e) (way-next-element-p way e))
      enums))))

(defmethod i-major-step-p ((index integer) (enum nary-bidirectional-leveled-enumerator)))
			       
(defmethod way-next-element ((way integer) (enum nary-bidirectional-leveled-enumerator)))
