;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defclass no-duplicates-enumerator (filter-enumerator accu-mixin)
  ((accu-test :initarg :accu-test :reader accu-test))
  (:documentation "memorizes already seen elements in order to avoid enumerating twice the same element"))

(defgeneric make-no-duplicates-enumerator (abstract-enumerator &key test)
  (:method ((e abstract-enumerator) &key (test #'eql))
    (let ((s (make-instance
	      'no-duplicates-enumerator
	      :enum (copy-enumerator e)
	      :accu-test test)))
      (setf (slot-value s 'fun)
	    (lambda (element)
	      (not (member element (accu s) :test test))))
      s)))
  
(defmethod copy-enumerator ((e no-duplicates-enumerator))
  (make-no-duplicates-enumerator (enum e) :test (accu-test e)))
