;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defclass one-value-enumerator (abstract-enumerator)
  ((value :reader value :initarg :value)
   (available :accessor available :initarg :available))
  (:documentation "one-value-enumerator"))

(defmethod next-element-p ((e one-value-enumerator))
  (available e))

(defmethod next-element ((e one-value-enumerator))
  (prog1 (value e)
    (setf (available e) nil)))

(defmethod init-enumerator :after ((e one-value-enumerator))
  (setf (available e) t))

(defun make-one-value-enumerator (value)
  (init-enumerator 
   (make-instance 'one-value-enumerator :value value)))

(defmethod copy-enumerator ((e one-value-enumerator))
  (make-one-value-enumerator (value e)))

(defmethod show-enumerator ((e one-value-enumerator))
  (format t "Value:~A Available: ~A" (value e) (available e))
  (values))
