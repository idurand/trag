;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; concrete parallel enumerator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass parallel-enumerator (nary-relying-enumerator fun-mixin)
  ()
  (:documentation
   "enumerates in parallel the elements of all
    enumerators in (underlying-enumerators e)"))

(defun make-parallel-enumerator (enums &key (fun #'identity))
  (assert (not (null enums)))
  (setf enums (mapcar #'copy-enumerator enums))
  (make-instance
   'parallel-enumerator
   :underlying-enumerators enums
   :fun fun))

(defmethod copy-enumerator ((e parallel-enumerator))
  (make-parallel-enumerator (underlying-enumerators e)))

(defmethod next-element-p ((e parallel-enumerator))
  (every #'next-element-p (underlying-enumerators e)))

(defmethod next-element ((e parallel-enumerator))
  (funcall (fun e) (loop
		     for enumerator in (underlying-enumerators e)
		     collect (next-element enumerator))))
