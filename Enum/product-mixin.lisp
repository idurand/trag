;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defclass product-mixin (init-mixin fun-mixin) ())

(defclass way-product-mixin (way-mixin product-mixin) ())

(defmethod flip-way ((e way-product-mixin))
  (invert-way e)
  (mapc #'flip-way (underlying-enumerators e)))

(defgeneric minor-step-p (enum)
  (:method ((enum bidirectional-enumerator)) nil)
  (:method :before ((enum abstract-enumerator))
    (assert (next-element-p enum)))
  (:method ((enum product-mixin))
    (and
     (latest-element-p (enum1 enum)) ;; it is not the first step
     (next-element-p (enum2 enum))
     (or (next-element-p (enum1 enum)) (minor-step-p (enum2 enum)))))
  (:method ((enum abstract-enumerator))
    (warn "minor-step-p on ~A~%" (class-of enum))
    nil))

(defmethod latest-element-p ((enum product-mixin))
  (and (latest-element-p (enum1 enum)) (latest-element-p (enum2 enum))))

(defmethod latest-element ((enum product-mixin))
  (funcall (fun enum)
	   (latest-element (enum1 enum))
	   (latest-element (enum2 enum))))

(defmethod next-element-p ((enum product-mixin))
;;  (print '(next-element-p product-mixin))
  (let ((n1 (next-element-p (enum1 enum)))
	(n2 (next-element-p (enum2 enum))))
    (if (initialized-p enum)
	(or (and n1 n2)
	    (true-next-element-p (enum1 enum))
	    (true-next-element-p (enum2 enum)))
	(and n1 n2))))

(defmethod way-next-element-p ((way integer) (enum way-product-mixin))
;;  (print 'way-next-element-p-way-product-mixin)
  (if (initialized-p enum)
      (or
       (with-way ((enum1 enum) way) (next-element-p (enum1 enum)))
       (with-way ((enum2 enum) way) (next-element-p (enum2 enum))))
      (and
       (with-way ((enum1 enum) way) (next-element-p (enum1 enum)))
       (with-way ((enum2 enum) way) (next-element-p (enum2 enum))))))
