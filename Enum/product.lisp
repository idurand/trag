;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defgeneric make-binary-product-enumerator (enum1 enum2)
  (:method ((enum1 abstract-enumerator) (enum2 abstract-enumerator))
    (make-binary-diagonal-product-enumerator enum1 enum2)))
;;    (make-binary-bidirectional-leveled-enumerator enum1 enum2)))

(defgeneric make-nary-product-enumerator (enumerators)
  (:method ((enums list))
    (make-nary-diagonal-product-enumerator enums)))
;;    (make-nary-recursive-bidirectional-leveled-enumerator enums)))

(defun make-product-enumerator (enumerators)
  (make-nary-product-enumerator enumerators))
