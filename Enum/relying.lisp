;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; abstract-enumerators relying on other enumerators
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass relying-enumerator (abstract-enumerator) ()
  (:documentation "enumerators relying on at least one enumerator"))

(defmethod show-enumerator :after ((e relying-enumerator))
  (loop
    with enums = (underlying-enumerators e)
    for k from 0 below (length enums)
    do (show-enumerator (elt enums k))))

(defmethod init-enumerator :before ((e relying-enumerator))
  (loop
    with enums = (underlying-enumerators e)
    for k from 0 below (length enums)
    do (init-enumerator (elt enums k))))

(defclass unary-relying-enumerator (relying-enumerator)
  ((enum :type abstract-enumerator :initarg :enum :reader enum))
  (:documentation "enumerator relying on one enumerator"))

(defgeneric underlying-enumerators (relying-enumerator)
  (:documentation "enumerators on which RELYING-ENUMERATOR relies")
  (:method ((e unary-relying-enumerator)) (list (enum e))))

(defmethod next-element-p ((e unary-relying-enumerator))
  (next-element-p (enum e)))

(defmethod next-element ((e unary-relying-enumerator))
  (next-element (enum e)))

(defclass binary-relying-enumerator (relying-enumerator)
  ((enum1 :type abstract-enumerator :initarg :enum1 :reader enum1)
   (enum2 :type abstract-enumerator :initarg :enum2 :reader enum2))
  (:documentation "enumerator relying on two enumerators"))

(defmethod underlying-enumerators ((e binary-relying-enumerator))
  (list (enum1 e) (enum2 e)))

(defclass nary-relying-enumerator (relying-enumerator)
  ((enums :type sequence :initarg :underlying-enumerators
	  :reader underlying-enumerators))
  (:documentation "enumerator relying on a list of enumerators"))

(defgeneric enum-i (nary-relying-enumerator integer)
  (:method ((e nary-relying-enumerator) (i integer))
    (aref (underlying-enumerators e) i)))

(defmethod latest-element-p ((enum relying-enumerator))
  (every #'latest-element-p (underlying-enumerators enum)))

(defmethod latest-element-p ((enum unary-relying-enumerator))
  (latest-element-p (underlying-enumerators enum)))
