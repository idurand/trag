;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; general enumeration of a sequence
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric make-sequence-enumerator (sequence)
  (:method ((l list))
    (make-list-enumerator l))
  (:method ((v vector))
    (make-vector-enumerator v)))

(defun my-map (result-type function &rest sequences)
  (let ((enumerator
	  (make-parallel-enumerator
	   (mapcar
	    (lambda (s)
	      (make-sequence-enumerator s))
	    sequences)
	   :fun (lambda (tuple)
		  (apply function tuple)))))
    (if (null result-type)
	nil
	(coerce (collect-enum enumerator) result-type))))
