;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; concrete sequence enumerator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass sequential-enumerator (nary-relying-enumerator)
  ((remaining-enumerators
    :type list :initarg :remaining-enumerators
    :accessor remaining-enumerators))
  (:documentation
   "enumerates sequentially the elements of each
    enumerator in (underlying-enumerators e)"))

(defmethod show-enumerator :after ((e sequential-enumerator))
  (format t "remaining-enumerators: ~A~%" (remaining-enumerators e)))

(defgeneric move-to-non-empty-sequence (sequential-enumerator)
  (:method ((e sequential-enumerator))
    (loop
      while (remaining-enumerators e)
      until
      (next-element-p
       (first (remaining-enumerators e)))
      do (pop (remaining-enumerators e)))))

(defmethod init-enumerator :after ((e sequential-enumerator))
  (setf (remaining-enumerators e) (underlying-enumerators e)))

(defun make-sequential-enumerator (enums)
  (assert (not (endp enums)))
  (setf enums (mapcar #'copy-enumerator enums))
  (if (endp (cdr enums))
      (car enums)
      (init-enumerator
       (make-instance 'sequential-enumerator :underlying-enumerators enums))))

(defmethod copy-enumerator ((e sequential-enumerator))
  (make-sequential-enumerator (underlying-enumerators e)))

(defmethod next-element-p ((e sequential-enumerator))
  (move-to-non-empty-sequence e)
  (let ((re (remaining-enumerators e)))
    (and re (next-element-p (first re)))))

(defmethod next-element ((e sequential-enumerator))
  (assert (next-element-p (first (remaining-enumerators e))))
  (next-element (first (remaining-enumerators e))))
