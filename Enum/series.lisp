;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; more
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun by-fun (by downto)
  (if downto
      (lambda (x) (- x by))
      (lambda (x) (+ x by))))

(defun range-enumerator (&key (from 0) (by 1) (downto nil))
  (make-inductive-enumerator
   from
   (by-fun by downto)))

(defgeneric collect-n-enum (abstract-enumerator integer &key init)
  (:method ((e abstract-enumerator) (n integer) &key (init t))
    (when init
      (init-enumerator e))
    (loop
      repeat n
      while (next-element-p e)
      collect (next-element e))))

(defgeneric collect-enum (abstract-enumerator &key init)
  (:method ((e abstract-enumerator) &key (init t))
    (when init
      (init-enumerator e))
    (loop
      while (next-element-p e)
      collect (next-element e)))
  (:method ((e list-enumerator) &key (init t))
    (copy-list 
     (if init
	 (initial-list e)
	 (current-list e)))))

(defgeneric collect-enum-set (enumerator &key init test)
  (:method ((e abstract-enumerator) &key (init t) (test #'eql))
    (delete-duplicates (collect-enum e :init init) :test test)))

(defgeneric collect-append-enum (abstract-enumerator &key init)
  (:method ((e abstract-enumerator) &key (init nil))
    (when init
      (init-enumerator e))
    (loop
      while (next-element-p e)
      append (next-element e))))

(defgeneric collect-fn-enum (initial-value fun abstract-enumerator &key init)
  (:method (initial-value fun (e abstract-enumerator) &key (init nil))
   (when init
     (init-enumerator e))
    (loop
      with x = initial-value
      while (next-element-p e)
      do (setf x (funcall fun x (next-element e)))
      finally (return x))))

(defgeneric count-enum (abstract-enumerator &key init)
  (:method ((e abstract-enumerator) &key (init t))
    (when init
      (init-enumerator e))
    (loop
      while (next-element-p e)
      count (next-element e)))
  (:method ((e list-enumerator) &key (init t))
    (when init
      (init-enumerator e))
    (length (current-list e))))


