;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defclass simple-enumerator (abstract-enumerator)
  ((latest-element :accessor latest-element)
   (latest-element-p :initform nil :accessor latest-element-p))
  (:documentation "class for simple (unrelying) enumerators"))

(defmethod next-element :after ((enum simple-enumerator))
  (setf (latest-element-p enum) t))

(defmethod next-element :around ((enum simple-enumerator))
  (setf (latest-element enum) (call-next-method)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; constant enumerator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass constant-enumerator (simple-enumerator)
  ((constant :initarg :constant :reader constant))
  (:documentation "enumerators of the elements of a list"))

(defmethod show-enumerator :after ((e constant-enumerator))
  (format t "current-list: ~A~%" (constant e)))

(defmethod next-element-p ((e constant-enumerator)) t)

(defmethod next-element ((e constant-enumerator))
  (constant e))

(defun make-constant-enumerator (constant)
  (make-instance 'constant-enumerator :constant constant))

(defmethod copy-enumerator ((e constant-enumerator))
  e)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; concrete enumerators of the elements of a list
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass list-enumerator (simple-enumerator)
  ((initial-list :type list :initarg :initial-list :reader initial-list)
   (current-list :type list :initarg :current-list :accessor current-list))
  (:documentation "enumerators of the elements of a list"))

(defmethod show-enumerator :after ((e list-enumerator))
  (format t "current-list: ~A~%" (current-list e)))

(defmethod next-element-p ((le list-enumerator))
  (not (endp (current-list le))))

(defmethod next-element ((le list-enumerator))
  (pop (current-list le)))

(defmethod init-enumerator :after ((e list-enumerator))
  (setf (current-list e) (initial-list e)))

(defun ncirc (l) (nconc l l))

(defun circ (l) (ncirc (copy-list l)))

(defun make-list-enumerator (l &key (circ nil))
  (when (endp l)
    (return-from make-list-enumerator (make-empty-enumerator)))
  (when circ (setf l (circ l)))
  (make-instance 'list-enumerator :initial-list l :current-list l))

(defmethod copy-enumerator ((le list-enumerator))
  (make-list-enumerator (initial-list le)))

(defmethod snapshot-enumerator ((e list-enumerator))
  (let* ((copy (copy-enumerator e)))
    (setf (current-list copy) (current-list e))))
