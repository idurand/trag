;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

(defclass stream-enumerator (abstract-enumerator)
  ((e-stream :type e-stream :initarg :e-stream :accessor e-stream)
   (next-line :accessor next-line)
   (stream-process :initarg :stream-process :reader stream-process)))

(defmethod next-element-p ((e stream-enumerator))
  (not (null (next-line e))))

(defgeneric move-to-next-line (stream-enumerator)
  (:method ((e stream-enumerator))
    (setf (next-line e) (read-line (e-stream e) nil nil))
    (unless (next-line e)
      (cleanup-enumerator e))))

(defmethod next-element ((e stream-enumerator))
  (prog1
      (next-line e)
    (move-to-next-line e)))

(defmethod init-enumerator ((e stream-enumerator))
;;  (warn "a stream-enumerator cannot be reinitialized")
  e)

(defun make-stream-enumerator (stream &key process)
  (assert (input-stream-p stream))
  (let ((e (make-instance 'stream-enumerator :e-stream stream :stream-process process)))
    (move-to-next-line e)
    e))
    
(defmethod copy-enumerator ((e stream-enumerator)) e)

(defgeneric cleanup-enumerator (stream-enumerator)
  (:documentation "cleanup stream-enumerator if needed")
  (:method ((e stream-enumerator))
    (when (e-stream e)
      (close (e-stream e)))
    (when (stream-process e)
      (sb-ext:process-close (stream-process e)))))
  
