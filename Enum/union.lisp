;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; concrete union enumerator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass union-enumerator (nary-relying-enumerator)
  ((index :type 'integer :initform 0
	  :accessor index))
  (:documentation
   "enumerates diagonally the elements of the union of the
    enumerators in (underlying-enumerators e)"))

(defmethod show-enumerator :after ((e union-enumerator))
  (format t "index: ~A~%" (index e)))

(defmethod init-enumerator :after ((e union-enumerator))
  (setf (index e) 0))

(defun make-union-enumerator (enums)
  (setf enums (mapcar #'copy-enumerator enums))
  (init-enumerator
   (make-instance
    'union-enumerator
    :underlying-enumerators (coerce enums' vector))))

(defmethod copy-enumerator ((e union-enumerator))
  (make-union-enumerator (underlying-enumerators e)))

(defmethod next-element-p ((e union-enumerator))
  (next-element-p (enum-i e (index e))))

(defmethod next-element ((e union-enumerator))
  (let ((index (index e)))
    (prog1 (next-element (enum-i e index))
      (setf (index e)
	    (mod (1+ index) (length (underlying-enumerators e)))))))
