;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :enum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; concrete enumerators of the elements of a vector
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass vector-enumerator (abstract-enumerator)
  ((ind :type integer :initform 0 :accessor ind)
   (ind-max :type integer :initarg :ind-max :reader ind-max)
   (ind-next :type function :initarg :ind-next :reader ind-next)
   (vect :type vect :initarg :vect :accessor vect))
  (:documentation "enumerators of the elements of a vect"))

(defmethod show-enumerator :after ((e vector-enumerator))
  (format t "index: ~A: ~A~%" (ind e) (vect e)))

(defmethod next-element-p ((e vector-enumerator))
  (< (ind e) (ind-max e)))

(defmethod next-element ((e vector-enumerator))
  (prog1
      (aref (vect e) (ind e))
    (setf (ind e) (funcall (ind-next e) (ind e)))))

(defmethod init-enumerator :after ((e vector-enumerator))
  (setf (ind e) 0))

(defun make-vector-enumerator (v &key (circ nil))
  (let ((len (length v)))
    (when (zerop len)
      (return-from make-vector-enumerator (make-empty-enumerator)))
    (make-instance
     'vector-enumerator
     :vect v :ind-max len
     :ind-next (if circ (lambda (i) (mod (1+ i) len)) #'1+))))

(defmethod copy-enumerator ((e vector-enumerator))
  (make-vector-enumerator (vect e)))

(defmethod snapshot-enumerator ((e vector-enumerator))
  (let ((copy (copy-enumerator e)))
    (setf (ind copy) (ind e))
    copy))
