;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand, Michael Raskin

(in-package :enum)

(defclass way-mixin ()
  ((way :accessor way :initform nil)))

(defmacro with-way ((enum way) &body body)
  (let ((saved-way (gensym)))
    `(let
	 ((,saved-way (way ,enum)))
       (unwind-protect
	    (progn
	      (setf (way ,enum) ,way)
	      ,@body)
	 (setf (way ,enum) ,saved-way)))))

(defmacro with-flipped-way ((enum) &body body)
  `(with-way (,enum (- (way ,enum))) ,@body))

(defmethod next-element-p ((e way-mixin))
;;  (print '(next-element-p way-mixin))
  (way-next-element-p (way e) e))

(defmethod next-element ((e way-mixin))
;;  (print '(next-element way-mixin))
  (way-next-element (way e) e))

(defgeneric previous-element-p (enum)
  (:method ((enum way-mixin))
    (way-next-element-p (- (way enum)) enum)))

(defgeneric previous-element (enum)
  (:method ((enum way-mixin))
    (way-next-element (- (way enum)) enum)))

(defmethod true-next-element-p ((enum way-mixin))
  (way-next-element-p 1 enum))

(defmethod true-next-element ((enum way-mixin))
  (assert (true-next-element-p enum))
  (way-next-element 1 enum))

(defmethod init-enumerator :after ((enum way-mixin))
  (setf (way enum) 1))

(defgeneric true-previous-element-p (enum)
  (:method ((enum abstract-enumerator))
    (way-next-element-p -1 enum)))

(defgeneric true-previous-element (enum)
  (:method ((enum way-mixin))
    (way-next-element -1 enum)))

(defmethod show-enumerator :after ((e way-mixin))
  (format t "way: ~A~%" (way e)))

(defgeneric invert-way (way-mixin)
  (:method ((e way-mixin)) (setf (way e) (- (way e))))
  (:documentation "invert way shallow"))

(defgeneric flip-way (way-mixin)
  (:method ((e way-mixin)) (invert-way e))
  (:documentation "invert! way deep"))
