(in-package :formula)

(defclass boolean-indexed-formula (abstract-term) ()
  (:documentation "A representation of a boolean formula with inputs numbered from 1 up"))

(defclass boolean-indexed-literal (boolean-indexed-formula)
  ((index :type integer :accessor literal-index :initarg :index)))

(defclass boolean-named-literal (boolean-indexed-formula)
  ((name :type :string :accessor name :initarg :name)))

(defclass boolean-indexed-operation (boolean-indexed-formula)
  ((arg :type list :accessor arg :initarg :arg)))

(defclass boolean-indexed-and (boolean-indexed-operation) ())
(defclass boolean-indexed-or (boolean-indexed-operation) ())
(defclass boolean-indexed-not (boolean-indexed-operation) ())

(defclass boolean-indexed-equal (boolean-indexed-operation) ())
(defclass boolean-indexed-implies (boolean-indexed-operation) ())
(defclass boolean-indexed-implied-by (boolean-indexed-operation) ())
(defclass boolean-indexed-xor (boolean-indexed-operation) ())

(defun make-boolean-indexed-literal (index)
  (make-instance 'boolean-indexed-literal :index index))
(defun make-boolean-named-literal (name)
  (make-instance 'boolean-named-literal :name name))
(defun make-boolean-indexed-and (&rest args)
  (make-instance 'boolean-indexed-and :arg args))
(defun make-boolean-indexed-or (&rest args)
  (make-instance 'boolean-indexed-or :arg args))
(defun make-boolean-indexed-not (arg)
  (make-instance 'boolean-indexed-not :arg (list arg)))

(defun make-boolean-indexed-equal (&rest args)
  (make-instance 'boolean-indexed-equal :arg args))
(defun make-boolean-indexed-implies (&rest args)
  (make-instance 'boolean-indexed-implies :arg args))
(defun make-boolean-indexed-implied-by (&rest args)
  (make-instance 'boolean-indexed-implied-by :arg args))
(defun make-boolean-indexed-xor (&rest args)
  (make-instance 'boolean-indexed-xor :arg args))

(defgeneric evaluate-indexed-boolean-formula (formula inputs)
  (:method ((formula boolean-indexed-literal) inputs)
           (not (null (find (elt inputs (literal-index formula)) '(t 1)))))
  (:method ((formula boolean-indexed-and) inputs)
           (loop for arg in (arg formula)
                 unless (evaluate-indexed-boolean-formula arg inputs) return nil
                 finally (return t)))
  (:method ((formula boolean-indexed-or) inputs)
           (loop for arg in (arg formula)
                 when (evaluate-indexed-boolean-formula arg inputs) return t
                 finally (return nil)))
  (:method ((formula boolean-indexed-not) inputs)
           (not (evaluate-indexed-boolean-formula (first (arg formula)) inputs)))
  (:method ((formula boolean-indexed-equal) inputs)
           (equal
             (evaluate-indexed-boolean-formula (first (arg formula)) inputs)
             (evaluate-indexed-boolean-formula (second (arg formula)) inputs)))
  (:method ((formula boolean-indexed-xor) inputs)
           (not
             (equal
               (evaluate-indexed-boolean-formula (first (arg formula)) inputs)
               (evaluate-indexed-boolean-formula (second (arg formula)) inputs))))
  (:method ((formula boolean-indexed-implies) inputs)
           (or
             (not (evaluate-indexed-boolean-formula (first (arg formula)) inputs))
             (evaluate-indexed-boolean-formula (second (arg formula)) inputs)))
  (:method ((formula boolean-indexed-implied-by) inputs)
           (or
             (evaluate-indexed-boolean-formula (first (arg formula)) inputs)
             (not (evaluate-indexed-boolean-formula (second (arg formula)) inputs))))
  (:documentation "Apply an indexed-input formula"))

(defmethod print-object ((object boolean-indexed-literal) stream)
  (format stream "X~3,'0d" (literal-index object)))
(defmethod print-object ((object boolean-named-literal) stream)
  (format stream "~a" (name object)))
(defmethod print-object ((object boolean-indexed-not) stream)
  (format stream "(¬~a)" (first (arg object))))
(defmethod print-object ((object boolean-indexed-and) stream)
  (format stream "(~{~a~#[~:;∧~]~})" (arg object)))
(defmethod print-object ((object boolean-indexed-or) stream)
  (format stream "(~{~a~#[~:;∨~]~})" (arg object)))
(defmethod print-object ((object boolean-indexed-equal) stream)
  (format stream "(~{~a=~a~})" (arg object)))
(defmethod print-object ((object boolean-indexed-xor) stream)
  (format stream "(~{~a≠~a~})" (arg object)))
(defmethod print-object ((object boolean-indexed-implies) stream)
  (format stream "(~{~a→~a~})" (arg object)))
(defmethod print-object ((object boolean-indexed-implied-by) stream)
  (format stream "(~{~a←~a~})" (arg object)))

(defpackage :formula-parsing-boolean-formula (:use))

(defparameter *boolean-formula-peg*
  "
  Whitespace <- [ \r\n\t]
  Sp <- Whitespace+
  Literal <- [A-Za-z0-9_]+
  OpNot <- ('not' / 'NOT' / '!' / '~' / '¬' / '￢' / 'Ꞁ' / '⎤' / '┓' / '┐')
  OpAnd <- ('and' / 'AND' / '&&' / '&' / '*' / '/\' / '∧' / '^' / '⋀' )
  OpOr <- ('or' / 'OR' / '||' / '|' / '+' / '\/' / '∨' / 'v' / '⋁' )
  OpImplies <- ('implies' / 'IMPLIES' / '->' / '=>' / '→' / '⇒' / '⊂' / '⊆')
  OpImpliedBy <- ('impliedBy' / 'impliedby' / 'IMPLIEDBY' / 'implied' / 'IMPLIED' / '<-' / '<=' / '←' / '⇐' / '⊃' / '⊇')
  OpEqual <- ('equal' / 'EQUAL' /  '==' / '=' / '<->' / '<=>' / '↔' / '⇔' / '≡')
  OpXor <- ('xor' / 'XOR' / 'unequal' / 'UNEQUAL' / '!=' / '~=' / '<>' / '#' / '/=' / '≠' / '⊕')
  ParenFormula <- '(' Formula ')' / '[' Formula ']' / '{' Formula '}'
  Atomic <- ParenFormula / Literal
  NotLiteral <- OpNot Sp? SignedLiteral
  SignedLiteral <- NotLiteral / Atomic
  AndExpression <- SignedLiteral Sp? OpAnd Sp? Conjunction
  Conjunction <- AndExpression / SignedLiteral
  OrExpression <- Conjunction Sp? OpOr Sp? Disjunction
  Disjunction <- OrExpression / Conjunction
  EqualExpression <- Disjunction Sp? OpEqual Sp? Disjunction
  ImpliesExpression <- Disjunction Sp? OpImplies Sp? Disjunction
  ImpliedByExpression <- Disjunction Sp? OpImpliedBy Sp? Disjunction
  XorExpression <- Disjunction Sp? OpXor Sp? Disjunction
  CombinationExpression <- ImpliesExpression / ImpliedByExpression / EqualExpression / XorExpression / Disjunction
  Formula <- Sp? CombinationExpression Sp?
  ")
(let ((*package* (find-package :formula-parsing-boolean-formula)))
  (esrap-peg:ast-eval
    (esrap-peg:basic-parse-peg *boolean-formula-peg*)))

(esrap-peg:def-peg-matchers
  ((Formula ((_ ?x _) (! ?x)))
   (CombinationExpression (?x (! ?x)))
   (Disjunction (?x (! ?x)))
   (Conjunction (?x (! ?x)))
   (Atomic (?x (! ?x)))
   (SignedLiteral (?x (! ?x)))
   (ParenFormula ((_ ?x _) (! ?x)))
   (Literal (?name (make-boolean-named-literal (s+ ?name))))
   (NotLiteral ((_ _ ?literal) (make-boolean-indexed-not (! ?literal))))
   (AndExpression ((?a _ _ _ ?b) (make-boolean-indexed-and (! ?a) (! ?b))))
   (OrExpression ((?a _ _ _ ?b) (make-boolean-indexed-or (! ?a) (! ?b))))
   (EqualExpression ((?a _ _ _ ?b) (make-boolean-indexed-Equal (! ?a) (! ?b))))
   (XorExpression ((?a _ _ _ ?b) (make-boolean-indexed-Xor (! ?a) (! ?b))))
   (ImpliesExpression ((?a _ _ _ ?b) (make-boolean-indexed-Implies (! ?a) (! ?b))))
   (ImpliedByExpression ((?a _ _ _ ?b) (make-boolean-indexed-Implied-By (! ?a) (! ?b)))))
  :package :formula-parsing-boolean-formula
  :abbreviations :default)

(defgeneric boolean-formula-variables (f)
  (:method ((f boolean-named-literal)) (list (name f)))
  (:method ((f boolean-indexed-literal)) (list (format nil "X~3,'0d" (literal-index f))))
  (:method ((f boolean-indexed-operation))
           (reduce (lambda (x y) (union x y :test 'equal))
                             (mapcar 'boolean-formula-variables (arg f)))))

(defgeneric boolean-formula-lookup-names (f names)
  (:method ((f boolean-indexed-literal) names) f)
  (:method ((f boolean-named-literal) names)
           (make-boolean-indexed-literal (position (name f) names :test 'equal)))
  (:method ((f boolean-indexed-operation) names)
           (setf
             (arg f)
             (loop for a in (arg f) collect
                   (boolean-formula-lookup-names a names)))
           f)
  (:documentation "Destructively replace the named variable references with index-based references"))

(defun boolean-formula-names-to-indices (f)
  (let* ((variables (sort (boolean-formula-variables f) 'string<)))
    (boolean-formula-lookup-names f variables)))

(defun boolean-formula-with-names-from-string (string)
  (esrap-peg:ast-eval
    (esrap:parse (intern "FORMULA" :formula-parsing-boolean-formula) string)))

(defun boolean-formula-from-string (string)
  (boolean-formula-names-to-indices
    (boolean-formula-with-names-from-string string)))

(defun append-indexed-boolean-formula-result (formula inputs)
  (let* ((input-type (type-of inputs))
         (target-type (if (listp input-type) (first input-type) input-type)))
    (concatenate target-type inputs
                 (list (evaluate-indexed-boolean-formula formula inputs)))))

(defun indexed-boolean-formula-append-preimage (formula inputs-list)
  (loop for inputs in inputs-list
        for l := (length inputs)
        for last := (elt inputs (1- l))
        for preimage := (subseq inputs 0 (1- l))
        for value := (evaluate-indexed-boolean-formula formula preimage)
        when (equal last value) collect preimage))
