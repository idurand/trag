;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin, Irène Durand

(asdf:defsystem
  :formula
  :description "Parsing and representing formulas"
  :version "0.0"
  :author "Michael Raskin <raskin@mccme.ru>, Irène Durand <idurand@labri.fr>"
  :depends-on (
               :esrap-peg :esrap :terms
               )
  :components
  (
   (:file "package")
   (:file "boolean-formula")
   ))

