(defpackage :formula
  (:use :common-lisp :terms :esrap-peg)
  (:export
    #:boolean-formula-from-string
    #:boolean-formula-with-names-from-string
    #:boolean-formula-variables
    #:evaluate-indexed-boolean-formula
    ))
