;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand, Michael Raskin

(in-package :general)

(defmethod list-pairs ((alist list)) alist)
