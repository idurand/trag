;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defgeneric annotation (annotated-object)
  (:documentation "annotation of ANNOTATED-OBJECT"))

(defclass annotation-mixin ()
  ((%annotation :initarg :annotation :accessor annotation :initform nil))
  (:documentation "a mixin class to add an annotation to objects"))

(defmethod print-object :after ((a annotation-mixin) stream)
  (let ((annotation (annotation a)))
    (when annotation
      (format stream "+~A" (annotation a)))))

(defgeneric delete-annotation (annotated-object)
  (:documentation "annotation set to NIL")
  (:method ((a annotation-mixin))
    (setf (annotation a) nil)))
