;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)
;;; Little functions for working on (ordered or not) pairs of integers

(defun cons< (cons1 cons2)
  "lexicographic order"
  (or (< (car cons1) (car cons2))
      (and (= (car cons1) (car cons2))
	   (< (cdr cons1) (cdr cons2)))))

(defun sorted-cons (i j)
  "build ordered pair"
  (if (< i j) (cons i j) (cons j i)))

(defun distinct-cons (numbers)
  (loop for i in numbers
    nconc (loop
	    for j in numbers
	    when (/= i j)
	      collect (cons i j))))
