;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defun date-string (&optional string)
  "date provided by the system possibly concatenated with STRING"
  (let ((date-string (time-string nil)))
    (if string
	(concatenate 'string date-string "-" string)
	date-string)))
