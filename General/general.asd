;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for General.
(in-package :asdf-user)

(defsystem :general
  :description "General: useful stuff"
  :name "general"
  :version "6.0"
  :author "Irène Durand"
  :licence "General Public Licence"
  :serial t
  :components
   (
    (:file "package")
    (:file "general")
    (:file "hashtable")
    (:file "alist")
    (:file "time")
    (:file "annotation")
    (:file "mapping")
    (:file "vector")
    (:file "string")
    (:file "date")
    (:file "num")
    (:file "pairs")
    (:file "cons")
    (:file "oriented") 
    (:file "name")
    ))

(pushnew :general *features*)
