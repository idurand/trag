;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defvar *debug* nil)

(defun normalize-string (s n)
  (let ((l (length s)))
    (if (>= l n)
	s
	(concatenate 'string " " s (make-string (- n l) :initial-element #\space)))))

(let* ((counter 0)
       (chars (make-array 4 :initial-contents '("\/" "\-" "\\" "\|")))
       (tog 0)
       (len (length chars)))
  (defun next-char ()
    (setf counter (mod (1+ counter) 100000))
    (if (zerop (mod counter 1000))
	(progn
	  (setf tog (mod (1+ tog) len))
	  (values t (aref chars tog)))
	(values nil (aref chars tog)))))

(defun find-all (item sequence &rest keyword-args
                 &key (test #'eql) test-not &allow-other-keys
					      )
  "Find all those elements of sequence that match item,
  according to the keywords.  Doesn't alter sequence."
  (if test-not
      (apply #'remove item sequence 
             :test-not (complement test-not) keyword-args)
      (apply #'remove item sequence
             :test (complement test) keyword-args)))

(defun mappend (fn &rest lsts)
  (apply #'append (apply #'mapcar fn lsts)))

(defun arrange-list (l arr)
  (mapcan (lambda (x) 
	    (mapcar (lambda (y) (cons x y))
		    arr))
	  l))

(defun arrange (l n)
  (if (= n 0)
      (list '())
      (arrange-list l (arrange l (1- n)))))

(defun combinaison (l n) ; au moins n elements dans l
  (cond ((= n 0)
	 (list '()))
	((null l)
	 '())
	((= n 1)
	 (mapcar #'list l))
	(t
	 (nconc
	  (mapcar (lambda (x) (cons (car l) x)) (combinaison (cdr l) (1- n)))
	  (combinaison (cdr l) n)))))

(defun distribute (e setofsets)
  (mapcar (lambda (x) (cons e x)) setofsets))

(defun distribute-set (set setofsets)
  (mapcan (lambda (x) (distribute x setofsets)) set))

(defun cartesian-product (args)
  (if (null args)
      (list '())
      (distribute-set (car args) (cartesian-product (cdr args)))))

(defun powerset (s)
  (if (null s)
      (list '())
      (let ((p (powerset (cdr s))))
	(nconc p (distribute (car s) p)))))

(defun not-null-powerset (s)
  (delete nil (powerset s)))

(defun flatten (l)
  (cond ((endp l)
	 '())
	((listp (car l)) (nconc (flatten (car l)) (flatten (cdr l))))
	(t
	 (cons (car l) (flatten (cdr l))))))

(defun iota (n &optional (start 0) (step 1))
  (loop repeat n
	for i from start by step
	collect i))

(defun permutations (l)
  (if (null (cdr l))
      (list l)
      (loop
	with len = (1- (length l))
	for p in (permutations (cdr l))
	nconc
	(loop
	  for i from 0 to (length p)
	  collect (nconc
		   (butlast p (- len i))
		   (list (car l))
		   (nthcdr i p))))))

(defun remove-duplicates-except-pairs (l &key (test #'eql))
  (if (null (cdr l))
      l
      (let* ((ll (remove-duplicates-except-pairs (cdr l) :test test))
	     (count (count (car l) ll :test test)))
	(if (> count 1)
	    ll
	    (cons (car l) ll)))))

(defun split-list (l pred)
  (let ((ok '())
	(nok '()))
    (dolist (e l (values (nreverse ok) (nreverse nok)))
      (if (funcall pred e)
	  (push e ok)
	  (push e nok)))))

(defgeneric display-sequence (l stream &key sep)
  (:documentation "print the objects of the sequence L separated by the separator SEP on the STREAM"))

(defmethod display-sequence ((l list) stream &key (sep " "))
  (when l
    (mapc (lambda (e)
	    (format stream "~A~A" e sep))
	  (butlast l))
    (format stream "~A" (car (last l)))))

(defmethod display-sequence ((l vector) stream &key (sep " "))
  (display-sequence (coerce l 'list) stream :sep sep))

(defun to-list (o)
  (if (listp o)
      o
      (list o)))

(defun no-duplicates (l)
  (= (length l) (length (remove-duplicates l))))

(defun remove-adjacent-duplicates (l &key (test 'equal))
  (loop for prev = (gensym) then x
	for x in l
	unless (funcall test prev x)
	  collect x))

(defun list< (l1 l2 &key (test #'<))
  (if (endp l1)
      (not (endp l2))
      (and
       (not (endp l2))
       (or
	(funcall test (car l1) (car l2))
	(and
	 (not (funcall test (car l2) (car l1)))
	 (list< (cdr l1) (cdr l2) :test test))))))

(defun list-random-element (l)
  "a random element of the list L"
  (nth (random (length l)) l))
