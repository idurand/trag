;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defgeneric list-hash (hash-table  &key fun)
  (:documentation "list the pairs (key,values) of a hashtable")
  (:method ((ht hash-table) &key (fun #'identity))
    (let ((l nil))
      (maphash (lambda (key value)
		 (push (list (funcall fun key) value) l))
	       ht)
      l)))

(defgeneric list-keys (hash-table &key fun)
  (:documentation "list the keys of a hashtable")
  (:method ((ht hash-table) &key (fun #'identity))
    (let ((l nil))
      (maphash (lambda (key value) (declare (ignore value))
		 (push (funcall fun key) l))
	       ht)
      l)))

(defgeneric list-values (hash-table)
  (:documentation "list the values of a hashtable")
  (:method ((ht hash-table))
    (let ((l nil))
      (maphash (lambda (key value) (declare (ignore key))
		 (push value l))
	       ht)
      l)))

(defgeneric list-pairs (hash-table)
  (:documentation "pairs (key . value) of HASH-TABLE")
  (:method ((ht hash-table))
    (let ((l nil))
      (maphash (lambda (key value)
		 (push (cons key value) l))
	       ht)
      l)))

(defgeneric hash-reduce (f hash-table &key key initial-value)
  (:documentation "reduce F over values of the HASH-TABLE")
  (:method (f (ht hash-table) &key (key #'identity) (initial-value 0 supplied-p))
    (let (value)
      (with-hash-table-iterator (next ht)
	(setf value
	      (if supplied-p
		  initial-value
		  (multiple-value-bind (found? k v) (next)
		    (declare (ignore k))
		    (if found?
			(funcall key v)
			(assert "no initial value in hash-reduce")))))
	(loop
	  (multiple-value-bind (found? k v) (next)
	    (declare (ignore k))
	    (unless found? (return-from hash-reduce value))
	    (setf value (funcall f value  (funcall key v)))))))))

(defgeneric hash-table-subset-p (ht1 h2 value-test)
  (:documentation "inclusion of hash-tables with values comparable with value-test")
  (:method ((ht1 hash-table) (ht2 hash-table) (value-test function))
    (maphash (lambda (k1 v1)
	       (let ((v2 (gethash k1 ht2)))
		 (unless (and v2 (funcall value-test v1 v2))
		   (return-from hash-table-subset-p))))
	     ht1)
    t))

(defgeneric hash-table-equal-p (ht1 ht2 value-test)
  (:documentation "equality of hash-tables with values comparable with value-test")
  (:method ((ht1 hash-table) (ht2 hash-table) (value-test function))
    (and   
     (= (hash-table-count ht1) (hash-table-count ht2))
     (hash-table-subset-p ht1 ht2 value-test)
     (hash-table-subset-p ht2 ht1 value-test))))

(defun make-key-value (&rest data)
  (let ((res (make-hash-table :test 'equal)))
    (loop for p := data then (subseq p 2)
          for key := (first p)
          for value := (second p)
          while p
          do (assert (rest p)) ; even number of parameters
          do (setf (gethash key res) value))
    res))

(defun merge-key-value (&rest data)
  (let ((res (make-hash-table :test 'equal)))
    (loop for container in data
          do (loop for pair in (list-pairs container)
                   for key := (car pair)
                   for value := (cdr pair)
                   do (setf (gethash key res) value)))
    res))
