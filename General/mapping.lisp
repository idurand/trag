;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defclass mapping ()
  ((mapping-fun :initarg :mapping-fun :reader mapping-fun)
   (injective :initform nil :initarg :injective :reader injective-mapping-p)))

(defgeneric map-mapping (mapping l)
  (:documentation "applies a mapping to every element of the list L")
  (:method ((mapping mapping) (l list))
    (mapcan (mapping-fun mapping) l)))

(defun make-mapping (mapping-fun &optional (injective nil))
  (make-instance 'mapping :mapping-fun mapping-fun :injective injective))

(defgeneric apply-mapping (mapping object)
  (:documentation "applies a mapping function to an object")
  (:method ((mapping mapping) o)
    (to-list (funcall (mapping-fun mapping) o))))
