;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defgeneric num (o)
  (:documentation "the number of a num-mixin")
  (:method ((o t))
    nil))

(defclass num-mixin ()
  ((num :initarg :num :accessor num :initform nil))
  (:documentation "mixin class to add an integer to an object"))
