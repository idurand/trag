;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defvar *oriented* nil
  "a global variable for temporarily changing from oriented to non oriented world")

(defgeneric oriented-p (o)
  (:documentation "is an oriented-object oriented"))

(defclass oriented-mixin ()
  ((oriented-p :initarg :oriented :accessor oriented-p))
  (:documentation "mixin-class for oriented objects"))
