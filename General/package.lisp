;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :general
  (:use :common-lisp)
  (:export #:normalize-string
	   #:display-sequence
	   #:find-all
	   #:mappend
	   #:arrange-list
	   #:arrange
	   #:combinaison
	   #:permutations
	   #:distribute
	   #:distribute-set
	   #:cartesian-product
	   #:powerset
	   #:not-null-powerset
	   #:flatten
	   #:list-keys
	   #:list-values
	   #:list-pairs
	   #:get-time
	   #:with-time
	   #:time-call
	   #:evaluate-time
	   #:iota
	   #:remove-duplicates-except-pairs
	   #:onep
	   #:annotation
	   #:annotation-mixin
	   #:delete-annotation
	   #:hash-reduce
	   #:split-list
	   #:mapping
	   #:mapping-fun
	   #:apply-mapping
	   #:map-mapping
	   #:make-mapping
	   #:injective-mapping-p
	   #:vector-sum
	   #:vector<
	   #:*debug*
	   #:decompose-string
	   #:compose-string
	   #:num-mixin
	   #:num
	   #:sorted-pair
	   #:sorted-pair-from-pair
	   #:opair
	   #:opair-p
	   #:pair<
	   #:opair-from-pair
	   #:distinct-pairs
	   #:distinct-couples
	   #:distinct-pairs-or-couples
	   #:pair<
	   #:sorted-cons
	   #:cons<
	   #:distinct-cons
	   #:oriented-mixin
	   #:oriented-p
	   #:*oriented*
	   #:date-string
	   #:time-string
	   #:strong-name
	   #:write-name
	   #:compose-name
	   #:prefix-length
	   #:trim-name
	   #:decompose-name
	   #:prefix-decompose-name
	   #:suffix-decompose-name
	   #:prefix-name
	   #:suffix-name
	   #:hash-table-equal-p
	   #:no-duplicates
	   #:list<
	   #:list-random-element
           #:remove-adjacent-duplicates
           #:make-key-value
           #:merge-key-value))

