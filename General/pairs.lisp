;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defun pair< (pair1 pair2)
  (or (< (first pair1) (first pair2))
      (and (= (first pair1) (first pair2))
	   (< (second pair1) (second pair2)))))

(defun sorted-pair (i j)
  (if (< i j) (list i j) (list j i)))

(defun sorted-pair-from-pair (pair)
  (sorted-pair (first pair) (second pair)))

(defun opair (i j orientation)
  (if (eq orientation t)
      (list i j)
      (sorted-pair i j)))

(defun opair-p (pair) (< (first pair) (second pair)))

(defun opair-from-pair (pair orientation)
  (opair (first pair) (second pair) orientation))

(defun distinct-couples (numbers)
  (loop for i in numbers
	nconc
	(loop for j in numbers
	      when (/= i j)
		collect (list i j))))

(defun distinct-pairs (numbers)
  (loop for i in numbers
	nconc
	(loop for j in numbers
	      when (< i j)
		collect (list i j))))
