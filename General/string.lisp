;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defgeneric decompose-string (string char)
  (:documentation "split SRTING into a list of substrings using CHAR as separator"))

(defmethod decompose-string ((string string) (char character))
  (let ((chars (coerce string 'list))
	(rblock ())
	(rblocks ()))
    (dolist (c chars)
      (if (eql c char)
	  (progn
	    (push rblock rblocks)
	    (setf rblock '()))
	  (push c rblock)))
    (push rblock rblocks)
    (let ((blocks '()))
      (dolist (rblock rblocks blocks)
	(push (coerce (nreverse rblock) 'string) blocks)))))

(defgeneric compose-string (strings char))
(defmethod compose-string ((strings list) (char character))
  (if (endp strings)
      ""
      (loop
	with s = (car strings)
	for string in (cdr strings)
	do (setq s (format nil "~A~A~A" s char string))
	finally (return s))))
