;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :general)

(defun vector-sum (v1 v2)
  (let* ((len1 (length v1))
	 (len2 (length v2))
	 (len (max len1 len2))
	 (min (min len1 len2))
	 (v (make-array len)))
    (loop for i from 0 below min
	  do (setf (aref v i) (+ (aref v1 i) (aref v2 i))))
    (loop for i from min below len1
	  do (setf (aref v i) (aref v1 i)))
    (loop for i from len1 below len2
	  do (setf (aref v i) (aref v2 i)))
    v))

(defun vector< (v1 v2)
  (loop for i from 0 below (length v1)
	do (let ((e1 (aref v1 i))
		 (e2 (aref v2 i)))
	     (cond ((< e1 e2) (return t))
		   ((> e1 e2) (return))))
	finally (return nil)))
