;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric biconnect (node1 node2 graph))
(defgeneric graph-2connected-components (graph))
(defgeneric graph-2connected-ndecomposition (graph))

(defgeneric min-label (arc))
(defmethod min-label ((arc arc))
  (min (label (origin arc)) (label (extremity arc))))

(defgeneric max-label (arc))
(defmethod max-label ((arc arc))
  (max (label (origin arc)) (label (extremity arc))))

(defgeneric origin-label (arc))
(defmethod origin-label ((arc arc))
  (label (origin arc)))

(let ((components '())
      (cpt -1)
      (stack '()))
  (labels ((init (g)
	     (setf components '())
	     (setf cpt -1)
	     (setf stack '())
	     (unset-nodes-weight g)
	     (unset-nodes-label g)
	     ))
    
    (defmethod biconnect ((v node) (u node) (g graph))
      (assert (not (label v)))
      (assert (or (null u) (typep u 'node)))
      (change-label v (incf cpt))
      (change-weight v (label v))
      ;;    (format t "v = ~A u = ~A l = ~A~%" 
      ;;      (num v) (if u (num u) 0) (mapcar (lambda (a) (num (extremity a))) (node-out-arcs v)))
      (arcs-map
       (lambda (arc)
	 (let ((w (extremity arc)))
	   (if (label w)
	       (when (and (<= (label w) (label v)) (not (eq w u)))
		 (push arc stack)
		 (setf (weight v) (min (weight v) (label w))))
	       (progn 
		 (push arc stack)
		 (biconnect w v g)
		 (setf (weight v) (min (weight v) (weight w)))
		 (when (>= (weight w) (label v))
					; (format t "articulation found ~A~%" v)
		   (let ((cc '()))
		     (loop
		       while (and stack
				  (> (origin-label (car stack))
				     (label v)))
		       do (push (pop stack) cc))
		     (push (pop stack) cc)
		     (push cc components)))))))
       (node-out-arcs v))
      )

    (defmethod graph-2connected-components ((g graph))
      (init g)
      (nodes-map
       (lambda (node)
	 (unless (label node)
	   (biconnect node (graph-model::make-node -1 nil) g)))
       (graph-nodes g))
      (unset-nodes-weight g)
      (unset-nodes-label g)
      components)))

(defgeneric component-articulation-node (component 2component))
(defmethod component-articulation-node ((c list) (2c list))
  (let ((nodes (get-nodes (remove c 2c :test #'eq))))
    (nodes-map
     (lambda (node)
       (when (nodes-member node nodes)
	 (return-from component-articulation-node node)))
     (get-nodes c))))

(defgeneric articulation-node-p (node 2components))
(defmethod articulation-node-p ((n node) (2c list))
  (some (lambda (c) (eq n (component-articulation-node c 2c))) 2c))

(defgeneric graph-articulation-nodes (2components))
(defmethod graph-articulation-nodes ((g graph))
  (articulation-nodes (graph-2connected-components g)))

(defgeneric articulation-nodes (2components))
(defmethod articulation-nodes ((2c list))
  (nodes-remove-if-not
   (lambda (p) (articulation-node-p p 2c))
   (get-nodes 2c)))
 
