;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)

(defclass abstract-arc (marked-mixin) ())

(defclass arc (abstract-arc weight-mixin key-mixin)
  ((origin :initarg :origin :reader origin)
   (extremity :initarg :extremity :reader extremity))
  (:documentation "an arc of an oriented graph"))

(defmethod object-key ((arc arc))
  (assert (node-p (origin arc)))
  (assert (node-p (extremity arc)))
  (format nil "~A ~A" (num (origin arc)) (num (extremity arc))))

(defgeneric arcs= (arc1 arc2))
(defmethod arcs= ((a1 arc) (a2 arc))
  (and (eq (origin a1) (origin a2))
       (eq (extremity a1) (extremity a2))))

(defmethod initialize-instance :after ((arc arc) &key &allow-other-keys)
  (assert (node-p (origin arc)))
  (assert (node-p (extremity arc)))
  (assert (or (null (weight arc)) (numberp (weight arc))))
  )

(defun make-arc (origin extremity &optional (weight nil))
  (make-instance
   'arc
   :origin origin
   :extremity extremity
   :weight weight))

(defmethod weighted-p ((arc arc))
  (weight arc))

(defmethod print-object ((arc arc) stream)
  (format stream "~A-~:[~;[~A]~]->~A"
	  (origin arc)
	  (weight arc)
	  (if (weight arc) (weight arc) (extremity arc))
	  (extremity arc)))

(defgeneric inverse-arc (arc))
(defmethod inverse-arc ((arc arc))
  (make-arc (extremity arc) (origin arc) (weight arc)))

(defgeneric edge-from-arc (arc)
  (:method ((arc arc))
    (let* ((extremities (container-contents (get-nodes arc)))
	   (origin (first extremities)))
    (make-arc origin (or (second extremities) origin) (weight arc)))))

(defgeneric mark-arc (arc))
(defmethod mark-arc ((arc arc))
  (mark-object arc))

(defgeneric unmark-arc (arc))
(defmethod unmark-arc ((arc arc))
  (unmark-object arc))
