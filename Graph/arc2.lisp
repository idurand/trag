;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)

;;; à enlever dans la version de base
(defgeneric arc-nodes (arc)
  (:method ((arc arc)) 
    (list (origin arc) (extremity arc))))
