;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)
;; todo a macro to generate code for specific containers

(defclass arcs (container) ())

(defgeneric make-arcs (arcs))
(defmethod make-arcs ((arcs list))
  (assert (every (lambda (arc) (typep arc 'arc)) arcs))
  (make-container-generic arcs 'arcs #'equal))

(defmethod print-object :before ((arcs arcs) stream)
  (format stream "arcs"))

(defgeneric make-empty-arcs ())
(defmethod make-empty-arcs ()
  (make-arcs '()))

(defgeneric arcs-contents (arcs))
(defmethod arcs-contents ((arcs arcs))
  (container-contents arcs))

(defgeneric arcs-union (arcs1 arcs2))
(defmethod arcs-union ((arcs1 arcs) (arcs2 arcs))
  (container-union arcs1 arcs2))

(defgeneric arcs-nunion (arcs1 arcs2))
(defmethod arcs-nunion ((arcs1 arcs) (arcs2 arcs))
  (container-nunion arcs1 arcs2))

(defgeneric arcs-difference (arcs1 arcs2))
(defmethod arcs-difference ((arcs1 arcs) (arcs2 arcs))
  (container-difference arcs1 arcs2))

(defgeneric arcs-ndifference (arcs1 arcs2))
(defmethod arcs-ndifference ((arcs1 arcs) (arcs2 arcs))
  (container-ndifference arcs1 arcs2))

(defgeneric arcs-intersection (arcs1 arcs2))
(defmethod arcs-intersection ((arcs1 arcs) (arcs2 arcs))
  (container-intersection arcs1 arcs2))

(defgeneric arcs-member (arc arcs))
(defmethod arcs-member ((arc arc) (arcs arcs))
  (container-member arc arcs))

(defgeneric arcs-nadjoin (arc arcs))
(defmethod arcs-nadjoin ((arc arc) (arcs arcs))
  (container-nadjoin arc arcs))

(defgeneric arcs-delete (arc arcs))
(defmethod arcs-delete ((arc arc) (arcs arcs))
  (container-delete arc arcs))

(defgeneric arcs-remove (arc arcs))
(defmethod arcs-remove ((arc arc) (arcs arcs))
  (container-remove arc arcs))

(defgeneric arcs-adjoin (arc arcs))
(defmethod arcs-adjoin ((arc arc) (arcs arcs))
  (container-adjoin arc arcs))

(defgeneric arcs-remove-if (pred arcs))
(defmethod arcs-remove-if (pred (arcs arcs))
  (container-remove-if pred arcs))

(defgeneric arcs-delete-if (pred arcs))
(defmethod arcs-delete-if (pred (arcs arcs))
  (container-delete-if pred arcs))

(defgeneric arcs-delete-if-not (pred arcs))
(defmethod arcs-delete-if-not (pred (arcs arcs))
  (container-delete-if-not pred arcs))

(defgeneric arcs-find-if (pred arcs))
(defmethod arcs-find-if (pred (arcs arcs))
  (container-find-if pred arcs))

(defgeneric arcs-find-if-not (pred arcs))
(defmethod arcs-find-if-not (pred (arcs arcs))
  (container-find-if-not pred arcs))

(defgeneric arcs-remove-if-not (pred arcs))
(defmethod arcs-remove-if-not (pred (arcs arcs))
  (container-remove-if-not pred arcs))

(defgeneric arcs-map (fun arcs))
(defmethod arcs-map (fun (arcs arcs))
  (container-map fun arcs))

(defgeneric arcs-every (pred arcs))
(defmethod arcs-every (pred (arcs arcs))
  (container-every pred arcs))

(defgeneric arcs-copy (arcs))
(defmethod arcs-copy ((arcs arcs))
  (container-copy arcs))

(defgeneric arcs-empty-p (arcs))
(defmethod arcs-empty-p ((arcs arcs))
  (container-empty-p arcs))

(defgeneric arcs-subset-p (arcs1 arcs2))
(defmethod arcs-subset-p ((arcs1 arcs) (arcs2 arcs))
  (container-subset-p arcs1 arcs2))

(defgeneric arcs-contents (arcs))
(defmethod arcs-contents ((arcs arcs))
  (container-contents arcs))

(defgeneric arcs-car (arcs))
(defmethod arcs-car ((arcs arcs))
  (container-car arcs))

(defgeneric arcs-pop (arcs))
(defmethod arcs-pop ((arcs arcs))
  (container-pop arcs))

(defgeneric arcs-mapcar (fun arcs))
(defmethod arcs-mapcar (fun (arcs arcs))
  (container-mapcar fun arcs))

(defgeneric arcs-size (arcs))
(defmethod arcs-size ((arcs arcs))
  (container-size arcs))

(defgeneric arcs-min-weight (arcs))
(defmethod arcs-min-weight ((arcs arcs))
  (assert (not (arcs-empty-p arcs)))
  (loop for arc in (arcs-contents arcs)
	minimize (weight arc)))

(defgeneric arcs-arc-min-weight (arcs))
(defmethod arcs-arc-min-weight ((arcs arcs))
  (let ((min (arcs-min-weight arcs)))
    (arcs-find-if
     (lambda (arc)
       (= (weight arc) min))
     arcs)))
