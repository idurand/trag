;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHORS: Elliot Blot, Irène Durand

(in-package :graph)

(defmacro expression-to-pairs (nb-nodes expression)
  `(loop
     for i from 1 to ,nb-nodes
     nconc (loop for j from 1 to ,nb-nodes
		 unless (= i j)
		   when ,expression
		     collect (list i j))))

(defun to-arcs (nb-nodes exp)
  (eval `(expression-to-pairs ,nb-nodes ,exp)))

(defun arith-graph (nb-nodes expression oriented)
  (setq expression (copy-tree expression)) ;; for being non destructive
  (nsubst nb-nodes 'n expression)
;;  (format t "expression:~A~%" expression)
  (graph-from-earcs-and-enodes
   (to-arcs nb-nodes expression)
   (iota nb-nodes 1)
   :oriented oriented))

(defun graph-cycle (n &optional (oriented nil))
  (rename-object
   (arith-graph
    n
    '(or (and (= i n) (= j 1)) (= j (1+ i)))
    oriented)
   (format nil "C~A" n)))

(defun graph-kn (n &optional (oriented nil))
  (rename-object
   (arith-graph n t oriented)
   (format nil "K~A" n)))

(defun graph-pn (n &optional (oriented nil))
  (rename-object
   (arith-graph n '(= j (1+ i)) oriented)
   (format nil "P~A" n)))

(defun graph-sn (n &optional (oriented nil))
  (rename-object
   (arith-graph (1+ n) '(and (= i 1) (> j 1)) oriented)
   (format nil "S~A" n)))

(defun graph-stable (n &optional (oriented nil))
  (rename-object
   (arith-graph n nil oriented)
   (format nil "Stable~A" n)))

(defun arith-graph-with-test (nb-nodes expression test oriented &rest args &key &allow-other-keys)
  (setq expression (copy-tree expression)) ;; for being non destructive
  (setq test (copy-tree test)) ;; for being non destructive
  ;;  (format t "~A~%" expression)
  ;;  (format t "args:~A~%" args)
  (loop
    while args
    do (let ((var (pop args))
	     (val (pop args)))
	 (nsubst  nb-nodes 'n expression)
	 (nsubst nb-nodes 'n test) ;; is this useful?
	 (nsubst val var test)
	 (nsubst val var expression)))
  (if (eval test)
      (graph-from-earcs-and-enodes (to-arcs nb-nodes expression) (iota nb-nodes 1) :oriented oriented)
      (warn "invalid test")))

(defun graph-square-grid (n)
  (rename-object
   (arith-graph-with-test
    (* n n)
    '(or (= (+ :x0 j) i) (and (= i (1+ j)) (/= (rem j :x0) 0)))
    '(= 0 (rem n :x0))
    nil
    :x0 n)
   (format nil "Grid~Ax~A" n n)))
