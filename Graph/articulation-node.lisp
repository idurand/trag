;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric articulation-node (graph))
(defgeneric articulation-node-aux (vnode unode graph))

(defgeneric articulation-aux (node1 node2 graph))

(let ((cpt -1))
  (labels ((init (g)
	     (setf cpt -1)
	     (unset-nodes-weight g)
	     (unset-nodes-label g)))
    (defmethod articulation-aux ((v node) (u node) (g graph))
      (assert (not (label v)))
      (assert (or (null u) (typep u 'node)))
      (change-label v (incf cpt))
      (change-weight v (label v))
;;    (format t "v = ~A u = ~A l = ~A~%" 
;;      (num v) (if u (num u) 0) (mapcar (lambda (a) (num (extremity a))) (node-out-arcs v)))
      (arcs-map
       (lambda (arc)
	 (let ((w (extremity arc)))
		 (if (label w)
		     (when (and (<= (label w) (label v)) (not (eq w u)))
		       (setf (weight v) (min (weight v) (label w))))
		     (progn 
		       (articulation-aux w v g)
		       (setf (weight v) (min (weight v) (weight w)))
		       (when (>= (weight w) (label v))
					; (format t "articulation found ~A~%" v)
			 (return-from articulation-aux v))))))
       (node-out-arcs v))
       (format *error-output* "no articulation found~%"))

    (defmethod articulation-node ((g graph))
      (init g)
      (let* ((nodes (nodes-contents (graph-nodes g)))
	     (articulation
	      (loop for node in nodes
		   unless (label node)
		     do (let ((a
			       (articulation-aux
				node
				(graph-model::make-node -1 nil) g)))
			  (when a
			    (return a))))))
	(unset-nodes-weight g)
	(unset-nodes-label g)
	articulation))))
