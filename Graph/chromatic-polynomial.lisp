;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defun make-polynomial (degree)
  (make-array (1+ degree)))

(defun polynomial-degree (p)
  (1- (length p)))

(defun monomial (p n)
  (aref p n))

(defun set-monomial (p n v)
  (setf (aref p n) v))

(defun polynomial-value (p x)
  (loop
    with s = 0
    for i from (polynomial-degree p) downto 0
    do (setq s (+ (* s x) (aref p i)))
    finally (return s)))

(defun polynomial-op (op p1 p2)
  (let* ((d1 (polynomial-degree p1))
	 (d2 (polynomial-degree p2))
	 (degree (max d1 d2))
	 (p (make-polynomial degree))
	 (m (min d1 d2)))
    (loop
      for d from 0 to m
      do (set-monomial p d (funcall op (monomial p1 d) (monomial p2 d))))
    (loop for d from (1+ m) to degree
	  do (set-monomial p d (monomial (if (> d1 d2) p1 p2) d)))
    p))
    
(defun polynomial-add (p1 p2) (polynomial-op #'+ p1 p2))
(defun polynomial-diff (p1 p2) (polynomial-op #'- p1 p2))
(defvar *polynomial-p0* #(0))
(defvar *polynomial-p1* #(0 1))
(defun polynomial-pi (i)
  (let ((p (make-polynomial i)))
    (set-monomial p i 1)
    p))

(defgeneric chromatic-polynomial (graph)
  (:method ((graph graph))
;;    (format *trace-output* "nodes:~A edges:~A~%" (graph-nodes graph) (graph-edges graph))
    (assert (not (oriented-p graph)))
    (let ((nb-nodes (graph-nb-nodes graph)))
      (if (zerop nb-nodes)
	  *polynomial-p0*
	  (let ((nb-edges (graph-nb-edges graph)))
	    (if (zerop nb-edges)
		(polynomial-pi nb-nodes)
		(let* ((pg-e (chromatic-polynomial
			      (graph-model::graph-delete-first-edge (graph-copy graph))))
		       (pg.e (chromatic-polynomial
			      (graph-model::graph-ncontract-first-edge (graph-copy graph)))))
		  (polynomial-diff pg-e pg.e))))))))

