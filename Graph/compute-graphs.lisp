;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

;; bruno mail du 2016-01-30
(defun graph-pn-with-diagos (n)
  (arith-graph
   n
   '(or 
     (= j (1+ i))
     (= (+ i j) n))
   nil))

(defun graph-complete-bipartite (r s)
  (rename-object
   (arith-graph-with-test
    (+ r s)
    '(and (<= i :r) (> j :r) (<= j :rs))
   t
   nil
   :r r
   :rs (+ r s))
   (format nil "K~A,~A" r s)))

(defun complete-bipartite (r s)
  (graph-from-earcs
   (loop 
     for i from 1 to r
     nconc (loop
	     for j from 1 to s
	     collect (list i (+ r j))))))
