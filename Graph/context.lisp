;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defclass graph-context (graph)
  ((cwd-term :initarg :cwd-term :initform nil :accessor cwd-term)))

(defgeneric make-graph-context (graph))
(defmethod make-graph-context ((graph graph))
  (make-instance 'graph-context :graph graph))
