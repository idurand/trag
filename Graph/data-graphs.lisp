;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defparameter *edges-mcgee*
'((1 2) (1 8) (1 24) (2 3) (2 19) (3 4) (3 15) (4 5) (4 11) (5 6) (5 22) (6 7) (6 18)
  (7 8) (7 14) (8 9) (9 10) (9 21) (10 11) (10 17) (11 12) (12 13) (12 24) (13 14) (13 20)
  (14 15) (15 16) (16 17) (16 23) (17 18) (18 19) (19 20) (20 21) (21 22) (22 23) (23 24)))

(defparameter *paths-mcgee*
  '((1 13 12 5 4 16 17 24 23 6 7 19 20 3 2 9 8 15 14 21 22 10 11 18 19) (24 1 2) (3 4) (5 6) (7 8)
    (9 10)
    (11 12) (13 14) (15 16) (17 18) (20 21) (22 23)))

(defun graph-mcgee ()
  (rename-object (graph-from-paths *paths-mcgee*) "McGee"))

(defparameter *edges-petersen*
  '((1 2) (1 5) (1 9) (2 3) (2 7) (3 4) (3 10) (4 5)
    (4 8) (5 6) (6 7) (6 10) (7 8) (8 9) (9 10)))

(defparameter *paths-petersen*
  '((1 2 3 4 5 1 9 10 6 7 8 9) (2 7) (3 10) (4 8) (5 6)))

(defun graph-petersen ()
  (rename-object (graph-from-paths *paths-petersen*) "Petersen"))

(defun graph-opetersen ()
  (rename-object (graph-from-paths *paths-petersen* :oriented t) "oPetersen"))

(defun graph-grunbaum ()
  (rename-object
   (graph-from-earcs
    '((1 2) (1 3) (1 4) (1 5) (2 1) (2 3) (2 5) (2 6) (3 1) (3 2) (3 4) (3 6) (4 1)
      (4 3) (4 5) (4 6) (5 1) (5 2) (5 4) (5 6) (6 2) (6 3) (6 4) (6 5)))
    "Grunbaum"))

(defparameter *edges-envelop*
  '((1 2) (1 4) (1 5) (2 3) (2 4) (2 5) (3 4) (4 5)))

(defparameter *paths-envelop* '((1 2 3 4 5 2 4 1 5)))

(defparameter *envelop*
  (graph-from-earcs *edges-envelop*))

(defparameter *oenvelop*
  (graph-from-earcs *edges-envelop* :oriented t))

(defparameter *oenvelop*
  (graph-from-paths *paths-envelop* :oriented t))

(defparameter *paths-house* '((1 2 3 5 4 2) (1 3)))

(defparameter *house*
  (graph-from-paths *paths-house*))


(defparameter *paths-carres* '((1 2 3 5 6 7 8 1) (2 4) (4 6) (6 8) (2 8)))

(defparameter *carres*
  (graph-from-paths *paths-carres*))

(defparameter *arcs-go* '((1 2) (1 4) (2 4)  (3 2) (4 3)))
(defparameter *go* (graph-from-earcs-and-enodes
		    *arcs-go* '(5) :oriented t))

(defparameter *arcs-bp* '((1 4) (1 5) (2 6) (3 6) (5 2)))
(defparameter *bp* (graph-from-earcs *arcs-bp* :oriented t))

(defparameter *arcs-508* '((12 5 5) (12 3 3) (3 5 2) (3 9 6) (5 3 1) (5 9 4) (5 11 6) (9 11 2) (11 12 3) (11 9 7)))

(defparameter *o508* (graph-from-earcs *arcs-508* :oriented t))
(defparameter *508* (graph-from-earcs *arcs-508*))

(defparameter *edges-490* '((1 2 4) (2 3 8) (3 4 7) (4 5 9) (5 6 10) (6 7 2)
			    (7 8 1) (8 1 8)
			    (2 8 11) (8 9 7) (3 9 2) (3 6 4) (9 6 14)))

(defparameter *490* (graph-from-earcs *edges-490*))

(defparameter *vertex* (graph-from-earcs-and-enodes '() '(1)))
(defparameter *overtex* (graph-from-earcs-and-enodes '() '(1) :oriented t))

(defparameter *edges-434* '((1 2) (1 3) (2 3) (2 4) (5 6) (5 7) (8 9)))
(defparameter *434* (graph-from-earcs-and-enodes *edges-434* '(10)))

(defparameter *arcs-518* '((1 2 10) (1 4 5) (2 3 1) (2 4 2) (3 5 4) (4 2 3) (4 3 9) (4 5 2) (5 1 7) (5 3 6)))
(defparameter *518* (graph-from-earcs *arcs-518* :oriented t))

(defparameter *edges-wiki-dijkstra* '((1 2 7) (1 3 9) (1 6 14)
				  (2 3 10) (2 4 14)
				  (3 4 11) (3 6 2)
				  (4 5 6) (5 6 9)))
(defparameter *wiki-dijkstra*
  (graph-from-earcs *edges-wiki-dijkstra*))

(defparameter *edges-wiki-prim* '((1 2 7) (1 4 5) (2 3 8) (2 4 9) (2 5 7)
			       (3 5 5) (4 5 15) (4 6 6) (5 6 9) (5 7 9)
			       (6 7 11)))

(defparameter *wiki-prim* (graph-from-earcs *edges-wiki-prim*))

(defparameter *villes-arcs*
  '(((1 "Paris") (2 "Lyon") 2)
    ((2 "Lyon") (3 "Marseille") 2)
    ((4 "Toulouse") (5 "Bordeaux") 2)
    ((1 "Paris") (5 "Bordeaux") 3)
    ((3 "Marseille") (4 "Toulouse") 5)))
  
(defparameter *villes*
  (graph-from-earcs-and-enodes *villes-arcs* '((6 "Ajaccio"))))

(defparameter *2c5* (graph-from-paths '((5 4 3 2 1 3) (1 6 2))))

(defparameter *2c4* (graph-from-paths '((1 3 2 1 4))))

(defparameter *2c2* (graph-from-paths '((2 1))))
(defparameter *2c3* (graph-from-paths '((3 2 1))))

(defparameter *2c9*
  (graph-from-paths
   '((2 3 4 2)  (9 8 7 9) (1 5) (7 1 6 5 2 1))))

(defparameter *2c9*
  (graph-from-earcs
   '((9 8) (8 7) (9 7) (6 5) (4 3) (5 2) (4 2) (3 2) (7 1) (6 1) (5 1) (2 1))))

(defparameter *3c*
  (graph-from-paths '((1 4 3 2 1) (4 5 6 7 8 5 9 4))))

(defparameter *not-connected*
  (graph-from-paths '((1 2 3 1) (4 5 6))))

(defparameter *p11* (graph-from-paths '((11 10 9 8 7 6 5 4 3 2 1))))
(defparameter *p10* (graph-from-paths '((10 9 8 7 6 5 4 3 2 1))))
(defparameter *p9* (graph-from-paths '((9 8 7 6 5 4 3 2 1))))
(defparameter *p8* (graph-from-paths '((8 7 6 5 4 3 2 1))))
(defparameter *p7* (graph-from-paths '((7 6 5 4 3 2 1))))
(defparameter *p6* (graph-from-paths '((6 5 4 3 2 1))))
(defparameter *p5* (graph-from-paths '((5 4 3 2 1))))
(defparameter *p4* (graph-from-paths '((1 2 3 4))))
(defparameter *p3* (graph-from-paths '((1 2 3))))
(defparameter *p2* (graph-from-paths '((1 2))))
(defparameter *p1* (graph-from-paths '((1))))
(defparameter *k3* (graph-from-paths '((1 2 3 1))))
(defparameter *k4* (graph-from-paths '((1 2 3 4 1 3) (2 4))))
(defparameter *k5* (graph-from-paths '((1 2 3 4 5 1 3 5 2 4 1))))
(defparameter *c3* *k3*)
(defparameter *c4* (graph-from-paths '((1 2 3 4 1))))
(defparameter *c5* (graph-from-paths '((1 2 3 4 5 1))))
(defparameter *c6* (graph-from-paths '((1 2 3 4 5 6 1))))
(defparameter *c7* (graph-from-paths '((1 2 3 4 5 6 7 1))))
(defparameter *c8* (graph-from-paths '((1 2 3 4 5 6 7 8 1))))
(defparameter *c9* (graph-from-paths '((1 2 3 4 5 6 7 8 9 1))))
(defparameter *c10* (graph-from-paths '((1 2 3 4 5 6 7 8 9 10 1))))
(defparameter *c11* (graph-from-paths '((1 2 3 4 5 6 7 8 9 10 11 1))))
(defparameter *c12* (graph-from-paths '((1 2 3 4 5 6 7 8 9 10 11 12 1))))
(defparameter *c13* (graph-from-paths '((1 2 3 4 5 6 7 8 9 10 11 12 13 1))))
(defparameter *g3*
  (graph-from-paths
   '((1 2 3)
     (4 5 6)
     (7 8 9)
     (1 4 7)
     (2 5 8)
     (3 6 9))))

(defparameter *g4*
  (graph-from-paths
   '(( 1  2  3  4)
     ( 5  6  7  8)
     ( 9 10 11 12)
     ( 3 14 15 16)
     ( 1  5  9 13)
     ( 2  6 10 14)
     ( 3  7 11 15))))

(defparameter *g5*
  (graph-from-paths
   '(( 1  2  3  4  5)
     ( 6  7  8  9 10)
     (11 12 13 14 15)
     (16 17 18 19 20)
     (21 22 23 24 25)
     ( 1  6 11 16 21)
     ( 2  7 12 17 22)
     ( 3  8 13 18 23)
     ( 4  9 14 19 24)
     ( 5 10 15 20 25))
  ))

(defparameter *g6*
  (graph-from-paths
   '((1 2 3 4 5 6)
     (7 8 9 10 11 12)
     (13 14 15 16 17 18)
     (19 20 21 22 23 24)
     (25 26 27 28 29 30)
     (31 32 33 34 35 36)
     (1 7 13 19 25 31)
     (2 8 14 20 26 32)
     (3 9 15 21 27 33)
     (4 10 16 22 28 34)
     (5 11 17 23 29 35)
     (6 12 18 24 30 36))
  ))
     
(defparameter *g7*
  (graph-from-paths
   '((1 2 3 4 5 6 7)
     (8 9 10 11 12 13 14)
     (15 16 17 18 19 20 21)
     (22 23 24 25 26 27 28)
     (29 30 31 32 33 34 35)
     (36 37 38 39 40 41 42)
     (43 44 45 46 47 48 49)
     (1 8 15 22 29 36 43)
     (2 9 16 23 30 37 44)
     (3 10 17 24 31 38 45)
     (4 11 18 25 32 39 46)
     (5 12 19 26 33 40 47)
     (6 13 20 27 34 41 48)
     (7 14 21 28 35 42 49))
  ))

(defparameter *g8*
  (graph-from-paths
   '(( 1  2  3  4  5  6  7  8)
     ( 9 10 11 12 13 14 15 16)
     (17 18 19 20 21 22 23 24)
     (25 26 27 28 29 30 31 32)
     (33 34 35 36 37 38 39 40)
     (41 42 43 44 45 46 47 48)
     (49 50 51 52 53 54 55 56)
     (57 58 59 60 61 62 63 64)
     ( 1  9 17 25 33 41 49 57)
     ( 2 10 18 26 34 42 50 58)
     ( 3 11 19 27 35 43 51 59)
     ( 4 12 20 28 36 44 52 60)
     ( 5 13 21 29 37 45 53 61)
     ( 6 14 22 30 38 46 54 62)
     ( 7 15 23 31 39 47 55 63)
     ( 8 16 24 32 40 48 56 64))
  ))

(defparameter *g9*
  (graph-from-paths
   '(( 1  2  3  4  5  6  7  8  9)
     (10 11 12 13 14 15 16 17 18)
     (19 20 21 22 23 24 25 26 27)
     (28 29 30 31 32 33 34 35 36)
     (37 38 39 40 41 42 43 44 45)
     (46 47 48 49 50 51 52 53 54)
     (55 56 57 58 59 60 61 62 63)
     (64 65 66 67 68 69 70 71 72)
     (73 74 75 76 77 78 79 80 81)
     ( 1 10 19 28 37 46 55 64 73)
     ( 2 11 20 29 38 47 56 65 74)
     ( 3 12 21 30 39 48 57 66 75)
     ( 4 13 22 31 40 49 58 67 76)
     ( 5 14 23 32 41 50 59 68 77)
     ( 6 15 24 33 42 51 60 69 78)
     ( 7 16 25 34 43 52 61 70 79)
     ( 8 17 26 35 44 53 62 71 80)
     ( 9 18 27 36 45 54 63 72 81))
  ))

(defparameter *g45* (graph-rgrid 4 5))

(defparameter *g10*
  (graph-from-paths
   '(( 1  2  3  4  5  6  7  8  9  10)
     (11 12 13 14 15 16 17 18 19  20)
     (21 22 23 24 25 26 27 28 29  30)
     (31 32 33 34 35 36 37 38 39  40)
     (41 42 43 44 45 46 47 48 49  50)
     (51 52 53 54 55 56 57 58 59  60)
     (61 62 63 64 65 66 67 68 69  70)
     (71 72 73 74 75 76 77 78 78  80)
     (81 82 83 84 85 86 87 88 89  90)
     (91 92 93 94 95 96 97 98 99 100)
     ( 1 11 21 31 41 51 61 71 81  91)
     ( 2 12 22 32 42 52 62 72 82  92)
     ( 3 13 23 33 43 53 63 73 83  93)
     ( 4 14 24 34 44 54 64 74 84  94)
     ( 5 15 25 35 45 55 65 75 85  95)
     ( 6 16 26 36 46 56 66 76 86  96)
     ( 7 17 27 37 47 57 67 77 87  97)
     ( 8 18 28 38 48 58 68 78 88  98)
     ( 9 19 29 39 49 59 69 79 89  99)
     (10 20 30 40 50 60 70 80 90 100))
  ))

(defparameter *g34*
  (graph-from-paths
   '((1  2  3  4)
     (5  6  7  8)
     (9 10 11 12)
     (1  5  9)
     (3  7 11)
     (4  8 12))
  ))

(defparameter *g35*
  (graph-from-paths
   '((1 2 3 4 5)
     (6 7 8 9 10)
     (11 12 13 14 15)
     (1 6  11)
     (2  7  12)
     (2  7  12)
     (3 8 13) 
     (4 9 14)
     (5 10 15))
  ))


(defparameter *g54*
  (graph-from-paths
   '(( 1  2  3  4)
     ( 5  6  7  8)
     ( 9 10 11 12)
     (13 14 15 16)
     (17 18 19 20)
     ( 1  5  9 13 17)
     ( 2  7 10 14 18)
     ( 3  8 11 15 19)
     ( 4  9 12 16 20))
  ))

(defparameter *paths-rp* '((1 2 3 4 5 6 1) (1 4) (2 6) (3 5)))
(defparameter *rp* (graph-from-paths *paths-rp*))
