;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defun save-dimacs-graph-stream (graph stream)
  (let ((oriented (oriented-p graph)))
    (format stream "p ~:[arc~;edge~] ~A ~A~%" (not oriented)
	    (graph-nb-nodes graph) (graph-nb-edges graph))
    (loop
      with oriented = (oriented-p graph)
      for earc in (mapcar (lambda (edge)
			    (opair (enode-num (first edge))
				   (enode-num (second edge))
				   oriented))
			  (if oriented
			      (graph::graph-to-earcs graph)
			      (graph::graph-to-eedges graph)
			      ))
     do (format stream "~:[a~;e~] ~A ~A~%" (not oriented) (first earc) (second earc)))))
  
(defun save-dimacs-graph-absolute (graph filename)
  (with-open-file (stream filename :direction
			  :output :if-does-not-exist :create :if-exists :supersede)
    (save-dimacs-graph-stream graph stream)))

(defun save-dimacs-graph (graph filename)
  (unless (char= (aref filename 0) #\/)
    (setq filename (absolute-filename *graph-directory* filename)))
  (save-dimacs-graph-absolute graph filename))
