;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric nelimination-ordering (graph function))

(defmethod nelimination-ordering ((graph graph) (fun function))
  (loop
    until (graph-empty-p graph)
    collect (let ((node (funcall fun graph)))
	      (prog1 (num node)
		(graph-delete-node node graph)))))

(defgeneric elimination-ordering (graph function))
(defmethod elimination-ordering ((graph graph) (fun function))
  (nelimination-ordering (graph-copy graph) fun))

(defgeneric max-degree-elimination-ordering (graph))
(defmethod max-degree-elimination-ordering ((graph graph))
  (elimination-ordering graph #'graph-degree-max-node))

(defgeneric min-degree-elimination-ordering (graph))
(defmethod min-degree-elimination-ordering ((graph graph))
  (elimination-ordering graph #'graph-degree-min-node))


