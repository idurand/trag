;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric graph-eulerian-p (graph))
(defmethod graph-eulerian-p ((g graph))
  (let ((n (nodes-size
	    (nodes-remove-if
	     (lambda (n)
	       (evenp (out-degree n g)))
	     (graph-nodes g)))))
    (or (zerop n) (= 2 n))))

  
(defun eulerian-next-arc (node path)
  (let* ((path-arcs (path-arcs path))
	 (path-nodes (mapcar #'origin path-arcs))
	 (next-arcs (node-out-arcs node)))
    (or (arcs-find-if-not
	 (lambda (arc)
	   (member (extremity arc) path-nodes :test #'eq))
	 next-arcs)
	(let ((last (inverse-arc (car path-arcs))))
	  (arcs-find-if
	   (lambda (arc)
	     (arcs= arc last))
	   next-arcs))
	(arcs-car next-arcs))))
		   
(defgeneric graph-eulerian-circuit (graph))
(defmethod graph-eulerian-circuit ((graph graph))
  (let* ((g (graph-copy graph))
	 (nodes (get-nodes g))
	 (paths '()))
    (loop
       until (nodes-empty-p nodes)
       for path = (make-path) ;; then (make-path)
       for start-node = (nodes-car nodes) ;; then (nodes-car nodes)
       for out-arc = (arcs-car (node-out-arcs start-node)) then (arcs-car (node-out-arcs start-node))
       for node = (extremity out-arc) ;; then (extremity out-arc)
       do (arcs-delete out-arc (node-out-arcs start-node))
       do (nodes-delete start-node nodes)
       do (path-rpush out-arc path)
       do (loop
	     until (eq start-node node)
	     until (arcs-empty-p (node-out-arcs node))
	     do (nodes-delete node nodes)
	     do (setf out-arc (eulerian-next-arc node path))
	     do (arcs-delete out-arc (node-out-arcs node))
	     do (setf node (extremity out-arc))
	     do (path-rpush out-arc path)
	     finally (push path paths)))
    (nreverse
     (delete-duplicates
      (mapcan
       (lambda (path)
	 (nreverse (path-origins-list path)))
       paths)))))

