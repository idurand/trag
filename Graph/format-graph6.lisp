;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)
;; graph6 format
(defvar *byte-size* 8)
(defvar *part-size* 6)
(defvar *diff-size* (- *byte-size* *part-size*))
(defvar *delta*)
(setq *delta* (1- (expt *byte-size* 2)))

(defun make-bits (n) (make-array n :element-type 'bit))
(defun make-byte () (make-bits *byte-size*))

(defun pad-bits (bits size)
  (let ((len (length bits)))
    (assert (<= len size))
    (if (< len size)
	(adjust-array bits size)
	bits)))

(defun pad-bits-to-multiple (bits part-size)
  (let ((len (length bits)))
    (multiple-value-bind (quotient remainder) (ceiling len part-size)
      (declare (ignore quotient))
      (pad-bits bits (+ len (abs remainder))))))

(defun bits-to-fixed (bits fixed-size)
  (let ((bits-size (length bits))
	(fixed (make-bits fixed-size)))
    (when (= fixed-size bits-size)
      (return-from bits-to-fixed bits))
    (assert (< bits-size fixed-size))
    (loop
      for i from 0 below bits-size
      and j from (- fixed-size bits-size)
      do (setf (aref fixed j) (aref bits i)))
    fixed))

(defun part-to-byte (part)
  (bits-to-fixed part *byte-size*))

(defun byte-to-part (byte)
  (let ((part (make-bits *part-size*)))
    (loop
      for i from 0 below *part-size*
      do (setf (aref part i) (aref byte (+ *diff-size* i))))
    part))

(defun split-bits (bits part-size)
  (assert (zerop (mod (length bits) part-size)))
  (let ((nb-bytes (ceiling (length bits) part-size)))
    (loop
       with i = -1
       repeat nb-bytes
       collect
	 (loop
	    with part = (make-bits part-size)
	    for j from 0 below part-size
	    do (let ((bit (aref bits (incf  i))))
		 (unless (zerop bit)
		   (setf (aref part j) bit)))
	    finally (return part)))))

(defun parts-to-bytes (parts)
  (mapcar #'part-to-byte parts))

(defun bytes-to-parts (bytes)
  (mapcar #'byte-to-part bytes))

(defun parts-to-bits (parts)
  (let ((nb-parts (length parts)))
    (assert (plusp nb-parts))
    (let* ((part-size (length (car parts)))
	   (bits (make-bits (* nb-parts part-size))))
      (loop
	 with i = 0
	 for part in parts
	 do (loop
	       for j from 0 below part-size
	       do (let ((bit (aref part j)))
		    (unless (zerop bit)
		      (setf (aref bits i) bit))
		    (incf i))))
      bits)))

(defun bits-to-integer (bits)
  (let ((len (length bits))
	(n (aref bits 0)))
    (loop 
       for j from 1 below len
       do (setq n (+ (* n 2) (if (zerop (aref bits j)) 0 1))))
    n))
	   
(defun integer-to-bits (n)
  (when (zerop n)
    (return-from integer-to-bits (make-bits 1)))
  (let* ((len (1+ (floor (log n 2))))
	 (bits (make-bits len)))
    (loop 
       while (plusp n)
       for i from (1- len) downto 0
       do (multiple-value-bind (q r) (floor n 2)
	    (unless (zerop r)
	      (setf (aref bits i) r))
	    (setq n q)))
    bits))

(defun integer-to-fixed (n fixed-size)
  (bits-to-fixed (integer-to-bits n) fixed-size))

(defun integer-to-byte (n)
  (integer-to-fixed n *byte-size*))

(defun nb-additional-bytes (n)
  (cond 
    ((<= n 62) 0)
    ((< n 258047) 1)
    (t (assert (<= n 68719476735)) 2)))

(defun nb-parts (n)
  (cond 
    ((<= n 62) 1)
    ((< n 258047) 3)
    (t (assert (<= n 68719476735)) 6)))

(defun fixed-to-bytes (fixed)
  (assert (zerop (mod (length fixed) *part-size*)))
  (parts-to-bytes
   (split-bits fixed *part-size*)))

(defun concat-parts (parts)
  (let ((bits (make-bits (reduce #'+ (mapcar #'length parts)))))
    (loop
       with k = -1
       for part in parts
       do (loop
	     for i from 0 below (length part)
	     do (setf (aref bits (incf k)) (aref part i))))
    bits))

(defun bytes-to-fixed (bytes)
  (concat-parts
   (bytes-to-parts bytes)))

(defun collect-edges (n)
  (loop
    with i = 0
    and j = 1
    for k from 0
    until (= j n)
    collect (prog1 
		(list i j)
	      (when (= (incf i) j)
		(setq i 0)
		(incf j)))))

(defun edge-bit-vector (n eedges)
  (let* ((size (/ (* n (1- n)) 2))
	 (all-edges (collect-edges n))
	 (bits (make-bits size)))
    (loop
      for k from 0
      for e in all-edges
      when (member e eedges :test #'equal)
	do (setf (aref bits k) 1))
    bits))

(defun additional-bytes (nb-bytes)
  (make-list nb-bytes :initial-element (integer-to-byte *delta*)))

(defun bits-to-fixed-nb-parts (bits nb-parts)
  (bits-to-fixed
   bits
   (* *part-size* nb-parts)))

(defun bits-to-bytes (bits nb-parts)
  (fixed-to-bytes
   (bits-to-fixed-nb-parts bits nb-parts)))

(defun n-to-bytes (n)
  (let ((nb-parts (nb-parts n)))
    (append
     (additional-bytes nb-parts)
     (bits-to-bytes 		  
      (integer-to-bits n)
      nb-parts))))

(defun get-nb-additional (shorts)
  (let ((nb-bytes 0))
    (when (= 126 (first shorts))
      (incf nb-bytes)
      (when (= 126 (second shorts))
	(incf nb-bytes)))
    nb-bytes))
      
(defun edges-to-bytes (n edges)
  (fixed-to-bytes
   (pad-bits-to-multiple 
    (edge-bit-vector n edges)
    *part-size*)))

(defun eedges-from-bits (n bits)
  (let ((all-edges (collect-edges n))
	(edges '()))
    (assert (>= (length bits) (length all-edges)))
    (loop
       for k from 0
       for e in all-edges
       unless (zerop (aref bits k))
       do (push e edges))
    edges))

(defun bytes-to-edges (n bytes)
  (eedges-from-bits 
   n
   (concat-parts (bytes-to-parts bytes))))

(defun graph6-bytes (n edges)
  (append (n-to-bytes n) (edges-to-bytes n edges)))

(defun bytes-to-shorts (bytes)
  (mapcar #'byte-to-short bytes))

(defun shorts-to-chars (shorts)
  (mapcar #'code-char shorts))

(defun byte-to-short (byte)
  (+ *delta*
     (bits-to-integer byte)))

(defun short-to-byte (short)
  (integer-to-byte (- short *delta*)))

(defun byte-to-char (byte)
  (code-char
   (byte-to-short byte)))

(defun bytes-to-chars (bytes)
  (mapcar #'byte-to-char bytes))

(defun char-to-short (char)
  (char-code char))

(defun chars-to-shorts (chars)
  (mapcar #'char-to-short chars))

(defun shorts-to-bytes (shorts)
  (mapcar #'short-to-byte shorts))

(defun chars-to-bytes (chars)
  (shorts-to-bytes (chars-to-shorts chars)))

(defun graph6-shorts (n edges)
  (bytes-to-shorts (graph6-bytes n edges)))

(defun graph6-chars (n edges)
  (shorts-to-chars (graph6-shorts n edges)))

(defun write-chars-to-stream (chars stream)
  (loop
    for char in chars
    do (write-char char stream)))

(defun read-chars-from-stream (stream)
  (loop
    for char = (read-char stream nil nil) ;; then (read-char stream nil nil)
    while char
    collect char))

(defun chars-to-parts (chars)
  (mapcar #'integer-to-bits (chars-to-shorts chars)))

(defun egraph-from-bytes (bytes)
  (let ((n (bits-to-integer (first bytes))))
    (list
     (bytes-to-edges n (cdr bytes))
     (iota n))))

(defun egraph-from-shorts (shorts)
  (let ((nb-bytes (get-nb-additional shorts)))
    (egraph-from-bytes
     (shorts-to-bytes (nthcdr nb-bytes shorts)))))

(defun egraph-from-chars (chars)
  (egraph-from-shorts (chars-to-shorts chars)))

(defun egraph-from-string (string)
  (egraph-from-chars (coerce string 'list)))

(defun graph-from-string (string)
  (let ((g 
	 (apply
	  #'graph-from-earcs-and-enodes
	  (egraph-from-string string))))
    (nodes-map (lambda (node)
		 (incf (num node))) (graph-nodes g))
    g))

(defgeneric geng-enumerator (nb-nodes options)
  (:method ((nb-nodes integer) (options string))
    (let* ((process
	     (sb-ext::run-program 
	      (which-program "geng")
	      (list options (format nil "~A" nb-nodes))
	      :output :stream
	      :wait nil))
	   (stream (sb-ext::process-output process)))
      (make-stream-enumerator stream :process process))))

(defgeneric graph-enumerator (nb-nodes &optional options)
  (:method ((nb-nodes integer) &optional (options "-cqd2"))
    (make-funcall-enumerator
     #'graph-from-string
     (geng-enumerator nb-nodes options))))
