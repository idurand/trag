;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric nsubgraph-edges (edge-cut graph)
  (:method ((edge-cut arcs) (graph graph))
    (assert (arcs-subset-p edge-cut (graph-edges graph)))
    (arcs-map
     (lambda (edge)
       (unless (arcs-member edge edge-cut)
	 (graph-delete-edge edge graph)))
     (graph-edges graph))
    (delete-unused-nodes graph)))

(defgeneric cut-p (graph nodes1 nodes2)
  (:method ((g graph) (nodes1 nodes) (nodes2 nodes))
    (partition-p (graph-nodes g) (list nodes1 nodes2))))

(defgeneric arc-between-two-sets-p (arc nodes1 nodes2)
  (:method ((arc arc) (nodes1 nodes) (nodes2 nodes))
    (and
     (nodes-member (origin arc) nodes1)
     (nodes-member (extremity arc) nodes2))))

(defgeneric arcs-between-two-sets (nodes1 nodes2 graph)
  (:method ((nodes1 nodes) (nodes2 nodes) (g graph))
    (remove-if-not
     (lambda (arc)
       (arc-between-two-sets-p arc nodes1 nodes2))
     (graph-arcs g))))

(defgeneric arc-crossing-cut-p (arc cut graph)
  (:method ((arc arc) (cut nodes) (g graph))
    (arc-between-two-sets-p
     arc
     cut
     (nodes-difference (graph-nodes g) cut))))

(defgeneric edge-crossing-cut-p (arc cut graph)
  (:method ((arc arc) (cut nodes) (g graph))
    (or (arc-crossing-cut-p arc cut g)
	(arc-crossing-cut-p (inverse-arc arc) cut g))))

(defgeneric arcs-crossing-cut (cut graph)
  (:method ((cut nodes) (g graph))
    (remove-if-not
     (lambda (arc)
       (arc-crossing-cut-p arc cut g))
     (graph-arcs g))))

(defgeneric minimal-object (l key)
  (:method ((l list) (key function))
    (loop with min = (car l)
	  for e in (cdr l)
	  when (< (funcall key e) (funcall key min))
	    do (setq min e)
	  finally (return min))))

(defgeneric edge-cut-for-minimum-spanning-tree (graph ponderation)
  (:documentation "arbre couvrant minimal selon ponderation")
  (:method ((g graph) ponderation)
    (let* ((edges (graph-edges g))
	   (edge (minimal-object (arcs-contents edges) ponderation))
	   (origin (origin edge))
	   (extremity (extremity edge))
	   (nodes (nodes-delete
		   extremity
		   (nodes-remove origin (graph-nodes g))))
	   (edge-cut (make-arcs (list edge))))
      (arcs-delete edge edges)
      (loop
	until (nodes-empty-p nodes)
	do (let* ((crossing-edges
		    (arcs-remove-if-not
		     (lambda (edge)
		       (edge-crossing-cut-p edge (get-nodes edge-cut) g))
		     edges))
		  (edge
		    (minimal-object (arcs-contents crossing-edges) ponderation)))
	     (arcs-delete edge edges)
	     (nodes-delete (extremity edge) (nodes-delete (origin edge) nodes))
	     (arcs-nadjoin edge edge-cut)))
      edge-cut)))

(defgeneric graph-minimum-spanning-tree (graph &optional ponderation)
  (:documentation "arbre couvrant minimal selon ponderation")
  (:method ((g graph) &optional (ponderation #'weight))
    (when (oriented-p g)
      (warn "applying acm to an oriented graph"))
    (assert (graph-connected-p g))
    (setf g (graph-copy g))
    (unless (weighted-p g)
      (set-arcs-weight g 1))
    (let ((edge-cut (edge-cut-for-minimum-spanning-tree g ponderation)))
      (nsubgraph-edges edge-cut g))))
