;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric oriented-paths-from-node (node graph))
(defmethod oriented-paths-from-node ((node node) (g graph))
  ;; paths (without duplicated  nodes except from the extremities) of length at least 1 starting from node
  (when (node-marked-p node)
    (return-from oriented-paths-from-node (list (make-path))))
  (mark-node node)
  (prog1
      (let ((out-arcs (node-out-arcs node))
	    (paths '()))
	(when (arcs-empty-p out-arcs)
	  (return-from oriented-paths-from-node paths))
	(arcs-map
	 (lambda (arc)
	   (setf paths (nconc
			(let ((subpaths
				(oriented-paths-from-node (extremity arc) g)))
			  (if subpaths
			      (distribute-arc-on-paths arc subpaths)
			      (list (make-path (list arc)))))
			paths)))
	 out-arcs)
	paths)
    (unmark-node node)))

(defgeneric unoriented-paths-from-node (node graph))
(defmethod unoriented-paths-from-node ((node node) (g graph))
  ;; side-effect (marks nodes)
  "returns a list of paths"
  (when (node-marked-p node)
    (return-from unoriented-paths-from-node (list (make-path))))
  (mark-node node)
  (prog1
      (let ((out-arcs (node-out-arcs node))
	    (paths '()))
	(arcs-map
	 (lambda (arc)
	   (setf paths (nconc paths
			      (let ((subpaths
				      (remove-if
				       (lambda (path)
					 (arcs-member
					  (inverse-arc arc) 
					  (make-arcs (path-arcs path)))) ;; to fix
				       (unoriented-paths-from-node (extremity arc) g))))
				(if subpaths
				    (distribute-arc-on-paths arc subpaths)
				    (list (make-path (list arc))))))))
	 out-arcs)
	paths)
    (unmark-node node)))
	
(defgeneric paths-from-node (node graph))
(defmethod paths-from-node ((node node) (g graph))
  (graph-unmark-all-nodes g)
  (if (oriented-p g)
      (oriented-paths-from-node node g)
      (unoriented-paths-from-node node g)))

(defgeneric graph-cycle-p (graph))
(defmethod graph-cycle-p ((g graph))
  (let ((nodes (nodes-contents (graph-nodes g))))
    (unless nodes
      (return-from graph-cycle-p))
    (loop
      while nodes
      do (let ((paths (paths-from-node (pop nodes) g)))
	   (when (some #'path-cycle-p paths)
	     (return-from graph-cycle-p t))
	   (setf nodes (nset-difference nodes (nodes-contents (get-nodes paths))
					 :test #'eq)
		 )))))

(defgeneric shortest-paths-between (node1 node2 graph &optional weight))
(defmethod shortest-paths-between
    ((n1 node) (n2 node) (g graph) &optional (weight #'weight))
  (when (node-marked-p n1)
    (return-from shortest-paths-between '()))
  (when (eq n1 n2)
    (return-from shortest-paths-between (list (make-path))))
  (let ((out-arcs (node-out-arcs n1)))
    (unless out-arcs
      (return-from shortest-paths-between '()))
    (mark-node n1)
    (prog1
	(let* ((arc (pop out-arcs))
	       (paths (shortest-paths-between (extremity arc) n2 g weight)))
	  (loop
	    while out-arcs
	    while (not paths)
	    do (setf arc (pop out-arcs))
	    do (setf paths (shortest-paths-between (extremity arc) n2 g weight)))
	  (if (endp paths)
	      '()
	      (progn
		(setf paths (distribute-arc-on-paths arc paths))
		(arcs-map
		 (lambda (arc)
		   (let ((newpaths (shortest-paths-between (extremity arc) n2 g weight)))
		       (when newpaths
			 (let ((newweight (+ (funcall weight arc) (funcall weight (car newpaths))))
			       (weight (funcall weight (car paths))))
			   (when (<= newweight weight)
			     (setf newpaths (distribute-arc-on-paths arc newpaths))
			     (if (= newweight weight)
				 (setf paths (nconc newpaths paths))
				 (setf paths newpaths)))))))
		 out-arcs)
		paths)))
      (unmark-node n1))))

(defgeneric shortest-path-between (node1 node2 graph &optional weight)
  (:documentation "shortest non empty path according to weight"))

(defmethod shortest-path-between
    ((n1 node) (n2 node) (g graph) &optional (weight #'weight))
  (when (node-marked-p n1)
    (return-from shortest-path-between (make-path)))
  (when (eq n1 n2)
    (return-from shortest-path-between nil))
  (let ((out-arcs (node-out-arcs n1)))
    (unless out-arcs
      (return-from shortest-path-between nil))
    (mark-node n1)
    (prog1
	(let* ((arc (pop out-arcs))
	       (path (shortest-path-between (extremity arc) n2 g weight)))
	  (loop
	    while out-arcs
	    until path
	    do (setf arc (pop out-arcs))
	    do (setf path (shortest-path-between (extremity arc) n2 g weight)))
	  (and path
	       (setf path (path-cons arc path))
	       (loop
		 for arc in out-arcs
		 do (let ((subpath (shortest-path-between (extremity arc) n2 g weight)))
		      (when subpath
			(let ((newweight (+ (funcall weight arc) (funcall weight (car subpath))))
			      (weight (funcall weight path)))
			  (when (< newweight weight)
			    (setf path (path-cons arc subpath))))))
		 finally (return path))))
      (unmark-node n1))))

(defgeneric path-between-p (node1 node2 graph))
(defmethod path-between-p
    ((n1 node) (n2 node) (g graph))
  (when (eq n1 n2)
    (return-from path-between-p n1))
  (mark-node n1)
  (nodes-map
   (lambda (node)
     (when (and (not (node-marked-p node)) (path-between-p node n2 g))
       (return-from path-between-p node)))
   (node-out-nodes n1))
  (unmark-node n1)
  nil)

(defgeneric distribute-arc-on-paths (arc paths))
(defmethod distribute-arc-on-paths ((arc arc) (paths list))
  (mapcar
   (lambda (path)
     (path-cons arc path))
   paths))

(defgeneric paths-between (node1 node2 graph))
(defmethod paths-between
    ((n1 node) (n2 node) (g graph))
  (when (node-marked-p n1)
    (return-from paths-between '()))
  (when (eq n1 n2)
    (return-from paths-between (list (make-path))))
  (mark-node n1)
  (prog1
      (loop
	for arc in (node-out-arcs n1)
	nconc (let ((subpaths (paths-between (extremity arc) n2 g)))
		(if (endp subpaths)
		    '()
		    (distribute-arc-on-paths arc subpaths))))
    (unmark-node n1)))
