;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric regular-p (graph))
(defmethod regular-p ((g graph))
  (assert (not (oriented-p g)))
  (let ((nodes (graph-nodes g)))
    (or (nodes-empty-p nodes)
	(let ((d (out-degree (nodes-car nodes) g)))
	  (nodes-every (lambda (n) (= d (out-degree n g))) nodes)))))

(defgeneric all-nodes-marked-p (graph))
(defmethod all-nodes-marked-p ((g graph))
  (nodes-every 
  #'node-marked-p (graph-nodes g)))

(defgeneric mark-from (node graph))
(defmethod mark-from ((node node) (g graph))
  (unless (node-marked-p node)
    (mark-node node)
    (arcs-map (lambda (arc)
		(mark-from (extremity arc) g))
	      (node-out-arcs node))
    (arcs-map (lambda (arc)
		 (mark-from (origin arc) g))
	      (node-in-arcs node g))))

(defgeneric nodes-connected-to-node (node graph))
(defmethod nodes-connected-to-node ((node node) (g graph))
  ;; at distance >= 1
  (let ((nodes (make-empty-nodes)))
    (labels
	((aux (n)
	   (unless (nodes-member n nodes)
	     (nodes-nadjoin n nodes)
	     (arcs-map (lambda (arc)
			 (aux (extremity arc)))
		       (node-out-arcs n))
	     (when (oriented-p g)
	       (arcs-map (lambda (arc)
			   (aux (origin arc)))
			 (node-in-arcs n g))))))
      (arcs-map
       (lambda (arc)
	 (aux (extremity arc)))
       (node-out-arcs node))
      (arcs-map
       (lambda (arc) (aux (origin arc)))
       (node-in-arcs node g))
      nodes)))

(defgeneric shortest-distance-from (node graph &optional arc-weight))
(defmethod shortest-distance-from ((node node) (g graph) &optional (arc-weight #'weight))
  ;; dijsktra
  ;; shortest non empty path starting from n according to weight
  ;; (assert (oriented-p g))
  (set-nodes-weight g *plus-infinity*)
  (change-weight node 0)
  (let ((weighted-nodes (make-empty-nodes)))
    (labels
	((aux (n)
	   (unless (node-marked-p n)
	     (let ((out-arcs (node-out-arcs n)))
	       (arcs-map
		(lambda (out-arc)
		  (let ((extremity (extremity out-arc)))
		      (unless (node-marked-p extremity)
			(let ((newweight
			       (weight+ (weight n)
					(funcall arc-weight out-arc))))
			  (when (weight< newweight (weight extremity))
			    (change-weight extremity newweight)
			    (nodes-nadjoin extremity weighted-nodes))))))
		out-arcs)
	       (mark-node n)
	       (let ((extremities
		      (sort (nodes-contents (node-out-nodes out-arcs))
			    #'weight< :key #'weight)))
		 (loop
		   for extremity in extremities
		   do (aux extremity)))))))
      (aux node)
      (prog1
	  weighted-nodes
	(graph-unmark-all-nodes g)))))

(defgeneric graph-connected-components (graph))

(defmethod graph-connected-components ((g graph))
  (let ((components '())
	(nodes (nodes-contents (graph-nodes g))))
    (loop
      while nodes
      do (let* ((node (pop nodes))
		(component (nodes-adjoin node (nodes-connected-to-node node g))))
	   (setf nodes (set-difference nodes (nodes-contents component))) ;; a voir pas efficace
	   (push component components)))
    components))

(defgeneric graph-component-ndecomposition (graph)
  (:documentation 
   "list of graphs induced by the
    connected components of the GRAPH; destructive")
  (:method ((g graph))
    (mapcar
     (lambda (nodes)
       (graph-ninduced-subgraph nodes g))
     (graph-connected-components g))))

(defgeneric graph-component-decomposition (graph)
  (:documentation 
   "list of graphs induced by the
    connected components of the GRAPH"))

(defmethod graph-component-decomposition ((g graph))
  (let ((connected-components (graph-connected-components g)))
    (if (endp (cdr connected-components))
	(list g)
	(mapcar
	 (lambda (nodes)
	   (graph-induced-subgraph nodes g))
	 connected-components))))

(defgeneric graph-connected-p (graph))
(defmethod graph-connected-p ((g graph))
  (let ((nodes (graph-nodes g)))
    (or (nodes-empty-p nodes)
	(progn
	  (graph-unmark-all-nodes g)
	  (mark-from (nodes-car nodes) g)
	  (all-nodes-marked-p g)))))

(defgeneric stable-p (graph))
(defmethod stable-p ((graph graph))
  (zerop (graph-nb-arcs graph)))

(defgeneric graph-has-loop-p (graph)
  (:method ((graph graph))
    (container-find-if
     (lambda (arc) (eq (origin arc) (extremity arc)))
     (graph-arcs graph))))

(defgeneric eulerian-p (graph))
(defmethod eulerian-p ((g graph))
  (let ((n (nodes-size
	    (nodes-remove-if
	     (lambda (n)
	       (evenp (out-degree n g)))
	     (graph-nodes g)))))
    (or (zerop n) (= 2 n))))

(defun 1+mod2 (n)
  (mod (1+ n) 2))

(defgeneric graph-bipartite-p (graph))
(defmethod graph-bipartite-p ((g graph))
  (let ((tab (make-array 2)))
    (setf (aref tab 0) (make-empty-nodes))
    (setf (aref tab 1) (make-empty-nodes))
    (let ((nodes (graph-connected-nodes g)))
      (when (nodes-empty-p nodes)
	(return-from graph-bipartite-p (list (aref tab 0) (aref tab 1))))
      (labels
	  ((aux (node i)
	     (unless (nodes-member node (aref tab i))
	       (when (nodes-member node (aref tab (1+mod2 i)))
		 (return-from graph-bipartite-p nil))
	       (progn
		 (nodes-nadjoin node (aref tab i))
		 (nodes-map
		  (lambda (n)
		    (aux n (1+mod2 i)))
		  (node-neighbours node g))))))
	(aux (nodes-car nodes) 0)
	(list (aref tab 0) (aref tab 1))))))

(defgeneric partition-p (nodes partition))
(defmethod partition-p ((nodes nodes) (partition list))
  (and
   (>=  2 (length partition))
   (every (lambda (subset)
	    (every (lambda (node) (nodes-member node nodes)) subset))
	  partition)
   (= (nodes-size nodes)
      (nodes-size
       (reduce (lambda (nodes1 nodes2)
		 (nodes-union nodes1 nodes2))
	       partition)))))
