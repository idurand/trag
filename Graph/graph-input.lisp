;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defun enode-num (n) (if (listp n) (first n) n))
(defun enode-label (n) (when (listp n) (second n)))
(defun enode-weight (n) (when (listp n) (third n)))
(defun earc-origin (a) (first a))
(defun earc-extremity (a) (second a))
(defun earc-extremities (a) (list (earc-origin a) (earc-extremity a)))
(defun earc-weight (a) (caddr a))

(defgeneric earcs-from-epath (epath)
  (:method ((epath list))
    (loop
      with origin = (car epath)
      for extremity in (cdr epath)
      collect (prog1 (list origin extremity)
		(setq origin extremity)))))

(defgeneric earcs-from-epaths (epaths)
  (:method ((epaths list))
    (loop
      for epath in epaths
      nconc (earcs-from-epath epath))))

(defgeneric graph-create-arc-from-earc (graph earc)
  (:method ((graph graph) (earc list))
    (let ((origin (first earc))
	  (extremity (second earc)))
      (graph-create-arc-nodes
       graph
       (find-ndajoin-node 
	(enode-num origin)
	graph
	(enode-label origin)
	(enode-weight origin))
       (find-ndajoin-node 
	(enode-num extremity) 
	graph 
	(enode-label extremity) 
	(enode-weight extremity))
       (earc-weight earc)
       (oriented-p graph)))))

(defgeneric fill-graph-from-earcs (graph earcs)
  (:method ((graph graph) (earcs list))
    (loop
      for earc in earcs
      do (graph-create-arc-from-earc graph earc))
    graph))

(defgeneric fill-graph-from-enodes (graph enodes)
  (:method ((graph graph) (enodes list))
    (mapc (lambda (n)
	    (find-ndajoin-node (enode-num n) graph (enode-label n) (enode-weight n)))
	  enodes)
  graph))

(defgeneric fill-graph-from-paths (graph paths)
  (:method ((g graph) (paths list))
    (loop
      for path in paths
      do (let ((origin (find-ndajoin-node (car path) g))
	       extremity)
	   (loop
	     for extremity-num in (cdr path)
	     do (progn
		  (setf extremity (find-ndajoin-node extremity-num g))
		  (graph-create-arc-nodes g origin extremity nil (oriented-p g))
		  (setf origin extremity)))))
    g))

(defgeneric graph-from-paths (paths &key oriented)
  (:method ((paths list) &key (oriented nil))
    (fill-graph-from-paths (make-graph (graph-name) oriented) paths)))

(defgeneric graph-from-earcs (earcs &key oriented)
  (:method ((earcs list) &key (oriented nil))
    (let ((graph (make-graph (graph-name) oriented)))
      (fill-graph-from-earcs graph earcs))))

(defgeneric graph-from-earcs-and-enodes (earcs enodes &key oriented)
  (:method ((earcs list) (enodes list) &key (oriented nil))
    (let ((graph (graph-from-earcs earcs :oriented oriented)))
      (fill-graph-from-enodes graph enodes)
      graph)))

(defun enodes-from-earcs (edges)
  (remove-duplicates (flatten edges)))
