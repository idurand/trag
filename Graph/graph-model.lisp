;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)

(defvar *graph-number* 0)

(defun graph-name ()
  (format nil "G~A" (incf *graph-number*)))

(defun graph-name-copy (graph)
  (format nil "~A-copy" (name graph)))

(defclass abstract-graph (named-mixin oriented-mixin) ())

(defgeneric graph-nodes (graph))

(defclass graph (abstract-graph)
  ((nodes :initarg :nodes
	  :initform (make-empty-nodes)
	  :reader graph-nodes :type nodes)))

(defgeneric graph-not-inodes (graph)
  (:method ((graph graph))
    (nodes-remove-if #'inodep (graph-nodes graph))))

(defgeneric graph-inodes (graph)
  (:method ((graph graph))
    (nodes-remove-if-not #'inodep (graph-nodes graph))))

(defgeneric graph-arcs (graph)
  (:method ((g graph))
    (let ((arcs (make-empty-arcs)))
      (nodes-map
       (lambda (node)
	 (arcs-map
	  (lambda (arc)
	    (arcs-nadjoin arc arcs))
	  (node-out-arcs node)))
       (graph-nodes g))
      arcs)))

(defmethod weighted-p ((graph graph))
  (let ((arcs (graph-arcs graph)))
    (or (arcs-empty-p arcs)
	(weighted-p (arcs-car arcs)))))

(defgeneric graph-nb-arcs (graph)
  (:method ((g graph))
    (let ((nb-arcs 0))
      (nodes-map
       (lambda (node)
	 (incf nb-arcs (arcs-size (node-out-arcs node))))
       (graph-nodes g))
      nb-arcs)))

(defgeneric graph-nodes-contents (graph)
  (:method ((graph graph))
    (nodes-contents (graph-nodes graph))))

(defun make-graph (name &optional (oriented *oriented*))
  (make-instance 'graph :oriented oriented :name name))

(defgeneric graph-create-node (numnode graph inode &optional label)
  (:method ((num integer) (g graph) inodep &optional (label nil))
    (let ((node (make-labeled-node num label inodep)))
      (nodes-nadjoin node (graph-nodes g))
      node)))

(defgeneric graph-create-inode (numnode graph &optional label)
  (:method ((num integer) (g graph) &optional (label nil))
    (graph-create-node num g t label)))

(defgeneric find-node-with-num (num graph)
  (:method (num (g graph))
    (container-key-member num (graph-nodes g))))

(defmethod find-node-with-num (num (nodes nodes))
  (container-key-member num nodes))

(defgeneric find-arc-with-nums (n1 n2 graph)
  (:method ((n1 integer) (n2 integer) (graph graph))
    (arcs-member
     (make-arc (find-node-with-num n1 graph)
	       (find-node-with-num n2 graph)) (graph-arcs graph))))

(defgeneric find-arc-with-pair (pair graph)
  (:method ((pair list) (graph graph))
    (find-arc-with-nums (first pair) (second pair) graph)))

(defgeneric find-ndajoin-node (num graph &optional label weight)
  (:method (num (g graph) &optional (label nil) (weight nil))
    (let ((node (find-node-with-num num g)))
      (if node
	  (let ((old-label (label node)))
	    (if (and label old-label)
		(assert (equal (label node) label))
	      (when (and label (not old-label))
		(setf (label node) label)))
	  (let ((old-weight (weight node)))
	    (if (and weight old-weight)
		(assert (equal old-weight weight)) 
		(when (and weight (not old-weight))
		  (setf (weight node) weight))))
	  node)
	(graph-create-node num g nil label)))))

(defmethod print-object ((g abstract-graph) stream)
  (let ((oriented (oriented-p g)))
    (format stream "~A [~A nodes " (name g) (graph-nb-nodes g))
    (if oriented 
    	(format stream "~A arcs " (graph-nb-arcs g))
    	(format stream "~A edges " (graph-nb-edges g)))
    (unless oriented
      (format stream "not "))
    (format stream "oriented]")))

(defgeneric graph-marked-nodes (graph)
  (:method ((g graph))
    (remove-if-not #'node-marked-p (graph-nodes g))))

(defgeneric graph-unmarked-nodes (graph)
  (:method ((g graph))
    (remove-if #'node-marked-p (graph-nodes g))))

(defgeneric graph-nb-nodes (graph)
  (:method ((g graph))
    (nodes-size (graph-nodes g))))

(defgeneric edges-from-arcs (arcs)
  (:method ((arcs arcs))
    (let ((edges (make-empty-arcs)))
      (arcs-map
       (lambda (arc)
	 (arcs-nadjoin (edge-from-arc arc) edges))
       arcs)
      edges)))

(defgeneric arcs-from-edges (edges)
  (:method ((edges arcs))
    (let ((arcs (make-empty-arcs)))
      (arcs-map
       (lambda (edge)
	 (arcs-nadjoin edge arcs)
	 (arcs-nadjoin (inverse-arc edge) arcs))
       edges)
      arcs)))

(defgeneric graph-edges (graph)
  (:method ((g graph))
    (edges-from-arcs (graph-arcs g))))

(defgeneric graph-nb-edges (graph)
  (:method ((g graph))
    (arcs-size (graph-edges g))))

(defgeneric node-out-nodes (node)
  (:method ((node node))
    (make-nodes
     (arcs-mapcar #'extremity (node-out-arcs node)))))

(defgeneric out-degree (node graph)
  (:method ((node node) (g graph))
    (arcs-size (node-out-arcs node))))

(defgeneric graph-average-degree (graph)
  (:method ((g graph))
    (let* ((nodes (graph-nodes g))
	   (nb-nodes (nodes-size nodes)))
      (if (zerop nb-nodes)
	  0
	  (/
	   (reduce #'+
		   (nodes-mapcar
		    (lambda (node)
		      (out-degree node g))
		    nodes))
	   nb-nodes)))))

(defgeneric in-degree (node graph)
  (:method ((node node) (g graph))
    (arcs-size (node-in-arcs node g))))

(defgeneric pending-node-p (node graph)
  (:method ((node node) (g graph))
    (= (in-degree node g) (out-degree node g) 1)))

(defgeneric pending-node (graph)
  (:method ((g graph))
    (find-if (lambda (node) (pending-node-p node g)) (graph-nodes g))))

(defgeneric pending-nodes (graph)
  (:method ((g graph))
    (nodes-remove-if-not
     (lambda (node) (pending-node-p node g))
     (graph-nodes g))))

(defgeneric out-degree-max (graph)
  (:method ((g graph))
    (apply #'max 0 (nodes-mapcar (lambda (n) (out-degree n g)) (graph-nodes g)))))

(defgeneric in-degree-max (graph)
  (:method ((g graph))
    (apply #'max 0 (nodes-mapcar (lambda (n) (in-degree n g)) (graph-nodes g)))))

(defgeneric graph-degree-max-node (graph)
  (:method ((g graph))
    (let ((dm (out-degree-max g)))
      (nodes-find-if
       (lambda (n) (= dm (out-degree n g)))
       (graph-nodes g)))))

(defgeneric graph-degree-max-nodes (graph)
  (:method ((g graph))
    (nodes-degree-min (graph-nodes g))))

(defgeneric out-degree-min (graph)
  (:method ((g graph))
    (apply #'min (nodes-mapcar (lambda (n) (out-degree n g)) (graph-nodes g)))))

(defgeneric graph-degree-min-nodes (graph)
  (:method ((g graph))
    (nodes-with-degree (graph-nodes g) (out-degree-min g))))

(defgeneric graph-degree-min-node (graph)
  (:method ((g graph))
    (let ((dm (out-degree-min g)))
      (nodes-find-if
       (lambda (n) (= dm (out-degree n g)))
       (graph-nodes g)))))

(defgeneric graph-num-min-node (graph)
  (:method ((g graph))
    (let ((min-num (apply #'min (nodes-mapcar #'num (graph-nodes g)))))
      (find-node-with-num min-num g))))

(defgeneric isolated-node-p (node graph)
  (:method ((node node) (g graph))
    (and
     (zerop (in-degree node g))
     (zerop (out-degree node g)))))

(defgeneric graph-isolated-nodes (graph)
  (:method ((g graph))
    (nodes-remove-if-not
     (lambda (node)
       (isolated-node-p node g))
     (graph-nodes g))))

(defgeneric mark-node (node)
  (:method ((node node))
    (mark-object node)))

(defgeneric unmark-node (node))
(defmethod unmark-node ((node node))
  (unmark-object node))

(defgeneric node-marked-p (node)
  (:method ((node node))
    (mark node)))

(defgeneric graph-unmark-all-nodes (graph)
  (:method ((g graph))
    (nodes-map #'unmark-node (graph-nodes g))))

(defgeneric set-nodes-weight (graph &optional weight)
  (:method ((g graph) &optional (w nil))
    (nodes-map
     (lambda (node) (change-weight node w))
     (graph-nodes g))))

(defgeneric set-nodes-label (graph &optional w)
  (:method ((g graph) &optional (w nil))
    (nodes-map
     (lambda (node) (change-label node w))
     (graph-nodes g))))

(defgeneric unset-nodes-label (graph)
  (:method ((g graph))
    (set-nodes-label g)))

(defgeneric unset-nodes-weight (graph)
  (:method ((g graph))
    (set-nodes-weight g)))

(defgeneric set-arcs-weight (graph &optional weight)
  (:method ((g graph) &optional (w nil))
    (arcs-map(lambda (arc) (change-weight arc w)) (graph-arcs g))))

(defgeneric unset-arcs-weight (graph)
  (:method ((g graph))
    (set-arcs-weight g)))

(defgeneric inverse-arcs-p (arc1 arc2)
  (:method ((a1 arc) (a2 arc))
    (and (eq (origin a1) (extremity a2))
	 (eq (origin a2) (extremity a1)))))

(defgeneric node-in-arcs (node graph)
  (:documentation "in-going arcs of node")
  (:method ((node node) (g graph))
    (let ((arcs (make-empty-arcs)))
      (arcs-map
       (lambda (arc)
	 (when (eq node (extremity arc))
	   (arcs-nadjoin arc arcs)))
       (graph-arcs g))
      arcs)))

(defgeneric node-in-nodes (node graph)
  (:method ((node node) (g graph))
  (let ((nodes (make-empty-nodes)))
    (arcs-map
     (lambda (arc)
       (nodes-nadjoin (origin arc) nodes))
     (node-in-arcs node g))
    nodes)))

(defgeneric node-neighbours (node graph)
  (:method ((node node) (g graph))
    (nodes-union
     (node-in-nodes node g)
     (node-out-nodes node)))
  (:method ((node-num integer) (g graph))
    (node-neighbours (find-node-with-num node-num g) g)))

(defgeneric node-nadjoin-arc (arc)
  (:method ((arc arc))
    (arcs-nadjoin arc (node-out-arcs (origin arc)))))

(defgeneric graph-nadjoin-arc (arc g)
  (:method ((arc arc) (g graph))
    (node-nadjoin-arc arc)))

(defgeneric graph-nadjoin-arc-nodes (origin-node extremity-node
				 graph &optional weight)
  (:method ((origin node) (extremity node)
	    (g graph) &optional (weight nil))
    (graph-nadjoin-arc (make-arc origin extremity weight) g)
    g))

(defgeneric node-delete-arc (node arc)
  (:method ((node node) (arc arc))
    (arcs-delete arc (node-out-arcs node))))

(defgeneric graph-delete-arc (arc graph)
  (:method ((arc arc) (g graph))
    (node-delete-arc (origin arc) arc)
    g))

(defgeneric graph-delete-edge (edge graph)
  (:method ((arc arc) (g graph))
    (graph-delete-arc arc g)
    (graph-delete-arc (inverse-arc arc) g)))

(defgeneric graph-add-edge (edge graph)
  (:method ((arc arc) (g graph))
    (assert (not (oriented-p g)))
    (graph-nadjoin-arc arc g)
    (graph-nadjoin-arc (inverse-arc arc) g)))

(defgeneric graph-create-arc-nodes (g origin extremity weight oriented)
  (:method ((g graph) (origin node) (extremity node) weight oriented)
    (assert (nodes-member origin (graph-nodes g)))
    (assert (nodes-member extremity (graph-nodes g)))
    (graph-nadjoin-arc-nodes origin extremity g weight)
    (unless oriented
      (graph-nadjoin-arc-nodes extremity origin g weight))
    g))

(defgeneric graph-create-arc-from-num-nodes (graph origin extremity oriented)
  (:method ((g graph) (origin integer) (extremity integer) oriented)
    (graph-create-arc-nodes
     g
     (find-node-with-num origin g)
     (find-node-with-num extremity g)
     nil oriented)))

(defgeneric get-nodes (o)
  (:documentation "fresh nodes container of nodes involved in object")
  (:method ((g graph)) (nodes-copy (graph-nodes g)))
  (:method ((l list))
    (let ((newnodes (make-empty-nodes)))
      (loop for e in l
	    do (nodes-nunion newnodes (get-nodes e)))
      newnodes))

  (:method ((node node)) (make-nodes (list node)))
  (:method ((arc arc)) (make-nodes (arc-nodes arc)))

  (:method ((arcs arcs))
    (let ((nodes (make-empty-nodes)))
      (arcs-map
       (lambda (arc)
	 (let ((extremities (arc-nodes arc)))
	   (nodes-nadjoin (first extremities) nodes)
	   (nodes-nadjoin (second extremities) nodes)))
       arcs)
      nodes)))

(defgeneric graph-empty-p (graph)
  (:method ((graph graph))
    (nodes-empty-p (graph-nodes graph))))

(defgeneric graph-nmerge-nodes (node1 node2 graph)
  (:method ((node1 node) (node2 node) (graph graph))
    (nodes-map
     (lambda (node)
       (unless (eq node node1)
	 (arcs-nadjoin (make-arc node1 node) (node-out-arcs node1))))
     (node-out-nodes node2))
    (nodes-map
     (lambda (node)
       (unless (eq node1 node)
	 (container-nadjoin (make-arc node node1) (node-out-arcs node))))
     (node-in-nodes node2 graph))
    (graph-delete-node node2 graph)
    graph))

(defgeneric graph-ncontract-edge (edge graph)
  (:method ((edge arc) (graph graph))
    (graph-nmerge-nodes (origin edge) (extremity edge) graph)))

(defgeneric graph-ncontract-first-edge (graph)
  (:method ((graph graph))
    (let ((edge (container-car (graph-edges graph))))
      (graph-ncontract-edge edge graph))))

(defgeneric graph-delete-first-edge (graph)
  (:method ((graph graph))
    (let ((edge (container-car (graph-edges graph))))
      (graph-delete-edge edge graph))))
