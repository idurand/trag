;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)

;; complète la version de base
(defgeneric get-arcs (o)
  (:documentation "fresh list of arcs in o without duplicates"))

(defmethod get-arcs ((arc arc))
  (list arc))

(defmethod get-arcs ((node node))
  (copy-list (node-out-arcs node)))

(defmethod get-arcs ((g graph))
  (arcs-copy (graph-arcs g)))

(defmethod get-arcs ((l list))
  (reduce #'arcs-union (mapcar #'get-arcs l)))
	  
(defgeneric graph-connected-nodes (graph))

(defmethod graph-connected-nodes ((g graph))
  (let ((nodes (make-empty-nodes)))
    (arcs-map
     (lambda (arc)
       (nodes-nunion nodes (get-nodes arc)))
     (graph-arcs g))
    nodes))

