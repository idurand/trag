;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defvar *show-node-only* t)

(defgeneric graph-node-to-enode (node)
  (:method ((node node))
    (if (or *show-node-only*
	    (and (not (label node)) (not (weight node))))
	(num node)
	(list (num node) (label node) (weight node)))))

(defgeneric graph-arc-to-earc (arc &optional inverse)
  (:method ((arc arc) &optional (inverse nil))
    (let* ((nodes (list (origin arc) (extremity arc)))
	   (out  (mapcar #'graph-node-to-enode (if inverse (nreverse nodes) nodes))))
      (if *show-node-only*
	  out
	  (append out (list (weight arc)))))))

(defgeneric graph-arcs-to-earcs (arcs &optional inverse)
  (:method ((arcs list) &optional (inverse nil))
    (sort 
     (mapcar
      (lambda (arc) (graph-arc-to-earc arc inverse))
      arcs)
     #'strictly-ordered-p))
  (:method ((arcs arcs) &optional (inverse nil))
    (graph-arcs-to-earcs (arcs-contents arcs) inverse)))

(defgeneric graph-nodes-to-enodes (nodes)
  (:method ((nodes list))
    (mapcar #'graph-node-to-enode nodes))
  (:method ((nodes nodes))
    (graph-nodes-to-enodes (nodes-contents nodes))))

(defgeneric graph-to-earcs (graph)
  (:method ((graph graph))
    (graph-arcs-to-earcs (graph-arcs graph))))

(defgeneric graph-to-enodes (graph)
  (:method ((graph graph))
    (graph-nodes-to-enodes (graph-nodes graph))))

(defgeneric graph-to-eedges (graph)
  (:method ((graph graph))
    (graph-arcs-to-earcs (graph-edges graph))))

(defgeneric graph-to-paths (graph)
  (:method ((graph graph))
    (if (oriented-p graph)
	(graph:graph-to-earcs graph)
	(graph:graph-to-eedges graph))))

(defgeneric graph-equivalent-p (graph1 graph2)
  (:method ((graph1 graph) (graph2 graph))
    (let ((*show-node-only* t))
      (let ((oriented1 (oriented-p graph1))
	    (oriented2 (oriented-p graph2)))
	(and (eq oriented1 oriented2)
	     (equal (graph-to-enodes graph1) (graph-to-enodes graph2))
	     (if oriented1
		 (equal (graph-to-earcs graph1) (graph-to-earcs graph2))
		 (equal (graph-to-eedges graph1) (graph-to-eedges graph2))))))))

(defun dot-to-svg-string (dot-code)
  (with-output-to-string (svg)
    (with-input-from-string (dot dot-code)
      (uiop:run-program (list "dot" "-Tsvg")
                        :input dot :output svg :error-output t))))

(defgeneric graph-to-svg-string (graph)
  (:method ((graph graph))
    (dot-to-svg-string
     (graph-to-dot-string graph))))
