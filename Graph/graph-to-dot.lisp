;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric output-label (node))
(defmethod output-label ((node node))
  (if (label node)
      (format nil "~A,~A" (num node) (label node))
      (format nil "~A" (num node))))

(defgeneric graph-to-dot (graph &optional stream))
(defmethod graph-to-dot ((g graph) &optional stream)
  (let* ((oriented (oriented-p g))
	 (arcs (if oriented (graph-arcs g) (graph-edges g))))
    (format stream "~:[~;di~]graph ~S {~%" oriented (name g))
    (arcs-map
     (lambda (arc)
       (let ((ori (origin arc))
	     (ext (extremity arc)))
	 (format stream "~S -~:[-~;>~] ~S"
		 (output-label ori)
		 oriented
		 (output-label ext))
	 (when (weight arc)
	   (format stream "[label=~S]" (weight arc)))
	 (format stream ";~%")))
     arcs)
    (nodes-map
     (lambda (node)
       (if (weight node)
	   (format stream
		   "~A [label=\"~A,~A\"];~%"
		   (output-label node)
		   (output-label node)
		   (weight node))
	   (format stream
		   "~S;~%"
		   (output-label node))))
     (graph-isolated-nodes g))
    (format stream "}~%")))

(defgeneric graph-to-dot-string (graph)
  (:method ((g graph::graph))
    (with-output-to-string (dot) (graph-to-dot g dot))))

(defgeneric graph-to-dot-file (graph name)
  (:method ((graph graph) (name string))
    (with-open-file (stream
		     (format nil "/tmp/~A.dot" name)
		     :direction :output :if-exists :supersede
		     :if-does-not-exist :create)
      (graph-to-dot graph stream))))

(defgeneric dot-to-pdf (string)
  (:method ((name string))
    (uiop:run-program (list "dot" "-Tpdf" (format nil "/tmp/~A.dot" name))
		      :output (format nil "/tmp/~A.pdf" name)
		      :if-output-exists :supersede)))

(defun show-pdf (name)
  (uiop:run-program 
   (list "open" (format nil "/tmp/~A.pdf" name))))

(defun tmp-graph-display (name)
  (uiop:run-program
   (list input-output::*viewer* (format nil "/tmp/~A.pdf" name))))

(defgeneric graph-show (graph &key name)
  (:method ((graph graph) &key (name "graph"))
    (graph-to-dot-file graph name)
    (dot-to-pdf name)
    (tmp-graph-display name)))
