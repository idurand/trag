;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric graph-copy (graph &optional rename)
  (:method ((graph graph) &optional (rename t))
    (let* ((oriented (oriented-p graph))
	   (name (name graph))
	   (g (make-graph
	       (if rename (format nil "~A-copy" name) name) oriented)))
      (nodes-map
       (lambda (node)
	 (graph-create-node (num node) g (inodep node) (label node)))
       (graph-nodes graph))
      (arcs-map 
       (lambda (arc)
	 (graph-create-arc-from-num-nodes 
	  g (num (origin arc)) (num (extremity arc)) oriented))
       (graph-arcs graph))
      g)))

(defgeneric delete-unused-nodes (graph)
  (:method ((graph graph))
    (let ((new-nodes (get-nodes (graph-arcs graph))))
      (nodes-delete-if-not 
       (lambda (node)
	 (nodes-member node new-nodes))
       (graph-nodes graph)))
    graph))

(defgeneric graph-ntranspose (graph)
  (:method ((g graph))
    (let* ((nodes (graph-nodes g))
	   (all-arcs (make-empty-arcs)))
      (nodes-map
       (lambda (origin)
	 (nodes-map
	  (lambda (extremity)
	    (unless (eq origin extremity)
	      (arcs-nadjoin (make-arc origin extremity) all-arcs)))
	  nodes))
       nodes)
      (mapc
       (lambda (node)
	 (setf (node-out-arcs node)
	       (remove-if-not (lambda (arc) (= (num (origin arc))
					       (num node)))
			      (graph-arcs g))))
       (graph-nodes g))
      g)))

(defgeneric graph-transpose (graph)
  (:method ((g graph))
    (graph-ntranspose (graph-copy g))))

(defgeneric nfilter-arcs-with-nodes (arcs nodes)
  (:method ((arcs arcs) (nodes nodes))
    (arcs-delete-if-not
     (lambda (arc)
       (and (nodes-member (origin arc) nodes)
	    (nodes-member (extremity arc) nodes)))
     arcs)))

(defgeneric nfilter-arcs-without-node (arcs node)
  (:method ((arcs arcs) (node node))
    (arcs-delete-if
     (lambda (arc)
       (or (eq (origin arc) node)
	   (eq (extremity arc) node)))
     arcs)))

(defgeneric filter-nodes-with-num-nodes (nodes num-nodes)
  (:method ((nodes nodes) (num-nodes list))
    (let ((newnodes
	    (nodes-remove-if-not
	     (lambda (node)
	       (member (num node) num-nodes))
	     nodes)))
      (nfilter-nodes newnodes)
      newnodes)))

(defgeneric nfilter-nodes (nodes)
  (:method ((nodes nodes))
    (nodes-map
     (lambda (node)
       (nfilter-arcs-with-nodes (node-out-arcs node) nodes))
     nodes)
    nodes))

(defgeneric nfilter-nodes-without-node (node nodes)
  (:method ((node node) (nodes nodes))
    (nodes-map
     (lambda (n)
       (setf (node-out-arcs n)
	     (nfilter-arcs-without-node (node-out-arcs n) node)))
     nodes)))

(defgeneric filter-nodes (nodes)
  (:method ((nodes nodes))
    (nfilter-nodes (nodes-copy nodes))))

(defgeneric graph-delete-node (node graph)
  (:documentation "destructively delete node in graph")
  (:method ((node node) (g graph))
    ;; destructive for graph and node
    ;;  (assert (nodes-member node (graph-nodes g)))
    (setf (node-out-arcs node) (make-empty-arcs))
    (nodes-delete node (graph-nodes g))
    (nfilter-nodes-without-node node (graph-nodes g))
    g)
  (:method ((num-node integer) (g graph))
    (let ((node (find-node-with-num num-node g)))
      (graph-delete-node node g))))

(defgeneric graph-delete-enode (enode graph)
  (:method ((enode integer) (graph graph))
    (graph-delete-node (find-node-with-num enode graph) graph)))

(defgeneric graph-delete-nodes (nodes graph)
  (:method ((nodes list) (graph graph))
    (loop 
      for node in nodes
      do (graph-delete-node node graph)
      finally (return graph))))

(defgeneric graph-delete-enodes (enodes graph)
  (:method ((enodes list) (graph graph))
    (graph-delete-nodes
     (mapcar (lambda (enode) (find-node-with-num enode graph)) enodes)
     graph)))

(defgeneric graph-ninduced-subgraph (nodes graph)
  (:method ((nodes nodes) (g graph))
    ;; destructive for graph
    (assert (nodes-subset-p nodes (graph-nodes g)))
    (let ((new (make-graph (graph-name-copy g) (oriented-p g))))
      (setf
       (slot-value new 'nodes)
       (nfilter-nodes nodes))
      new)))

(defgeneric graph-induced-subgraph (nodes graph)
  (:method ((nodes nodes) (g graph))
    (assert (nodes-subset-p nodes (graph-nodes g)))
    (graph-induced-subgraph (nodes-num nodes) g))
  (:method ((nodes-num list) (g graph))
    (let* ((newg (graph-copy g))
	   (newnodes (filter-nodes-with-num-nodes (graph-nodes newg) nodes-num)))
      (graph-ninduced-subgraph newnodes newg))))

(defgeneric graph-nsubgraph (arcs graph)
  (:method ((arcs arcs) (g graph))
    (let ((oriented (oriented-p g)))
      (arcs-map
       (lambda (arc)
	 (graph-delete-arc arc g))
       arcs)
      (arcs-map 
       (lambda (arc)
	 (graph-create-arc-from-num-nodes 
	  g (num (origin arc)) (num (extremity arc)) oriented))
       (graph-arcs g))
      g)))

(defgeneric graph-nunorient (graph)
  (:documentation "destructively unorients graph")
  (:method ((g graph))
    (when (oriented-p g)
      (let ((arcs (arcs-contents (graph-arcs g))))
	(loop for arc in arcs
	      do (graph-nadjoin-arc-nodes (extremity arc) (origin arc) g (weight arc))))
      (setf (oriented-p g) nil))
    g))

(defgeneric graph-unorient (graph)
  (:method ((g graph))
    (unless (oriented-p g)
      (return-from graph-unorient g))
    (graph-nunorient (graph-copy g))))

(defgeneric graph-norient (graph)
  (:documentation "destructively unorients graph")
  (:method ((g graph))
    (assert (not (oriented-p g)))
    (setf (oriented-p g) t)
    g))

(defgeneric graph-orient (graph)
  (:method ((g graph))
    (if (oriented-p g)
	g
	(graph-norient (graph-copy g nil)))))

(defmacro with-node-deleted (node graph &body body)
  (let ((out-arcs (gensym)))
    `(let ((,out-arcs (node-out-arcs ,node)))
       (graph-delete-node ,node ,graph)
       (prog1
	   (progn
	     ,@body)
	 (nodes-nadjoin ,node (graph-nodes ,graph))
	 (arcs-map
	  (lambda (arc)
	    (graph-create-arc-nodes 
	     ,graph (origin arc) (extremity arc) (weight arc) nil))
	  ,out-arcs)))))

(defgeneric graph-complete-orientation (graph)
  (:method ((graph graph))
    (assert (oriented-p graph))
    (rename-object
     (graph-from-earcs
      (loop
	with edges = (graph-to-eedges graph)
	for edge in edges
	nconc (list edge (reverse edge)))
      :oriented t)
     (format nil "~A-o" (name graph)))))

(defgeneric graph-erase-orientation (graph)
  (:method ((graph graph))
    (if (oriented-p graph)
	(rename-object
	 (graph-from-earcs (graph-to-eedges graph) :oriented nil)
	 (format nil "~A-no" (name graph)))
	graph)))

(defgeneric graph-randomize-orientation (graph)
  (:method ((graph graph))
    (rename-object
     (graph-from-earcs
      (loop
	with edges = (graph-to-eedges graph)
	for edge in edges
       collect (if (zerop (random 2)) edge (reverse edge)))
      :oriented t)
     (format nil "~A-ro" (name graph)))))

(defgeneric graph-randomize-orientation (graph)
  (:method ((graph graph))
    (rename-object
     (graph-from-earcs
      (loop
	with edges = (graph-to-eedges graph)
	for edge in edges
       collect (if (zerop (random 2)) edge (reverse edge)))
      :oriented t)
     (format nil "~A-ro" (name graph)))))

(defgeneric graph-delete-earc-nums (n1 n2 graph)
  (:method ((n1 integer) (n2 integer) (graph graph))
    (assert (oriented-p graph))
    (graph-delete-arc (find-arc-with-nums n1 n2 graph) graph)))

(defgeneric graph-delete-earc (earc graph)
  (:method ((earc list) (graph graph))
    (graph-delete-earc-nums (first earc) (second earc) graph)))

(defgeneric graph-delete-eedge-nums (n1 n2 graph)
  (:method ((n1 integer) (n2 integer) (graph graph))
    (assert (not (oriented-p graph)))
    (graph-delete-edge (find-arc-with-nums n1 n2 graph) graph)))

(defgeneric graph-delete-eedge (eedge graph)
  (:method ((eedge list) (graph graph))
    (graph-delete-eedge-nums (first eedge) (second eedge) graph)))

(defgeneric graph-remove-eedge (eedge graph)
  (:method ((eedge list) (graph graph))
    (graph-delete-eedge-nums (first eedge) (second eedge) (graph-copy graph))))

(defgeneric graph-nadjoin-eedge-nums (n1 n2 graph)
  (:method ((n1 integer) (n2 integer) (graph graph))
    (assert (not (oriented-p graph)))
    (graph-create-arc-from-num-nodes graph n1 n2 nil)))

(defgeneric graph-nadjoin-earc-nums (n1 n2 graph)
  (:method ((n1 integer) (n2 integer) (graph graph))
    (assert (oriented-p graph))
    (graph-create-arc-from-num-nodes graph n1 n2 t)))

(defgeneric graph-nadjoin-eedge (eedge graph)
  (:method ((eedge list) (graph graph))
    (graph-nadjoin-eedge-nums (first eedge) (second eedge) graph)))

(defgeneric graph-adjoin-eedge (eedge graph)
  (:method ((eedge list) (graph graph))
    (graph-nadjoin-eedge-nums (first eedge) (second eedge) (graph-copy graph))))

(defgeneric graph-nadjoin-earc (eearc graph)
  (:method ((eearc list) (graph graph))
    (graph-nadjoin-earc-nums (first eearc) (second eearc) graph)))

(defgeneric graph-adjoin-earc (eearc graph)
  (:method ((eearc list) (graph graph))
    (graph-nadjoin-earc-nums (first eearc) (second eearc) (graph-copy graph))))

(defgeneric graph-delete-epath (epath graph)
  (:method ((epath list) (graph graph))
    (let ((earcs (earcs-from-epath epath))
	  (oriented (oriented-p graph)))
      (loop
	for earc in earcs
	do (if oriented
	       (graph-delete-earc earc graph)
	       (graph-delete-eedge earc graph))
	finally (return graph)))))

(defgeneric graph-remove-epath (epath graph)
  (:method ((epath list) (graph graph))
    (graph-delete-epath epath (graph-copy graph))))

(defgeneric graph-delete-elink (elink graph)
  (:method ((elink list) (graph graph))
    (if (oriented-p graph)
	(graph-delete-earc elink graph)
	(graph-delete-eedge elink graph))))

	  
      
      
