;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Graph.
(in-package :asdf-user)

(defsystem :graph
  :description "Graph: library of operations on graphs"
  :name "graph"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:container :uiop :input-output :enum)
  :serial t
  :components ((:file "package-graph-model")
	       (:file "label")
	       (:file "weight")
	       (:file "mark")
	       (:file "utilities")
	       (:file "node")
	       (:file "arc")
	       (:file "arc2")
	       (:file "nodes")
	       (:file "arcs")
	       (:file "graph-model")
	       (:file "path")
	       (:file "graph-model2")
	       (:file "package")
	       (:file "graph-input")
	       (:file "input")
	       (:file "graph-output")
	       (:file "dimacs")
	       (:file "graph-utils")
	       (:file "graph-to-dot")
	       (:file "galgo")
	       (:file "eulerian")
	       (:file "galgo-path")
	       (:file "galgo-acm")
	       (:file "matching")
	       (:file "articulation-node")
	       (:file "2connected-components")
	       (:file "random-tw2")
	       (:file "compute-graphs")
	       (:file "graphs")
	       (:file "context")
	       (:file "grids")
	       (:file "trees")
	       (:file "data-graphs")
	       (:file "init-graph")
	       (:file "incidence")
	       (:file "arith-graph")
	       (:file "format-graph6")
	       (:file "chromatic-polynomial")
	       ))

(pushnew :graph *features*)
