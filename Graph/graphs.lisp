;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric graphs-table (graphs)
  (:documentation "the hash-table containing the graphs keyed by name"))

(defclass graphs ()
  ((graphs-table :initform (make-hash-table :test #'equal)
		 :accessor graphs-table)))

(defun make-graphs () (make-instance 'graphs))

