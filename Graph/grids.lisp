;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defun grid-paths (n)
  (append 
   (loop for i from 0 below (* n n) by n
	 collect (loop
		   for j from 1 to n
		   collect (+ i j)))
   (loop for i from 0 below n
	 collect (loop
		   for j from 1 to (* n n) by n
		   collect (+ i j)))))
  
(defun rgrid-paths (x y)
  (if (> x y)
      (rgrid-paths y x)
      (append 
       (loop for i from 0 below (* y x) by x
	     collect (loop
		       for j from 1 to x
		       collect (+ i j)))
       (loop for i from 0 below x
	     collect (loop
		       for j from 1 to (* y x) by x
		       collect (+ i j))))))

(defun graph-rgrid (n m)
  (rename-object
   (graph-from-paths (rgrid-paths n m))
   (format nil "g~Ax~A" n m)))

(defun variant-rgrid-paths (x y)
  (if (> x y)
      (variant-rgrid-paths y x)
      (append
       (rgrid-paths x y)
       (loop
	 for i from 1
	 repeat (1- x)
	 collect (list i (1+ (+ i x)))))))

(defun variant-rgrid (x y)
  (rename-object
   (graph-from-paths
    (variant-rgrid-paths x y))
    (format nil "variant-g~Ax~A" x y)))

(defun diago-paths (x)
  (loop
     for i from 1 by (1+ x)
     for j from (+ x 2) by (1+ x)
     repeat (1- x)
     collect (list i j)))

(defun one-diago-rgrid-paths (x y)
  (if (> x y)
      (one-diago-rgrid-paths y x)
      (append
       (rgrid-paths x y)
       (diago-paths x))))

(defun one-diago-rgrid (x y)
  (rename-object
   (graph-from-paths (one-diago-rgrid-paths x y))
   (format nil "one-diago-g~Ax~A" x y)))
