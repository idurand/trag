;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric incidence-graph-of (graph)
  (:method ((graph graph))
    (let* ((oriented (oriented-p graph))
	   (igraph (make-graph (format nil "incidence-~A" (name graph))
			       oriented))
	   (n (graph-nb-nodes graph)))
      (arcs-map 
       (lambda (arc)
	 (let ((origin-node (find-ndajoin-node (num (origin arc)) igraph))
	       (extremity-node (find-ndajoin-node (num (extremity arc)) igraph))
	       (inode (graph-create-inode (incf n) igraph)))
	   (graph-create-arc-nodes igraph origin-node inode nil oriented)
	   (graph-create-arc-nodes igraph inode extremity-node nil oriented)))
       (if (oriented-p graph)
	   (graph-arcs graph)
	   (graph-edges graph)))
      igraph)))

(defgeneric nset-incidence-nodes (graph)
  (:method ((graph graph))
    (when (nodes-empty-p (graph-inodes graph))
      (let ((bipartite (graph-bipartite-p graph)))
	(assert bipartite)
	(let* ((one-transition (container-find-if
				(lambda (node) (= 1 (num node)))
				(first bipartite)))
	       (inodes (if one-transition (second bipartite) (first bipartite))))
	  (nodes-map
	   (lambda (inode)
	     (setf (slot-value inode 'inodep) t))
	   inodes))
	(rename-object graph (prefix-name "incidence-" (name graph)))))
    graph))

(defgeneric set-incidence-nodes (graph)
  (:method ((graph graph))
    (if (nodes-empty-p (graph-inodes graph))
	(rename-object
	 (nset-incidence-nodes (graph-copy graph))
	 (prefix-name "incidence-" (name graph)))
	graph)))

(defgeneric incidence-graph-to-graph (incidence-graph)
  (:method ((incidence-graph graph))
    (when (nodes-empty-p (graph-inodes incidence-graph))
      (setq incidence-graph
	    (set-incidence-nodes (graph-copy incidence-graph))))
    (let* ((oriented (oriented-p incidence-graph))
	   (graph (make-graph 
		   (prefix-decompose-name "incidence-" (name incidence-graph))
		   oriented)))
      (nodes-map
       (lambda (node)
	 (graph-create-node (num node) graph (inodep node) (label node)))
       (graph-not-inodes incidence-graph))
      (arcs-map
       (lambda (arc)
	 (let ((origin (origin arc))
	       (extremity (extremity arc)))
	   (unless (inodep origin)
	     (let* ((numo (num origin))
		    (nume (num (container-car
				(nodes-remove-if
				 (lambda (n) (= (num n) numo))
				 (node-out-nodes extremity))))))
	       ;;	       (format *trace-output* "~A->~A " numo nume)
	       (graph-create-arc-from-num-nodes
		graph numo nume oriented)
	       (unless oriented
		 ;;		 (format *trace-output* "~A->~A " nume numo)
		 (graph-create-arc-from-num-nodes graph
						  nume numo oriented))))))
       (graph-arcs incidence-graph))
      graph)))
