;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defun init-graph ()
  (setq *data-directory* (initial-data-directory))
  (set-graph-directory)
  (setq graph-model::*graph-number* 0))
