;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defvar *graph-directory*)
(defvar *graph-relative-directory* "Graphs")

(defun set-graph-directory ()
  (setq *graph-directory*
	(concatenate 'string *data-directory* "Graphs/")))

(define-condition junk-found (simple-error) ())

(defun skip-lines-starting-with (characters input)
  (loop
    do
       (let ((line (read-line input nil)))
	 (unless line (return))
	 (when line
	   (let ((first (aref line 0)))
	     (unless (member first characters)
	       (return line)))))))

(defun skip-spaces (s)
  (loop
    for i from 0 below (length s)
    unless (eq (aref s i) #\Space)
      do (return (subseq s i))))

(defun load-dimacs-graph-stream (input)
  (let ((line (skip-lines-starting-with (list #\c) input)))
    (unless line (return-from load-dimacs-graph-stream))
    (let ((edges-or-arcs '())
	  (nb-nodes 0)
	  (nb-edges-or-arcs 0)
	  (oriented nil))
      (when (eql (aref line 0) #\p)
	(setq line (skip-spaces (subseq line 1)))
	(when (eq (aref line 0) #\a)
	  (setq oriented t))
	(setq line (subseq line (if oriented 3 4)))
	(with-input-from-string (stream line)
	  (setq nb-nodes (read stream))
	  (setq nb-edges-or-arcs (read stream)))
	(setq line (read-line input nil nil)))
      (loop
	with c = (if oriented #\a #\e)
	when (or (not line) (equal line ""))
	  do (return)
	when (eql c (aref line 0))
	  do (let ((rest (skip-spaces (subseq line 1))))
	       (multiple-value-bind (origin i)
		   (parse-integer rest :junk-allowed t)
		 (unless origin
		   (return))
		 (let ((extremity
			 (parse-integer
			  (subseq rest (1+ i))
			  :junk-allowed t)))
		   (push (list origin extremity) edges-or-arcs))))
	do (setq line (read-line input nil))
	)
      (if (zerop nb-nodes)
	  (graph-from-earcs (nreverse edges-or-arcs) :oriented oriented)
	  (graph-from-earcs-and-enodes (nreverse edges-or-arcs) (iota nb-nodes 1) :oriented oriented)))))

(defun load-dimacs-graph-absolute (filename)
  (with-open-file (input filename :if-does-not-exist nil)
    (unless input (return-from load-dimacs-graph-absolute nil))
    (rename-object 
     (load-dimacs-graph-stream input) 
     (remove-extension (basename filename)))))

(defgeneric load-dimacs-graph (filename))

(defmethod load-dimacs-graph ((filename pathname))
  (load-dimacs-graph-absolute (namestring filename)))

(defmethod load-dimacs-graph ((filename string))
  (unless (char= (aref filename 0) #\/)
    (setq filename (absolute-filename *graph-directory* filename)))
  (load-dimacs-graph-absolute filename))
