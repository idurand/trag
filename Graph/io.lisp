;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-io)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Save information for various model classes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-save-info arc
  (:origin origin)
  (:extremity extremity))

(define-save-info node
  (:out-arcs node-out-arcs))

(define-save-info weight-mixin
  (:weight weight))

(define-save-info graph
  (:nodes graph-nodes)
  (:arcs graph-arcs))

(define-save-info label-mixin
  (:label label))

(define-save-info oriented-mixin
  (:oriented-p oriented-p))

(define-save-info num-mixin
  (:num num))

(defvar *graph-io-allowed-version-names* '("GraphV1" "GraphV1.1"))
(defvar *graph-io-current-version-name*  "GraphV1.1")

(defun graph-io-read-from-stream (stream)
  (read-model-from-stream stream *graph-io-allowed-version-names*))

(defun graph-io-write-to-stream (stream object)
  (write-model-to-stream stream *graph-io-current-version-name* object))

(defun graph-io-read (filename)
  (read-model filename *graph-io-allowed-version-names*))

(defun graph-io-write (filename object)
  (write-model filename *graph-io-current-version-name* object))

(defmethod graph-io-copy ((graph graph))
  (copy-model graph *graph-io-current-version-name*))
