;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)

(defgeneric label (label-mixin))
(defmethod label ((o t)) nil)

(defclass label-mixin ()
  ((label :initarg :label :accessor label :initform nil)))

(defmethod print-object :after ((o label-mixin) stream)
  (when (label o)
    (format stream "<~A> " (label o))))

(defgeneric change-label (label-mixin &optional newlabel))
(defmethod change-label ((o label-mixin) &optional (newlabel nil))
  (setf (slot-value o 'label) newlabel))

(defgeneric unset-label (label-mixin))
(defmethod unset-label ((o label-mixin))
  (change-label o))
