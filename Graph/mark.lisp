;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)

(defgeneric mark (o)
  (:documentation "returns the mark of a marked object"))

(defgeneric mark-object (o))
(defgeneric unmark-object (o))

(defclass marked-mixin ()
  ((mark :initarg :mark :accessor mark :initform nil)))

(defmethod mark-object ((o marked-mixin))
  (setf (mark o) t))

(defmethod unmark-object ((o marked-mixin))
  (setf (mark o) nil))



