;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric graph-nmatching-min-weight (graph))
(defgeneric graph-matching-min-weight (graph))

(defmethod graph-nmatching-min-weight ((graph graph))
  (loop
    with edges = '()
    for nodes = (graph-nodes graph) ;; then (graph-nodes graph)
    until (nodes-empty-p nodes)
    do (let* ((node (nodes-car nodes))
	      (arc (arcs-pop (node-out-arcs node)))
	      (edge (edge-from-arc arc)))
	 (arcs-delete (inverse-arc arc) (node-out-arcs node))
	 (push edge edges)
	 (graph-delete-node node graph)
	 (graph-delete-node (extremity arc) graph))
    finally (return edges)))

(defmethod graph-matching-min-weight ((graph graph))
  (graph-nmatching-min-weight (graph-copy graph)))
