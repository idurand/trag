;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)
;;; nodes of a graph should be eq-comparable
(defgeneric node-out-arcs (node)
  (:documentation "out-going arcs of node"))

(defgeneric node-out-arcs-contents (node)
  (:documentation "out-going arcs of node"))

(defmethod node-out-arcs-contents (node)
  (container-contents (node-out-arcs node)))

(defgeneric inodep (node)
  (:documentation "is NODE and incidence node"))

(defclass node (num-mixin marked-mixin label-mixin weight-mixin key-mixin)
  ((out-arcs
    :initform (make-empty-arcs)
    :initarg :node-out-arcs
    :type arcs
    :accessor node-out-arcs)
   (inodep :initform nil :reader inodep :initarg :inodep)))

(defun node-p (o)
  (typep o 'node))

(defmethod object-key ((node node))
  (num node))

(defmethod print-object :before ((node node) stream)
  (when (node-marked-p node) (prin1 '* stream)))

(defmethod print-object ((node node) stream)
  (format stream "n~A~:[~;[~A]~]"
	  (num node)
	  (weight node)
	  (weight node)))

(defun make-labeled-node (num label inodep)
  (make-instance 'node :num num :label label :inodep inodep))

(defun make-node (num inodep)
  (assert (plusp num))
  (make-labeled-node num nil inodep))

(defgeneric node-out-degree (node)
  (:method ((node node))
    (arcs-size (node-out-arcs node))))
