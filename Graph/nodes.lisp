;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)

(defclass nodes (ordered-container) ())

(defgeneric make-nodes (nodes))
(defmethod make-nodes ((nodes list))
  (assert (every (lambda (node) (typep node 'node)) nodes))
  (make-container-generic nodes 'nodes #'eq))

(defmethod print-object :before ((nodes nodes) stream)
  (format stream "nodes"))

(defgeneric make-empty-nodes ())
(defmethod make-empty-nodes ()
  (make-nodes '()))

(defgeneric nodes-union (nodes1 nodes2))
(defmethod nodes-union ((nodes1 nodes) (nodes2 nodes))
  (container-union nodes1 nodes2))

(defgeneric nodes-nunion (nodes1 nodes2))
(defmethod nodes-nunion ((nodes1 nodes) (nodes2 nodes))
  (container-nunion nodes1 nodes2))

(defgeneric nodes-difference (nodes1 nodes2))
(defmethod nodes-difference ((nodes1 nodes) (nodes2 nodes))
  (container-difference nodes1 nodes2))

(defgeneric nodes-ndifference (nodes1 nodes2))
(defmethod nodes-ndifference ((nodes1 nodes) (nodes2 nodes))
  (container-ndifference nodes1 nodes2))

(defgeneric nodes-intersection (nodes1 nodes2))
(defmethod nodes-intersection ((nodes1 nodes) (nodes2 nodes))
  (container-intersection nodes1 nodes2))

(defgeneric nodes-member (node nodes))
(defmethod nodes-member ((node node) (nodes nodes))
  (container-member node nodes))

(defgeneric nodes-car (nodes))
(defmethod nodes-car ((nodes nodes))
  (container-car nodes))

(defgeneric nodes-pop (nodes))
(defmethod nodes-pop ((nodes nodes))
  (container-pop nodes))

(defgeneric nodes-nadjoin (node nodes))
(defmethod nodes-nadjoin ((node node) (nodes nodes))
  (container-nadjoin node nodes))

(defgeneric nodes-delete (node nodes))
(defmethod nodes-delete ((node node) (nodes nodes))
  (container-delete node nodes))

(defgeneric nodes-remove (node nodes))
(defmethod nodes-remove ((node node) (nodes nodes))
  (container-remove node nodes))

(defgeneric nodes-adjoin (node nodes))
(defmethod nodes-adjoin ((node node) (nodes nodes))
  (container-adjoin node nodes))

(defgeneric nodes-remove-if (pred nodes))
(defmethod nodes-remove-if (pred (nodes nodes))
  (container-remove-if pred nodes))

(defgeneric nodes-delete-if (pred nodes))
(defmethod nodes-delete-if (pred (nodes nodes))
  (container-delete-if pred nodes))

(defgeneric nodes-delete-if-not (pred nodes))
(defmethod nodes-delete-if-not (pred (nodes nodes))
  (container-delete-if-not pred nodes))

(defgeneric nodes-find-if (pred nodes))
(defmethod nodes-find-if (pred (nodes nodes))
  (container-find-if pred nodes))

(defgeneric nodes-find-if-not (pred nodes))
(defmethod nodes-find-if-not (pred (nodes nodes))
  (container-find-if-not pred nodes))

(defgeneric nodes-remove-if-not (pred nodes))
(defmethod nodes-remove-if-not (pred (nodes nodes))
  (container-remove-if-not pred nodes))

(defgeneric nodes-map (fun nodes))
(defmethod nodes-map (fun (nodes nodes))
  (container-map fun nodes))

(defgeneric nodes-every (pred nodes))
(defmethod nodes-every (pred (nodes nodes))
  (container-every pred nodes))

(defgeneric nodes-copy (nodes))
(defmethod nodes-copy ((nodes nodes))
  (container-copy nodes))

(defgeneric nodes-empty-p (nodes))
(defmethod nodes-empty-p ((nodes nodes))
  (container-empty-p nodes))

(defgeneric nodes-subset-p (nodes1 nodes2))
(defmethod nodes-subset-p ((nodes1 nodes) (nodes2 nodes))
  (container-subset-p nodes1 nodes2))

(defgeneric nodes-contents (nodes))
(defmethod nodes-contents ((nodes nodes))
  (container-contents nodes))

(defgeneric nodes-num (nodes))
(defmethod nodes-num ((nodes nodes))
  (mapcar #'num (nodes-contents nodes)))

(defgeneric nodes-size (nodes))
(defmethod nodes-size ((nodes nodes))
  (container-size nodes))

(defgeneric nodes-mapcar (fun nodes))
(defmethod nodes-mapcar (fun (nodes nodes))
  (container-mapcar fun nodes))

(defgeneric node-degree (node)
  (:documentation "number of out-going arcs of node"))

(defmethod node-degree ((node node))
  (arcs-size (node-out-arcs node)))

(defgeneric nodes-degree-min (nodes))
(defmethod nodes-degree-min ((nodes nodes))
  (apply #'min (nodes-mapcar (lambda (n) (node-degree n)) nodes)))

(defgeneric nodes-degree-max (nodes))
(defmethod nodes-degree-max ((nodes nodes))
  (apply #'max 0 (nodes-mapcar (lambda (n) (node-degree n)) nodes)))

(defgeneric nodes-weight-min (nodes))
(defmethod nodes-weight-min ((nodes nodes))
  (apply #'min (nodes-mapcar (lambda (n) (weight n)) nodes)))

(defgeneric nodes-weight-max (nodes))
(defmethod nodes-weight-max ((nodes nodes))
  (apply #'max (nodes-mapcar (lambda (n) (weight n)) nodes)))

(defgeneric nodes-with-weight (nodes weigth))
(defmethod nodes-with-weight ((nodes nodes) (weight integer))
  (nodes-remove-if
   (lambda (n) (/= weight (weight n)))
   nodes))

(defgeneric nodes-with-degree (nodes degree))
(defmethod nodes-with-degree ((nodes nodes) (degree integer))
  (nodes-remove-if
   (lambda (n) (/= degree (node-out-degree n)))
   nodes))

(defgeneric node-with-degree (nodes degree))
(defmethod node-with-degree ((nodes nodes) (degree integer))
  (nodes-find-if
   (lambda (n) (= degree (node-out-degree n)))
   nodes))

(defgeneric nodes-with-min-weight (nodes))
(defmethod nodes-with-min-weight ((nodes nodes))
  (nodes-with-weight nodes (nodes-weight-min nodes)))

(defgeneric node-with-min-weight (nodes))
(defmethod node-with-min-weight ((nodes nodes))
  (nodes-car (nodes-with-min-weight nodes)))

(defgeneric nodes-with-max-weight (nodes))
(defmethod nodes-with-max-weight ((nodes nodes))
  (nodes-with-weight nodes (nodes-weight-max nodes)))

(defgeneric node-with-max-weight (nodes))
(defmethod node-with-max-weight ((nodes nodes))
  (nodes-car (nodes-with-max-weight nodes)))


(defgeneric nodes-max-num (nodes))
(defmethod nodes-max-num ((nodes nodes))
  (let ((num 0))
    (nodes-map
     (lambda (node)
       (when (> (num node) num)
	 (setq num (num node))))
     nodes)
    num))

(defgeneric nodes-min-num (nodes))
(defmethod nodes-min-num ((nodes nodes))
  (let ((num (nodes-max-num nodes)))
    (nodes-map
     (lambda (node)
       (when (< (num node) num)
	 (setq num (num node))))
     nodes)
    num))

(defgeneric nodes-min-num-node (nodes))
(defmethod nodes-min-num-node ((nodes nodes))
  (find-node-with-num
   (nodes-min-num nodes)
   nodes))
