;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage #:graph-io
  (:use #:common-lisp #:general #:io #:graph-model)
  (:export #:*graph-io-allowed-version-names*
	   #:*graph-io-current-version-name*
	   #:graph-io-read
	   #:graph-io-write
	   #:graph-io-copy
	   #:graph-io-read-from-stream
	   #:graph-io-write-to-stream))
