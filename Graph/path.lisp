;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph-model)

(defgeneric path-arcs (path)
  (:documentation "return the list of the arcs of PATH"))

(defclass path ()
  ((arcs :initform nil :initarg :arcs :accessor path-arcs)))

(defgeneric path-arcs-container (path)
  (:documentation "return an arc-container of the arcs of PATH")
  (:method ((path path))
    (make-arcs (path-arcs path))))

(defmethod print-object ((path path) stream)
  (format stream "[")
  (display-sequence (path-arcs path) stream :sep ",")
  (format stream "]"))

(defun make-path (&optional arcs)
  (make-instance 'path :arcs (copy-list arcs)))

(defgeneric path-cons (arc path))
(defmethod path-cons ((arc arc) (path path))
  (let ((arcs (path-arcs path)))
    (when arcs
      (assert (eq (extremity arc) (origin (car arcs)))))
    (make-path
     (cons arc arcs))))

(defgeneric path-push (arc path))
(defmethod path-push ((arc arc) (path path))
  (let ((arcs (path-arcs path)))
    (when arcs 
      (assert (eq (extremity arc) (origin (car arcs)))))
    (push arc (path-arcs path))
    path))

(defgeneric path-rcons (arc path))
(defmethod path-rcons ((arc arc) (path path))
  (let ((arcs (path-arcs path)))
    (when arcs
      (assert (eq (origin arc) (extremity (car arcs)))))
    (make-path
     (cons arc arcs))))

(defgeneric path-rpush (arc path))
(defmethod path-rpush ((arc arc) (path path))
  (let ((arcs (path-arcs path)))
    (when arcs 
      (assert (eq (origin arc) (extremity (car arcs)))))
    (push arc (path-arcs path))
    path))

(defgeneric path-pop (path))
(defmethod path-pop ((path path))
  (pop (path-arcs path)))

(defgeneric path-push-end (arc path))
(defmethod path-push-end ((arc arc) (path path))
  (let ((arcs (path-arcs path)))
    (when arcs 
      (assert (eq (origin arc) (extremity (car (last arcs))))))
    (setf (path-arcs path) (nconc (path-arcs path) (list arc)))
    path))

(defgeneric path-origins (path))
(defmethod path-origins ((path path))
  (loop
     with nodes = (make-empty-nodes)
     for arc in (path-arcs path)
     do (nodes-nadjoin (origin arc) nodes)
     finally (return nodes)))

(defgeneric path-origins-list (path))
(defmethod path-origins-list ((path path))
  (loop
     with nodes = '()
     for arc in (path-arcs path)
     do (push (origin arc) nodes)
     finally (return nodes)))

(defgeneric path-extremities-list (path))
(defmethod path-extremities-list ((path path))
  (loop
     with nodes = '()
     for arc in (path-arcs path)
     do (push (extremity arc) nodes)
     finally (return nodes)))

(defgeneric path-origin (path))
(defmethod path-origin ((path path))
  (origin (car (path-arcs path))))

(defgeneric path-extremity (path))
(defmethod path-extremity ((path path))
  (extremity (car (last (path-arcs path)))))

(defgeneric path-extremities (path))
(defmethod path-extremities ((path path))
  (loop
     with nodes = (make-empty-nodes)
     for arc in (path-arcs path)
     do (nodes-nadjoin (extremity arc) nodes)
     finally (return nodes)))

(defmethod get-nodes ((path path))
  (nodes-union
   (path-origins path)
   (path-extremities path)))
  
(defmethod weight ((path path))
  (weight (path-arcs path)))

(defgeneric path-cycle-p (path))
(defmethod path-cycle-p ((path path))
  (let ((arcs (path-arcs path)))
    (and
     arcs
     (eq (origin (first arcs)) (extremity (car (last arcs)))))))
