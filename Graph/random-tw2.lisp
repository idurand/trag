;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defgeneric twd-two-random-graph (nb-nodes nb-arcs)
  (:method  ((nb-nodes integer) (nb-arcs integer))
    (let ((g (make-graph (graph-name) t))
	  (num-node 0))
      (graph-nadjoin-arc-nodes (graph-create-node (incf num-node) g nil)
			   (graph-create-node (incf num-node) g nil) g)
      (loop
	repeat (- nb-nodes 1)
	do (let* ((arcs (graph-arcs g))
		  (la (random (arcs-size arcs)))
		  (arc (nth la (arcs-contents arcs)))
		  (node (graph-create-node (incf num-node) g nil)))
	     (graph-nadjoin-arc-nodes (origin arc) node g)
	     (graph-nadjoin-arc-nodes node (extremity arc) g)))
      (loop
	repeat (- (* 2 nb-nodes) 1 nb-arcs)
	do (let* ((arcs (graph-arcs g))
		  (la (random (arcs-size arcs)))
		  (arc (nth la (arcs-contents arcs))))
	     (graph-delete-arc arc g)))
      (graph-unorient g))))
