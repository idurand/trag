;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :graph)

(defun graph-binary-tree (n &optional (oriented nil))
  (graph-from-earcs
   (loop
     for i from 1 to n
     for 2i = (* 2 i) ;; then (* 2 i)
     when (<= 2i n)
       collect (list i 2i) 
     when (< 2i n)
       collect (list i (1+ 2i)))
   :oriented oriented))
