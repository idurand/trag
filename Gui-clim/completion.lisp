;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :gui-clim)

(define-condition gui-error (error)
  ())


(defun suggest-arg (filename)
  (let ((str (file-namestring filename)))
    (if (member #\. (coerce str 'list))
	(concatenate 'string  (pathname-name filename) "." (pathname-type filename))
	(file-namestring filename))))

(define-presentation-method accept ((type data-file-name) stream
                    (view textual-view)
                    &key)
  (let* ((data-pattern (concatenate 'string *data-directory* (file-namestring pattern)))
	 (pathnames (directory data-pattern)))
    (format *error-output* "*data-directory: ~A, data-pattern:~A ~%" *data-directory* pattern)
    (handler-case
	(let ((filename 
	       (completing-from-suggestions
		(stream)
		(loop
		 for file in pathnames
		 do (suggest (suggest-arg file) file))
		 )))
	      filename)
      (simple-parse-error ()
	  (prog1 nil
	    (format-output "no match~%")))
      (simple-completion-error ()
	  (prog1 nil
	    (format-output "no match~%")))
      (gui-error ()
	  (prog1 nil
	    (format-output "error"))))))

