;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Gui-clim.

(in-package :asdf-user)

(defsystem :gui-clim
  :description "For a GUI with mcclim"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:mcclim :termauto :termwrite)
  :serial t
  :components ((:file "package")
	       (:file "presentations")
	       (:file "completion")
	       (:file "interface-variables")
	       (:file "process")
	       (:file "utils-interface")
	       (:file "toy")
	       (:file "processes")
))

(pushnew :gui-clim *features*)
