;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :gui-clim)

(defvar *term-pane* nil)
(defvar *termset-pane* nil)
(defvar *automaton-pane* nil)
(defvar *tautomaton-pane* nil)
