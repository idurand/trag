;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :gui-clim
  (:use
   :names
   :input-output
   :terms
   :termauto
   :termwrite
   :clim-lisp
   :clim
   :clim-extensions
   )
  (:export
   #:suggest-arg
   #:data-file-name
   #:gui-error
   #:in-process
   #:process-yield
   #:in-process-with-time
   #:*with-processes*
   #:*aux-process*
   #:*term-pane*
   #:*termset-pane*
   #:*automaton-pane*
   #:*tautomaton-pane*
   #:display-object
   #:display-automaton
   #:display-tautomaton
   #:display-current-automaton
   #:display-current-tautomaton
   #:display-current-term
   #:display-term
   #:display-current-termset
   #:display-termset
   #:set-and-display-current-term
   #:clear-current-term
   #:set-and-display-current-termset
   #:clear-current-termset
   #:load-current-term
   #:load-current-termset
   #:set-and-display-current-automaton
   #:set-and-display-current-tautomaton
   #:clear-current-automaton
   #:display-processname
   #:process-minimize
   #:process-complement
   #:process-reduce
   #:process-determinize
))

