;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :gui-clim)

(define-presentation-type data-file-name (&optional (pattern "*")))
(define-presentation-type automaton ())

(define-presentation-method present (object (type abstract-automaton) stream view &key)
  (declare (ignore view))
  (write-string (name object) stream))

