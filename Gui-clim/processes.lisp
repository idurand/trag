;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :gui-clim)

(defun process-determinize ()
  (in-process-with-time (determinize) "Determinize"))

(defun process-reduce ()
  (in-process-with-time (my-reduce) "Reduce"))

(defun process-complement ()
  (in-process-with-time (complementation) "Complement"))

(defun process-minimize ()
  (in-process-with-time (minimize) "Minimize"))
