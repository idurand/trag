;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :gui-clim)

(defun my-reduce ()
  (let ((aut (reduce-automaton (automaton (current-spec)))))
    (set-and-display-current-automaton aut)))

(defun complementation ()
  (let ((aut (complement-automaton (automaton (current-spec)))))
    (set-and-display-current-automaton aut)))


(defun minimize ()
  (let ((aut (minimize-automaton (automaton (current-spec)))))
    (set-and-display-current-automaton aut)))

(defun determinize ()
  (let ((aut (determinize-automaton (automaton (current-spec)))))
    (set-and-display-current-automaton aut)))
