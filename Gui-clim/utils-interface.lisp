;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :gui-clim)

(defun stream-or-pane (stream)
  (if stream
      (if (streamp stream)
	  stream
	  (climi::find-pane-named *application-frame* stream))
      *output-stream*))

(defun display-object (object pane &key (prefix "") (newline nil) (clear t))
  (when clear
    (window-clear pane))
  (format pane "~A: " prefix)
  (when newline
    (terpri pane))
  (show object pane)
;;  (format pane "~A" object)
  (scroll-extent pane 0 0))

(defun display-automaton (automaton &key (prefix "CURRENT AUTOMATON") (clear t))
  (when automaton
    (display-object automaton *automaton-pane* :prefix prefix :clear clear)))

(defun display-current-automaton ()
  (display-automaton (automaton (current-spec))))

(defun display-tautomaton (tautomaton &key (prefix "CURRENT TAUTOMATON") (clear t))
  (when tautomaton
    (display-object tautomaton *tautomaton-pane* :prefix prefix :clear clear)))

(defun display-current-tautomaton ()
  (display-tautomaton (tautomaton (current-spec))))

(defun display-term (term &key (prefix "CURRENT TERM") (clear t))
  (when term
    (display-object term *term-pane* :clear clear :prefix prefix)))

(defun display-current-term ()
  (display-term (term (current-spec))))

(defun display-termset (termset &key (prefix "CURRENT TERMSET") (clear t))
  (when termset
    (display-object termset *termset-pane* :clear clear :prefix prefix)))

(defun display-current-termset ()
  (display-termset (termset (current-spec))))

(defun set-and-display-current-term (term)
  (set-current-term term)
  (display-current-term))

(defun load-current-term (new-term)
  (when new-term
    (setf (previous-term (current-spec)) nil)
    (set-and-display-current-term new-term)))

(defun clear-current-term ()
  (setf (term (current-spec)) nil)
  (window-clear *term-pane*))

(defun set-and-display-current-termset (termset)
  (set-current-termset termset)
  (display-current-termset))

(defun clear-current-termset ()
  (setf (termset (current-spec)) nil)
  (window-clear *termset-pane*))

;;; termset
(defun load-current-termset (name new-termset)
  (when new-termset
    (let ((termset (make-termset name new-termset)))
      (set-and-display-current-termset termset))))

;;; automaton
(defgeneric set-and-display-current-automaton (automaton))
(defmethod set-and-display-current-automaton ((automaton abstract-transducer))
  (set-current-automaton automaton)
  (display-current-automaton))
  
(defun set-and-display-current-tautomaton (tautomaton)
  (set-current-tautomaton tautomaton)
  (display-current-tautomaton))

(defun clear-current-automaton ()
  (setf (automaton (current-spec)) nil))
