;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Io.
(in-package :asdf-user)

(defsystem :io
  :description "input output with reader's macros"
  :name "io"
  :version "6.0"
  :author "Irene Durand <irene.durand@u-bordeaux.fr>"
  :serial t
  :depends-on (:general)
  :components
  ((:file "package")
   (:file "io")))

(pushnew :io *features*)
