(in-package :common-lisp-user)

(defpackage #:io
  (:use #:common-lisp)
  (:export #:*print-for-file-io*
	   #:define-save-info
           #:read-model-from-stream
           #:write-model-to-stream
           #:read-model
           #:write-model
           #:copy-model
	   #:file-does-not-exist
	   #:unknown-file-version
	   #:read-line-to-list
	   #:eof))
