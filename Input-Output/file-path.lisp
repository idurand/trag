;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :input-output)

(defvar *data-relative-directory* "Data/")

(defun default-directory ()
  "The default directory."
  #+allegro (excl:current-directory)
  #+clisp (#+lisp=cl ext:default-directory #-lisp=cl lisp:default-directory)
  #+cmu (ext:default-directory)
  #+cormanlisp (ccl:get-current-directory)
  #+lispworks (hcl:get-working-directory)
  #+lucid (lcl:working-directory)
  #-(or allegro clisp cmu cormanlisp lispworks lucid) (truename "."))


(defvar *absolute-directory*)
(setq *absolute-directory* (default-directory))

(defun initial-data-directory ()
  (concatenate 'string
	       (namestring 
		*absolute-directory*
		)
	       *data-relative-directory*))

(defun init-data-directory ()
  (setq *data-directory* (initial-data-directory)))

(defmacro source-file ()
  `(or *compile-file-truename* *load-truename*))

(defmacro source-directory ()
  `(directory-namestring
     (namestring
       ,(macroexpand '(source-file)))))

(defmacro source-relative-path (filename)
  `(format
     nil "~a/~a"
     ,(source-directory) ,filename))
