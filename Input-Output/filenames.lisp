;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :input-output)

(defvar *data-directory*)
(defvar *directory* (namestring *default-pathname-defaults*))
  ;;  (slot-value (asdf:find-system "autograph") 'asdf:source-file)
  
(defun init-directories ()
  (setq *directory* (directory-namestring *default-pathname-defaults*))
  (format t "current-directory is ~A ~%" *directory*)
  (init-data-directory))

(defun extend-name (name extension)
  (format nil "~A.~A" name extension))

(defun remove-extension (name)
  (coerce
   (loop
     for c across name
     until (char= c #\.)
     collect c)
   'string))

(defun home-directory ()
  (sb-ext::posix-getenv "HOME"))

(defun bin-directory ()
  (format nil "~A/bin/" (home-directory)))

(defun mkdir (dir)
  (sb-unix:unix-mkdir (directory-namestring dir) #o777))

(defun tmp-local-directory () 
  (let ((pathname "/tmp/local/"))
    (unless (probe-file pathname)
      (mkdir pathname))
    pathname))

(defun absolute-filename (directory filename)
  (format nil "~A~A" directory filename))

(defun absolute-data-filename (filename)
  (absolute-filename *data-directory* filename))

(defun absolute-bin-filename (filename)
  (absolute-filename (bin-directory) filename))

(defun absolute-tmp-filename (filename)
  (absolute-filename (tmp-local-directory) filename))

(defun which-program (name)
  (ignore-errors
    (let* ((process (sb-ext:run-program
                      "which" (list name) :output :stream :search t))
           (out (sb-ext::process-output process)))
      (prog1
        (read-line out)
        (sb-ext:process-close process)))))

(defun basename (name)
  (let* ((process
	   (sb-ext::run-program
	    (which-program "basename")
	    (list name)
	    :output :stream))
	 (stream (sb-ext::process-output process)))
    (prog1 (read-line stream)
      (sb-ext::process-output process))))
