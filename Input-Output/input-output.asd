;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Input-Output.

(in-package :asdf-user)

(defsystem :input-output
  :description "general input output"
  :name "input-output"
  :version "6.0"
  :author "Irène Durand"
  :serial t
  :depends-on (:general)
  :components
   (
    (:file "package")
    (:file "filenames")
    (:file "input")
    (:file "output")
    (:file "file-path")
    (:file "viewer")
    ))

(pushnew :input-output *features*)
