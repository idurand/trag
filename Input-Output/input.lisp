;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :input-output)

(defun load-file-from-absolute-filename  (absolute-filename reader)
  (with-open-file (foo absolute-filename :direction :input :if-does-not-exist nil)
    (if foo
	(funcall reader foo)
	(prog1
	    nil
	  (format *error-output* "unable to open ~A~%" absolute-filename)))))
