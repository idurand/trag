;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :input-output)

(defvar *output-stream* *standard-output*)

(defmacro with-time-to-output-stream (call)
  `(with-time ,call *output-stream*))

(defmacro format-output (format &rest rest)
  `(format *output-stream* ,format ,@rest))

(defun write-object-to-file  (object file writer)
  (when (probe-file (absolute-data-filename file))
    (format *error-output* "~A~%" (absolute-data-filename file))
    (rename-file (absolute-data-filename file) (concatenate 'string file ".bak")))
  (with-open-file (foo (absolute-data-filename file) :direction :output)
    (if foo
	(funcall writer object foo)
	(prog1
	    nil
	  (format *error-output* "unable to open ~A~%" file)))))

(defun back-up-file (file)
  (when (probe-file file)
    (let ((nfile (concatenate 'string file ".bak")))
	(back-up-file nfile)
      (rename-file file nfile))))
