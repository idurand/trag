;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :input-output
  (:use :common-lisp :general)
  (:export
   #:*output-stream*
   #:format-output
   #:with-time-to-output-stream   
   #:write-object-to-file
   #:load-file-from-absolute-filename
   #:*data-directory*
   #:absolute-data-filename
   #:absolute-filename
   #:home-directory
   #:bin-directory
   #:absolute-bin-filename
   #:which-program
   #:absolute-tmp-filename
   #:extend-name
   #:remove-extension
   #:basename
   #:back-up-file
   #:*absolute-directory*
   #:initial-data-directory
   #:init-directories
   #:source-file
   #:source-directory
   #:source-relative-path
))
