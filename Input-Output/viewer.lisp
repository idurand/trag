;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :input-output)

(defvar *viewer-name* "evince")
(defparameter *viewer* (ignore-errors (which-program *viewer-name*)))

