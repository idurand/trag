# Makefile pour projet Lisp

.SUFFIXES:
.SUFFIXES: .lisp .fasl

all:
	.PHONY clean clean-objs links

clean:
	-rm -f *~ TAGS

clean-objs:
	find . -name \*.fasl -print -exec rm \{\} \;

links:
	for i in */*.asd; do ln -s $$i "." ; done

clean-links:
	for i in */*.asd; do rm $$(basename $$i) ; done
