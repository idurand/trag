;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Names.
(in-package :asdf-user)

(defsystem :names
  :description "names mixin"
  :name "names"
  :version "6.0"
  :author "Irène Durand"
  :serial t
  :depends-on (:container)
  :components
   (
    (:file "package")
    (:file "names")))

(pushnew :names *features*)
