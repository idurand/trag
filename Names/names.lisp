;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :names)

(defclass names (container) ())

(defvar *names* (make-empty-container-generic 'names #'equal) "set of already defined names")

(defun init-names ()
  (container-empty *names*))

(defun make-name (name)
  (let ((found (container-member name *names*)))
    (or found
	(and
	 (container-nadjoin name *names*)
	 name))))

