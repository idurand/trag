;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :names
  (:use :common-lisp :container)
  (:export #:init-names
	   #:make-name
))
