;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :object)

(defgeneric attribute-of (attributed-object))
(defgeneric object-of (attributed-object))

(defclass attribute-mixin ()
  ((%attribute :initarg :attribute :accessor attribute-of)))

(defmethod print-object :after ((attribute-mixin attribute-mixin) stream)
  (format stream ":~A" (attribute-of attribute-mixin)))

(defclass attributed-object (attribute-mixin key-mixin)
  ((%object :initarg :object :reader object-of)))

(defmethod print-object ((attributed-object attributed-object) stream)
  (format stream "~A" (object-of attributed-object)))

(defgeneric attributed-p (object))
(defmethod attributed-p ((o t)) nil)
(defmethod attributed-p ((o attribute-mixin)) t)

(defmethod object-key ((attributed-object attributed-object))
  (object-key (object-of attributed-object)))

(defun make-attributed-object (object attribute &optional (type 'attributed-object))
  (make-instance type :object object :attribute attribute))

(defgeneric object-copy (object))
(defmethod object-copy ((object t))
  object)

(defun objects-copy (objects)
  (loop for object in objects
     collect (object-copy object)))

(defmethod object-copy ((ao attributed-object))
  (make-attributed-object
   (object-of ao)
   (attribute-of ao)
   (type-of ao)))

(defgeneric object-subst (new old object))
(defmethod object-subst (new old (object t))
  (subst new old object))

(defmethod object-subst (new old (ao attributed-object))
  (make-attributed-object
   (subst new old (object-of ao))
   (attribute-of ao)
   (type-of ao)))

(defgeneric object-funcall (fun object))
(defmethod object-funcall (fun (object t))
  (funcall fun object))

(defmethod object-funcall (fun (ao attributed-object))
  (funcall fun (object-of ao)))

(defgeneric attributed-object-combine (attributed-object attribute combine-fun))
(defmethod attributed-object-combine ((ao attributed-object) attribute combine-fun)
  (setf (attribute-of ao) (funcall combine-fun (attribute-of ao) attribute))
  ao)

(defmethod strictly-ordered-p ((ao1 attributed-object) (ao2 attributed-object))
  (strictly-ordered-p (object-of ao1) (object-of ao2)))

