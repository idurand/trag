;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :object)

(defgeneric name (named-mixin)
  (:documentation "the name of the NAMED-OBJECT"))

(defclass named-mixin (key-mixin)
  ((name :initarg :name
	 :initform ""
	 :accessor name)))

(defmethod object-key ((named-mixin named-mixin))
  (name named-mixin))

(defmethod print-object ((object named-mixin) stream)
  (format stream "~A" (name object))
  object)

(defun rename-object (object name)
  (setf (name object) name)
  object)


