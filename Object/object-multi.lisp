;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :object)

(defun make-object-multi (e &optional (m 1))
  (make-attributed-object e m))

(defun objects-to-multis (objects)
  (mapcar
   #'make-object-multi
   objects))

(defgeneric multi-object-fun (object-fun multi-fun)
  (:method (object-fun multi-fun)
    (lambda (me1 me2)
      (make-object-multi
       (funcall object-fun (object-of me1) (object-of me2))
       (funcall multi-fun (attribute-of me1) (attribute-of me2))))))

