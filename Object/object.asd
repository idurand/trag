;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Object.
(in-package :asdf-user)

(defsystem :object
  :description "object"
  :name "object"
  :version "6.0"
  :author "Irène Durand"
  :serial t
  :depends-on (:general)
  :components
   (
    (:file "package")
    (:file "object")
    (:file "named-object")
    (:file "object-multi")
    (:file "attributed-object")))

(pushnew :object *features*)
