;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :object)

(defvar *print-object-readably* t)

(defgeneric object-key (object)
  (:documentation "any object should have a key which is a non CLOS object")
  (:method ((object standard-object)) ;; clos by default as print-object
    (let ((*print-object-readably* nil)) (format nil "~A" object)))
  (:method ((object t)) ;; non clos object
    object))

(defclass key-mixin () ()
  (:documentation "to mix with objects to put in a key container"))

(defun find-object (object objects
		    &key (test #'compare-object) (key #'identity))
  (car (member object objects :test test :key key)))

(defgeneric size (object)
  (:documentation "returns the size of OBJECT"))

(defgeneric show (o &optional stream)
  (:documentation
   "print an object O defined to STREAM when defined \
    to *output-stream* otherwise")
  (:method ((o t) &optional stream) (print-object o stream))
  (:method ((l sequence) &optional (stream t))
    (mapc (lambda (x) (show x stream) (format stream " ")) l)))

(defgeneric compare-object (o1 o2)
  (:documentation "compare objects (clos or not) of the same type")
  (:method (o1 o2)
    (error "no compare-object for ~A:~A ~A:~A"
	   o1 (type-of o1) o2 (type-of o2)))
  (:method ((o1 number) (o2 number)) (= o1 o2))
  (:method ((o1 string) (o2 string)) (string= o1 o2))
  (:method ((o1 standard-object) (o2 standard-object))
    (when *debug* (assert (eq (type-of o1) (type-of o2))))
    (or (eq o1 o2)
	(equalp (object-key o1) (object-key o2))))
  (:method ((l1 sequence) (l2 sequence))
    (and (= (length l1) (length l2)) (every #'compare-object l1 l2)))
  (:method ((bv1 bit-vector) (bv2 bit-vector)) (equal bv1 bv2)))

(defun set-equal-p (set1 set2)
  (and (subsetp set1 set2 :test #'compare-object)
       (subsetp set2 set1 :test #'compare-object)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; if there is a need to order objects 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric strictly-ordered-p (object1 object2)
  (:method ((s1 string) (s2 string)) (string< s1 s2))
  (:method ((n1 number) (n2 number)) (< n1 n2))
  (:method ((l1 sequence) (l2 sequence))
    (or (endp l1)
	(and l2
	     (let ((e1 (car l1))
		   (e2 (car l2)))
	       (or (strictly-ordered-p e1 e2)
		   (and (compare-object e1 e2)
			(strictly-ordered-p (cdr l1) (cdr l2))))))))
  (:method (object1 object2)
    (when *debug* (assert (eq (type-of object1) (type-of object2))))
    (string< (object-key object1) (object-key object2))))

(defun my-assoc (x l) (assoc x l :test #'compare-object))
(defun my-member (x l) (member x l :test #'compare-object))
(defun my-remove (e set) (remove e set :test #'compare-object))
(defun my-remove-duplicates (l) (remove-duplicates l :test #'compare-object))
					; destructive 
(defun my-delete (e set) (delete e set :test #'compare-object))
					; destructive
(defun my-delete-duplicates (l) (delete-duplicates l :test #'compare-object))
(defun my-adjoin (e set) (adjoin e set :test #'compare-object))
(defun my-subsetp (s1 s2) (subsetp s1 s2 :test #'compare-object))
(defun compare-set (s1 s2) (and (my-subsetp s1 s2) (my-subsetp s2 s1)))
(defun my-union (l1 l2) (union l1 l2 :test #'compare-object))
(defun my-nunion (l1 l2) (nunion l1 l2 :test #'compare-object))
(defun my-intersection (l1 l2) (intersection l1 l2 :test #'compare-object))
(defun my-setdifference (l1 l2) (set-difference l1 l2 :test #'compare-object))
(defun my-nsetdifference (l1 l2) (nset-difference l1 l2 :test #'compare-object))
