;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :object
  (:use :common-lisp :general)
  (:export
   #:show
   #:compare-object
   #:size
   #:find-object
   #:my-assoc
   #:my-member
   #:my-remove
   #:my-remove-duplicates
   #:my-delete
   #:my-delete-duplicates
   #:my-adjoin
   #:my-subsetp
   #:compare-set
   #:my-union
   #:my-nunion
   #:my-intersection
   #:my-setdifference
   #:my-nsetdifference
   #:named-mixin
   #:name
   #:rename-object
   #:strictly-ordered-p
   #:object-key
   #:em-object
   #:make-object-multi
   #:make-object-multi-max
   #:multi-object-fun
   #:objects-to-multis
   #:attribute-mixin
   #:attributed-object
   #:attributed-p
   #:make-attributed-object
   #:copy-attributed-object
   #:copy-attributed-objects
   #:object-of
   #:attribute-of
   #:combine-fun
   #:object-subst
   #:object-copy
   #:object-funcall
   #:attributed-object-combine
   #:key-mixin
   #:*print-object-readably*
   #:set-equal-p
   ))
