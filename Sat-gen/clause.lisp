;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand
(in-package :sat-gen)

;;; clause
(defclass clause ()
  ((clause-literals :initarg :clause-literals
		    :reader clause-literals :type list)))

(defgeneric empty-clause-p (clause)
  (:method ((clause clause))
    (let ((literals (clause-literals clause)))
      (and (not (endp literals))
	   (endp (cdr literals))
	   (literal= (first literals) (make-literal *sat-zero-var*))))))

(defgeneric tautology-clause-p (clause)
  (:method ((clause clause))
    (endp (clause-literals clause))))

(defun clause-codes (clause)
  (sort (mapcar #'code-from-literal (clause-literals clause))
	#'<))

(defun clause= (clause1 clause2)
  (equal (clause-codes clause1) (clause-codes clause2)))

(defun clause-size (clause)
  (length (clause-literals clause)))

(defmethod print-object ((clause clause) stream)
  (let ((literals (clause-literals clause)))
    ;; (unless *sat-debug*
    ;;   (setq literals (append literals (list (make-literal *sat-zero-var*)))))
    (when (empty-clause-p clause)
      (format stream "[]")
      (return-from print-object))
    (unless *sat-debug*
      (setq literals (append literals (list *sat-zero-literal*))))
    (if (endp literals)
	(format stream "T")
	(display-sequence literals stream))))

(defun make-clause (clause-literals)
  (make-instance 'clause :clause-literals clause-literals))

(defun make-empty-clause ()
  (make-clause (list *sat-zero-literal*)))

(defun clause-vars (clause)
  (remove-duplicates (mapcar #'var (clause-literals clause)) :test #'var=))

(defgeneric make-clause-from-literals (&rest literals)
  (:method (&rest literals)
    (make-clause literals)))

(defgeneric one-literal-clause-p (clause)
  (:method ((clause clause))
    (= 1 (length (clause-literals clause)))))

(defun arc-clause (u v)
  (make-clause-from-literals
   (make-literal (arc-var u v) (not (is-arc u v)))))

(defun unary-clause (fun u v level negated)
  (let ((var (funcall fun u v level)))
    (make-clause-from-literals (make-literal var negated))))

(defun component-clause (u v level negated)
  (unary-clause #'component-var u v level negated))

(defun group-clause (u v level negated)
  (unary-clause #'group-var u v level negated))

(defun component-group-clause (u v level ug vg levelg) ;; D2
  (let ((c (component-var u v level))
	(g (group-var ug vg levelg)))
    (make-clause-from-literals (make-literal c) (make-literal g t))))

(defun component-component-clause (u v level) ;; D3
  (make-clause-from-literals
   (make-literal (component-var u v (1- level)) t)
   (make-literal (component-var u v level))))

(defun group-group-clause (u v level)
  (make-clause-from-literals
   (make-literal (group-var u v (1- level)) t)
   (make-literal (group-var u v level))))

(defun clean-list (l unused)
  (loop
    with offset = 0
    for i in l
    if (and unused (= i (car unused)))
      do (progn
	   (incf offset)
	   (pop unused))
    else
      collect (cons i (- i offset))))

(defun c1-clause (v level)
  (let ((literals
	  (loop
	    for c from 1 to *sat-nb-colors*
	    collect (make-literal (s-var v c level)))))
    (apply #'make-clause-from-literals
	   (append literals (list (make-literal (representative-var v level) t))))))

(defun c2-clause (v a b level)
  (make-clause-from-literals
   (make-literal (representative-var v level) t)
   (make-literal (s-var v a level) t)
   (make-literal (s-var v b level) t)))

(defun c3-clause (u v a level)
  (make-clause-from-literals
   (make-literal (representative-var u level) t)
   (make-literal (representative-var v level) t)
   (make-literal (component-var u v level) t)
   (make-literal (s-var u a level) t)
   (make-literal (s-var v a level) t)))

(defun c4-clause (u a level)
  (make-clause-from-literals
   (make-literal (s-var u a level) t)
   (make-literal (representative-var u level))))

(defun make-representative-vars ()
  (loop
    for level from 1 to (nb-levels)
    do (loop
	 for u from 1 to *sat-nb-nodes*
	 do (make-representative-var u level))))

(defun representative-clause1 (s v)
  (let ((literals
	  (loop
	    for u from 1 below v
	    collect (make-literal (group-var u v s)))))
    (apply #'make-clause-from-literals
     (cons (make-literal (representative-var v s)) literals))))
  
(defun representative-clause2 (s v u)
  (make-clause-from-literals
   (make-literal (representative-var v s) t)
   (make-literal (group-var u v s) t)
   ))

(defgeneric literal-simplify-clause (literal clause)
  (:method ((literal literal) (clause clause))
    (let ((literals (clause-literals clause)))
      (make-clause
       (if (member literal literals :test #'literal=)
	   '() ;; tautologie
	   (let ((l (remove-if (lambda (lit) (var= (var literal) (var lit))) literals)))
	     (if (endp l)
		 (make-empty-clause) ;; clause vide
		 l)))))))

(defgeneric clause-nb-literals (clause)
  (:method ((clause clause))
    (1- (length (clause-literals clause)))))

(defgeneric clause-first-literal (clause)
  (:method ((clause clause))
    (first (clause-literals clause))))
