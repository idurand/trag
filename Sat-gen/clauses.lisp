;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand
(in-package :sat-gen)

;;; clauses

(defun clauses-vars (clauses)
  (remove-duplicates (mapcan #'clause-vars clauses) :test #'var=))

(defun nb-clauses-vars (clauses) (length (clauses-vars clauses)))

(defun arc-clauses ()
  (loop
    for u from 1 to *sat-nb-nodes*
    nconc (loop
	    for v from 1 to *sat-nb-nodes*
	    collect (arc-clause u v))))

(defun print-clauses (clauses)
  (loop for clause in clauses
	do (format t "~A~%" clause)))

(defun unused-clauses-vars-codes (clauses)
  (cdr (reverse
	(set-difference (iota (nb-vars))
			(mapcar #'var-code (clauses-vars clauses))))))


(defun c1-clauses ()
  (loop
    for level from 1 to (nb-levels)
    nconc
    (loop
      for v from 1 to *sat-nb-nodes*
      collect (c1-clause v level))))
  
(defun c2-clauses ()
  (loop
    for level from 1 to (nb-levels)
    nconc
    (loop
      for v from 1 to *sat-nb-nodes*
      nconc
      (loop
	for a from 1 to *sat-nb-colors*
	nconc
	(loop
	  for b from (1+ a) to *sat-nb-colors*
	  collect (c2-clause v a b level))))))

(defun c3-clauses ()
 (loop
   for level from 1 to (nb-levels)
   nconc
   (loop
     for u from 1 to *sat-nb-nodes*
     nconc
     (loop for v from (1+ u) to *sat-nb-nodes*
          nconc
          (loop
            for a from 1 to *sat-nb-colors*
	    collect (c3-clause u v a level))))))

(defun c4-clauses ()
  (loop
    for level from 1 to (nb-levels)
    nconc (loop
	 for v from 1 to *sat-nb-nodes*
	 nconc (loop
	      for a from 1 to *sat-nb-colors*
	      collect (c4-clause v a level)))))

(defun ci-clauses ()
  (nconc
   (c1-clauses)
   (c2-clauses)
   (c3-clauses)
   (c4-clauses)))
   
(defun singleton-and-full-components-clauses ()
  ;; singleton-and-full-components-clauses"
  (loop for u from 1 to *sat-nb-nodes*
	nconc
	(loop
	  for v from (1+ u) to *sat-nb-nodes*
	  append (list
		  (component-clause u v 0 t)
		  (component-clause u v (nb-levels) nil)))))

(defun groups-in-components-clauses ()
  (loop
    for s from 1 to (nb-levels) ;; start with 1 or 0?
    nconc (loop
	    for u from 1 to *sat-nb-nodes*
	    nconc (loop
		    for v from (1+ u) to *sat-nb-nodes*
		    collect (component-group-clause u v s u v s)))))

(defun level-component-clauses ()
  (loop for level from 2 below (nb-levels)
	nconc
	(loop for u from 1 to *sat-nb-nodes*
	      nconc
	      (loop for v from (1+ u) to *sat-nb-nodes*
		    collect (component-component-clause u v level)))))

(defun level-group-clauses () ;; D4
  (loop
    for level from 2 to (nb-levels)
    nconc
    (loop for u from 1 to *sat-nb-nodes*
	  nconc
	  (loop for v from (1+ u) to *sat-nb-nodes*
		collect (group-group-clause u v level)))))

(defun three-transitive-clauses (fun u v w level)
  (list
  (make-clause-from-literals
   (make-literal (funcall fun u v level) t)
   (make-literal (funcall fun v w level) t)
   (make-literal (funcall fun u w level)))
  (make-clause-from-literals
   (make-literal (funcall fun u v level) t)
   (make-literal (funcall fun u w level) t)
   (make-literal (funcall fun v w level)))
  (make-clause-from-literals
   (make-literal (funcall fun u w level) t)
   (make-literal (funcall fun v w level) t)
   (make-literal (funcall fun u v level)))))

(defun step-u-v-transitive-clauses (fun u v level)
  (loop
    for w from (1+ v) to *sat-nb-nodes*
    unless (or (= u w) (= v w))
      nconc (three-transitive-clauses fun u v w level)))
  
(defun step-u-transitive-clauses (fun u level)
  (loop
    for v from (1+ u) to *sat-nb-nodes*
    nconc (step-u-v-transitive-clauses fun u v level)))

(defun step-transitive-clauses (fun level)
  (loop
    for u from 1 to *sat-nb-nodes*
    nconc (step-u-transitive-clauses fun u level)))
  
(defun transitive-clauses (fun nb-levels)
  (loop
    for level from 1 to nb-levels
    nconc (step-transitive-clauses fun level)))
    
(defun component-transitive-clauses ()
  (transitive-clauses #'component-var (1- (nb-levels))))

(defun group-transitive-clauses ()
  (transitive-clauses #'group-var (nb-levels)))

(defun binary-representative-clauses ()
  (loop
    for s from 1 to (nb-levels)
    nconc (loop
	 for v from 1 to *sat-nb-nodes*
	 nconc (loop
	      for u from 1 below v
	      collect (representative-clause2 s v u)))))

(defun nary-representative-clauses ()
  (loop
    for s from 1 to (nb-levels)
    nconc (loop
	    for u from 1 to *sat-nb-nodes*
	    collect (representative-clause1 s u))))

(defun representative-clauses ()
  ;; "representative clauses"
  (nconc
;;   (unary-representative-clauses) ; R1
   (nary-representative-clauses)
   (binary-representative-clauses) ; R2
   (ci-clauses)))
  
(defun header (clauses)
  (format t "c shorter = ~d (~d)~%" 0 3)
  (format t "c groups ~d, colors ~d~%" (nb-groups) *sat-nb-colors*)
  (format t  "p cnf ~d ~d~%" (nb-clauses-vars clauses) (length clauses)))

(defun display-clauses (clauses)
  (header clauses)
  (print-clauses clauses))
  
(defun arc-independent-clauses ()
  (nconc
   (singleton-and-full-components-clauses) ; D1 + D2 len=1
   (groups-in-components-clauses) ; D1 len=2
   (level-component-clauses) ; D3 len = 2
   (level-group-clauses)   ; D4 len = 2
   (component-transitive-clauses) ;; len = 3
   (group-transitive-clauses) ;; len=3
   (representative-clauses)))  ;; len=unbound

(defgeneric literal-simplify-clauses (literal clauses)
  (:method ((literal literal) (clauses list))
    (loop
      for clause in clauses
      for new-clause = (literal-simplify-clause literal clause)
	then (literal-simplify-clause literal clause)
      unless (tautology-clause-p new-clause)
	collect new-clause)))

(defgeneric simplify-clauses-rec (clauses)
  (:method ((clauses list))
    (let ((ec (find-if #'empty-clause-p clauses)))
      (if ec
       (list ec)
       (let ((one-literal-clause (find-if #'one-literal-clause-p clauses)))
	 (if one-literal-clause
	     (simplify-clauses-rec
	      (literal-simplify-clauses (clause-first-literal one-literal-clause) clauses))
	     clauses))))))

(defgeneric simplify-clauses (clauses)
  (:method ((clauses list))
    (simplify-clauses-rec clauses)))

(defgeneric once-simplify-clauses (clauses)
  (:method ((clauses list))
    (multiple-value-bind (one-literal-clauses clauses)
	(split-list clauses #'one-literal-clause-p)
      (loop
	with positive-literals = '()
	for c in one-literal-clauses
	for l = (clause-first-literal c) then (clause-first-literal c)
	do (setq clauses (literal-simplify-clauses l clauses))
	do (unless (negated-p l)
	     (push l positive-literals))
	finally (return (values clauses positive-literals))))))

(defgeneric clauses-nb-literals (clauses)
  (:method ((clauses list))
    (reduce #'+ (mapcar #'clause-nb-literals clauses))))

(defgeneric clause< (clause1 clause2)
  (:method ((clause1 clause) (clause2 clause))
    (< (clause-size clause1) (clause-size clause2))))

(defgeneric clause> (clause1 clause2)
  (:method ((clause1 clause) (clause2 clause))
    (> (clause-size clause1) (clause-size clause2))))

(defgeneric clause>= (clause1 clause2)
  (:method ((clause1 clause) (clause2 clause))
    (>= (clause-size clause1) (clause-size clause2))))

(defgeneric isort-clauses (clauses)
  (:method ((clauses list))
    (sort clauses (lambda (c1 c2) (clause> c1 c2)))))

(defgeneric isorted-p (clauses)
  (:method ((clauses list))
    (loop for c1 in (butlast clauses)
	  for c2 in (cdr clauses)
	  unless (clause>= c1 c2)
	    do (return)
	  finally (return t))))
