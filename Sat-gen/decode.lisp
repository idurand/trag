(in-package :sat-gen)

(defun literals-vector (literals)
  (coerce
   (mapcar #'plusp (cons 0 literals)) 'vector))

(defun solution-string-to-literals (solution-string)
  (with-input-from-string (in solution-string)
    (loop 
      for i = (read in nil nil)
      until (zerop i)
      collect i)))
  
(defun load-literals (file-solution)
  (with-open-file (in file-solution)
    (solution-string-to-literals (read-line in))))

(defun save-literals (literals file-solution)
  (with-open-file (out file-solution
		       :direction :output :if-exists :supersede)
    (loop
      for literal in literals
      do (format out "~A~%" (literal-from-code literal)))))

(defun save-variables (variables file-solution)
  (with-open-file (out file-solution
		       :direction :output :if-exists :supersede)
    (loop
      for variable in variables
      do (format out "~A~%" variable))))

(defun input-positive-variables (file-solution)
  (mapcar #'var-from-code
	  (remove-if #'minusp
		     (load-literals file-solution)
				      :from-end t)))

(defun input-negative-variables (file-solution)
  (mapcar #'var-from-code
	  (remove-if #'plusp
		     (load-literals file-solution)
				      :from-end t)))
