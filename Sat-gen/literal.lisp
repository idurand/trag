;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand
(in-package :sat-gen)

;;; literals
(defgeneric negated-p (literal))

(defclass literal ()
  ((var :initarg :var :reader var :type clause-var)
   (negated :initform nil :initarg :negated :reader negated-p)))

(defgeneric literal= (literal1 literal2)
  (:method ((literal1 literal) (literal2 literal))
    (and (var= (var literal1) (var literal2))
	 (eql (negated-p literal1) (negated-p literal2)))))

(defmethod print-object ((literal literal) stream)
  (format stream "~:[ ~;-~]" (negated-p literal))
  (format stream "~A" (var literal)))

(defun make-literal (var &optional negated)
  (make-instance 'literal :negated negated :var var))

(defgeneric negated-literal (literal)
  (:method ((literal literal))
    (make-literal (var literal) (not (negated-p literal)))))

(defvar *sat-zero-literal* (make-literal *sat-zero-var*))

(defun literal-from-code (code)
  (make-literal (var-from-code code) (minusp code)))

(defun code-from-literal (literal)
  (let ((code (var-code (var literal))))
    (if (negated-p literal)
	(- code)
	code)))

(defun make-component-literal (u v level negated)
  (make-literal (component-var u v level) negated))

(defun make-representative-literal (u level negated)
  (make-literal (representative-var u level) negated))

