;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Decomp.
(in-package :asdf-user)

(defsystem :sat-gen
  :description "Sat-gen: library for generic sat problems for graphs"
  :name "sat"
  :version "1.0"
  :author "Irène Durand"
  :depends-on (:graph :clique-width)
  :serial t
  :components ((:file "package")
	       (:file "sat-gen")
	       (:file "var")
	       (:file "literal")
	       (:file "clause")
	       (:file "clauses")
	       (:file "decode")
	       ))

(pushnew :sat-gen *features*)
