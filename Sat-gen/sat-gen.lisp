;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand
(in-package :sat-gen)

(defvar *sat-debug* nil)

(defun toggle-sat-debug () (setq *sat-debug* (not *sat-debug*)))
(defvar *sat-nb-arcs*)
(defvar *sat-nb-nodes*)
(defvar *sat-nb-colors*)
(defvar *sat-arcs*)

(defun nb-groups () (/ (* *sat-nb-nodes* (1- *sat-nb-nodes*)) 2))
(defun nb-levels () (1+ (- *sat-nb-nodes* *sat-nb-colors*)))

;;; variables
(defun nb-component-vars () (* (1+ (nb-levels)) (nb-groups)))
(defun nb-group-vars () (* (nb-levels) (nb-groups)))

(defun nb-arc-vars () 
  (* *sat-nb-arcs* (1- *sat-nb-arcs*)))

(defun get-arc-code (u v)
  (+ (/ (* (1- u) (- (* 2 *sat-nb-nodes*) u 2)) 2) (1- v)))

(defun get-edge-code (u v)
  (get-arc-code (min u v) (max u v)))

(defun make-grid (initial-element)
  (make-array (make-list 2 :initial-element (1+ *sat-nb-nodes*)) :initial-element initial-element))

(defun make-arcs (graph)
  (let ((grid (make-grid nil)))
    (container-map
     (lambda (arc)
       (let ((origin (num (origin arc)))
	     (extremity (num (extremity arc))))
	 (setf (aref grid origin extremity ) t)
	 ))
      (graph-arcs graph))
    grid))

(defun is-arc (u v) (aref *sat-arcs* u v))

(defun init-sat-gen-vars (graph nb-colors)
  (setq *sat-nb-nodes* (graph-nb-nodes graph))
  (setq *sat-nb-arcs* (graph-nb-arcs graph))
  (setq *sat-arcs* (make-arcs graph))
  (setq *sat-nb-colors* nb-colors))

(defun make-component-vars ()
  (loop
    for u from 1 to *sat-nb-nodes*
    do (loop
	 for v from (1+ u) to *sat-nb-nodes*
	 do (loop
	      for level from 0 to (nb-levels)
	      do (make-component-var u v level)))))

(defun make-group-vars ()
  (loop
    for level from 1 to (nb-levels)
    do (loop
	 for u from 1 to *sat-nb-nodes*
	 do (loop
	      for v from (1+ u) to *sat-nb-nodes*
	      do (make-group-var u v level)))))
  
(defun dimacs-name (name) (extend-name name "arc"))

(defun out-name (name) (extend-name name "out"))

(defun sol-name (cwd-name) (extend-name cwd-name "sol"))

(defun decomp-name (cwd-name) (extend-name cwd-name "decomp"))

(defun cnf-name (cwd-name) (extend-name cwd-name "cnf"))

(defun cwd-name (name cwd) (format nil "~A.~A" name cwd))

(defun absolute-dimacs-name (name)
  (absolute-tmp-filename (dimacs-name name)))

(defun absolute-out-name (cwd-name)
  (absolute-tmp-filename (out-name cwd-name)))

(defun absolute-cnf-name (cwd-name)
  (absolute-tmp-filename (cnf-name cwd-name)))

(defun absolute-sol-name (cwd-name)
  (absolute-tmp-filename (sol-name cwd-name)))

(defun absolute-decomp-name (cwd-name)
  (absolute-tmp-filename (decomp-name cwd-name)))

(defgeneric call-glucose (cnf-file out-file)
  (:method (cnf-file out-file)
    (uiop:run-program
     (list "glucose" cnf-file "-model")
     :ignore-error-status t
     :output out-file :if-output-exists :supersede)))

(defgeneric glucose (clauses &key name sort)
  (:method ((clauses list) &key (name "toto") (sort t))
    (when sort
      (setq clauses (isort-clauses clauses)))
    (let ((cnf-file (absolute-cnf-name name))
	  (sol-file (absolute-sol-name name))
	  (out-file (absolute-out-name name))
	  (*sat-debug* nil))
       (with-open-file (*standard-output* cnf-file :direction :output :if-exists :supersede)
	 (display-clauses clauses))
      (call-glucose cnf-file out-file)
      (let ((satisfiable (call-grep out-file))
	    (literals nil))
	(when satisfiable
	  (call-cut out-file sol-file)
	  (setq literals (load-literals sol-file)))
	(values satisfiable literals)))))

(defgeneric call-grep (filename)
  (:method (filename)
    (with-open-file (f filename)
      (loop for l := (read-line f nil nil)
	    while l
	    when (cl-ppcre:scan "^s SATISFIABLE" l)
	      return t))))

(defgeneric call-rm (&rest filenames)
  (:method (&rest filenames)
    (map nil #'delete-file filenames)))

(defgeneric call-cut (out-file sol-file)
  (:method ((out-file string) (sol-file string))
    (with-open-file (f out-file)
      (loop for l := (read-line f nil nil)
	    while l
	    when (and (> (length l) 2)
		      (equal (subseq l 0 2) "v "))
	      do (progn
		   (with-open-file
		       (ff
			sol-file
			:direction :output :if-exists :supersede)
		     (princ (subseq l 2) ff))
		   (return nil))))))

