;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand
(in-package :sat-gen)

(defvar *clause-vars*)
(setq *clause-vars* (make-hash-table :test #'equal))

(defun sat-vars ()
  (let ((clause-vars '()))
    (maphash (lambda (k v)
	       (declare (ignore k))
	       (push v clause-vars))
	     *clause-vars*)
    (nreverse clause-vars)))
    
(defun display-sat-vars ()
  (loop for var in (sat-vars)
	do (print var)))

(defun var-from-code (num)
  (maphash (lambda (k v)
	     (declare (ignore k))
	     (when (= (var-code v) num)
	       (return-from var-from-code num)))
	   *clause-vars*))

(defun nb-vars () (hash-table-count *clause-vars*))

(defclass clause-var ()
  ((var-name :type string :reader var-name :initarg :var-name)
   (var-code :type integer :reader var-code :initarg :var-code)))

(defgeneric var= (v1 v2)
  (:method ((v1 clause-var) (v2 clause-var))
    (equal (var-name v1) (var-name v2))))

(defgeneric var< (v1 v2)
  (:method ((v1 clause-var) (v2 clause-var))
    (< (var-code v1) (var-code v2))))

(defmethod print-object ((clause-var clause-var) stream)
  (if *sat-debug* 
      (format stream "~A:~A" (var-name clause-var) (var-code clause-var))
      (format stream "~A" (var-code clause-var))))


(defun make-clause-var (var-name)
  (assert (not (gethash  var-name *clause-vars*)))
  (make-instance
   'clause-var :var-name var-name :var-code (hash-table-count *clause-vars*)))

(defmethod initialize-instance :after ((clause-var clause-var) &key &allow-other-keys)
  (setf (gethash (var-name clause-var) *clause-vars*) clause-var))

(defun make-zero-var ()
  (make-clause-var "X0"))

(defvar *sat-zero-var* (make-zero-var))

(defun var-from-name (var-name)
  (gethash var-name *clause-vars*))

(defun init-clause-vars ()
  (setq *clause-vars* (make-hash-table :test #'equal))
  (make-zero-var))

(defun arc-var-name (u v)
  (format nil "e_~A,~A" u v))

(defun make-arc-var (u v)
  (make-clause-var (arc-var-name u v)))

(defun component-var-name (u v level)
  (format nil "c_~A,~A,~A" u v level))

(defun make-component-var (u v level)
  (make-clause-var (component-var-name u v level)))

(defun component-var (u v level)
  (var-from-name (component-var-name u v level)))

(defun group-var-name (u v level)
  (format nil "g_~A,~A,~A" (min u v) (max u v) level))

(defun make-group-var (u v level)
  (assert (plusp level))
  (make-clause-var (group-var-name u v level)))

(defun group-var (u v level)
  (var-from-name (group-var-name u v level)))

(defun representative-var-name (u level)
  (format nil "r_~A,~A" u level))

(defun make-representative-var (u level)
  (make-clause-var (representative-var-name u level)))

(defun representative-var (u level)
  (var-from-name (representative-var-name u level)))

(defun s-var-name (u a level)
  (format nil "s_~A,~A,~A" u a level))

(defun make-s-var (u a level)
  (make-clause-var (s-var-name u a level)))

(defun make-arc-vars ()
  (loop
    for u from 1 to *sat-nb-nodes*
    do (loop
	 for v from 1 to *sat-nb-nodes*
	 when (/= u v)
	 do (make-arc-var u v))))

(defun arc-var (u v)
  (var-from-name (arc-var-name u v)))

(defun s-var (u a level)
  (var-from-name (s-var-name u a level)))

(defun make-s-vars ()
  (loop
    for level from 1 to (nb-levels)
    do (loop
	 for u from 1 to *sat-nb-nodes*
	 do (loop
	      for c from 1 to *sat-nb-colors*
	      do (make-s-var u c level)))))

(defun blk-var-name (u num-blk)
  (format nil "b_~A,~A" u num-blk))

(defun make-blk-var (u num-blk)
  (make-clause-var (blk-var-name u num-blk)))

(defun make-blk-vars (nb-blks)
  (loop
    for u from 1 to *sat-nb-nodes*
    do (loop
	 for num-blk from 1 to nb-blks
	 do (make-blk-var u num-blk))))
    
(defun blk-var (u num-blk)
  (var-from-name (blk-var-name u num-blk)))
