;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand
(in-package :sat-problems)

(defun cwd-inf-k-make-clause-vars ()
  (init-clause-vars)
  (make-component-vars)
  (make-group-vars)
  (make-representative-vars)
  (make-s-vars)
  (make-arc-vars))

(defun cwd-inf-k-init-globals (graph nb-colors)
  (init-sat-gen-vars graph nb-colors)
  (cwd-inf-k-make-clause-vars))

(defun cwd-inf-k-neighbor-clauses (u v w level)
  (let ((uv (arc-var u v))
	(uw (arc-var u w))
	(wv (arc-var w v))
	(cuv (component-var (min u v) (max u v) (1- level)))
	(gvw (group-var (min v w) (max v w) level))
	(guw (group-var (min u w) (max u w) level)))
    (list
     (make-clause-from-literals
      (make-literal uv t)
      (make-literal uw)
      (make-literal cuv)
      (make-literal gvw t))
     (make-clause-from-literals
      (make-literal uv t)
      (make-literal wv)
      (make-literal cuv)
      (make-literal guw t)))))
   
(defun level-cwd-inf-k-neighbor-clauses (level)
  (remove-duplicates
   (loop
     for u from 1 to *sat-nb-nodes*
     nconc
     (loop
       for v from 1 to *sat-nb-nodes*
       nconc
       (loop
	 for w from 1 to *sat-nb-nodes*
	 when (/= u v w)
	   nconc (cwd-inf-k-neighbor-clauses u v w level))))
   :test #'clause= :from-end t))

(defun cwd-inf-k-neighbor-property-clauses ()
   (loop
     for level from 1 to (nb-levels)
     nconc (level-cwd-inf-k-neighbor-clauses level)))

(defun cwd-inf-k-path-clause (u v w x level)
  (make-clause-from-literals
   (make-literal (arc-var u v) t)
   (make-literal (arc-var u w) t)
   (make-literal (arc-var x v) t)
   (make-literal (arc-var x w))
   (make-literal (group-var (min u x) (max u x) level) t)
   (make-literal (group-var (min v w) (max v w) level) t)))

(defun u-v-cwd-inf-k-path-clauses (level u v)
  (loop
    for w from 1 to *sat-nb-nodes*
    nconc (loop
	    for x from 1 to *sat-nb-nodes*
	    when (/= u v x w)
	      collect (cwd-inf-k-path-clause u v w x level))))

(defun u-cwd-inf-k-path-clauses (level u)
  (loop
    for v from 1 to *sat-nb-nodes*
    nconc (u-v-cwd-inf-k-path-clauses level u v)))

(defun level-cwd-inf-k-path-clauses (level)
  (loop
    for u from 1 to *sat-nb-nodes*
    nconc (u-cwd-inf-k-path-clauses level u)))

(defun cwd-inf-k-path-property-clauses ()
  (loop
    for level from 1 to (nb-levels)
    nconc (level-cwd-inf-k-path-clauses level)))

(defun arc-clause (u v)
  (make-clause-from-literals
   (make-literal (arc-var u v) (not (is-arc u v)))))

(defun arc-clauses ()
  (loop
    for u from 1 to *sat-nb-nodes*
    nconc
    (loop
      for v from 1 to *sat-nb-nodes*
      when (/= u v)
	collect (arc-clause u v))))

(defun arc-component-group-clause (u v level)
  (let ((c (component-var u v (1- level)))
	(g (group-var u v level))
	(uv (arc-var u v))
	(vu (arc-var v u)))
    (list
     (make-clause-from-literals
      (make-literal uv t) (make-literal c) (make-literal g t))
     (make-clause-from-literals
      (make-literal vu t) (make-literal c) (make-literal g t)))))
     

(defun level-cwd-inf-k-arc-property-clauses (level)
  (loop
    for u from 1 to *sat-nb-nodes*
    nconc (loop
	    for v from (1+ u) to *sat-nb-nodes*
	    nconc (arc-component-group-clause u v level))))

(defun cwd-inf-k-arc-property-clauses ()
  (loop
    for level from 1 to (nb-levels)
    nconc (level-cwd-inf-k-arc-property-clauses level)))

(defun cwd-inf-k-arc-dependent-clauses ()
  (nconc
   (arc-clauses)
   (cwd-inf-k-arc-property-clauses)
   (cwd-inf-k-neighbor-property-clauses)
   (cwd-inf-k-path-property-clauses)))
	 
(defgeneric cwd-inf-k-clauses (graph cwd)
  (:method :before ((graph graph) (cwd integer))
    (assert (> (graph-nb-nodes graph) cwd)))
  (:method ((graph graph) (cwd integer))
    (cwd-inf-k-init-globals graph cwd)
    (nconc
     (cwd-inf-k-arc-dependent-clauses)
     (arc-independent-clauses))))

(defgeneric cwd-inf-k-p (graph-or-file cwd)
  (:method :around ((graph graph) (cwd integer))
    (or (<= (graph-nb-nodes graph) cwd)
	(call-next-method)))
  (:method ((graph graph) (cwd integer))
    (let ((name (name graph)))
      (unless (oriented-p graph)
	(setq graph (graph-orient graph)))
      (glucose (cwd-inf-k-clauses graph cwd) :name name)))
  (:method ((dimacs-file string) (cwd integer))
      (cwd-inf-k-p (load-dimacs-graph dimacs-file) cwd)))
