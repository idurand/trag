;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand
(in-package :sat-problems)

;; very slow method
;; (defgeneric cwd-k-p (graph-or-file cwd)
;;   (:method ((graph graph) (cwd integer))
;;     (assert (> cwd 1))
;;     (and
;;      (not (cwd-inf-k-p graph (1- cwd)))
;;      (cwd-inf-k-p graph cwd))))
