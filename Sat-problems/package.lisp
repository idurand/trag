;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :sat-problems
  (:use :common-lisp
   :general :input-output :vbits :object :color :symbols
   :container :graph :sat-gen :decomp)
  (:export
   cwd-inf-k-p
   ))

