;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; Block relative cwd
(in-package :sat-problems)

(defgeneric blks-ok? (graph blks)
  (:method ((graph graph) (blks list))
    (let ((n (graph-nb-nodes graph)))
      (and
       (some
	(lambda (blk)
	  (let ((len (length blk)))
	    (or (> len 1) (< len n))))
	blks)
       (= n (length (reduce #'append blks)))))))
		      
(defun br-cwd-k-make-clause-vars (blks)
  (init-clause-vars)
  (make-component-vars)
  (make-group-vars)
  (make-representative-vars)
  (make-s-vars)
  (make-blk-vars (length blks)))

(defun br-cwd-inf-k-make-clause-vars (blks)
  (init-clause-vars)
  (make-component-vars)
  (make-group-vars)
  (make-representative-vars)
  (make-s-vars)
  (make-arc-vars)
  (make-blk-vars (length blks)))

(defun br-cwd-k-init-globals (graph blks nb-colors)
  (init-sat-gen-vars graph nb-colors)
  (br-cwd-k-make-clause-vars blks))

(defun br-cwd-inf-k-init-globals (graph blks nb-colors)
  (init-sat-gen-vars graph nb-colors)
  (br-cwd-inf-k-make-clause-vars blks))

(defun blk-clauses (blks)
  (loop 
    for u from 1 to *sat-nb-nodes*
    nconc
    (loop
      for num-blk from 1 to (length blks)
      collect (make-clause-from-literals
	       (make-literal (blk-var u num-blk) (member u (aref blks (1- num-blk))))))))

(defgeneric br-cwd-inf-k-clauses (graph blks cwd)
  (:method :before ((graph graph) (blks list) (cwd integer))
    (assert (> (graph-nb-nodes graph) cwd))
    (assert (blks-ok? graph blks)))
  (:method ((graph graph) (blks list) (cwd integer))
    (br-cwd-inf-k-init-globals graph blks cwd)
    (nconc
     (blk-clauses (coerce blks 'vector))
     (cwd-inf-k-arc-dependent-clauses)
     (arc-independent-clauses))))

(defgeneric br-cwd-k-clauses (graph blks cwd)
  (:method :before ((graph graph) (blks list) (cwd integer))
    (assert (> (graph-nb-nodes graph) cwd))
    (assert (blks-ok? graph blks)))
  (:method ((graph graph) (blks list) (cwd integer))
    (br-cwd-k-init-globals graph blks cwd)
    (nconc
     (blk-clauses (coerce blks 'vector))
     (decomp::arc-dependent-clauses)
     (arc-independent-clauses))))

(defgeneric br-cwd-inf-k-p (graph-or-file blks cwd)
  (:method ((graph graph) (blks list) (cwd integer))
    (let ((name (name graph)))
      (unless (oriented-p graph)
	(setq graph (graph-orient graph)))
      (glucose (br-cwd-inf-k-clauses graph blks cwd) :name name)))
  (:method ((dimacs-file string) (blks list) (cwd integer))
    (br-cwd-inf-k-p (load-dimacs-graph dimacs-file) blks cwd)))

(defgeneric br-cwd-k-p (graph-or-file blks cwd)
  (:method ((graph graph) (blks list) (cwd integer))
    (let ((name (name graph)))
      (unless (oriented-p graph)
	(setq graph (graph-orient graph)))
      (glucose (br-cwd-k-clauses graph blks cwd) :name name)))
  (:method ((dimacs-file string) (blks list) (cwd integer))
      (br-cwd-k-p (load-dimacs-graph dimacs-file) blks cwd)))

(defgeneric br-graph-cwd (graph blks)
  (:method ((graph graph) (blks list))
    (let ((cwd 1)
	  literals)
      (loop
	with ok = nil
	do (format t " Trying ~A~%" (1+ cwd))
	do (multiple-value-bind (res lit) (br-cwd-k-p graph blks (incf cwd))
	     (setq ok res)
	     (setq literals lit))
	until ok)
      (values cwd literals))))

(defgeneric graph-br-cwd (graph blks)
  (:method ((graph graph) (blks list))
    (let ((cwd 1)
	  literals)
      (loop
	with ok = nil
	do (format t " Trying ~A~%" (1+ cwd))
	do (multiple-value-bind (res lit) (br-cwd-k-p graph blks (incf cwd))
	     (setq ok res)
	     (setq literals lit))
	until ok)
      (values cwd literals))))

(defgeneric br-cwd-sat-derivation (graph blks)
  (:method ((graph graph) (blks list))
    (multiple-value-bind (cwd literals) (graph-br-cwd graph blks)
      (values (decomp::decode-graph-literals graph literals) cwd))))

(defgeneric br-cwd-sat-decomposition (graph blks)
  (:method ((graph graph) (blks list))
    (let ((oterm (decomp::derivation-to-term (br-cwd-sat-derivation graph blks) graph)))
      (if (oriented-p graph)
	  oterm
	  (clique-width::cwd-term-unorient oterm))))
  (:method ((file string) (blks list))
    (br-cwd-sat-decomposition (load-dimacs-graph file) blks)))
  
(defun br-cwd-decomposition (graph blks)
  (br-cwd-sat-decomposition graph blks))

(defparameter *mgee-blks*
  '((1 7 9) (2 3 4 5 6 11 12 14 15 17 18) (8) (10) (13 19 21) (16 22 24) (20)
       (23)))
