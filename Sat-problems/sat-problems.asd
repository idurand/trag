;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Sat-problems.
(in-package :asdf-user)

(defsystem :sat-problems
  :description "Sat: library for generic sat problems for graphs"
  :name "sat-problems"
  :version "1.0"
  :author "Irène Durand"
  :depends-on (:sat-gen :decomp)
  :serial t
  :components ((:file "package")
	       (:file "cwd-inf-k")
	       (:file "cwd-k")
	       ))

(pushnew :sat-problems *features*)
