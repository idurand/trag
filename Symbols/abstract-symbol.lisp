;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)
(defvar *show-ac* t)

(defun initial-symbol-table ()
  "keys are concrete classes of symbols,
   values the list of symbols of the class"
  (make-hash-table :test #'eq))

(defvar *symbol-table* (initial-symbol-table))

(defun init-symbol-table () (setq *symbol-table* (initial-symbol-table)))

(defgeneric symbol-commutative-p (symbol))
(defgeneric symbol-constant-p (symbol))
(defgeneric symbol-of (decorated-symbol))
(defgeneric symbols (symbol)
  (:documentation "all symbols of the class of this symbol"))

(defgeneric symbols (symbol-or-class) 
  (:documentation "all symbols of the class (of the symbol)"))

(defgeneric (setf symbols) (value symbol-class) 
  (:documentation "modifier of the list of symbols of class"))

(defclass abstract-symbol (signed-object) ()
  (:documentation "abstract class for all symbols"))

(defmethod symbols ((s abstract-symbol))
  (symbols (class-of s)))

(defmethod symbols ((class standard-class))
  (gethash class *symbol-table*))

(defmethod (setf symbols) ((value list) (s abstract-symbol))
  (setf (symbols (class-of s)) value))

(defmethod (setf symbols) ((value list) (class standard-class))
  (setf (gethash class *symbol-table*) value))

(defmethod symbol-of ((s abstract-symbol)) s)

(defmethod print-object ((s abstract-symbol) stream)
  (format stream "~A" (name s)))

(defclass commutative-mixin () ()
  (:documentation "adds commutativity to symbol"))

;; a concrete symbol must be either constant-mixin or non-constant-mixin
(defclass constant-mixin () ()
  (:documentation "for constant symbols"))

(defmethod arity ((s constant-mixin)) 0)

(defclass non-constant-mixin () ()
  (:documentation "for non-constant symbols"))

(defclass abstract-constant-symbol (constant-mixin abstract-symbol) ()
  (:documentation "class to derive concrete classes of constant-symbols"))

(defclass abstract-parity-symbol (non-constant-mixin abstract-symbol)
  ((arity :initarg :arity :reader arity))
  (:documentation "class to derive concrete classes of non-constant-symbols"))

(defclass abstract-non-constant-symbol (non-constant-mixin abstract-symbol) ()
  (:documentation "class to derive concrete classe of non constant symbols"))

(defclass abstract-named-symbol (named-mixin abstract-symbol) ()
  (:documentation "class to derive concrete classes of named constant-symbols"))

(defmethod compare-object
    ((s1 abstract-named-symbol) (s2 abstract-named-symbol))
  (equal (name s1) (name s2)))

(defgeneric cast-symbol (symbol)
  (:documentation 
   "if similar symbol exists return the existing one otherwise \
    save symbol as the representative of the class")
  (:method ((symbol abstract-symbol))
    (or (find-object symbol (symbols symbol))
	(car (push symbol (symbols symbol))))))
