;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

;;(defclass annotated-symbol (annotation-mixin decorated-symbol non-constant-mixin) ()
(defclass annotated-symbol (annotation-mixin decorated-symbol abstract-symbol) ()
  (:documentation
   "should not be casted because thay are not real symbols \
    but contain a symbol"))

(defun annotated-symbol-p (s) (typep s 'annotated-symbol))

(defmethod print-object ((s annotated-symbol) stream)
  (prin1 (symbol-of s) stream))

(defgeneric annotate-symbol (symbol annotation)
  (:documentation "annotated symbol containing SYMBOL and ANNOTATION")
  (:method ((symbol abstract-symbol) annotation)
    (make-instance 'annotated-symbol :symbol symbol :annotation annotation)))

(defmethod color ((annotated-symbol annotated-symbol))
  (color (symbol-of annotated-symbol)))

(defmethod apply-signature-mapping ((s annotated-symbol))
  (mapcar (lambda (sym)
	    (annotate-symbol sym (annotation s)))
	  (apply-signature-mapping (symbol-of s))))

(defmethod in-signature ((sym annotated-symbol) (signature abstract-signature))
  (in-signature (symbol-of sym) signature))

(defgeneric remove-annotation-if (annotated-signature-object predicate)
  (:method ((sym annotated-symbol) (pred function))
    (if (funcall pred (annotation sym))
	(symbol-of sym)
	sym))
  (:method ((sym abstract-symbol) (pred function)) ;; non annotated
    sym))

(defgeneric remove-annotation-if-not (annotated-signature-object predicate)
  (:method ((sym annotated-symbol) (pred function))
    (if (not (funcall pred (annotation sym)))
	(symbol-of sym)
	sym))
  (:method ((sym abstract-symbol) (pred function))
    sym))
