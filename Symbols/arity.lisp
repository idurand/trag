;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric arity (symbol)
  (:documentation "arity ot SYMBOL"))

(defclass parity-mixin ()
  ((arity :initarg :arity :reader arity))
  (:documentation "mixin to store the arity of a symbol"))
