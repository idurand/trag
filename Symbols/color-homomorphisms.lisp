;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; homomorphisms on colored symbols
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric nothing-to-random-color-body (symbol k)
  (:documentation "c -> { c~rand(k) }, f -> { f }")
  (:method ((sym (eql *empty-symbol*)) k)
    '())
  (:method ((symbol constant-mixin) k)
    (list (make-random-color-symbol symbol k)))
  (:method ((symbol abstract-symbol) k)
    (list symbol)))

(defun nothing-to-random-color-h (k)
  (lambda (s)
    (nothing-to-random-color-body s k)))

(defgeneric nothing-to-random-color (signed-object k)
  (:method ((signed-object signed-object) k)
    (homomorphism
     (add-empty-symbols signed-object)
     (nothing-to-random-color-h k)
     (nothing-to-random-color-ih)
     (cons :nb-colors k))))

(defgeneric nothing-to-color-f (symbol signature k color)
  (:documentation "c -> { c~1, ..., c~k }, f -> { f }")
  (:method ((s (eql *empty-symbol*)) (csign signature) k color)
    (format t " nothing-to-color-f empty-symbol ~A ~A ~%" k color)
    (mapcan
     (lambda (c)
       (mapcar (lambda (cs)
		 (make-color-symbol cs c))
	       (signature-symbols csign)))
     (remove color (color-iota k))))
  (:method ((s constant-mixin) (csign abstract-signature) k color)
    (list (make-color-symbol s color)))
  (:method ((s abstract-symbol) (csign abstract-signature) k color)
    (list s)))

(defgeneric nothing-to-color-h (signature k color)
  (:method ((csign abstract-signature) k color)
    (lambda (s)
      (nothing-to-color-f s csign k color))))

(defgeneric nothing-to-colors-constants-f (symbol signature k)
  (:method ((s constant-mixin) (csign abstract-signature) k)
    (mapcar
     (lambda (c)
       (make-color-symbol s c))
     (color-iota k)))
  (:method ((s abstract-symbol) (csign abstract-signature) k)
    (list s)))

(defgeneric nothing-to-colors-constants-h (signature k)
  (:method ((csign abstract-signature) k)
    (lambda (s)
      (nothing-to-colors-constants-f s csign k))))

(defgeneric nothing-to-colors-f (symbol signature k)
  (:method ((s abstract-symbol) (signature abstract-signature) k)
    (mapcar
     (lambda (c)
       (make-color-symbol s c))
     (color-iota k))))

(defgeneric nothing-to-colors-h (signature k)
  (:method ((signature abstract-signature) k)
    (lambda (s)
      (nothing-to-colors-f s signature k))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TO BE TESTED
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric nothing-to-c1-union-c2-f (symbol signature k c1 c2)
  (:method ((s (eql *empty-symbol*)) (csign abstract-signature) k c1 c2)
    (let ((colors (remove c1 (remove c2
				     (color-iota k))))
	  (csyms (signature-symbols csign)))
      (mapcan
       (lambda (cs)
	 (mapcar (lambda (color)
		   (make-color-symbol cs color))
		 colors))
       csyms)))
  (:method  ((s constant-mixin) (csign abstract-signature) k c1 c2)
    (let ((colors (list c1 c2)))
      (mapcar
       (lambda (color)
	 (make-color-symbol s color))
       colors)))
  (:method ((s abstract-symbol) (csign abstract-signature) k c1 c2)
    (list s)))

(defgeneric nothing-to-c1-union-c2-h (signature k c1 c2)
  (:method ((csign abstract-signature) k c1 c2)
    (lambda (s)
      (nothing-to-c1-union-c2-f s csign k c1 c2))))
