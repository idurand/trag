;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric nothing-to-color-if (symbol color)
  (:method ((s color-constant-symbol) color)
    (let ((c (color s)))
      (list (if (= c color) (symbol-of s) *empty-symbol*))))
  (:method ((s abstract-symbol) color)
    (list s)))

(defun nothing-to-color-ih (color)
  (lambda (s) (nothing-to-color-if s color)))

(defgeneric nothing-to-colors-constants-if (symbol)
  (:method ((s color-constant-symbol))
    (list (symbol-of s)))
  (:method ((s abstract-symbol))
    (list s)))

(defun nothing-to-colors-constants-ih ()
  (lambda (s) (nothing-to-colors-constants-if s)))

(defgeneric nothing-to-colors-if (symbol)
  (:method ((s color-symbol))
    (list (symbol-of s)))
  (:method ((s abstract-symbol))
    (list s)))

(defun nothing-to-colors-ih ()
  (lambda (s) (nothing-to-colors-if s)))

(defgeneric nothing-to-c1-union-c2-if (symbol c1 c2)
  (:method ((s color-constant-symbol) c1 c2)
    (let ((c (color s)))
      (list
       (if (or (= c c1) (= c c2))
	   (symbol-of s)
	   *empty-symbol*))))
  (:method ((s abstract-symbol) c1 c2)
    (list s)))

(defun nothing-to-c1-union-c2-ih (c1 c2)
  (lambda (s) (nothing-to-c1-union-c2-if s c1 c2)))

(defgeneric nothing-to-random-color-if (symbol)
  (:method ((s color-constant-symbol))
    (list (symbol-of s)))
  (:method ((s abstract-symbol))
    (list s)))

(defun nothing-to-random-color-ih ()
  (lambda (s) (nothing-to-random-color-if s)))

(defgeneric nothing-to-random-partition-if (symbol)
  (:method ((s vbits-constant-symbol))
    (list (symbol-of s)))
  (:method ((s abstract-symbol))
    (list s)))

(defun nothing-to-random-partition-ih ()
  (lambda (s) (nothing-to-random-partition-if s)))
