;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric constants-color-projection-f (abstract-symbol)
  (:documentation "c~k -> {c}")
  (:method ((s abstract-symbol))
    (list s))
  (:method ((s color-constant-symbol))
    (list (symbol-of s))))

(defun constants-color-projection-h ()
  (lambda (s) (constants-color-projection-f s)))

(defgeneric constants-color-projection-if (constant-mixin k)
  (:method ((s abstract-symbol) (k integer))
    (list s))
  (:method ((s constant-mixin) (k integer))
    (mapcar
     (lambda (color)
       (make-color-symbol s color))
     (color-iota k))))

(defgeneric constants-color-projection-ih (k)
  (:method ((k integer))
    (lambda (s)
      (constants-color-projection-if s k))))

(defgeneric constants-color-projection (signed-object)
  (:documentation "projection which removes colors on constants")
  (:method ((signed-object signed-object))
    (let ((k (signature-nb-colors (signature signed-object))))
      (homomorphism
       signed-object
       (constants-color-projection-h)
       (constants-color-projection-ih k)
       (cons :nb-colors 0)
       nil))))


