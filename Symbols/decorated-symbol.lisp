;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric decoration-of (decorated-symbol)
  (:documentation
   "the decoration depends on the class of symbol \
    vbits, color"))

(defclass decorated-symbol (abstract-symbol)
  ((symbol :initarg :symbol :reader symbol-of)))

(defmethod arity ((s decorated-symbol))
  (arity (symbol-of s)))

(defmethod symbol-commutative-p ((s decorated-symbol))
  (symbol-commutative-p (symbol-of s)))

(defmethod symbol-constant-p ((s decorated-symbol))
  (symbol-constant-p (symbol-of s)))

(defmethod name ((s decorated-symbol))
  (name (symbol-of s)))

(defmethod print-object ((s decorated-symbol) stream)
  (format stream "~A~A" (symbol-of s) (decoration-of s)))

(defgeneric decoration-projection-f (decorated-symbol)
  (:method ((s decorated-symbol))
    (list (symbol-of s))))

(defun decoration-projection-h ()
  (lambda (s)
    (decoration-projection-f s)))
