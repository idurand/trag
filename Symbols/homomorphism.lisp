;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric homomorphism (o h ih plist &optional injective)
  (:documentation "transforms the symbols c of object o into
    (c1 ... ck) with h(c) -> {c1 ... ck} and conversely")
  (:method ((signed-object signed-object) h ih pair &optional (first-injective t))
    (let ((*signature-mapping* (make-mapping h first-injective))
	  (*inverse-signature-mapping* (make-mapping ih))
	  (*signature-pair* pair))
      (apply-signature-mapping signed-object))))
