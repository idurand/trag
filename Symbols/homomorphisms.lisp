;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric empty-symbols (signature)
  (:documentation "todo")
  (:method ((signature abstract-signature))
    (let ((nb-colors (signature-nb-colors signature)))
      (if (plusp nb-colors)
	  (nothing-to-colors-constants *empty-symbol* nb-colors)
	  (let ((nb-set-variables (signature-nb-set-variables signature)))
	    (if (plusp nb-set-variables)
		(nothing-to-vbits-constants *empty-symbol* nb-set-variables)
		(list *empty-symbol*)))))))

(defgeneric add-empty-symbols (signed-object)
  (:method ((o signed-object)) o)
  (:method ((s abstract-symbol)) s)
  (:method ((s abstract-signature))
    (adjoin-symbols-to-signature (empty-symbols s) s)))

(defgeneric vbits-cylindrification (signed-object positions)
  (:method ((signed-object signed-object) (positions list))
;;    (format *trace-output* "vbits-cylindrification signed-object ~A~%" signed-object)
    (let ((m (signature-nb-set-variables (signature signed-object))))
      (hvmodification
       (if (zerop m) (add-empty-symbols signed-object) signed-object) ;; oops
       (vbits-projection-ifun positions)
       (vbits-projection-fun (complementary-positions positions (+ m (length positions))))
       (+ m (length positions))))))

(defgeneric vbits-projection (signed-object &optional positions)
  (:method ((signed-object signed-object) &optional (positions '()))
    (assert (every #'plusp positions))
    (let ((m (signature-nb-set-variables (signature signed-object))))
      (hvmodification
       signed-object
       (vbits-projection-fun positions)
       (vbits-projection-ifun (complementary-positions positions m))
       (length positions)
       nil ;; not injective
       ))))

(defgeneric xj-to-cxj (signed-object m j)
  (:method ((signed-object signed-object) m j)
    (hvmodification
     signed-object
     (xj-to-cxj-fun m j)
     (xj-to-cxj-ifun m j)
     m
     t ;; injective
     )))

(defgeneric xi-eq-cxj (signed-object m i j)
  (:method ((signed-object signed-object) m i j)
    (hvmodification
     signed-object
     (xi-eq-cxj-fun m i j)
     (xi-eq-cxj-ifun m i j)
     (1- m)
     t)))

(defgeneric x1-to-xj (signed-object m j)
  (:method :around ((signed-object signed-object) m j)
    (if (= m 1)
	signed-object
	(call-next-method)))
  (:method ((signed-object signed-object) m j)
      (hvmodification
       signed-object
       (x1-to-xj-fun m j)
       (x1-to-xj-ifun m j)
       m
       nil)))

(defgeneric x1-to-bool-xi (signed-object m bool)
  (:method :around ((signed-object signed-object) m bool)
    (if (= m 1)
	signed-object
	(call-next-method)))
  (:method ((signed-object signed-object) m bool)
    (hvmodification
     signed-object
     (x1-to-bool-xi-fun m bool)
     (x1-to-bool-xi-ifun m bool)
     m
     t)))

(defgeneric x1-to-xj1-bool-xj2 (signed-object m j1 j2 bool)
  (:method :around ((signed-object signed-object) m j1 j2 bool)
    (if (= 1 m) signed-object (call-next-method)))
  (:method ((signed-object signed-object) m j1 j2 bool)
    (hvmodification
     signed-object
     (x1-to-xj1-bool-xj2-fun m j1 j2 bool)
     (x1-to-xj1-bool-xj2-ifun m j1 j2 bool)
     m
     nil)))

(defgeneric y1-y2-to-xj1-xj2 (signed-object m j1 j2)
  (:method :around ((signed-object signed-object) m j1 j2)
    (if (and (= m j2 2) (= 1 j1)) signed-object (call-next-method)))
  (:method ((signed-object signed-object) m j1 j2)
    (assert (/= j1 j2))
    (assert (>= m 2))
    (hvmodification
     signed-object
     (y1-y2-to-xj1-xj2-fun m j1 j2)
     (y1-y2-to-xj1-xj2-ifun m j1 j2)
     m
     (= m 2))))

(defgeneric y1-y2-to-xj-xj1-bool-xj2  (signed-object m j j1 j2 bool)
  (:method ((signed-object signed-object) m j j1 j2 bool)
    (hvmodification
     signed-object
     (y1-y2-to-xj-xj1-bool-xj2-fun m j j1 j2 bool)
     (y1-y2-to-xj-xj1-bool-xj2-ifun m j j1 j2 bool)
     m
     nil)))

(defgeneric nothing-to-random-partition (signed-object m)
  (:method ((signed-object signed-object) m)
    (homomorphism
     (add-empty-symbols signed-object)
     (nothing-to-random-partition-h m)
     (nothing-to-random-partition-ih)
     (cons :nb-set-variables m))))

(defgeneric nothing-to-xj (signed-object m j)
  (:method ((signed-object signed-object) (m integer) (j integer))
    (assert (<= 0 j m))
    (homomorphism
     (add-empty-symbols signed-object)
     (nothing-to-xj-h m j (constant-signature signed-object))
     (nothing-to-xj-ih m j)
     (cons :nb-set-variables m))))

(defgeneric nothing-to-xj1-bool-xj2 (signed-object m j1 j2 bool)
  (:method ((signed-object signed-object) m j1 j2 bool)
    (homomorphism
     (add-empty-symbols signed-object)
     (nothing-to-xj1-bool-xj2-h m j1 j2 (signature signed-object) bool)
     (nothing-to-xj1-bool-xj2-ih m j1 j2 bool)
     (cons :nb-set-variables m)
     nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; derived
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric nothing-to-x1 (signed-object)
  (:method ((signed-object signed-object))
    (nothing-to-xj signed-object 1 1)))

(defgeneric x1-to-cx1 (signed-object)
  (:method ((signed-object signed-object))
    (xj-to-cxj signed-object 1 1)))

(defgeneric nothing-to-xj1-union-xj2 (signed-object m j1 j2)
  (:method ((signed-object signed-object) (m integer) (j1 integer) (j2 integer))
    (nothing-to-xj1-bool-xj2 signed-object m j1 j2 'union)))

(defgeneric nothing-to-xj1-intersection-xj2 (signed-object m j1 j2)
  (:method ((signed-object signed-object) (m integer) (j1 integer) (j2 integer))
    (nothing-to-xj1-bool-xj2 signed-object m j1 j2 'intersection)))

(defgeneric x1-to-xj1-union-xj2 (signed-object m j1 j2)
  (:method ((signed-object signed-object) m j1 j2)
    (x1-to-xj1-bool-xj2 signed-object m j1 j2 'union)))

(defgeneric x1-to-xj1-intersection-xj2 (signed-object m j1 j2)
  (:method ((signed-object signed-object) m j1 j2)
    (x1-to-xj1-bool-xj2 signed-object m j1 j2 'intersection)))

(defgeneric y1-y2-to-xj-xj1-union-xj2  (signed-object m j j1 j2)
  (:method ((signed-object signed-object) m j j1 j2)
    (y1-y2-to-xj-xj1-bool-xj2 signed-object m j j1 j2 'union)))

(defgeneric y1-y2-to-xj-xj1-intersection-xj2  (signed-object m j j1 j2)
  (:method ((signed-object signed-object) m j j1 j2)
    (y1-y2-to-xj-xj1-bool-xj2 signed-object m j j1 j2 'intersection)))

(defgeneric nothing-to-c1-union-c2 (signed-object k c1 c2)
  (:method ((signed-object signed-object) k c1 c2)
    (homomorphism
     (add-empty-symbols signed-object)
     (nothing-to-c1-union-c2-h (constant-signature signed-object) k c1 c2)
     (nothing-to-c1-union-c2-ih c1 c2)
     (cons :nb-colors k))))

(defgeneric nothing-to-color (signed-object k color)
  (:method ((signed-object signed-object) k color)
    (homomorphism
     (add-empty-symbols signed-object)
     (nothing-to-color-h (signature signed-object) k color)
     (nothing-to-color-ih color)
     (cons :nb-colors k))))

(defgeneric nothing-to-colors-constants (signed-object k)
  (:method ((signed-object signed-object) (k integer))
    (homomorphism
     signed-object
     (nothing-to-colors-constants-h (signature signed-object) k)
     (nothing-to-colors-constants-ih)
     (cons :nb-colors k))))

(defgeneric nothing-to-colors (signed-object k)
  (:method ((signed-object signed-object) k)
    (homomorphism
     signed-object
     (nothing-to-colors-h (signature signed-object) k)
     (nothing-to-colors-ih)
     (cons :nb-colors k))))

(defgeneric nothing-to-vbits-constants (signed-object m)
  (:method ((signed-object signed-object) (m integer))
    (assert (plusp m))
    (homomorphism
     signed-object
     (nothing-to-vbits-constants-h (signature signed-object) m)
     (nothing-to-vbits-constants-ih)
     (cons :nb-set-variable m))))
