;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric s-modification (symbol hv)
  (:documentation "applies hv to the vbits of symbol")
  (:method ((s abstract-symbol) hv)
    (list s))
  (:method ((s vbits-constant-symbol) hv)
    (let ((lvbits (funcall hv (vbits s))))
      (mapcar
       (lambda (vbits)
	 (make-vbits-symbol (symbol-of s) vbits))
       lvbits)))
  (:method ((s constant-mixin) hv)
    (let ((lvbits (funcall hv (make-vbits nil))))
      (mapcar
       (lambda (vbits)
	 (make-vbits-symbol s vbits))
       lvbits))))

(defun hv-fun (hv) (lambda (s) (s-modification s hv)))

(defgeneric hvmodification (s hv ihv nb-set-variable &optional injective)
  (:documentation "si s^w constante et h(w) = {w1 ...wk} -> (s^w1 ... s^vk) 
  sinon (s)")
  (:method ((s signed-object) hv ihv nb-set-variables &optional (injective t))
    (homomorphism
     s
     (hv-fun hv)
     (hv-fun ihv)
     (cons :nb-set-variables nb-set-variables)
     injective)))
