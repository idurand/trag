;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defun init-predefined-symbols ()
  (setq *omega-symbol* (make-constant-symbol "?"))
  (setq *extra-symbol* (make-constant-symbol "@"))
  (setq *bullet-symbol* (make-constant-symbol "o"))
  (setq *predefined-constant-symbols* (list *bullet-symbol* *extra-symbol*  *omega-symbol*)))

(defun init-symbols ()
  (init-symbol-table)
  (init-predefined-symbols))
