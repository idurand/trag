;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :symbols
  (:use :common-lisp :general :vbits :color :names :container :object)
  (:export
   #:init-symbols
   #:*print-arity*
   #:*show-ac*
   #:arity
   #:symbol-member
   #:symbol-remove
   #:symbol-adjoin
   #:symbol-delete
   #:symbols-of
   #:symbols-from
   #:constant-symbol
   #:decorated-symbol
   #:vbits-symbol
   #:vbits-constant-symbol
   #:vbits-non-constant-symbol
   #:color-symbol
   #:color-symbol-p
   #:color-constant-symbol
   #:color-non-constant-symbol
   #:named-constant-symbol
   #:make-vbits-symbol
   #:make-color-symbol
   #:make-random-partition-vbits-symbol
   #:make-random-vbits-symbol
   #:make-random-color-symbol
   #:make-zero-vbits-symbol
   #:color-symbol-constant-p
   #:abstract-symbol
   #:abstract-constant-symbol
   #:abstract-non-constant-symbol
   #:arity-symbol
   #:parity-symbol
   #:abstract-parity-symbol
   #:commutative-symbol
   #:commutative-mixin
   #:symbol-commutative-p
   #:symbol-ac-p
   #:ac-symbol
   #:ac-mixin
   #:commutative-symbol
   #:ac-to-commutative-symbol
   #:ac-to-commutative-signature
   #:make-commutative-symbol
   #:find-symbol-from-name
   #:make-constant-symbol
   #:make-arity-symbol
   #:make-parity-symbol
   #:make-ac-symbol
   #:annotate-symbol
   #:symbol-constant-p
   #:symbol-subsetp
   #:symbol-intersection
   #:symbol-union
   #:symbol-remove-duplicates
   #:symbol-delete-duplicates
   #:symbol-set-difference
   #:reset-aux-symbols
   #:make-aux-symbol
   #:abstract-signature
   #:signature-finite-p
   #:signature
   #:fly-signature
   #:signature-nb-colors
   #:signature-nb-set-variables
   #:signature-colors-p
   #:signature-set-variables-p
   #:get-arities
   #:max-arity
   #:arrange-signature
   #:constant-signature
   #:non-constant-signature
   #:merge-signature
   #:copy-signature
   #:nb-symbols
   #:included-signature-p
   #:equiv-signature-p
   #:adjoin-symbol-to-signature
   #:adjoin-symbols-to-signature
   #:remove-symbol-from-signature
   #:remove-symbols-from-signature
   #:add-bullet-symbol
   #:add-empty-symbols
   #:add-extra-symbol
   #:remove-bullet-symbol
   #:remove-extra-symbol
   #:signature-subset-p
   #:signature-intersection
   #:signature-union
   #:signature-difference
   #:signature-empty-p
   #:signature-symbols
   #:symbols-of-arity
   #:make-signature
   #:make-fly-signature
   #:in-signature
   #:in-signature-extra
   #:in-signature-bullet
   #:*bullet-symbol*
   #:*extra-symbol*
   #:*omega-symbol*
   #:*empty-symbol*
   #:empty-symbol
   #:*predefined-symbols*
   #:signed-object
   #:signature-mixin
   #:signature
   #:*signature-mapping*
   #:*inverse-signature-mapping*
   #:apply-signature-mapping
   #:make-signature-mapping
   #:signature-mapping
   #:all-symbols
   #:all-constant-symbols
   #:aux-symbols
   #:annotated-symbol
   #:annotated-symbol-p
   #:sym
   #:inverse-finite-mapping
   #:x1-to-xj
   #:x1-to-bool-x1
   #:x1-to-bool-xi
   #:xj-to-cxj
   #:xi-eq-cxj
   #:x1-to-cx1
   #:nothing-to-random
   #:nothing-to-random-color
   #:nothing-to-random-partition
   #:nothing-to-x1
   #:nothing-to-xj
   #:nothing-to-color
   #:nothing-to-colors-constants
   #:nothing-to-colors
   #:nothing-to-xj1-union-xj2
   #:nothing-to-xj1-intersection-xj2
   #:nothing-to-c1-union-c2
   #:x1-to-xj1-union-xj2
   #:x1-to-xj1-intersection-xj2
   #:y1-y2-to-xj1-xj2
   #:y1-y2-to-xj-xj1-union-xj2
   #:y1-y2-to-xj-xj1-intersection-xj2
   #:hvmodification
   #:vmodification
   #:homomorphism
   #:vprojection
   #:constants-color-projection
   #:vbits-projection
   #:vbits-cylindrification
   #:constants-color-projection-ih
   #:*warn-new-symbol*
   #:*show-commutative*
   #:constant-mixin
   #:non-constant-mixin
   #:make-casted-symbol
   #:make-zero-vbits-symbol
   #:symbol-of
   #:symbols
   #:symbol-equal
   #:decoration-of
   #:decoration-projection
   #:remove-annotation-if
   #:remove-annotation-if-not
   #:plist
   #:vbits-p
   #:constant-color-p
   #:signature-nb-set-variables
   #:signature-color-nb-colors
   #:signature-proved-incompatible))
