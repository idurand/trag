;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric plist (plist-mixin))

(defclass plist-mixin ()
  ((plist :initform nil :reader plist :initarg :plist)))

(defun copy-plist (plist)
  (copy-alist plist))

(defun plist-value-p (keyword plist)
  (assoc keyword plist))

(defun plist-value (keyword plist &optional default-value)
  (or (cdr (assoc keyword plist)) default-value))

(defun plist-set-value (keyword value plist)
  (let* ((found nil)
	 (new-plist
	   (loop for pair in plist
		 collect
		 (if (eq (car pair) keyword)
		     (progn 
		       (setq found t)
		       (cons keyword value))
		     pair))))
    (if found
	new-plist
	(cons (cons keyword value) new-plist))))

(defun plist-cons (keyword value plist)
  (acons keyword value plist))

(defun plist-equivalent-p (plist1 plist2)
  (and 
   (= (length plist1) (length plist2))
   (loop for p1 in plist1
	 do (let ((pname1 (car p1))
		  (pvalue1 (cdr p1)))
	      (let ((p2 (plist-value-p pname1 plist2)))
		(unless p2
		  (return-from plist-equivalent-p))
		(unless (eql (cdr p2) pvalue1)
		  (return-from plist-equivalent-p))))
	 finally (return t))))

(defun plist-incompatible-p (plist1 plist2)
  (loop for p1 in plist1
	do (let ((pname1 (car p1))
		 (pvalue1 (cdr p1)))
	     (let ((p2 (plist-value-p pname1 plist2)))
	       (unless p2
		 (return-from plist-incompatible-p t))
	       (unless (eql (cdr p2) pvalue1)
		 (return-from plist-incompatible-p t))))))
