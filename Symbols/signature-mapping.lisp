;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defvar *signature-mapping* nil) ;; contains h
(defvar *inverse-signature-mapping* nil) ;; contains ih
(defvar *signature-pair* nil) ;; info about signature (nb-set-variables or nb-colors

(defgeneric apply-signature-mapping (object)
  (:method ((l list))
    (mapcar (lambda (object) (apply-signature-mapping object)) l))
  (:method ((signature signature))
    (let ((plist (plist-set-value (car *signature-pair*) (cdr *signature-pair*) (plist signature))))
      (make-signature
       (map-mapping
	*signature-mapping*
	(signature-symbols signature))
       plist)))
  (:method ((signature fly-signature))
    (let ((plist (plist-set-value (car *signature-pair*) (cdr *signature-pair*) (plist signature))))
      (make-fly-signature
       (let ((inverse-signature-mapping *inverse-signature-mapping*))
	 (lambda (sym)
	   (let ((*signature-mapping* inverse-signature-mapping))
	     (every (lambda (s)
		      (in-signature s signature))
		    (apply-signature-mapping sym)))))
       plist)))
  (:method ((s abstract-symbol))
    (apply-mapping *signature-mapping* s)))

(defgeneric inverse-finite-mapping (mapping signature))
(defmethod inverse-finite-mapping ((mapping mapping) (signature signature))
  (let* ((injective nil)
	 (ipairs nil))
    (loop
      for s in (signature-symbols signature)
      ;;       do (format *error-output* "ipairs ~A s ~A~%" ipairs s)
      do (loop
	   with target = (apply-mapping mapping s)
	   for ts in target
	   ;;	     do (format *error-output* "ts ~A ~%" ts)
	   do (let ((ipair (member ts ipairs :key #'car :test #'eq)))
		;;		  (format *error-output* "ipair ~A ~%" ipair)
		(if ipair
		    (push s (cdr ipair))
		    (push (cons ts (list s)) ipairs)))
	   unless (and (not injective) (endp (cdr target)))
	     do (setf injective nil)))
    ;;    (format *error-output* "ipairs ~A ~%" ipairs)
    (make-mapping 
     (lambda (sym)
       (cdr (assoc sym ipairs :test #'eq)))
     injective)))
