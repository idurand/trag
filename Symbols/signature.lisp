;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)
(defclass abstract-signature (signed-object plist-mixin) ())

(defmethod oriented-p ((o signed-object))
  (oriented-p (signature o)))

(defclass fly-signature (abstract-signature)
  ((signature-fun :initform nil :initarg :signature-fun :accessor signature-fun)))

(defmethod print-object :after ((s fly-signature) stream)
  (format stream "~A~%" (plist s)))

(defgeneric signature-symbols (signature)
  (:documentation "list of the symbols of the SIGNATURE"))

(defclass signature (abstract-signature)
  ((symbols :initform nil :initarg :symbols :accessor signature-symbols)))

(defmethod signature-finite-p ((s fly-signature)) nil)

(defun make-fly-signature (signature-fun &optional plist)
  (make-instance 'fly-signature :signature-fun signature-fun :plist plist))

(defgeneric in-signature (sym signature)
  (:method ((sym abstract-symbol) (signature signature))
    (symbol-member sym (signature-symbols signature)))
  (:method ((sym abstract-symbol) (signature fly-signature))
    (funcall (signature-fun signature) sym)))

(defmethod constant-signature ((signature fly-signature))
  (make-fly-signature
   (lambda (sym)
     (and (symbol-constant-p sym) (in-signature sym signature)))
   (plist signature)))

(defgeneric in-signature-extra (signature)
  (:documentation "T if @ belongs to the signature")
  (:method ((signature abstract-signature))
    (in-signature *extra-symbol* signature)))

(defun in-signature-bullet (signature)
  (in-signature *bullet-symbol* signature))

(defgeneric signature-symbols (signature)
  (:documentation "list of the symbols of the SIGNATURE"))

(defmethod signature-finite-p ((s signature)) t)

(defmethod oriented-p ((signature abstract-signature))
  (cdr (assoc :oriented (plist signature))))

(defmethod print-object ((s signature) stream)
  (let ((*print-arity* t))
    (display-sequence (signature-symbols s) stream))
  s)

(defun homogeneous-constant-symbols-p (symbols)
  (let ((constants (constant-symbols symbols)))
    (or (endp (cdr constants))
	(let ((c (car constants)))
	  (if (vbits-mixin-p c)
	      (let ((len (length (vbits c))))
		(every
		 (lambda (ct)
		   (and (vbits-mixin-p ct)
			(= (length (vbits ct)) len)))
		 (cdr constants)))
	      (notany #'vbits-mixin-p (cdr constants)))))))

(defun make-signature (symbols &optional plist)
  (setf symbols (remove-duplicates symbols :test #'eq))
  (assert (homogeneous-constant-symbols-p symbols))
  (unless plist
    (let ((constants (constant-symbols symbols)))	   
      (setq plist (list (cons :nb-colors (constant-symbols-nb-colors constants))
			(cons :nb-set-variables (constant-symbols-nb-set-variables constants))))))
  (make-instance 'signature :symbols symbols :plist plist))

(defmethod signature ((o signed-object))
  (make-signature (symbols-from o)))

(defgeneric get-arities (signature)
  (:documentation "list of the arities present in the signature without duplicates")
  (:method ((signature signature))
    (delete-duplicates (mapcar #'arity (signature-symbols signature)) :test #'=)))

(defgeneric max-arity (signature)
  (:documentation "maximal arity of the symbols in the signature (0 if signature empty)")
  (:method ((signature signature))
    (let ((arities (get-arities signature)))
      (if (endp arities)
	  0
	  (apply #'max arities)))))

(defgeneric arrange-signature (signature)
  (:method ((signature signature))
    (let ((a (make-array (1+ (max-arity signature)) :initial-element nil)))
      (loop
	for s in (signature-symbols signature)
	do (push s (aref a (arity s))))
      a)))

(defmethod constant-signature ((signature signature))
  (make-signature 
   (remove-if-not #'symbol-constant-p (signature-symbols signature))
   (plist signature)))

(defgeneric non-constant-signature (signature)
  (:documentation "return a signature containing the non constant symbols of SIGNATURE")
  (:method ((signature signature))
    (make-signature 
     (remove-if #'symbol-constant-p (signature-symbols signature))
     (plist signature))))

(defgeneric merge-signature (&rest signatures)
  (:documentation "return a signature merging all the SIGNATURES assuming the arities are coherent")
  (:method (&rest signatures)
    ;;  (assert signatures)
    (let ((plist (and signatures (plist (first signatures)))))
      (if (every (lambda (signature) (typep signature 'signature)) signatures)
	  (make-signature
	   (symbol-remove-duplicates
	    (apply #'append (mapcar #'signature-symbols signatures)))
	   plist)
	  (make-fly-signature
	   (lambda (sym)
	     (some (lambda (signature) (in-signature sym signature)) signatures))
	   plist)))))

(defgeneric copy-signature (signature) ; not destructive
  (:documentation "return a signature containing the same symbols as SIGNATURE")
  (:method ((signature signature)) ; not destructive
    (make-signature (copy-list (signature-symbols signature))
		    (copy-plist (plist signature)))))

(defgeneric nb-symbols (signature)
  (:documentation "return the number of symbols in the signature SIGNATURE")
  (:method ((signature signature))
    (length (signature-symbols signature))))

(defgeneric symbols-of-arity (signature arity)
  (:documentation "list of symbols of SIGNATURE with this ARITY")
  (:method ((signature signature) arity)
    (remove-if-not (lambda (sym) (= (arity sym) arity))
		   (signature-symbols signature))))

(defgeneric signature-empty-p (signature)
  (:documentation "check whether a signature is empty")
  (:method ((s signature))
    (endp (signature-symbols s))))

(defgeneric signature-subset-p (signature1 signature2)
  (:method ((s1 signature) (s2 abstract-signature))
    (every (lambda (sym) (in-signature sym s2))
	   (signature-symbols s1))))

(defmethod signature-fun ((signature signature))
  (lambda (sym) (in-signature sym signature)))

(defgeneric included-signature-p (signature1 signature2)
  (:documentation "true if SIGNATURE1 is included in SIGNATURE2")
  (:method ((s1 signature) (s2 signature))
    (every (lambda (s)
	     (symbol-member s (signature-symbols s2)))
	   (signature-symbols s1))))

(defgeneric equiv-signature-p (signature1 signature2)
  (:documentation "true if SIGNATURE1 contains the same symbols that SIGNATURE2")
  (:method ((s1 signature) (s2 signature))
    (and
     (= (nb-symbols s1) (nb-symbols s2))
     (included-signature-p s1 s2)
     (included-signature-p s2 s1))))

(defgeneric adjoin-symbol-to-signature (symbol signature)
  (:documentation "return a new signature containing SYMBOL in addition to the symbols of SIGNATURE")
  (:method ((sym abstract-symbol) (signature signature))
    (make-signature 
     (symbol-adjoin sym (signature-symbols signature))
     (plist signature)))
  (:method((sym abstract-symbol) (signature fly-signature))
    (make-fly-signature
     (lambda (symbol)
       (or (symbol-equal symbol sym) (in-signature symbol signature)))
     (plist signature))))

(defgeneric adjoin-symbols-to-signature (symbols signature)
  (:method ((symbols list) (signature abstract-signature))
    (loop
      for symbol in symbols
      do (setq signature (adjoin-symbol-to-signature symbol signature))
      finally (return signature))))

(defgeneric remove-symbol-from-signature  (symbol signature)
  (:documentation "return a new signature the symbols of SIGNATURE except SYMBOL")
  (:method ((sym abstract-symbol) (signature signature))
    (make-signature
     (symbol-remove sym (signature-symbols signature))
     (plist signature)))
  (:method ((sym abstract-symbol) (signature fly-signature))
    (make-fly-signature
     (lambda (symbol)
       (and (not (symbol-equal symbol sym)) (in-signature symbol signature)))
     (plist signature))))

(defgeneric remove-symbols-from-signature (symbols signature)
  (:documentation
   "return a new signature containing the symbols of SIGNATURE \
    except the ones in the list SYMBOLS")
  (:method ((syms list) (signature signature))
    (make-signature
     (symbol-set-difference (signature-symbols signature) syms)
     (plist signature))))

(defmethod remove-symbols-from-signature
    ((syms list) (signature fly-signature))
  (make-fly-signature
   (lambda (symbol)
     (and (notany (lambda (sym) (in-signature sym signature)) syms) 
	  (in-signature symbol signature)))
   (plist signature)))

(defgeneric add-bullet-symbol (signature)
  (:documentation "return a new signature containing the bullet-symbol in addition to the symbols of SIGNATURE")
  (:method ((s abstract-signature))
    (adjoin-symbol-to-signature *bullet-symbol* s)))

(defgeneric add-extra-symbol (signature)
  (:documentation "return a new signature containing the extra-symbol in addition to the symbols of SIGNATURE")
  (:method((s abstract-signature))
    (adjoin-symbol-to-signature *extra-symbol* s)))

(defgeneric remove-bullet-symbol (signature)
  (:documentation
   "return a new signature containing the symbols of SIGNATURE \
    except the bullet-symbol)")
  (:method ((s abstract-signature))
    (remove-symbol-from-signature *bullet-symbol* s)))

(defgeneric remove-extra-symbol (signature)
  (:documentation
   "return a new signature containing the symbols of SIGNATURE \
   except the extra-symbol)")
  (:method ((s abstract-signature))
    (remove-symbol-from-signature *extra-symbol* s)))

(defgeneric signature-intersection (signature1 signature2)
  (:documentation
   "return a new signature with containing the intersection of \
    the symbols of SIGNATURE1 and SIGNATURE2")
  (:method ((s1 signature) (s2 signature))
    (make-signature
     (symbol-intersection (signature-symbols s1) (signature-symbols s2))
     (plist s1)))
  (:method ((s1 signature) (s2 abstract-signature))
    (make-signature
     (loop for s in (signature-symbols s1)
	   when (in-signature s s2)
	     collect s)
     (plist s1)))
  (:method ((s1 abstract-signature) (s2 signature))
    (signature-intersection s2 s1))
  (:method ((s1 abstract-signature) (s2 abstract-signature))
    (make-fly-signature
     (lambda (sym)
       (and (in-signature sym s1) (in-signature sym s2)))
     (plist s1))))

(defgeneric signature-union (signature1 signature2)
  (:documentation
   "return a new signature with containing the union of \
    the symbols of SIGNATURE1 and SIGNATURE2")
  (:method ((s1 abstract-signature) (s2 abstract-signature))
    (make-fly-signature
     (lambda (sym)
       (or (in-signature sym s1) (in-signature sym s2)))
     (plist s1)))
  (:method ((s1 signature) (s2 signature))
    (make-signature
     (symbol-union (signature-symbols s1) (signature-symbols s2))
     (plist s1))))

(defgeneric signature-difference (signature1 signature2)
  (:documentation "return a new signature with containing the set-difference of the symbols of SIGNATURE1 and SIGNATURE2")
  (:method ((s1 abstract-signature) (s2 abstract-signature))
    (make-fly-signature
     (lambda (sym)
       (and (in-signature sym s1) (not (in-signature sym s2))))
     (plist s1)))
  (:method ((s1 signature) (s2 signature))
    (make-signature
     (symbol-set-difference (signature-symbols s1) (signature-symbols s2))
     (plist s1))))

(defgeneric signature-proved-incompatible (signature1 signature2)
  (:method ((signature1 abstract-signature) (signature2 abstract-signature))
    (not (plist-equivalent-p (plist signature1) (plist signature2)))))

(defgeneric ac-to-commutative-signature (signature)
  (:method ((signature signature))
    (make-signature
     (mapcar (lambda (symbol)
	       (if (symbol-ac-p symbol)
		   (ac-to-commutative-symbol symbol)
		   symbol))
	     (signature-symbols signature))
     (plist signature))))

(defgeneric signature-nb-colors (signature)
  (:method ((signature abstract-signature))
    (plist-value :nb-colors (plist signature) 0)))

(defgeneric signature-nb-set-variables (signature)
  (:method ((signature abstract-signature))
    (plist-value :nb-set-variables (plist signature) 0)))

(defmethod vbits-p ((signature abstract-signature))
  (< 0 (signature-nb-set-variables signature)))

(defmethod constant-color-p ((signature abstract-signature))
  (< 0 (signature-nb-colors signature)))
