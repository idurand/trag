;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric signature (signed-object)
  (:documentation "signature of a signed object")
  (:method ((s t))
    (format *error-output* "signature on ~A~%" (type-of s)))
  (:method ((l list))
    (apply #'merge-signature (mapcar #'signature l))))

(defgeneric constant-signature (signed-object)
  (:documentation "signature of a signed object"))

(defclass signed-object () ()
  (:documentation "objects on which signature may be called"))

(defgeneric vbits-p (signed-object)
  (:documentation "true if the signature has bits on its constants")
  (:method ((o t)) (warn "vbits-p on ~A (~A)~%" o (type-of o)))
  (:method ((signed-object signed-object)) nil))

(defgeneric constants-color-p (signed-object)
  (:documentation "true if the signature has a color on its constants")
  (:method ((o t)) (warn "constants-color-p on ~A (~A)~%" o (type-of o)))
  (:method ((signed-object signed-object)) (signature signed-object)))

(defclass signature-mixin (signed-object)
  ((signature
    :initform nil
    :initarg :signature
    :accessor signature)))

(defmethod constant-signature ((signed-object signed-object))
  (constant-signature (signature signed-object)))

(defgeneric signature-finite-p (signed-object)
  (:method ((signature-mixin signature-mixin))
    (signature-finite-p (signature signature-mixin))))
