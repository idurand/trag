;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Symbols.
(in-package :asdf-user)

(defsystem :symbols
  :description "Symbols and Symbols"
  :name "symbols"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:color :vbits :names :input-output)
  :serial t
  :components
  ((:file "package")
   (:file "arity")
   (:file "signed-object")
   (:file "abstract-symbol")
   (:file "decorated-symbol")
   (:file "symbols")
   (:file "aux-symbol")
   (:file "init-symbols")
   (:file "plist")
   (:file "signature")
   (:file "signature-mapping")
   (:file "annotated-symbols")
   (:file "vbits-homomorphisms")
   (:file "vbits-ihomomorphisms")
   (:file "color-homomorphisms")
   (:file "color-ihomomorphisms")
   (:file "homomorphisms")
   (:file "homomorphism")
   (:file "cprojection")
   (:file "hvmodification")
   (:file "input")))

(pushnew :symbols *features*)
