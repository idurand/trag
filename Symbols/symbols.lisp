;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defclass vbits-symbol (vbits-mixin decorated-symbol) ())

(defmethod vbits-p ((symbol vbits-symbol)) t)

(defclass vbits-constant-symbol (constant-mixin vbits-symbol) ())

(defclass vbits-non-constant-symbol (non-constant-mixin vbits-symbol) ())

(defmethod decoration-of ((s vbits-symbol))
  (vbits s))

(defmethod print-object ((s vbits-symbol) stream)
  (format stream "~A^~A" (symbol-of s) (vbits-to-string (vbits s))))

(defmethod name ((s vbits-symbol))
  (format nil "~A^~A" (name (symbol-of s)) (vbits-to-string  (vbits s))))

(defun make-casted-symbol (class &rest initargs)
  (cast-symbol 
   (apply 'make-instance class initargs)))

(defgeneric make-vbits-constant-symbol (symbol vbits)
  (:method ((s abstract-symbol) (vbits bit-vector))
    (make-casted-symbol 'vbits-constant-symbol :symbol s :vbits vbits)))

(defgeneric make-vbits-non-constant-symbol (symbol vbits)
  (:method ((s abstract-symbol) (vbits bit-vector))
    (make-casted-symbol 'vbits-constant-symbol :symbol s :vbits vbits)))

(defgeneric make-vbits-symbol (symbol vbits)
  (:method ((s abstract-symbol) (vbits bit-vector))
    (cond
      ((zerop (length vbits)) s)
      ((symbol-constant-p s) (make-vbits-constant-symbol s vbits))
      (t (make-vbits-non-constant-symbol s vbits))))
  (:method ((s abstract-symbol) (vbits list))
    (if (endp vbits)
	s
	(make-vbits-symbol s (make-vbits vbits)))))

(defclass color-symbol (color-mixin decorated-symbol) ())

(defclass color-constant-symbol (constant-mixin color-symbol) ())

(defmethod constant-color-p ((symbol color-constant-symbol)) t)

(defclass color-non-constant-symbol (non-constant-mixin color-symbol) ())

(defmethod decoration-of ((s color-symbol))
  (color s))

(defmethod print-object ((s color-symbol) stream)
  (format stream "~A~~~A" (symbol-of s) (color s)))

(defmethod name ((s color-symbol))
  (format nil "~A~A~A" (name (symbol-of s)) #\~(color s)))

(defgeneric make-color-constant-symbol (symbol color)
  (:method ((s constant-mixin) (color integer))
    (make-casted-symbol 'color-constant-symbol :symbol s :color color)))

(defgeneric make-color-non-constant-symbol (symbol color)
  (:method ((s non-constant-mixin) (color integer))
    (make-casted-symbol 'color-non-constant-symbol :symbol s :color color)))

(defgeneric make-color-symbol (symbol color)
  (:method ((s abstract-symbol) (color integer))
    (if (symbol-constant-p s)
	(make-color-constant-symbol s color)
	(make-color-non-constant-symbol s color))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some concrete classes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass constant-symbol (constant-mixin abstract-named-symbol) ())

(defun make-constant-symbol (name)
  (make-casted-symbol 'constant-symbol :name name))

(defvar *empty-symbol* (make-constant-symbol "@"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass parity-symbol (non-constant-mixin abstract-named-symbol)
  ((arity :initarg :arity :reader arity)))

(defmethod compare-object ((s1 parity-symbol) (s2 parity-symbol))
  (and (= (arity s1) (arity s2))
       (call-next-method)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass ac-mixin (commutative-mixin non-constant-mixin) ())
(defclass ac-symbol (ac-mixin abstract-named-symbol) ())

(defclass commutative-symbol (commutative-mixin non-constant-mixin abstract-named-symbol) ())

(defmethod arity ((s ac-symbol)) 2)
(defmethod arity ((s commutative-symbol)) 2)

(defmethod print-object :after ((s ac-symbol) stream)
  (when *show-ac*
    (format stream "%")))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod symbol-constant-p ((s abstract-symbol)) nil)
(defmethod symbol-constant-p ((s constant-mixin)) t)

(defmacro symbol-equal (symbol1 symbol2)
  `(eq ,symbol1 ,symbol2))

(defmethod compare-object ((s1 vbits-symbol) (s2 vbits-symbol))
  (and 
   (symbol-equal (symbol-of s1) (symbol-of s2))
   (equal (vbits s1) (vbits s2))))

(defmethod compare-object ((s1 color-symbol) (s2 color-symbol))
  (and
   (symbol-equal (symbol-of s1) (symbol-of s2))
   (= (color s1) (color s2))))

(defvar *predefined-constant-symbols* nil
  "predefined constant symbols omega bullet extra")

(defvar *print-arity* nil)

(defvar *extra-symbol* nil)
(defvar *bullet-symbol* nil)
(defvar *omega-symbol* nil)

(defgeneric symbols-from (o)
  (:documentation "list of the symbols appearing in o"))

(defmethod print-object :after ((s abstract-symbol) stream)
  (when (and *print-arity* (plusp  (arity s)))
    (format stream ":~A" (arity s))))

(defun all-symbols (&key (predefined nil))
  (let ((symbols
	  (mappend (lambda (class-symbol)
		     (symbols (make-instance class-symbol)))
		   '(constant-symbol parity-symbol commutative-symbol))))
    (unless predefined
      (setq symbols (nset-difference symbols *predefined-constant-symbols* :test #'eq)))
    symbols))

(defun all-constant-symbols (&key (predefined nil))
  (let ((symbols (all-symbols :predefined predefined)))
    (remove-if-not #'symbol-constant-p symbols)))

(defmethod symbol-commutative-p ((s abstract-symbol)) nil)
(defmethod symbol-commutative-p ((s ac-symbol)) t)
(defmethod symbol-commutative-p ((s commutative-symbol)) t)

(defgeneric symbol-ac-p (symbol)
  (:method ((s abstract-symbol)) nil)
  (:method ((s ac-symbol)) t))

(defun find-symbol-from-name (name)
  (find-if
   (lambda (s)
     (equal (name s) name))
   (all-symbols :predefined t)))

(defgeneric make-random-vbits-symbol (symbol m &key exclusive)
  (:method ((symbol abstract-symbol) m &key (exclusive nil))
    (make-vbits-symbol
     symbol
     (if exclusive
	 (random-partition-vbits m)
	 (random-vbits m)))))

(defgeneric make-random-partition-vbits-symbol (symbol m &key exclusive)
  (:method ((symbol abstract-symbol) (m integer) &key (exclusive nil))
    (make-random-vbits-symbol symbol m :exclusive exclusive)))

(defgeneric make-zero-vbits-symbol (symbol m)
  (:method ((symbol abstract-symbol) (m integer))
    (make-vbits-symbol symbol (zero-vbits m))))

(defgeneric make-random-color-symbol (symbol k)
  (:method ((symbol abstract-symbol) k)
    (make-color-symbol symbol (random-color k))))

(defun make-parity-symbol (name arity)
  (make-casted-symbol 'parity-symbol :name name :arity arity))

(defun make-ac-symbol (name)
  (make-casted-symbol 'ac-symbol :name name))

(defun make-commutative-symbol (name)
  (make-casted-symbol 'commutative-symbol :name name))

(defun make-arity-symbol (name arity)
  "non commutative symbol with arity"
  (if (zerop arity)
      (make-constant-symbol name)
      (make-parity-symbol name arity)))

(defgeneric symbol-member (symbol l)
  (:documentation "true if SYMBOL is in the list of symbols L")
  (:method ((s abstract-symbol) (l list))
    (member s l :test #'eq)))

(defgeneric symbol-remove (symbol l)
  (:documentation "return a list of the symbols in L except SYMBOL")
  (:method ((s abstract-symbol) l)
    (remove s l :test #'eq)))

(defun symbol-subsetp (l1 l2)
  (subsetp l1 l2 :test #'eq))

(defgeneric symbol-adjoin (symbol l)
  (:documentation "return a list containing SYMBOL and the symbols of L")
  (:method ((s abstract-symbol) l)
    (adjoin s l :test #'eq)))

(defgeneric symbol-delete (symbol l)
  (:documentation
   "return the list of symbols L where SYMBOL has been deleted (destructive!)")
  (:method ((s abstract-symbol) l)
    (delete s l :test #'eq)))

(defun symbol-intersection (l1 l2)
  (intersection l1 l2 :test #'eq))

(defun symbol-union (l1 l2)
  (union l1 l2 :test #'eq))

(defun symbol-remove-duplicates (l)
  (remove-duplicates l :test #'eq))

(defun symbol-delete-duplicates (l)
  (delete-duplicates l :test #'eq))

(defun symbol-set-difference (l1 l2)
  (set-difference l1 l2 :test #'eq))

(defmethod symbols-from ((s abstract-symbol)) (list s))

(defmethod symbols-from ((l list))
  (let ((symbols '()))
    (loop
      for o in l
      do (loop for s in (symbols-from o)
	       do (pushnew s symbols :test #'eq)))
    symbols))

(defgeneric ac-to-commutative-symbol (symbol)
  (:method ((symbol ac-symbol))
    (make-commutative-symbol (name symbol))))

(defun constant-symbols-nb-set-variables (constants)
  (if (endp constants)
      0
      (let ((constant (car constants)))
	(if (typep constant 'vbits-constant-symbol)
	    (length (vbits constant))
	    0))))

(defun symbols-nb-set-variables (symbols)
  (constant-symbols-nb-set-variables (constant-symbols symbols)))

(defun constant-symbols-nb-colors (constants)
  (if (or (endp constants) (not (color-symbol-p (first constants))))
      0
      (reduce #'max constants :key #'color :initial-value 0)))

(defun symbols-nb-colors (symbols)
  (constant-symbols-nb-colors (constant-symbols symbols)))

(defgeneric color-symbol-p (symbol)
  (:documentation "is symbol a colored symbol")
  (:method ((s color-symbol)) t)
  (:method ((a abstract-symbol)) nil))

(defgeneric vbits-symbol-p (symbol)
  (:documentation "is symbol a vbits symbol")
  (:method ((s vbits-symbol)) t)
  (:method ((a abstract-symbol)) nil))

(defgeneric constant-symbols (symbols)
  (:method ((symbols list))
    (remove-if-not #'symbol-constant-p symbols)))
