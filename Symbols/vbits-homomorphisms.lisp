;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

(defgeneric nothing-to-random-partition-f (symbol m)
  (:method ((s (eql *empty-symbol*)) m)
    (list (make-zero-vbits-symbol s m)))
  (:method ((s constant-mixin) m)
    (list (make-random-partition-vbits-symbol s m)))
  (:method ((s abstract-symbol) m) (list s)))

(defgeneric nothing-to-random-partition-h (m)
  (:method ((m integer))
    (lambda (s) (nothing-to-random-partition-f s m))))

(defgeneric nothing-to-xj-f (symbol m j signature)
  (:method ((s (eql *empty-symbol*)) m j (csign abstract-signature))
    (let ((lvbits (remove-if
		   (lambda (vbits)
		     (onep (aref vbits (1- j))))
		   (mapcar #'make-vbits (lvbits m))))
	  (csyms (signature-symbols csign)))
      (mapcan
       (lambda (cs)
	 (mapcar
	  (lambda (vbits)
	    (make-vbits-symbol cs vbits))
	  lvbits))
       csyms)))
  (:method ((s constant-mixin) m j (csign abstract-signature))
    (let ((lvbits (remove-if
		   (lambda (vbits)
		     (zerop (aref vbits (1- j))))
		   (mapcar #'make-vbits (lvbits m)))))
      (mapcar (lambda (vbits)
		(make-vbits-symbol s vbits))
	      lvbits)))
  (:method ((s abstract-symbol) m j (csign abstract-signature))
    (list s)))

(defgeneric nothing-to-xj-h (m j signature)
  (:method (m j (csign abstract-signature))
    (lambda (s)
      (nothing-to-xj-f s m j csign))))

(defgeneric nothing-to-xj1-bool-xj2-f (symbol m j1 j2 signature bool)
  (:method ((s (eql *empty-symbol*)) (m integer) (j1 integer) (j2 integer)
	    (signature abstract-signature) bool)
    (let ((lvbits
	    (remove-if
	     (lambda (vbits)
	       (let ((b1 (aref vbits (1- j1)))
		     (b2 (aref vbits (1- j2))))
		 (case bool
		   (union (union-bin-p b1 b2))
		   (intersection (intersection-bin-p b1 b2)))))
	     (mapcar #'make-vbits (lvbits m))))
	  (csyms (signature-symbols signature)))
      (mapcan (lambda (cs)
		(mapcar (lambda (vbits) 
			  (make-vbits-symbol cs vbits))
			lvbits))
	      csyms)))
  (:method ((s constant-mixin) m j1 j2 (signature signature) bool)
    (let ((lvbits
	    (remove-if-not
	     (lambda (vbits)
	       (let ((b1 (aref vbits (1- j1)))
		     (b2 (aref vbits (1- j2))))
		 (case bool
		   (union (union-bin-p b1 b2))
		   (intersection (intersection-bin-p b1 b2)))))
	     (mapcar #'make-vbits (lvbits m)))))
      (mapcar (lambda (vbits)
		(make-vbits-symbol s vbits))
	      lvbits)))
  (:method ((s abstract-symbol) m j1 j2 (signature abstract-signature) bool)
    (list s)))

(defgeneric nothing-to-xj1-bool-xj2-h (m j1 j2 signature bool)
  (:method ((m integer) (j1 integer) (j2 integer) (signature abstract-signature) bool)
    (lambda (s)
      (nothing-to-xj1-bool-xj2-f s m j1 j2 signature bool))))

(defgeneric nothing-to-vbits-constants-f (symbol signature m)
  (:method ((s abstract-symbol) (signature abstract-signature) m)
    (mapcar (lambda (vbits)
	      (make-vbits-symbol s vbits))
	    (mapcar #'make-vbits (lvbits m)))))

(defgeneric nothing-to-vbits-constants-h (signature m)
  (:method ((signature abstract-signature) m)
    (lambda (s)
      (nothing-to-vbits-constants-f s signature m))))
