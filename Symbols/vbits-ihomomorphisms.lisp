;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :symbols)

;;; the homomorphisms and inverse-homomorphisms operate on symbols

(defgeneric nothing-to-vbits-constants-if (symbol)
  (:method ((s color-symbol))
    (list (symbol-of s)))
  (:method  ((s abstract-symbol))
    (list s)))

(defun nothing-to-vbits-constants-ih ()
  (lambda (s) (nothing-to-vbits-constants-if s)))

(defgeneric nothing-to-xj-if (symbol m j)
  (:method ((s vbits-constant-symbol) (m integer) (j integer))
    (let* ((vbits (vbits s))
	   (b (aref vbits (1- j))))
      (assert (= m (length vbits)))
      (list
       (if (zerop b)
	   *empty-symbol*
	   (symbol-of s)))))
  (:method ((s abstract-symbol) m j)
    (list s)))

(defgeneric nothing-to-xj-ih (m j)
  (:method ((m integer) (j integer))
    (lambda (s)
      (nothing-to-xj-if s m j))))

(defgeneric nothing-to-xj1-bool-xj2-if (symbol m j1 j2 bool)
  (:method ((s vbits-constant-symbol) (m integer) (j1 integer) (j2 integer) bool)
    (let* ((vbits (vbits s))
	   (b1 (aref vbits (1- j1)))
	   (b2 (aref vbits (1- j2))))
      (assert (= m (length vbits)))
      (list
       (if (case bool
	     (union (union-bin-p b1 b2))
	     (intersection (intersection-bin-p b1 b2)))
	   (symbol-of s)
	   *empty-symbol*))))
  (:method ((s abstract-symbol) (m integer) (j1 integer) (j2 integer) bool)
    (list s)))

(defgeneric nothing-to-xj1-bool-xj2-ih (m j1 j2 bool)
  (:method ((m integer) (j1 integer) (j2 integer) bool)
    (lambda (s)
      (nothing-to-xj1-bool-xj2-if s m j1 j2 bool))))
