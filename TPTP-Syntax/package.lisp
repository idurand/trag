(defpackage :tptp-syntax
  (:use :common-lisp :input-output :object :terms)
  (:export 
    #:tptp-fof-string-to-formula-object

    #:logic-formula
    #:logic-term
    #:logic-constant
    #:logic-variable
    #:logic-function-call
    #:logic-predicate-call
    #:logic-comparison
    #:logic-operation
    #:logic-quantification

    #:subformula
    ; reexport
    #:name
    #:arg

    #:*logic-formula-print-unicode*

    #:logic-formula-free-variables
    #:logic-formula-walk
    ))
