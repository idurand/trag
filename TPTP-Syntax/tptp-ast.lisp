(in-package :tptp-syntax)

(defun extract-ast-source (tree)
  (let* ((flat (alexandria:flatten tree))
         (strings (remove-if-not 'stringp flat))
         (result (reduce (lambda (x y) (concatenate 'string x y)) strings)))
    result))

(defun tptp-passthrough-rule-p (tree)
  (let* ((args (second tree)))
    (when (listp args)
      (or (symbolp (first args))
          (when (= (length args) 3)
            (let* ((lead-in (first args))
                   (lead-out (first (last args))))
              (and (or (null lead-in)
                       (eq (first lead-in) 'tptp-bnf-syntax::Sp))
                   (or (null lead-out)
                       (eq (first lead-out) 'tptp-bnf-syntax::Sp)))))))))

(defun recurse-on-tree (tree func)
  (cond ((not (listp tree)) (values tree nil))
        ((null tree) (values nil nil))
        ((symbolp (first tree))
         (let ((new (funcall func tree)))
           (values new (not (eq new tree)))))
        (t (loop with changedp := nil
                 with new := nil
                 for node in tree
                 do (multiple-value-bind (node-new node-changed-p)
                      (recurse-on-tree node func)
                      node-changed-p
                      (unless (eq node-new node) (setf changedp t))
                      (push node-new new))
                 finally
                 (return
                   (if changedp (values (reverse new) t)
                     (values tree nil)))))))

(defun alter-parse-tree (tree preprocessor-list postprocessor-list &key repeat)
  (if (listp tree)
    (let* ((preprocessor-matched-p nil)
           (preprocessed
             (loop for f in preprocessor-list
                   for input := tree then output
                   for output := (funcall f input)
                   while (listp output)
                   unless (eq input output) do (setf preprocessor-matched-p t)
                   finally (return (if preprocessor-list output tree))))
           (op (and (listp preprocessed) (first preprocessed)))
           (args (and (listp preprocessed) (second preprocessed)))
           (new-args
             (recurse-on-tree 
               args
               (lambda (node)
                 (alter-parse-tree node preprocessor-list postprocessor-list))))
           (recursion-matched-p (not (eq new-args args)))
           (recursed (if recursion-matched-p (list op new-args) preprocessed))
           (postprocessor-matched-p nil)
           (postprocessed
             (loop for f in postprocessor-list
                   for input := recursed then output
                   for output := (if (listp input) (funcall f input) input)
                   while (listp output)
                   unless (eq input output) do (setf postprocessor-matched-p t)
                   finally (return (if postprocessor-list output recursed))))
           (changed-p (or preprocessor-matched-p
                          recursion-matched-p
                          postprocessor-matched-p)))
      (if (and repeat changed-p)
        (values (alter-parse-tree
                  postprocessed preprocessor-list postprocessor-list
                  :repeat repeat)
                t)
        (values postprocessed changed-p)))
    (values tree nil)))

(defun whitespace-node-killer (tree)
  (case (first tree)
    ((tptp-bnf-syntax::Sp tptp-bnf-syntax::comment) nil)
    (t tree)))

(defun tptp-coalesce-strings (tree)
  (case (first tree)
    ((tptp-bnf-syntax::single_quoted)
     (cl-ppcre:regex-replace-all
       "'(.*)'"
       (cl-ppcre:regex-replace-all
         "\\\\([\\\\'])" (extract-ast-source tree) "\\1")
       "\\1"))
    ((tptp-bnf-syntax::lower_word tptp-bnf-syntax::upper_word
      tptp-bnf-syntax::dollar_word tptp-bnf-syntax::dollar_dollar_word)
     (extract-ast-source tree))
    ((tptp-bnf-syntax::distinct_object)
     (list (first tree) (extract-ast-source tree)))
    ((tptp-bnf-syntax::number)
     (if (and (= 2 (length tree)) (stringp (second tree)))
       tree
       (list (first tree) (extract-ast-source tree))))
    (t tree)))

(defun tptp-merge-assoc (tree)
  (if (and (find (first tree)
                 '(tptp-bnf-syntax::fof_and_formula
                    tptp-bnf-syntax::fof_or_formula))
           (listp (second (second tree))))
    (list (first tree)
          (reduce 'append
                  (let ((subtree (second (second tree))))
                    (cons (list (first subtree)) (second subtree)))))
    tree))

(defun tptp-normalise-passthrough (tree)
  (let* ((args (second tree))
         (neededp
           (and (listp args)
                (= (length args) 3)
                (let* ((lead-in (first args))
                       (lead-out (first (last args))))
                  (and (or (null lead-in)
                           (eq (first lead-in) 'tptp-bnf-syntax::Sp))
                       (or (null lead-out)
                           (eq (first lead-out) 'tptp-bnf-syntax::Sp))))))
         (main-arg (when neededp (second args))))
    (if neededp (list (first tree) main-arg) tree)))

(defun tptp-clean-whitespace (tree)
  (let* ((args (second tree))
         (arglist (and (listp args)
                       (not (and (first args)
                                 (symbolp (first args))))))
         (changedp nil)
         (new-args (when arglist
                     (remove-if
                       (lambda (x)
                         (when (or (null x) 
                                   (and (listp x)
                                        (eq (first x) 'tptp-bnf-syntax::Sp)))
                           (setf changedp t)))
                       args))))
    (if changedp (list (first tree)
                       (if (= (length new-args) 1) (first new-args) new-args))
      tree)))

(defparameter *tptp-useful-leaf-types*
  '(tptp-bnf-syntax::upper_word
     tptp-bnf-syntax::dollar_word
     tptp-bnf-syntax::dollar_dollar_word
     tptp-bnf-syntax::number
     tptp-bnf-syntax::distinct_object
     tptp-bnf-syntax::atomic_word))

(defun collapse-passthrough (tree &key except)
  (if (listp tree)
    (let* ((args (second tree))
           (passthroughp (and (listp args) (first args) (symbolp (first args))))
           (exceptionalp (and passthroughp (find (first args) except))))
      (if (and passthroughp (not exceptionalp)) (list (first tree) (second args))
        tree))
    tree))

(defun tptp-collapse-passthrough (tree)
  (collapse-passthrough tree :except *tptp-useful-leaf-types*))

(defun flatten-ast-child-list (tree)
  (cond ((not (listp tree)) (list tree))
        ((and (first tree) (symbolp (first tree))) (list tree))
        (t (loop for x in tree append (flatten-ast-child-list x)))))

(defun flatten-ast-children (tree)
  (let* ((old-data (second tree))
         (new-list (flatten-ast-child-list old-data))
         (new-data (if (= (length new-list) 1) (first new-list) new-list)))
    (if (equal old-data new-data) tree (list (first tree) new-data))))

(defun tptp-normalise-tree (tree)
  (alter-parse-tree
    tree
    (list
      'tptp-coalesce-strings
      'flatten-ast-children
      'tptp-clean-whitespace
      )
    (list
      )
    :repeat t))

(defclass logic-formula (terms:abstract-term) ()
  (:documentation "A representation of a logic formula"))

(defclass logic-term (terms:abstract-term) ()
  (:documentation "A representation of a logic term"))

(defclass logic-constant (logic-term named-mixin) ())

(defclass logic-variable (logic-term named-mixin) ())

(defclass logic-function-call (logic-term named-mixin)
  ((arg :type list :accessor arg :initarg :arg)))

(defclass logic-predicate-call (logic-formula named-mixin)
  ((arg :type list :accessor arg :initarg :arg)))

(defclass logic-comparison (logic-formula named-mixin)
  ((arg :type list :accessor arg :initarg :arg)))

(defclass logic-operation (logic-formula named-mixin)
  ((arg :type list :accessor arg :initarg :arg)))

(defclass logic-quantification (logic-formula named-mixin)
  ((arg :type list :accessor arg :initarg :arg)
   (subformula :type logic-formula :initarg :subformula :accessor subformula)))

(defvar *logic-formula-print-unicode* nil)

(defun tptp-maybe-quote (name)
  (if (cl-ppcre:scan "^[$]{0,2}[a-z][A-Za-z0-9_]*$" name)
    name 
    (format
      nil "'~a'"
      (cl-ppcre:regex-replace-all
        "'"
        (cl-ppcre:regex-replace-all "\\\\" name "&&")
        "\\\\'"))))

(defmethod print-object ((object logic-constant) stream)
  (format stream "~a" (tptp-maybe-quote (name object))))

(defmethod print-object ((object logic-variable) stream)
  (assert (cl-ppcre:scan "[A-Z][A-Za-z0-9_]*" (name object)))
  (format stream "~a" (name object)))

(defmethod print-object ((object logic-function-call) stream)
  (format stream "~a(~{~a~#[~:;,~]~})"
          (tptp-maybe-quote (name object)) (arg object)))

(defmethod print-object ((object logic-predicate-call) stream)
  (format stream "~a(~{~a~#[~:;,~]~})"
          (tptp-maybe-quote (name object)) (arg object)))

(defmethod print-object ((object logic-comparison) stream)
  (format stream "~a ~a ~a"
          (first (arg object)) (name object) (second (arg object))))

(defmethod print-object ((object logic-operation) stream)
  (format stream "~a~{(~a)~#[~:; ~a ~]~}"
          (if (= (length (arg object)) 1)
            (name object) "")
          (loop for a in (arg object) for r on (arg object)
                collect a
                when (rest r) collect (name object))))

(defun quantifier-to-unicode (name)
  (cond ((equal name "!") "∀")
        ((equal name "?") "∃")
        (t (error "unknown quantifier"))))

(defun quantifier-maybe-to-unicode (name)
  (if *logic-formula-print-unicode* (quantifier-to-unicode name) name))

(defmethod print-object ((object logic-quantification) stream)
  (format stream "~a [~{~a~#[~:;,~]~}]: (~a)"
          (quantifier-maybe-to-unicode (name object))
          (arg object)
          (subformula object)))

(defvar *tptp-in-object-term* nil)

(defun tptp-to-formula-object (tree)
  (let* ((op (first tree))
         (args (second tree))
         (single-arg (and (listp args)
                          (symbolp (first args))
                          args)))
    (or (case op
          (tptp-bnf-syntax::variable
            (make-instance 'logic-variable :name (extract-ast-source args)))
          (tptp-bnf-syntax::constant
            (make-instance 'logic-constant
                           :name (extract-ast-source args)))
          ((tptp-bnf-syntax::fof_arguments tptp-bnf-syntax::fof_variable_list)
           (if single-arg (list (tptp-to-formula-object single-arg))
             (cons (tptp-to-formula-object (first args))
                   (tptp-to-formula-object (third args)))))
          ((tptp-bnf-syntax::fof_and_formula tptp-bnf-syntax::fof_or_formula)
           (make-instance
             'logic-operation
             :name (extract-ast-source (second args))
             :arg (loop for a in args for k upfrom 0 when (evenp k)
                        collect (tptp-to-formula-object a))))
          ((tptp-bnf-syntax::fof_defined_infix_formula tptp-bnf-syntax::fof_infix_unary)
           (make-instance
             'logic-comparison
             :name (extract-ast-source (second args))
             :arg (let ((*tptp-in-object-term* t))
                    (mapcar 'tptp-to-formula-object
                            (list (first args) (third args))))))
          (tptp-bnf-syntax::fof_binary_nonassoc
            (make-instance
              'logic-operation
              :name (extract-ast-source (second args))
              :arg (mapcar 'tptp-to-formula-object (list (first args) (third args)))))
          (tptp-bnf-syntax::fof_unary_formula
            (cond
              ((eq (first single-arg) 'tptp-bnf-syntax::fof_infix_unary)
               (tptp-to-formula-object single-arg))
              (t
                (make-instance
                  'logic-operation
                  :name (extract-ast-source (first args))
                  :arg (list (tptp-to-formula-object (second args)))))))
          (tptp-bnf-syntax::fof_plain_term
            (when (= (length args) 4)
              (make-instance
                (if *tptp-in-object-term* 
                  'logic-function-call 'logic-predicate-call)
                :name (extract-ast-source (first args))
                :arg (let ((*tptp-in-object-term* t))
                       (tptp-to-formula-object (third args))))))
          (tptp-bnf-syntax::fof_quantified_formula
            (make-instance
              'logic-quantification
              :name (extract-ast-source (first args))
              :arg (tptp-to-formula-object (third args))
              :subformula (tptp-to-formula-object (sixth args))))
          (tptp-bnf-syntax::number
            (read-from-string args))
          (t nil))
        (when (and (= (length args) 3)
                   (equal (first args) "(")
                   (equal (third args) ")"))
          (tptp-to-formula-object (second args)))
        (and single-arg (tptp-to-formula-object single-arg))
        (error "Cannot interpret the AST:~a" tree))))

(defun tptp-fof-string-to-formula-object (s)
  (tptp-to-formula-object
    (tptp-normalise-tree
      (esrap:parse 'tptp-bnf-syntax::fof_logic_formula s))))

(defgeneric logic-formula-walk (f 
                                 callback-pre &optional callback-post
                                 bound-variables))

(defmethod logic-formula-walk ((f logic-quantification) 
                               callback-pre &optional (callback-post (constantly nil))
                               bound-variables)
  (funcall callback-pre :formula f :bound-variables bound-variables :allow-other-keys t)
  (let* ((bound-variables (union bound-variables (mapcar 'name (arg f)) :test 'equal)))
    (logic-formula-walk (subformula f)
                        callback-pre callback-post bound-variables))
  (funcall callback-post :formula f :bound-variables bound-variables :allow-other-keys t))

(defmethod logic-formula-walk ((f logic-predicate-call) 
                               callback-pre &optional (callback-post (constantly nil))
                               bound-variables)
  (funcall callback-pre :formula f :bound-variables bound-variables :allow-other-keys t)
  (funcall callback-post :formula f :bound-variables bound-variables :allow-other-keys t))

(defmethod logic-formula-walk ((f logic-comparison) 
                               callback-pre &optional (callback-post (constantly nil))
                               bound-variables)
  (funcall callback-pre :formula f :bound-variables bound-variables :allow-other-keys t)
  (funcall callback-post :formula f :bound-variables bound-variables :allow-other-keys t))

(defmethod logic-formula-walk ((f logic-operation) 
                               callback-pre &optional (callback-post (constantly nil))
                               bound-variables)
  (funcall callback-pre :formula f :bound-variables bound-variables :allow-other-keys t)
  (loop for a in (arg f) do
        (logic-formula-walk a callback-pre callback-post bound-variables))
  (funcall callback-post :formula f :bound-variables bound-variables :allow-other-keys t))

(defgeneric logic-formula-free-variables (f &optional bound-names))

(defmethod logic-formula-free-variables ((f integer) &optional bound-names)
  (declare (ignorable bound-names))
  (list))

(defmethod logic-formula-free-variables ((f logic-constant) &optional bound-names)
  (declare (ignorable bound-names))
  (list))

(defmethod logic-formula-free-variables ((f logic-variable) &optional bound-names)
  (if (find (name f) bound-names :test 'equal)
    (list) (list (name f))))

(defmethod logic-formula-free-variables ((f logic-function-call) &optional bound-names)
  (reduce (lambda (x y) (union x y :test 'equal))
          (loop for a in (arg f) collect (logic-formula-free-variables a bound-names))))

(defmethod logic-formula-free-variables ((f logic-predicate-call) &optional bound-names)
  (loop with res := nil
        for a in (arg f)
        do (setf res (union res (logic-formula-free-variables a bound-names) 
                            :test 'equal))
        finally (return res)))

(defmethod logic-formula-free-variables ((f logic-comparison) &optional bound-names)
  (reduce (lambda (x y) (union x y :test 'equal))
          (loop for a in (arg f) collect (logic-formula-free-variables a bound-names))))

(defmethod logic-formula-free-variables ((f logic-operation) &optional bound-names)
  (reduce (lambda (x y) (union x y :test 'equal))
          (loop for a in (arg f) collect (logic-formula-free-variables a bound-names))))

(defmethod logic-formula-free-variables ((f logic-quantification) &optional bound-names)
  (logic-formula-free-variables 
    (subformula f)
    (union bound-names (mapcar 'name (arg f)) :test 'equal)))

