;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin
;;;; Note: SyntaxBNF is copied from TPTP distribution and copyright by 
;;;; Geoff Sutcliffe and Christian Suttner. Only verbatim redistribution
;;;; is permitted by the authors.

(asdf:defsystem
  :tptp-syntax
  :description "TPTP Syntax comparibility"
  :version "0.0"
  :author "Michael Raskin <raskin@mccme.ru>"
  :depends-on (:esrap-peg :esrap :alexandria :terms)
  :serial t
  :components
  (
   (:file "package")
   (:static-file "SyntaxBNF")
   (:static-file "SyntaxBNF.fix" :depends-on ("SyntaxBNF"))
   (:file "tptp" :depends-on ("package" "SyntaxBNF" "SyntaxBNF.fix"))
   (:file "tptp-ast" :depends-on ("package"))
   ))

