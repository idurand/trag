(in-package :tptp-syntax)

(esrap-peg:peg-compile
  (esrap-peg:parse-peg-file
    (source-relative-path "./tptp-staged-bnf.peg")))

(defun
  ; function to make heavy visually light, but semantically heavy names
  option-length (s)
  (+ (length s)
     (cond ((cl-ppcre:scan "_(or|and)_formula$" s) 40)
           ((cl-ppcre:scan "_(apply|type)_formula$" s) 20)
           ((cl-ppcre:scan "_(apply|xprod|union|mapping)_type$" s) 20)
           ((cl-ppcre:scan "_typing$" s) 15)
           ((cl-ppcre:scan "_(infix|binary)_formula$" s) 10)
           ((cl-ppcre:scan "_(infix|binary)$" s) 7)
           ((cl-ppcre:scan "(pair|binary|assoc)_connective$" s) 20)
           ((cl-ppcre:scan "_nonassoc$" s) 20)
           ((cl-ppcre:scan "_assoc$" s) 40)
           ((cl-ppcre:scan "_unary_" s) 10)
           ((cl-ppcre:scan "_infix_unary$" s) 50)
           ((cl-ppcre:scan "^thf_fof" s) 100)

           ((equal "real" s) 20)
           ((equal "rational" s) 20)
           ((cl-ppcre:scan "_exponent$" s) 10)

           ((cl-ppcre:scan "_typed_atom$" s) 60)
           ((cl-ppcre:scan "(^|_)let(_|$)" s) 40)
           (t 0))))

(defun compare-option-strings (x y)
  (let* ((lx (option-length x))
         (ly (option-length y)))
    (cond
      ((> lx ly) t)
      ((= lx ly) (string> x y))
      (t nil))))

(defun 
  order-options
  (opts)
  (let* ((keyed (loop for x in opts collect (list x (esrap-peg:ast-eval x))))
         (sorted (sort keyed 'compare-option-strings :key 'second))
         (res (mapcar 'first sorted)))
    res))

(defun left-recursion-process-lfold (name expansions)
  (when (< (length expansions) 2)
    (return-from left-recursion-process-lfold nil))
  (let*
    (
     (name-mentions
       (loop for x in expansions
	     collect
	     (list
	       (equal name (esrap-peg:ast-eval (caadr x)))
	       (cadr x))))
     (left-recursion-list 
       (mapcar 'second (remove-if 'not name-mentions :key 'car )))
     (seed-list 
       (mapcar 'second (remove-if 'identity name-mentions :key 'car )))
     (enough-mentions
       (unless
	 (and
	   (<= 1 (length left-recursion-list))
	   (<= 1 (length seed-list))
	   )
	 (return-from left-recursion-process-lfold nil)))
     (plus-candidate
       (= 1 (length left-recursion-list) (length seed-list)))
     (seed-rule (when plus-candidate (first seed-list)))
     (iter-rule (when plus-candidate (first left-recursion-list)))
     (iter-tail (cdr iter-rule))
     (iter-l (length iter-tail))
     (seed-l (length seed-rule))
     (seed-tail
       (and
	 plus-candidate
	 (>= seed-l iter-l)
	 (subseq seed-rule (- seed-l iter-l) seed-l)))
     (seed-head
       (and
	 plus-candidate
	 (>= seed-l iter-l)
	 (subseq seed-rule 0 (- seed-l iter-l))))
     (plus-ok
       (and
	 seed-tail
	 (equal
	   (esrap-peg:ast-eval (list 'MultiTokenSequence iter-tail))
	   (esrap-peg:ast-eval (list 'MultiTokenSequence seed-tail))
	   )
	 ))
     (star-tails
       (mapcar
	 (lambda (x)
	   (list 'MultiTokenSequence (cdr x)))
	 left-recursion-list))
     (res
       (if
	 plus-ok
	 `(Lfold-seeded
	    (
	     (MultiTokenSequence ,seed-head)
	     (MultiTokenSequence ,iter-tail)
	     ))
	 `(Lfold-unseeded
	    (
	     (MultiTokenChoiceDispatcher
	       ,(loop for x in seed-list
		      collect
		      `(MultiTokenSequence ,x)))
	     (MultiTokenChoiceDispatcher
	       ,star-tails
	       )
	     ))
	 )
       )
     )
    (declare
      (ignore enough-mentions)
      )
    res
    ))

(defvar *rule-name* nil)

(esrap-peg:def-peg-matchers
  (
   ; Basic parsing
   (SyntaxBNFFile
     ((?x _) (remove nil (m! #'! ?x))))
   (Comment
     (_ nil))
   (EmptyLine
     (_ nil))
   (Rule
     ((?name _ ?kind _ ?x ?xs _) 
      (list 
	(! ?name) (! ?kind) 
	(s+
	  (cons (! ?x) 
		(m! #'! ?xs))))))
   (RuleBody
     ((?data _) (s+ (m! #'! ?data))))
   (RuleContinuation
     ((_ _ ?data) (concatenate 'string " " (! ?data))))
   (LineChar
     ((_ ?x) ?x))
   (RuleName
     ((_ ?name _) (! ?name)))
   (RuleAssignment
     (_ x))
   (BaseName
     (_ (s+ x)))

   ; Parsing of sub-token ::: expressions
   (SubTokenEntry
     ((?data ?repetition _) 
      (let*
	(
	 (d (! ?data))
	 )
	(if 
	  ?repetition
	  (concatenate 'string d "*")
	  d
	  )
	)))
   (SubTokenEntrySequence
     (_ (format nil "~{~a~#[~:; ~]~}" (m! #'! x))))
   (SubTokenChoice
     ((?x ?xs) 
      (if ?xs (! (cons 'SubTokenChoiceDispatcher 
		       (list (cons ?x (m! #'n4 ?xs))))) (! ?x))))
   ; Put reorderings here, I guess
   (SubTokenChoiceDispatcher
     (_
       (! `(SubTokenChoiceDispatcherStrings
	      ,(m! #'! (order-options x))))))
   (SubTokenChoiceDispatcherStrings
     (_ (format nil "~{~a~#[~:; / ~]~}" x)))
   (SubTokenGroupEntry
     ((_ ?x _) (concatenate 'string "(" (! ?x) ")")))
   (SubTokenExpression
     (_ (! x)))

   ; Character class parsing per se
   (CharClassLiteral
     ((_ ?neg ?x _)
      (let*
	((data (! (list 'CharClassPositiveLiteral ?x))))
	(if
	  ?neg
	  (format nil "(! ~a .)" data)
	  data
	  )
	)
      ))
   (CharClassPositiveLiteral
     (_
       (cond
	 ((null x) "!. .")
	 ((= (length x) 1) (concatenate 'string "[" (! (n1 x)) "]"))
	 (t (format nil "[~{~a~}]" (m! #'! x)))
	 )))
   (CharClassEntry
     (_ (! x)))
   (CharClassRange
     ((?b _ ?e) (concatenate 'string (! ?b) "-" (! ?e))))
   (CharClassEntrySingle
     (_ (! x)))
   (CharClassEscape
     (_ (! x)))
   (CharClassEntryLiteral
     ((_ ?x) (if (equal ?x "-") "\\55" ?x)))
   (CharClassEscapeLiteral
     ((_ ?x) (concatenate 'string "\\" ?x)))
   (CharClassEscapeOct
     ((_ ?x) (concatenate 'string "\\" (! ?x)))) ; dirty hack because PEG works the same way
   (CharClassOctalNumber
     (_ (if (stringp x) x (s+ x))))
   (CharClass
     ("." x)
     (_ (! x)))

   ; Non-token rules
   ; Problems: 
   ;   1) order
   (MultiTokenExpression
     (_ (format nil " Sp? (~a) Sp? " (! x))))
   (MultiTokenChoice
     ((?x ?xs) 
      (if ?xs (! (cons 'MultiTokenChoiceDispatcher 
		       (list (cons ?x (m! #'n4 ?xs))))) (! ?x))))
   ; Put reorderings here, I guess
   (MultiTokenChoiceDispatcher
     (_ 
       (let*
	 ((lfold
	    (left-recursion-process-lfold
	      *rule-name*
	      x)))
	 (if
	   lfold
	   (! lfold)
	   (! `(MultiTokenChoiceDispatcherStrings
		 ,(m! #'! (order-options x))))))))
   (MultiTokenChoiceDispatcherStrings
     (_ (format nil "~{~a~#[~:; / ~]~}" x)))
   (MultiTokenSequence
     (() "''")
     (_ (format nil "~{~a~#[~:; Sp? ~]~}" (m! #'! x))))
   (MultiTokenEntry
     ((?data ?repetition _) 
      (let*
	(
	 (d (! ?data))
	 )
	(if 
	  ?repetition
	  (concatenate 'string d "*")
	  d
	  )
	)))
   (PseudoToken
     ((_ ?r) (! ?r)))
   (MultiTokenLiteralSymbol 
     (_ (n4 x)))
   (MultiTokenLiteralPart
     ((PseudoToken _) (! x))
     (_ (format nil "'~a'" (cl-ppcre:regex-replace-all "'" (s+ (m! #'! x)) "\\\\'"))))
   (MultiTokenLiteral
     (_ (s+ (m! #'! x))))

   ; Lfold conversion
   ; A = A B | C
   ; A <- C B*
   (Lfold-seeded
     ((?seed ?tail) (format nil "(~a) (~a)+" (! ?seed) (! ?tail)))
     )
   (Lfold-unseeded
     ((?core ?tail) (format nil "(~a) (~a)*" (! ?core) (! ?tail)))
     )
   )
  :abbreviations
  (
   (! (x) (esrap-peg:ast-eval x))
   (n1 (x) (first x)) (n2 (x) (second x)) (n3 (x) (third x))
   (n4 (x) (fourth x)) (n5 (x) (fifth x)) (n6 (x) (sixth x))
   (n7 (x) (seventh x)) (n8 (x) (eighth x)) (n9 (x) (ninth x))
   (s+ (x) (apply 'concatenate 'string x))
   (l+ (x) (apply 'append x))
   (m! (f x) (mapcar f x))
   )
  :package :tptp-syntax
  :arg x
  )

(defun syntax-bnf-to-peg
  (fn)
  (let*
    (
     (BNF-data (esrap-peg::file-contents fn))
     (parsed-rules (esrap-peg:ast-eval (esrap:parse 'SyntaxBNFFile BNF-data)))
     (subtoken-rules (loop for x in parsed-rules 
			   when (find (second x) '(":::" "::-") :test 'equal) 
			   collect x))
     (multitoken-rules (loop for x in parsed-rules 
			     when (find (second x) '("::=") :test 'equal) 
			     collect x))
     (parsed-rules
       (append
	 (loop 
	   for x in subtoken-rules
	   collect (list (first x) 
			 (esrap-peg:ast-eval 
			   (esrap:parse 
			     'SubTokenExpression 
			     (third x)))))
	 (loop 
	   for x in multitoken-rules
	   collect 
	   (let
	     ((*rule-name* (first x)))
	     (list 
	       *rule-name*
	       (esrap-peg:ast-eval 
		 (esrap:parse 
		   'MultiTokenExpression 
		   (third x))))))
	 ))
     (peg-data
       (format
	 nil
	 "~{~{~a <- ~a~}~%~}"
	 parsed-rules
	 ))
     (res peg-data)
     )
    res
    )
  )

(defpackage :tptp-bnf-syntax (:use))

(defun load-tptp-syntax-bnf (fn &key dump-peg)
  (let*
    (
     (peg-code (syntax-bnf-to-peg fn))
     (define-whitespace
       (format
	 nil "~{~a~%~}"
	 (list
	   "LineBreak <- '\\n' / '\\r' "
	   "Whitespace <- ' ' / '\\t' / '\\uc2a0' / comment / LineBreak "
	   "Sp <- Whitespace+ "
	   )
	 ))
     (overrides
       (format
	 nil "~{~a~%~}"
	 (list
	   "printable_char <- ! LineBreak ."
	   "not_star_slash <- (!'*/' .)*"
	   "sq_char <- ('\\\\' ['\\\\]/ (! ['\\\\] .))"
	   )
	 ))
     (peg-code (concatenate 'string 
			    peg-code define-whitespace overrides))
     (peg-tree (esrap-peg:basic-parse-peg peg-code))
     )
    (when 
      dump-peg
      (format t "~a~%" peg-code))
    (let*
      ((*package* (find-package :tptp-bnf-syntax)))
      (esrap-peg:ast-eval peg-tree)
      )
    ))

(defun parse-tptp (x)
  (esrap:parse 'tptp-bnf-syntax::TPTP_file x))

(defun load-tptp-file (fn &key limit-size)
  (let*
    ((data (esrap-peg::file-contents fn)))
    (if
      (or
	(not limit-size)
	(> limit-size (length data))
	)
      (parse-tptp data)
      (progn
	(format *error-output* "~s length ~s > ~s~%"
		fn (length data) limit-size)
	nil
	))))

(defun test-tptp-file-loading 
  (path &key blacklist (progress t) (size-limit 100000) (lower-size-limit 0))
  (let* ((proper-name
           (or (ignore-errors 
                 (directory (concatenate 'string (namestring path) "/")))
               (ignore-errors (directory path))))
         (proper-name (namestring (first proper-name)))
         (directoryp (and proper-name (cl-ppcre:scan "/$" proper-name)))
         (members (cond
                    (directoryp (directory
                                  (concatenate 'string proper-name "*.*")))
                    (proper-name (list proper-name))))
         (members 
           (remove-if 
             (lambda (x) 
               (or (find-if (lambda (r) (cl-ppcre:scan r (namestring x)))
                            blacklist)
                   (with-open-file (f x)
                     (not (<= lower-size-limit (file-length f) size-limit)))))
             members)))
    (cond
      (directoryp
        (mapcar 
          (lambda (p) (test-tptp-file-loading p :blacklist blacklist))
          members)
        t)
      (members
        (when progress (format t "Loading TPTP:~%~s~%" (first members)))
        (load-tptp-file (first members))
        (when progress (format t "OK~%~%"))
        t)
      (t nil))))

(eval-when (:load-toplevel :execute)
  (load-tptp-syntax-bnf (source-relative-path "./SyntaxBNF"))
  (load-tptp-syntax-bnf (source-relative-path "./SyntaxBNF.fix")))
