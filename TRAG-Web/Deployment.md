== Notes about TRAG deployment for WWW access ==

= Hosting =

Server: trag.labri.fr
OS: Red Hat Enterprise Linux Server 7.4
Access level: Includes `root`

= Updates =

To update the Lisp code it is enough to update the checkout (and possibly the
QuickLisp packages already installed) and kill the running SBCL process (by
attaching the `trag-web` session of `screen` or by finding SBCL with the
command `pgrep -a -x sbcl`). Another option is restarting the `trag-web`
service.

Note that the server doesn't have enough access. A local `~/.ssh/config` on the
client machine (a new client machine will need these tricks added; they look
like
```
Host trag.labri.fr
IdentityFile ~/.ssh/id_rsa
ProxyCommand ssh -D 5108X user@ssh.labri.fr nc %h %p
RemoteForward 5108X 127.0.0.1:5108X
```) sets up a SOCKS5 proxy available to server. To use the proxy for the
`trag` repository, log in using the personal user, then issue the command
`sudo su web` and `cd` to the necessary directory. This will inherit the proxy
settings in a form suitable for `git`. A direct login to `web` will not have
network access. Quicklisp currently cannot use SOCKS5 proxies, so the
recommended way of updating Quicklisp-managed dependencies is updating the
dependencies in a local development environment and copying them to server
using the `scp` command.

A catch related to Quicklisp is caching the list of the `.asd` files in the
`local-projects` directory. If an `.asd` file is added to `trag`, remove the
file `trag-quicklisp/local-projects/system-index.txt`.

There should be only one `screen` instance running as the `web` user. To
connect to it, it is enough to run `screen -R`; if there are multiple sessions
running (`screen -ls` will list them), `screen -R -S [PID]` will be needed.

In general, `screen` keybindings start with `Ctrl-a`. Most of the time, only
one is needed: `Ctrl-a d`, which detaches the screen.

To perform a system update it is needed to do `yum -v update` as the `root`
user, then accept the proposed list of package updates.

To update the certificate, the new certificate and the private key should go
into `/var/cert/`. Please concatenate the certificate with the CA certificate.
After updating the certificate/key, restart Nginx service with the command
`systemctl restart nginx` (as root).

= Current state =

Key-based SSH access available for accounts `web`, `root` and accounts
corresponding to people.

Installed repositories:
- EPEL7: Extra Packages for Enterprise Linux 7, see:
  - `/etc/yum.repos.d/epel.repo`
  - `/root/epel-release-latest-7.noarch.rpm`

Installed software:
- Graphviz from the main RHEL repositories
- SBCL from EPEL
- Nginx (Web server/Reverse proxy) from EPEL
- Glucose — manually compiled — see
  - `/root/glucose-syrup-4.1.tgz`

User web owns the trag deployment in `/home/web/trag` (git checkout)
Additional directories:
- `/home/web/trag-quicklisp`: a QuickLisp instance for TRAG dependencuies
  - contains a symbolic link to `/home/web/trag` inside `local-projects/`
- `/home/web/cwd-encode`: a checkout of `cwd-encode` with the binaries built
- `/home/web/bin`:
  - symlink to `/home/web/cwd-encode/cwd-encode`
  - symlink to `/home/web/cwd-encode/decode`
  - `launch-sbcl-web-server` — a script to start (and restart) an SBCL instance
  serving TRAG web pages

= The general setup =

There is a `systemd` service in the file
`/etc/systemd/system/multi-user.target.wants/trag-web.service` (a symlink to
the `/usr/lib/systemd/system/trag-web.service` service fule) which runs
`/usr/bin/screen -U -d -m -D -S trag-web /home/web/bin/launch-sbcl-web-server`
to start a `screen` instance with TRAG SBCL instance serving webpages. This
is executed on every reboot (it can also be restarted with the command
`systemctl restart trag-web` as root).

There is an Nginx instance configured in `/etc/nginx/nginx.conf` that controls
the ports 80 and 443 (HTTP and HTTPS) and obtains the content from
`http://127.0.0.1:8080` (where the SBCL instance serves web pages). To apply
the change, restart or reload `nginx` (with the command
`systemctl reload nginx`). Note that Nginx logs session IDs instead of IPs to
reduce private data storage.
