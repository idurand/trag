(in-package :trag-web)

(defparameter *ajax-processor*
  (make-instance 'smackjack::ht-simple-ajax-processor :server-uri "/ajax/request"))
(defparameter *ajax-dispatcher* (smackjack::create-ajax-dispatcher *ajax-processor*))

(defvar *hunchentoot-acceptor* nil)

(defvar *uris* (list))

(defun start-server (&key (port 8080))
  (unless *hunchentoot-acceptor*
    (hunchentoot:start
      (setf *hunchentoot-acceptor*
            (make-instance 'hunchentoot:easy-acceptor :port port)))
    (cond
      ((endp *uris*))
      ((endp (rest *uris*)) (format *trace-output* "Visit the page:~%"))
      (t (format *trace-output* "Visit one of the pages:~%")))
    (loop for uri in *uris* do
          (format *trace-output*
                  "http://localhost:~A~A~%" port uri))
    ; activate AJAX handler
    (setf hunchentoot:*dispatch-table*
          (list 'hunchentoot:dispatch-easy-handlers
                *ajax-dispatcher*))))

(defun stop-server ()
  (progn
    (hunchentoot:stop *hunchentoot-acceptor*)
    (setf *hunchentoot-acceptor* nil)))

(defun restart-server (&key (port 8080))
  (when *hunchentoot-acceptor* (stop-server))
  (start-server :port port))

(defmacro define-ajax-rpc (function-name package &key wrapper-code)
  `(smackjack::defun-ajax
     ,function-name (name farguments)
     (*ajax-processor* :method :post :callback-data :json)
     (prog1
       (encode-json-to-string
         (,@ (or wrapper-code '(progn))
             (apply
               (find-symbol (string-upcase name) ,package)
               (coerce
                 (decode-json-from-string farguments)
                 'list)))))))
