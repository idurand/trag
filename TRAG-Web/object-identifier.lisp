;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin, Irène Durand

(in-package :trag-web)

;;; Object identifier is JSON code
;;; type
;;; operation
;;; parameters is a list of parameters in the same format
;;; immediate-data is a direct string representation of the desired object
;;;   - operation is ignored and should be missing
;;; type is checked on evaluation
;;;
;;; operations and types are symbols to UPCASE and INTERN into
;;; *trag-web-id-package*

(defvar *trag-web-id-package* (find-package :trag-web-ids))

(import :string *trag-web-id-package*)
(import :integer *trag-web-id-package*)
(import :multiple-strings *trag-web-id-package*)

(defclass object-identifier ()
  ((type
    :initarg :type
    :initform (error "Type must be specified")
    :accessor object-identifier-type
    :documentation "The type of the object")
   (immediate-data
    :initarg :immediate-data
    :initform nil
    :type (or null string)
    :accessor object-identifier-immediate-data
    :documentation "An immediate string that precisely specifies the object")
   (operation
    :initarg :operation
    :initform nil
    :accessor object-identifier-operation
    :documentation "The operation used to build the object")
   (parameters
    :initarg :parameters
    :initform nil
    :type sequence
    :accessor object-identifier-parameters
    :documentation "The parameters to pass to the operation")))

(defmethod initialize-instance ((obj object-identifier) 
                                &key type immediate-data operation parameters)
  (setf (object-identifier-type obj)
        (when type (intern (string-upcase type) *trag-web-id-package*))
        (object-identifier-immediate-data obj)
        immediate-data
        (object-identifier-operation obj)
        (when operation 
          (intern (string-upcase operation) *trag-web-id-package*))
        (object-identifier-parameters obj)
        (map 'vector 'object-identifier-from-json parameters)))

(defun object-slot-json-entry (object slot &key for-js-only)
  (let* ((value (slot-value object slot))
         (slot-name (string-downcase (symbol-name slot)))
         (value-representation
           (cond ((null value) nil)
                 ((symbolp value) (string-downcase (symbol-name value)))
                 (t value))))
    (cons (if for-js-only slot slot-name) value-representation)))

(defmethod cl-json:encode-json
    ((value object-identifier) &optional (stream cl-json:*json-output*))
  (cl-json:encode-json
   (loop for slot in '(type immediate-data operation parameters)
	 collect (object-slot-json-entry value slot))
   stream))

(defun create-immediate-identifier (type data)
  (make-instance 'object-identifier :type type :immediate-data data))

(defun create-operation-identifier (type operation parameters)
  (make-instance 'object-identifier
                 :type type :operation operation :parameters parameters))

(defun object-identifier-from-json (id)
  (cond
    ((stringp id)
     (object-identifier-from-json (cl-json:decode-json-from-string id)))
    ((typep id 'object-identifier) id)
    ((listp id)
     (apply 'make-instance 'object-identifier
            (loop for entry in id collect (car entry) collect (cdr entry))))
    (t (error "Cannot interpret ~s as an object identifier" id))))

; We always reencode to make sure field order and null fields are represented
; in a consistent way
(defun object-identifier-to-json (id)
  (cl-json:encode-json-to-string (object-identifier-from-json id)))

(defgeneric check-identifier-type (id-type value)
  (:documentation "Verify that the value has required type")
  (:method (id-type value) nil)
  (:method ((id-type (eql :string)) (value string)) t)
  (:method ((id-type (eql :integer)) (value integer)) t))

(defgeneric parse-immediate-data (type string)
  (:documentation "Parse direct string representation of a value")
  (:method (type string)
    (error "Cannot parse as ~a; got string ~s" type string))
  (:method ((type (eql :integer)) (string string)) (parse-integer string))
  (:method ((type (eql :string)) (string string)) string))

(defgeneric evaluate-identifier-function (operation type parameters)
  (:documentation "Apply an operation")
  (:method (operation type parameters)
    (error "unknown operation ~a (in package ~a) of type ~a invoked on parameters ~s"
	   operation (package-name *trag-web-id-package*) type parameters))
  (:method ((operation (eql 'trag-web-ids::concatenate)) (type (eql :string)) parameters)
    (apply 'concatenate 'string parameters))
  (:method ((operation (eql 'trag-web-ids::sum)) (type (eql :integer)) parameters)
    (apply '+ parameters)))

(defmethod evaluate-identifier-function
    ((operation (eql 'trag-web-ids::repeat-string-times)) (type (eql :string)) parameters)
  (destructuring-bind (str cnt) parameters
    (apply
     'concatenate 'string
     (loop for k from 1 to cnt collect str))))

(defgeneric object-identifier-operation-destructive-p (operation type)
  (:documentation "Report whether an operation is destructive")
  (:method (operation type) nil))

(defun lookup-or-evaluate-parameters
  (parameters
    &key parameter-lookup parameter-evaluation-hook
    destructive-callback)
  (loop with parameter-lookup := (or parameter-lookup (lambda (id) (declare (ignore id))))
        with parameter-evaluation-hook :=
        (or parameter-evaluation-hook (lambda (id value destructivep)
                                           (declare (ignore id value destructivep))))
        for p in (coerce parameters 'list) collect
        (multiple-value-bind (value match) (funcall parameter-lookup p)
          (if match value
            (multiple-value-bind (value destructivep)
              (evaluate-identifier
                p :parameter-lookup parameter-lookup
                :parameter-evaluation-hook parameter-evaluation-hook)
              (when (and destructive-callback destructivep)
                (funcall destructive-callback))
              (funcall parameter-evaluation-hook p value destructivep)
              value)))))

(defun evaluate-identifier (id &key (package *trag-web-id-package*)
				 parameter-lookup parameter-evaluation-hook)
  (let* ((package (find-package package))
         (parsed-id (let* ((*trag-web-id-package* package))
                      (object-identifier-from-json id)))
	 (type (object-identifier-type parsed-id))
	 (operation (object-identifier-operation parsed-id))
	 (parameters (object-identifier-parameters parsed-id))
	 (immediate-data (object-identifier-immediate-data parsed-id))
         (destructivep nil)
	 (value
	   (cond ((null type) (error "Type must be specified; got ~a"
                                     (cl-json:encode-json-to-string parsed-id)))
		 (immediate-data (parse-immediate-data type immediate-data))
		 (operation 
		  (setf destructivep
			(object-identifier-operation-destructive-p
			 operation type)
			parameters
			(lookup-or-evaluate-parameters
                          parameters :parameter-lookup parameter-lookup
                          :parameter-evaluation-hook parameter-evaluation-hook
                          :destructive-callback
                          (lambda () (setf destructivep t))))
		  (evaluate-identifier-function operation type parameters))
		 (t (error
		     "Either immediate-data or operation must be present; got ~s"
                     (cl-json:encode-json-to-string parsed-id)))))
	 (type-match (check-identifier-type type value)))
    (if type-match
	(values value destructivep)
	(error
	 "Identifier evaluation lead to an object not of type ~a; evaluated ~s to ~s"
	 type parsed-id value))))

(defgeneric canonical-identifier (type value)
  (:documentation "Create a canonical JSON representation; nil means failure")
  (:method (type value)
    (warn "undefined type/value combination ~S/~S" type value))
  (:method ((type (eql :integer)) (value integer))
    (create-immediate-identifier :integer (format nil "~a" value)))
  (:method ((type (eql :string)) (value string))
    (create-immediate-identifier :string value)))

(defgeneric canonical-description (type value)
  (:documentation "A human-readable description of a value")
  (:method ((type (eql :integer)) (value integer))
    (format nil "Integer: ~a" value))
  (:method ((type (eql :string)) (value string))
    (format nil "String: ‘‘~a’’" value))
  (:method (type value)
    (format nil "Value of type ~a: ~a" (string-downcase type) value)))

(defgeneric canonical-html-description (type value)
  (:documentation "A human-readable HTML description of a value; nil if none")
  ( :method (type value)))

;;; In general, the methods here should be :around methods
(defgeneric object-identifier-attributes (type value)
  (:documentation "Additional attributes for the JS client")
  (:method (type value)))

(defmacro define-object-identifier-type ((type &key (type-var (gensym)))
                                         &body clauses)
  `(progn
     ,@(loop for clause in clauses
             for kind := (first clause)
             for name := (case kind
                           (:check 'check-identifier-type)
                           (:lisp-type 'check-identifier-type)
                           (:parse 'parse-immediate-data)
                           (:parser 'parse-immediate-data)
                           (:canonical-id 'canonical-identifier)
                           (:canonical-immediate-data 'canonical-identifier)
                           (:unparser 'canonical-identifier)
                           (:description 'canonical-description)
                           (:html-description 'canonical-html-description)
                           (:attributes 'object-identifier-attributes)
                           (:override-attributes 'object-identifier-attributes))
             for arguments := (case kind
                                (:lisp-type `(value))
                                (:parser `(string))
                                (:unparser `(value))
                                (t (second clause)))
             for body := (case kind
                           (:lisp-type `((typep value ',(second clause))))
                           (:parser `((,(second clause) string)))
                           (:unparser `((create-immediate-identifier
					 ',type (,(second clause) value))))
                           (:canonical-immediate-data
			    `((create-immediate-identifier 
			       ',type (progn ,@(subseq clause 2)))))
                           (:attributes
                             `((general:merge-key-value
                                 (call-next-method)
                                 (progn ,@(subseq clause 2)))))
                           (t (subseq clause 2)))
             for method-qualifiers := (case kind
                                  (:attributes (list :around)))
             when name
	       collect
             `(defmethod ,name ,@method-qualifiers
                ((,type-var (eql ',type)) ,@arguments)
                (declare (ignorable ,type-var))
                ,@body))))

(defmacro define-object-identifier-operations ((type &key (type-var (gensym))
						       (operation-var (gensym)))
                                               &body clauses)
  `(progn
     ,@(loop for clause in clauses
             collect
             (destructuring-bind (name arguments &rest body) clause
               `(defmethod evaluate-identifier-function
		    ((,operation-var (eql ',name)) (,type-var (eql ',type))
		     parameters)
                  (declare (ignorable ,type-var ,operation-var))
                  (destructuring-bind ,arguments parameters
                    ,@body))))))

(define-object-identifier-type (:multiple-strings)
  (:check (value) (and (listp value)
                       (every 'stringp value)))
  (:parse (string) (cl-ppcre:split (format nil "[~%]+") string))
  (:description (value) (format nil "~{~a~%~}" value))
  (:html-description (value)
                     (format nil "~{~a<br/>~%~}"
                             (mapcar 'cl-emb::escape-for-xml value)))
  (:canonical-id (value) nil))
