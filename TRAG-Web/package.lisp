(defpackage :trag-web
  (:use :common-lisp :json :input-output)
  (:export
    ; JSON-based object identifiers
    #:create-immediate-identifier
    #:create-operation-identifier

    #:check-identifier-type
    #:parse-immediate-data
    #:evaluate-identifier-function
    #:evaluate-identifier
    #:canonical-identifier
    #:canonical-description
    #:object-identifier-attributes

    #:define-object-identifier-type
    #:define-object-identifier-operations

    #:*trag-web-id-package*

    ; Sessions
    #:random-session-id
    #:get-session
    #:kill-session
    #:refresh-session
    #:kill-old-sessions
    #:evaluate-in-session

    #:*session-table*
    #:make-session-table

    ; AJAX/JS helpers
    #:empty-html
    #:define-ajax-rpc
    #:*ajax-processor*

    #:start-server
    #:stop-server
    #:restart-server

    #:save-variables
    #:add-parenscript-definitions
    #:pass-value
    #:define-function-wrapper
    #:chain-async-postfix
    #:chain-async-prefix
    #:chain-async
    #:eval-with-continuation
    #:*parenscript-aliases*
    #:*parenscript-control-flow-helpers*
    #:*parenscript-datatype-helpers*
    #:*parenscript-html-helpers*
    #:*parenscript-file-helpers*
    #:*parenscript-trag-web-page-definitions*

    #:*trag-web-js-render-helpers*

    #:*trag-web-session-js-helpers*

    #:*uris*

    #:defcommand
    #:defmenu
    #:define-pane-naming
    #:define-pane

    #:defcommand-with-backend
    #:defdownload-with-backend

    #:define-pane-page

    #:web-log-summary
    )
  )

(defpackage :trag-web-ids (:use))
