(in-package :trag-web)

; Wrap code in a function call with parameters to ensure that the variables
; are captured in a closure
(ps:defpsmacro
  save-variables ((&rest vars) &body body)
  `((lambda ,(loop for v in vars collect (if (listp v) (first v) v)) ,@body)
    ,@(loop for v in vars collect (if (listp v) (second v) v))))

(eval-when
  (:compile-toplevel :load-toplevel :execute)
  (defparameter *parenscript-definitions* ()))

(defmacro add-parenscript-definitions
  ((definition-set &optional (clear nil)) &body args)
  "Put parenscript definitions inside some variable; use defvar or defparameter
depending on the optional CLEAR parameter.
The code inside is Parenscript code with JavaScript semantics"
  `(eval-when
     (:compile-toplevel :load-toplevel :execute)
     (,(if clear 'defparameter 'defvar) ,definition-set nil)
     (apply 'ps:ps* ',args)
     (setf
       ,definition-set
       (concatenate 'list ,definition-set ',args))))

; Pass a JSON-friendly value from Lisp code to the generated Javascript
(ps:defpsmacro
  pass-value (value)
  (let
    ((name (gensym)))
    `(let
       ((,name (ps:lisp (encode-json-to-string ,value))))
       (ps:chain -j-s-o-n (parse ,name)))))

; Create a quick wrapper for (possibly nested) object method
; Reasons: brevity and working around some heuristics
(ps:defpsmacro
  define-function-wrapper (name &rest chain)
  `(defun ,name (&rest args)
     (apply (ps:chain ,@chain) args)))

(add-parenscript-definitions
  (*parenscript-aliases* t)
  (define-function-wrapper document-create-element
                           document create-element)
  (define-function-wrapper document-create-text-node
                           document create-text-node)
  (define-function-wrapper document-get-element-by-id
                           document get-element-by-id)
  (define-function-wrapper document-get-elements-by-class-name
                           document get-elements-by-class-name)
  (define-function-wrapper object-values
                           -object "values")
  (define-function-wrapper object-keys
                           -object "keys")
  (define-function-wrapper console-log
                           console log))

(add-parenscript-definitions
  (*parenscript-control-flow-helpers* t)
  (defun chain-callbacks (calls prefixp)
    (let* ((thunk (lambda ())))
      (loop 
        for c in (ps:chain calls (reverse))
        for f := (ps:chain c func)
        for args := (ps:chain c args)
        do (save-variables 
             (f args)
             (setf thunk 
                   (save-variables
                     (thunk)
                     (if prefixp
                       (lambda () (apply f (append (list thunk) args)))
                       (lambda () (apply f (append args (list thunk)))))))))
      (thunk)))
  (defun chain-prefix-callbacks (calls)
    (chain-callbacks calls t))
  (defun chain-postfix-callbacks (calls)
    (chain-callbacks calls nil)))

(ps:defpsmacro chain-async (calls prefixp)
  (loop for call in (reverse calls)
        for callback := nil then `(lambda () ,code)
        for function := (first call)
        for args := (rest call)
        for full-args := (if prefixp
                           (append (list callback) args)
                           (append args (list callback)))
        for code := `(,function ,@full-args)
        finally (return code)))

(ps:defpsmacro chain-async-prefix (&rest calls) `(chain-async ,calls t))
(ps:defpsmacro chain-async-postfix (&rest calls) `(chain-async ,calls nil))
(ps:defpsmacro eval-with-continuation (value then-do)
  `(progn ,value (let ((then-do ,then-do)) (when then-do (then-do)))))

(add-parenscript-definitions
  (*parenscript-html-helpers* t)
  ; Basic addition of nodes
  (defun put-text (node text)
    (ps:chain node (append-child (document-create-text-node text))))
  (defun put-empty-element (node tag)
    (let* ((child (document-create-element tag)))
      (ps:chain node (append-child child))
      child))

  ; Element visibility control
  (defun toggle-visibility (node display &key hide show)
    (if (or show
            (and
              (= (ps:chain node style display) "none")
              (not hide)))
      (setf (ps:chain node style display) display)
      (progn (setf (ps:chain node style display) "none") nil)))

  ; Displaying information in a node
  (defun show-text (target text keep)
    (unless keep (setf (ps:chain target inner-h-t-m-l) ""))
    (cond
      ((not text) nil)
      ((ps:objectp text)
       (loop
         for line in text
         do (progn
              (put-text target line)
              (put-empty-element target :br))))
      (t (put-text target text)))
    (toggle-visibility target "block" :hide (not text) :show text)
    target)
  (defun show-html (target text keep)
    (setf (ps:chain target inner-h-t-m-l)
          (+
            (if keep (ps:chain target inner-h-t-m-l) "")
            (or text "")))
    (toggle-visibility target "block" :hide (not text) :show text)
    target)
  (defun show-pre (target text keep)
    (unless keep (setf (ps:chain target inner-h-t-m-l) ""))
    (when text (put-text (put-empty-element target :pre) text))
    (toggle-visibility target "block" :hide (not text) :show text)
    target)

  ; Locating an element
  (defun select-element (selector supercontainer)
    (let*
      ((supercontainer (or supercontainer document)))
      (or
        ; selector is already an element
        (and (ps:objectp selector)
             (ps:chain selector inner-h-t-m-l)
             selector)
        ; try using selector as an element ID, then class name
        (and (ps:stringp selector)
             (ps:chain
               document
               (get-element-by-id selector)))
        (and (ps:stringp selector)
             (elt
               (ps:chain
                 supercontainer
                 (get-elements-by-class-name selector))
               0))
        (and (ps:stringp selector)
             (elt
               (ps:chain
                 supercontainer
                 (get-elements-by-name selector))
               0))
        ; use selector as a CSS selector
        ; note that a plain tag name is a valid CSS selector for that tag name
        (and (ps:stringp selector)
             (ps:chain
               supercontainer
               (query-selector selector))))))

  ; Removing/inserting/moving elements inside a parent element
  (defun kill-element (e) (ps:chain e parent-node (remove-child e)))
  (defun insert-as-first (node parent)
    (ps:chain parent (insert-before node (ps:chain parent first-child))))
  (defun raise-in-parent (node)
    (let*
      ((parent (ps:chain node parent-node)))
      (ps:chain parent (remove-child node))
      (insert-as-first node parent)))
  (defun raise-or-insert (container predicate creator
                               &key leave-extras)
    (let*
      ((matching-nodes
         (remove-if-not
           predicate
           (ps:chain container children)))
       (first-match (when (> (length matching-nodes) 0)
                      (elt matching-nodes 0))))
      (cond
        ((not first-match) (insert-as-first (creator) container))
        (leave-extras (raise-in-parent first-match container))
        (t (mapcar kill-element matching-nodes)
           (insert-as-first first-match container)))))

  (defun space-event-p (event)
    (or (= 32 (ps:chain event char-code))
        (= "Space" (ps:chain event code))
        (= " " (ps:chain event key)))))

(add-parenscript-definitions
  (*parenscript-datatype-helpers* t)
  ; list filtering
  (defun remove-if (predicate data)
    (if data
      (loop
        for x in data
        unless (predicate x)
        collect x)
      (list)))
  (defun remove-if-not (predicate data)
    (if data
      (loop
        for x in data
        when (predicate x)
        collect x)
      (list)))

  ; string prefix filtering
  (defun choose-starting-with (s l)
    (remove-if-not
      (lambda (x)
        (ps:chain x (to-lower-case)
                  (starts-with (ps:chain s (to-lower-case)))))
      l))
  (defun common-start (s1 s2)
    (loop
      for n from 0 to (- (min (length s1) (length s2)) 1)
      when (/= (ps:chain (elt s1 n) (to-lower-case))
               (ps:chain (elt s2 n) (to-lower-case)))
      do (return (ps:chain s1 (slice 0 n)))
      finally (return (ps:chain s1 (slice 0 n)))))
  (defun common-start-list (l)
    (loop
      for s in l
      for res := s then (common-start s res)
      finally (return res)))
  (defun common-continuation (s l)
    (common-start-list
      (choose-starting-with s l)))

  ; trivial, but useful for client-server interaction
  (defun json-args (&rest l)
    (ps:chain -j-s-o-n (stringify l))))

(add-parenscript-definitions
  (*parenscript-file-helpers* t)
  (defun use-uploaded-file (input processor)
    (let* ((files (or (ps:chain input files) input))
           (file (and (< 0 (ps:chain files length)) (elt files 0)))
           (reader (ps:new -file-reader)))
      (setf (ps:chain reader onload)
            (lambda () (processor (ps:chain reader result))))
      (when file (ps:chain reader (read-as-text file)))))
  (defun use-uploaded-json-file (input processor)
    (use-uploaded-file
      input
      (lambda (text) (processor (ps:chain -j-s-o-n (parse text))))))
  (defun save-as (a data name type)
    (let* ((blob
             (ps:new
               (-blob
                 (list data)
                 (ps:create 'type (or type "text/plain; charset=utf-8")))))
           (url (ps:chain -u-r-l (create-object-u-r-l blob))))
      (setf (ps:chain a href) url
            (ps:chain a download) (or name "download.dat")
            (ps:chain a onclick)
            (lambda ()
              (set-timeout
                (lambda ()
                  (setf
                    (ps:chain a download) null
                    (ps:chain a href) null)
                  (ps:chain -u-r-l (revoke-object-u-r-l url)))
                0)))))
  (defun download-data (str filename type)
    (let* ((a (document-create-element :a)))
      (save-as a str filename type)
      (setf (ps:chain a style display) "none")
      (ps:chain document body (append-child a))
      (set-timeout (lambda () (ps:chain a (click))
                     (kill-element a)) 0)))
  (defun date-file-name (filename fileext)
    (let* ((date (ps:chain (ps:new (-date))
                           (to-i-s-o-string)
                           (replace (-reg-exp ":" "g") "-")
                           (replace (-reg-exp "[.].*") ""))))
      (+ filename "-" date "." fileext))))

(defparameter *parenscript-trag-web-page-definitions*
  (append
    *parenscript-aliases*
    *parenscript-control-flow-helpers*
    *parenscript-datatype-helpers*
    *parenscript-html-helpers*
    *parenscript-file-helpers*))
