(in-package :trag-web)

(defclass remote-session-table ()
  ((hash-table
     :type hash-table
     :initarg :hash-table
     :initform (make-hash-table :test 'equal)
     :accessor sessions
     :documentation "The table of sessions")
   (lock
     :initarg lock
     :initform (bordeaux-threads:make-lock)
     :accessor lock
     :documentation "The lock used for accessing the table")))

(defun make-session-table () (make-instance 'remote-session-table))

(defvar *session-table* (make-session-table))

(defvar *random-state-initialised* nil)
(unless *random-state-initialised*
  (setf *random-state* (make-random-state t)
        *random-state-initialised* t))

(defun random-session-id () (format nil "~36r" (random (expt 36 20))))

; possible status:
; :done
; :error
; :cancelled
; :busy
; :destroyed
(defclass session-evaluation-result ()
  ((status
     :initarg :status
     :accessor status
     :initform nil
     :documentation "Current evaluation status")
   (value
     :initarg :value
     :accessor value
     :initform nil
     :documentation "Result of the evaluation or data related to an error")
   (time
     :initarg :time
     :accessor evaluation-time
     :initform nil
     :documentation "Time spend for evaluation")))

(defun copy-session-evaluation-result (result)
  (make-instance 'session-evaluation-result
                 :status (status result) :value (value result)))

(defun make-session-evaluation-result-done (value &optional time)
  (make-instance
    'session-evaluation-result
    :status :done :value value :time time))

(defun make-session-evaluation-result-error (error)
  (make-instance
    'session-evaluation-result
    :status :error :value error))

(defun make-session-evaluation-result-busy (&optional thunk)
  (make-instance
    'session-evaluation-result
    :status :busy :value thunk))

(defun make-session-evaluation-result-cancelled ()
  (make-instance
    'session-evaluation-result
    :status :cancelled :value nil))

(defun make-session-evaluation-result-destroyed ()
  (make-instance
    'session-evaluation-result
    :status :destroyed :value nil))

(defun session-evaluation-result-successful-p (result)
  (find (status result) '(:done)))

(defun session-evaluation-result-error-text (result)
  (ecase (status result)
    (:error (format nil "Error: ~a" (value result)))
    (:cancelled "The request has been cancelled")
    (:busy "Another request is being processed")
    (:destroyed "The session has been destroyed")))

(defclass remote-session ()
  ((key
     :initarg :key
     :initform (random-session-id)
     :accessor key
     :documentation "The key for the session")
   (last-operation 
     :initarg :latest-operation 
     :initform (local-time:timestamp-to-unix (local-time:now))
     :accessor latest-operation 
     :documentation
     "Unix timestamp of the latest operation performed in the session")
  (entries
     :initarg :entries
     :initform (make-hash-table :test 'equal)
     :accessor entries
     :documentation "Saved named entries inside the session")
  (lock
    :initarg lock
    :initform (bordeaux-threads:make-lock)
    :accessor lock
    :documentation "The lock used to modify the session")
  ; When the session is reachable through the session table either
  ;    - worker and worker-result are both null
  ;    - worker is a thread (maybe already finished),
  ;      worker-result is a session-evaluation-result
  (worker
    :initarg :worker
    :initform nil
    :accessor worker
    :documentation "The worker thread for the current pending operation")
  (worker-result
    :initarg :worker-result
    :initform nil
    :accessor worker-result
    :type session-evaluation-result
    :documentation
    "The result produced by the worker thread for the current session")))

(defun set-session-worker-result (session result &optional worker)
  (setf (worker-result session) result
        (worker session) worker))

(defun make-session (&optional key)
  (if key
    (make-instance 'remote-session :key key)
    (make-instance 'remote-session)))

(defun get-session (key &key (create t) (session-table *session-table*))
  (if (typep key 'remote-session)
      key
      (bordeaux-threads:with-lock-held ((lock session-table))
	(or
	 (gethash key (sessions session-table))
	 (when create
	   (setf
            (gethash key (sessions session-table))
            (make-session key)))))))

(defmacro with-locked-session (session &body body)
  `(bordeaux-threads:with-lock-held ((lock ,session))
     ,@body))

(defun kill-session (key &key (session-table *session-table*))
  (let ((session
          (bordeaux-threads:with-lock-held
            ((lock session-table))
            (prog1
              (gethash key (sessions session-table))
              (remhash key (sessions session-table))))))
    (when session
      (with-locked-session session
        (when (worker session)
          (bordeaux-threads:destroy-thread (worker session)))
        (set-session-worker-result
          session (make-session-evaluation-result-destroyed))))))

(defun refresh-session (key)
  (let ((session (get-session key :create nil)))
    (when session
      (with-locked-session session
        (setf (latest-operation session)
              (local-time:timestamp-to-unix (local-time:now)))))))

(defun cancel-session-request (key)
  (let ((session (get-session key :create nil)))
    (with-locked-session session
      (when (worker session)
        (bordeaux-threads:destroy-thread (worker session)))
      (set-session-worker-result session nil))))

(defun kill-old-sessions (age &key (session-table *session-table*))
  (bordeaux-threads:with-lock-held
    ((lock session-table))
    (let*
      ((sessions-to-kill nil)
       (threshold (- (local-time:timestamp-to-unix (local-time:now)) age)))
      (maphash
        (lambda (k v)
          (bordeaux-threads:with-lock-held
            ((lock v))
            (when (< (latest-operation v) threshold)
              (push k sessions-to-kill))))
        (sessions session-table))
      (loop
        for k in sessions-to-kill
        do (kill-session k :session-table session-table)))))

(defun calculate-session-evaluation-result (thunk session trag-web-id-package)
  (handler-bind
    ((error (lambda (e)
              (return-from calculate-session-evaluation-result
                           (make-session-evaluation-result-error e)))))
    (let ((*trag-web-id-package* trag-web-id-package))
      (multiple-value-bind (result time)
        (general:time-call (funcall thunk session))
        (make-session-evaluation-result-done result time)))))

(defun set-session-entry (key value session)
  (setf (gethash key (entries session)) value))

(defun locked-set-session-entry (key value session)
  (with-locked-session session
    (set-session-entry key value session)))

(defun get-session-entry (key session)
  (gethash key (entries session)))

(defun locked-get-session-entry (key session)
  (with-locked-session session
    (get-session-entry key session)))

(defun evaluate-in-session (session thunk &optional names)
  (refresh-session (key session))
  (let ((thread 
          (with-locked-session session
            (if (worker-result session)
              (progn
                (format *error-output* "Session ~s already busy: ~s~%"
                        (key session) (worker-result session))
                nil)
              (set-session-worker-result
                session
                (make-session-evaluation-result-busy thunk)
                (bordeaux-threads:make-thread
                  (let* ((trag-web-id-package *trag-web-id-package*))
                    (lambda ()
                      ; This evaluation should be outside the lock
                      (let ((result (calculate-session-evaluation-result
                                      thunk session trag-web-id-package)))
                        (with-locked-session session
                          (setf (worker-result session) result)))))))))))
    (when thread (bordeaux-threads:join-thread thread)))
  (refresh-session (key session))
  (with-locked-session session
      (let* ((result (or (worker-result session)
                         (make-session-evaluation-result-cancelled))))
	(when (session-evaluation-result-successful-p result)
	  (loop for name in names
		do (set-session-entry name (value result) session)))
	(set-session-worker-result session nil)
	result)))
