(in-package :trag-web)

(eval-when
  (:compile-toplevel :load-toplevel :execute)
  (defparameter
    *trag-web-minimal-input-form*
    (alexandria:read-file-into-string
      (source-relative-path
        "html/minimal-input-form.html"))))

(add-parenscript-definitions
  (*trag-web-js-render-helpers* t)
  (defun simple-completion (input options
                                   list-container
                                   selection-callback)
    (let*
      ((good-options
         (choose-starting-with input options))
       (completed (common-start-list good-options)))
      (when list-container
        (show-text list-container nil)
        (toggle-visibility list-container "block")
        (loop for o in good-options
              for s := (document-create-element "span")
              do (show-text s o)
              do (setf (ps:chain s onclick)
                       (lambda ()
                         (toggle-visibility list-container "none")
                         (cond
                           ((ps:functionp selection-callback)
                            (selection-callback o))
                           ((null selection-callback) nil)
                           ((ps:objectp selection-callback)
                            (setf (ps:chain selection-callback value) o)
                            (ps:chain selection-callback (focus)))
                           )))
              do (ps:chain list-container (append-child s)))
        )
      completed))
  (defun put-minimal-input-form
    (target callback &key
            completion-options-callback
            completion-callback
            hide-input hide-expand hide-upload
            prompt help
            )
    (setf (ps:chain target inner-h-t-m-l)
          (pass-value *trag-web-minimal-input-form*))
    (setf
      (ps:chain target completion-callback) completion-callback
      (ps:chain target completion-options-callback) completion-options-callback
      (ps:chain target callback) callback
      )
    (flet
      ((find-part (x) (select-element x target)))
      (let*
        (
         (input-span (find-part "input-span"))
         (input-tb-span (find-part "input-tb-span"))
         (input-prompt (find-part "input-prompt"))
         (input-textbox (find-part "input-textbox"))
         (input-expand (find-part "input-expand"))
         (input-tb-completion (find-part "input-tb-completion"))
         (input-ta-span (find-part "input-ta-span"))
         (input-textarea (find-part "input-textarea"))
         (input-ok (find-part "input-ok"))
         (input-file (find-part "input-file"))
         (input-help-button (find-part "input-help-button"))
         (input-help-text (find-part "input-help-text"))
         )
        (setf
          (ps:chain target visible-expand)
          (let ((d (ps:chain input-expand style display)))
            (if
              (or
                (null d)
                (equal d "none"))
              "inline" d))
          (ps:chain target hide-expand)
          (save-variables 
            (input-expand)
            (lambda () (setf (ps:chain input-expand style display) "none")))
          (ps:chain target show-expand)
          (save-variables 
            (input-expand)
            (lambda () (setf (ps:chain input-expand style display)
                             (ps:chain target visible-expand))))
          (ps:chain target visible-upload)
          (let ((d (ps:chain input-file style display)))
            (if
              (or
                (null d)
                (equal d "none"))
              "inline" d))
          (ps:chain target hide-upload)
          (save-variables 
            (input-file)
            (lambda () (setf (ps:chain input-file style display) "none")))
          (ps:chain target show-upload)
          (save-variables 
            (input-file)
            (lambda () (setf (ps:chain input-file style display)
                             (ps:chain target visible-upload))))
          (ps:chain target visible-input)
          (let ((d (ps:chain input-textbox style display)))
            (if
              (or
                (null d)
                (equal d "none"))
              "inline" d))
          (ps:chain target hide-input)
          (save-variables 
            (input-file)
            (lambda () (setf (ps:chain input-textbox style display) "none")))
          (ps:chain target show-input)
          (save-variables 
            (input-file)
            (lambda () (setf (ps:chain input-textbox style display)
                             (ps:chain target visible-input))))
          (ps:chain target set-prompt)
          (save-variables (input-prompt)
                          (lambda (x) (show-text input-prompt x)
                            (toggle-visibility
                              input-prompt "inline" :show x)))
          (ps:chain target collapse)
          (save-variables 
            (
             input-textbox input-textarea
             input-ta-span input-tb-span input-tb-completion)
            (lambda ()
              (setf
                (ps:chain input-textbox value) ""
                (ps:chain input-textarea value) ""
                (ps:chain input-file files) null)
              (toggle-visibility input-tb-span "inline" :show t)
              (toggle-visibility input-ta-span "none")
              (toggle-visibility input-tb-completion "none")
              ))
          )
        (when hide-expand (ps:chain target (hide-expand)))
        (when hide-upload (ps:chain target (hide-upload)))
        (when hide-input (ps:chain target (hide-input)))
        (when prompt (ps:chain target (set-prompt prompt)))
        (when help
          (setf (ps:chain input-help-text inner-h-t-m-l) help)
          (ps:chain input-help-button (set-attribute "style" ""))
          (ps:chain input-help-button (set-attribute "tabindex" 0))
          )
        (flet
          ((text-value
             ()
             (or
               (ps:chain input-textarea value)
               (ps:chain input-textbox value))))
          (setf
            (ps:chain input-textbox onkeydown)
            (lambda (event)
              (when (= 13 (ps:chain event key-code))
                (let
                  ((value (text-value)))
                  (when value
                    (ps:chain target (collapse))
                    ((ps:chain target callback) value))))
              (when
                (and
                  (ps:chain event ctrl-key)
                  (not (ps:chain event shift-key))
                  (space-event-p event))
                (cond
                  ((ps:chain target completion-callback)
                   (let*
                     ((now (text-value))
                      (completed
                        ((ps:chain target completion-callback)
                         now input-tb-completion)))
                     (when
                       (and
                         completed
                         (/= now completed))
                       (setf (ps:chain input-textbox value)
                             completed))))
                  ((ps:chain target completion-options-callback)
                   (let*
                     ((now (text-value))
                      (completed
                        (simple-completion
                          now
                          ((ps:chain target completion-options-callback) now)
                          input-tb-completion
                          input-textbox
                          )))
                     (when
                       (and
                         completed
                         (/= now completed))
                       (setf (ps:chain input-textbox value)
                             completed))))
                  ))
              (when
                (or
                  (and
                    (ps:chain event ctrl-key)
                    (ps:chain event shift-key)
                    (space-event-p event))
                  (= 27 (ps:chain event key-code)))
                (toggle-visibility input-tb-completion "none")))
            (ps:chain input-ok onclick)
            (lambda ()
              (let
                ((value (text-value)))
                (ps:chain target (collapse))
                ((ps:chain target callback) value)))
            ))
        (setf (ps:chain input-expand onclick)
              (lambda ()
                (toggle-visibility input-ta-span "inline" :show t)
                (toggle-visibility input-tb-span "none")
                (toggle-visibility input-expand "none")
                (setf
                  (ps:chain input-textarea value)
                  (ps:chain input-textbox value))
                (ps:chain input-textarea (focus))))
        (setf (ps:chain input-file onchange)
              (lambda ()
                (let ((files (ps:chain input-file files)))
                  (ps:chain target (collapse))
                  (use-uploaded-file files (ps:chain target callback)))))
        (setf (ps:chain input-help-button onclick)
              (lambda ()
                (toggle-visibility input-help-text "block"))
              (ps:chain input-help-button onkeydown)
              (lambda (event)
                (when
                  (= (ps:chain event key-code) 13)
                  (toggle-visibility input-help-text "block")))
              (ps:chain input-help-button onmouseenter)
              (lambda () (toggle-visibility input-help-text "block" :show t))
              (ps:chain input-help-button onmouseleave)
              (lambda () (toggle-visibility input-help-text "none"))
              )
        )))
  (defun put-editable-label (target text caption callback)
    (let*
      (
       (header (document-create-element "span"))
       (span (document-create-element "span"))
       (input (document-create-element "input"))
       (ok (document-create-element "button"))
       )
      (setf 
        (ps:chain input type) "text"
        (ps:chain input style display) "none"
        (ps:chain ok style display) "none"
        (ps:chain span onclick)
        (lambda ()
          (setf (ps:chain input value) (ps:chain span inner-text))
          (toggle-visibility span "none")
          (toggle-visibility input "inline" :show t)
          (toggle-visibility ok "inline" :show t))
        (ps:chain header onclick) (ps:chain span onclick)
        (ps:chain ok onclick)
        (lambda ()
          (let
            ((text (ps:chain input value)))
            (setf (ps:chain span inner-h-t-m-l) "")
            (put-text span text)
            (toggle-visibility span "inline" :show t)
            (toggle-visibility input "none")
            (toggle-visibility ok "none")
            (when callback (callback text))))
        (ps:chain input onkeydown)
        (lambda (e)
          (when (equal (ps:chain e key-code) 13)
            (ps:chain ok (click))))
        )
      (put-text header (or caption ""))
      (put-text span text)
      (put-text ok "OK")
      (ps:chain target (append-child header))
      (ps:chain target (append-child span))
      (ps:chain target (append-child input))
      (ps:chain target (append-child ok))
      ))
  (defun menu-submenu-entry (caption target help disabledp)
    (ps:create
      'caption caption
      'help-text (or help nil)
      'disabledp disabledp
      'content (ps:create 'submenu (or target caption))))
  (defun menu-command-entry (caption target help disabledp)
    (ps:create
      'caption caption
      'help-text help
      'disabledp disabledp
      'content (ps:create 'menu-command (or target caption))))
  (defun put-menu (target caption content
                          &key
                          command-list submenu-list disabledp
                          visibility-list
                          top-class top-style extra-click-callback
                          help width min-width
                          (dx "0em") (dy "0em") nobr)
    (let*
      (
       (container (document-create-element "div"))
       (child-container (document-create-element "div"))
       (label (document-create-element "span"))
       (help-label (document-create-element "span"))
       (help-body (document-create-element "div"))
       (effective-disabled-p
         (lambda ()
           (or (and disabledp (disabledp))
               (and content (ps:chain content menu-command)
                    visibility-list 
                    (ps:functionp (aref visibility-list 
                                        (ps:chain content menu-command)))
                    (not 
                      (funcall (aref visibility-list 
                                     (ps:chain content menu-command))))))))
       )
      (setf 
        (ps:chain target style display) "inline"
        (ps:chain target style vertical-align) "top")
      (if (or top-class top-style)
        (setf 
          (ps:chain container class) top-class
          (ps:chain container style) top-style
          )
        (progn
          (setf (ps:chain container style background)
                "rgb(250,250,250)")
          (setf (ps:chain container style padding) "0.5em")
          (setf (ps:chain container style border-color) "black")
          (setf (ps:chain container style border-style) "solid")
          (setf (ps:chain container style border-width) "0.05em")
          (setf (ps:chain container style width) width)
          (setf (ps:chain container style min-width) min-width)
          ))
      (setf (ps:chain container style display) "inline-block")
      (ps:chain target (append-child container))
      (ps:chain label (set-attribute "tabindex" 0))
      (put-text label caption)
      (ps:chain container (append-child label))
      (when help
        (setf (ps:chain help-body inner-h-t-m-l) help)
        (put-text help-label " ⓘ")
        (setf (ps:chain help-body style display) "none")
        (setf (ps:chain help-body style background) "rgb(255,255,200)")
        (setf (ps:chain help-body style padding) "0.5em")
        (setf (ps:chain help-body style position) "absolute")
        (ps:chain container (append-child help-label))
        (ps:chain container (append-child help-body))
        (setf
          (ps:chain help-label onclick)
          (lambda (event)
            (toggle-visibility help-body "block")
            (ps:chain event (stop-propagation)))
          (ps:chain help-label onmouseenter)
          (lambda (event) (toggle-visibility help-body "block" :show t))
          (ps:chain help-label onmouseleave)
          (lambda (event) (toggle-visibility help-body "none")))
        )
      (let*
        ((skip-extra-callback nil)
         (main-onclick
           (cond
             ((ps:stringp content) (lambda () (eval content)))
             ((ps:functionp content) content)
             ((and
                (ps:objectp content)
                (ps:chain content menu-command))
              (aref command-list (ps:chain content menu-command)))
             ((and
                (ps:objectp content)
                (ps:chain content submenu))
              (put-text container " →")
              (put-empty-element container :br)
              (setf (ps:chain child-container style display) "none"
                    (ps:chain child-container style position) "absolute"
                    (ps:chain child-container style margin-left) dx
                    (ps:chain child-container style margin-top) dy
                    (ps:chain target onmouseleave)
                    (lambda ()
                      (toggle-visibility child-container "none")
                      (toggle-visibility help-body "none")
                      )
                    )
              (ps:chain container (append-child child-container))
              (loop
                for e in (aref submenu-list (ps:chain content submenu))
                for submenu-caption := (ps:chain e caption)
                for submenu-content := (ps:chain e content)
                for submenu-help := (ps:chain e help-text)
                for submenu-container := (document-create-element "span")
                for submenu-disabled-p := (ps:chain e disabledp)
                do (put-menu
                     submenu-container submenu-caption submenu-content
                     :submenu-list submenu-list
                     :command-list command-list
                     :visibility-list visibility-list
                     :top-class top-class :top-style top-style
                     :help submenu-help
                     :width "100%"
                     :min-width min-width
                     :disabledp submenu-disabled-p
                     :extra-click-callback
                     (lambda () 
                       (toggle-visibility child-container "none")
                       (toggle-visibility help-body "none")
                       (when extra-click-callback (extra-click-callback))))
                do (ps:chain child-container (append-child submenu-container))
                do (unless nobr (put-empty-element child-container :br))
                )
              (setf skip-extra-callback t)
              (save-variables
                (child-container)
                (lambda ()
                  (toggle-visibility child-container "block")
                  (toggle-visibility help-body "none")
                  (loop for x in (ps:chain child-container child-nodes)
                        when (ps:chain x recalculate-disabled)
                        do (ps:chain x (recalculate-disabled)))))))))
        (setf (ps:chain container onclick)
              (save-variables
                (main-onclick skip-extra-callback)
                (lambda (event)
                  (unless
                    (effective-disabled-p)
                    (unless skip-extra-callback
                      (when extra-click-callback (extra-click-callback)))
                    (when main-onclick (main-onclick))
                    (ps:chain event (stop-propagation)))))
              (ps:chain target recalculate-disabled)
              (lambda ()
                (if (effective-disabled-p)
                  (setf (ps:chain container style color) "gray")
                  (setf (ps:chain container style color) "black")))
              (ps:chain label onkeydown)
              (save-variables
                (label)
                (lambda (event)
                  (when
                    (= (ps:chain event key-code) 13)
                    (ps:chain container (click)))))))
      target))
  (defun make-pane (caption &key pane-class pane-style)
    (let*
      ((pane (document-create-element :div)))
      (if pane-style
        (setf (ps:chain pane style) pane-style)
        (setf
          (ps:chain pane style min-width) "20em"
          (ps:chain pane style width) "45%"
          (ps:chain pane style display) "inline-block"
          (ps:chain pane style background) "rgb(250,250,250)"
          (ps:chain pane style margin) "0.5em"
          (ps:chain pane style vertical-align) "top"
          ))
      (when pane-class
        (setf (ps:chain pane class) pane-class))
      (setf (ps:chain pane header) (put-empty-element pane :div))
      (setf (ps:chain pane header style width) "100%")
      (show-text (ps:chain pane header) caption)
      (setf (ps:chain pane content) (put-empty-element pane :div))
      pane))
  (defun click-hidable (element)
    (setf (ps:chain element onclick)
          (lambda () (toggle-visibility element "none"))))
  (defun 
    evaluate-with-arguments
    (callback initial-arguments argument-descriptions argument-request-container)
    (if (= 0 (length argument-descriptions))
      (progn
        (show-text argument-request-container nil)
        (apply callback initial-arguments))
      (progn
        (let*
          ((first-argument (elt argument-descriptions 0))
           (remaining-arguments (ps:chain argument-descriptions (slice 1))))
          (put-minimal-input-form
            argument-request-container
            (lambda (value)
              (evaluate-with-arguments
                callback
                (append initial-arguments (list value))
                remaining-arguments argument-request-container))
            :completion-options-callback
            (if (ps:chain first-argument
                          (has-own-property 'completion-options))
              (lambda () (ps:chain first-argument completion-options))
              (lambda () (list)))
            :hide-expand (not (ps:chain first-argument allow-expand))
            :hide-upload (not (ps:chain first-argument allow-upload))
            :hide-input (and
                          (ps:chain first-argument (has-own-property 'hide-input))
                          (ps:chain first-argument hide-input))
            :prompt (or (ps:chain first-argument prompt) "Enter a value: ")
            :help (if (ps:chain first-argument (has-own-property 'help))
                    (ps:chain first-argument help) null)
            )
          (toggle-visibility argument-request-container "block" :show t)
          (ps:chain
            (select-element :input-textbox argument-request-container)
            (focus)))
        )
      ))
  (defun maybe-error (x)
    (when (= (ps:chain x status) :error)
      (let*
        ((error-pane (document-create-element "div")))
        (setf
          (ps:chain error-pane style background-color) "rgb(255,127,127)"
          (ps:chain error-pane class) :error-holder
          (ps:chain error-pane onclick) (lambda () (kill-element error-pane))
          )
        (show-text error-pane (ps:chain x error))
        (insert-as-first error-pane (ps:chain document body))
        t)))

  )

(defun empty-html (&key script body-onload css-url head-code noscript-message)
  (format
    nil
    "<html><head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ~a
     <link rel=\"stylesheet\" type=\"text/css\" href=\"~a\">
     <script type=\"text/javascript\">~a</script></head>
     <body onload=\"~a\"><noscript>~a</noscript></body></html>"
    head-code css-url script body-onload noscript-message))
