(asdf:make :trag-web)

(assert
  (equal
    (trag-web:evaluate-identifier
      "{
      \"type\":\"string\", \"operation\":\"concatenate\",
      \"parameters\":[
                      {\"type\":\"string\",\"immediate-data\":\"asd\"},
                      {\"type\":\"string\",\"immediate-data\":\"qwe\"}
                      ]}")
      "asdqwe"))
