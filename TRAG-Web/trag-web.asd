;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin, Irène Durand

(asdf:defsystem
    :trag-web
  :description "General TRAG Web UI utilities"
  :version "0.0"
  :author "Michael Raskin <raskin@mccme.ru>, Irène Durand <irene.durand@u-bordeaux.fr>"
  :depends-on (:general :input-output :object
               :bordeaux-threads :cl-json :smackjack :parenscript :alexandria
			:hunchentoot :local-time :cl-ppcre :cl-emb)
  :components
  ((:file "package")
   (:file "object-identifier" :depends-on ("package"))
   (:file "remote-session" :depends-on ("package"))
   (:file "ajax-utils" :depends-on ("package" "object-identifier"))
   (:file "parenscript-utils" :depends-on ("package"))
   (:file "render-utils" :depends-on ("parenscript-utils" "package"))
   (:file "web-session-log" :depends-on ("package" "object-identifier"))
   (:file "web-session" :depends-on ("package" "remote-session" 
                                     "object-identifier" "web-session-log"))
   (:file "web-panes" :depends-on
          ("package" "web-session" "render-utils" "parenscript-utils"
           "ajax-utils" "object-identifier"))
   (:file "web-demo" :depends-on
          ("package" "web-session" "web-panes" "render-utils"
           "parenscript-utils" "ajax-utils" "object-identifier"))))
