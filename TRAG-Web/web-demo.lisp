(defpackage :trag-web-demo
  (:use :common-lisp :trag-web)
  (:export #:define-trag-web-demo))

(in-package :trag-web-demo)

(defpackage :trag-web-demo-ids
  (:use)
  (:import-from :keyword :integer :string)
  (:import-from :trag-web-ids
                #:concatenate #:sum #:repeat-string-times))

(defpackage :trag-web-demo-ajax (:use :trag-web-ajax-handlers))

(defmethod evaluate-identifier-function 
  ((operation (eql 'trag-web-demo-ids::string-length))
   (type (eql 'trag-web-demo-ids::integer))
   parameters)
  (destructuring-bind
    (str) parameters
    (assert (stringp str))
    (length str)))

(add-parenscript-definitions
  (*trag-web-demo-js* t)
  (defcommand
    "Input the integer"
    ((value
       'prompt "The new integer: "
       'help "The integer that will become the new current integer"))
    (set-pane-current
      "integer" (create-immediate-identifier :integer value)))
  (defcommand
      "Command1" ()
    (set-pane-current "result" (create-immediate-identifier :string "Hello")))
  (defcommand
      "Command2"
      ((my-value 'prompt "my-value" 'help "anything will become a string"))
    (set-pane-current "string" (create-immediate-identifier :string my-value)))
  (setf
    (aref *known-commands* "Cancel pending computation")
    (lambda () (server-funcall
                 :session-cancel-request (json-args)
                 (lambda ()
                   (default-state)
                   (show-text *message-holder* "Succesfully cancelled")
                   )))
    (aref *known-commands* "Reset session")
    (lambda () (server-funcall
                 :session-destroy (json-args)
                 (lambda ()
                   (default-state)
                   (load-session-values)
                   (show-text *message-holder* "Succesfully cleared")
                   )))
    (aref *known-commands* "Input the string")
    (lambda ()
      (default-state)
      (evaluate-with-arguments
        (lambda (value)
          (default-state)
          (set-current
            "string" 
            (ps:chain *current-string-pane* content)
            (create-immediate-identifier :string value))
          (default-state))
        (list)
        (list
          (ps:create
            'prompt "Enter the string: "
            'help "The string that will become the new current string"
            'completion-options nil
            'allow-expand nil
            'allow-upload nil
            ))
        *argument-request-holder*))
    (aref *known-commands* "Repeat the string")
    (lambda ()
      (default-state)
      (evaluate-with-arguments
        (lambda (str cnt)
          (default-state)
          (set-current 
            "string" 
            (ps:chain *current-string-pane* content)
            (create-operation-identifier
              :string
              :repeat-string-times
              (list str cnt))))
        (list
          (ps:chain *current-string-pane* content object)
          (ps:chain *current-integer-pane* content object)
          ) (list) *argument-request-holder*))
    (aref *known-commands* "String length")
    (lambda ()
      (default-state)
      (evaluate-with-arguments
        (lambda (str)
          (default-state)
          (set-current
            "integer" (ps:chain *current-integer-pane* content)
            (create-operation-identifier
              :integer :string-length (list str))))
        (list
          (ps:chain *current-string-pane* content object)
          )
        (list) *argument-request-holder*))
    (aref *known-commands* "Name the integer")
    (lambda ()
      (default-state)
      (evaluate-with-arguments
        (lambda (name)
          (default-state)
          (set-name "integer" *current-integer-pane* name))
        (list)
        (list
          (ps:create
            'prompt "Name for the current integer"
            'allow-expand nil
            'allow-upload nil
            'help nil
            'completion-options nil
            ))
        *argument-request-holder*
        ))
    (aref *known-commands* "Use a named integer")
    (lambda ()
      (default-state)
      (evaluate-with-arguments
        (lambda (name)
          (default-state)
          (set-current
            "integer" (ps:chain *current-integer-pane* content)
            (aref (aref *named-objects* "integer") name) name))
        (list)
        (list
          (ps:create
            'prompt "Name to retrieve"
            'allow-expand nil
            'allow-upload nil
            'help nil
            'completion-options
            (lambda () 
              (and
                (aref *named-objects* "integer")
                (object-keys (aref *named-objects* "integer"))))
            ))
        *argument-request-holder*
        ))
    )
  (defmenu "Session"
           (menu-command-entry "Cancel pending computation" "Cancel pending computation")
           (menu-command-entry "Reset" "Reset session"))
  (defmenu "Irene"
      (menu-command-entry "Item1" "Command1")
      (menu-command-entry "Item2" "Command2"))
  (setf
    (aref *known-menus* "Data")
    (list
      (menu-submenu-entry "Enter" "Data entry")
      (menu-submenu-entry "Process" "Data processing")
      (menu-submenu-entry "Combine" "Data combination"))
    (aref *known-menus* "Data entry")
    (list
      (menu-command-entry "Integer" "Input the integer")
      (menu-command-entry "String" "Input the string"))
    (aref *known-menus* "Data processing")
    (list 
      (menu-command-entry "String length" "String length"))
    (aref *known-menus* "Data combination")
    (list
      (menu-command-entry "Repeat the string" "Repeat the string"))
    (aref *known-menus* "Named data")
    (list 
      (menu-submenu-entry "Set name" "Name setting")
      (menu-submenu-entry "Retrieve by name" "Name retrieval"))
    (aref *known-menus* "Name setting")
    (list
      (menu-command-entry "Name the integer" "Name the integer"))
    (aref *known-menus* "Name retrieval")
    (list
      (menu-command-entry "Use a named integer" "Use a named integer")))
  (setf *top-menus*
        (list
          (menu-submenu-entry "Data" "Data")
          (menu-submenu-entry "Named data" "Named data")
          (menu-submenu-entry "Session" "Session")
          (menu-submenu-entry "IRENE" "Irene")))

  (define-pane "integer" "Current integer:")
  (define-pane "string" "Current string:")
  (define-pane "result" "Latest result:")

  (defun load-session-values ()
    (chain-postfix-callbacks
      (list 
        (ps:create
          :func get-current
          :args (list "integer" (ps:chain *current-integer-pane* content)))
        (ps:create
          :func get-current
          :args (list "string" (ps:chain *current-string-pane* content))))))
  
  (defun initialize ()
    (default-state)))

;; this macro has to be called before launching the server
(defmacro define-trag-web-demo ()
  `(define-pane-page
     :uri trag-web::*uri*
     :ajax-package :trag-web-demo-ajax
     :object-identifier-package :trag-web-demo-ids
     :parenscript-code *trag-web-demo-js*))

