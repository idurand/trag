(in-package :trag-web)

(ps:defpsmacro defcommand (name parameters &rest body)
  `(setf
     (aref *known-commands* ,name)
     (lambda ()
       (default-state)
       (evaluate-with-arguments
         (lambda (,@(mapcar 'first parameters))
           (default-state)
           ,@body)
         (list)
         (list
           ,@(mapcar (lambda (x) `(ps:create ,@(cdr x))) parameters))
         *argument-request-holder*))))

(defun lisp-name-to-js-name (lisp-name)
  (cl-ppcre:regex-replace-all
    "(^| )(.)"
    (cl-ppcre:regex-replace-all "-" (string-downcase lisp-name) " ")
    (list
      0
      (lambda
        (target start end match-start match-end reg-starts reg-ends)
        (declare (ignorable start end match-start match-end reg-starts reg-ends))
        (string-upcase (elt target (elt reg-starts 1)))))))

(defmacro defcommand-with-backend
  ((lisp-name type parenscript-var &key js-name
              (creator-function 'create-op-id))
   ui-parameters lisp-parameters enabledp lisp-form js-form)
  (let* ((type-var (gensym))
         (op-var (gensym))
         (param-var (gensym))
         (js-name (or js-name
                      (lisp-name-to-js-name lisp-name))))
    `(progn
       (defmethod evaluate-identifier-function
         ((,op-var (eql ',lisp-name)) (,type-var (eql ',type)) ,param-var)
         (declare (ignorable ,op-var ,type-var))
         (destructuring-bind
           ,(append lisp-parameters (mapcar 'first ui-parameters)) ,param-var
           ,lisp-form))
       (add-parenscript-definitions
         (,parenscript-var)
         (defcommand ,js-name ,ui-parameters
                     (flet ((,creator-function (&rest args)
                                               (create-operation-identifier
                                                 ,(intern (symbol-name type) :keyword)
                                                 ,(intern (symbol-name lisp-name) :keyword)
                                                 args)))
                       ,js-form))
         (setf (aref *command-visibility* ,js-name)
               (lambda () ,enabledp))))))

(defmacro defdownload-with-backend
  ((lisp-name file-name parenscript-var &key
              js-name (creator-function 'create-op-id)
              file-ext file-type)
   direct-arguments condition lisp-code js-code)
  `(defcommand-with-backend
     (,lisp-name :string ,parenscript-var
                 :js-name ,js-name
                 :creator-function ,creator-function)
     () ,direct-arguments
     ,condition ,lisp-code
     (server-funcall
       :session-describe-identifier
       (json-args ,js-code)
       (lambda (x)
         (download-data
           (ps:aref
             (ps:chain -j-s-o-n (parse (ps:chain x canonical-id)))
             :immediate-data)
           (date-file-name ,file-name ,(or file-ext "txt"))
           ,(or file-type "text/plain; charset=utf-8"))))))

(ps:defpsmacro defmenu (name &rest content)
  `(setf (aref *known-menus* ,name) (list ,@ content)))

(ps:defpsmacro
  define-pane-naming
  (kind pane &optional (prompt-name (cl-ppcre:regex-replace-all "-" kind " ")))
  `(progn
     (defcommand (+ "Name the " ,prompt-name)
                 ((name 'prompt (+ "The name for the " ,prompt-name ": ")))
                 (set-name ,kind ,pane name))
     (defcommand (+ "Use a named " ,prompt-name)
                 ((name 'prompt (+ "The name of the " ,prompt-name ": ")
                        'completion-options (kind-names ,kind)))
                 (set-pane-current ,kind (get-named ,kind name) nil name))
     (defmenu (+ "Named " ,prompt-name)
              (menu-command-entry
                (+ "Set name for the current " ,prompt-name)
                (+ "Name the " ,prompt-name))
              (menu-command-entry
                (+ "Retrieve a " ,prompt-name " by name")
                (+ "Use a named " ,prompt-name)))))

(ps:defpsmacro define-pane (kind header &key skip prompt-name)
   (let* ((pane-var (intern (concatenate 'string 
                               "*CURRENT-" (string-upcase kind) "-PANE*"))))
     `(progn
        (defvar ,pane-var)
        (setf ,pane-var (make-pane ,header))
        (setf (aref *known-panes* ,kind) ,pane-var)
        (define-pane-naming ,kind ,pane-var 
           ,(or prompt-name (cl-ppcre:regex-replace-all "-" kind " ")))
        ,(unless skip
           `(progn(ps:chain *top-panes* (push ,pane-var))
              (ps:chain *top-pane-kinds* (push ,kind)))))))

(defmacro define-pane-page
  (&key
    (uri (error "URI must be defined"))
    (parenscript-code
      (error "Variable for Parenscript code must be specified"))
    (ajax-package
      (error "Package for functions accessible via AJAX must be specified"))
    (object-identifier-package
      (error "Package for object identifiers must be specified"))
    (remote-session-table *session-table*)
    (uri-handler-sym (gensym))
    (ajax-function-name 
      (make-symbol
        (string-upcase
          (format nil "_server_funcall_~a"
                  (cl-ppcre:regex-replace-all
                    "[^a-zA-Z0-9]" (package-name ajax-package) "_")))))
    (noscript-message
      "This is a site with no static content and a Javascript-based calculation UI")
    (css-url "/site.css")
    (page-bottom-message)
    (page-bottom-version)
    (command-line-help)
    (web-log-pane))
  (let* ((default-js-code-var (gensym)))
    `(progn
       (define-ajax-rpc ,ajax-function-name ,ajax-package
                        :wrapper-code
                        (let* ((*trag-web-id-package*
                                 ,object-identifier-package)
                               (*session-table* ,remote-session-table))
                          (declare (special *trag-web-id-package*))))
       (let*
         ((,default-js-code-var
            '((defvar *known-panes* (ps:create))
              (defvar *top-panes* (list))
              (defvar *top-pane-kinds* (list))
              (defvar *known-commands* (ps:create))
              (defvar *command-visibility* (ps:create))
              (defvar *known-menus* (ps:create))
              (defvar *named-objects* {})
              (defvar *top-menus* (list))
              (defvar *main-command-line*)
              (defvar *argument-request-holder*)
              (defvar *main-menu-holder*)
              (defvar *message-holder*)
              (defvar *busy-holder*)
              (defvar *error-holder*)
              (defun kind-names (kind)
                (and (aref *named-objects* kind)
                     (object-keys (aref *named-objects* kind))))
              (defun define-pane (kind header &key skip prompt-name)
                (let* ((pane (make-pane header)))
                  (setf (aref *known-panes* kind) pane)
                  (define-pane-naming kind pane
                    (or prompt-name (cl-ppcre:regex-replace-all "-" kind " ")))
                  (unless skip
                    (ps:chain *top-panes* (push pane))
                    (ps:chain *top-pane-kinds* (push kind)))))
              (defvar *log-requests* nil)
              (defcommand "Toggle request logging" ()
                          (setf *log-requests* (not *log-requests*)))
              ,@(when web-log-pane
                  `((defun request-log-entry-selected (x))
                    (defcommand "Show request statistics"
                                ((mask 'prompt "Period mask: "))
                                (set-pane-current
                                  ,web-log-pane
                                  (create-operation-identifier
                                    :web-request-log
                                    :web-request-log
                                    (list
                                      (create-immediate-identifier
                                        :string mask)))))))
              (defun server-funcall (func args callback)
                (show-text *busy-holder* "Asking the server…")
                (when *log-requests*
                  (console-log (list func (ps:chain -j-s-o-n (parse args)))))
                (,ajax-function-name
                  func args
                  (lambda (&rest callback-args)
                    (toggle-visibility *busy-holder* "none")
                    (apply callback callback-args))
                  (lambda (request)
                    (show-text
                      *error-holder*
                      (+ "Server request error. "
                         (ps:chain request status)
                         "; "
                         (ps:chain request status-text))))))
              (defun default-init ()
                (setf *main-command-line* (document-create-element "div"))
                (setf *main-menu-holder* (document-create-element "div"))
                (setf *argument-request-holder* (document-create-element :div))
                (setf *message-holder* (document-create-element :div))
                (setf *busy-holder* (document-create-element :div))
                (setf *error-holder* (document-create-element :div))
                (click-hidable *message-holder*)
                (click-hidable *busy-holder*)
                (click-hidable *error-holder*)
                (setf (ps:chain *error-holder* style background)
                      "rgb(255,200,200)")
                (put-minimal-input-form
                  *main-command-line*
                  (lambda (name)
                    (when (aref *known-commands* name)
                      (funcall (aref *known-commands* name))))
                  :hide-upload t
                  :hide-expand t
                  :prompt "Enter a command"
                  :help ,command-line-help
                  :completion-options-callback
                  (lambda ()
                    (loop
                      for name in (object-keys *known-commands*)
                      for enabledp := (aref *command-visibility* name)
                      unless (and enabledp (ps:functionp enabledp) (not (enabledp)))
                      collect name)))
                (ps:chain document body
                          (append-child *main-menu-holder*))
                (ps:chain document body
                          (append-child *main-command-line*))
                (ps:chain document body
                          (append-child *message-holder*))
                (ps:chain document body
                          (append-child *busy-holder*))
                (ps:chain document body
                          (append-child *error-holder*))
                (ps:chain document body
                          (append-child *argument-request-holder*))
                (setf (ps:chain document body onclick)
                      (lambda ()
                        (ps:chain document document-element
                                  (query-selector-all ".input-tb-completion")
                                  (for-each
                                    (lambda (c) (toggle-visibility c "none"))))))
                (server-funcall 
                  :session-retrieve-string
                  (json-args (list :named-objects))
                  (lambda (x)
                    (when x
                      (setf *named-objects*
                            (ps:chain -j-s-o-n (parse x))))
                    (load-session-top-panes))))
              (defun maybe-error (x)
                (when
                  (= (ps:chain x status) :error)
                  (show-text *error-holder* (ps:chain x error))
                  t))
              (defun default-state ()
                (setf
                  (ps:chain *argument-request-holder* inner-h-t-m-l) ""
                  (ps:chain
                    (select-element :input-textbox *main-command-line*)
                    value) "")
                (toggle-visibility
                  (select-element :input-tb-completion *main-command-line*)
                  "none")
                (toggle-visibility *message-holder* "none")
                (toggle-visibility *busy-holder* "none")
                (toggle-visibility *error-holder* "none")
                (ps:chain
                  (select-element :input-textbox *main-command-line*)
                  (focus)))
              (defun set-pane-current (kind id then-do name)
                (set-current
                  kind (ps:chain (aref *known-panes* kind) content) id
                  then-do name))
              (defun get-pane-current (kind)
                (or
                  (ps:chain (aref *known-panes* kind) content object)
                  nil))
              (defun get-pane-current-type (kind)
                (ps:chain (or (get-pane-current kind) (ps:create :type nil))
                          type))
              (defun get-pane-current-attribute (kind attribute)
                (aref (ps:chain (aref *known-panes* kind) content extra-attributes)
                      attribute))
              (defun set-name (kind pane name then-do)
                (setf 
                  (aref *named-objects* kind)
                  (or (aref *named-objects* kind)
                      (ps:create)))
                (setf (aref (aref *named-objects* kind) name)
                      (ps:chain pane content object))
                (server-funcall
                  :session-store-string
                  (json-args (list :named-objects) 
                             (ps:chain -j-s-o-n (stringify *named-objects*)))
                  (lambda (x)
                    (unless
                      (maybe-error x)
                      (set-current 
                        kind (ps:chain pane content)
                        (ps:chain pane content object)
                        then-do
                        name)))))
              (defun get-named (kind name)
                (and
                  (aref *named-objects* kind)
                  (aref (aref *named-objects* kind) name)))
              (defun load-session-top-panes (&optional then-do)
                (chain-postfix-callbacks
                  (append
                    (loop for kind in *top-pane-kinds*
                          for pane in *top-panes*
                          collect
                          (ps:create
                            :func get-current
                            :args (list kind (ps:chain pane content))))
                    (if then-do
                      (list (ps:create
                              :func (lambda () (funcall then-do))
                              :args (list)))
                      (list)))))
              (defun session-export ()
                (let* ((a (document-create-element :a))
                       (current-values (ps:create))
                       (final-data
                         (progn
                           (loop for kind in *top-pane-kinds* do
                                 (setf (aref current-values kind)
                                       (get-pane-current kind)))
                           (ps:create 'current current-values
                                      'named *named-objects*)))
                       (json-string (ps:chain -j-s-o-n (stringify final-data)))
                       (date (ps:chain (ps:new (-date))
                                       (to-i-s-o-string) 
                                       (replace (-reg-exp ":" "g") "-")
                                       (replace (-reg-exp "[.].*") "")))
                       (file-name (+ "autograph-web-session." date ".json")))
                  (save-as a json-string file-name "text/json; charset=utf-8")
                  (setf (ps:chain a style display) "none")
                  (ps:chain document body (append-child a))
                  (set-timeout (lambda () (ps:chain a (click))
                                 (kill-element a)) 0)))
              (defun session-import (data)
                (let* ((session-data (ps:chain -j-s-o-n (parse data)))
                       (current-values (ps:chain session-data current)))
                  (setf *named-objects* (ps:chain session-data named))
                  (loop for kind in (object-keys current-values) do
                        (setf (ps:chain (aref *known-panes* kind)
                                        content object)
                              (aref current-values kind)))
                  (resend-session-to-server
                    (lambda () (load-session-top-panes)))))
              (defun resend-session-to-server (&optional then-do)
                (chain-postfix-callbacks
                  (append
                    (loop for kind in *top-pane-kinds* collect
                          (ps:create :func server-funcall
                                     :args (list :session-set-current
                                                 (json-args 
                                                   kind
                                                   (get-pane-current kind)
                                                   nil))))
                    (list (ps:create
                            :func server-funcall
                            :args
                            (list :session-store-string
                                  (json-args
                                    (list :named-objects)
                                    (ps:chain -j-s-o-n
                                              (stringify *named-objects*))))))
                    (if then-do
                      (list (ps:create :func then-do :args (list)))
                      (list)))))
              (defun default-post-init ()
                (loop 
                  for m in *top-menus*
                  do 
                  (put-menu
                    (ps:chain
                      *main-menu-holder*
                      (append-child (document-create-element "span")))
                    (ps:chain m caption)
                    (ps:chain m content)
                    :min-width "10em"
                    :help (ps:chain m help-text)
                    :command-list *known-commands*
                    :submenu-list *known-menus*
                    :visibility-list *command-visibility*))
                (loop for p in *top-panes* do
                      (ps:chain document body (append-child p)))
                (let ((bottom-div (document-create-element :div)))
                  (setf (ps:chain bottom-div inner-h-t-m-l)
                        ,page-bottom-message)
                  ,@(when page-bottom-version
                      `((put-text bottom-div "Version with changes from: ")
                        (put-text bottom-div
                                  ,(or
                                     (ignore-errors
                                       (uiop:run-program
                                         (format
                                           nil
                                           "cd ~s;
                                           git log -n1 --format=format:%ad --date=format:%Y-%m-%d"
                                           (source-directory))
                                         :output :string))
                                     "Unknown"))))
                  (ps:chain document body
                            (append-child bottom-div)))
                (default-state))
              (defun initialize ()))))
         (hunchentoot:define-easy-handler
           (,uri-handler-sym :uri ,uri) ()
           (empty-html
             :script
             (concatenate
               'string
               (ps:ps* (cons 'progn *parenscript-trag-web-page-definitions*))
               (ps:ps* (cons 'progn *trag-web-js-render-helpers*))
               (ps:ps* (cons 'progn *trag-web-session-js-helpers*))
               (ps:ps* (cons 'progn ,default-js-code-var))
               (ps:ps* (cons 'progn ,parenscript-code)))
             :head-code (smackjack:generate-prologue *ajax-processor*)
             :body-onload "defaultInit(); initialize(); defaultPostInit();"
             :css-url ,css-url
             :noscript-message ,noscript-message))))))
