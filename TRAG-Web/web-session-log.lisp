(in-package :trag-web)

(defparameter *trag-web-log*
  (when (uiop:getenv "HOME")
    (format nil "~a/.trag-web-log/" (uiop:getenv "HOME"))))

(defun log-identifier (id)
  (when *trag-web-log*
    (multiple-value-bind
      (second minute hour day month year dow dst tz)
      (decode-universal-time (get-universal-time))
      (declare (ignorable dow))
      (let* ((timestamp (format nil
                                "~4,'0d-~2,'0d-~2,'0d/~2,'0d-~2,'0d-~2,'0d"
                                year month day
                                (+ hour tz (if dst -1 0))
                                minute second))
             (target (format nil "~a/~a/~a/"
                             *trag-web-log*
                             (package-name *trag-web-id-package*) timestamp)))
        (ensure-directories-exist target)
        (uiop:with-temporary-file
          (:stream stream :directory target :keep t)
          (format stream "~a" (object-identifier-to-json id)))))))

(defun log-files (full-mask)
  (loop for p in (directory full-mask)
        for n := (namestring p)
        for dirp := (cl-ppcre:scan "/$" n)
        for subfm := (format nil "~a/*.*" n)
        for subdm := (format nil "~a/*.*/" n)
        for subf := (directory subfm)
        for subd := (directory subdm)
        for need-subd := (and subd (not (find (first subd) subf :test 'equal)))
        when subf append (log-files subfm)
        when need-subd append (log-files subdm)
        unless dirp collect n))

(defun web-log-summary (mask)
  (assert (cl-ppcre:scan "^[-/*A-Z0-9a-z]*$" mask))
  (when *trag-web-log*
    (loop with ht := (make-hash-table :test 'equal)
          with res := nil
          with maskn := (if (stringp mask) mask (namestring mask))
          with absolute-mask := (if (cl-ppcre:scan "^/" maskn)
                                  maskn
                                  (format nil "~a/~a" *trag-web-log* maskn))
          for fn in (log-files absolute-mask)
          for id := (alexandria:read-file-into-string fn)
          do (incf (gethash id ht 0))
          finally
          (progn
            (maphash (lambda (k v) (push (list v (length k) k) res)) ht)
            (return (mapcar
                      (lambda (x) (list (first x) (third x)))
                      (reverse (sort res #'object:strictly-ordered-p))))))))

(define-object-identifier-type (trag-web-ids::web-request-log)
  (:check (value) (and (listp value)
                       (every 'listp value)
                       (every (lambda (x) (and (integerp (first x))
                                               (stringp (second x))))
                              value)))
  (:parse (string) (web-log-summary
                     (format nil "~a/~a"
                             (package-name *trag-web-id-package*) string)))
  (:description (value) (format nil "~{~a~%~}" (mapcar 'second value)))
  (:canonical-id (value) nil)
  (:html-description (value)
                     (format
                       nil
                       "~{
                       ~{
                       <span onclick='requestLogEntrySelected(~
                            decodeURIComponent(\"~a\"))'>
                       ~a
                       </span><br/>
                       (×~a)
                       <br/><br/>
                       ~}
                       ~}"
                       (loop for x in value
                             collect
                             (list
                               (cl-emb::escape-for-xml 
                                 (cl-emb::url-encode
                                   (second x)))
                               (cl-emb::escape-for-xml (second x))
                               (first x))))))

(define-object-identifier-operations (trag-web-ids::web-request-log)
  (trag-web-ids::web-request-log (mask)
    (web-log-summary (format nil "~a/~a" (package-name *trag-web-id-package*)
                             mask))))

(defmethod object-identifier-operation-destructive-p
  ((operation (eql 'trag-web-ids::web-request-log))
   (type (eql 'trag-web-ids::web-request-log)))
  t)
