(in-package :trag-web)

(defclass web-session-result ()
  ((status
     :initarg :status
     :initform (error "Status must be specified")
     :type (or (eql :success) (eql :error))
     :accessor web-session-result-status
     :documentation "General request status")
   (error
     :initarg :error
     :initform nil
     :accessor web-session-result-error
     :documentation "The description of an error in processing request")
   (type
     :initarg :type
     :initform nil
     :accessor web-session-result-type
     :documentation "The result type")
   (id
     :initarg :id
     :initform nil
     :accessor web-session-result-id
     :documentation "An identifier of the object")
   (canonical-id
     :initarg :canonical-id
     :initform nil
     :accessor web-session-result-canonical-id
     :documentation "The canonical identifier of the object")
   (description
     :initarg :description
     :initform nil
     :accessor web-session-result-description
     :documentation "A text description of the object")
   (html-description
     :initarg :html-description
     :initform nil
     :accessor web-session-result-html-description
     :documentation "An HTML description of the object")
   (time
     :initarg :time
     :initform nil
     :accessor web-session-result-time
     :documentation "Time used to compute the object")
   (extra-attributes
     :initarg :extra-attributes
     :initform nil
     :accessor web-session-result-extra-attributes
     :documentation "A key-value structure of extra object attributes")))

(defun make-web-session-result (&rest args &key status error id canonical-id
                                      type description html-description
                                      extra-attributes time)
  (declare (ignorable status error type id canonical-id
                      description html-description extra-attributes time))
  (apply 'make-instance 'web-session-result args))

(defmethod cl-json:encode-json ((result web-session-result) 
                                &optional (stream cl-json:*json-output*))
  (cl-json:encode-json
    (loop for slot in '(status error type id canonical-id description
                               html-description extra-attributes time)
          collect (object-slot-json-entry result slot :for-js-only t))
    stream))

(defun get-cookie-session ()
  (let ((session-id (ignore-errors (hunchentoot:cookie-in "session_id"))))
    (and session-id (get-session session-id))))

(defun get-current-session (&optional session)
  (or session (get-cookie-session)
      (let ((new-session (make-session)))
        (hunchentoot:set-cookie "session_id" :value (key new-session))
        new-session)))

(defun cached-session-eval (key thunk &key session names)
  (let* ((top-destructivep nil)
         (result
           (evaluate-in-session
             (get-current-session session)
             (lambda (session)
               (or (with-locked-session session
                                        (get-session-entry key session))
                   (multiple-value-bind (result destructivep) (funcall thunk)
                     (if destructivep
                       (setf top-destructivep t)
                       (locked-set-session-entry key result session))
                     result)))
             names)))
    (values result top-destructivep)))

(defun session-eval-identifier (id &key session names)
  (cached-session-eval
    (object-identifier-to-json id)
    (lambda ()
      (evaluate-identifier
        id :parameter-lookup
        (lambda (p)
          (let*
            ((p (object-identifier-to-json p)))
            (with-locked-session session (get-session-entry p session))))
        :parameter-evaluation-hook
        (lambda (id value destructivep)
          (unless destructivep
            (let* ((id (object-identifier-to-json id)))
              (locked-set-session-entry id value session))))))
    :session session :names names))

(defun handle-session-evaluation-results (results continuation)
  (if (every 'session-evaluation-result-successful-p results)
    (apply continuation (mapcar 'value results))
    (make-web-session-result
      :status :error :error
      (session-evaluation-result-error-text
        (find-if-not 'session-evaluation-result-successful-p results)))))

(defun handle-session-evaluation-result (result continuation)
  (handle-session-evaluation-results (list result) continuation))

(defun session-describe-identifier (id &key session name)
  (let ((session (get-current-session session)))
    (multiple-value-bind (result destructivep)
      (session-eval-identifier id :session session)
      (let ((time (evaluation-time result)))
        (handle-session-evaluation-result result
          (lambda (value)
            (let* ((type (object-identifier-type
                           (object-identifier-from-json id)))
                   (canonical-id
                     (or (ignore-errors (canonical-identifier type value)) id))
                   (canonical-id-string
                     (object-identifier-to-json canonical-id)))
              (unless destructivep
                (locked-set-session-entry
                  (object-identifier-to-json id) value session)
                (locked-set-session-entry canonical-id value session)
                (locked-set-session-entry canonical-id-string value session))
              (when name
                (when
                  (typecase value
                    (object:named-mixin (object:rename-object value name) t))
                  (with-locked-session session
                    (remhash (list :description canonical-id-string)
                             (entries session))
                    (remhash (list :html-description canonical-id-string)
                             (entries session)))))
              (handle-session-evaluation-results
                (list
                  (if destructivep
                    (calculate-session-evaluation-result
                      (lambda (&rest args) args
                        (canonical-description type value))
                      session *trag-web-id-package*)
                    (cached-session-eval
                      (list :description canonical-id-string)
                      (lambda () (canonical-description type value))
                      :session session))
                  (if destructivep
                    (calculate-session-evaluation-result
                      (lambda (&rest args) args
                        (canonical-html-description type value))
                      session *trag-web-id-package*)
                    (cached-session-eval
                      (list :html-description canonical-id-string)
                      (lambda () (canonical-html-description type value))
                      :session session))
                  (evaluate-in-session
                    (get-current-session session)
                    (lambda (session)
                      (declare (ignorable session))
                      (object-identifier-attributes type value))
                    nil))
                (lambda (description html-description extra-attributes)
                  (make-web-session-result
                    :time time
                    :status :success
                    :type type
                    :id id
                    :canonical-id canonical-id-string
                    :description description
                    :html-description html-description
                    :extra-attributes extra-attributes))))))))))

(defun session-set-current (kind id name &key session)
  (let ((session (get-current-session session)))
    (with-locked-session session
       (set-session-entry (list :current kind) id session))
    (if (= (length id) 0)
      (make-web-session-result :status :success)
      (progn
        (log-identifier id)
        (session-describe-identifier id :session session :name name)))))

(defun session-get-current (kind &key session)
  (let* ((session (get-current-session session)))
    (session-describe-identifier
     (with-locked-session session
       (get-session-entry (list :current kind) session))
     :session session)))

(defun session-store (path id &key session)
  (let ((session (get-current-session session)))
    (with-locked-session session
      (set-session-entry (cons :saved path) id session))
    (session-describe-identifier id :session session)))

(defun session-retrieve (path &key session)
  (let ((session (get-current-session session)))
    (session-describe-identifier
     (with-locked-session session
       (get-session-entry (cons :saved path) session))
     :session session)))

(defun session-store-string (path id &key session)
  (let ((session (get-current-session session)))
    (with-locked-session session
      (set-session-entry (cons :saved-string path) id session)))
  t)

(defun session-retrieve-string (path &key session)
  (let ((session (get-current-session session)))
    (with-locked-session session
      (get-session-entry (cons :saved-string path) session))))

(defun session-cancel-request (&key session)
  (cancel-session-request (get-current-session session))
  (make-web-session-result :status :success :description
                           "Pending process cancelled"))

(defun session-destroy (&key session)
  (kill-session (get-current-session session))
  (ignore-errors
    (hunchentoot:set-cookie
      "session_id" :value (random-session-id)))
  (make-web-session-result :status :success))

					; This package is intended for things that the client can freely invoke
					; with arbitrary positional parameters
(defpackage :trag-web-ajax-handlers
  (:use)
  (:import-from :trag-web
		#:session-describe-identifier
		#:session-set-current
		#:session-get-current
		#:session-store
		#:session-retrieve
		#:session-store-string
		#:session-retrieve-string
		#:session-cancel-request
		#:session-destroy
		)
  (:export
   #:session-describe-identifier
   #:session-set-current
   #:session-get-current
   #:session-store
   #:session-retrieve
   #:session-store-string
   #:session-retrieve-string
   #:session-cancel-request
   #:session-destroy
   ))

(add-parenscript-definitions
    (*trag-web-session-js-helpers* t)
					; manipulating the notion of current object of some kind
					; we have a target node to display the value
    (defun set-current (kind target id then-do name)
      (server-funcall
       "SESSION-SET-CURRENT"
       (json-args
         kind
         (cond
           ((ps:stringp id) id)
           ((null id) nil)
           (t (ps:chain -j-s-o-n (stringify id))))
         name)
       (lambda (value)
	 (unless (maybe-error value)
	   (setf (ps:chain target object)
                 (ps:chain -j-s-o-n 
                    (parse
                      (or (ps:chain value canonical-id)
                          (ps:chain value id))))
		 (ps:chain target type) (ps:chain value type)
		 (ps:chain target description) (ps:chain value description)
		 (ps:chain target name) (ps:chain value name)
		 (ps:chain target time) (ps:chain value time)
                 (ps:chain target extra-attributes)
                 (ps:chain value extra-attributes))
	   (show-text target nil)
	   (when name 
	     (show-text target (+ "Named: " name) t)
	     (put-empty-element target :br))
	   (if (ps:chain value html-description)
	       (show-html target (ps:chain value html-description) t)
	       (show-text target (ps:chain value description) t))
	   (if name
	       (server-funcall
		"SESSION-STORE"
		(json-args (list :named kind name) id)
		(or then-do (lambda ())))
	       (when then-do (then-do)))))))
    (defun get-current (kind target then-do)
      (server-funcall
       :session-get-current
       (json-args kind)
       (lambda (id)
	 (if
          (or
	   (= (ps:chain id status) "error")
	   (not id))
          (progn
            (toggle-visibility target "none")
            (setf (ps:chain target object) nil)
            (when then-do (then-do)))
          (set-current
            kind target
            (or (ps:chain id canonical-id) (ps:chain id id))
            then-do)))))
					; client-side helpers for identifiers
    (defun create-immediate-identifier (type data)
      (ps:create
       :type type
       :immediate-data data))
    (defun create-operation-identifier (type operation parameters)
      (ps:create
       :type type
       :operation operation
       :parameters parameters)))
