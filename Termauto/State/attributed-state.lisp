;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :state)

(defclass attributed-state (attributed-object combine-mixin abstract-state) ())

(defgeneric make-attributed-state (abstract-state attribute combine-fun))
(defmethod make-attributed-state
    ((state abstract-state) attribute (combine-fun function))
  (make-instance 'attributed-state :object state :attribute attribute
					  :combine-fun combine-fun))

(defgeneric attribute-state (abstract-state attribute)
  (:method ((state abstract-state) attribute)
    (make-attributed-state state attribute nil)))

(defmethod cast-state ((attributed-state attributed-state))
  (make-attributed-state
   (cast-state (object-of attributed-state))
   (attribute-of attributed-state)
   (combine-fun attributed-state)))

(defmethod casted-state-p ((attributed-state attributed-state))
  (casted-state-p (object-of attributed-state)))

(defgeneric unattribute-target (attributed-target))
(defmethod unattribute-target ((attributed-state attributed-state))
  (object-of attributed-state))

(defmethod unattribute-target ((attributed-target attributed-container))
  (make-target (container-mapcar #'object-of attributed-target)))

