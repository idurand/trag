(in-package :state)

(defvar *states-vector*)
(defvar *states-vector-len*)

(defgeneric expand-bv (bit-vector)
  (:documentation "transforms bit-vector to list of states according to *states-vector*"))

(defmethod expand-bv ((bv bit-vector))
  (loop
     for i from 0 below *states-vector-len*
     unless (zerop (aref bv i))
     collect (aref *states-vector* i)))

(defgeneric create-state-vector (container))
(defmethod create-state-vector ((container abstract-state-container))
  (let ((states (sort (copy-list (container-contents container)) #'< :key #'state-number)))
    (make-array (length states) :initial-contents states)))

(defun set-states-vector (states)
  (setf *states-vector* (create-state-vector states)
	*states-vector-len* (length *states-vector*)))

(defun new-states-vector (states)
  (setf *states-vector* (create-state-vector states)
	*states-vector-len* (length *states-vector*)))

(defmacro with-states-vector (states &body body)
  `(let* ((*states-vector* (create-state-vector ,states))
	  (*states-vector-len* (length *states-vector*)))
     ,@body))

(defun make-empty-bv ()
  (make-array *states-vector-len* :element-type 'bit))

(defgeneric states-to-bv (states))
(defmethod states-to-bv ((states list))
;;  (format *trace-output* "states-to-bv ~A~%" *states-vector*)
    (loop
       with bv = (make-empty-bv)
       for state in states
       do (setf (aref bv (state-number state)) 1)
       finally (return bv)))

(defgeneric istate-to-bv (integer))
(defmethod istate-to-bv ((q integer))
  (let ((bv (make-empty-bv)))
    (setf (aref bv q) 1)
    bv))

(defgeneric empty-bv-p (bit-vector)
  (:documentation "T if BIT-VECTOR contains only zeros"))

(defmethod empty-bv-p ((bv bit-vector))
  (every #'zerop bv))
