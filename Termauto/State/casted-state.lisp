(in-package :state)

(defvar *state-internal-number* 0)

(defun init-casted-states ()
  (setf *state-internal-number* 0))

(defclass casted-state (in-state key-mixin abstract-state)
  ((internal-number
    :initform (incf *state-internal-number*)
    :reader state-internal-number)
   (state-number :initform nil :initarg :state-number :accessor state-number)))

(defmethod state-index ((state casted-state))
 (state-index (in-state state)))

(defmethod state-size ((s casted-state))
  (+ (state-size (in-state s)) 2))

(defmethod compare-object ((s1 casted-state) (s2 casted-state))
  (eq s1 s2))

(defun nsort-casted-states (states)
  (sort states #'< :key #'state-internal-number))

(defun sort-casted-states (states)
  (nsort-casted-states (copy-list states)))

(defvar *automaton-states-table*)
(defvar *show-casted* t)

(defmethod casted-state-p ((s casted-state)) t)
(defmethod casted-state-p ((s uncasted-state)) nil)

(defmethod print-object :before ((casted-state casted-state) stream)
  (when (and *print-object-readably* *show-casted*)
   (format stream "!")))
;;    (format stream "!~A" (state-internal-number casted-state))))

(defmethod object-key ((casted-state casted-state))
  (state-internal-number casted-state))

(defmethod print-object ((casted-state casted-state) stream)
  (if *print-object-readably*
    (if (state-number casted-state)
	(format stream "[~A]" (state-number casted-state))
	(format stream "~A" (in-state casted-state)))
    (format stream "~A" (object-key casted-state))))

(defgeneric find-casted-state-from-state (state l)
  (:method ((state uncasted-state) (l list))
    (find-object state l :key #'in-state :test #'compare-object)))

(defgeneric put-state-in-automaton-states-table (key casted-state states-table)
  (:method (key (casted-state casted-state) (states-table hash-table))
    (setf (gethash key states-table) casted-state)))

(defgeneric make-casted-state (uncasted-state)
  (:method ((uncasted-state uncasted-state))
    (make-instance 'casted-state :in-state uncasted-state)))

(defgeneric uncast-state (state)
  (:method ((state uncasted-state))
    (warn "uncasting uncasted state") state)
  (:method ((state casted-state))
    (in-state state))
  (:method ((state (eql nil))) nil))

(defvar *sanity-check* nil)

(defun sanity-check ()
  (maphash (lambda (k v)
	     (unless (equal k (object-key (in-state v)))
	       (format *error-output* "key: ~A, object-key: ~A ~%" k  (object-key (in-state v)))
	       (error "sanity-check")))
	   *automaton-states-table*))

(defgeneric states-table-cast-state (uncasted-state states-table)
  (:method ((state null) (states-table hash-table)) nil)
  (:method ((state casted-state) (states-table hash-table))
    (error "casting casted-state ~A" state))
  (:method ((state uncasted-state) (states-table hash-table))
    (let* ((key (object-key state))
	   (casted-state
	     (find-state-in-automaton-states-table
	      key states-table)))
      (when *sanity-check* (sanity-check))
      (unless casted-state
	(setf casted-state (make-casted-state state))
	(put-state-in-automaton-states-table key casted-state states-table))
      casted-state)))

(defgeneric cast-state (state)
  (:method (state)
    (states-table-cast-state state *automaton-states-table*)))

(defmacro with-states-table (transitions &body body)
  `(let ((*automaton-states-table* (states-table ,transitions)))
     ,@body))

(defmethod state-internal-number ((ism in-state-mixin))
  (state-internal-number (in-state ism)))

;; on essaye de ne pas indexer les casted-states

(defgeneric nindex-state (casted-state index)
  (:method ((casted-state casted-state) (index integer))
    (warn "index-state on casted-state ~A~%" casted-state))
  (:method ((casted-state casted-state) (index integer))
    (setf (in-state casted-state) (index-state (in-state casted-state) index))
  casted-state))

(defmethod state-neutral-p ((s casted-state))
  (state-neutral-p s))

(defmethod state-final-p ((s casted-state))
  (state-final-p s))


