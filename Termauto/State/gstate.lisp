(in-package :state)
(defgeneric gstate-container (gstate))
(defclass gstate (uncasted-state)
  ((container :initarg :container :reader gstate-container)))

(defmethod print-object ((gstate gstate) stream)
  (format stream "$~A" (gstate-container gstate)))

(defgeneric make-gstate (state-container)
  (:method :before ((container state-container))
    (when *debug*
      (unless (eq 'ordered-state-container (type-of container))
	(warn "make-gstate it would be better to cast the automaton"))))
  (:method ((target null)) nil)
  (:method ((container abstract-state-container))
    (make-instance 'gstate :container container))
  (:method ((state abstract-state))
    (make-instance 'gstate :container (make-state-container-from-state state))))
