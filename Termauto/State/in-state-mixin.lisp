(in-package :state)

(defgeneric in-state (in-state-mixin)
  (:method ((s (eql nil)))
    (error "in-state of NIL")
    nil)
  (:method ((state abstract-state))
    (format *error-output* "in-state for any-state ~A:~A~%" state (type-of state))
    (error "state ~A" state)
    state))


(defclass in-state-mixin ()
  ((in-state :initform nil
	     :initarg
	     :in-state :accessor in-state)))

(defmethod print-object ((in-state-mixin in-state-mixin) stream)
  (format stream "~A" (in-state in-state-mixin)))

(defmethod casted-state-p ((ism in-state-mixin))
  (casted-state-p (in-state ism)))

  
