(in-package :state)

(defclass indexed-state (in-state-mixin uncasted-state)
  ((state-index :initform nil :initarg :state-index :accessor state-index)))
 
(defmethod print-object ((indexed-state indexed-state) stream)
  (format stream "~A-~A" (in-state indexed-state) (state-index indexed-state)))

(defgeneric index-state (uncasted-state index))

(defmethod index-state ((uncasted-state uncasted-state) (index integer))
  (make-instance 'indexed-state :state-index index :in-state uncasted-state))

(defmethod compare-object ((s1 indexed-state) (s2 indexed-state))
  (and (= (state-index s1) (state-index s2))
       (compare-object (in-state s1) (in-state s2))))

;; ne doit servir que quand on a des table-transitions
(defgeneric nindex-states (states index))
(defmethod nindex-states ((states list) (index integer))
  (dolist (state states)
    (nindex-state state index))
  states)
