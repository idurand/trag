(in-package :state)
(defvar *with-bv* t)

(defgeneric sstates-to-state-container (sstates))
(defmethod sstates-to-state-container ((sstates ordered-state-container))
  sstates)

(defun print-sstates (sstates &optional (stream t))
  (format stream "~A " (sstates-to-state-container sstates)))

(defmethod sstates-to-state-container ((sstates bit-vector))
  (make-ordered-state-container (expand-bv sstates)))

(defgeneric make-sstates-from-state (integer))
(defmethod make-sstates-from-state ((q integer))
  (istate-to-bv q))

(defmethod make-sstates-from-state ((q casted-state))
  (make-state-container-from-state q))

(defgeneric empty-sstates-p (sstates))
(defmethod empty-sstates-p ((container ordered-state-container))
  (container-empty-p container))

(defmethod empty-sstates-p ((sstates (eql nil)))
  t)
    
(defmethod empty-sstates-p ((sstates casted-state))
  (declare (ignore sstates))
  t)
    
(defmethod empty-sstates-p ((bit-vector bit-vector))
  (empty-bv-p bit-vector))

(defgeneric in-sstates (casted-state sstates))
(defmethod in-sstates ((q casted-state) (sstates ordered-state-container))
  (container-member q sstates))

(defmethod in-sstates ((q casted-state) (sstates bit-vector))
  (not (zerop (aref sstates (state-number q)))))

(defgeneric make-sstates (states))
(defmethod make-sstates ((states list))
  (if *with-bv*
      (states-to-bv states)
      (make-ordered-state-container states)))

(defmethod make-sstates ((state casted-state))
  (make-sstates (list state)))

(defmethod make-sstates ((states ordered-state-container))
  (if *with-bv*
      (states-to-bv (container-contents states))
      states))

(defmethod make-sstates ((states bit-vector))
  (if *with-bv*
      states
      (sstates-to-state-container states)))

(defgeneric sstates-nadjoin (casted-state sstates))
(defmethod sstates-nadjoin ((q casted-state) (sstates ordered-state-container))
  (container-nadjoin q sstates))

(defmethod sstates-nadjoin ((q integer) (sstates bit-vector))
  (let ((ss (make-sstates sstates)))
    (setf (aref ss q) 1)
    ss))

(defgeneric sstates-contents (sstates))
(defmethod sstates-contents ((sstates ordered-state-container))
  (container-contents sstates))

(defgeneric sstates-cardinality (sstates))
(defmethod sstates-cardinality ((sstates bit-vector))
  (count 1 sstates))

(defgeneric sstates-cardinality (sstates))
(defmethod sstates-cardinality ((sstates ordered-state-container))
  (length (container-contents sstates)))

(defgeneric sstates-singleton-p (sstates))
(defmethod sstates-singleton-p ((sstates bit-vector))
  (= 1 (sstates-cardinality sstates)))

(defmethod sstates-singleton-p ((sstates ordered-state-container))
  (= 1 (container-size sstates)))

(defmethod sstates-contents ((sstates bit-vector))
  (loop
     for i from 0 below *states-vector-len*
     unless (zerop (aref sstates i))
     collect i))

(defgeneric sstates-realstate (integer))
(defmethod sstates-realstate ((q integer))
  (aref *states-vector* q))

(defmethod sstates-realstate ((q casted-state)) q)

(defgeneric sstates-difference (sstates1 sstates2))
(defmethod sstates-difference ((sstates1 ordered-state-container) (sstates2 ordered-state-container))
  (container-difference sstates1 sstates2))

(defmethod sstates-difference ((sstates1 bit-vector) (sstates2 bit-vector))
  (bit-andc2  sstates1 sstates2))
