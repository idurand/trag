;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand
(in-package :state)

(defclass abstract-state-container (abstract-container) ()
  (:documentation "container for uncasted states"))

(defgeneric abstract-state-container-p (o)
  (:method (o) nil)
  (:method ((c abstract-state-container)) t))

(defclass state-container (container abstract-state-container) ()
  (:documentation "container for uncasted states"))

(defclass attributed-state-container (attributed-container abstract-state-container) ()
  (:documentation "container for uncasted states"))

(defmethod container-order-fun ((c state-container)) #'strictly-ordered-p)

(defclass ordered-state-container (ordered-container abstract-state-container) ()
  (:documentation "container for casted states"))

(defclass ordered-attributed-state-container (attributed-container ordered-mixin abstract-state-container) ()
  (:documentation "container for attributed casted states"))

(defmethod container-contents ((abstract-state abstract-state))
  ;;  (format *error-output* "container-contents state Warning a supprimer ")
  (list abstract-state))

(defmethod container-unordered-contents ((abstract-state abstract-state))
  ;;  (format *error-output* "container-contents state Warning a supprimer ")
  (list abstract-state))

(defmethod container-unordered-contents ((abstract-state (eql nil)))
  ;;  (format *error-output* "container-contents state Warning a supprimer ")
  '())

(defgeneric make-attributed-state-container (states combine-fun)
  (:method ((states list) combine-fun)
    (make-container-generic
     states
     'attributed-state-container
     #'equal
     (list :combine-fun combine-fun))))

(defgeneric make-ordered-attributed-state-container (states combine-fun)
  (:method ((states list) combine-fun)
    (make-container-generic 
     states 
     'ordered-attributed-state-container
     #'equal
     (list :combine-fun combine-fun))))

(defgeneric make-state-container (states)
  (:method ((states list))
    (make-container-generic states 'state-container #'equal)))

(defgeneric make-ordered-state-container (states)
  (:method ((states list))
    (assert
     (notany (lambda (state) (typep state 'attributed-state)) states))
    (make-container-generic
     states
     'ordered-state-container
     #'eql ;; en fait =
     )))

(defun state-container-p (s) (typep s 'state-container))
(defun ordered-state-container-p (s) (typep s 'ordered-state-container))

(defun make-empty-ordered-state-container ()
  (make-ordered-state-container '()))

(defun make-empty-state-container ()
  (make-state-container '()))

(defgeneric make-appropriate-attributed-state-container (states)
  (:method ((states list))
    (let* ((state (first states))
	   (combine-fun (combine-fun state)))
      (if (casted-state-p state)
	  (make-ordered-attributed-state-container states combine-fun)
	  (make-attributed-state-container states combine-fun)))))

(defgeneric make-appropriate-unattributed-state-container (states)
  (:method ((states list))
    (let ((state (first states)))
      (if (casted-state-p state)
	  (make-ordered-state-container states)
	  (make-state-container states)))))
  
(defgeneric make-appropriate-state-container (states)
  (:method ((states list))
    (if (attributed-p (first states))
	(make-appropriate-attributed-state-container states)
	(make-appropriate-unattributed-state-container states))))

(defgeneric make-state-container-from-state (state)
  (:method ((state abstract-state))
    (if (casted-state-p state)
	(make-ordered-state-container (list state))
	(make-state-container (list state))))
  (:method ((state casted-state)) (make-ordered-state-container (list state)))
  (:method ((state abstract-state)) (make-appropriate-state-container (list state))))

(defmethod find-casted-state-from-state ((state uncasted-state) (l abstract-container))
  (find-casted-state-from-state state (container-contents l)))

(defgeneric target-to-container (target)
  (:method ((c abstract-container)) c)
  (:method ((state abstract-state)) (make-state-container-from-state state)))
