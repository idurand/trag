(in-package :state)

(defgeneric casted-state-p (s)
  (:method ((s t)) nil)
  (:method ((s (eql nil))) nil)
  (:documentation "casted-states for has-consing states"))

