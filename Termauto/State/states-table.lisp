(in-package :state)

(defclass cast-states-mixin ()
  ((states-table :initarg :states-table
		 :initform (make-empty-automaton-states-table)
		 :accessor states-table)))

(defun make-empty-automaton-states-table ()
  (let* ((h (make-hash-table :test #'equal))
	 (*automaton-states-table* h))
    h))

(defun copy-automaton-states-table (states-table)
  (let ((new (make-empty-automaton-states-table)))
    (maphash (lambda (k v)
	       (setf (gethash k new) v))
	     states-table)
    new))

(defun find-state-in-automaton-states-table (key automaton-states-table)
 (gethash key automaton-states-table))

(defun automaton-states-table-count ()
  (hash-table-count *automaton-states-table*))

(defgeneric delete-states-in-states-table (ordered-state-container states-table &key test-not))
(defmethod delete-states-in-states-table
    ((states ordered-state-container) (states-table hash-table) &key test-not)
  (maphash
   (lambda (k v)
     (let ((m (and (container-member v states) t)))
       (when (eq m (not test-not))
	 (remhash k states-table))))
   states-table)
  states-table)

(defgeneric nfilter-states-table (states states-table))
(defmethod nfilter-states-table ((states ordered-state-container) (states-table hash-table))
  (delete-states-in-states-table states states-table :test-not t))

(defgeneric filter-states-table (states states-table)
  (:method ((states ordered-state-container) (states-table hash-table))
    (let ((new (make-empty-automaton-states-table)))
      (maphash
       (lambda (k v)
	 (when (container-member v states)
	   (setf (gethash k new) v)))
       states-table)
      new)))

(defgeneric reset-states-table (cast-states-mixin)
  (:method ((transitions cast-states-mixin))
    (clrhash (states-table transitions))))
  
;;  (format *standard-output* "calling GC manually~%")
;;  (sb-ext:gc :gen t))

(defun states-from-states-table (states-table)
  (make-ordered-state-container (list-values states-table)))

(defgeneric biggest-state (states-table)
  (:method ((states-table hash-table))
    (hash-reduce #'max states-table :key #'state-size :initial-value 0)))
