(in-package :state)

;; a target is either nil or a state
;; the abstract-state-container should never contain a completion state? but 
;; we cannot verify that at that point as we do not have the completion state of the transitions

(defmethod container-contents ((target (eql nil))) 
  (error "container-contents of nil")
  nil)

(defgeneric target-contents (target)
  (:method ((target (eql nil))) nil)
  (:method ((state abstract-state)) (list state))
  (:method ((c abstract-state-container))
    (container-contents c)))

(defgeneric target-size (target)
  (:method ((target (eql nil))) 0)
  (:method ((target (eql t))) 1)
  (:method  ((target abstract-state-container))
    (container-size target))
  (:method ((target abstract-state)) 1))

(defgeneric target-empty-p (target))
(defmethod target-empty-p (target)
  (zerop (target-size target)))

(defgeneric target-deterministic-p (target))
(defmethod target-deterministic-p ((target casted-state))
  (declare (ignore target))
  t)

(defmethod target-deterministic-p ((target abstract-state-container))
  (singleton-container-p target))

(defgeneric make-target (states))
(defmethod make-target ((states (eql nil))) nil)

(defmethod make-target ((states list))
  (assert states)
;;  (error "make-target")
  (make-target (make-appropriate-state-container states)))

(defmethod make-target ((container abstract-state-container))
  (let ((size (container-size container)))
    (and
     (plusp size)
;;     (if (= 1 size)
;;	 (container-car container)
     container)))
;;)

(defgeneric target-union (target1 target2)
  (:documentation "union of non completion-state targets"))

(defgeneric target-nunion (target1 target2)
  (:documentation "union of non completion-state targets
                   if target1 is a state-container the result is this state-container
                   possibly modified"))

(defgeneric target-copy (target))
(defmethod target-copy ((target (eql nil))) nil)
(defmethod target-copy ((target abstract-state))
  target)

(defmethod target-copy ((target abstract-state-container))
  (container-copy target))

(defmethod target-nunion ((target1 (eql nil)) target2)
  target2)
			  
(defmethod target-nunion (target1 (target2 (eql nil)))
  target1)

(defmethod target-nunion
    ((target1 abstract-state-container) (target2 abstract-state-container))
  (container-nunion target1 target2))

(defmethod target-nunion ((target1 abstract-state) (target2 abstract-state))
;;  (warn "target-nunion on states should not happen")
  (make-target (list target1 target2)))

(defmethod target-nunion ((target1 abstract-state) (target2 abstract-state-container))
  (container-nadjoin target1 target2))

(defmethod target-nunion ((target1 abstract-state-container) (target2 abstract-state))
  (container-nadjoin target2 target1))

(defmethod target-union (target1 target2)
  (target-nunion (target-copy target1) (target-copy target2)))

;;; target-intersection
(defgeneric target-intersection (target1 target2))

(defmethod target-intersection ((target1 abstract-state-container) (target2 abstract-state-container))
  (make-target
   (container-intersection target1 target2)))

(defmethod target-intersection ((target1 abstract-state-container) (target2 abstract-state))
  (and (container-member target2 target1) target2))

(defmethod target-intersection ((target1 abstract-state) (target2 abstract-state-container))
  (and (container-member target1 target2) target1))

(defmethod target-intersection ((target1 casted-state) (target2 casted-state))
  (and (eq target1 target2) target1))

(defmethod target-intersection ((target1 abstract-state) (target2 abstract-state))
  (and (compare-object target1 target2) target1))

(defgeneric target-product (target1 target2))

(defgeneric target-product (target1 target2))
(defmethod target-product ((rh1 ordered-state-container) (rh2 ordered-state-container))
  (make-ordered-state-container
   (mapcar
    (lambda (state)
      (cast-state (make-tuple-state state)))
    (cartesian-product (list (container-contents rh1) (container-contents rh2))))))

(defmethod target-product ((rh1 casted-state) (rh2 ordered-state-container))
  (make-ordered-state-container
   (mapcar
    (lambda (state)
      (cast-state (make-tuple-state (list rh1 state))))
    (container-contents rh2))))

(defmethod target-product ((rh1 ordered-state-container) (rh2 casted-state))
  (make-ordered-state-container
   (mapcar
    (lambda (state)
      (cast-state (make-tuple-state (list state rh2))))
    (container-contents rh1))))

(defmethod target-product ((rh1 ordered-attributed-state-container) (rh2 ordered-attributed-state-container))
  (make-ordered-attributed-state-container
   (mapcar
    (lambda (state)
      (cast-state (make-tuple-state state)))
    (cartesian-product (list (container-contents rh1) (container-contents rh2))))
   (combine-fun rh1)))

(defmethod target-product ((rh1 casted-state) (rh2 ordered-attributed-state-container))
  (make-ordered-attributed-state-container
   (mapcar
    (lambda (state)
      (cast-state (make-tuple-state (list rh1 state))))
    (container-contents rh2))
   (combine-fun rh2)))

(defmethod target-product ((rh1 ordered-attributed-state-container) (rh2 casted-state))
  (make-ordered-attributed-state-container
   (mapcar
    (lambda (state)
      (cast-state (make-tuple-state (list state rh2))))
    (container-contents rh1))
   (combine-fun rh1)))

(defmethod target-product ((rh1 abstract-state) (rh2 abstract-state))
  (cast-state (make-tuple-state (list rh1 rh2))))

(defgeneric targets-product (targets))
(defmethod targets-product ((targets list))
  (cartesian-product (mapcar #'container-unordered-contents targets)))

(defgeneric targets-product-enumerator (targets))
(defmethod targets-product-enumerator ((targets list))
  (make-product-enumerator
   (mapcar
    (lambda (target)
      (make-list-enumerator (container-contents target)))
    targets)))

(defgeneric cast-target (target))

(defmethod cast-target ((target (eql nil)))
  nil)

(defmethod cast-target ((state uncasted-state))
  (cast-state state))

(defmethod cast-target ((state casted-state))
  (error "cast-state of casted-state should not happen"))

(defmethod cast-target ((container abstract-state-container))
  (make-ordered-state-container (mapcar
			   (lambda (state)
			     (cast-state state))
			   (container-contents container))))

(defmethod cast-target ((container attributed-state-container))
  (make-ordered-attributed-state-container
   (mapcar
    (lambda (state)
      (cast-state state))
    (container-contents container))
   (combine-fun container)))

(defgeneric uncast-target (casted-target))

(defmethod uncast-target ((state uncasted-state))
  state)

(defmethod uncast-target ((container state-container))
  container)

(defmethod uncast-target ((state casted-state))
  (in-state state))

(defmethod uncast-target ((state attributed-state))
  (make-attributed-state (uncast-state (object-of state))
			 (attribute-of state)
			 (combine-fun state)))

(defmethod uncast-target ((state attributed-state))
  (make-attributed-state (uncast-state (object-of state)) (attribute-of state) (combine-fun state)))

(defmethod cast-target ((state attributed-state))
  (make-attributed-state
   (cast-state (object-of state))
   (attribute-of state) (combine-fun state)))

(defmethod uncast-target ((target (eql nil)))
  nil)

(defmethod uncast-target ((container ordered-state-container))
  (make-state-container (mapcar #'in-state (container-contents container))))

(defgeneric casted-target-p (target))
(defmethod casted-target-p ((state (eql nil))) nil)
(defmethod casted-target-p ((state state-container)) nil)
(defmethod casted-target-p ((state ordered-state-container)) t)
(defmethod casted-target-p ((state casted-state)) t)

(defmethod casted-target-p ((state uncasted-state)) nil)

(defgeneric index-target (target index))

(defmethod index-target ((container abstract-state-container) (index integer))
  (make-appropriate-state-container
   (container-mapcar
    (lambda (s) (index-state s index))
    container)))

(defmethod index-target ((state uncasted-state) (index integer))
  (index-state state index))

(defmethod index-target ((target (eql nil)) (index integer))
  nil)

(defgeneric attribute-target (target attribute combine-fun))
(defmethod attribute-target ((container abstract-state-container) attribute
			     (combine-fun function))
  (let ((attributed-states 
	  (container-mapcar
	   (lambda (s) (make-attributed-state s attribute combine-fun))
	   container)))
    (assert attributed-states)
    (if (casted-state-p (first attributed-states))
	(make-ordered-attributed-state-container attributed-states combine-fun)
	(make-attributed-state-container attributed-states combine-fun))))

(defmethod attribute-target ((state abstract-state) attribute (combine-fun function))
  (make-attributed-state state attribute combine-fun))

(defmethod attribute-target ((target null) attribute combine-fun)
  nil)
