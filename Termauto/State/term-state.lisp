(in-package :state)

(defgeneric make-term-state (term)
  (:documentation "a term associated with an integer"))

(defclass term-state (uncasted-state)
  ((term :initarg :term :reader term :initform nil)))

(defmethod make-term-state (term)
  (make-instance 'term-state :term (linearize term)))

(defmethod print-object ((term-state term-state) stream)
  (format stream "[~A]" (term term-state)))

(defmethod compare-object ((s1 term-state) (s2 term-state))
  (compare-object (term s1) (term s2)))

(defgeneric make-term-states (terms))
(defmethod make-term-states ((terms list))
  (mapcar (lambda (term)
	    (make-term-state term))
	  terms))

(defun r-state ()
  (make-term-state (build-term (make-constant-symbol "r"))))

(defun l-state ()
  (make-term-state (build-term (make-constant-symbol "l"))))

(defun var-state ()
 (make-term-state *new-var*))

(defvar *final-var-state* (var-state))
(defmethod state-final-p ((s (eql *final-var-state*))) t)
