(in-package :state)

(defclass tuple-state (uncasted-state)
  ((%tuple :initarg :tuple :reader tuple)))

(defmethod print-object ((tuple-state tuple-state) stream)
  (format stream "<")
  (display-sequence (tuple tuple-state) stream :sep ",")
  (format stream ">"))

(defun make-tuple-state (tuple)
  (make-instance 'tuple-state :tuple tuple))

(defun tuple-state-p (state)
  (typep state 'tuple-state))

(defmethod tuple ((casted-state casted-state))
  (tuple (in-state casted-state)))
