(in-package :state)

(defclass uncasted-state (abstract-state key-mixin) ())

(defgeneric uncasted-state-p (abstract-state)
  (:documentation "T if  ABSTRACT-STATE is uncasted")
  (:method ((s abstract-state)) nil)
  (:method ((s uncasted-state)) t))

(defmethod compare-object ((s1 uncasted-state) (s2 uncasted-state))
  (or (eq s1 s2) (equal (object-key s1) (object-key s2))))

(defclass in-state (in-state-mixin abstract-state) ())

(defclass special-state (uncasted-state) ()
  (:documentation "class for special states like ok, ok-ok, neutral. sinkstates are special states"))

(defmethod state-size ((s special-state)) 2)

(defclass neutral-state (special-state) ())

(defgeneric sink-state-final (state))

(defclass sink-state (special-state)
  ((sink-state-final :initform nil :initarg :sink-state-final :reader sink-state-final)))

(defmethod print-object ((state sink-state) stream)
  (if (sink-state-final state)
      (write-string "Success" stream)
      (write-string "Error" stream)))

(defvar *error-state* (make-instance 'sink-state))
(defvar *success-state* (make-instance 'sink-state :sink-state-final t))

(defclass final-neutral-state (neutral-state) ())
(defclass ok-state (special-state) ())
;; oplus(Ok,q) -> Ok forall q not in { Error, Ok}
;; oplus(Ok,Ok) -> Error

(defclass ok-ok-state (special-state) ()) 
;; oplus(Okk,q) -> Okk forall q not in { Error, Okk}
;; oplus(Okk,Okk) -> Okk

(defmethod print-object ((state ok-state) stream)
  (write-string "Ok" stream))

(defmethod print-object ((state ok-ok-state) stream)
  (write-string "Okk" stream))

(defmethod print-object ((state neutral-state) stream)
  (write-string "#" stream))

(defmethod print-object ((state final-neutral-state) stream)
  (write-string "#f" stream))

(defgeneric state-neutral-p (state)
  (:method ((state uncasted-state)) nil)
  (:method ((state neutral-state)) t))

(defgeneric state-final-p (state)
  (:method ((s abstract-state)) (warn "in State/state-final-p abstract-state") nil)
  (:method ((s uncasted-state)) nil)
  (:method ((o (eql nil)))  nil)
  (:method ((state ok-state)) t)
  (:method ((state ok-ok-state)) t)
  (:method ((s neutral-state))  nil)
  (:method ((s final-neutral-state)) t)
  (:method ((s sink-state)) (sink-state-final s)))

(defvar *neutral-state-final-p* nil)
(defvar *neutral-state* (make-instance 'neutral-state))
(defvar *final-neutral-state* (make-instance 'final-neutral-state))
(defvar *ok-state* (make-instance 'ok-state))
(defvar *ok-ok-state* (make-instance 'ok-ok-state))
