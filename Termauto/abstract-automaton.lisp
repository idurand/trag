;; LICENSE: see trag/LICENSE.text
;; AUTHOR: Irène Durand

(in-package :termauto)
(defvar *cast* nil)
(defvar *cast-inside* t)

(defun combine-name (name string)
  (or (decompose-name string name) (compose-name string name)))

(defun prefix-combine-name (prefix name)
  (or (prefix-decompose-name prefix name) (compose-name prefix name)))

(defun suffix-combine-name (name suffix)
  (or (suffix-decompose-name name suffix) (suffix-name name suffix)))

(defgeneric final-state-fun (abstract-automaton)
  (:documentation
   "the predicate to be applied to a state of ABSTRACT-AUTOMATON \
    to decide whether it is a finalstate"))

(defgeneric automata-table (automata))

(defclass automata ()
  ((automata-table :initform (make-hash-table :test #'equal)
		   :accessor automata-table)))

(defun make-automata () (make-instance 'automata))

(defclass abstract-automaton (abstract-transducer signed-object) 
  ())

(defgeneric automaton-name (a)
  (:method ((automaton abstract-automaton)) (name automaton)))

(defgeneric abstract-automaton-p (automaton)
  (:method ((automaton abstract-automaton))
    (typep automaton 'abstract-automaton)))

(defgeneric final-state-p (state abstract-automaton)
  (:method :before (s a))
  (:method ((state abstract-state) (a abstract-automaton))
    (funcall (final-state-fun a) state))
  (:method ((state attributed-state) (a abstract-automaton))
    (final-state-p (object-of state) a))
  (:method ((state (eql *success-state*)) (a abstract-automaton)) t)
  (:method ((state (eql nil)) (a abstract-automaton)) nil))

(defgeneric final-target-p (target abstract-automaton)
  (:method ((target abstract-state-container) (a abstract-automaton))
    (and (container-find-if (lambda (state) (final-state-p state a)) target) t))
  (:method ((state (eql nil)) (a abstract-automaton)) nil)
  (:method ((state abstract-state) (a abstract-automaton)) (final-state-p state a)))

(defgeneric final-target (target abstract-automaton)
  (:method ((state abstract-state) (a abstract-automaton))
    (if (final-state-p state a) state nil))
  (:method ((state (eql *success-state*)) (a abstract-automaton)) *success-state*)
  (:method ((state (eql nil)) (a abstract-automaton)) nil)
  (:method ((target abstract-state-container) (a abstract-automaton))
    (let ((c (container-remove-if-not
	      (lambda (state) (final-state-p state a))
	      target)))
      (if (container-empty-p c) nil c))))

(defgeneric parallel-compute-target (term automaton &key signal save-run)
  (:documentation "computes the target of TERM with AUTOMATON")
  (:method ((term term) (automaton abstract-automaton)
	    &key (signal t) (save-run nil))
    (transitions-compute-target term (transitions-of automaton)
				:signal signal
				:save-run save-run)))

(defgeneric compute-target (term automaton
			    &key save-run enum signal final just-one)
  (:documentation "computes the target of TERM with automaton")
  (:method :around ((term term) (automaton abstract-automaton)
		    &key save-run enum signal final just-one)
    (declare (ignore save-run enum signal final just-one))
    (unless (signature-subset-p (signature term) (signature automaton))
      (warn "signature of term ~A not compatible with signature of automaton ~A"
	    term
	    (signature automaton))
      (return-from compute-target))
    (call-next-method))
  (:method ((term term) (automaton abstract-automaton)
	    &key (save-run nil) (enum nil) (signal t) (final nil) (just-one t))
    (let ((target
	    (if (and enum (not (deterministic-p automaton)))
		(enum-compute-target term automaton :final final :just-one just-one)
		(parallel-compute-target
		 term automaton :save-run save-run :signal signal))))
      (if final
	  (final-target target automaton)
	  target))))

(defgeneric compute-final-target (term automaton &key enum save-run signal)
  (:documentation "computes the final target of TERM with AUTOMATON")
  (:method ((term term) (automaton abstract-automaton)
	    &key (enum nil) (save-run nil) (signal t))
    (compute-target term automaton :enum enum :save-run save-run :signal signal :final t)))

(defmethod deterministic-p ((a abstract-automaton))
  (deterministic-p (transitions-of a)))

(defgeneric table-automaton-p (abstract-automaton)
  (:method ((automaton abstract-automaton)) nil))

(defgeneric recognized-p (term automaton &key enum save-run signal)
  (:documentation "true if TERM is recongized by AUTOMATON")
  (:method :around ((term term) (automaton abstract-automaton) &key save-run enum signal)
    (declare (ignore save-run enum signal))
    (let ((precondition-automaton (precondition-automaton automaton)))
      (when (and precondition-automaton
		 (not (recognized-p term precondition-automaton)))
	  (warn "term not recognized by precondition-automaton ~A" (name precondition-automaton))
	  (return-from recognized-p)))
    (call-next-method))
  (:method ((term term) (a abstract-automaton)
	    &key (enum nil) (save-run nil) (signal t))
    (let ((target
	    (compute-final-target term a :enum enum :save-run save-run :signal signal)))
      (if target
	  (values t target)
	  (values nil nil)))))

(defgeneric parallel-recognized-p (term automaton &key save-run signal)
  (:documentation "true if TERM is recongized by AUTOMATON (sequential)")
  (:method ((term term) (automaton abstract-automaton) &key (save-run nil) (signal t))
    (recognized-p term automaton :enum nil :signal signal :save-run save-run)))

(defgeneric casted-p (abstract-automaton)
  (:method ((automaton abstract-automaton))
    (casted-transitions-p (transitions-of automaton))))

(defmethod constant-signature ((a abstract-automaton))
  (constant-signature (signature a)))

(defgeneric successful-run (term abstract-automaton)
  (:method ((term term) (automaton abstract-automaton))
    (let ((state (container-find-if
		  (lambda (st) (final-state-p st automaton))
		  (transitions-compute-target
		   term (transitions-of automaton) :save-run t))))
      (if state
	  (select-one-run term state (transitions-of automaton))
	  (setf (run term) nil))
      term)))

(defgeneric intersection-automata-compatible (automata))

(defgeneric intersection-automaton-gen (&rest automata)
  (:method (&rest automata)
    (intersection-automata automata)))

(defgeneric intersection-automaton-compatible-gen (&rest automata)
  (:method (&rest automata) (intersection-automata-compatible automata)))

(defgeneric union-automaton-gen (&rest automata)
  (:method (&rest automata) (union-automata automata)))

(defgeneric disjoint-union-automaton-gen (&rest automata)
  (:method (&rest automata)
    (let* ((fly-automata 
	     (mapcar #'uncast-automaton
		     (remove-duplicates automata :test #'eq)))
	   (u (disjoint-union-automata-gen fly-automata)))
      (when (every #'table-automaton-p automata)
	(setq u (compile-automaton u)))
      u)))

(defgeneric epsilon-closure-automaton (automaton)
  (:documentation
   "Returns an equivalent automaton of automaton without epsilon-transitions."))

(defgeneric reduced-p (automaton))
(defgeneric co-reduced-p (automaton))

(defgeneric nreduce-automaton (automaton)
  (:documentation "destructive version"))

(defgeneric reduce-automaton (automaton)
  (:documentation "returns AUTOMATON if reduced or the reduced version \
    of AUTOMATON otherwise")
  (:method ((automaton abstract-automaton))
    :documentation "returns AUTOMATON if reduced or the reduced version \
     of AUTOMATON otherwise"
    (if (reduced-p automaton)
	automaton
	(nreduce-automaton (duplicate-automaton automaton)))))

(defgeneric co-reduce-automaton (automaton)
  (:documentation "returns AUTOMATON if co-reduced or the co-reduced version \
    of AUTOMATON otherwise")
  (:method ((automaton abstract-automaton))
    :documentation "returns AUTOMATON if reduced or the reduced version of AUTOMATON otherwise"
    (if (co-reduced-p automaton)
	automaton
	(nco-reduce-automaton (duplicate-automaton automaton)))))

(defmethod signature ((automaton abstract-automaton))
  (signature (transitions-of automaton)))

(defmethod show-constant-transitions ((automaton abstract-automaton) &optional (stream t))
  (show-constant-transitions
   (transitions-of automaton) stream))

(defgeneric rename-and-save (name automaton)
  (:method (name (a abstract-automaton)) (rename-object a name)))

(defgeneric recognized-terms-enumerator (abstract-automaton depth-fun depth)
  (:method ((a abstract-automaton) (depth-fun function) (depth integer))
    (make-filter-enumerator 
     (funcall depth-fun (signature a) depth)
     (lambda (term)
       (enum-recognized-p term a)))))

(defgeneric recognized-terms-depth<=-enumerator (abstract-automaton depth)
  (:method ((a abstract-automaton) (depth integer))
    (recognized-terms-enumerator a #'terms-depth<=-enumerator depth)))

(defgeneric recognized-terms-depth=-enumerator (abstract-automaton depth)
  (:method ((a abstract-automaton) (depth integer))
    (recognized-terms-enumerator a #'terms-depth=-enumerator depth)))

(defgeneric index-states-automaton (automaton state-index)
  (:method ((automaton abstract-automaton) (state-index integer))
    (nindex-states-automaton (duplicate-automaton automaton) state-index)))

(defgeneric and-finalpstate-fun (automaton1 automaton2)
  (:method ((aut1 abstract-automaton) (aut2 abstract-automaton))
    (lambda (pstate)
      (and
       (tuple-state-p pstate)
       (final-state-p (first (tuple pstate)) aut1)
       (final-state-p (second (tuple pstate)) aut2)
       t))))

(defgeneric and-finalpstate-fun-gen (automata)
  (:method ((automata list))
    (lambda (pstate)
      (and
       (tuple-state-p pstate)
       (every (lambda (state automaton)
		(final-state-p state automaton))
	      (tuple pstate)
	      automata)))))

(defgeneric or-finalpstate-fun-gen (automata)
  (:method ((automata list))
    (lambda (pstate)
      (and
       (tuple-state-p pstate)
       (some (lambda (state automaton)
	       (final-state-p state automaton))
	     (tuple pstate)
	     automata)))))

(defgeneric or-finalpstate-fun (automaton1 automaton2)
  (:method ((aut1 abstract-automaton) (aut2 abstract-automaton))
    (lambda (pstate)
      (and
       (tuple-state-p pstate)
       (or (final-state-p (first (tuple pstate)) aut1)
	   (final-state-p (second (tuple pstate)) aut2))
       t))))

(defgeneric op-automata-list (automata &key op cop)
  (:method ((automata list) &key (op 'intersection-automaton)
			      (cop 'minimize-automaton))
  (assert automata)
  (let ((a (funcall cop (car automata))))
    (mapc
     (lambda (ap)
       (setf a (funcall op a (funcall cop ap)))
       (setf a (funcall cop a)))
     (cdr automata))
    a)))
