;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric output-fun (abstract-transducer))

(defgeneric transitions-of (transducer)
  (:documentation "transitions of TRANSDUCER"))

(defgeneric precondition-automaton (abstract-transtucer)
  (:documentation
   "auxiliary automaton used as a precondition to ABSTRACT-TRANSDUCER"))

     
(defclass abstract-transducer (named-mixin)
  ((transitions :initarg :transitions :accessor transitions-of)
   (output-fun :accessor output-fun :initarg :output-fun
	       :initform (lambda (target) (and target T)))
   (precondition-automaton :initform nil
			   :type abstract-automaton
			   :initarg :precondition-automaton
			   :accessor precondition-automaton))
  (:documentation "named object with transitions which applied to a term
                   returns a target"))

(defgeneric abstract-transducer-p (transducer)
  (:method ((transducer abstract-transducer))
    (typep transducer 'abstract-transducer)))

(defgeneric target-value (target abstract-transducer)
  (:documentation "computes the value of the target using the output-fun")
  (:method (target (transducer abstract-transducer))
    (values (funcall (output-fun transducer) target) t)))

(defgeneric compute-value (term transducer &key final enum)
  (:documentation "computes the value of the term with the transducer")
  (:method ((term term) (transducer abstract-transducer) &key (final t) (enum nil))
    (target-value
     (compute-target term transducer :enum enum :final final :just-one nil)
     transducer)))

(defgeneric compute-final-value (term transducer &key enum)
  (:documentation "computes the final value of the term with the transducer")
  (:method ((term term) (transducer abstract-transducer) &key (enum nil))
    (compute-value term transducer :enum enum :final t)))

(defgeneric enum-compute-value (term transducer)
  (:method (term (transducer abstract-transducer))
    (compute-value term transducer :enum t)))

(defgeneric enum-compute-final-value (term transducer)
  (:method (term (transducer abstract-transducer))
    (compute-final-value term transducer :enum t)))

(defgeneric value-enumerator (term abstract-transducer &key final)
  (:method ((term term) (transducer abstract-transducer) &key (final nil))
    (make-append-enumerator
     (make-funcall-enumerator
      (lambda (target) (target-value target transducer))
      (target-enumerator term transducer :final final)))))

(defgeneric final-value-enumerator (term abstract-transducer)
  (:method ((term term) (transducer abstract-transducer))
    (value-enumerator term transducer :final t)))
