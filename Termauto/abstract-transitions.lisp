;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric deterministic-p (automaton-or-transitions))
(defgeneric epsilon-p (automaton-or-transitions))
(defgeneric epsilon-closure (transitions)
  (:documentation
   "Returns equivalent TRANSISIONS without epsilon-transitions."))

(defclass abstract-transitions (signature-mixin)
  ((sink-state :initarg :sink-state :initform nil :accessor sink-state)))

(defgeneric casted-transitions-p (transitions)
  (:method ((transitions abstract-transitions))
    nil))

(defgeneric transitions-fun (transitions)
  (:documentation
   "the transition function to be applied to a symbol of arity i and i states
    returns a set of states or a single state if deterministic"))

(defgeneric show-states-characteristics (transitions &key stream)
  (:method ((transitions abstract-transitions) &key (stream t))
    (declare (ignore transitions stream))))

(defgeneric to-commutative (transitions))
(defgeneric possibly-equivalent-to-state (q qp sym-table))
(defgeneric show-constant-transitions (transitions &optional stream))
