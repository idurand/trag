;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric has-only-marked-states (key marked))
(defmethod has-only-marked-states ((key list) (marked ordered-state-container))
  (has-only-marked-states (make-ordered-state-container (cdr key)) marked))

(defmethod has-only-marked-states ((casted-state casted-state)
				   (marked ordered-state-container))
  (container-member casted-state marked))

(defmethod has-only-marked-states ((arity-symbol abstract-symbol)
				   (marked ordered-state-container))
  t)

(defmethod has-only-marked-states ((container ordered-state-container) (marked ordered-state-container))
  (container-subset-p container marked))

(defgeneric has-at-least-one-marked-state (target marked-states))

(defmethod has-at-least-one-marked-state
    ((states ordered-state-container) (marked ordered-state-container))
  (not (container-empty-p (container-intersection states marked))))

(defmethod has-at-least-one-marked-state
    ((casted-state casted-state) (marked ordered-state-container))
  (container-member casted-state marked))

(defmethod has-at-least-one-marked-state
    ((arity-symbol abstract-symbol) (marked ordered-state-container))
  nil)

