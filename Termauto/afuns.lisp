;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defvar *nb-color*)

(defclass afuns (named-mixin)
  ((symbol-fun :reader symbol-fun :initarg :symbol-fun)
   (combine-attribute-fun
    :reader combine-attribute-fun :initarg :combine-attribute-fun))
  (:documentation
   "combine-attribute-fun handles non-determinism: when we obtain several attributed-states
    containing the same state q,  attributes must be combined to obain the attribute for q"))

(defun make-afuns (symbol-fun combine-attribute-fun name)
  (make-instance 'afuns :symbol-fun symbol-fun
			:combine-attribute-fun combine-attribute-fun
			:name name))

(defun container-union-fun (container-fun)
  (lambda (&rest attributes)
    (if (endp attributes)
	(funcall container-fun nil)
	(merge-containers attributes))))

(defun union-fun (&key (test #'compare-object))
  (lambda (&rest attributes)
    (reduce (lambda (a1 a2) (union a1 a2 :test test))
	    attributes :initial-value '())))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; count runs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric count-run-symbol-fun (symbol)
  (:documentation "counting runs")
  (:method ((s abstract-symbol)) #'*))

(defvar *count-afun*
  (make-afuns #'count-run-symbol-fun #'+ "#"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; compute height
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric height-symbol-fun (symbol)
  (:documentation "compute height")
  (:method ((s constant-mixin)) (lambda () 1))
  (:method ((s abstract-symbol))
    (lambda (&rest attributes) (1+ (reduce #'max attributes))))
  (:method ((s annotated-symbol))
    (height-symbol-fun (symbol-of s))))

(defun height-combine-attribute-fun (&rest attributes)
  (assert (apply #'= attributes))
  (car attributes))

(defparameter *height-afun*
  (make-afuns #'height-symbol-fun #'height-combine-attribute-fun "Height"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; compute stralher
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric strahler-symbol-fun (symbol)
  (:documentation "compute stralher")
  (:method ((s constant-mixin)) (lambda () 1))
  (:method ((s abstract-symbol))
    (lambda (&rest attributes)
      (let ((m (apply #'max attributes)))
	(if (> (count m attributes) 1)
	    (1+ m)
	    m))))
  (:method ((s annotated-symbol)) (strahler-symbol-fun (symbol-of s))))

(defun strahler-combine-attribute-fun (&rest attributes)
  (assert (apply #'= attributes))
  (car attributes))

(defparameter *strahler-afun*
  (make-afuns #'strahler-symbol-fun #'strahler-combine-attribute-fun "Strahler"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; when attributes are sets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun attributes-fun (element-fun)
  (lambda (attributes1 attributes2)
    (let ((attributes (container-empty-copy attributes1)))
      (container-map
       (lambda (a1)
	 (container-map
	  (lambda (a2)
	    (container-nadjoin
	     (funcall element-fun a1 a2)
	     attributes))
	  attributes2))
       attributes1)
      attributes)))

(defgeneric attributes-symbol-fun-body (attributes element-fun)
  (:method ((attributes list) element-fun)
    (if (endp (cdr attributes))
	(car attributes)
	(reduce (attributes-fun element-fun) attributes))))

(defgeneric attributes-symbol-fun (element-fun)
  (:method (element-fun)
    (lambda (&rest attributes)
      (attributes-symbol-fun-body attributes element-fun))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; spectrum
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass spectrum-container (container) ())

(defun make-spectrum-container (lvbits)
  (make-container-generic
   lvbits
   'spectrum-container
   #'equalp))

(defun color-to-vbits (color nb-color)
  (let ((v (make-array nb-color)))
    (setf (aref v (1- color)) 1)
    v))

(defgeneric spectrum-symbol-fun (abstract-symbol)
  (:documentation "")
  (:method ((s vbits-constant-symbol))
    (assert (symbol-constant-p s))
    (lambda () (list (vbits s))))
  (:method ((s non-constant-mixin))
    (attributes-symbol-fun #'vector-sum))
  (:method ((s annotated-symbol)) (spectrum-symbol-fun (symbol-of s)))
  (:method ((s color-constant-symbol))
    (assert (symbol-constant-p s))
    (let ((nb-color *nb-color*))
      (lambda ()
	(make-spectrum-container 
	 (list (color-to-vbits (color s) nb-color)))))))

(defparameter *spectrum-afun*
  (make-afuns #'spectrum-symbol-fun
	      (container-union-fun #'make-spectrum-container)
	      "Sp"))

(defun color-spectrum-afun (nb-color)
  (make-afuns
   (lambda (s) (let ((*nb-color* nb-color)) (spectrum-symbol-fun s)))
   (container-union-fun #'make-spectrum-container)
   (format nil "Sp-C~A" nb-color)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; when attributes are multi-sets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; multi-spectrum
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass multi-spectrum-container (multi-container) ())

(defun make-multi-spectrum-container (lvbits)
  (make-multi-container-generic
   lvbits
   'multi-spectrum-container
   #'equalp
   #'+))

(defgeneric multi-spectrum-symbol-fun (s)
  (:documentation "")
  (:method ((s vbits-constant-symbol))
    (lambda () (make-multi-spectrum-container (list (vbits s)))))
  (:method ((s color-constant-symbol))
    (assert (symbol-constant-p s))
    (let ((nb-color *nb-color*))
      (lambda ()
	(make-multi-spectrum-container
	 (list (color-to-vbits (color s) nb-color))))))
  (:method ((s annotated-symbol))
    (multi-spectrum-symbol-fun (symbol-of s)))
  (:method ((s non-constant-mixin))
    (attributes-symbol-fun (multi-object-fun #'vector-sum #'*))))

(defparameter *multi-spectrum-afun*
  (make-afuns #'multi-spectrum-symbol-fun
	      (container-union-fun #'make-multi-spectrum-container)
	      "Msp"))

(defun color-multi-spectrum-afun (nb-color)
  (make-afuns
  (lambda (s) (let ((*nb-color* nb-color)) (multi-spectrum-symbol-fun s)))
   (container-union-fun #'make-multi-spectrum-container)
   (format nil "Msp-C~A" nb-color)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; card
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric card-symbol-fun (abstract-symbol)
  (:method ((s non-constant-mixin))
    (attributes-symbol-fun #'+)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; card-1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric card-1-symbol-fun (abstract-symbol)
  (:method  ((s non-constant-mixin))
    (card-symbol-fun s)))

(defmethod card-1-symbol-fun ((s vbits-symbol))
  (lambda ()
    (assert (= (length (vbits s)) 1))
    (list (aref (vbits s) 0))))

(defparameter *card-1-afun*
  (make-afuns #'card-1-symbol-fun (union-fun :test #'=)
	      "Card-1"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; card-i cardinality of X_i
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric card-i-symbol-fun (abstract-symbol i)
  (:method ((s non-constant-mixin) (i integer))
    (declare (ignore i))
    (card-symbol-fun s))
  (:method ((s vbits-symbol) (i integer))
    (lambda ()
      (assert (>= (length (vbits s)) i))
      (list (aref (vbits s) (1- i))))))

(defun card-i-fun (i) (lambda (s) (card-i-symbol-fun s i)))

(defun card-i-afun (i) (make-afuns (card-i-fun i) (union-fun :test #'=)
				   (format nil "Card-~A" i)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; min-card
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric min-max-card-symbol-fun (abstract-symbol)
  (:method ((s non-constant-mixin)) #'+)
  (:method ((s vbits-symbol))
    (lambda ()
      (assert (= (length (vbits s)) 1))
      (aref (vbits s) 0)))
  (:method ((s annotated-symbol))
    (min-max-card-symbol-fun (symbol-of s))))

(defparameter *min-card-afun* (make-afuns #'min-max-card-symbol-fun #'min "Min"))
(defparameter *max-card-afun* (make-afuns #'min-max-card-symbol-fun #'max "Max"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; leaf positions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric leaf-positions-symbol-fun (symbol)
  (:method ((s constant-mixin)) (lambda () (list (make-position '()))))
  (:method ((s annotated-symbol)) (leaf-positions-symbol-fun (symbol-of s)))
  (:method ((s abstract-symbol))
    (lambda (&rest positionss)
      (loop for i from 0
	    for positions in positionss
	    nconc
	    (loop for position in positions
		  collect (left-extend-position position i))))))

(defparameter *leaf-positions-afun*
  (make-afuns #'leaf-positions-symbol-fun (union-fun) "Leaf-positions"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; positions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric positions-symbol-fun (symbol)
  (:method ((s constant-mixin)) (lambda () (list (make-position '()))))
  (:method ((s annotated-symbol)) (positions-symbol-fun (symbol-of s)))
  (:method ((s abstract-symbol))
    (lambda (&rest positionss)
      (loop for i from 0
	    for positions in positionss
	    nconc
	    (loop for position in (cons (make-position '()) positions)
		  collect (left-extend-position position i))))))

(defparameter *positions-afun* (make-afuns #'positions-symbol-fun (union-fun)
					   "Positions"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; sat 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric assignment-fun (attributes1 attributes2)
  (:method ((attributes1 list) (attributes2 list))
    (loop with attributes = '()
	  for a1 in attributes1
	  do (loop for a2 in attributes2
		   do (push (positions-assignment-union a1 a2) attributes))
	  finally (return attributes))))

(defgeneric assignment-symbol-fun (s)
  (:documentation "for Sat")
  (:method ((s vbits-symbol))
    (lambda ()
      (list
       (make-positions-assignment
	(list (make-position-assignment (make-position '()) (vbits s)))))))
  (:method ((s color-symbol))
    (assert (symbol-constant-p s))
    (lambda ()
      (list (make-positions-assignment
	     (list (make-position-assignment (make-position '()) (color s)))))))
  (:method ((s non-constant-mixin))
    (lambda (&rest attributes)
      (setf
       attributes
       (loop for attribute in attributes
	     for i from 0
	     collect
	     (loop for positions-assignment in attribute
		   collect (left-extend-positions-assignment positions-assignment i))))
      (if (endp (cdr attributes))
	  (car attributes)
	  (reduce #'assignment-fun attributes))))
  (:method ((s annotated-symbol)) (assignment-symbol-fun (symbol-of s))))

(defparameter *assignment-afun*
  (make-afuns #'assignment-symbol-fun (union-fun :test #'compare-object)
	      "Sat"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric target-attribute (target)
  (:documentation "compute the attribute of an attributed target")
  (:method ((target null)) nil)
  (:method ((state abstract-state)) state)
  (:method ((state attributed-object)) (attribute-of state))
  (:method ((c abstract-state-container))
    (and (not (container-empty-p c)) (container-attribute c))))

(defgeneric target-max-run-count (attributed-state)
  (:documentation "non determinism degree")
  (:method ((target null)) 0)
  (:method ((s attributed-state)) (attribute-of s)))
