;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric attribute-automaton (automaton afuns)
  (:method ((automaton abstract-automaton) (afuns afuns))
    (make-fly-automaton-with-transitions
     (attribute-transitions (transitions-of automaton) afuns)
     (final-state-fun automaton)
     (lambda (target)
       (if (null target)
	   (funcall (combine-attribute-fun afuns)) ;; neutral element
	   (target-attribute target)))
     (format nil "~A.~A" (name afuns) (name automaton))
     :precondition-automaton (precondition-automaton automaton))))

		  
			    

  

