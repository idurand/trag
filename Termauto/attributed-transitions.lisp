(in-package :termauto)

(defclass attributed-transitions (fly-transitions)
  ((%afuns :accessor afuns :initarg :afuns)))

(defgeneric attributed-transitions-fun (abstract-transitions afuns)
  (:method ((transitions abstract-transitions) (afuns afuns))
    (let ((combine-fun (combine-attribute-fun afuns))
	  (symbol-fun (symbol-fun afuns)))
      (lambda (root attributed-states)
	(let ((target
		(apply-transitions
		 root
		 (mapcar #'object-of attributed-states) transitions)))
	  (when target
	    (let ((attribute (apply (funcall symbol-fun root)
				    (mapcar #'attribute-of attributed-states))))
	      (attribute-target target attribute combine-fun))))))))

(defgeneric attribute-transitions (transitions afuns)
  (:documentation "associate an attribution mecanisms to a deterministic automaton")
  (:method ((transitions abstract-transitions) (afuns afuns))
    (let ((f (make-fly-transitions
	      (signature transitions)
	      (attributed-transitions-fun transitions afuns)
	      t
	      (sink-state transitions)
	      :transitions-type 'attributed-transitions)))
      (setf (afuns f) afuns)
      f)))
