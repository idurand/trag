;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric make-casted-term-state (term))

(defmethod make-casted-term-state (term)
  (cast-state (make-term-state term)))

(defgeneric make-casted-term-states (terms))

(defmethod make-casted-term-states ((terms list))
  (mapcar
   #'cast-state
   (make-term-states terms)))

(defun casted-r-state ()
  (cast-state (r-state)))

(defun casted-l-state ()
  (cast-state (l-state)))

(defun casted-var-state ()
  (cast-state (var-state)))

(defgeneric find-casted-r-state (transitions))

(defmethod find-casted-r-state ((transitions table-transitions))
  (find-casted-state (r-state) transitions))

(defgeneric find-casted-l-state (transitions))

(defmethod find-casted-l-state ((transitions table-transitions))
  (find-casted-state (l-state) transitions))

(defun  place-r-for-arity  (arity)
  (do ((l nil (cons
	       (append (casted-var-states (1- i))
		       (list (casted-r-state))
		       (casted-var-states (- arity i)))
	       l))
       (i 1 (1+ i)))
      ((> i arity) l)))

;;; some particular useful automata for TRSs
(defun redex-transitions (lhs &optional (redex t))
  (loop
     for lh in lhs
     do
	(let ((qkey (make-qkey lh)))
	 (add-transition-to-global-transitions
	  (car qkey) (cdr qkey)
	  (casted-r-state))))
  (when redex
    (loop for lh in lhs
       do (let ((qkey (make-qkey lh)))
	    (add-transition-to-global-transitions
	     (car qkey) (cdr qkey)
	     (casted-l-state))))))

(defun matching-transitions (terms)
  (loop for term in terms
     do (let ((qkey (make-qkey term)))
	  (add-transition-to-global-transitions
	   (car qkey) (cdr qkey)
	   (make-casted-term-state term)))))

(defun reducible-transitions (ncsignature)
  (loop for sym in (signature-symbols ncsignature)
     do
       (loop for arg in (place-r-for-arity (arity sym))
	  do
	    (add-transition-to-global-transitions
	     sym arg
	     (casted-r-state)))))

(defun propagation-transitions (signature)
  (loop for sym in (signature-symbols signature)
     do
       (add-transition-to-global-transitions
	sym (casted-var-states (arity sym))
	(casted-var-state))))

(defgeneric make-qkey (term))

(defmethod make-qkey ((term var))
  (make-casted-term-state term))

(defmethod make-qkey ((term term))
  (let ((linear-term (linearize term)))
    (cons
     (root linear-term)
     (make-casted-term-states (arg linear-term)))))

(defun casted-var-states (a)
  (make-list a :initial-element (casted-var-state)))

(defun make-matching-automaton (lhs signature &key (strict nil) (finalstates nil) (subterms t))
  (with-new-transitions signature
    (let* ((sub (if strict
		    (strict-subterms-list lhs)
		    (subterms-list lhs)))
	   (fs (and finalstates
		    (make-casted-term-states
		     (if subterms sub lhs)))))
      (matching-transitions sub)
      (propagation-transitions signature)
      (make-table-automaton
       (make-table-transitions-from-global-sym-table)
       :name "matching"
       :finalstates (make-ordered-state-container fs)))))

(defgeneric make-universal-automaton (signature)
  (:documentation "return automaton recognizing all ground terms built with SIGNATURE"))

(defmethod make-universal-automaton ((signature signature))
  (with-new-transitions signature
    (propagation-transitions signature)
    (make-table-automaton
     (make-table-transitions-from-global-sym-table)
     :name "universal"
     :finalstates (states-from-states-table *automaton-states-table*))))

(defgeneric make-empty-automaton (signature)
  (:documentation "return automaton recognizing all ground terms built with SIGNATURE"))

(defmethod make-empty-automaton ((signature signature))
  (with-new-transitions signature
    (make-table-automaton
     (make-table-transitions-from-global-sym-table)
     :name "empty")))

(defun make-redex-automaton (lhs signature)
  (rename-object
   (make-matching-automaton lhs signature :finalstates t :subterms nil)
   "redex"))

(defun make-reducible-automaton (lhs signature)
  (with-new-transitions signature
    (reducible-transitions (non-constant-signature signature))
    (redex-transitions lhs nil)
    (propagation-transitions signature)
    (matching-transitions (subterms-list lhs))
    (make-table-automaton
     (make-table-transitions-from-global-sym-table)
     :name "reducible"
     :finalstates (make-state-container-from-state (casted-r-state)))))

(defun make-b-automaton (lhs signature &key (bullet nil) (extra nil) (finalstates nil))
  (when extra
    (setf signature (add-extra-symbol signature)))
  (when bullet
    (setf signature (add-bullet-symbol signature)))
  (with-new-transitions signature
    (redex-transitions lhs)
    (reducible-transitions (non-constant-signature signature))
    (propagation-transitions signature)
    (matching-transitions (strict-subterms-list lhs))
    (make-table-automaton
     (make-table-transitions-from-global-sym-table)
     :name "aut-b"
     :finalstates (make-ordered-state-container (and finalstates (list (casted-r-state)))))))

(defgeneric  make-context-automaton (ground-term signature)
  (:documentation "build automaton recognizing terms of the form C[ground-term]"))

(defmethod make-context-automaton ((gt term) (signature signature))
  (assert (term-ground-p gt))
  (with-new-transitions signature
    (let ((qkey (make-qkey gt)))
     (add-transition-to-global-transitions (car qkey) (cdr qkey) (casted-r-state))
      (matching-transitions (subterms gt))
      (redex-transitions (list gt) nil)
      (reducible-transitions (non-constant-signature signature)))
    (make-table-automaton
     (make-table-transitions-from-global-sym-table)
     :name "context-l"
     :finalstates (make-state-container-from-state (casted-r-state)))))

(defgeneric propagation-transition-fun (root arg)
  (:method ((root abstract-symbol) (arg list))
    (declare (ignore root arg))
    *final-var-state*))

(defun universal-automaton (&optional (signature nil))
  (make-fly-automaton
   signature
   #'propagation-transition-fun
   :name "UNIVERSAL"))

(defun table-universal-automaton (signature)
  (compile-automaton
   (universal-automaton signature)))
