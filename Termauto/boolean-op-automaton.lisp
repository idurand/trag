;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defvar *complement-string* "-C")

(defun complement-name (name)
  (suffix-combine-name name *complement-string*))

(defgeneric complement-automaton (automaton)
  (:documentation "returns complement of AUTOMATON")
  (:method ((automaton table-automaton))
    (compile-automaton (complement-automaton (uncast-automaton automaton))))
  (:method ((automaton fly-automaton))
    (setq automaton (if (deterministic-p automaton)
			(duplicate-automaton automaton)
			(determinize-automaton automaton)))
    (setf (final-state-fun automaton) (complement (final-state-fun automaton)))
    (let ((transitions (transitions-of automaton)))
      (setf (sink-state transitions) (complement-sink (sink-state transitions))))
    (rename-object automaton (complement-name (name automaton)))))

(defgeneric inclusion-automaton (automaton1 automaton2)
  (:method ((aut1 abstract-automaton) (aut2 abstract-automaton))
    :documentation "automata must have the same signature
  returns t, nil if l(aut1) included in l(aut2) 
  returns nil, term otherwise"
    (let ((caut2 (complement-automaton aut2)))
      (intersection-emptiness aut1 caut2))))

(defgeneric equality-automaton-compatible (automaton1 automaton2)
  (:method ((aut1 abstract-automaton) (aut2 abstract-automaton))
    (multiple-value-bind (res term)  
	(inclusion-automaton aut1 aut2)
      (if res
	  (inclusion-automaton aut2 aut1)
	  (values nil term)))))

(defgeneric equality-automaton (abstract-automaton1 abstract-automaton2)
  (:method ((aut1 abstract-automaton) (aut2 abstract-automaton))
    (if (signature-proved-incompatible (signature aut1) (signature aut2))
	(values nil nil))
    (equality-automaton-compatible aut1 aut2)))
