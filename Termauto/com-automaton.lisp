;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defun get-automaton (name)
  (gethash name (automata-table (automata (current-spec)))))

(defun get-tautomaton (name)
  (gethash name (tautomata-table (tautomata (current-spec)))))

(defun list-automata ()
  (list-keys (automata-table (automata (current-spec)))))

(defgeneric automaton-empty (automaton))

(defmethod automaton-empty ((automaton abstract-automaton))
  (multiple-value-bind (res term)
      (automaton-emptiness automaton)
    (format-output "L(~A) " (automaton-name automaton))
    (if res
	(format-output "empty~%")
	(format-output "not empty witnessed by ~A~%" term))))

(defgeneric set-current-automaton (automaton))

(defun add-current-automaton ()
  (add-automaton (automaton (current-spec)) (current-spec)))

(defun add-current-tautomaton ()
  (add-tautomaton (tautomaton (current-spec)) (current-spec)))

(defmethod set-current-automaton ((automaton abstract-automaton))
  (setf (automaton (current-spec)) automaton)
  (add-current-automaton))

(defun set-current-tautomaton (tautomaton)
  (setf (tautomaton (current-spec)) tautomaton)
  (add-current-tautomaton))
