;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric constrained-automaton (automaton)
  (:documentation "automaton that runs discarding states that are not in the symbols annotation"))

(defgeneric constrained-transitions-fun-body (annotated-root attributed-states transitions))
(defmethod constrained-transitions-fun-body
    ((root annotated-symbol) (attributed-states list) (transitions abstract-transitions))
  (let ((attributed-target
	  (apply-transitions
	   (symbol-of root) attributed-states transitions))
	(run (annotation root)))
    (when attributed-target
      (let* ((target (unattribute-target attributed-target))
	     (inter (target-intersection target run)))
	(when inter
	  (if (or (abstract-state-p target) (abstract-state-p run))
	      attributed-target
	      (container-remove-if-not
	       (lambda (attributed-state)
		 (container-member (object-of attributed-state) inter))
	       attributed-target)))))))
	      
(defgeneric constrained-transitions-fun (transitions))
(defmethod constrained-transitions-fun ((transitions abstract-transitions))
  (lambda (root states)
    (constrained-transitions-fun-body root states transitions)))

(defmethod constrained-automaton ((automaton abstract-automaton))
  (let ((transitions (transitions-of automaton)))
    (make-fly-automaton-with-transitions
     (make-fly-transitions
      (signature transitions)
      (constrained-transitions-fun transitions)
      (deterministic-p automaton)
      (sink-state transitions))
     (final-state-fun automaton)
     (output-fun automaton)
     (format nil "~A-constrained" (name automaton))
     :precondition-automaton (precondition-automaton automaton))))
