;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defvar *next-count-fun*)
(defvar *symbol-fun*)

(defgeneric count-nodes-fun (symbol states))
(defmethod count-nodes-fun ((symbol abstract-symbol) (states list))
  (1+ (reduce #'+ states :initial-value 0 :key #'num)))

(defgeneric count-leaves-fun (symbol states))
(defmethod count-leaves-fun ((symbol abstract-symbol) (states list))
  (if (symbol-constant-p symbol)
      1
      (reduce #'+ states :initial-value 0 :key #'num)))

(defclass count-state (uncasted-state)
  ((num :initarg :num :reader num)))

(defun make-count-state (n)
  (make-instance 'count-state :num n))

(defmethod print-object ((co count-state) stream)
  (format stream "[~A]" (num co)))

(defmethod compare-object ((co1 count-state) (co2 count-state))
  (= (num co1) (num co2)))

(defgeneric count-transitions-fun (root arg))
(defmethod count-transitions-fun ((root abstract-symbol) (arg list))
  (make-count-state 
   (funcall *symbol-fun* root arg)))

(defun count-automaton (pred-final next-count-fun symbol-fun neutral-state-p signature)
  (make-fly-automaton
   signature
   (lambda (root states)
     (let ((*neutral-state-final-p* neutral-state-p)
	   (*symbol-fun* symbol-fun)
	   (*next-count-fun* next-count-fun))
       (count-transitions-fun root states)))
   :final-state-fun (lambda (state) (funcall pred-final (num state)))
   :name (format nil "COUNT")))

(defun sup-nodes-automaton (count signature)
  (rename-object
   (count-automaton
    (lambda (n) (print n) (>= n count))
    (lambda (n) (min n (1+ count)))
    #'count-nodes-fun
    nil
    signature)
   (format nil "NODES>=~A" count)))

(defun sup-leaves-automaton (count signature)
  (rename-object
   (count-automaton
    (lambda (n) (>= n count))
    (lambda (n) (min n (1+ count)))
    #'count-leaves-fun
    nil
    signature)
   (format nil "LEAVES>=~A" count)))
