(in-package :termauto)
(defvar *limit-transitions* t)
(defvar *limit-transitions-value* 80)

(defun toggle-limit-transitions ()
  (setf *limit-transitions* (not *limit-transitions*)))

(defun casted-state-or-container-p (target)
  (or (casted-state-p target) (ordered-state-container-p target)))

(defun make-empty-dag-table ()
  (make-hash-table :test #'eq))

(defgeneric dag-table-access (key dag-table))
(defmethod dag-table-access ((key list) (dag-table hash-table))
  (loop
     for e in key
     and target = dag-table then (gethash e target)
     unless target
     do (return)
       finally (return (gethash e target))))

(defgeneric dag-table-set (key rh dag-table))

(defmethod dag-table-set ((key list) (rh (eql nil)) (dag-table hash-table)))

(defmethod dag-table-set ((key list) rh (dag-table hash-table))
  (let ((state (car key)))
    (if (null (cdr key))
	(setf (gethash state dag-table) rh)
	(let ((target (gethash state dag-table)))
	  (unless target
	    (setf target (setf (gethash state dag-table) (make-empty-dag-table))))
	  (dag-table-set (cdr key) rh target))))
  dag-table)

(defgeneric dag-table-list (dag-table))
(defmethod dag-table-list ((dag-table hash-table))
  (let ((acc '()))
    (labels ((aux (target rfts)
	       ;;	       (format t "target ~A rfts ~A~%" target rfts)
	       (if (casted-state-or-container-p target)
		   (let ((lh (flat-term-from-key (reverse rfts))))
		     (mapcar
		      (lambda (q)
			(push (make-transition lh q) acc))
		      (container-contents target)))
		   (maphash
		    (lambda (key value)
		      (aux value (cons key rfts)))
		    target))))
      (aux dag-table ())
      acc)))

(defgeneric dag-table-number-of-transitions (dag-table))
(defmethod dag-table-number-of-transitions ((dag-table hash-table))
  (let ((n 0))
    (labels
	((aux (target)
	   (if (casted-state-or-container-p target)
	       (incf n (target-size target))
	       (maphash
		(lambda (key value)
		  (declare (ignore key))
		  (aux value))
		target))))
      (aux dag-table))
    n))

(defgeneric dag-table-copy (target))
(defmethod dag-table-copy ((target casted-state))
  target)

(defmethod dag-table-copy ((target ordered-state-container))
  target)

(defmethod dag-table-copy ((dag-table hash-table))
  (let ((new-dt (make-empty-dag-table)))
    (maphash
     (lambda (key target)
       (setf (gethash key new-dt) (dag-table-copy target)))
     dag-table)
    new-dt))

(defgeneric dag-table-union (target1 target2))
(defmethod dag-table-union (target1 target2)
  (target-union target1 target2))

(defmethod dag-table-union ((target1 casted-state) (target2 casted-state))
  (if (eq target1 target2)
      target1
      (target-union target1 target2)))

(defmethod dag-table-union ((target1 (eql nil)) target2)
  target2)

(defmethod dag-table-union (target1 (target2 (eql nil)))
  target1)


(defmethod dag-table-union ((dag-table1 hash-table) (dag-table2 hash-table))
  (let ((new-dt (dag-table-copy dag-table1)))
    (maphash
     (lambda (key value)
       (setf (gethash key new-dt)
	     (dag-table-union (dag-table-copy value) (gethash key new-dt))))
     dag-table2)
    new-dt))

(defgeneric dag-table-apply-states-mapping (dag-table states-mapping)
  (:method ((state null) (states-mapping list)) nil)
  (:method ((casted-state casted-state) (states-mapping list))
    (apply-states-mapping casted-state states-mapping))
  (:method ((ordered-container ordered-state-container) (states-mapping list))
    (apply-states-mapping ordered-container states-mapping))
  (:method ((dag-table hash-table) (states-mapping list))
    (let ((new-dag-table (make-empty-dag-table)))
    (maphash
     (lambda (key value)
;;       (format t "key ~A value ~A ~%" key value)
       (let ((newkey (apply-states-mapping key states-mapping)))
	 (setf (gethash newkey new-dag-table)
	       (dag-table-union
		(dag-table-apply-states-mapping value states-mapping)
		(gethash newkey new-dag-table)))))
     dag-table)
      new-dag-table)))

(defgeneric dag-table-cright-handsides (dag-table))
(defmethod dag-table-cright-handsides ((dag-table hash-table))
  (let ((crhs (make-empty-ordered-state-container)))
    (labels ((aux (table)
	       (maphash
		(lambda (k value)
		  (declare (ignore k))
		  (unless (null value)
		    (if (casted-state-or-container-p value)
			(loop for state in (container-unordered-contents value)
			     do (setf crhs (container-nadjoin state crhs)))
			(aux value))))
		table)))
      (aux dag-table))
    crhs))

(defgeneric dag-constant-table-show (dag-table &optional stream))
(defmethod dag-constant-table-show ((dag-table hash-table) &optional (stream t))
  (maphash
   (lambda (key value)
     (when (symbol-constant-p key)
       (format
	stream
	"~A -> ~A~%" key value)))
   dag-table))

(defgeneric dag-table-show (dag-table &optional stream))
(defmethod dag-table-show ((dag-table hash-table) &optional (stream t))
  (let ((n 0))
    (labels
	((aux (target tuple)
	   (if (casted-state-or-container-p target)
	       (progn
		 (format
		  stream
		  "~A -> ~A~%"
		  (flat-term-from-key (reverse tuple)) target)
		 (when
		     (and *limit-transitions* (>= (incf n) *limit-transitions-value*))
		   (format stream "...~%")
		   (return-from dag-table-show)))
	       (maphash
		(lambda (key value)
		  (aux value (cons key tuple)))
		target))))
      (aux dag-table ()))))

(defgeneric dag-table-filter-with-states (states dag-table))
(defmethod dag-table-filter-with-states ((states ordered-state-container) (dag-table hash-table))
  (let ((new-dt (make-empty-dag-table)))
    (labels
	((aux (target tuple)
	   (if (casted-state-or-container-p target)
	       (let ((ti (target-intersection states target)))
		 (unless (target-empty-p ti)
		   (dag-table-set (reverse tuple) ti new-dt)))
	       (maphash
		(lambda (k value)
;;		 (format *error-output* "aux ~A ~A~%" k states)
		  (when (has-only-marked-states k states)
		    (aux value (cons k tuple))))
		target))))
      (aux dag-table ()))
    new-dt))

(defgeneric dag-table-new-marked (dag-table marked))
(defmethod dag-table-new-marked ((dag-table hash-table) (marked ordered-state-container))
  (let ((tomark (make-empty-ordered-state-container)))
    (labels
	((aux (target rfts)
	   (if (casted-state-or-container-p target)
	       (let ((k (reverse rfts)))
		 (when (has-only-marked-states k marked)
		   (setf tomark (target-union tomark target))))
	       (maphash
		(lambda (k value)
		  (aux value (cons k rfts)))
		target))))
      (aux dag-table ()))
    tomark))

(defgeneric dag-table-co-new-marked (dag-table co-marked)
  (:method ((dag-table hash-table) (co-marked ordered-state-container))
  (let ((tomark (make-empty-ordered-state-container)))
    (labels
	((aux (target rfts)
	   (if (casted-state-or-container-p target)
	       (if (has-at-least-one-marked-state target co-marked)
		   (let ((key (reverse rfts)))
		     (unless (casted-state-p (car key))
		       (setf key (cdr key)))
		     (container-nunion tomark (make-ordered-state-container key))))
	       (maphash
		(lambda (key value)
		  (aux value (cons key rfts)))
		target))))
      (aux dag-table ()))
    tomark)))

(defgeneric dag-table-tuples-rh (dag-table))
(defmethod dag-table-tuples-rh ((dag-table hash-table))
  (let ((tuples-rh '()))
    (labels ((aux (target tuple)
	       (if (casted-state-or-container-p target)
		   (push (list (reverse tuple) target) tuples-rh)
		   (maphash
		    (lambda (key value) (aux value (cons key tuple)))
		    target))))
      (aux dag-table ())
      tuples-rh)))

(defgeneric dag-table-map (fun dag-table))
(defmethod dag-table-map ((fun function) (dag-table hash-table))
  (labels ((aux (target rtuple)
	     (if (casted-state-or-container-p target)
		 (funcall fun (reverse rtuple) target)
		 (maphash
		  (lambda (key value) (aux value (cons key rtuple)))
		  target))))
    (aux dag-table ())))

(defgeneric dag-table-things-from (dag-table f &optional op))
(defmethod dag-table-things-from ((dag-table hash-table) f &optional (op #'cons))
  (let ((things '()))
    (labels ((aux (target rtuple)
	       (if (casted-state-or-container-p target)
		   (setf things (funcall op (funcall f target rtuple) things))
		   (maphash
		    (lambda (key value) (aux value (cons key rtuple)))
		    target))))
      (aux dag-table ())
      things)))

(defgeneric dag-table-get-states-from (dag-table))
(defmethod dag-table-get-states-from ((dag-table hash-table))
  (let ((container (make-empty-ordered-state-container)))
    (labels ((aux (target)
	       (if (casted-state-or-container-p target)
		   (loop
		      for casted-state in (container-unordered-contents target)
		      do (setf container (container-nadjoin casted-state container)))
		   (maphash
		    (lambda (key value)
		      (declare (ignore key))
		      (aux value))
		    target))))
      (aux dag-table))
    container))

(defgeneric dag-table-tuples-rh (dag-table))
(defmethod dag-table-tuples-rh ((dag-table hash-table))
  (dag-table-things-from
   dag-table
   (lambda (target rtuple) (list (reverse rtuple) target))))

(defgeneric dag-table-tuples-from (dag-table))
(defmethod dag-table-tuples-from ((dag-table hash-table))
   (dag-table-things-from
    dag-table
    (lambda (target rtuple) (declare (ignore target)) (reverse rtuple))))
 
(defgeneric dag-table-uncommutative (dag-table))
(defmethod dag-table-uncommutative ((dag-table casted-state))
  dag-table)

(defmethod dag-table-uncommutative ((dag-table ordered-state-container))
  dag-table)

(defmethod dag-table-uncommutative ((dag-table hash-table))
  (let ((new-dt (make-empty-dag-table)))
    (mapc
     (lambda (tuple-rh)
;;       (format *trace-output* "~A~%" tuple-rh)
       (dag-table-set (first tuple-rh) (second tuple-rh) new-dt))
     (mapcan (lambda (tuple-rh)
	      (mapcar (lambda (states)
			(list states (second tuple-rh)))
		      (permutations (first tuple-rh))))
	     (dag-table-tuples-rh dag-table)))
    new-dt))

(defgeneric dag-table-to-commutative (dag-table))
(defmethod dag-table-to-commutative ((dag-table hash-table))
  (let ((new-dt (make-empty-dag-table)))
    (mapc
     (lambda (tuple-rh)
       (dag-table-set (sort-casted-states (first tuple-rh)) (second tuple-rh) new-dt))
     (dag-table-tuples-rh dag-table))
    new-dt))

(defgeneric dag-table-deterministic-p (target))

;;(defmethod dag-table-deterministic-p (target)
;;  (print (list 'dag-table-deterministic-p target))
;;  (target-deterministic-p target))

(defmethod dag-table-deterministic-p ((target casted-state))
  t)

(defmethod dag-table-deterministic-p ((target ordered-state-container))
  nil)

(defmethod dag-table-deterministic-p ((dag-table hash-table))
  (maphash
   (lambda (key value)
     (declare (ignore key))
     (unless (dag-table-deterministic-p value)
       (return-from dag-table-deterministic-p)))
   dag-table)
  t)

(defgeneric dag-table-equiv-mod-classes (tt1 tt2)
  (:documentation "for deterministic tables only"))

(defmethod dag-table-equiv-mod-classes ((dt1 (eql nil)) (dt2 (eql nil)))
  t)

(defmethod dag-table-equiv-mod-classes (dag-table1 (dag-table2 (eql nil)))
  nil)

(defmethod dag-table-equiv-mod-classes ((dag-table1 casted-state) (dag-table2 casted-state))
  (in-the-same-eqclass dag-table1 dag-table2))

(defmethod dag-table-equiv-mod-classes ((dag-table1 hash-table) (dag-table2 hash-table))
  (maphash
   (lambda (key value)
     (let ((value2 (gethash key dag-table2)))
       (unless (and value2 (dag-table-equiv-mod-classes value value2))
	 (return-from dag-table-equiv-mod-classes nil))))
   dag-table1)
  t)

(defgeneric dag-table-epsilon-closure (dag-table))
(defmethod dag-table-epsilon-closure ((dag-table hash-table))
  (warn "dag-table-epsilon-closure to be done \ doing nothing")
  dag-table)

;; uniquement sur tables de transitions non-commutatives
(defmethod possibly-equivalent-to-state (q qp (dag-table hash-table))
  ;;  (format *trace-output* "possibliy-equivalent-to-state q ~A qp ~A ~A~%" q qp table-transitions)
  (labels
      ((compat-states (position n table)
	 ;; (format *trace-output* "compat-states i=~A n=~A q=~A qp=~A ~%" i n q qp)
	 (cond
	   ((> n position) nil)
	   ((= position n)
	    (let ((target (gethash q table))
		  (targetp (gethash qp table)))
	      (when (and (null target) (null targetp))
		(return-from compat-states t))
	      (when (or (null target) (null targetp))
		(return-from compat-states nil))
	      (dag-table-equiv-mod-classes target targetp)))
	   (t ;; position > n
	    (maphash
	      (lambda (key value)
		(declare (ignore key))
		(unless (compat-states position (1+ n) value)
		  (return-from compat-states nil)))
	      table)
	    t))))
    (maphash
     (lambda (sym value)
       (dotimes (position (arity sym))
;;	 (format *trace-output* "sym=~A position = ~A ~%" sym position)
	 (unless (compat-states position 0 value)
	   (return-from possibly-equivalent-to-state nil))))
     dag-table)
;;    (format *trace-output* "OUI~%")
    t))
