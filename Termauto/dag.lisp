;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

;; you need to compile this file if you want to do dag optimisation

(defmethod transitions-compute-target-and-signal-rec :around
    ((term term) (transitions abstract-transitions) deterministic csf signal save-run)
  (if save-run
      (progn 
	(format *error-output* "dag optimisation ")
	(run term))
      (call-next-method)))
