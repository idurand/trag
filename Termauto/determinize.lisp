;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defun det-final-state-fun-body (gstate f)
  (if (typep gstate 'gstate)
      (container-find-if
       (lambda (state)
	 (final-state-p state f))
       (gstate-container gstate))
      (final-state-p gstate f)))

(defgeneric det-final-state-fun (abstract-automaton)
  (:method ((f abstract-automaton))
    (lambda (gstate) ;; gstate or nil
      (det-final-state-fun-body gstate f))))

(defgeneric det-transitions-fun-body (root states transitions-fun)
  (:method ((root abstract-symbol) (states list) (transitions-fun function))
      (let ((target 
	      (apply-transitions-fun-gft
	       root 
	       (mapcar (lambda (state) (gstate-container state)) states)
	       transitions-fun)))
	(make-gstate target))))
    
(defgeneric det-transitions-fun (fly-transitions)
  (:method ((transitions abstract-transitions))
    (lambda (root states) (det-transitions-fun-body root states (transitions-fun transitions)))))

(defgeneric determinize-automaton-to-fly (abstract-automaton)
  (:method ((automaton abstract-automaton))
    (when (deterministic-p automaton)
      (return-from determinize-automaton-to-fly automaton))
    (when *cast-inside*
      (setq automaton (cast-automaton automaton)))
    (let ((transitions (transitions-of automaton)))
      (make-fly-automaton-with-transitions
       (make-fly-transitions
	(signature transitions)
	(det-transitions-fun transitions)
	t ; deterministic
	(sink-state transitions))
       (det-final-state-fun automaton)
       (output-fun automaton)
       (format nil "~A-det" (name automaton))
       :precondition-automaton (precondition-automaton automaton)
       ))))

(defgeneric determinize-automaton-to-table (abstract-automaton)
  (:method ((automaton abstract-automaton))
    (compile-automaton (determinize-automaton-to-fly automaton))))

(defgeneric determinize-automaton-to-x (abstract-automaton fly)
  (:method ((automaton abstract-automaton) fly)
    (funcall
     (if fly #'determinize-automaton-to-fly #'determinize-automaton-to-table)
     automaton)))

(defgeneric determinize-automaton (abstract-automaton)
  (:method ((automaton abstract-automaton))
    (determinize-automaton-to-fly automaton))
  (:method ((automaton table-automaton))
    (determinize-automaton-to-table automaton)))

