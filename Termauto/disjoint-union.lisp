;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defun intersection-precondition-automata (automata)
  (setq automata (remove nil automata))
  (when automata 
    (intersection-automata automata)))

(defgeneric disjoint-union-automaton-to-fly (automaton1 automaton2)
  (:method ((a1 table-automaton) (a2 table-automaton))
    (disjoint-union-automaton
     (uncast-automaton a1) (uncast-automaton a2))))

(defgeneric disjoint-union-automaton (automaton1 automaton2)
  (:method ((a1 table-automaton) (a2 table-automaton))
    (compile-automaton (disjoint-union-automaton-to-fly a1 a2)))
  (:method ((a1 fly-automaton) (a2 fly-automaton))
    (assert (not (casted-p a1)))
    (assert (not (casted-p a2)))
    (let ((signature (merge-signature (signature a1) (signature a2))))
      (assert signature)
      (let ((ia1 (index-states-automaton a1 0)) ;; indexed copie
	    (ia2 (index-states-automaton a2 1))) ;; indexed copie
	(make-fly-automaton-with-transitions
	 (let ((transitions1 (transitions-of ia1))
	       (transitions2 (transitions-of ia2)))
	   (assert (null (sink-state transitions1)))
	   (assert (null (sink-state transitions2)))
	   (make-fly-transitions
	    signature
	    (disjoint-union-transitions-fun transitions1 transitions2)
	    (and (signature-empty-p
		  (signature-intersection (signature ia1) (signature ia2)))
		 ;; not deterministic sauf peut-etre s'il n'ont pas de constante commune
		 (deterministic-p ia1) (deterministic-p ia2))
	    nil))
	 (lambda (state)
	   (or (and (state-with-index-p state 0) (final-state-p state ia1))
	       (and (state-with-index-p state 1) (final-state-p state ia2))))
	 #'identity
	 (format nil "DU(~A ~A)" (automaton-name a1) (automaton-name a2))
	 :precondition-automaton (intersection-precondition-automata
				  (list (precondition-automaton a1)
					(precondition-automaton a2))))))))

(defgeneric disjoint-union-automata-gen (automata)
  (:method ((automata list))
    (assert (notany #'casted-p automata))
    (let ((signature (apply #'merge-signature (mapcar #'signature automata))))
      (assert signature)
      (setf automata
	    (loop
	      for i from 0
	      for f in automata
	      collect (index-states-automaton f i)))
      (let ((transitionss (mapcar #'transitions-of automata)))
	(assert (notany #'sink-state transitionss))
	(make-fly-automaton-with-transitions
	 (make-fly-transitions
	  signature
	  (disjoint-union-gen-transitions-fun transitionss)
	  ;; not deterministic sauf peut-etre s'il n'ont pas de constante commune
	  (and  (signature-empty-p
		 (reduce #'signature-intersection (mapcar #'signature automata)))
		(every #'deterministic-p automata))
	  nil)
	 (lambda (state)
	   (let ((index -1))
	     (some
	      (lambda (f)
		(and (state-with-index-p state (incf index))
		     (final-state-p state f)))
	      automata)))
	 #'identity
	 (format nil "DU(~A)"
		 (apply #'concatenate
			'string
			(mapcar (lambda (f)
				  (format nil "~A " (name f)))
				automata)))
	  :precondition-automaton
	  (intersection-precondition-automata (mapcar  #'precondition-automaton automata)))))))
