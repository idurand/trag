;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

;; side effect on *automaton-states-table*
(defgeneric duplicate-state (casted-state)
  (:method ((casted-state casted-state))
    (let ((ncs (cast-state (in-state casted-state))))
      (setf (state-number ncs) (state-number casted-state))
      ncs)))

(defgeneric duplicate-states (ordered-state-container)
  (:method ((container ordered-state-container))
    (with-new-states-table
      (mapcar (lambda (state)
		(cons state (duplicate-state state)))
	      (container-contents container)))))

(defgeneric duplicate-table-transitions (transitions states-mapping)
  (:method ((transitions table-transitions) (states-mapping list))
    (apply-states-mapping transitions states-mapping)))

(defmethod duplicate-automaton ((automaton table-automaton))
  (let* ((states-mapping (duplicate-states (get-states automaton)))
	 (transitions (transitions-of automaton))
	   (new-transitions (duplicate-table-transitions transitions states-mapping))
	   (finalstates (apply-states-mapping (get-finalstates automaton) states-mapping))
	   (a (make-table-automaton
	       new-transitions
	       :finalstates finalstates
	       :name (name automaton)
	       :precondition-automaton (precondition-automaton automaton))))
      (when (equivalence-classes-boundp automaton)
	(setf (get-equivalence-classes a)
	      (apply-states-mapping (get-equivalence-classes automaton) states-mapping)))
      (when (slot-boundp automaton 'reduced)
	(setf (get-reduced a) (get-reduced automaton)))
      (when (slot-boundp automaton 'epsilon)
	(setf (get-epsilon a) (get-epsilon automaton)))
      a))
