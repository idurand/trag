;; LICENSE: see trag/LICENSE.text
;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric term-strictly-smaller-p (term1 term2)
  (:method ((term1 term) (term2 term))
    (let ((size1 (term-size term1))
	  (size2 (term-size term2))
	  (depth1 (term-depth term1))
	  (depth2 (term-depth term2)))
      (or (< size1 size2) (and (= size1 size2) (< depth1 depth2))))))

(defgeneric state-term-table (stop-fun abstract-automaton)
  (:method (stop-fun (automaton abstract-automaton))
    (assert (casted-p automaton))
    (let* ((transitions (transitions-of automaton))
	   (signature (signature transitions))
	   (csign (constant-signature signature))
	   (ncsign (non-constant-signature signature))
	   (ncsym (signature-symbols ncsign))
	   (amax (max-arity ncsign))
	   (table (make-hash-table :test #'eq))
	   (states (make-empty-ordered-state-container))
	   (newstates (make-empty-ordered-state-container)))
      (with-states-table (transitions-of automaton)
	(labels
	    ((add (sym arg)
	       (let ((target (apply-transitions sym arg transitions)))
		 (loop
		   for q in (target-contents target)
		   do (let ((new-term (build-term sym (mapcar (lambda (s) (gethash s table)) arg)))
			    (old-term (gethash q table)))
			(when (or (null old-term) (term-strictly-smaller-p new-term old-term))
			  (setf (gethash q table) new-term)
			  (container-nunion newstates (target-to-container q)))))))
	     (fs-foundp ()
	       (container-find-if (lambda (state) (funcall stop-fun state))
				  newstates)))
	  (loop
	    for sym in (reverse (signature-symbols csign))
	    do (add sym nil))
	  (loop
	    for fs = (fs-foundp)
	    for oldstates = (container-contents states)
	    when fs do (return-from state-term-table (values table fs))
	      do (container-nunion states newstates)
	    until (target-empty-p newstates)
	    do (setf newstates (make-empty-ordered-state-container))
	    do (let ((afa (arrange-for-arities-and-filter
			   (container-unordered-contents states) amax oldstates ncsign)))
		 (loop for sym in ncsym
		       do (loop for arg in (aref afa (arity sym))
				do (add sym arg))))
	    finally (return (values table nil))))))))

(defgeneric term-to-given-state (abstract-automaton state)
  (:method ((a abstract-automaton) (state abstract-state))
    (unless (casted-p a) (setf a (cast-automaton a)))
    (gethash state (state-term-table
		    (lambda (s) (eq s state)) a))))

(defgeneric non-emptiness-witness (abstract-automaton)
  (:method ((a abstract-automaton))
    (let ((precondition-automaton (precondition-automaton a)))
      (when precondition-automaton
	(setq a (intersection-automaton a precondition-automaton))))
    (unless (casted-p a) (setq a (cast-automaton a)))
    (multiple-value-bind (table state)
	(state-term-table
	 (lambda (casted-state) (final-state-p casted-state a))
	 a)
      (when state
	(gethash state table)))))

(defgeneric automaton-emptiness (automaton)
  (:documentation
   "Returns t, nil if the recognized language is empty.
    Returns nil, term if the recognized language is not empty.")
  (:method ((a abstract-automaton))
    (let ((term (non-emptiness-witness a)))
      (if term
	  (values nil term)
	  (values t nil)))))

(defmacro def-non-emptiness-witness (var automaton)
  `(defparameter ,var ,(non-emptiness-witness automaton)))
