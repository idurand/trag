;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defmacro delay (expr) `(lambda () ,expr))

(defgeneric target-enumerator-ra (symbol arg transitions)
  (:documentation "does not treat sink states")
  (:method
      ((root constant-mixin) (arg (eql nil)) (transitions abstract-transitions))
    (make-lazy-enumerator
     (delay
      (make-list-enumerator ;; here no duplicates because containers are sets
	 (container-unordered-contents
	  (apply-transitions root nil transitions))))))
  (:method
      ((root abstract-symbol) (arg list) (transitions abstract-transitions))
    (let ((targets
	    (apply
	     #'make-enumerator-list
	     (mapcar
	      (lambda (arg)
		(target-enumerator-ra (root arg) (arg arg) transitions))
	      arg))))
      (make-append-enumerator
       (make-funcall-enumerator
	(lambda (states)
	  (container-unordered-contents
	   (apply-transitions
	    root states transitions)))
	targets)))))

(defgeneric transitions-target-enumerator (term transitions)
  (:method ((term term) (transitions abstract-transitions))
    (target-enumerator-ra (root term) (arg term) transitions)))

(defgeneric target-enumerator (term automaton &key final)
  (:method ((term term) (automaton abstract-automaton) &key final)
    (let ((enum (transitions-target-enumerator term (transitions-of automaton))))
      (if final
	  (make-filter-enumerator
	   enum
	   (lambda (state) (final-state-p state automaton)))
	  enum))))

(defgeneric final-target-enumerator (term abstract-automaton)
  (:method ((term term) (automaton abstract-automaton))
    (target-enumerator term (transitions-of automaton) :final t)))

(defgeneric enum-compute-target (term automaton &key just-one final)
  (:method
      ((term term) (automaton abstract-automaton) &key (just-one t) final)
    (let ((enum (target-enumerator term automaton :final final)))
      (if (next-element-p enum)
	  (if just-one
	      (next-element enum)
	      (let ((states (collect-enum enum)))
		(if (endp (cdr states))
		    (car states)
		    (make-appropriate-state-container
		     (collect-enum (target-enumerator term automaton))))))
	  nil))))

(defgeneric enum-recognized-p (term automaton)
  (:documentation
   "true if TERM is recongized by AUTOMATON (using enumeration)")
  (:method ((term term) (automaton abstract-automaton))
    (recognized-p term automaton :enum t)))
