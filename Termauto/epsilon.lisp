;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; Methods used by the reduction and the epsilon closure of an automaton.
(in-package :termauto)

(defmethod epsilon-p (automaton)
  (epsilon-p (transitions-of automaton)))

(defgeneric nepsilon-closure-automaton (table-automaton))
(defmethod nepsilon-closure-automaton ((automaton table-automaton))
  (when (epsilon-p automaton)
    (setf (transitions-of automaton)
	  (epsilon-closure (transitions-of automaton))))
  automaton)

(defmethod epsilon-closure-automaton ((automaton table-automaton))
  (nreduce-automaton
   (nepsilon-closure-automaton (duplicate-automaton automaton))))
