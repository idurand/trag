;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric equivalence-classes-automaton (table-automaton)
  (:method ((automaton table-automaton))
    (assert (and (reduced-p automaton)
		 (deterministic-p automaton)))
    (let ((states (get-states automaton)))
      (when (container-empty-p states)
	(return-from equivalence-classes-automaton '()))
      (let ((finalstates (get-finalstates automaton))
	    (transitions (transitions-of automaton)))
	(when (container-empty-p finalstates)
	    (return-from equivalence-classes-automaton
	      (container-mapcar
	       (lambda (state)
		 (make-ordered-state-container (list state)))
	       states)))
	(let ((numbered (states-numbered-p automaton)))
	  (unless numbered (number-states automaton))
	  (with-states-vector (get-states automaton)
	    (let*
		((*eqclasses* (make-array *states-vector-len*))
		 (*eqindex* 0)
		 (equivalence-classes
		   (compute-equivalence-classes
		    (make-sstates finalstates)
		    (make-sstates ;; nonfinalstates
		     (container-difference states finalstates))
		    (sym-table transitions)))
		 (containers (mapcar #'sstates-to-state-container equivalence-classes)))
	      (unless numbered
		(name-states automaton))
	      ;; sort in order to have the representatives ordered according to state-internal-number
	      (sort containers
		    #'< :key
		    (lambda (c)
		      (object-key (car (container-contents c))))))))))))
