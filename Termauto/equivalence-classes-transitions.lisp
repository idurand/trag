;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

;; si on suit les memes arguments on arrive dans des etats d'une meme classe
(defgeneric possibly-equivalent-to-class (q newclass transitions))
(defmethod possibly-equivalent-to-class (q newclass (dag-table hash-table))
  (loop
     for qp in (sstates-contents newclass)
     do (unless
	    (possibly-equivalent-to-state
	     (sstates-realstate q)
	     (sstates-realstate qp) dag-table)
	  (return-from possibly-equivalent-to-class nil))
     finally (return t)))

(defgeneric partition-eqclass (eqclass dag-table)
  (:documentation ""))

(defmethod partition-eqclass (eqclass (dag-table hash-table))
;;   (format *trace-output* "partition-eqclass " )
;;   (print-sstates eqclass *trace-output*) 
;;   (format *trace-output* "~%" )
;; (print-eqclasses *trace-output*)
  (loop
     with partition = nil
     for q in (sstates-contents eqclass)
     do (let ((ncl (find-if
		    (lambda (newclass)
		      (possibly-equivalent-to-class q newclass dag-table))
		    partition)))
	  (if ncl
	      (setf ncl (sstates-nadjoin q ncl))
	      (push (make-sstates-from-state q) partition)))
;;	  (format *error-output* "q~A partition ~A ~%" q partition)
     finally (return partition)))

;;;; il faut enlever des classes celle qu'on traite et mettre a la place les newclasses qui lui correspondent
(defgeneric refine-partition (dag-table)
  (:documentation "replace each eqclass by its refined set of eqclasses"))

(defmethod refine-partition ((dag-table hash-table))
;;  (print-eqclasses)
  (dotimes (i *eqindex*)
    (let ((eqclass (aref *eqclasses* i)))
      (unless (sstates-singleton-p eqclass))
	(let ((partition (partition-eqclass eqclass dag-table)))
	  (set-eqclass i (first partition))
	  (when (cdr partition)
	    (dolist (c (cdr partition))
	      (add-eqclass c)))))))

(defgeneric compute-equivalence-classes (finalstates nonfinalstates transitions)
  (:method (finalstates nonfinalstates (sym-table sym-table))
    (add-eqclass finalstates)
    ;;  (format *trace-output* "nonfinal ~A ~%" nonfinalstates)
    (unless (empty-sstates-p nonfinalstates)
      (add-eqclass nonfinalstates))
    (do ((old-index (- *eqindex* 1)))
	((= *eqindex* old-index) (loop for i from 0 below *eqindex*  collect (aref *eqclasses* i)))
      (setf old-index *eqindex*)
      (refine-partition (dag-table (sym-table-uncommutative sym-table))))))
