;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)
;;; avec transitions
;;; avec tableau de blocs
;;; standard minimization algorithm in O(N²)

(defvar *eqclasses*)
(defvar *eqindex*)

(defun set-eqclass (i eqclass)
  (assert (not (empty-sstates-p eqclass)))
  (setf (aref *eqclasses* i) eqclass))

(defun add-eqclass (eqclass)
  (assert (not (empty-sstates-p eqclass)))
  (setf (aref *eqclasses* *eqindex*) eqclass)
  (incf *eqindex*))

(defun in-the-same-eqclass (q1 q2)
  (or
   (eq q1 q2)
   (and
    q1 ;; permet de gerer le cas ou l automate est incomplet
    q2
    (loop
       for i from 0 below *eqindex*
       when (and (in-sstates q1 (aref *eqclasses* i))
		 (in-sstates q2 (aref *eqclasses* i)))
       do (return-from in-the-same-eqclass t)))))

(defun print-eqclasses (&optional (stream t))
  (loop
     for i from 0 below *eqindex*
;;     do (format stream "~A " (aref *eqclasses* i)))
     do (print-sstates (aref *eqclasses* i)))
  (format stream "~%"))
