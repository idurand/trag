;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defclass fly-automaton (abstract-automaton)
  ((final-state-fun :initarg :final-state-fun :accessor final-state-fun)))

(defgeneric make-fly-automaton-with-transitions (transitions final-state-fun output-fun name &key precondition-automaton)
  (:method ((transitions abstract-transitions) (final-state-fun function) (output-fun function) (name string) &key precondition-automaton)
    (make-instance 'fly-automaton
		   :transitions transitions
		   :final-state-fun final-state-fun
		   :output-fun output-fun
		   :name name
		   :precondition-automaton precondition-automaton
		   )))

(defun make-fly-automaton
    (signature transitions-fun
     &key (final-state-fun #'state-final-p)
       (output-fun #'identity) name (deterministic t) (cast *cast*) sink-state precondition-automaton)
  (let ((transitions
	  (make-fly-transitions signature transitions-fun deterministic sink-state)))
    (when cast
      (setq transitions (cast-transitions transitions)))
    (make-fly-automaton-with-transitions
     transitions final-state-fun output-fun name
     :precondition-automaton precondition-automaton)))

(defgeneric duplicate-automaton (a)
  (:method ((f fly-automaton))
    (make-fly-automaton-with-transitions
     (duplicate-transitions (transitions-of f))
     (final-state-fun f)
     (output-fun f)
     (name f)
     :precondition-automaton (precondition-automaton f))))

(defgeneric uncast-automaton (automaton)
  (:documentation "equivalent automaton which returns uncasted-states, so necessarily a fly-automaton")
  (:method((f fly-automaton))
    (make-fly-automaton-with-transitions
     (uncast-transitions (transitions-of f))
     (final-state-fun f)
     (output-fun f)
     (name f)
     :precondition-automaton (precondition-automaton f))))

(defgeneric cast-automaton (automaton)
  (:documentation "same automaton with states-table in transitions")
  (:method ((f fly-automaton))
    (if (casted-p f)
	f
	(make-fly-automaton-with-transitions
	 (cast-transitions (transitions-of f))
	 (lambda (casted-state)
	   (final-state-p (in-state casted-state) f))
	 (output-fun f)
	 (name f)
	 :precondition-automaton (precondition-automaton f)))))

(defmethod reduced-p ((fly-automaton fly-automaton))
  (declare (ignore fly-automaton)) t)

(defmethod nreduce-automaton ((automaton fly-automaton))
  (warn "why reduce a fly-automaton?")
  automaton)

(defun state-with-index-p (state index)
  (= (state-index state) index))

(defmethod apply-signature-mapping ((f fly-automaton))
  (let ((new (duplicate-automaton f)))
    (setf (transitions-of new) (apply-signature-mapping (transitions-of f)))
    (setf (precondition-automaton new) (apply-signature-mapping (precondition-automaton new)))
    new))

(defgeneric nindex-states-automaton (automaton state-index)
  (:method ((f fly-automaton) (index integer))
    (setf (transitions-of f)
	  (index-fly-transitions (transitions-of f) index))
    (let ((final-state-fun (final-state-fun f)))
      (setf (final-state-fun f)
	    (lambda (state)
	      (funcall final-state-fun (in-state state)))))
    f))

(defmethod show-constant-transitions ((f fly-automaton) &optional (stream t))
  (show-constant-transitions
   (compile-automaton f)
   stream))

(defmethod add-empty-symbols ((f fly-automaton))
  (let ((nf (duplicate-automaton f)))
    (setf (signature (transitions-of nf)) (add-empty-symbols (signature nf)))
    nf))

(defgeneric nautomaton-without-symbol (symbol automaton)
  (:method ((symbol abstract-symbol) (automaton fly-automaton))
    (nautomaton-without-signature (make-signature (list symbol)) automaton)))

(defgeneric nautomaton-without-signature (signature automaton)
  (:method ((signature abstract-signature) (automaton fly-automaton))
;;    (warn "nautomaton without-signature fly")
    (setf (signature (transitions-of automaton)) (signature-difference (signature automaton) signature))
    automaton))

(defgeneric automaton-without-signature (signature automaton)
  (:method ((signature abstract-signature) (automaton fly-automaton))
    (nautomaton-without-signature signature (duplicate-automaton automaton))))

(defgeneric automaton-without-symbol (symbol automaton)
  (:method ((symbol abstract-symbol) (automaton fly-automaton))
    (nautomaton-without-symbol symbol (duplicate-automaton automaton))))
