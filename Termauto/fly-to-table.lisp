;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric compute-constant-transitions (fly-transitions constant-signature)
  (:method ((fly-transitions fly-transitions) (constant-signature signature))
    (mapc
     (lambda (root)
       ;; we use transitions-fun because we do not want to put sink rules in the table
       (let ((target (apply-transitions-fun root '() (transitions-fun fly-transitions))))
	 (unless (sink-p target)
	   (global-transition-function-set root '() target))))
     (signature-symbols constant-signature))))

(defgeneric add-transitions-for-one-symbol (fly-transitions symbol afai)
  (:method ((fly-transitions fly-transitions) symbol afai)
    (mapc
     (lambda (arg)
       (let ((target
	       (apply-transitions-fun
		symbol (mapcar #'uncast-target arg) (transitions-fun fly-transitions))))
	 (unless (sink-p target)
	   (global-transition-function-set symbol arg target))))
     afai)))

(defgeneric add-transitions (fly-transitions signature states newstates)
  (:method ((fly-transitions fly-transitions) (ncsign signature)
	    (states ordered-state-container) (newstates ordered-state-container))
    (let ((afa
	    (arrange-for-arities-and-filter
	     (append (container-contents states)
		     (container-contents newstates))
	     (max-arity ncsign) (container-contents states) ncsign)))
      (mapc
       (lambda (symbol)
	 (add-transitions-for-one-symbol
	  fly-transitions symbol (aref afa (arity symbol))))
       (signature-symbols ncsign)))))

(defgeneric compute-transitions (fly-transitions non-constant-signature)
  (:method ((fly-transitions fly-transitions) (non-constant-signature signature))
    (do* ((states (make-empty-ordered-state-container))
	  (newstates
	   (states-from-states-table *automaton-states-table*)
	   (container-difference (states-from-states-table *automaton-states-table*)
				 states)))
	 ((container-empty-p newstates))
      (add-transitions
       fly-transitions
       non-constant-signature
       states newstates)
      (container-nunion states newstates))
    (make-table-transitions-from-global-sym-table (sink-state fly-transitions))))

(defgeneric compute-all-transitions (fly-transitions signature)
  (:method ((fly-transitions fly-transitions) (signature signature))
    (compute-constant-transitions
     fly-transitions (constant-signature signature))
    (compute-transitions 
     fly-transitions (non-constant-signature signature))))

(defgeneric compile-transitions (fly-transitions)
  (:method ((fly-transitions fly-transitions))
    (let ((signature (signature fly-transitions)))
      (assert (signature-finite-p signature))
      (setf fly-transitions (uncast-transitions fly-transitions))
      (with-new-transitions signature
	(compute-all-transitions fly-transitions signature)
	(make-table-transitions-from-global-sym-table (sink-state fly-transitions))))))

(defvar *intersect-with-precondition*
  nil
  "if true the precondition-automaton will be intersected with the automataon before compilation")

(defgeneric compile-automaton (fly-automaton)
  (:method ((f fly-automaton))
    (when *intersect-with-precondition*
      (let ((precondition-automaton (precondition-automaton f)))
	(when precondition-automaton
	  (setq f (intersection-automaton precondition-automaton f)))))
    (let* ((table-transitions (compile-transitions (transitions-of f)))
	   (finalstates (container-remove-if-not
			 (lambda (state)
			   (final-state-p (in-state state) f))
			 (get-states table-transitions))))
      (when (complete-sym-table-p (signature table-transitions)
      				  (sym-table table-transitions))
      	(nmerge-sinks table-transitions finalstates))
      (make-table-automaton
       table-transitions
       :name (name f)
       :reduced t
       :finalstates finalstates
       :precondition-automaton (if *intersect-with-precondition* nil (precondition-automaton f)))))
  (:method ((automaton table-automaton)) automaton))
