;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defclass fly-transitions (abstract-transitions)
  ((transitions-fun :initarg :transitions-fun :accessor transitions-fun)
   (deterministic :reader deterministic-p :initarg :deterministic)))

;; transitions-fun plain transition function does not handle sink
;; return a state, a state-container or nil

(defclass fly-casting-transitions (cast-states-mixin fly-transitions) ())

(defmethod casted-transitions-p ((transitions cast-states-mixin)) t)

(defmethod print-object ((transitions fly-transitions) stream)
  (format stream "~A" (type-of transitions)))

(defun make-fly-transitions 
    (signature transitions-fun deterministic sink-state &key (transitions-type 'fly-transitions))
  (make-instance transitions-type
		 :signature signature :transitions-fun transitions-fun
		 :deterministic deterministic :sink-state sink-state))

(defgeneric convert-transitions (transitions transitions-type)
  (:method ((transitions abstract-transitions) transitions-type)
    (make-fly-transitions
     (signature transitions)
     (transitions-fun transitions)
     (deterministic-p transitions)
     (sink-state transitions)
     :transitions-type transitions-type)))

(defgeneric cast-transitions (fly-transitions)
  (:method ((fly-transitions fly-transitions))
    (if (casted-transitions-p fly-transitions)
	fly-transitions
	(convert-transitions fly-transitions 'fly-casting-transitions))))

(defgeneric uncast-transitions (fly-transitions)
  (:method ((fly-transitions fly-transitions)) fly-transitions)
  (:method ((fly-transitions fly-casting-transitions))
    (convert-transitions fly-transitions 'fly-transitions)))

(defgeneric duplicate-transitions (fly-transitions)
  (:method ((fly-transitions fly-transitions))
    (convert-transitions fly-transitions (class-of fly-transitions))))

(defmethod transitions-fun ((fly-transitions cast-states-mixin))
  (lambda (root casted-states)
    (let ((target (funcall
		   (call-next-method)
		   root (mapcar #'in-state casted-states))))
      (when target
	(with-states-table fly-transitions (cast-target target))))))

(defgeneric apply-signature-mapping-transitions-fun-body (root states transitions inverse-signature-mapping)
  (:method
      ((root abstract-symbol) (states list) (transitions abstract-transitions) inverse-signature-mapping)
    (let ((*signature-mapping* inverse-signature-mapping))
      (loop with target := nil
            for r in (apply-signature-mapping root)
            for newtarget := (apply-transitions r states transitions)
            when (eq newtarget *success-state*) return nil
            when newtarget do (setf target (target-nunion target newtarget))
            finally (return target)))))

(defgeneric apply-signature-mapping-transitions-fun (fly-transitions)
  (:method ((transitions fly-transitions))
    (let ((inverse-signature-mapping *inverse-signature-mapping*))
      (lambda (root states)
	(apply-signature-mapping-transitions-fun-body root states transitions inverse-signature-mapping)))))

(defmethod apply-signature-mapping ((fly-transitions fly-transitions))
  (make-fly-transitions
   (apply-signature-mapping (signature fly-transitions))
   (apply-signature-mapping-transitions-fun fly-transitions)
   (and (deterministic-p fly-transitions)
	(injective-mapping-p *signature-mapping*))
   (sink-state fly-transitions)
   :transitions-type 'fly-transitions))

(defgeneric complement-sink (sink)
  (:method ((sink null)) *success-state*)
  (:method ((sink casted-state))
    (setf (in-state sink) (complement-sink (in-state sink))) sink)
  (:method ((sink (eql *success-state*))) nil)
  (:method ((sink t)) (warn "unknown sink: ~A~%" sink)))

(defgeneric complement-transitions (transitions)
  (:method ((transitions fly-transitions))
    (assert (deterministic-p transitions))
    (make-fly-transitions
     (signature transitions)
     (transitions-fun transitions)
     t
     (complement-sink (sink-state transitions))
     :transitions-type (class-of transitions))))

(defgeneric index-transitions-fun (fly-transitions state-index)
  (:method ((fly-transitions fly-transitions) (state-index integer))
    (lambda (root indexed-states)
      (index-target
       (apply-transitions-fun root (mapcar #'in-state indexed-states) (transitions-fun fly-transitions))
       state-index))))

(defgeneric index-fly-transitions (fly-transitions state-index)
  (:method ((fly-transitions fly-transitions) (state-index integer))
    (make-fly-transitions
     (signature fly-transitions)
     (index-transitions-fun fly-transitions state-index)
     (deterministic-p fly-transitions)
     (sink-state fly-transitions))))

(defun make-p-target (&rest targets)
  (if (every (lambda (target) (or (sink-p target) (abstract-state-p target))) targets)
      (make-tuple-state targets)
      (make-target
       (mapcar #'make-tuple-state
	       (cartesian-product
		(mapcar (lambda (target)
			  (if (container-p target)
			      (container-unordered-contents target)
			      (list target)))
			targets))))))

(defgeneric product-transitions-fun-body (root pstates transitions1 transitions2 union)
  (:method ((root abstract-symbol) (pstates list)
	    (transitions1 abstract-transitions)
	    (transitions2 abstract-transitions) union)
    (let* ((states1 (mapcar (lambda (pstate) (first (tuple pstate))) pstates))
           (states2 (mapcar (lambda (pstate) (second (tuple pstate))) pstates))
           (rhs1 (apply-transitions root states1 transitions1))
           (rhs2 (apply-transitions root states2 transitions2)))
      (unless
        (if union
          (or 
            (or (eq rhs1 *success-state*) (eq rhs2 *success-state*))
            (and (null rhs1) (null rhs2)))
          (or 
            (and (eq rhs1 *success-state*) (eq rhs2 *success-state*))
            (or (null rhs1) (null rhs2))))
        (make-p-target rhs1 rhs2)))))

(defun product-transitions-fun-gen-body (root pstates ltransitions union)
  (assert (every (lambda (tr) (typep tr 'abstract-transitions)) ltransitions))
  (let ((rhs (if (endp pstates)
		 (mapcar (lambda (transitions)
			   (apply-transitions root '() transitions))
			 ltransitions)
		 (loop for transitions in ltransitions
		       for tuples = (mapcar #'tuple pstates)
			 then (mapcar #'cdr tuples)
		       collect
		       (let ((states (mapcar #'car tuples)))
			 (and (notany #'null states)
			      (apply-transitions root states transitions)))))))
    (when (every #'null rhs)
      (return-from product-transitions-fun-gen-body))
    (when (or
	   ;; union
	   (and union (not (member *success-state* rhs)))
	   (and (not union) (not (member nil rhs))))
      (apply #'make-p-target rhs))))

(defgeneric product-transitions-fun (transitions1 transitions2 &key union)
  (:method ((transitions1 abstract-transitions) (transitions2 abstract-transitions) &key (union nil))
    (lambda (root pstates)
      (product-transitions-fun-body root pstates transitions1 transitions2 union))))

(defun product-transitions-fun-gen (union &rest ltransitions)
  (lambda (root pstates)
    (product-transitions-fun-gen-body root pstates ltransitions union)))

(defun all-states-with-index-p (states index)
  (every (lambda (state) (= (state-index state) index)) states))

(defgeneric disjoint-union-transitions-fun (transitions1 transitions2)
  (:method ((transitions1 abstract-transitions) (transitions2 abstract-transitions))
    (lambda (root states)
      (if states
	  (let ((transitions
		  (cond
		    ((all-states-with-index-p states 0) transitions1)
		    ((all-states-with-index-p states 1) transitions2)
		    (t nil))))
	    (when transitions
	      (apply-transitions-fun root states (transitions-fun transitions))))
	  (target-union
	   (apply-transitions-fun root nil (transitions-fun transitions1))
	   (apply-transitions-fun root nil (transitions-fun transitions2)))))))

(defgeneric disjoint-union-gen-transitions-fun (transitionss)
  (:method ((transitionss list))
    (lambda (root states)
      (if states
	  (let ((transitions
		  (loop
		    for index from 0
		    for tr in transitionss
		    when (all-states-with-index-p states index)
		      do (return tr))))
	    (when transitions
	      (apply-transitions-fun root states (transitions-fun transitions))))
	  (reduce #'target-union
		  (mapcar (lambda (tr)
			    (apply-transitions-fun root nil (transitions-fun tr)))
			  transitionss))))))

(defmethod add-empty-symbols ((f fly-transitions))
  (let ((new-transitions (duplicate-transitions f)))
    (setf (signature new-transitions) (add-empty-symbols (signature new-transitions)))
    new-transitions))

;;;;;;;;;;;;;;;;;;;;;;; A TESTER ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar *max-number-of-states* 200000)
(defvar *clean-states-table* t)

;; comment to avoid overhead when not using it

(defmethod apply-transitions :after
    ((root abstract-symbol) (states list) (transitions fly-casting-transitions))
  (when (and
	 *clean-states-table*
	 (> (hash-table-count (states-table transitions)) *max-number-of-states*))
    (reset-states-table transitions)))
