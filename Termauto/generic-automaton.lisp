;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric generic-transition-fun (root arg)
  (:method ((root abstract-symbol) (arg list))
    (declare (ignore root arg))
    *final-neutral-state*))

(defun generic-automaton (&optional (signature nil))
  (make-fly-automaton signature #'generic-transition-fun :name "GENERIC"))

(defun height-automaton (&optional (signature nil))
  (attribute-automaton (generic-automaton signature) *height-afun*))

(defun table-generic-automaton (signature)
  (compile-automaton (generic-automaton signature)))

