;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defun init-termauto ()
  (init-specs)
  (init-casted-states)
  (init-names)
  (init-symbols)
  (init-parser))
