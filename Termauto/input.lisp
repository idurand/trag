;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defun read-transitions (stream)
  (handler-case
      (parse-transitions stream)
    (parser-error (c)
      (prog1 nil
	(format *error-output* "read-transitions parse error ~A: ~A~%"
		(parser-token c) (parser-message c))))
    (error ()
      (prog1 nil
	(format *error-output* "Unable to read transitions")))))

(defun read-automaton (stream)
  (handler-case 
      (parse-automaton stream)
    (parser-error (c)
      (prog1 nil
	(format *error-output* "read-automaton parse error ~A: ~A~%"
		(parser-token c) (parser-message c))))
    (error ()
      (prog1 nil
	(format *error-output* "error")))))
