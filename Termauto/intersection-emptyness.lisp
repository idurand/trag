;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric intersection-emptiness (automaton1 automaton2))

(defmethod intersection-emptiness
    ((aut1 abstract-automaton) (aut2 abstract-automaton))
  (let* ((signature1 (signature aut1))
	 (signature2 (signature aut2))
	 (finite1 (signature-finite-p signature1))
	 (finite2 (signature-finite-p signature2)))
    (if (and finite1 finite2)
	(automaton-emptiness (intersection-automaton-to-fly aut1 aut2))
	(error "intersection-emptiness not applicable to automata with infinite signature: ~A, ~A~%" aut1 aut2))))
