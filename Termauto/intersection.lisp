;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)
;; f(q1,q2) -> q3
;; f(p1,p2)-> p3
;; gives
;; ;; f(<q1,p1>,<q2,p2>) -> <q3,p3> ainsi que

;; it is much more complex when root is commutative
;; because we have
;; f(q2,q1) -> q3
;; f(p2,p1)-> p3
;; implicites 
;; ca donne
;; f(<q1,p1>,<q2,p2>) -> <q3,p3> ainsi que
;; f(<q1,p2>,<q2,p1>) -> <q3,p3>

(defgeneric intersection-automaton (automaton1 automaton2))
(defmethod intersection-automaton :before ((a1 abstract-automaton) (a2 abstract-automaton))
  (when *debug*
      (assert (not (signature-proved-incompatible (signature a1) (signature a2))))))

(defgeneric intersection-automaton-compatible (automaton1 automaton2)
  (:method ((aut1 abstract-automaton) (aut2 abstract-automaton))
    (intersection-automaton-to-fly aut1 aut2))
  (:method ((aut1 table-automaton) (aut2 table-automaton))
    (compile-automaton
     (call-next-method))))

(defmethod intersection-automaton ((aut1 abstract-automaton) (aut2 abstract-automaton))
  :documentation "automata must be without epsilon transitions and have compatible signatures"
  (let ((s1 (signature aut1))
	(s2 (signature aut2)))
    (let* ((is (signature-intersection s1 s2))
	   (d1 (signature-difference s1 is))
	   (d2 (signature-difference s2 is))
	   (a1 (automaton-without-signature d1 aut1))
	   (a2 (automaton-without-signature d2 aut2)))
      (intersection-automaton-compatible a1 a2))))

(defmethod intersection-automata-compatible ((automata list))
  (let ((intersection (intersection-automata-to-fly automata)))
    (if (every #'table-automaton-p automata)
	(compile-automaton intersection)
	intersection)))

(defgeneric intersection-automata (automata))
(defmethod intersection-automata ((automata list))
  (assert automata)
  (when (endp (cdr automata))
    (return-from intersection-automata (car automata)))
  (let* ((is (reduce #'signature-intersection (mapcar #'signature automata)))
	 (compatible-automata
	   (mapcar (lambda (aut)
		     (automaton-without-signature
		      (signature-difference (signature aut) is)
		      aut))
		   automata)))
	 (intersection-automata-compatible compatible-automata)))

(defgeneric intersection-automaton-to-fly (automaton1 automaton2)
  (:method ((f1 abstract-automaton) (f2 abstract-automaton))
    (intersection-automata-to-fly (list f1 f2))))

(defgeneric intersection-automata-to-fly (automata)
  (:method ((automata list))
    (when *cast-inside*
      (setq automata (mapcar #'cast-automaton automata)))
    (let* ((transitionss (mapcar #'transitions-of automata))
	   (deterministic (every #'deterministic-p transitionss)))
      (make-fly-automaton-with-transitions
       (make-fly-transitions
	(apply #'merge-signature (mapcar #'signature transitionss))
	(apply #'product-transitions-fun-gen nil transitionss)
	deterministic
	(and (every #'sink-state transitionss) *success-state*))
       (and-finalpstate-fun-gen automata)
       #'identity
       (format nil "I~A" (mapcar #'automaton-name automata))
       :precondition-automaton
       (intersection-precondition-automata (mapcar #'precondition-automaton automata))))))
