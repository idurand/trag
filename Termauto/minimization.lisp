;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric quotiented-p (automaton)
  (:method ((automaton table-automaton))
    (every #'singleton-container-p
	   (equivalence-classes automaton))))

(defgeneric minimal-p (automaton)
  (:method ((automaton table-automaton))
    (and (reduced-p automaton)
	 (co-reduced-p automaton)
	 (deterministic-p automaton)
	 (quotiented-p automaton))))

(defgeneric minimize-automaton (automaton)
  (:documentation
   "returns the minimal version of AUTOMATON (non destructive)")
  (:method ((automaton table-automaton))
    (if (minimal-p automaton)
	automaton
	(nminimize-automaton 
	 (if (deterministic-p automaton)
	     (duplicate-automaton automaton)
	     (determinize-automaton automaton)))))
  (:method ((automaton fly-automaton))
;;    (warn "fly-automaton cannot be minimized")
    automaton))
