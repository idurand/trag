;; LICENSE: see trag/LICENSE.text
;; AUTHOR: Irène Durand

(in-package :termauto)

(defmethod vbits-projection :around ((a abstract-automaton) &optional (positions '()))
  (let ((b (call-next-method)))
    (rename-object
     b
     (concatenate
       'string
       (apply #'concatenate
	      'string
	      (cons "?"
		    (mapcar (lambda (p) (format nil "X~A," p)) positions)))
       (name a)))))

(defmethod constants-color-projection :around ((a abstract-automaton))
  (let ((b (call-next-method)))
    (rename-object
     b
     (format nil "?C~A.~A" (signature-nb-colors (signature a)) (name a)))))

(defmethod nothing-to-xj :around ((a abstract-automaton) (m integer) (j integer))
  (let* ((b (call-next-method))
	 (name (name a))
	 (new-name (if (= m j 1)
		       (format nil "~A[X1]" name)
		       (format nil "~A[X~A]^~A" name j m))))
    (rename-object b new-name)))

(defmethod nothing-to-colors-constants :around ((a abstract-automaton) (k integer))
  (let ((b (call-next-method)))
    (rename-object b (format nil "~A-C\~~~A" (name a) k))))

(defmethod nothing-to-colors :around ((a abstract-automaton) (k integer))
  (let ((b (call-next-method)))
    (rename-object b (format nil "~A-C\~~~A" (name a) k))))

(defmethod nothing-to-color
    :around ((a abstract-automaton) (k integer) (color integer))
  (let ((b (call-next-method)))
    (rename-object b (format nil "~A-C~A\~~~A" (name a) color k))))

(defmethod nothing-to-vbits-constants :around ((a abstract-automaton) (m integer))
  (let ((b (call-next-method)))
    (rename-object b (format nil "~A(X1,.,X~A)" (name a) m))))

(defmethod y1-y2-to-xj1-xj2
    :around ((a abstract-automaton) (m integer) (j1 integer) (j2 integer))
  (let* ((b (call-next-method))
	 (name (copy-seq (name b))))
    (replace name "  " :start1 (search "X1" name))
    (replace name (format nil "X~A" j2) :start1 (search "X2" name))
    (replace name (format nil "X~A" j1) :start1 (search "  " name))
    (rename-object b (format nil "~A^~A" name m))))

(defgeneric projection-automaton (automaton)
  (:documentation "total projection according to decoration (vbits, colors, nothing)")
  (:method ((a abstract-automaton))
    (let ((signature (signature a)))
      (funcall
       (cond ((vbits-p signature)  #'vbits-projection)
	     ((constant-color-p signature) #'constants-color-projection)
	     (t #'identity))
       a))))
