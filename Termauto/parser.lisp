;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

;; <automaton> ::= Automaton <name> [<states>] [<description>] [<final states>] [<transitions>] 
;; <tautomaton> ::= Tautomaton <name> [<automata>]  tuple d'automates
;; <automata> ::= Automata <name>*

;; <states> ::= States <state name>*
;; <state name> ::= <memorized-name>
;; <description> ::= Description <state description>*
;; <state description> ::= <memorized-name> : <string>
;; <final states> ::= Final States <state name>*
;; <transisions> ::= Transitions <transition>*
;; <transition> ::= <term state> -> <state>
;; <state> ::= <memorized-name>
;; <term state> ::= <state> | <memorized-name> [<state arglist>]
;; <state arglist> ::= ( <state> <state args> )
;; <state args> ::= <empty> | , <state> <state args>

(defvar *warn-already-defined-state* t)

(defun name-state (stream)
  (let* ((name (scan-name stream))
	 (state (make-named-state name)))
    (when (eq *current-token* +sep-colon+)
      (parse-colon stream)
      (scan-int stream))
    (unless state
      (signal-parser-error 'flat-term-expected-error
			   (format nil "undefined state ~A" name)))
    state))

(defun parse-name-state (stream)
  (lexmem stream)
  (parse-name-state stream))

(defun scan-pstate (stream)
  ;;  (format-output "scan-pstate ~A ~%" *current-token*)
  (prog1 
      (make-tuple-state (parse-states stream))
    (unless (eq +sep-closed-sbracket+ (lexmem stream))
      (signal-parser-error 'closed-sbracket-expected-error "closed-bracket-expected"))))

(defun scan-name-state (stream)
  (let* ((name (scan-name stream))
         (state (make-named-state name)))
    (when (eq *current-token* +sep-colon+)
      (parse-colon stream)
      (scan-int stream))
    (unless state
      (signal-parser-error 'flat-term-expected-error
			   (format nil "undefined state ~A" name)))
    state))

(defun scan-state (stream)
  ;;  (format-output "scan-state *current-token*~A~%" *current-token*)
  (cast-state
   (if (eq +sep-open-sbracket+ *current-token*)
       (scan-pstate stream)
       (scan-name-state stream))))

(defun parse-state (stream)
  (lexmem stream)
  (scan-state stream))

(defun scan-flat-term (stream)
  (if (symbol-or-string-p)
      (let ((rootname (scan-name stream))
	    (vbits nil)
	    (color nil))
	(when (parser-vbits-p)
	  (setf vbits (bits-from-chars (lexer-vbits *current-token*)))
	  (lexmem stream))
	(when (parser-color-p)
	  (setq color (lexer-color *current-token*))
	  (lexmem stream))
	(let* ((s (find-symbol-from-name rootname))
	       (root (cond
		       (vbits (make-vbits-symbol s vbits))
		       (color (make-color-symbol s color))
		       (t s))))
	  (if root ;; we have a symbol here
	      (let ((arg nil))
		(when (eq +sep-open-par+ *current-token*)
		  (setf arg (parse-states stream))
		  (parse-closed-par stream)
		  (check-arity arg root))
		(make-flat-term root arg))
	      (make-named-state rootname))))))

(defun parse-flat-term (stream)
  (lexmem stream)
  (scan-flat-term stream))

(defun same-type-of-states-p (states)
  (and
   states
   (let ((type (type-of (car states))))
     (every (lambda (state) (eq (type-of state) type))
	    (cdr states)))))

(defun check-states (states)
  (unless (same-type-of-states-p states)
    (signal-parser-error 'heterogeneous-states-error "Error: heterogenous states")))

(defun scan-states (stream)
  ;;  (format-output "scan-states *current-token* ~A ~%"*current-token*)
  (do ((states nil))
      ((not (or (eq *current-token* +sep-open-sbracket+) (symbol-or-string-p)))
       (prog1
	   (nreverse states)
	 (check-states states)))
    (push (scan-state stream) states)
    (when (eq *current-token* +sep-comma+)
      (parse-comma stream))))

(defun parse-states (stream)
  ;;  (format-output "parse-states *current-token* ~A ~%"*current-token*)
  (lexmem stream)
  (scan-states stream))

(defun parse-transition (stream)
  (let ((lh (scan-flat-term stream)))
    (parse-arrow stream)
    (let ((key (flat-term-to-key lh)))
      (add-transition-to-global-transitions
       (car key) (cdr key) (scan-state stream)))))

(defun scan-transitions (stream)
  (do ()
      ((not (symbol-or-string-p)))
    (parse-transition stream))
  (make-table-transitions-from-global-sym-table))

(defun parse-transitions (stream)
  (lexmem stream)
  (scan-transitions stream))

(defun parse-description (stream)
  (cons 
   (scan-name stream)
   (progn
     (parse-colon stream)
     (parse-string stream))))

(defun parse-descriptions (stream)
  (lexmem stream)
  (do ((descriptions '()))
      ((not (symbol-or-string-p)) (nreverse descriptions))
    (push (parse-description stream) descriptions)))

(defun scan-automaton (stream)
  (lexmem stream)
  (if (symbol-or-string-p)
      (with-new-transitions (make-signature (all-symbols))
	(let ((name (name (lexmem stream)))
	      (final nil)
	      (transitions nil)
	      (prior-transitions nil))
	  (declare (ignore prior-transitions))
	  (when (my-keyword-p "States")
	    (parse-states stream))
	  (when (my-keyword-p "Description")
	    (parse-descriptions stream))
	  (let ((*warn-already-defined-state* nil))
	    (when (my-keyword-p "Final")
	      (lexmem stream)
	      (setf final (parse-states stream)))
	    (when (my-keyword-p "Prior")
	      (lexmem stream)
	      (scan-transitions stream))
	    (setf transitions (parse-transitions stream)))
	  ;;	    (setf transitions (epsilon-closure (parse-transitions stream)))
	  ;;	  (format t "finalstates ~A" final)
	  (make-table-automaton transitions
				:name name
				:finalstates (make-ordered-state-container final))))
      (signal-parser-error 'name-expected-error "name expected")))

(defun parse-automaton (stream)
  (lexmem stream)
  (scan-automaton stream))

(defun parse-autname (stream)
  ;; add checks whether automata exists?
  (parse-name stream))

(defun scan-autname (stream)
  (scan-name stream))

(defun scan-autnames (stream)
  (do ((automata nil))
      ((not (symbol-or-string-p)) (nreverse automata))
    (push (scan-autname stream) automata)))

(defun parse-autnames (stream)
  (lexmem stream)
  (scan-autnames stream))

(defun parse-automata (stream)
  (if (my-keyword-p "Automata")
      (parse-autnames stream)
      (signal-parser-error 'my-keyword-expected-error "keyword Automata expected")))

(defun scan-tautomaton (stream)
  (lexmem stream)
  (if (symbol-or-string-p)
      (let*
	  ((name (name (lexmem stream)))
	   (autnames (parse-automata stream))
	   (automata
	     (mapcar
	      (lambda (aname)
		(let ((automaton (find aname *automata* :key #'name :test #'equal)))
		  (or automaton
		      (signal-parser-error
		       'parser-error
		       (format nil "~A Undefined automaton in Tautomaton ~A~%" aname name)))))
	      autnames)))
	(make-tautomaton name automata))
      (signal-parser-error 'name-expected-error "name expected")))

(defun parse-tautomaton (stream)
  (lexmem stream)
  (scan-tautomaton stream))

(defun parse-termauto-spec (stream)
  (init-parser)
  (let ((termsets '())
	(tautomata ())
	(terms ()))
    (parse-symbols stream)
    (do ()
	((or (my-keyword-p "Eof")
	     (not (eq (type-of *current-token*) 'kwd)))
	 (make-termauto-spec (make-signature (all-symbols))
			      :automata (nreverse *automata*)
			      :tautomata (nreverse tautomata)
			      :termsets (nreverse termsets)
			      :terms (nreverse terms)))
      (cond
	((my-keyword-p "Automaton") (push (scan-automaton stream) *automata*))
	((my-keyword-p "Tautomaton") (push (scan-tautomaton stream) tautomata))
	((my-keyword-p "Termset") (push (parse-named-termset stream) termsets))
	((my-keyword-p "Term") (push (parse-term stream) terms))
	(t (error 'parser-error :token *current-token* "unknown keyword"))))))

