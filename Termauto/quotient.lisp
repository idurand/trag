;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric representative-state (casted-state classes))
(defmethod representative-state ((state casted-state) (classes list))
  (cons state
	(container-car
	 (find-if
	  (lambda (class) (container-member state class))
	  classes))))

(defgeneric representative-states (casted-states classes))
(defmethod representative-states ((states abstract-state-container) (classes list))
  (container-mapcar
   (lambda (state)
     (representative-state state classes))
   states))

(defgeneric nquotient-automaton (table-automaton representative-states))
(defmethod nquotient-automaton ((automaton table-automaton) (representative-states list))
  (let ((a (napply-states-mapping automaton representative-states)))
    (assert (eq (deterministic-p a) (deterministic-p automaton)))
    a))
