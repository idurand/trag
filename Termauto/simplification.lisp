;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric nminimize-automaton (automaton)
  (:documentation "DESTRUCTIVE minimizes AUTOMATON"))

(defmethod nminimize-automaton ((automaton table-automaton))
  (nreduce-automaton automaton)
  (nco-reduce-automaton automaton)
  (when (minimal-p automaton)
    (return-from nminimize-automaton automaton))
  (nquotient-automaton
   automaton
   (representative-states
    (get-states automaton)
    (equivalence-classes automaton)))
  (setf (get-equivalence-classes automaton)
	(container-mapcar #'make-state-container-from-state
			  (get-states (transitions-of automaton))))
  automaton)

(defmethod nminimize-automaton ((automaton fly-automaton))
  (warn "fly automaton cannot be minimized")
  automaton)
