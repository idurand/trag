;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defclass termauto-spec (term-spec)
  (
   (automaton :initform nil :initarg :automaton :accessor automaton)
   (tautomaton :initform nil :initarg :tautomaton :accessor tautomaton)
   (automata :initform (make-automata) :initarg :automata :accessor automata)
   (tautomata :initform (make-tautomata) :initarg :tautomata :accessor tautomata)
   ))

(defgeneric remove-automaton (a spec))
(defmethod remove-automaton ((a abstract-transducer) (spec termauto-spec))
  (remhash (name a) (automata-table (automata spec))))

(defgeneric remove-tautomaton (tautomaton spec))  
(defmethod remove-tautomaton ((tautomaton tautomaton) (spec termauto-spec))  
  (remhash (name tautomaton) (tautomata-table (tautomata spec))))

(defgeneric add-automaton (a spec))
(defmethod add-automaton ((a abstract-transducer) spec)
  (setf (gethash (name a) (automata-table (automata spec))) a))

(defgeneric add-tautomaton (tautomaton termauto-spec))
(defmethod add-tautomaton ((tautomaton tautomaton) (spec termauto-spec))
  (setf (gethash (name tautomaton) (tautomata-table (tautomata spec)))
	tautomaton))

(defgeneric fill-termauto-spec
    (spec &key termsets automata tautomata terms))

(defmethod fill-termauto-spec
    ((spec termauto-spec)
     &key (termsets nil) (automata nil) (tautomata nil) (terms nil))
  (loop for termset in termsets
     do (add-termset termset spec))
  (loop for automaton in automata
     do (add-automaton automaton spec))
  (loop for tautomaton in tautomata
     do (add-tautomaton tautomaton spec))
  (loop for term in terms
     do (add-term term spec))
  spec)
  
(defun make-termauto-spec
    (signature
     &key (termsets nil) (automata nil) (tautomata nil) (terms nil))
  (let ((spec (make-instance 'termauto-spec
		 :signature signature
		 :automaton (car automata)
		 :tautomaton (car tautomata))))
    (fill-termauto-spec spec :termsets termsets :automata automata :tautomata tautomata :terms terms)))

(defgeneric write-automata (spec stream))
(defmethod write-automata ((spec termauto-spec) stream)
  (maphash (lambda (key value)
	     (declare (ignore key))
	     (show value stream)
	     (format stream "~%")
	     )
	   (automata-table (automata spec))))

(defgeneric write-tautomata (spec stream))
(defmethod write-tautomata ((spec termauto-spec) stream)
  (maphash (lambda (key value)
	     (declare (ignore key))
	     (show value stream)
	     (format stream "~%")
	     )
	   (tautomata-table (tautomata spec))))

(defmethod write-spec :after ((spec termauto-spec) stream)
  (write-automata spec stream)
  (format stream "~%"))

(defun load-termauto-spec (stream)
  (handler-case
      (parse-termauto-spec stream)
    (parser-error (c)
      (prog1 nil (format *error-output* "load-autowrite-spec parse error ~A: ~A~%" (parser-token c) (parser-message c))))
    (error (c)
      (prog1 nil (format *error-output* "error ~A in loading spec~%" c)))))

(defun read-termauto-spec-from-path (path)
  (let ((spec (load-file-from-absolute-filename path #'load-termauto-spec)))
    (when spec
      (name-spec spec (file-namestring path)))))
