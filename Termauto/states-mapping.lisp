;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defun bijective-states-mapping-p (states-mapping &key (test #'eq))
  (let ((targets (mapcar #'cdr states-mapping)))
    (= (length targets)
       (length (remove-duplicates targets :test test)))))

(defgeneric napply-states-mapping (object-with-states states-mapping)
  (:method :around ((object-with-states t) (states-mapping list))
    (if (endp states-mapping)
	object-with-states
	(call-next-method))))

(defgeneric apply-states-mapping (object-with-states states-mapping)
  (:method ((o null) (state-mapping list)) nil)
  (:method ((l list) (state-mapping list))
    (mapcar
     (lambda (object) (apply-states-mapping object state-mapping))
     l))
  (:method ((state casted-state) (states-mapping list))
    (let ((found (assoc state states-mapping)))
      (if found (cdr found) state)))
  (:method ((term term) (states-mapping list))
    (build-term (root term) (apply-states-mapping (arg term) states-mapping)))
  (:method ((container state-container) (states-mapping list))
    (make-state-container
     (apply-states-mapping (container-contents container) states-mapping)))
  (:method ((container ordered-state-container) (states-mapping list))
    (make-ordered-state-container
     (apply-states-mapping (container-contents container) states-mapping))))
