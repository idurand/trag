;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Michael Raskin, Irène Durand

(in-package :termauto)

(defclass samplable-list ()
  ((contents-vector :accessor container-contents-vector-support
                    :initform (make-array 16))
   (contents-index :accessor container-contents-index
                   :initform (make-hash-table :test 'equal))
   (size :initform 0 :accessor container-size)
   (test :initarg :test :initform 'equal :accessor container-test)))

(defmethod container-contents-vector ((container samplable-list))
  (subseq (container-contents-vector-support container)
          0 (container-size container)))

(defmethod container-empty-p ((container samplable-list))
  (= 0 (container-size container)))

(defmethod container-copy ((container samplable-list))
  (let* ((copy (make-instance 'samplable-list)))
    (setf (container-contents-vector-support copy)
          (subseq (container-contents-vector-support container) 0)
          (container-contents-index copy) (container-contents-index container)
          (container-size copy) (container-size container))))

(defmethod container-nadjoin (object (container samplable-list))
  (when (= (length (container-contents-vector-support container))
           (container-size container))
    (let* ((old-contents (container-contents-vector-support container))
           (old-size (container-size container))
           (new-size (* 2 old-size)))
      (setf (container-contents-vector-support container)
            (make-array new-size))
      (replace (container-contents-vector-support container) old-contents)))
  (unless (gethash (object-key object) (container-contents-index container))
    (setf (elt (container-contents-vector-support container)
               (container-size container))
          object
          (gethash (object-key object) (container-contents-index container))
          (container-size container))
    (incf (container-size container)))
  container)

(defmethod container-delete-at ((idx integer) (container samplable-list))
  (assert (< idx (container-size container)))
  (remhash (object-key (elt (container-contents-vector-support container) idx))
           (container-contents-index container))
  (setf (elt (container-contents-vector-support container) idx)
        (elt (container-contents-vector-support container)
             (decf (container-size container))))
  (setf
    (gethash (object-key (elt (container-contents-vector-support container) idx))
             (container-contents-index container))
    idx)
  container)

(defmethod container-delete (object (container samplable-list))
  (let ((idx (gethash (object-key object)
                      (container-contents-index container))))
    (when idx
      (container-delete-at idx container)
      (remhash (object-key object) (container-contents-index container)))
    container))

(defmethod container-nth ((container samplable-list) (i integer))
  (assert (< i (container-size container)))
  (elt (container-contents-vector-support container) i))

(defmethod container-random-element ((container samplable-list))
  (container-nth container (random (container-size container))))

(defmethod container-car ((container samplable-list))
  (container-nth container 0))

(defmethod container-pop-random ((container samplable-list))
  (let ((idx (random (container-size container))))
    (prog1
      (container-nth container idx)
      (container-delete-at idx container))))

(defclass multistate (uncasted-state)
  ((components :initarg components :accessor components)
   (finalp :initarg :finalp :accessor state-final-p)))

(defmethod object-key ((object multistate)) object)

(defclass index-transition-entry ()
  ((root :accessor root :initarg :root
         :initform (error "Specify transition root"))
   (arg :accessor arg :initarg :arg
        :initform (error "Specify transition arguments"))))

(defmethod object-key ((object index-transition-entry))
  (list (root object) (mapcar 'object-key (arg object))))

(defmethod print-object ((object index-transition-entry) stream)
  (format stream "#<transition ~s>" (object-key object)))

(defclass multi-minimization-state ()
  ((components :type samplable-list :initform (make-instance 'samplable-list)
               :accessor components)
   (automata :initarg :automata
                :type list
                :initform (error "Please specify automata")
                :accessor automata-of)
   (signature-symbols :initarg :signature-symbols
                      :type list
                      :accessor signature-symbols)
   (signature :initarg :signature :type signature
              :accessor signature)
   (state-index :type hash-table :initform (make-hash-table :test 'equal)
                :accessor multi-minimization-state-index)
   (transition-targets :type hash-table
                       :initform (make-hash-table :test 'equal)
                       :initarg :transition-index
                       :accessor multi-minimization-state-transition-targets)
   (missing-transitions :type samplable-list
                        :initform (make-instance 'samplable-list)
                        :initarg :missing-transitions
                        :accessor multi-minimization-state-missing-transitions)
   (transition-index :type hash-table
                     :initform (make-hash-table :test 'equal)
                     :initarg :transition-index
                     :accessor multi-minimization-state-transition-index)
   (missing-transition-index
     :type hash-table
     :initform (make-hash-table :test 'equal)
     :initarg :missing-transition-index
     :accessor
     multi-minimization-state-missing-transition-index)))

(defmethod print-object ((object multi-minimization-state) stream)
  (format stream 
          (concatenate 'string
                       "#<multi-minimization-state for ~a automata "
                       "(~a multistates, ~a symbols, ~a transitions with ~a missing)>")
          (length (automata-of object))
          (container-size (components object))
          (length (signature-symbols object))
          (hash-table-count (multi-minimization-state-transition-targets object))
          (container-size (multi-minimization-state-missing-transitions object))))

(defvar *compile-automata-verbose-merges* t)

(defmethod merge-multistates-in ((container multi-minimization-state)
                                 (src multistate) (dst multistate))
  (let* ((mti (multi-minimization-state-missing-transition-index container))
         (ti (multi-minimization-state-transition-index container))
         (tt (multi-minimization-state-transition-targets container))
         (dst-mt (gethash (object-key dst) mti))
         (src-mt (gethash (object-key src) mti))
         (dst-t (gethash (object-key dst) ti))
         (src-t (gethash (object-key src) ti))
         (g-mt (multi-minimization-state-missing-transitions container)))
    (when *compile-automata-verbose-merges*
      (loop for src-s in (map 'list 'container-car (components src))
            for dst-s in (map 'list 'container-car (components dst))
            for a in (automata-of container)
            unless (equal (object-key src-s) (object-key dst-s))
            do (format 
                 *trace-output*
                 "[[Equality assumed:~%[~a]~s~%=~%[~a]~s~%]]~%"
                 (final-target-p src-s a) src-s
                 (final-target-p dst-s a) dst-s)))
    (unless (or (null src-t) (container-empty-p src-t))
      (loop for idx downfrom (1- (container-size dst-mt)) to 0
            for mt := (container-nth dst-mt idx)
            ;; replace all copies of dst with src to check if this transition
            ;; is now represented
            for new-arg := (let ((res (subseq (arg mt) 0)))
                             (substitute src dst res)
                             res)
            for new-mt := (make-instance 'index-transition-entry
                                         :root (root mt)
                                         :arg new-arg)
            when (gethash (object-key new-mt) tt)
            do (progn
                 (loop for s in (arg mt) do
                       (container-delete mt (gethash (object-key s) mti)))
                 (setf (gethash (object-key mt) tt)
                       (gethash (object-key new-mt) tt))
                 (container-delete mt g-mt)
                 (container-nadjoin mt dst-t))))
    (when src-mt
      (loop for trans across (container-contents-vector src-mt)
            do (container-delete trans g-mt)
            do (loop for s in (arg trans) do
                     (container-delete trans (gethash (object-key s) mti)))))
    (when src-t
      (loop for trans across (container-contents-vector src-t)
            do (remhash (object-key trans) tt)
            do (loop for s in (arg trans) do
                     (container-delete trans (gethash (object-key s) ti)))))
    (remhash (object-key src) (multi-minimization-state-missing-transition-index container))
    (remhash (object-key src) (multi-minimization-state-transition-index container))
    (loop for c across (components src)
          for cd across (components dst) do
          (loop for idx from 0 to (1- (container-size c))
                for s := (container-nth c idx)
                do (setf (gethash (object-key s)
                                  (multi-minimization-state-index container))
                         dst)
                do (container-nadjoin s cd)))
    (container-delete src (components container))
    (eq (state-final-p src) (state-final-p dst))))

(defmethod intersecting-multistates ((object multistate)
                                     (container multi-minimization-state))
  (let* ((set (make-empty-container-generic 'container 'equal)))
    (loop for c across (components object) do
          (loop for s across (container-contents-vector c)
                for mms := (gethash (object-key s)
                                    (multi-minimization-state-index container))
                when mms do (container-nadjoin mms set)))
    (container-unordered-contents set)))

(defun increase-number-list (min max l)
  (cond ((null l) (values nil t))
        ((< (first l) max) (cons (1+ (first l)) (rest l)))
        (t (multiple-value-bind (result end)
             (increase-number-list min max (rest l))
             (values (unless end (cons min result)) end)))))

(defun map-number-lists (f min max n)
  (loop for list := (make-list n :initial-element min)
        then (increase-number-list min max list)
        while list
        collect (funcall f list)))

(defun all-combinations (n seq)
  (map-number-lists (lambda (l)
                      (mapcar (lambda (x) (elt seq x)) l))
                    0 (1- (length seq)) n))

(defmethod container-nadjoin-bare ((object multistate)
                                   (container multi-minimization-state))
  (container-nadjoin object (components container))
  (loop for c across (components object) do
        (loop for s across (container-contents-vector c) do
              (setf (gethash (object-key s)
                             (multi-minimization-state-index container))
                    object))))
   
(defmethod container-nadjoin-fresh ((object multistate)
                                    (container multi-minimization-state))
  (container-nadjoin-bare object container)
  (setf (gethash (object-key object)
                 (multi-minimization-state-missing-transition-index
                   container))
        (make-instance 'samplable-list))
  (setf (gethash (object-key object)
                 (multi-minimization-state-transition-index
                   container))
        (make-instance 'samplable-list))
  (loop for op in (signature-symbols container)
        for arg-options :=
        (or (all-combinations 
              (1- (arity op))
              (container-contents-vector (components container)))
            '(()))
        do (loop
             for args in arg-options
             do
             (loop
               for continue := t then tail
               for head := nil then (cons (first tail) head)
               for tail := args then (rest tail)
               for idx upfrom 0
               for current-arg := (append (reverse head)
                                          (list object)
                                          tail)
               for transition :=
               (make-instance 'index-transition-entry
                              :root op
                              :arg current-arg)
               while continue
               do (container-nadjoin
                    transition
                    (multi-minimization-state-missing-transitions
                      container))
               do (loop for a in (cons object args)
                        do
                        (container-nadjoin
                          transition
                          (gethash
                            (object-key a)
                            (multi-minimization-state-missing-transition-index
                              container))))))))

(defmethod container-nadjoin ((object multistate)
                              (container multi-minimization-state))
  (let* ((is (intersecting-multistates object container))
         (target (first is))
         (extra-merges (rest is))
         (inconsistentp
           (loop for s in is 
                 unless (eq (state-final-p object)
                            (state-final-p s))
                 return t)))
    (cond (is (container-nadjoin-bare object container)
              (loop for merger in (cons object extra-merges)
                    for legal := (merge-multistates-in
                                   container merger target)
                    do (format t "~s~%" legal)
                    unless legal do (setf inconsistentp t))
              (values container (not inconsistentp)))
          (t (container-nadjoin-fresh object container)
             (values container t)))))

(defun finality-consistent-p (ms automata)
  (let* ((states
           (loop for a in automata
                 for c across (components ms)
                 append (loop for s across (container-contents-vector c)
                              collect (list s a))))
         (finality (loop for e in states collect (apply 'final-target-p e))))
    (setf (state-final-p ms) (first finality))
    (loop with ffp := (first finality)
               for fp in (rest finality)
               for s in states
               unless (eq fp ffp)
               return (values nil (first s) (second s))
               finally (return (values t)))))

(defmethod sample-states ((object multistate))
  (map 'vector 'container-random-element (components object)))

(defmethod container-nadjoin ((object index-transition-entry)
                              (container multi-minimization-state))
  (let* ((argstates (map 'list 'sample-states (arg object)))
         (root (root object))
         (result-states (loop for a in (automata-of container)
                              for trans := (transitions-of a)
                              for idx upfrom 0
                              for arg := (mapcar
                                           (lambda (ss) (elt ss idx))
                                           argstates)
                              for result := (and
                                              (every 'identity arg)
                                              (apply-transitions-fun-gft
                                                root arg (transitions-fun trans)))
                              collect result))
         (ms (make-instance 'multistate)))
    (setf (components ms) (coerce (loop for s in result-states
                                        for c := (make-instance 'samplable-list)
                                        do (container-nadjoin s c)
                                        collect c)
                                  'vector))
    (let ((consistent (multiple-value-list (finality-consistent-p 
                                             ms (automata-of container)))))
      (setf (gethash (object-key object)
                     (multi-minimization-state-transition-targets
                       container))
            ms)
      (container-delete
        object (multi-minimization-state-missing-transitions
                 container))
      (loop for a in (arg object)
            do (container-delete 
                 object
                 (gethash (object-key a)
                          (multi-minimization-state-missing-transition-index
                            container)))
            do (container-nadjoin 
                 object
                 (gethash (object-key a)
                          (multi-minimization-state-transition-index
                            container))))
      (if (first consistent)
        (container-nadjoin ms container)
        (progn (container-nadjoin ms container)
               (apply 'values container consistent))))))

(defun compile-automata (&rest automata)
  (assert (some 'signature-finite-p (mapcar 'signature automata)))
  (let* ((signature (reduce 'signature-intersection 
                            (remove-if-not 'signature-finite-p
                                            (mapcar 'signature automata))))
         (constant-signature (constant-signature signature))
         (state (make-instance 'multi-minimization-state
                               :automata automata
                               :signature signature
                               :signature-symbols
                               (signature-symbols
                                 (non-constant-signature signature)))))
    (assert (not (signature-empty-p signature)))
    (assert (not (signature-empty-p constant-signature)))
    (loop for sym in (signature-symbols constant-signature)
          for transition := (make-instance 'index-transition-entry
                                           :root sym :arg nil)
          for joining := (multiple-value-list
                           (container-nadjoin transition state))
          unless (first joining) return (apply 'values state joining))
    (loop until (container-empty-p
                  (multi-minimization-state-missing-transitions state))
          for random-missing-transition :=
          (container-random-element
            (multi-minimization-state-missing-transitions state))
          for joining := (multiple-value-list
                           (container-nadjoin random-missing-transition
                                             state))
          unless (second joining) return (apply 'values state joining)
          finally (return (values state t)))))

(defmethod multi-minimization-state-transition-fun
  ((object multi-minimization-state))
  (lambda (root arg)
  (gethash (object-key (make-instance 'index-transition-entry
                                      :root root :arg arg))
           (multi-minimization-state-transition-targets
             object))))

(defun multi-minimization-state-automaton (object)
  (make-fly-automaton
    (signature object)
    (multi-minimization-state-transition-fun object)
    :name "Multi-merge"
    :sink-state (sink-state (transitions-of (first (automata-of object))))))

