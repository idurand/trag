;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defvar *global-sym-table*)

(defclass sym-table ()
  ((dag-table :reader dag-table :initarg :dag-table)))

(defgeneric number-of-transitions (transitions)
  (:documentation "number of stored transitions f(..) -> {q1,q2} counts for 2"))

(defmethod number-of-transitions ((sym-table sym-table))
  (dag-table-number-of-transitions (dag-table sym-table)))

(defmethod print-object ((sym-table sym-table) stream)
  (format stream "~A rules" (number-of-transitions sym-table)))

(defgeneric make-sym-table (dag-table)
  (:documentation "empty table-transitions"))

(defmethod make-sym-table ((dag-table hash-table))
  (make-instance 'sym-table :dag-table dag-table))

(defgeneric make-empty-sym-table ()
  (:documentation "empty table-transitions"))

(defmethod make-empty-sym-table ()
  (make-sym-table (make-empty-dag-table)))

(defgeneric add-transition-to-sym-table (root states rh sym-table)
  (:documentation "adds the transition ROOT STATES -> RH to TRANSITIONS returns T if something new"))

(defgeneric add-dtransition-to-sym-table (root states rh sym-table)
  (:documentation "adds the dtransition ROOT STATES -> RH to TRANSITIONS returns T if something new"))

(defgeneric sym-table-apply (root states sym-table))
(defmethod sym-table-apply ((root abstract-symbol) (states list) (sym-table sym-table))
  (target-copy (dag-table-access (key-modc root states) (dag-table sym-table))))

(defgeneric transition-function-set (root states rh sym-table))

(defmethod transition-function-set ((root abstract-symbol) (states list) target (sym-table sym-table))
  (unless (casted-target-p target)
    (setq target (cast-target target)))
  (unless (and (symbol-commutative-p root)
	       (sym-table-apply root states sym-table))
    (dag-table-set (key-modc root states) target (dag-table sym-table))))

(defmethod add-transition-to-sym-table
    (root (states list) (target (eql nil)) (sym-table sym-table)))

;; (defmethod add-transition-to-sym-table
;;     (root (states list) (uncasted-state uncasted-state) (sym-table sym-table))
;;   (warn "uncasted state in add-transition-to-sym-table")
;;   (add-transition-to-sym-table root states (cast-state uncasted-state) sym-table))

(defmethod add-transition-to-sym-table (root (states list) target (sym-table sym-table))
  ;; target must be a casted-state or an ordered-state-container
  ;;  (format *error-output* "add-transition-to-sym-table target state-container ~A -> ~A ~%" key target)
  (assert (casted-target-p target))
  (let* ((oldtarget (sym-table-apply root states sym-table))
	 (newtarget (target-union oldtarget target)))
    (transition-function-set root states newtarget sym-table)
    (> (target-size newtarget) (target-size oldtarget))))

;; adds f(q1, ..., qn) -> q rule replacing previous such rules
(defmethod add-dtransition-to-sym-table (root (states list) (rh uncasted-state) (sym-table sym-table))
  ;;  (format *error-output* "add-dtransition-to-sym-table to uncasted-state")
  (add-dtransition-to-sym-table root states (cast-state rh) sym-table))

(defmethod add-dtransition-to-sym-table (root (states list) (rh ordered-state-container) (sym-table sym-table))
  ;;  (format *error-output* "add-dtransition-to-sym-table to uncasted-state")
  ;;    (format *error-output* "add-dtransition-to-sym-table key:~A  rh:~A~%" key rh)
  (add-dtransition-to-sym-table root states (cast-state ( make-gstate rh)) sym-table))

(defmethod add-dtransition-to-sym-table (root (states list) (rh (eql nil)) (sym-table sym-table)))

(defmethod add-dtransition-to-sym-table (root (states list) (rh casted-state) (sym-table sym-table))
  (let ((oldstate (sym-table-apply root states sym-table)))
    ;;    (format *error-output* "add-dtransition-to-sym-table key:~A oldstate:~A rh:~A~%" key oldstate rh)
    (unless (eq oldstate rh)
      (transition-function-set root states rh sym-table)
      (return-from add-dtransition-to-sym-table t))))

(defgeneric sym-table-cright-handsides (sym-table))
(defmethod sym-table-cright-handsides ((sym-table sym-table))
  (dag-table-cright-handsides (dag-table sym-table)))

(defgeneric sym-table-new-marked (sym-table marked))
(defmethod sym-table-new-marked ((sym-table sym-table) (marked ordered-state-container))
  (dag-table-new-marked (dag-table sym-table) marked))

(defgeneric sym-table-co-new-marked (sym-table marked)
  (:method ((sym-table sym-table) (marked ordered-state-container))
    (dag-table-co-new-marked (dag-table sym-table) marked)))

(defgeneric sym-table-to-commutative (sym-table))
(defmethod sym-table-to-commutative ((sym-table sym-table))
  (let ((dag-table (dag-table sym-table))
	(new-dag-table (make-empty-dag-table)))
    (maphash
     (lambda (sym value)
       (setf (gethash sym new-dag-table)
	     (if (symbol-commutative-p sym)
		 (dag-table-to-commutative value)
		 (dag-table-copy value))))
     dag-table)
    (make-sym-table new-dag-table)))

(defgeneric sym-table-apply-states-mapping (sym-table states-mapping)
  (:method ((sym-table sym-table) (states-mapping list))
    (let ((new-dag-table (make-empty-dag-table)))
      (maphash
       (lambda (sym value)
	 (setf (gethash sym new-dag-table)
	       (dag-table-apply-states-mapping value states-mapping)))
       (dag-table sym-table))
      (make-sym-table new-dag-table))))

(defgeneric sym-table-apply-signature-mapping (sym-table))
(defmethod sym-table-apply-signature-mapping ((sym-table sym-table))
  (let ((dag-table (dag-table sym-table))
	(new-dag-table (make-empty-dag-table)))
    (maphash
     (lambda (sym value)
       (let ((newsyms (apply-signature-mapping sym)))
	 (mapc
	  (lambda (newsym)
	    (setf (gethash newsym new-dag-table)
		  (dag-table-union value (gethash newsym new-dag-table))))
	  newsyms)))
     dag-table)
    (make-sym-table new-dag-table)))

(defgeneric sym-table-epsilon-closure (sym-table))
(defmethod sym-table-epsilon-closure ((sym-table sym-table))
  (make-sym-table
   (dag-table-epsilon-closure (dag-table sym-table))))

(defgeneric sym-table-map (fun sym-table))
(defmethod sym-table-map ((fun function) (sym-table sym-table))
  (dag-table-map fun (dag-table sym-table)))

(defgeneric sym-table-show (sym-table stream))
(defmethod sym-table-show ((sym-table sym-table) stream)
  (dag-table-show (dag-table sym-table) stream))

(defmethod show ((sym-table sym-table) &optional (stream t))
  (sym-table-show sym-table stream))

(defgeneric sym-table-deterministic-p (sym-table))
(defmethod sym-table-deterministic-p ((sym-table sym-table))
  (dag-table-deterministic-p (dag-table sym-table)))

(defgeneric sym-table-filter-with-states (states sym-table))
(defmethod sym-table-filter-with-states ((states ordered-state-container) (sym-table sym-table))
  (make-sym-table
   (dag-table-filter-with-states states (dag-table sym-table))))

(defgeneric sym-table-uncommutative (sym-table))
(defmethod sym-table-uncommutative ((sym-table sym-table))
  (let ((new-dag-table (make-empty-dag-table))
	(dag-table (dag-table sym-table)))
    (maphash
     (lambda (sym value)
       (setf (gethash sym new-dag-table)
	     (if (symbol-commutative-p sym)
		 (dag-table-uncommutative value)
		 (dag-table-copy value))))
     dag-table)
    (make-sym-table new-dag-table)))

(defgeneric sym-table-epsilon-p (sym-table))
(defmethod sym-table-epsilon-p ((sym-table sym-table))
  (maphash
   (lambda (key value)
     (when (casted-state-p key)
       (return-from sym-table-epsilon-p (make-transition key (car (dag-table-list value))))))
   (dag-table sym-table)))

(defgeneric sym-table-get-states-from (sym-table))
(defmethod sym-table-get-states-from ((sym-table sym-table))
  (dag-table-get-states-from (dag-table sym-table)))

;; specialized version when transitions are stored
(defmethod sym-table-apply ((root ac-mixin) (states list) (sym-table sym-table))
  (if (endp (cddr states))
      (call-next-method)
      (let ((target (sym-table-apply root (list (pop states) (pop states)) sym-table)))
	(unless target
	  (return-from sym-table-apply))
	(let ((ss (if (casted-state-p target)
		      (list target)
		      (container-contents target))))
	  (loop
	    with result = nil
	    for s in ss
	    do (setq result (target-union result
					  (sym-table-apply root (cons s states) sym-table)))
	    finally (return result))))))

(defgeneric complete-sym-table-p (signature sym-table)
  (:documentation
   "true if SYM-TABLE is complete according to the 
    SIGNATURE and all possible combination of states in SYM-TABLE")
  (:method ((signature signature) (sym-table sym-table))
    (let ((states (sym-table-get-states-from sym-table)))
      (when (container-empty-p states) (return-from complete-sym-table-p t))
      (let ((afa (arrange-for-arities (container-contents states) (max-arity signature))))
	(loop for symbol in (signature-symbols signature)
	      do (loop
		   for arg in (aref afa (arity symbol))
		   unless (sym-table-apply symbol arg sym-table)
		     do (return-from complete-sym-table-p (values nil (list symbol arg))))
	      finally (return t))))))
