;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)
(defvar *limit-states-value* 10)

(defclass table-automaton (abstract-automaton)
  ((finalstates :initarg :finalstates :initform (make-empty-ordered-state-container) :accessor get-finalstates)
   (equivalence-classes :accessor get-equivalence-classes)
   (reduced :accessor get-reduced :initarg :reduced)
   (co-reduced :accessor get-co-reduced :initarg :co-reduced)
   (epsilon :accessor get-epsilon)))

(defmethod print-object :after ((a table-automaton) stream)
  (format stream " ~A" (transitions-of a)))

(defgeneric get-non-finalstates (automaton)
  (:method ((automaton table-automaton))
    (container-difference (get-states automaton) (get-finalstates automaton))))

(defmethod table-automaton-p ((automaton table-automaton)) t)

(defmethod get-states ((automaton table-automaton))
  (get-states (transitions-of automaton)))

(defgeneric make-table-automaton (transitions &key name finalstates reduced co-reduced epsilon precondition-automaton)
  (:method ((transitions table-transitions)
	    &key
	      name
	      finalstates
	      (reduced '?? supplied-p-reduced)
	      (co-reduced '?? supplied-p-co-reduced)
	      (epsilon '?? supplied-p-epsilon)
	      precondition-automaton)
    (when finalstates (assert (ordered-state-container-p finalstates)))
    (unless finalstates
      (setq finalstates
	    (if (sink-state transitions)
		(make-ordered-state-container
		 (list
		  (with-states-table transitions (cast-state *success-state*))))
		(make-empty-ordered-state-container))))
    (let ((a (make-instance 'table-automaton
			    :name name
			    :finalstates finalstates
			    :transitions transitions
			    :precondition-automaton precondition-automaton)))
      (when supplied-p-reduced (setf (get-reduced a) reduced))
      (when supplied-p-co-reduced (setf (get-co-reduced a) co-reduced))
      (when supplied-p-epsilon
	(setf (get-epsilon a) epsilon)
	(when epsilon
	  (assert (not (deterministic-p a)))))
      (unless (reduced-p a) (nreduce-automaton a))
      ;; don't co-reduce because it may be an auxiliary automaton
      ;; whose finalstates are not known yet
      ;;    (unless (co-reduced-automaton-p a) 
      ;;      (nco-reduce-automaton a))
      a)))

(defmethod final-state-p ((state casted-state) (a table-automaton))
  (and
   (container-member state (get-finalstates a))
   t))

(defmethod final-state-fun ((automaton table-automaton))
  (lambda (casted-state)
    (final-state-p casted-state automaton)))

(defgeneric uncasted-final-state-fun (table-automaton))
(defmethod uncasted-final-state-fun ((automaton table-automaton))
  (lambda (state)
    (let ((*automaton-states-table* (states-table (transitions-of automaton))))
      (final-state-p (cast-state state) automaton))))

(defgeneric equivalence-classes-boundp (table-automaton))
(defmethod equivalence-classes-boundp ((automaton table-automaton))
  (slot-boundp automaton 'equivalence-classes))

(defgeneric unset-equivalence-classes (table-automaton))
(defmethod unset-equivalence-classes ((automaton table-automaton))
  (slot-makunbound automaton 'equivalence-classes))

(defgeneric set-epsilon-unknown (table-automaton))
(defmethod set-epsilon-unknown ((automaton table-automaton))
  (slot-makunbound automaton 'epsilon))

(defgeneric set-reduced-unknown (table-automaton))
(defmethod set-reduced-unknown ((automaton table-automaton))
  (slot-makunbound automaton 'reduced))

(defgeneric show-states (state-container stream)
  (:method ((states abstract-state-container) stream)
    (setf states (container-contents states))
    (let ((n (length states)))
      (if (and (not *loadable*) (> n *limit-states-value*))
	  (progn
	    (display-sequence (nthcdr *limit-states-value* states) stream)
	    (format stream "... ~A more" (- n *limit-states-value*)))
	  (display-sequence states stream)))))

(defmethod show :before ((a table-automaton) &optional (stream t))
  (format stream "Automaton ~A" (name a)))

(defmethod show :after ((a table-automaton) &optional (stream t))
  (when *loadable*
    (setf a (duplicate-automaton a))
    (number-states a))
  (format stream "~%States ")
  (show-states (get-states a) stream)
  (format stream "~%")
  (format stream "Final States ")
  (show-states (get-finalstates a) stream)
  (format stream "~%")
  (format stream "Transitions~%")
  (show (transitions-of a) stream))

(defgeneric equivalence-classes (table-automaton))
(defmethod equivalence-classes ((automaton table-automaton))
  (unless (slot-boundp automaton 'equivalence-classes)
    (setf (get-equivalence-classes automaton)
	  (equivalence-classes-automaton automaton)))
  (get-equivalence-classes automaton))

(defmethod reduced-p ((automaton table-automaton))
  (unless (slot-boundp automaton 'reduced)
    (setf (get-reduced automaton)
  	  (reduced-automaton-p automaton)))
  (get-reduced automaton))

(defmethod co-reduced-p ((automaton table-automaton))
  (unless (slot-boundp automaton 'co-reduced)
    (setf (get-co-reduced automaton)
  	  (co-reduced-automaton-p automaton)))
  (get-co-reduced automaton))

;; destructive
(defmethod number-states ((automaton table-automaton) &optional (start 0))
  (number-states (transitions-of automaton) start)
  automaton)

(defmethod name-states ((automaton table-automaton))
  (name-states (transitions-of automaton))
  automaton)

(defmethod states-numbered-p ((automaton table-automaton))
  (states-numbered-p (transitions-of automaton)))

(defmethod show-states-characteristics  ((transitions table-transitions) &key (stream t))
  (format stream
	  " ~D states" (states-count transitions)))

(defmethod automaton-emptiness ((automaton table-automaton))
  (let ((aut (reduce-automaton automaton)))
    (or (container-empty-p (get-finalstates aut))
	(values nil (non-emptiness-witness automaton)))))

(defparameter *save-automata* nil)

(defgeneric save-the-automaton (abstract-transducer))
(defmethod save-the-automaton ((a abstract-transducer))
  (save-the-automaton (compile-automaton a)))

(defmethod save-the-automaton ((a table-automaton))
  (when *save-automata*
    (let ((path (format nil "Data/~A.txt" (name a))))
      (with-open-file
	  (out path :direction :output :if-exists :supersede)
	(format out "Ops ")
	(format out "~A~%"(signature a))
	(let ((*loadable* t))
	  (show a out)))))
  a)

(defmethod nindex-states-automaton ((automaton table-automaton) (n integer))
  (compile-automaton
   (nindex-states-automaton (uncast-automaton automaton) n)))

;; ; destructive
;; (defun nindex-states-automaton-gen (automata &optional (n -1))
;;   (loop
;;     for automaton in automata
;;     do (nindex-states-automaton automaton (incf n))
;;     finally (return n)))

;; (defmethod rename-and-save :after (name (a table-automaton))
;;   (save-the-automaton a))

(defmethod cast-automaton ((a table-automaton)) a)

(defmethod apply-signature-mapping ((automaton table-automaton))
  (compile-automaton (apply-signature-mapping (uncast-automaton automaton))))

(defmethod add-empty-symbols ((automaton table-automaton))
  (compile-automaton (add-empty-symbols (uncast-automaton automaton))))

(defmethod number-of-states ((automaton table-automaton))
  (number-of-states (transitions-of automaton)))

(defmethod number-of-transitions ((automaton table-automaton))
  (number-of-transitions (transitions-of automaton)))

(defgeneric verify-automaton (a nb-states nb-rules)
  (:method ((a table-automaton) (nb-states integer) (nb-rules integer))
    (when *debug*
      (format *trace-output* "verifying ~A ~%" (name a)))
    (verify-transitions (transitions-of a) nb-states nb-rules)))

(defmethod uncast-automaton ((automaton table-automaton))
  (let ((transitions (transitions-of automaton)))
    (make-fly-automaton-with-transitions
     (uncast-transitions transitions)
     (lambda (uncasted-state)
       (final-state-p (with-states-table transitions (cast-state uncasted-state))
		      automaton))
     (output-fun automaton)
     (name automaton)
     :precondition-automaton (precondition-automaton automaton))))

(defgeneric nmerge-sinks (transitions finalstates)
  (:method ((transitions table-transitions) (finalstates ordered-state-container))
    (let ((states (get-states transitions))
	  (puits-state (find-puits-state transitions)))
      (when puits-state
	(let ((final (container-member puits-state finalstates)))
	  (nfilter-with-states
	   (container-remove puits-state states) transitions)
	  (when final
	    (setf (sink-state transitions)
		  (with-states-table transitions (cast-state *success-state*)))
	    (container-nadjoin
	     (sink-state transitions)
	     (container-delete puits-state finalstates)))))
      transitions)))

(defmethod napply-states-mapping ((automaton table-automaton) (states-mapping list))
  (let ((new-transitions (apply-states-mapping (transitions-of automaton) states-mapping))
	(new-finalstates (apply-states-mapping (get-finalstates automaton) states-mapping)))
    (setf (transitions-of automaton) (nmerge-sinks new-transitions new-finalstates))
    (setf (get-finalstates automaton) new-finalstates)
    automaton))

;; in fact equivalent to uncast-automaton
(defgeneric decompile-automaton (table-automaton)
  (:method ((a table-automaton))
    (make-fly-automaton-with-transitions
     (decompile-transitions (transitions-of a))
     (uncasted-final-state-fun a)
     (output-fun a)
     (name a)
     :precondition-automaton (precondition-automaton a))))


