;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric co-reduced-automaton-p (table-automaton)
  (:method ((automaton table-automaton))
    (let* ((transitions (transitions-of automaton))
	   (co-accessible (get-finalstates automaton))
	   (sink (sink-state transitions)))
      (when sink
	(setq co-accessible (state-co-states sink transitions)))
      (setq co-accessible (co-mark-states transitions co-accessible))
      (let ((not-co-accessible
	      (container-difference (get-states automaton) co-accessible)))
	(values
	 (container-empty-p not-co-accessible)
	 not-co-accessible)))))

(defgeneric nco-reduce-automaton (automaton)
  (:documentation "Destructive: co-reduces AUTOMATON")
  (:method ((automaton table-automaton))
    (multiple-value-bind (res not-co-accessible)
	(co-reduced-automaton-p automaton)
      (unless res
	(let ((newstates
		(container-difference (get-states automaton)
				      not-co-accessible)))
	  (nfilter-with-states newstates (transitions-of automaton))
	  (setf (get-co-reduced automaton) t))))
    automaton))
