;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric reduced-automaton-p (table-automaton))
(defmethod reduced-automaton-p ((automaton table-automaton))
  (let ((inaccessible
	  (container-difference
	   (get-states automaton)
	   (accessible-states (transitions-of automaton)))))
    (values
     (container-empty-p inaccessible)
     inaccessible)))

(defmethod nreduce-automaton ((automaton table-automaton))
  :documentation "Destructive: reduces AUTOMATON"
  (unless (reduced-p automaton)
    (let* ((transitions (transitions-of automaton))
	   (newstates (accessible-states transitions)))
      (nfilter-with-states newstates transitions)
      (setf (get-finalstates automaton)
	    (container-intersection newstates (get-finalstates automaton))))
    (setf (get-reduced automaton) t))
  automaton)

(defmethod automaton-without-signature ((signature abstract-signature) (automaton table-automaton))
  (compile-automaton
   (nautomaton-without-signature signature (uncast-automaton automaton))))

(defmethod automaton-without-symbol ((symbol abstract-symbol) (automaton table-automaton))
  (compile-automaton
   (nautomaton-without-symbol symbol (uncast-automaton automaton))))

(defgeneric automaton-without-extra-symbol (automaton))
(defmethod automaton-without-extra-symbol ((automaton abstract-automaton))
  (automaton-without-symbol *extra-symbol* automaton))
