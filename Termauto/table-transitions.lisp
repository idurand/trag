;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric make-table-transitions-from-sym-table
    (signature sym-table states-table &optional sink-state)
  (:method ((signature abstract-signature) (sym-table sym-table) states-table &optional sink-state)
    (when sink-state
      (assert (not (casted-state-p sink-state))))
    (if (and sink-state (complete-sym-table-p signature sym-table))
	(progn
	  (remhash (object-key sink-state) states-table)
	  (setq sink-state nil))
	(setq sink-state (and sink-state
			      (states-table-cast-state sink-state states-table))))
    (make-instance 'table-transitions
		   :signature signature
		   :sym-table sym-table
		   :states-table states-table
		   :sink-state sink-state)))

(defun make-table-transitions-from-global-sym-table (&optional sink-state)
  (make-table-transitions-from-sym-table
   *global-signature* *global-sym-table* *automaton-states-table* sink-state))

(defmethod show ((table-transitions table-transitions) &optional (stream t))
  (sym-table-show (sym-table table-transitions) stream))

(defmethod show-constant-transitions ((table-transitions table-transitions) &optional (stream t))
  (dag-constant-table-show (sym-table table-transitions) stream))

(defmethod cright-handsides ((table-transitions table-transitions))
  (sym-table-cright-handsides (sym-table table-transitions)))

(defgeneric get-states-from (transitions)
  (:documentation "obtain the states of a transition or several transitions")
  (:method ((table-transitions table-transitions))
    (sym-table-get-states-from (sym-table table-transitions))))

(defmethod deterministic-p ((table-transitions table-transitions))
  (sym-table-deterministic-p (sym-table table-transitions)))

(defmethod epsilon-p ((table-transitions table-transitions))
  (sym-table-epsilon-p (sym-table table-transitions)))

(defmethod epsilon-closure ((table-transitions table-transitions))
  (let ((new-states-table (copy-automaton-states-table (states-table table-transitions))))
  (make-table-transitions-from-sym-table 
   (signature table-transitions)
   (sym-table-epsilon-closure (sym-table table-transitions))
   new-states-table (sink-state table-transitions))))

(defmethod duplicate-transitions ((transitions table-transitions))
  (warn "duplicate-transitions should not be applied on table-transitions ~A~%"
	transitions))

(defgeneric decompile-transitions (table-transitions)
  (:documentation "fly-transitions from TABLE-TRANSITIONS")
  (:method ((transitions table-transitions))
    (make-fly-transitions
     (signature transitions)
     (uncasted-transitions-fun transitions)
     (deterministic-p transitions)
     (sink-state transitions))))

(defgeneric complete-transitions-p (table-transitions)
  (:documentation
   "true if the TRANSITIONS are complete according to the 
    SIGNATURE and all possible combination ;; of states in AFA")
  (:method ((transitions table-transitions))
    (complete-sym-table-p (signature transitions) (sym-table transitions))))

(defgeneric states-table-apply-states-mapping (states-table states-mapping)
  (:method ((states-table hash-table) (states-mapping list))
    (let ((new (make-empty-automaton-states-table)))
      (maphash
       (lambda (k v)
	 (setf (gethash k new) (apply-states-mapping v states-mapping)))
       states-table)
      new)))

(defmethod apply-states-mapping ((transitions table-transitions) (states-mapping list))
  (let ((new-states-table (states-table-apply-states-mapping
			    (states-table transitions) states-mapping))
	(sink-state (sink-state transitions)))
    ;;     (print (list 'new-states-table new-states-table))
    (make-table-transitions-from-sym-table
     (signature transitions)
     (sym-table-to-commutative
      ;; we have to do that because if mapping not injective states order might change
      (sym-table-apply-states-mapping
       (sym-table-uncommutative (sym-table transitions)) states-mapping))
     new-states-table
     (and sink-state (in-state (apply-states-mapping sink-state states-mapping))))))
