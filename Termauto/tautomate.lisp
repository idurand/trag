;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric tautomata-table (tautomata))

(defclass tautomata ()
  ((tautomata-table :initform (make-hash-table :test #'equal)
		     :accessor tautomata-table)))

(defun make-tautomata ()
  (make-instance 'tautomata))

(defclass tautomaton (named-mixin signature-mixin)
  ((automata :accessor automata :initarg :automata)
   (fun :initarg :fun :reader tautomaton-fun)))

(defmethod print-object :after ((tautomaton tautomaton) stream)
  (format stream "~A" (mapcar #'name (automata tautomaton))))

(defun make-tautomaton (name automata &optional (fun #'intersection-automaton-gen))
  (assert (every (lambda (a) (typep a 'abstract-automaton)) automata))
  (let ((signature (reduce #'merge-signature (mapcar #'signature automata))))
    (if signature
	(make-instance 'tautomaton
		       :name name
		       :automata automata
		       :fun fun)
	(format *error-output* "Incompatible signatures"))))

;;; faire une classe state-term qui hérite à la fois de state et term et faire que tous
;;; les defun qui s'appliquent à un term soient maintenant des méthodes sur la classe state-term

(defmethod show :after ((tautomaton tautomaton) &optional (stream t))
  (format stream ": ")
  (display-sequence (automata tautomaton) stream))

(defgeneric add-automaton-to-tautomaton (automaton tautomaton))

(defmethod add-automaton-to-tautomaton ((automaton abstract-automaton) (tautomaton tautomaton))
  (let ((automata (automata tautomaton)))
    (unless (member automaton automata)
      (let ((signature (merge-signature (signature tautomaton) (signature automaton))))
	(if signature
	    (setf (automata tautomaton)
		  (append automata (list automaton))
		  (signature tautomaton)
		  signature)
	    (format *error-output* "Incompatible signatures"))))))

(defmethod recognized-p ((term term) (ta tautomaton) &key (enum nil) (save-run nil) (signal t))
  (every
   (lambda (automaton) (recognized-p term automaton :enum enum :save-run save-run :signal signal))
   (automata ta)))

(defgeneric tcompute-target (term tautomaton))

(defmethod tcompute-target (term (tautomaton tautomaton))
  (mapcar
   (lambda (automaton)
     (compute-target term (transitions-of automaton)))
   (automata tautomaton)))

(defgeneric projection (tautomaton i)
  (:documentation "i-th projection of tuple of automata TAUTOMATON"))

(defmethod projection ((tautomaton tautomaton) (i integer))
  (assert (< i (length (automata tautomaton))))
  (nth i (automata tautomaton)))

(defgeneric min-union-automata (automata))
(defmethod min-union-automata ((automata list))
  (op-automata-list automata :op #'union-automaton :cop #'minimize-automaton))

(defgeneric min-intersection-automata (automata))
(defmethod min-intersection-automata ((automata list))
  (op-automata-list automata :op #'intersection-automaton :cop #'minimize-automaton))
