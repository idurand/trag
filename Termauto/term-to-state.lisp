;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric show-state-term-table (state-term-table)
  (:method ((table hash-table))
    (format *trace-output* "count ~A ~%" (hash-table-count table))
    (maphash
     (lambda (state term)
       (format *trace-output* "~A : ~A~%" state term))
     table)))
