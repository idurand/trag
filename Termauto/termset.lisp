;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric aut-termset (lang)
  (:documentation
   "automaton which recognizes the termset lang"))

(defmethod aut-termset ((lang termset))
  (with-slots (signature terms aut-termset) lang
    (or aut-termset
	(setf aut-termset
	      (rename-object
	       (nreduce-automaton
		(make-matching-automaton terms signature :finalstates t :subterms nil))
	       (name lang)
	       )))))
