;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric nskim-run (term transitions-fun)
  (:documentation "destructive on the run"))

(defmethod nskim-run ((term term) (transitions-fun function))
  (let ((arg (arg term)))
    (when arg
      (let ((tuples (targets-product (mapcar #'run arg))))
	(mapc (lambda (st) (setf (run st) (make-empty-ordered-state-container)))
	      arg)
	(loop
	  with root-symbol = (symbol-of (root term))
	  with containers = (mapcar #'run arg)
	  with run = (target-to-container (run term))
	  for tuple in tuples
	  do (let ((target (apply-transitions-fun
			    root-symbol
			    tuple
			    transitions-fun)))
	       (when target
		 (unless (target-empty-p (target-intersection target run))
		   (mapc (lambda (state container)
			   (container-nadjoin state container))
			 tuple
			 containers)))))
	(mapc (lambda (st)
		(nskim-run st transitions-fun))
	      arg)))
    term))

(defgeneric nannotation-from-run (term))
(defmethod nannotation-from-run ((term term))
  (setf (annotation (root term)) (target-to-container (run term)))
  (setf (run term) nil)
  (mapc #'nannotation-from-run (arg term))
  term)

(defgeneric compute-run (term nd-automaton &key final))
(defmethod compute-run ((term term) nd-automaton &key final)
  (compute-target term
		  nd-automaton
		  :final final
		  :save-run t)
  (when (run term)
    (nskim-run term (transitions-fun (transitions-of nd-automaton)))))

(defun compute-annotated-term (term automaton projection final)
  (let ((aterm (term-to-annotated-term term)))
    (compute-run aterm (funcall projection automaton) :final final)
    (when (run aterm)
      (nannotation-from-run aterm))))

(defgeneric three-phases-compute-target (term automaton afuns projection  &key enum final))
(defmethod three-phases-compute-target
    ((term term)
     (automaton abstract-automaton)
     (afuns afuns)
     (projection function)
     &key enum final)
  (setq automaton (cast-automaton automaton))
  (let ((aterm (compute-annotated-term term automaton projection final)))
  (when aterm
    (compute-target aterm
		    (constrained-automaton
		     (funcall projection (attribute-automaton automaton afuns)))
		    :enum enum :final final))))

(defgeneric three-phases-compute-final-target (term automaton afuns projection  &key enum))
(defmethod three-phases-compute-final-target
    ((term term)
     (automaton abstract-automaton)
     (afuns afuns)
     (projection function)
     &key enum)
  (three-phases-compute-target term automaton afuns projection :enum enum :final t))

(defgeneric three-phases-compute-value (term automaton afuns projection  &key enum final))
(defmethod three-phases-compute-value ((term term)
				       (automaton abstract-automaton)
				       (afuns afuns)
				       (projection function)
				       &key enum final)
  (target-attribute
   (three-phases-compute-target term automaton afuns projection :enum enum :final final)))

(defgeneric three-phases-compute-final-value (term automaton afuns projection  &key enum))
(defmethod three-phases-compute-final-value ((term term)
					     automaton
					     (afuns afuns)
					     (projection function)
					     &key enum)
  (three-phases-compute-value term automaton afuns projection :enum enum :final t))
