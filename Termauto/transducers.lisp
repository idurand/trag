;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric count-run-transducer (abstract-automaton)
  (:method ((automaton abstract-automaton))
    (attribute-automaton automaton *count-afun*)))

(defmethod target-max-run-count ((target abstract-state-container))
  (reduce #'max (container-contents target) :key #'attribute-of))

(defgeneric compute-target-counting-runs (term automaton)
  (:method ((term term) (automaton abstract-automaton))
    (compute-final-target term (count-run-transducer automaton))))

(defgeneric run-count (term automaton)
  (:documentation "number of runs of AUTOMATON on TERM")
  (:method ((term term) (automaton abstract-automaton))
    (compute-final-value term (count-run-transducer automaton))))

(defgeneric max-run-count (term automaton)
  (:method ((term term) (automaton abstract-automaton))
    (let ((transducer (count-run-transducer automaton)))
      (target-max-run-count
       (compute-final-target term transducer)))))

(defgeneric compute-height-transducer (automaton)
  (:method ((automaton abstract-automaton))
    (attribute-automaton automaton *height-afun*)))

(defgeneric compute-target-and-height (term automaton)
  (:method ((term term) (automaton abstract-automaton))
    (compute-final-target term (compute-height-transducer automaton))))

(defgeneric compute-afun-value (term automaton afuns)
  (:documentation
   "given AUTOMATON corresponding to a property with free set variables \
    P(X1,...,Xm) or colors P(Ci) \
    compute the value afun.X1,...,Xm P(X1,...,Xm) or afuns.Ci P(Ci)")
  (:method ((term term) (automaton abstract-automaton) (afuns afuns))
    (compute-value
     term (projection-automaton (attribute-automaton automaton afuns)))))

(defgeneric compute-count (term automaton)
  (:documentation
   "given AUTOMATON corresponding to a property with free set variables \
    P(X1,...,Xm) (or colors P(Ci)), or the projection of it \
    (Exists X1,...,Xm . P(X1,...,Xm) or Exists Ci . P(Ci)) \
    compute the value of #X1,...,Xm P(X1,...,Xm) (or #Ci P(Ci))")
  (:method ((term term) (automaton abstract-automaton))
    (compute-afun-value term automaton *count-afun*)))

(defgeneric compute-spectrum (term automaton)
  (:documentation
   "given AUTOMATON corresponding to a property with free set variables \
    P(X1,...,Xm) (or colors P(Ci)), compute the value of \
    Sp.X1,...,Xm P(X1,...,Xm) (or Sp.Ci P(Ci))")
  (:method ((term term) (automaton abstract-automaton))
    (let ((nb-colors (signature-nb-colors (signature automaton))))
      (compute-afun-value term automaton (if (plusp nb-colors)
					     (color-spectrum-afun nb-colors)
					     *spectrum-afun*)))))

(defgeneric compute-multi-spectrum (term automaton)
  (:documentation
   "given AUTOMATON corresponding to a property with free set variables \
    P(X1,...,Xm) (or colors P(Ci)), compute the value of \
    MSp.X1,...,Xm P(X1,...,Xm) (or MSp.Ci P(Ci))")
  (:method ((term term) (automaton abstract-automaton))
    (let ((nb-colors (signature-nb-colors (signature automaton))))
    (compute-afun-value term automaton 
			(if (plusp nb-colors)
			    (color-multi-spectrum-afun nb-colors)
			    *multi-spectrum-afun*)))))

(defgeneric compute-sat (term automaton)
  (:documentation
   "given AUTOMATON corresponding to a property with free set variables \
    P(X1,...,Xm) (or colors P(Ci)), compute the value of \
    Sat.X1,...,Xm P(X1,...,Xm) (or Sat.Ci P(Ci))")
  (:method ((term term) (automaton abstract-automaton))
    (compute-afun-value term automaton *assignment-afun*)))

(defun value-to-terms (value term)
  (mapcar (lambda (pas) (apply-positions-assignment term pas)) value))

(defun compute-assigned-terms (term automaton)
  (value-to-terms (compute-sat term automaton) term))

