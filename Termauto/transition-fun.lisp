;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric apply-transitions-fun (root states transitions-fun)
  (:documentation "may return a state, a non empty container, NIL")
  (:method ((root abstract-symbol) (states list) (transitions-fun function))
    (funcall transitions-fun root states)))

(defgeneric apply-transitions (root states transitions)
  (:documentation
   "computes the target of ROOT(STATES) with TRANSITIONS \
    may return a state (possibly a sink), a non empty-container or NIL")
  (:method ((root abstract-symbol) (states list) (transitions abstract-transitions))
    (if (some (lambda (state) (transitions-sink-p state transitions)) states)
	(sink-state transitions)
        (let ((target (apply-transitions-fun root states (transitions-fun transitions))))
	  (if (null target) (sink-state transitions) target)))))

(defgeneric apply-transitions-tuple (root tuples transitions-fun)
  (:method ((root abstract-symbol) (tuples list) (transitions-fun function))
    (let ((target (apply-transitions-fun root (pop tuples) transitions-fun))) ;; first tuple
      (loop ;; try tuples until we find a non failure target
	    while (and (not target) tuples)
	    do (setf target (apply-transitions-fun root (pop tuples) transitions-fun))) ;; try tuples
      (unless target (return-from apply-transitions-tuple target))
      (loop while tuples
	    do (let ((new-target (apply-transitions-fun root (pop tuples) transitions-fun)))
		 (setf target (target-nunion target new-target)))
	    finally (return target)))))

(defgeneric apply-transitions-fun-gft (root targets transitions-fun)
  (:method ((root abstract-symbol) (targets list) (transitions-fun function))
    (if (every #'abstract-state-p targets)
	(apply-transitions-fun root targets transitions-fun)
	(apply-transitions-tuple
	 root (targets-product targets) transitions-fun))))

;; handles sink, calls transitions-fun, handles sink
(defgeneric apply-transitions-gft (root targets transitions)
  (:method ((root abstract-symbol) (targets list) (transitions abstract-transitions))
    (if (some (lambda (target) (transitions-sink-p target transitions)) targets)
	(sink-state transitions)
	(let ((target (apply-transitions-fun-gft root targets (transitions-fun transitions))))
	  (or target (sink-state transitions))))))
