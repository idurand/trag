;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defclass transition (rule) ())

(defgeneric make-transition (lh rh)
  (:documentation
   "create an automaton transition from a left-handside LH
(state or flat-term) and a right-handside RH (a state)"))

(defmethod make-transition ((lh abstract-state) (rh abstract-state))
  (make-instance 'transition :lh lh :rh rh))

(defmethod make-transition ((lh term) (rh casted-state))
  (assert (every #'casted-state-p (arg lh)))
  (make-instance 'transition :lh lh :rh rh))

(defgeneric arg-modc (root arg))

(defmethod arg-modc ((root abstract-symbol) arg)
  (declare (ignore root))
  arg)

(defmethod arg-modc ((root commutative-mixin) arg)
  (sort-casted-states arg))

(defgeneric key-modc (root arg))

(defmethod key-modc (root arg)
  (cons root (arg-modc root arg)))
