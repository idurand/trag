;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric new-marked (transitions marked)
  (:documentation "states accessible assuming marked states are accessible")
  (:method ((table-transitions table-transitions) (marked ordered-state-container))
    (sym-table-new-marked (sym-table table-transitions) marked)))

(defgeneric state-co-states (state table-transitions)
  (:method ((state casted-state) (transitions table-transitions))
    (let* ((ncsign (non-constant-signature (signature transitions)))
	   (states (get-states transitions))
	   (afa (arrange-for-arities (container-contents states) (max-arity ncsign)))
	   (co-states (make-state-container-from-state state)))
      (loop for symbol in (signature-symbols ncsign)
	    do (loop for arg in (aref afa (arity symbol))
		     when (eq (apply-transitions symbol arg transitions)
			      state)
		       do (loop for st in arg
				do (container-nadjoin st co-states))))
      co-states)))
 
(defgeneric co-new-marked (transitions co-marked)
  (:documentation
   "states co-accessible assuming co-marked states are co-accessible")
  (:method ((table-transitions table-transitions) (marked ordered-state-container))
    (sym-table-co-new-marked (sym-table table-transitions) marked)))

(defgeneric mark-states (transitions marked)
  (:documentation "returns a state-container of accessible states")
  (:method ((transitions table-transitions) (marked ordered-state-container))
    (let* ((nm (new-marked transitions marked))
	   (sd (container-difference nm marked)))
      (if (container-empty-p sd)
	  marked
	  (mark-states transitions (container-union marked sd))))))

(defgeneric accessible-states (transitions)
  (:documentation "returns a state-container of accessible states")
  (:method ((transitions table-transitions))
    (mark-states transitions (make-empty-ordered-state-container))))

(defgeneric co-mark-states (transitions co-marked)
  (:documentation "returns a state-container of co-accessible states")
  (:method ((transitions table-transitions) (co-marked ordered-state-container))
    (let* ((nm (co-new-marked transitions co-marked))
	   (sd (container-difference nm co-marked)))
      (if (container-empty-p sd)
	  co-marked
	  (co-mark-states transitions (container-union co-marked sd))))))

(defgeneric puits-state-p (casted-state table-transitions)
  (:method ((casted-state casted-state) (transitions table-transitions))
    (let* ((ncsign (non-constant-signature (signature transitions)))
	   (states (get-states transitions))
	   (afa (arrange-for-arities (container-contents states) (max-arity ncsign))))
      (mapc
       (lambda (symbol) 
	 (loop for arg in (aref afa (arity symbol))
	       when (and (member casted-state arg :test #'eq)
			 (not (eq casted-state
				  (apply-transitions symbol arg transitions))))
		 do (return-from puits-state-p nil)))
       (signature-symbols ncsign))
      t)))

(defgeneric find-puits-state (table-transitions)
  (:method ((transitions table-transitions))
    (container-map
     (lambda (state)
       (when (puits-state-p state transitions)
	 (return-from find-puits-state state)))
     (get-states transitions))))


