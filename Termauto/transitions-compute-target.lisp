;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(define-condition sink-found ()
  ((sink :initarg :sink :reader sink)
   (subterm :initarg :subterm :reader subterm)))

(defgeneric signal-sink (term sink)
  (:method ((term term) sink)
    (signal (make-condition 'sink-found :subterm term :sink sink))))

(defgeneric sink-p (target)
  (:method ((target t)) nil)
  (:method ((target null)) t)
  (:method ((target (eql *success-state*))) t)
  (:method ((target casted-state)) (sink-p (in-state target))))

(defgeneric transitions-sink-p (target transition)
  (:method (target (transitions abstract-transitions))
    (eq target (sink-state transitions))))

(defgeneric transitions-compute-target-and-signal-rec (term transitions signal save-run)
  (:method
      ((state abstract-state) (transitions abstract-transitions) signal save-run)
    (declare (ignore transitions signal save-run))
    (when *debug* (warn "State ~A in transitions-compute-target-and-signal-rec~%" state))
    state)
  
  (:method ((term term) (transitions abstract-transitions) signal save-run)
    (let* ((targets 
	     (mapcar
	      (lambda (arg)
		(transitions-compute-target-and-signal-rec
		 arg transitions signal save-run))
	      (arg term)))
	   (target
	     (apply-transitions-gft 
	      (root term) targets transitions)))
      (when (transitions-sink-p target transitions)
	(signal-sink term target))
      (when save-run
	(setf (run term) target))
      target)))

(defgeneric transitions-compute-target (term transitions &key signal save-run)
  (:documentation "computes the target of TERM with TRANSITIONS; \
    handles sink-found exception")
  (:method ((term term) (transitions abstract-transitions)
	    &key (signal t) (save-run nil))
    (assert (signature-subset-p (signature term) (signature transitions)))
    (when save-run (delete-run term))
    (handler-case
	(transitions-compute-target-and-signal-rec
	 term transitions signal save-run)
      (sink-found (c)
	 (prog1
	     (sink c)
	   (when *debug*
	     (format *standard-output* "sink reached at ~A ~A~%"
		     (sink c) (subterm c))))))))

(defgeneric select-one-run (term abstract-state abstract-transitions)
  (:method
      ((term term) (state abstract-state) (transitions abstract-transitions))
    (setf (run term) state)
    (let* ((args
	     (targets-product (mapcar #'run (arg term))))
	   (arg (find-if
		 (lambda (arg)
		   (target-intersection
		    state
		    (apply-transitions (root term) arg transitions)))
		 args))
	   (subterms (arg term)))
      (build-term (root term)
		  (mapcar (lambda (subterm sstate)
			    (select-one-run
			     subterm sstate transitions))
			  subterms
			  arg)))))

(defgeneric states-of-run (term)
  (:method ((term term))
    (let ((states (make-state-container '())))
      (labels ((aux (st)
		 (container-nadjoin (run st) states)
		 (mapc #'aux (arg st))))
	(aux term))
      states)))

