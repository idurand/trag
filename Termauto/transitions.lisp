;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)
(defvar *loadable* nil)

;;; explicit transitions (represented in extension)
;;; in a sym-table, the value is always a state-container of states
;;; accessible from the corresponding lh (the key).
;;; in a dtransitions-table the value is a state

(defvar *global-signature*)

(defun new-states-table ()
  (setq *automaton-states-table* (make-empty-automaton-states-table)))

(defmacro with-new-states-table (&body body)
  `(let* ((*automaton-states-table* (make-empty-automaton-states-table)))
     ,@body))

(defmacro with-new-transitions (signature &body body)
  `(let* ((*global-signature* ,signature)
	  (*global-sym-table* (make-empty-sym-table))
	  (*automaton-states-table* (make-empty-automaton-states-table)))
     ,@body))

(defun set-new-transitions (signature)
  (setf *global-signature* signature)
  (setf *global-sym-table* (make-empty-sym-table))
  (setf *automaton-states-table* (make-empty-automaton-states-table)))

(defgeneric sym-table (transitions))

(defclass table-transitions (abstract-transitions cast-states-mixin)
  ((sym-table :initarg :sym-table :accessor sym-table)))

(defgeneric find-casted-state (uncasted-state transitions)
  (:method ((state uncasted-state) (transitions table-transitions))
    (with-states-table transitions
      (cast-state state))))

(defmethod casted-transitions-p ((transitions table-transitions)) t)

(defgeneric cright-handsides (transitions))

(defgeneric nfilter-with-states (accessible-states transitions)
  (:documentation
   "destructive remove transitions whose left-handside has a not accessible state")
  (:method ((accessible-states ordered-state-container) (transitions table-transitions))
    (setf (sym-table transitions)
	  (sym-table-filter-with-states accessible-states (sym-table
							   transitions)))
    (nfilter-states-table accessible-states (states-table transitions))
    transitions))

(defgeneric states-count (transitions)
  (:method ((transitions table-transitions))
    (hash-table-count (states-table transitions))))

(defgeneric get-states (transitions)
  (:method ((transitions table-transitions))
    (let ((states (states-from-states-table (states-table transitions))))
      (when *debug*
	(let ((states2 (get-states-from transitions)))
	  (unless (= (container-size states) (container-size states2))
	    (warn "states-table and transitions incoherent")
	    (format *error-output* "~A ~A ~%" states states2))))
      states)))

(defmethod transitions-fun ((transitions table-transitions))
  (lambda (root states)
    (sym-table-apply root states (sym-table transitions))))

(defgeneric uncasted-transitions-fun (table-transitions)
  (:method ((transitions table-transitions))
    (lambda (root states)
      (let ((*automaton-states-table* (states-table transitions)))
	(uncast-state
	 (sym-table-apply root (mapcar #'cast-state states) (sym-table transitions)))))))

(defmethod number-of-transitions ((transitions table-transitions))
  (number-of-transitions (sym-table transitions)))

(defgeneric number-of-states (table-transitions)
  (:method ((transitions table-transitions))
    (let ((nb-states (container-size (get-states transitions))))
      (when (and (null (sink-state transitions))
		 (not (complete-transitions-p transitions)))
	(incf nb-states))
      nb-states)))

(defgeneric total-number-of-transitions (nb-states signature)
  (:method ((nb-states integer) (arity integer))
    (loop
      with n = 1
      repeat arity
      do (setq n (* n nb-states))
      finally (return n)))
  (:method ((nb-states integer) (symbol abstract-symbol))
    (total-number-of-transitions nb-states (arity symbol)))
  (:method ((nb-states integer) (signature signature))
    (loop
      for symbol in (signature-symbols signature)
      sum (total-number-of-transitions nb-states symbol))))

(defgeneric max-number-of-transitions (table-transitions)
  (:method ((transitions table-transitions))
    (let ((signature (signature transitions)))
      (assert (signature-finite-p signature))
      (total-number-of-transitions (number-of-states transitions) signature))))

(defmethod print-object ((transitions table-transitions) stream)
  (format stream "~a " (sym-table transitions))
  (format stream "~D states" (number-of-states transitions))
  (unless (complete-transitions-p transitions)
    (let ((sink (sink-state transitions)))
      (when sink
	(format stream " (~A)" (in-state sink))))))

(defmethod cast-transitions ((transitions table-transitions))
  transitions)

(defgeneric verify-transitions (table-transitions nb-states nb-rules)
  (:documentation
   "check that the number of states and transitions are \
    the one given as parameters")
  (:method ((transitions table-transitions) (nb-states integer) (nb-rules integer))
    (let ((nbs (number-of-states transitions))
	  (nbt (number-of-transitions transitions)))
      (if *debug*
	  (format t "verifying ~A ~A ~%" nbs nbt))
      (assert (= nb-states nbs))
      (assert (= nb-rules nbt)))))

(defmethod uncast-transitions ((transitions table-transitions))
  ;;  (format *error-output* "uncast-transtions table-transitions~%")
  (make-fly-transitions
   (signature transitions)
   (lambda (root states)
     (uncast-target 
      (apply-transitions-fun
       root
       (with-states-table transitions
	 (mapcar #'cast-state states))
       (transitions-fun transitions))))
   (deterministic-p transitions)
   (if (sink-state transitions)
       (in-state (sink-state transitions))
       nil)
   :transitions-type 'fly-transitions))

(defgeneric states-numbered-p (transitions)
  (:documentation "true if states are numbered")
  (:method ((transitions table-transitions))
    (state-number (container-car (get-states transitions)))))

(defgeneric number-states (table-transitions &optional start)
  (:documentation "destructive")
  (:method ((transitions table-transitions) &optional (start 0))
    (let ((states (container-contents (get-states transitions))))
      (let ((n (1- start)))
	(dolist (s states n)
	  (setf (state-number s) (incf n)))))
    transitions))

(defgeneric name-states (transitions)
  (:documentation "destructive toggle numbered states to named states")
  (:method ((transitions table-transitions))
    (let ((states (container-contents (get-states transitions))))
      (when states
	(let ((first (first states)))
	  (unless (in-state first)
	    (format *error-output* "Warning: States have no name"))
	  (when (and (state-number first) (in-state first))
	    (mapc (lambda (s) (setf (state-number s) nil)) states)))))
    transitions))

(defgeneric add-dtransition-to-global-transitions (root states rh)
  (:documentation
   "adds the dtransition ROOT STATES -> RH to *global-sym-table* \
      returns T if something new")
  (:method (root (states list) (rh abstract-state))
    (unless (casted-state-p rh) (setq rh (cast-state rh)))
    (add-dtransition-to-sym-table root states rh *global-sym-table*)))

(defgeneric add-transition-to-global-transitions (root states rh)
  (:documentation
   "adds the transition ROOT STATES -> RH to *global-sym-table* \
      returns T if something new")
  (:method (root (states list) rh)
    (unless (casted-target-p rh) (setq rh (cast-target rh)))
    (add-transition-to-sym-table root states rh *global-sym-table*)))

(defgeneric global-transition-function-set (root states target)
  (:method ((root abstract-symbol) (states list) target)
    (transition-function-set root states target *global-sym-table*)))
