;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termauto)

(defgeneric union-automaton (automaton1 automaton2)
  (:method :before ((a1 abstract-automaton) (a2 abstract-automaton))
    (assert (merge-signature (signature a1) (signature a2)))))

(defmethod union-automaton ((aut1 abstract-automaton) (aut2 abstract-automaton))
  (union-automaton-to-fly aut1 aut2))

(defmethod union-automaton ((aut1 table-automaton) (aut2 table-automaton))
  (compile-automaton (call-next-method)))

(defgeneric union-automata (automata)
  (:method  ((automata list))
    (assert automata)
    (assert (every #'abstract-automaton-p automata))
    (when (endp (cdr automata))
      (return-from union-automata (car automata)))
    (let ((union
	    (union-automata-to-fly automata)))
      (if (every #'table-automaton-p automata)
	  (compile-automaton union)
	  union))))

(defgeneric union-automaton-to-fly (automaton1 automaton2)
  (:method ((f1 abstract-automaton) (f2 abstract-automaton))
    (union-automata-to-fly (list f1 f2))))

(defgeneric union-automata-to-fly (automata)
  (:documentation "automata must have compatible signatures")
  (:method ((automata list))
    (assert (every #'abstract-automaton-p automata))
    (let* ((signature (apply #'merge-signature (mapcar #'signature automata)))
	   (transitionss (mapcar #'transitions-of automata))
	   (deterministic (every #'deterministic-p transitionss)))
      (assert signature)
      (make-fly-automaton-with-transitions
       (make-fly-transitions
	signature
	(apply #'product-transitions-fun-gen t transitionss)
	deterministic
	(and (some #'sink-state transitionss) *success-state*))
       (or-finalpstate-fun-gen automata)
       #'identity
       (format nil "U~A" (mapcar #'automaton-name automata))
       :precondition-automaton
       (intersection-precondition-automata (mapcar #'precondition-automaton automata))))))

