;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defclass abstract-state (abstract-zeroary-term) ())

(defmethod name ((s abstract-state))
  (strong-name (format nil "~A" s)))

(defun abstract-state-p (s)
  (typep s 'abstract-state))

(defgeneric state-size (state)
  (:method ((s (eql t))) 1)
  (:method ((state abstract-state))
    (error (format t "state-size unknown: ~A" (type-of state))))
  (:method ((l list))
    (warn "state-size of a list")
    (loop
      for s in l
      sum (state-size s))))

(defmethod symbols-from ((term abstract-state)) '())

(defmethod vars-of ((state abstract-state)) '())

(defmethod apply-signature-mapping ((state abstract-state))
  state)
