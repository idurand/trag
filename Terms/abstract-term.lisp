;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric arg (term)
  (:documentation "list of arguments of TERM"))

(defgeneric root (term)
  (:documentation "root of TERM"))

(defgeneric zerorary (term)
  (:documentation "return TRUE if term is zeroary (zeroary symbol, variable or state)"))

(defgeneric vars-of (term)
  (:documentation "set of variables ot TERM"))

(defclass abstract-term (signed-object) ())

(defclass abstract-zeroary-term (abstract-term) ())

(defun abstract-term-p (term)
  (typep term 'abstract-term))

(defgeneric term-constant-p (abstract-term))
(defmethod term-constant-p ((term abstract-term)) nil)

(defmethod root ((term abstract-zeroary-term)) term)
(defmethod arg ((term abstract-zeroary-term)) '())
(defgeneric term-zeroary-p (term))
(defmethod term-zeroary-p ((term abstract-zeroary-term)) t)

(defgeneric my-copy-term (term))

(defmethod my-copy-term ((term abstract-zeroary-term)) term)
(defmethod vars-of ((term abstract-zeroary-term)) '())
(defmethod vars-of ((objects list))
  (let ((vars '()))
    (loop for o in objects
	  do (loop for var in (vars-of o)
		   do (pushnew var vars :test #'compare-object)))
    vars))

(defgeneric apply-substitution (term substitution))
(defmethod apply-substitution ((term abstract-term) substitution)
  term)
