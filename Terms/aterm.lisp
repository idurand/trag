;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric remove-annotation (term)
  (:documentation "all annotation removed")
  (:method ((term term))
    (set-term-eti
     (build-term
      (symbol-of (root term))
      (mapcar #'remove-annotation (arg term)))
     (term-eti term))))

(defmethod remove-annotation-if ((aterm term) (pred function))
  (set-term-eti
   (build-term
    (remove-annotation-if (root aterm) pred)
    (mapcar (lambda (st) (remove-annotation-if st pred)) (arg aterm)))
   (term-eti aterm)))

(defgeneric ncompute-annotation-rec (term father fun)
  (:method ((term term) father fun)
    (assert (or (null father) (annotated-symbol-p father)))
    (unless (term-zeroary-p term)
      (setf (annotation (root term)) (funcall fun term father))
      (mapc (lambda (ast)
	      (ncompute-annotation-rec ast (root term) fun))
	    (arg term)))
    term))

(defgeneric ncompute-annotation (term fun))
(defmethod ncompute-annotation ((term term) fun)
  (ncompute-annotation-rec term nil fun))

(defgeneric term-to-annotated-term (term &key constants non-constants))
(defmethod term-to-annotated-term ((term term) &key (constants t) (non-constants t))
  (set-run
   (set-term-eti
    (let ((root (root term))
	  (arg (arg term)))
      (build-term
       (if (or (and constants (endp arg)) (and non-constants arg))
	   (annotate-symbol root nil)
	   root)
       (mapcar (lambda (st) (term-to-annotated-term st :constants constants :non-constants non-constants))
	       (arg term))))
    (term-eti term))
   (run term)))

(defgeneric compute-annotation (term fun))
(defmethod compute-annotation ((term term) fun)
  (ncompute-annotation (term-to-annotated-term term) fun))
