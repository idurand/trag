;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)
;; aux-variables are not unique 
;; to compare them we must compare their names

(defvar *aux-variables-number* -1)

(defclass aux-var (abstract-variable) ())

(defun reset-aux-variables (&optional (n -1))
  (setf *aux-variables-number* n))

(defun make-aux-var (&optional (str "Y"))
  (let ((name (if (zerop (incf *aux-variables-number*))
		  str
		  (format nil "~A~A" str *aux-variables-number*))))
    (make-instance 'aux-var :name name)))

(defun aux-var-p (v)
  (typep v 'aux-var))
