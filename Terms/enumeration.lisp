;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric constant-terms-enumerator (signature)
  (:method ((signature signature))
    (make-funcall-enumerator
     #'build-term
     (make-list-enumerator
      (signature-symbols
       (constant-signature signature))))))

(defgeneric terms-depth<-enumerator (signature depth)
  (:method ((signature signature) (depth integer))
    (case depth
      (0 (make-empty-enumerator))
      (1 (constant-terms-enumerator signature))
      (t (terms-depth<=-enumerator signature (1- depth))))))

(defgeneric terms-depth<=-enumerator (signature depth)
  (:method ((signature signature) (depth integer))
    (if (zerop depth)
	(constant-terms-enumerator signature)
	(make-sequential-enumerator
	 (list
	  (terms-depth<-enumerator signature depth)
	  (terms-depth=-enumerator signature depth))))))

(defgeneric terms-depth=-enumerator (signature depth)
  (:method ((signature signature) (depth integer))
    (if (zerop depth) ;;; should be (= 1 depth)???
	(constant-terms-enumerator signature)
	(let* ((non-constant-symbols (signature-symbols
				      (non-constant-signature signature)))
	     (e< (terms-depth<-enumerator signature (1- depth)))
	     (e= (terms-depth=-enumerator signature (1- depth)))
	     (afa (arrange-for-arities (list e< e=) (max-arity signature))))
	(loop ;; remove the ones that are all e<
	  for i from 1 to (max-arity signature)
	  do (setf (aref afa i)
		   (remove-if
		    (lambda (arg)
		      (every (lambda (x) (eq x e<)) arg))
		    (aref afa i))))
	  (make-sequential-enumerator
	   (mapcar
	    (lambda (symbol)
	      (let ((afai (aref afa (arity symbol))))
		(make-sequential-enumerator
		 (mapcar
		  (lambda (etuple)
		    (make-funcall-enumerator
		     (lambda (tuple)
		       (build-term
			symbol
			tuple))
		     (make-product-enumerator etuple)))
		  afai))))
	    non-constant-symbols))))))

(defgeneric terms-enumerator (signature &optional min-depth)
  (:method ((signature signature) &optional (min-depth 0))
  (make-flatten-enumerator
   (make-funcall-enumerator
    (lambda (i)
      (terms-depth=-enumerator signature i))
    (make-inductive-enumerator min-depth #'1+ :current-value min-depth)))))
