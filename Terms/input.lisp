;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defun read-termset (stream)
  (handler-case
      (parse-termset stream)
     (parser-error (c)
       (prog1 nil
	 (format *error-output* "read-termset parse error ~A: ~A~%" (parser-token c) (parser-message c) )))
     (error () 
       (prog1  nil
	 (format *error-output* "Unable to read a termset ~%")))))

(defun read-term (stream)
  (let ((term
	 (handler-case
	     (parse-term stream)
	   (parser-error (c)
	     (prog1 nil
	       (format *error-output* "read-term parse error ~A: ~A~%" (parser-token c) (parser-message c) )))
	  (error ()
	     (prog1 nil (format *error-output* "Unable to read a term~%"))))))
    (if term
	(if (term-ground-p term)
	    term
	    (prog1 nil (format *error-output* "unground term~%")))
	(format *error-output* "no term~%"))))

(defun input-term (string)
  (with-input-from-string (foo string) (parse-term foo)))

(defgeneric load-term (name &optional extension)
  (:method ((name string) &optional (extension "term"))
    (setq name (absolute-data-filename (extend-name name extension)))
  (let ((stream (open name :direction :input :if-does-not-exist nil)))
    (if stream
	(read-term stream)
	(warn "not loaded file ~A does not exist~%" name)))))
