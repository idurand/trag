;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric term-to-stream (term stream))

(defmethod term-to-stream ((term term) stream)
  (let ((*show-term-eti* t)
	(*show-ac* nil))
    (print term stream)))

(defgeneric save-term (term filename &optional extension))

(defmethod save-term ((term term) (name string) &optional (extension "term"))
  (setq name (absolute-data-filename (extend-name name extension)))
  (with-open-file (stream name :direction :output :if-exists nil)
    (if stream
	(term-to-stream term stream)
	(warn "not saved file ~A already exists~%" name))))
