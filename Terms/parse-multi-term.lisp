(in-package :terms)

(defclass multi-term-part ()
  ((name :type string :accessor multi-term-part-name :initarg :name)
   (text :type string :accessor multi-term-part-text :initarg :text)
   (deps :type list   :accessor multi-term-part-deps :initarg :deps)))

(defmethod print-object :after ((object multi-term-part) stream)
  (format stream "~a(~{~a~#[~:;,~]~})>"
          (multi-term-part-name object)
          (multi-term-part-deps object)))

(defun make-multi-term-part (name text deps)
  (make-instance 'multi-term-part :name name :text text :deps deps))

(defclass multi-term ()
  ((multi-term-parts :type list :accessor multi-term-parts :initarg :multi-term-parts)))

(defmethod print-object :after((multi-term multi-term) stream)
  (format stream "~A" (multi-term-parts multi-term)))

(defun make-multi-term (parts)
  (make-instance 'multi-term :multi-term-parts parts))

(defun separate-name-regexp (x) (format nil "\\b~a\\b" x))

(defun find-deps (text variables)
  (loop for v in variables
        when (cl-ppcre:scan (separate-name-regexp v) text)
        collect v))

(defun parse-multi-term (str)
  (let* ((prefixed (concatenate 'string "." str))
         (split (remove "" (cl-ppcre:split "\\s*[.]\\s*" prefixed)
                :test 'equal))
         (assignments (loop for x in split collect
                            (cl-ppcre:split "\\s*=\\s*" x)))
         (variables (mapcar 'first assignments))
         (withdeps
           (loop for x in assignments
                 for name := (first x)
                 for text := (second x)
                 for deps := (find-deps text variables)
                 collect (make-multi-term-part name text deps))))
    (make-multi-term withdeps)))

(defun apply-multi-term-definition (definition part)
  (make-multi-term-part
                  (multi-term-part-name part)
                  (cl-ppcre:regex-replace-all
                         (separate-name-regexp
                           (multi-term-part-name definition))
                         (multi-term-part-text part)
                         (multi-term-part-text definition))
                  (remove (multi-term-part-name definition)
                               (multi-term-part-deps part)
                               :test #'equal)))

(defun apply-multi-term-definitions (definitions part)
  (loop with res := part
        for definition in definitions
        do (setf res (apply-multi-term-definition definition res))
        finally (return res)))

(defgeneric resolve-multi-term (multi-term)
  (:method ((multi-term multi-term))
    (loop with task := (multi-term-parts multi-term)
	  for resolved := (remove-if 'multi-term-part-deps task)
	  for unresolved := (remove-if-not 'multi-term-part-deps task)
	  unless unresolved return (multi-term-part-text (first resolved))
	    unless resolved do (error "Everything has deps: ~s" task)
	      do (setf task (loop for u in unresolved
				  collect (apply-multi-term-definitions
					   resolved u))))))

(defgeneric collect-unary-prefix-string (term)
  (:method ((term term))
    (assert (plusp (arity (root term))))
    (loop
      with prefix = ""
      while (= 1 (arity (root term)))
      do (setq prefix (format nil "~A~A(" prefix (root term)))
      do (setq term (car (arg term)))
      finally (return (values prefix (arg term))))))

;; (defgeneric term-to-multi-term (term)
;;    (:documentation "builds a multi-term-part from the TERM")
;;   (:method ((term term))
;;     (loop
;;       with n = -1
;;       with todo = (list (list term 0))
;;       with parts = '()
;;       while todo
;;       do (destructuring-bind (term n) (pop todo)
;; 	   (let ((arity (arity (root term))))
;; 	     (case arity
;; 	       (0 (make-multi-term-part (format nil "t~A" n) (format nil "~A" term) '()))
;; 	       (1 (multiple-value-bind (prefix subterms)
;; 		      (collect-unary-prefix-string term)
;; 		    (make-multi-term-part
;; 		     (format nil "t~A" n)
;; 		     (loop
;; 		       with arg-string = (format nil "t~A" (incf n))
;; 		       do (push (list (pop (arg term)) n) todo)
;; 		       for arg in (butlast (arg term))
		       
;; 		       do (setq arg-string = (format nil "~A,~A" arg-string (incf n))
;; 				do (push (list arg n) todo))))))
;; 	       (t
;; 		(make-multi-term-part

(defun parse-and-resolve-multi-term-string (string)
  (resolve-multi-term (parse-multi-term string)))

(defun input-multi-term (string)
  (input-term
   (resolve-multi-term (parse-multi-term string))))

#+nil
(parse-and-resolve-multi-term-string
  "t0 = add_a_b(oplus(a,b)). t1 = oplus(t0,ren_b_c(t0)).")

#+nil
(input-multi-term "t0 = add_a_b(oplus(a,b)). t1 = oplus(t0,ren_b_c(t0)).")
