;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)
;; <name> ::= <symbol> | <string>
;; <memorized-name> := <name>
;; <term> ::= <memorized-name> [<arglist>]
;; <flat-term> ::= <memorized-name> [<state arglist>]
;; <arglist> ::= ( <arg> <args> )
;; <args> ::= <empty> | , <arg> <args>
;; <ops> ::= Ops <declaration>*
;; <declaration> ::= <memorized-name> [: <integer>]

;; classes : kwd sym separator string
;; constants : +sep-open-par+ +sep-closed-par+ +sep-colon+ +sep-arrow+ +sep-comma+ +sep-open-bracket+ +sep-closed-bracket+

(defgeneric parser-token (token))
(defgeneric parser-message (token))

(define-condition parser-error (error)
  ((token :reader parser-token :initform "no token")
   (message :reader parser-message :initform "no message")))

(define-condition my-keyword-expected-error (parser-error) ())
(define-condition symbol-or-string-expected-error (parser-error) ())
(define-condition integer-expected-error (parser-error) ())
(define-condition colon-expected-error (parser-error) ())
(define-condition comma-expected-error (parser-error) ())
(define-condition arrow-expected-error (parser-error) ())
(define-condition closed-par-expected-error (parser-error) ())
(define-condition closed-bracket-expected-error (parser-error) ())
(define-condition open-sbracket-expected-error (parser-error) ())
(define-condition closed-sbracket-expected-error (parser-error) ())
(define-condition string-expected-error (parser-error) ())
(define-condition name-expected-error (parser-error) ())
(define-condition flat-term-expected-error (parser-error) ())
(define-condition arity-error (parser-error) ())
(define-condition heterogeneous-states-error (parser-error) ())

(defvar *automata* '() "defined automata")

(defvar *current-token* nil)
(defvar *previous-token* nil)

(defun init-parser ()
  (init-symbols)
  (init-variables)
  (setf *current-token* nil)
  (setf *previous-token* nil)
  (setf *automata* nil)
)

(defmacro lexmem (stream)
  `(progn
    (setf *previous-token* *current-token*)
    (setf *current-token* (lex ,stream))
    *previous-token*))

(defun my-keyword-p (name)
  (and (eq (type-of *current-token*) 'kwd)
       (string= (name *current-token*) name)))

(defun integer-p (token)
  (integerp (name token)))

(defun sym-p ()
  (eq (type-of *current-token*) 'sym))

(defun symbol-or-string-p ()
  (or (sym-p)
      (stringp *current-token*)))

(defun parser-vbits-p ()
  (eq (type-of *current-token*) 'vbits))

(defun parser-color-p ()
  (eq (type-of *current-token*) 'color))

(defun separator-p ()
  (eq (type-of *current-token*) 'sep))

(defun keyword-p ()
  (eq (type-of *current-token*) 'kwd))

(defun keyword-or-separator-p ()
  (or (keyword-p) (separator-p)))

(defun token-or-name (token)
  (if (eq (type-of token) 'sym)
      (name token)
      token))

(defun signal-parser-error (error-type message)
  (let ((name (token-or-name *current-token*))
	(pname (token-or-name *previous-token*)))
    (format-output "~A, ~A: ~A~%" pname name message)
    (signal error-type :token name :message message)))

(defun scan-int (stream)
  (if (integer-p *current-token*)
      (name (lexmem stream))
      (signal-parser-error 'integer-expected-error "integer expected")))

(defun parse-int (stream)
  (lexmem stream)
  (scan-int stream))

(defun parse-colon (stream)
  (or (eq +sep-colon+ (lexmem stream))
      (signal-parser-error 'colon-expected-error "colon expected")))

(defun parse-comma (stream)
  (or (eq +sep-comma+ (lexmem stream))
      (signal-parser-error 'comma-expected-error "comma expected")))

(defun parse-string (stream)
  (if (stringp *current-token*)
      (lexmem stream)
      (signal-parser-error 'string-expected-error "string expected")))

(defun parse-arrow (stream)
  (or (eq +sep-arrow+ (lexmem stream))
      (signal-parser-error 'arrow-expected-error "arrow expected")))

(defun parse-closed-par (stream)
  (or (eq +sep-closed-par+ (lexmem stream))
      (if (null *current-token*)
	  (format-output "Warning: missing closing parenthesis~%")
	  (signal-parser-error 'closed-par-expected-error "closed-par expected"))))

(defun parse-closed-bracket (stream)
  (or (eq +sep-closed-bracket+ (lexmem stream))
      (if (null *current-token*)
	  (format-output "Warning: missing closing bracket~%")
	  (signal-parser-error 'closed-bracket-expected-error "closed-bracket expected"))))

(defun scan-name (stream)
  (if (symbol-or-string-p)
      (if (eq (type-of *current-token*) 'sym)
	  (let ((name (format nil "~A" (name (lexmem stream)))))
	    (when (eq *current-token* +sep-closed-sbracket+)
	      (setq name (concatenate 'string name ">" (parse-name stream))))
	    name)
	  (lexmem stream))
      (signal-parser-error 'symbol-or-string-expected-error "symbol or string")))

;;; faire un parse string-name qui ne s'interesse pas a memoriser le name
;;; mais lis soit un symbole soit une chaine

(defun parse-name (stream)
  (lexmem stream)
  (scan-name stream))

(defun parse-memorized-name (stream)
  (lexmem stream)
  (scan-name stream))

(defun bits-from-chars (chars)
  (mapcar
   (lambda (bit) (parse-integer (make-string 1 :initial-element bit)))
   chars))

(defun scan-declaration (stream)
  (let ((name (scan-name stream))
	(arity 0)
	(vbits nil))
    (when (parser-vbits-p)
      (setf vbits (lexer-vbits *current-token*))
      (lexmem stream))
    (when (eq +sep-colon+ *current-token*)
      (setf arity (parse-int stream))
      (when (and (or (minusp arity) (plusp arity)) vbits)
	(signal-parser-error 'parser-error "symbol with vbits with arity > 1 or -1")))
    (if (zerop arity)
	(let ((s (make-constant-symbol name)))
	  (if vbits (make-vbits-symbol s (bits-from-chars vbits)) s))
	  (if (minusp arity)
	      (make-ac-symbol name)
	      (let ((s (make-parity-symbol name arity)))
		(if vbits (make-vbits-symbol s vbits) s))))))

(defun parse-declaration (stream)
  (lexmem stream)
  (scan-declaration stream))

(defun parse-symbols (stream)
  (lexmem stream)
  (unless (my-keyword-p "Ops")
    (signal-parser-error 'my-keyword-expected-error  "keyword Ops expected"))
  (cons (parse-declaration stream)
	(loop
	   until (keyword-p)
	   collect (scan-declaration stream))))

(defun parse-arglist (stream)
  (let ((arglist nil))
    (do ()
	((not (symbol-or-string-p)) (nreverse arglist))
      (push (scan-term stream) arglist)
      (when (eq *current-token* +sep-comma+)
	  (lexmem stream)))))

(defun check-arity (arg root)
  (let ((arity (arity root)))
    (unless (symbol-ac-p root)
      (unless (= (length arg) arity)
	(format-output
	 "list of arguments ~A incompatible with arity of ~A:~A~%" arg root arity)
	(signal-parser-error
	 'arity-error
	 (format
	  nil
	  "list of arguments ~A incompatible with arity of ~A~%" arg root))))))

(defun scan-term (stream)
  (let ((root-name (scan-name stream)))
    (or 
     (find-var-with-name root-name *variables*)
     (let ((arg nil)
	   (root (find-symbol-from-name root-name))
	   (vbits nil)
	   (color nil)
	   (eti))
     (when (parser-color-p)
       (setq color (lexer-color *current-token*))
       (lexmem stream))
     (when (parser-vbits-p)
       (setf vbits (bits-from-chars (lexer-vbits *current-token*)))
       (lexmem stream))
     (when (eq *current-token* +sep-open-bracket+)
	 (setf eti (parse-int stream))
	 (parse-closed-bracket stream))
       (when (eq *current-token* +sep-open-par+)
	 (lexmem stream)
	 (setf arg (parse-arglist stream))
	 (parse-closed-par stream))
       (when (and (null root) *warn-new-symbol*)
	 (warn "~A not a defined symbol" root))
       (when root (check-arity arg root))
       (let ((s (or root (make-arity-symbol root-name (length arg)))))
	 (setf root (cond
		      (vbits (make-vbits-symbol s vbits))
		      (color (make-color-symbol s color))
		      (t s)))
	 (set-term-eti
	  (build-term root arg) eti))))))

(defun parse-term (stream)
  (lexmem stream)
  (scan-term stream))

(defun scan-termset (stream)
  (let ((termset nil))
    (do ()
	((not (symbol-or-string-p)) (nreverse termset))
      (push (scan-term stream) termset))))
 
(defun parse-termset (stream)
  (lexmem stream)
  (scan-termset stream))

(defun parse-var (stream)
  (make-var (scan-name stream)))

(defun parse-vars (stream)
  (loop
    while (symbol-or-string-p)
    do (parse-var stream)))

(defun parse-named-termset (stream)
  (lexmem stream)
  (if (symbol-or-string-p)
      (let ((name (scan-name stream)))
	(when (find-symbol-from-name name)
	  (format-output "Warning ~A also declared as a symbol~%" name)
	  (format-output "possibly missing termset name ~A ~%" name))
	(make-termset name (scan-termset stream)))
      (signal-parser-error 'name-expected-error "name expected")))

