;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric pa-position (position-assignment))
(defgeneric pa-assignment (position-assignment))

(defclass position-assignment ()
  ((position :initarg :position :reader pa-position :type position)
   (assignment :initarg :assignment :reader pa-assignment)))

(defun make-position-assignment (position assignment)
  (assert (typep position 'my-position))
  (make-instance 'position-assignment
		 :position position
		 :assignment assignment))

(defmethod print-object ((pa position-assignment) stream)
  (format stream "[~A:~A]" (pa-position pa) (pa-assignment pa)))

(defgeneric left-extend-position-assignment (position-assignment i))
(defmethod left-extend-position-assignment
    ((position-assignment position-assignment) (i integer))
  (make-position-assignment 
   (left-extend-position (pa-position position-assignment) i)
   (pa-assignment position-assignment)))

(defgeneric left-cut-position-assignment (position-assignment))
(defmethod left-cut-position-assignment
    ((position-assignment position-assignment))
  (make-position-assignment 
   (left-cut-position (pa-position position-assignment))
   (pa-assignment position-assignment)))

(defgeneric move-down-position-assignments (position-assignments i))
(defmethod move-down-position-assignments ((position-assignments list) (i integer))
  (mapcar
   #'left-cut-position-assignment
   (remove-if
    (lambda (position-assignment)
	   (/= (first (path (pa-position position-assignment))) i))
	 position-assignments)))
