;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric position-assignments (positions-assignment)
  (:documentation "the list position-assignments of POSITIONS-ASSIGNMENT"))

(defclass positions-assignment ()
  ((position-assignments :initarg :position-assignments
			 :reader position-assignments :type list)))

(defun make-positions-assignment (position-assignments)
  (assert (every (lambda (pa) (typep pa 'position-assignment)) position-assignments))
  (make-instance 'positions-assignment
		 :position-assignments position-assignments))

(defmethod print-object ((positions-assignment positions-assignment) stream)
  (format stream "<")
  (display-sequence (position-assignments positions-assignment) stream)
  (format stream ">"))

(defmethod compare-object
    ((psa1 positions-assignment) (psa2 positions-assignment))
  (and
   (subsetp (position-assignments psa1) (position-assignments psa2) :test #'compare-object)
   (subsetp (position-assignments psa2) (position-assignments psa1) :test #'compare-object)))

(defgeneric positions-assignment-union (positions-assignments1 positions-assignments2))

(defmethod positions-assignment-union (positions-assignment1 positions-assignment2)
  (make-positions-assignment
   (union (position-assignments positions-assignment1)
	  (position-assignments positions-assignment2)
	  :test #'compare-object)))

(defgeneric left-extend-positions-assignment (positions-assignment i)
  (:method ((positions-assignment positions-assignment) (i integer))
    (make-positions-assignment
     (loop
       for pa in (position-assignments positions-assignment)
       collect (left-extend-position-assignment pa i)))))

(defgeneric positions-assignment-to-etis (term positions-assignment &optional fun))
(defmethod positions-assignment-to-etis
 ((term term) (positions-assignment list) &optional (fun #'identity))
  (loop 
     for pa in positions-assignment
     when (funcall fun (pa-assignment pa))
     collect (cons (term-eti (term-at-position term (pa-position pa))) (pa-assignment pa))))

(defmethod positions-assignment-to-etis
    ((term term) (positions-assignment positions-assignment) &optional (fun #'identity))
  (positions-assignment-to-etis 
   term
   (position-assignments positions-assignment)
   fun))

(defgeneric get-etis-assignment (term positions-assignment)
  (:method ((term term) (positions-assignment positions-assignment))
    (positions-assignment-to-etis
     (term-num-leaves term)
     positions-assignment)))

(defgeneric apply-positions-assignment (term positions-assignment)
  (:method ((term term) (positions-assignment list))
    (if (term-zeroary-p term)
	(progn
	  (assert positions-assignment)
	  (assert (endp (cdr positions-assignment)))
	  (let ((pa (car positions-assignment)))
	    (set-term-eti
	     (build-term (make-vbits-symbol
			  (root term)
			  (pa-assignment pa)))
	     (term-eti term))))
	(build-term 
	 (root term)
	 (loop
	   for i from 0
	   for arg in (arg term)
	   collect
	   (apply-positions-assignment
	    arg
	    (mapcar #'left-cut-position-assignment
		    (remove-if-not
		     (lambda (pa) (= i (first (path (pa-position pa)))))
		     positions-assignment)))))))
  (:method ((term term) (positions-assignment positions-assignment))
    (apply-positions-assignment term (position-assignments positions-assignment))))

