;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric write-spec (spec stream))

(defvar *spec* nil "current specification")

(defgeneric specs-table (terms))

(defclass specs ()
   ((specs-table :initform (make-hash-table :test #'equal)
 	  :accessor specs-table)))

(defun make-specs ()
  (make-instance 'specs))

(defmacro with-spec (spec &body body)
  `(let ((*spec* ,spec))
     ,@body))

(defvar *specs* (make-specs) "hash-table of loaded specifications")

(defun current-spec () *spec*)
(defun current-specs () *specs*)

(defun init-specs ()
  (setf *specs* (make-specs)))

(defgeneric previous-term (term-spec))

(defclass term-spec (named-mixin)
  ((signature :initform nil :initarg :signature :accessor signature)
   (termset :initform nil :initarg :termset :accessor termset)
   (termsets :initform (make-termsets) :accessor termsets)
   (terms :initform (make-terms) :accessor terms)
   (term :initform nil :initarg :term :accessor term)
   (previous-term :initform nil :initarg :previous-term :accessor previous-term)
   ))

(defgeneric terms-table (terms))

(defclass terms ()
  ((terms-table :initform (make-hash-table :test #'equal)
		    :accessor terms-table)))

(defun make-terms ()
  (make-instance 'terms))

(defun fill-term-spec (spec &key (termsets nil) (terms nil))
  (loop for termset in termsets
     do (add-termset termset spec))
  (loop for term in terms
     do (add-term term spec))
  spec)
  
(defun make-term-spec
    (signature
     &key (termsets nil) (terms nil))
  (let ((spec (make-instance 'term-spec
		 :signature signature
)))
    (fill-term-spec spec :termsets termsets :terms terms)))

(defgeneric name-spec (spec name))
(defmethod name-spec ((spec term-spec) (name string))
  (rename-object spec name))
  
(defun add-term (term spec)
  (setf (gethash (name term) (terms-table (terms spec))) term))

(defun write-term (term stream)
  (when term
    (format stream "Term ")
    (show term stream)
    (format stream "~%")))

(defun write-terms (spec stream)
  (maphash (lambda (key value)
	     (declare (ignore key))
	     (write-term value stream))
	   (terms-table (terms spec))))

(defun write-termset (termset stream)
  (when termset
    (format stream "Termset ")
    (write-name (name termset) stream)
    (format stream " ")
    (display-sequence (terms-of termset) stream)
    (format stream "~%~%")))

(defun write-termsets (spec stream)
  (maphash (lambda (key value)
	     (declare (ignore key))
	     (write-termset value stream))
	   (termsets-table (termsets spec))))

(defgeneric add-termset (termset spec))
(defmethod add-termset ((termset termset) (spec term-spec))
  (setf (gethash (name termset) (termsets-table (termsets spec))) termset))

(defun add-current-termset ()
  (add-termset (termset (current-spec)) (current-spec)))

(defun add-current-term ()
  (add-term (term (current-spec)) (current-spec)))

(defgeneric set-current-termset (termset))

(defun unset-current-term ()
  (setf (term (current-spec)) nil))

(defun set-current-term (term)
  (setf (term (current-spec)) term))

(defmethod set-current-termset :before ((termset t))
  (format *error-output* "~A~%" (type-of termset)))

(defmethod set-current-termset ((termset termset))
  (setf (termset (current-spec)) termset)
  (add-current-termset))

(defun get-termset (name)
  (gethash name (termsets-table (termsets (current-spec)))))

(defun add-spec (spec specs)  
  (setf (gethash (name spec) specs) spec))

(defun write-current-spec (stream)
  (write-spec (current-spec) stream))

(defun set-current-spec (spec)
  (setf *spec* spec)
  (add-current-spec))

(defun add-current-spec ()
  (add-spec (current-spec) (specs-table *specs*)))

(defun get-spec (name)
  (gethash name (specs-table *specs*)))

(defun list-specs ()
  (list-keys (specs-table *specs*)))

(defmethod write-spec ((spec term-spec) stream)
  (format stream "Ops ~A~%~%" (signature spec))
  (write-terms spec stream)
  (write-termsets spec stream)
  (format stream "~%")
  )

(defgeneric display-spec (spec))

(defun display-current-spec ()
  (display-spec (current-spec)))

(defun set-and-display-current-spec (spec)
  (set-current-spec spec)
  (display-spec spec))

