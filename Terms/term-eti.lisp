;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric set-term-eti (term n))
(defmethod set-term-eti ((term term) n)
;;  (assert (term-constant-p term))
  (setf (term-eti term) n)
  term)

(defgeneric term-eti= (node1 node2)
  (:documentation "T if term-eti(node1) = term-eti(node2)"))

(defmethod term-eti= ((node1 term) (node2 term))
  (= (term-eti node1) (term-eti node2)))

(defgeneric nterm-num-leaves (term &key start)
  (:documentation "destructive; store a unique number on the eti slot of the leaves of the term")
  (:method ((term term) &key (start 0))
    (let ((n start))
      (loop for leave in (leaves-of-term term)
	    do (set-term-eti leave (incf n))))
    term))

(defgeneric term-num-leaves (term &key start force)
  (:documentation "store a unique number on the eti slot of the leaves of the term")
  (:method ((term term) &key (start 0) (force nil))
    (if (and (term-leaves-eti-p term) (not force))
	term
	(nterm-num-leaves (my-copy-term term) :start start))))

(defgeneric term-leaves-eti-p (term))
(defmethod term-leaves-eti-p ((term term))
  (let ((leaves (leaves-of-term term)))
    (or
     (endp leaves)
     (term-eti (car leaves)))))

(defgeneric term-eti-p (term))
(defmethod term-eti-p ((term term))
  (term-eti term))
