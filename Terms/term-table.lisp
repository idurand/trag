;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defun make-empty-term-table ()
  (make-hash-table :test #'eq))

(defun make-dag-struct (depth)
  (loop
    with vector = (make-array depth)
    for i from 0 below depth
    do (setf (aref vector i) (make-empty-term-table))
    finally (return vector)))

(defgeneric term-table-access (key term-table))
(defmethod term-table-access ((key list) (term-table hash-table))
  (loop
     for e in key
     and target = term-table then (gethash e  target)
     unless target
     do (return)
       finally (return (gethash e target))))

(defgeneric term-table-set (key rh term-table))
(defmethod term-table-set ((key list) (rh (eql nil)) (term-table hash-table)))

(defmethod term-table-set ((key list) rh (term-table hash-table))
  (let ((term (car key)))
    (if (null (cdr key))
	(setf (gethash term term-table) rh)
	(let ((target (gethash term term-table)))
	  (unless target
	    (setf target (setf (gethash term term-table) (make-empty-term-table))))
	  (term-table-set (cdr key) rh target))))
  term-table)


(defgeneric term-table-show (term-table &optional stream))
(defmethod term-table-show ((term-table hash-table) &optional (stream t))
  (labels
      ((aux (target tuple)
	 (if (term-p target)
	     (format
	      stream
	      "~A -> ~A~%"
	      (flat-term-from-key (reverse tuple)) target)
	     (maphash
	      (lambda (key value)
		(aux value (cons key tuple)))
	      target))))
    (aux term-table ())))

(defgeneric term-table-things-from (term-table f &optional op))
(defmethod term-table-things-from ((term-table hash-table) f &optional (op #'cons))
  (let ((things '()))
    (labels ((aux (target rtuple)
	       (if (term-p target)
		   (setf things (funcall op (funcall f target rtuple) things))
		   (maphash
		    (lambda (key value) (aux value (cons key rtuple)))
		    target))))
      (aux term-table ())
      things)))


(defgeneric term-table-tuples-rh (term-table))
(defmethod term-table-tuples-rh ((term-table hash-table))
  (term-table-things-from
   term-table
   (lambda (target rtuple) (list (reverse rtuple) target))))

(defgeneric term-table-tuples-from (term-table))
(defmethod term-table-tuples-from ((term-table hash-table))
   (term-table-things-from
    term-table
    (lambda (target rtuple) (declare (ignore target)) (reverse rtuple))))
 
