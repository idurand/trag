(in-package :terms)

(defclass term-to-multi-term-task ()
  ((num :type integer :initarg :num :accessor num)
   (object :initarg :object :accessor object-of)))

(defmethod print-object :after ((ttmtt term-to-multi-term-task) stream)
  (format stream "num = ~A, object = ~A~%" (num ttmtt) (object-of ttmtt)))

(defun make-term-to-multi-term-task (num object)
  (make-instance 'term-to-multi-term-task :num num :object object))

(defun apply-operation-stack (start ops)
  (loop with out := start
        for op in ops
        do (setf out (format nil "~a(~a)" op out))
        finally (return out)))

(defun multi-term-entry (idx content &key (name "t"))
  (format nil "~a~a = ~a." name idx content))

(defun format-call (root args)
  (format nil "~a(~{~a~#[~:;,~]~})" root args))

(defun operation-args-and-tasks (args counter &key (name "t"))
  (loop with op-args := nil
        with tasks := nil
        for a in args
        if (endp (arg a))
	  do (push a op-args) ;; push the whole term so that we have term-etis
	else
	  do (let ((idx (funcall counter)))
	       (push (make-term-to-multi-term-task idx a) tasks)
	       (push (format nil "~a~a" name idx) op-args))
        finally (return (values (reverse op-args) (reverse tasks)))))

(defgeneric term-to-multi-term-string (term &key name)
  (:method ((term term) &key (name "t"))
    (loop with res := nil
	  with jobs := (list (make-term-to-multi-term-task 0 term))
	  with counter := 0
	  with pre-ops := (list)
	  while jobs
	  for job := (pop jobs)
	  for jobidx := (num job)
	  for jobterm := (object-of job)
	  for root := (root jobterm)
	  for args := (arg jobterm)
	  do (cond ((endp args)
		    (push (multi-term-entry
			   jobidx
			   (apply-operation-stack root pre-ops)
			   :name name)
			  res)
		    (setf pre-ops nil))
		   ((endp (cdr args))
		    (push root pre-ops)
		    (push (make-term-to-multi-term-task
			   jobidx (first args)) jobs))
		   (t
		    (push
                     (multi-term-entry
		      jobidx
		      (apply-operation-stack
		       (format-call
			root
			(multiple-value-bind (op-args new-jobs)
			    (operation-args-and-tasks
			     args (lambda () (incf counter))
			     :name name)
			  (setf jobs (append new-jobs jobs))
			  op-args)) pre-ops)
		      :name name)
                     res)
		    (setf pre-ops nil)))
	  finally (return (format nil "~{~a~%~}" (reverse res))))))
