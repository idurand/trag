`p;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric constructor-symbols-p (term dsymbols)
  (:documentation "checks whether TERM is constructor according to the list of defined symbols DSYMBOLS"))

(defgeneric term-eti (term))

(defclass term (abstract-term)
  ((root :initarg :root :accessor root)
   (arg :initform nil :initarg :arg :accessor arg)
   ;; temporary slots
   (run :accessor run :initform nil)
   (term-eti :initform nil :accessor term-eti)))

(defun build-term (root &optional (arg nil))
  (make-instance 'term :root root :arg arg))

(defmethod compare-object ((term1 abstract-term) (term2 abstract-term))
  (let ((root (root term1)))
    (and
     (symbol-equal root (root term2))
     (let ((arg1 (arg term1))
	   (arg2 (arg term2)))
       (if (symbol-commutative-p root)
	   (and
	    (subsetp arg1 arg2 :test #'compare-object)
	    (subsetp arg2 arg1 :test #'compare-object))
	   (compare-object (arg term1) (arg term2)))))))

(defun bullet-term () (build-term *bullet-symbol*))

(defun extra-term () (build-term *extra-symbol*))

(defun omega-term () (build-term *omega-symbol*))

(defun empty-term () (build-term *empty-symbol*))

(defun term-p (term) (typep term 'term))

(defvar *show-run* nil)
(defvar *show-term-eti* t)

(defun toggle-show-run ()
  (setf *show-run* (not *show-run*)))

(defun toggle-show-term-eti ()
  (setf *show-term-eti* (not *show-term-eti*)))

(defmethod name ((term term))
  (format nil "~A" term))

(defmethod print-object :after ((term term) stream)
  (when (and *show-term-eti*
	     ;; (term-zeroary-p term)
	     (term-eti term))
    (format stream "[~A]" (term-eti term))))

(defmethod print-object ((term term) stream)
  (let ((root (root term))
	(arg (arg term)))
    (format stream "~A" root)
    (when (and *show-run* (slot-boundp term 'run))
      (format stream "<~A>" (run term)))
    (unless (term-zeroary-p term)
      (format stream "(")
      (display-sequence arg stream :sep ",")
      (format stream ")"))))

(defgeneric delete-run (term)
  (:method ((term term))
    (slot-makunbound term 'run)
    (unless (term-zeroary-p term)
      (mapc #'delete-run (arg term)))))

(defgeneric set-run (term run)
  (:method ((term term) run)
    (setf (run term) run)
    term))

(defmethod term-zeroary-p ((term term))
  (endp (arg term)))

(defmethod term-constant-p ((term term))
  (term-zeroary-p term))

(defun count-occ-atom (x term)
  (if (term-zeroary-p term)
      (if (compare-object x term) 1 0)
      (reduce #'+ (mapcar (lambda (arg) (count-occ-atom x arg)) (arg term)))))

(defun terms-size (terms)
  "size of list of TERMS"
  (loop
    with sum = 0
    for candidates = terms then (loop
				  for term in candidates
				  do (incf sum)
				  append (arg term))
    while candidates
    finally (return sum)))

(defgeneric term-size (term)
  (:documentation "size of a TERM")
  (:method ((term abstract-term)) 0)
  (:method ((term term)) (terms-size (list term))))

(defun terms-depth (terms)
  "depth of list of TERMS"
  (loop for candidates = terms then (loop
				      for term in candidates
				      append (arg term))
	for depth upfrom 0
	while candidates
	finally (return depth)))

(defgeneric term-depth (term)
  (:documentation "depth of TERM")
  (:method ((term term)) (terms-depth (list term))))

(defmethod symbols-from ((term term))
  (let ((symbols '()))
    (labels ((aux (st)
	       (if (term-constant-p st)
		   (pushnew (root st) symbols :test #'eq)
		   (let ((root (symbol-of (root st)))
			 (args (arg st)))
		     (loop for arg in args
			   do (loop for s in (symbols-from arg)
				    do (pushnew s symbols :test #'eq)))
		     (pushnew root symbols :test #'eq)))))
      (aux term)
      symbols)))

(defmethod signature ((term term)) (make-signature (symbols-from term)))

(defmethod my-copy-term ((term term))
  (set-term-eti
   (build-term (root term) (mapcar #'my-copy-term (arg term)))
   (term-eti term)))

(defun contains-bullet-p (term)
  (my-member (bullet-term) (subterms term)))

(defmethod constructor-symbols-p ((term abstract-term) (dsymbols list))
  (notany (lambda (st) (member (root st) dsymbols))
	  (strict-subterms term)))

(defgeneric build-zeroary-terms-from-symbols (csignature)
  (:documentation "list of zeroary terms in the signature CSIGNATURE containing constant symbols"))

(defmethod build-zeroary-terms-from-symbols ((signature signature))
  (mapcar (lambda (x) (build-term x))
	  (signature-symbols signature)))

(defgeneric nodes-satisfying-pred (term pred))
(defmethod nodes-satisfying-pred ((term term) pred)
  "Returns a list of the nodes contained in the term satisfying PRED"
  (let ((nodes '()))
    (labels
	((intern-get (subterm)
	   (when (funcall pred subterm)
	     (push subterm nodes))
	   (unless (term-zeroary-p subterm)
	     (mapcar #'intern-get (arg subterm)))))
      (intern-get term))
    (nreverse nodes)))

(defgeneric nodes-of-term (term))
(defmethod nodes-of-term ((term term))
  "Returns a list of the nodes of a term"
  (let ((nodes '()))
    (labels
	((intern-get (subterm)
	   (push subterm nodes)
	   (unless (term-zeroary-p subterm)
	     (mapcar #'intern-get (arg subterm)))))
      (intern-get term))
    nodes))

(defgeneric leaves-of-term (term))
(defmethod leaves-of-term ((term term))
  (nodes-satisfying-pred term #'term-constant-p))

(defgeneric colors-of-term (term)
  (:documentation "list of colors appearing on the constants")
  (:method ((term term))
    (let ((leaf-symbols (mapcar #'root (leaves-of-term term))))
      (loop for leaf-symbol in leaf-symbols
	    when (symbols::color-symbol-p leaf-symbol)
	      collect (color::color leaf-symbol)))))

(defgeneric max-color-of-term (term)
  (:documentation "max color appearing on the constants")
  (:method ((term term))
    (reduce #'max (colors-of-term term) :initial-value 0)))

(defgeneric nb-nodes-satisfying-pred (term pred))
(defmethod nb-nodes-satisfying-pred ((term term) pred)
  "Returns a list of the nodes contained in the term satisfying PRED"
  (let ((nb 0))
    (labels
	((intern-get (subterm)
	   (when (funcall pred subterm)
	     (incf nb))
	   (unless (term-zeroary-p subterm)
	     (mapcar #'intern-get (arg subterm)))))
      (intern-get term))
    nb))

(defgeneric term-nb-leaves (term))
(defmethod term-nb-leaves ((term term))
  (nb-nodes-satisfying-pred term #'term-constant-p))

(defun vars-depth-greater-than-one (lh)
  (and
   (not (var-p lh))
   (let ((vars (vars-of lh)))
     (set-difference
      vars
      (remove-if-not #'var-p (arg lh))))))

(defgeneric term-subst-symbols (term subst-symbol-fun)
  (:method ((terms list) fun)
    (mapcan
     (lambda (term) (term-subst-symbols term fun))
     terms))
  (:method ((term term) fun)
    (set-term-eti
     (build-term
      (funcall fun (root term))
      (mapcar (lambda (arg)
		(term-subst-symbols arg fun))
	      (arg term)))
     (term-eti term))))

(defgeneric term-subst-symbols-eti (term fun)
  (:method ((term term) fun)
    (set-term-eti
     (build-term
      (funcall fun (root term) (term-eti term))
      (mapcar (lambda (arg)
		(term-subst-symbols-eti arg fun))
	      (arg term)))
     (term-eti term))))

(defgeneric term-set-vbit-from-etis (term etis)
  (:method ((term term) (etis list))
    (term-subst-symbols-eti
     term
     (lambda (root eti)
       (let ((sym (symbol-of root)))
	 (if (symbol-constant-p sym)
	     (make-vbits-symbol
	      sym
	      (list (if (member eti etis) 1 0)))
	     sym))))))

(defmethod term-add-vbit-by-eti ((term term) fun)
  (term-subst-symbols-eti
    term
    (lambda (sym eti)
      (let* ((core (if (typep sym 'vbits-symbol)
                     (symbol-of sym)
                     sym))
             (vbits (if (typep sym 'vbits-symbol)
                      (decoration-of sym)
                      nil))
             (new-vbit (funcall fun eti core vbits))
             (new-vbits (if new-vbit (vbits::multiply-vbits vbits new-vbit)
                          vbits)))
        (if new-vbits (make-vbits-symbol core new-vbits) core)))))

(defmethod term-add-vbit-by-eti-list ((term term) etis &key inverted)
  (term-add-vbit-by-eti
    term
    (lambda (eti sym vbits)
      (declare (ignore sym vbits))
      (when eti (let ((found (find eti etis :test 'equal)))
                  (if (or (and found (not inverted))
                          (and (not found) inverted)) 1 0))))))

(defmethod term-add-vbits-by-eti-lists ((term term) lists &key inverted)
  (loop for etis in (cons nil lists)
        for i in (append (list nil) inverted
                         (make-list (length lists) :initial-element nil))
        for res := term then (term-add-vbit-by-eti-list res etis
                                                        :inverted i)
        finally (return res)))

;; in fact should return a list of terms
;; see where it is called
;; we need a special function for when the mapping is an application
;; (defmethod apply-signature-mapping ((term term))
;;   (term-subst-symbols
;;    term
;;    (lambda (x) (car (funcall
;; 		     (mapping-fun *signature-mapping*) x)))))

(defmethod apply-signature-mapping ((term term))
  (let ((args (loop for subterm in (arg term)
		    collect (apply-signature-mapping subterm)))
	(roots (apply-signature-mapping (root term))))
    (loop for root in roots
	  nconc (loop for arg in (cartesian-product args)
		      collect (build-term root arg)))))

(defgeneric terms-subst-symbols (terms subst-symbol-fun)
  (:method ((term term) h)
    (let ((syms (funcall h (root term)))
	  (args (cartesian-product
		 (mapcar
		  (lambda (arg)
		    (terms-subst-symbols arg h))
		  (arg term)))))
      (loop for sym in syms
	    nconc (loop for arg in args
			collect (build-term sym arg))))))

(defgeneric make-term-enumerator (term h)
  (:documentation
   "term enumerator based on the function H
    and the term TERM")
  (:method ((term term) h)
    (make-funcall-enumerator
     (lambda (tuple)
       (build-term (first tuple) (cdr tuple)))
     (make-enumerator-cons
      (make-list-enumerator (funcall h (root term)))
      (apply #'make-enumerator-list
	     (mapcar
	      (lambda (arg)
		(make-term-enumerator arg h))
	      (arg term)))))))

(defgeneric term-depth-prefix (term depth)
  (:documentation "return the prefix of depth DEPTH of the ters TERM")
  (:method ((term term) (depth integer))
    (if (zerop depth)
	(empty-term)
	(let ((root (root term))
	      (arg (arg term)))
	  (set-term-eti
	   (build-term
	    root
	    (mapcar (lambda (st) (term-depth-prefix st (1- depth))) arg))
	   (term-eti term))))))

(defgeneric unary-term-to-string (unary-term)
  (:method ((unary-term term))
    (loop
      with s = ""
      for term = unary-term then (car (arg term))
      until (term-constant-p term)
      do (setq s (concatenate 'string s (name (root term))))
      finally (return s))))

(defgeneric term-add-random-vbit (term)
  (:method ((term term))
    (term-subst-symbols
     term
     (lambda (s)
       (car (funcall (symbols::nothing-to-random-partition-h 1) s))))))
