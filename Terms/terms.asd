;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Terms.
(in-package :asdf-user)

(defsystem :terms
  :description "Symbols and Terms"
  :name "terms"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:enum :symbols :cl-ppcre)
  :serial t
  :components
  ((:file "package")
   (:file "abstract-term")
   (:file "variables")
   (:file "aux-var")
   (:file "vars-subst")
   (:file "path")
   (:file "position")
   (:file "term")
   (:file "parse-multi-term")
   (:file "term-to-multi-term")
   (:file "term-eti")
   (:file "term-variable")
   (:file "term-pos")
   (:file "position-assignment")
   (:file "positions-assignment")
   (:file "omega-terms")
   (:file "aterm")
   (:file "unify")
   (:file "terms")
   (:file "termset")
   (:file "arities")
   (:file "abstract-state")
   (:file "flat-term-state")
   (:file "lexer")
   (:file "parser")
   (:file "input")
   (:file "output")
   (:file "specification")
   (:file "term-to-egraph")
   (:file "enumeration")
   ))

  (pushnew :terms *features*)
