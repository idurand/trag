;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defun redex (term lhs)
  (if (linear-terms-p lhs)
      (linear-unify-with-a-term term lhs)
      (unify-with-a-term term lhs)))

(defun instance-of-any (term lhs)
  (find-if (lambda (lh) (instance term lh)) lhs))

(defun orthogonal-p (lhs)
  (and (linear-terms-p lhs) (not (overlapp lhs))))
 
(defun normal-form (term lhs)
  (or
   (term-constant-p term)
   (and
    (not (redex term lhs)) 
    (every (lambda (x) (normal-form x lhs)) (arg term)))))

(defun overlapp (lhs)
   (or 
    (overlapp-patterns lhs)
    (overlapp-subpatterns lhs (strict-subterms-list lhs))))

(defun overlapp-patterns (l)
  (if (null l)
       nil
       (or
	(linear-unify-with-a-term (car l) (cdr l))
	(overlapp-patterns (cdr l)))))

(defun overlapp-subpatterns (lhs subpatterns)
  (find-if (lambda (x) (linear-unify-with-a-term x subpatterns)) lhs))

(defun filter-non-redex (l lhs)
  (remove-if (lambda (x) (redex x lhs)) l))

(defun equiv-terms-p (terms1 terms2)
  (and
   (= (length terms1) (length terms2))
   (subsetp terms1 terms2 :test #'compare-object)
   (subsetp terms1 terms2 :test #'compare-object)))

(defgeneric redex-positions-lhs (term lhs)
  (:documentation "return a list of redex positions in TERM according to the left-handsides LHS"))

(defmethod redex-positions-lhs ((term term) lhs)
  (let ((tp (term-positions term)))
    (remove-if-not (lambda (p) (redex (term-at-position term p) lhs)) tp)))

(defgeneric outermost-redex-positions-lhs (term lhs)
  (:documentation "return a list of outermost redex positions in TERM according to the left-handsides LHS"))

(defmethod outermost-redex-positions-lhs ((term term) lhs)
  (outer-positions (redex-positions-lhs term lhs)))
