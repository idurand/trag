;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defgeneric terms-of (sys)
  (:documentation "termset"))

(defgeneric (setf terms-of) (newterms termset)
  (:documentation "replaces the terms in TERMSET by NEWTTERMS"))

(defgeneric termset-merge-signature (termset newsignature)
  (:documentation "merge a signature in a termset signature"))

(defgeneric termsets-table (termsets))
(defclass termsets ()
  ((termsets-table :initform (make-hash-table :test #'equal)
		    :accessor termsets-table)))

(defun make-termsets ()
  (make-instance 'termsets))

(defclass termset (named-mixin)
  ((terms :initform nil
	  :initarg :terms
	  :reader terms-of)
   (aut-termset :initform nil)
   (signature :initform nil
	      :initarg :signature
	      :reader signature)))

(defun make-termset (name terms)
  (make-instance 'termset
		 :name name
		 :terms terms
		 :signature
		 (signature terms)))

(defmethod print-object :after ((l termset) stream)
  (format stream ": ")
  (display-sequence (terms-of l) stream)
  (format stream "~%")
  (when (and (not (ground-termset-p (terms-of l))))
    (let ((additional-signature (signature-difference
				 (signature l)
				 (signature (terms-of l)))))
      (unless (signature-empty-p  additional-signature)
	(format stream "additional signature: ~A ~%" additional-signature)))))
	

(defmethod (setf terms-of) (newterms (lang termset))
  (with-slots (terms signature aut-termset) lang
    (unless (and
	     (equiv-terms-p terms newterms)
	     (equiv-signature-p (signature terms) (signature lang)))
      (setf aut-termset nil)
      (setf signature (signature newterms))
      (setf terms newterms))
  terms))

(defmethod termset-merge-signature ((lang termset) newsignature)
  (with-slots (signature aut-termset) lang
    (let ((merge-signature (merge-signature signature newsignature)))
      (unless (equiv-signature-p signature merge-signature)
	(setf aut-termset nil signature merge-signature))
      merge-signature)))

(defun ground-termset-p (termset)
  (every #'term-ground-p termset))

(defun equiv-termset-p (l1 l2)
  (and
   (equiv-signature-p (signature l1) (signature l2))
   (equiv-terms-p (terms-of l1) (terms-of l2))))
