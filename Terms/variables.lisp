;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :terms)

(defclass abstract-variable (named-mixin abstract-zeroary-term) ())
(defclass var (abstract-variable) ())

(defmethod print-object :after ((v abstract-variable) stream))
;;  (format stream "!"))

(defmethod show ((v abstract-variable) &optional stream)
  (format stream "~A" (name v)))

(defun var-p (v)
  (typep v 'abstract-variable))

(defmethod vars-of ((v abstract-variable)) (list v))

(defvar *variables* 
  '()
  "variables declared in spec")

(defun make-empty-variables () '())

(defun init-variables ()
  (setf *variables* (make-empty-variables)))

(defun find-var-with-name (name vars)
  (car (member name vars :test #'equal :key #'name)))

(defmethod compare-object ((v1 abstract-variable)
			   (v2 abstract-variable))
  (equal (name v1) (name v2)))

(defun make-var (name)
  (let ((found (find-var-with-name name *variables*)))
    (or found
	(let ((newvar (make-instance 'var :name name)))
	  (push newvar *variables*)
	  newvar))))
