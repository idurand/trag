;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termwrite)
(defvar *det-aut-c* t)
(defvar *d-iterations* 0)

(defgeneric cstates (ctransitions))
(defmethod cstates ((ctransitions abstract-transitions))
  (let ((target (transitions-compute-target (bullet-term) ctransitions)))
    (assert target)
    (if *det-aut-c*
	(make-ordered-state-container (list target))
	target)))

(defun make-d-transitions-for-constants (aut-c csignature aut-b )
  (mapc
   (lambda (sym)
       (let*
	   ((bc (transitions-compute-target (build-term sym) (transitions-of aut-b)))
	    (sc (apply-transitions sym '() (transitions-of aut-c)))
	    (pc (if (bc-redex-p bc)
		    *cstates* ; redex
		    '()))
	    (dstate (make-dstate sc bc pc :deterministic *det-aut-c*)))
	 (funcall
	  (if *det-aut-c*
	      #'add-dtransition-to-global-transitions
	      #'add-transition-to-global-transitions)
	  sym '() dstate)))
   (signature-symbols csignature)))


;; p1 list for ndet oc for det
(defgeneric adapt-p-when-redex (p1))
(defmethod adapt-p-when-redex (p1)
  (if *det-aut-c*
      (target-union *cstates* p1)
      (g-adapt-p-when-redex p1)))
;; NEW
;; ??

;; rhap liste de ocontainer (non det
;; retourne  liste de ocontainer (non det) ou ocontainer (det)
(defgeneric transform-rhap (rhap))
(defmethod transform-rhap ((rhap list))
  (if *det-aut-c*
      (and rhap
	   (make-ordered-state-container rhap))
      (g-transform-rhap rhap)))

(defun new-p-rh (symbol lds1 lds2 p ctransitions)
;; if *det-aut-c* return list of gstates else list of ordered-state-containers
;;   (format *error-output*
;;	    "new-p-rh symbol ~A  lds1 ~A lds2 ~A p ~A~%" symbol lds1 lds2 p)
   (let ((args
	   (mapcar (lambda (q) (append lds1 (list q) (cdr lds2)))
		   (expand p))))
;;     (format *error-output* "terms ~A" args)
     (loop with targets = '()
	   for arg in args
	   do
	      (let ((target (apply-transitions-gft symbol arg ctransitions)))
		(when target
		  (unless *det-aut-c*
		    (when (casted-state-p target)
		      (setf target (make-ordered-state-container (list target)))))
		  (pushnew target targets :test #'compare-object)))
	   finally (return (nreverse targets)))))


;; pour f (S1,S2,S3) (P1,P2,P3) 
;; donne (f(p11,S2,S3)^ f(p12,S2,S3)^ 
;;        f(S1,p21,S3)^ f(S1,p22,S3)^
;;        f(S1,S2,p31)^ f(S1,S2,p32)^) pour
;; tout Pi non nul 
(defun rhs-avec-un-p (symbol lds ldp ctransitions)
;;   (format *error-output*
;;	    "rhs-avec-un-p symbol ~A  lds ~A ldp ~A~%" symbol lds ldp)
  (do
   ((lds1 nil (append lds1 (list (car lds2))))
    (lds2 lds (cdr lds2))
    (ldp ldp (cdr ldp))
    (aux '()))
   ((endp lds2) (values t (my-remove-duplicates aux)))
    (unless (null (car ldp))
      (let ((ll (new-p-rh symbol lds1 lds2 (car ldp) ctransitions)))
	(if (member nil ll)
	    (return (values nil nil))
	    (setf aux (nconc aux ll)))))))

;; lds1 S-1 ...S-i-1
;; lds2 S-i+1 ... S-n
;; p P-i
;; retourne la liste des  gstates obtenus 
;; par application des regles 
;; sur (symbol S-1 ...S-i-1 {q} S-i+1 ... S-n)
;; pour chaque element q de P-i
;; retourne un o-state-container de gstates (det) ou de ordered-state-containers (ndet)
(defun p1rh (root arg ctransitions)
  (let* ((lds (mapcar #'s-comp arg))
	 (ldp (mapcar #'p-comp arg)))
;;     (format *error-output*
;; 	    "p1rh root ~A  lds ~A ldp ~A~%" root lds ldp)
    (multiple-value-bind (res rhap)
	(rhs-avec-un-p root lds ldp ctransitions)
;;      (format *error-output* "res rhs-avec-un-p ~A  rhap ~A ~%" res rhap)
      (if res
	  (values t (transform-rhap rhap))
	  (values nil nil)))))

;; if returns t, the second value is a list of gstates (det) or ostate-container (ndet)
(defgeneric create-prh (root arg bc ctransitions))
(defmethod create-prh (root (arg list) (bc ordered-state-container) ctransitions)
  (multiple-value-bind (res p1)
      (p1rh root arg ctransitions)
    (if (null res)
	(values nil nil)
	(values t
		(if (bc-redex-p bc)
		    (adapt-p-when-redex p1)
		    p1)))))

(defgeneric expand (casted-state))
(defmethod expand ((casted-state casted-state))
  (list casted-state))

(defmethod expand ((states ordered-state-container))
  (mapcar
   (lambda (casted-state)
     (make-ordered-state-container (list casted-state)))
   (container-contents states)))

;; prh list for ndet o-state-container for det
(defgeneric create-transitions (root states bc srh prh))
(defmethod create-transitions ((root abstract-symbol) (states list) bc srh prh)
  (if *det-aut-c*
      (det-create-transitions root states bc srh prh)
      (g-create-transitions root states bc srh prh)))

(defun gen-d-transitions (root arg ctransitions aut-b)
  (let* ((srh (apply-transitions-gft root (mapcar #'s-comp arg) ctransitions))
	 (target (apply-transitions-gft
		  root
		  (b-projection arg)
		  (transitions-of aut-b)))
	 (bc (and target (target-to-container target))))
    (unless (null srh)
      (multiple-value-bind (res prh)
	  (create-prh root arg bc ctransitions)
	(unless (null res)
	  (create-transitions root arg bc srh prh))))))

(defun adapt-d-transitions-for-one-symbol (symbol afai ctransitions aut-b)
;;  (format t "adapt-d-transitions-for-one-symbol ~A ~A~%" symbol (length afai))
  (mapc
   (lambda (arg)
     (gen-d-transitions symbol arg ctransitions aut-b))
   afai))

;;(defun adapt-d-transitions (ncsignature states newstates ctransitions aut-b)
(defgeneric adapt-d-transitions (ncsignature states newstates ctransitions aut-b))
(defmethod adapt-d-transitions
    (ncsignature (states ordered-state-container) (newstates ordered-state-container) ctransitions aut-b)
  (let* ((max-arity (max-arity ncsignature))
	 (afa (arrange-for-arities-and-filter
	       (append (container-contents states) (container-contents newstates)) max-arity (container-contents states) ncsignature)))
    (incf *d-iterations*)
    (mapc
     (lambda (symbol)
       (let* ((arity (arity symbol))
	      (afai (aref afa arity)))
	 (adapt-d-transitions-for-one-symbol
	  symbol
	  afai
	  ctransitions
	  aut-b)))
     (signature-symbols ncsignature))))

(defun make-d-transitions (signature aut-c aut-b partial)
  (let ((ctransitions (transitions-of aut-c)))
    (make-d-transitions-for-constants aut-c (constant-signature signature) aut-b)
    (do* ((states (make-empty-ordered-state-container))
	  (newstates (sym-table-cright-handsides *global-sym-table*)
		     (container-difference (sym-table-cright-handsides *global-sym-table*) states)))
	((or
	  (container-empty-p newstates)
	  (and partial
	       (container-find-if
		(lambda (dstate) (is-final-state-d dstate (get-finalstates aut-c)))
		newstates))))
      ;; (show *global-sym-table*)
      ;; (format t "states ~A~%"  states)
      ;; (format t "newstates ~A~%"  newstates)
      (adapt-d-transitions
       (non-constant-signature signature) states newstates ctransitions aut-b)
      (container-nunion states newstates))))

(defun make-d-automaton (aut-c aut-b &key deterministic (partial nil))
  (let ((*det-aut-c* deterministic)
	(signature (signature aut-c)))
    (with-new-transitions signature
      (setf *d-iterations* 0)
      (init-dstates)
      (let*
	  ((*st-redex*
	    (find-casted-l-state (transitions-of aut-b))) ; 0
	   (*st-reducible*
	    (find-casted-r-state (transitions-of aut-b))) ; 1
	   (*cstates* (cstates (transitions-of aut-c)))
	   (signature (remove-bullet-symbol signature)))
	(make-d-transitions
	 signature
	 aut-c
	 aut-b
	 partial)
	(let* ((states
		(states-from-states-table *automaton-states-table*))
	       (fs (container-remove-if-not
		    (lambda (dstate)
		      (is-final-state-d dstate (get-finalstates aut-c)))
		    states)))
	  (nreduce-automaton
	   (make-table-automaton
	    (make-table-transitions-from-global-sym-table)
	    :name (format nil
			  "aut-d-~A"
			  (if deterministic "det" "ndet"))
	    :finalstates fs)))))))
