;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termwrite)

(defgeneric det-create-transitions (root states bc srh prh))
(defmethod det-create-transitions ((root abstract-symbol) (states list) bc srh prh)
  (assert (or (null prh) (ordered-state-container-p prh)))
  ;; creates one rule in this deterministic case
  (add-dtransition-to-global-transitions root states (make-dstate srh bc prh)))


