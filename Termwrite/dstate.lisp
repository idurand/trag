;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termwrite)

(defvar *st-reducible* nil)
(defvar *cstates* nil)
(defvar *st-redex* nil)

(defclass dstate (uncasted-state)
  ((s-comps :allocation :class :accessor s-comps)
   (b-comps :allocation :class :accessor b-comps)
   (p-comps :allocation :class :accessor p-comps)
   (sc :initarg :sc :reader s-comp)
   (bc :initarg :bc :reader b-comp)
   (pc :initarg :pc :reader p-comp)))

(defun init-dstates ()
  (let ((s (make-instance 'dstate)))
    (setf (s-comps s) nil (b-comps s) nil (p-comps s) nil)))

(defgeneric make-b-comp (target))
(defmethod make-b-comp ((b casted-state))
  (make-b-comp (make-ordered-state-container (list b))))

(defmethod make-b-comp ((b ordered-state-container))
  (let ((s (make-instance 'dstate))) ;; only to access b-comps
    (or (car (member b (b-comps s) :test #'compare-object))
	(prog1 b
	  (push b (b-comps s))))))

(defun make-s-comp (s deterministic)
  (assert (or (casted-state-p s) (ordered-state-container-p s)))
  (if deterministic
      s
      (let ((ts (make-instance 'dstate)))
	(when (casted-state-p s)
	  (setf s (make-ordered-state-container (list s))))
	(or (car (member s (s-comps ts) :test #'compare-object))
	  (prog1 s (push s (s-comps ts)))))))

(defgeneric make-p-comp (target))
(defmethod make-p-comp ((p (eql nil)))
  nil)

(defmethod make-p-comp ((p casted-state))
  (make-p-comp (make-ordered-state-container (list p))))

(defmethod make-p-comp ((p ordered-state-container))
  (let ((s (make-instance 'dstate)))
    (or (car (member p (p-comps s) :test #'compare-object))
	(prog1 p (push p (p-comps s))))))

(defun make-dstate (sc bc pc &key (deterministic t))
  (let* ((s (make-s-comp sc deterministic))
	 (b (make-b-comp bc))
	 (p (make-p-comp pc)))
    (make-instance 'dstate :sc s :bc b :pc p)))

(defmethod print-object ((dstate dstate) stream)
  (format stream "<")
  (format stream "~A : ~A : ~A" (s-comp dstate) (b-comp dstate) (p-comp dstate))
  (format stream ">"))

(defmethod b-comp ((d-state casted-state))
  (b-comp (in-state d-state)))

(defmethod s-comp ((d-state casted-state))
  (s-comp (in-state d-state)))

(defmethod p-comp ((d-state casted-state))
  (p-comp (in-state d-state)))

(defgeneric are-finalstates (pc cfinalstates))
(defmethod are-finalstates ((pc ordered-state-container) (cfinalstates ordered-state-container))
  (every (lambda (state)
	   (container-member state cfinalstates))
	 (container-contents pc)))

(defgeneric bc-redex-p (target))

(defmethod bc-redex-p ((bc ordered-state-container))
  (container-member *st-redex* bc))

(defmethod bc-redex-p ((bc (eql nil)))
  nil)

(defmethod bc-redex-p ((bc casted-state))
  (eq bc *st-redex*))

(defgeneric is-final-state-d (d-state cfinalstates))
(defmethod is-final-state-d (d-state (cfinalstates ordered-state-container))
  (let ((bc (b-comp d-state))
	(pc (p-comp d-state)))
    (and
     (not (bc-redex-p bc))
     (container-member *st-reducible* bc)
     (not (null pc))
     (are-finalstates pc cfinalstates)
     d-state)))

(defun b-projection (arg)
  (mapcar #'b-comp arg))

