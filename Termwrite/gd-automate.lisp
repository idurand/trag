;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termwrite)

(defgeneric g-create-transitions (root states bc srh prh))

(defmethod g-create-transitions ((root abstract-symbol) (states list) bc srh (prh list))
  (mapc
   (lambda (oc) (add-transition-to-global-transitions
		 root states
		 (make-dstate srh bc oc :deterministic nil)))
   prh))
;; p1 liste de ordered-state-containers
;; retourne liste de ordered-state-containers
(defgeneric g-adapt-p-when-redex (p1))
(defmethod g-adapt-p-when-redex ((p1 list))
  (my-delete-duplicates
   (mapcar
    (lambda (oc) (target-to-container (target-union oc *cstates*)))
    p1)))

;; rhap liste de ordered-state-containers
;; retourne liste de ordered-state-containers
;; rhs avec un p
(defgeneric g-transform-rhap (rhap))
(defmethod g-transform-rhap ((rhap list))
  (let ((cp  (cartesian-product 
	      (mapcar #'not-null-powerset
		      (mapcar
		       #'container-contents
 		       rhap)))))
    (mapcar
     (lambda (l)
       (make-ordered-state-container (reduce #'append l)))
     cp)))

