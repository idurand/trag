;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termwrite)

(defvar *u-states*) ; container d'etats de u

(defgeneric make-q (theta term allstates))
(defmethod make-q (theta term (allstates ordered-state-container))
  (if (var-p term)
      (let ((insubst (assoc term theta :test #'compare-object)))
	(if insubst
	    (list (second insubst))
	    (container-contents *u-states*)))
      (list (find-casted-state-from-state
	     (index-state (make-term-state term) 0)
	     *u-states*))))

(defgeneric make-q-state-containers (theta args allstates))
(defmethod make-q-state-containers (theta args (allstates ordered-state-container))
  (mapcar
   (lambda (arg)
     (make-ordered-state-container (make-q theta arg allstates)))
   args))

(defgeneric make-q-args (theta args allstates))
(defmethod make-q-args (theta args (allstates ordered-state-container))
  (mapcar
   (lambda (arg)
     (make-q theta arg allstates))
   args))

;; returns T if transition really added
(defgeneric add-transitions-for-theta (lh theta target sym-table))
(defmethod add-transitions-for-theta
    ((lh term) theta target (sym-table sym-table))
  (let ((args (cartesian-product
	       (make-q-args theta (arg lh) *u-states*)))
	(new nil))
    (mapc
     (lambda (arg)
       (setf new
	     (or new
		 (add-transition-to-sym-table 
		  (root lh) arg target sym-table))))
     args)
    new))

(defgeneric add-transitions-for-theta (var theta target sym-table)
  (:method ((lh var) theta target (sym-table sym-table))
    (format *error-output* "add-transitions-for-theta (lh var) ~%")
    (let ((state (cadr (assoc lh theta))))
      (add-transition-to-sym-table state '() target sym-table))))

;; returns T if at least one transition has been added
(defgeneric add-transitions-for-lh (lh tsl transitions))
(defmethod add-transitions-for-lh ((lh abstract-term) (tsl list) (sym-table sym-table))
  (let ((new nil))
    (dolist (theta-state tsl)
      (setf new
	    (or new
		(add-transitions-for-theta
		 lh (car theta-state)
		 (make-target
		  (make-ordered-state-container (cadr theta-state)))
		 sym-table))))
    new))

(defgeneric make-theta-state-list (rh transitions)
  (:method (rh (transitions table-transitions))
;; rh term or variable!!!
  (let* ((vars (vars-of rh))
	 (substs (g-gen-substitution vars (container-contents *u-states*))))
    (remove-if #'null
	       (mapcar
		(lambda (theta)
		  (let* ((term (apply-substitution rh theta))
			 (target (if (abstract-state-p term)
				     term
				     (transitions-compute-target term transitions :signal nil))))
		    (list theta (target-contents target))))
		substs)
	       :key #'cadr))))

;; Returns T if at least one transition has been added
(defgeneric add-transitions-for-rule (transitions rule))
(defmethod add-transitions-for-rule ((transitions table-transitions) (rule rule))
  (add-transitions-for-lh
   (left-handside rule)
   (make-theta-state-list (right-handside rule) transitions)
   (sym-table transitions)))

(defgeneric add-transitions-for-rules (transitions rules)
  (:documentation "saturates TRANSITIONS with RULES")
  (:method ((transitions table-transitions) (rules list))
    (let ((new nil))
      (mapc
       (lambda (rule)
	 (setq new (or new (add-transitions-for-rule transitions rule))))
       rules)
      new)))

(defgeneric nsaturation (rules automaton))
(defmethod nsaturation ((rules rules) (automaton table-automaton))
  (let* ((transitions (transitions-of automaton))
	 (rules-list (rules-list rules))
	 (*u-states* (get-states automaton)))
    (loop
       until
	 (or (not (add-transitions-for-rules
		   transitions
		   rules-list)))))
;;  automaton)
  (epsilon-closure-automaton automaton))

(defgeneric saturation (rules automaton))
(defmethod saturation ((rules rules) (automaton table-automaton))
  (nsaturation rules (duplicate-automaton automaton)))
