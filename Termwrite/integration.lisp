;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termwrite)

(defun u-automaton (signature lhs aut-a)
  (let ((aut-b (make-matching-automaton lhs signature :strict t)))
    (disjoint-union-automaton aut-b aut-a)))

(defun make-u-automaton (signature lhs aut-a &key (bullet nil) (extra nil))
  (when bullet
    (setf signature (add-bullet-symbol signature)))
  (when extra
    (setf signature (add-extra-symbol signature)))
   (let ((usignature (merge-signature signature (signature aut-a))))
     (u-automaton usignature lhs aut-a)))

(defgeneric nfold-constant-states (table-automaton))

(defmethod nfold-constant-states ((automaton table-automaton))
  (let ((states (get-states automaton))
	(constant-symbols
	 (signature-symbols
	  (constant-signature (signature automaton)))))
    (flet ((aux (i)
	     (mapcan
	      (lambda (csym)
		(let ((s (find-casted-state-from-state
			  (index-state (make-term-state (build-term csym)) i)
			  states)))
		  (and s (list s))))
	      constant-symbols)))
      (let* ((zeros  (aux 0))
	     (ones (aux 1))
	     (rest (set-difference (container-contents states) (append zeros ones))))
	(napply-states-mapping
	 automaton
	 (pairlis (append zeros rest) (append ones rest)))))))

(defgeneric ndet-saturation-c-automaton (seqsys aut-u))
(defmethod ndet-saturation-c-automaton ((sys seqsys) (aut-u table-automaton))
  (let ((aut-c (nreduce-automaton
		(nfold-constant-states
		 (saturation (get-approx-rules sys) aut-u)))))
    (rename-object
     aut-c
     (format nil "aut-c-jacquemard-~A~A~A~A"
	     (name sys)
	     (if (in-signature-extra
		  (signature aut-c)) "@-" "")
	     (get-approx-name (seqsys-approx sys))
	     (if (in-signature-bullet (signature aut-u)) "-o" "")))))

(defgeneric make-c-automaton (seqsys aut-u deterministic &key minimize))

(defmethod make-c-automaton ((sys seqsys) (aut-u table-automaton) deterministic &key (minimize nil))
  (if deterministic
      (det-saturation-automaton sys aut-u :minimize minimize)
      (ndet-saturation-c-automaton sys aut-u)))
