;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :termwrite)

; retourne un ordered-state-container d'etats de u
(defgeneric make-new-rh (rhetheta nlstates))
(defmethod make-new-rh (rhetheta (nlstates ordered-state-container))
  (if (casted-state-p rhetheta)
      (gstate-container (in-state rhetheta))
      (if (var-p rhetheta)
	  *u-states*
	  (let* ((vars (vars-of rhetheta))
		 (gstates
		  (mapcar
		   (lambda (theta)
		     (let ((term (apply-substitution rhetheta theta)))
;;		       (sym-table-apply (root term) (arg term) *global-sym-table*)
		       ;;		       ici remplace sym-table-compute-target qui avait disparu mais...
		       (if (abstract-state-p term)
			   term
			   (transitions-compute-target 
			    term
			    (make-table-transitions-from-global-sym-table)))))
		   (g-gen-substitution vars (container-contents nlstates))))
		 (containers (mapcar (lambda (cgs) (gstate-container (in-state cgs)))
				      (remove nil gstates))))
	    ;;	    (format *trace-output* "~A containers~%" containers)
	    (container-nmerge-containers
	     (make-empty-ordered-state-container) containers)))))

(defun gmatch (argse args)
  (loop
    for c1 in argse
    and c2 in args
    when (container-empty-p (container-intersection c1 (gstate-container (in-state c2))))
      do (return)
    finally (return t)))

(defgeneric update-rhs-deltatransition-with-rule (key rule nlstates))
;; state-container updated with new accessible states
(defmethod update-rhs-deltatransition-with-rule (key rule (nlstates ordered-state-container))
  (let* ((lhe (left-handside rule))
	 (rhe (right-handside rule))
	 (argse (make-q-state-containers '() (arg lhe) *u-states*)))
    (and (symbol-equal (root lhe) (car key))
	 (gmatch argse (cdr key))
	 (let* ((theta (remove-if-not
			(lambda (x) (var-p (car x)))
			(mapcar #'list (arg lhe) (cdr key))))
		(rhetheta  (apply-substitution rhe theta)))
	   (make-new-rh rhetheta nlstates)))))

(defgeneric update-rhs-deltatransition-with-rules
    (key value rules nlstates))
(defmethod update-rhs-deltatransition-with-rules
    (key (value casted-state) (rules rules) (nlstates ordered-state-container))
  (let ((oldcontainer (container-copy (gstate-container (in-state value))))
	(new-containers
	 (remove nil
		 (mapcar
		  (lambda (rule)
		    (update-rhs-deltatransition-with-rule key rule nlstates))
		  (rules-list rules)))))
    (container-nmerge-containers  oldcontainer new-containers)))

(defgeneric update-deltatransitions-with-rules (rules nlstates))
(defmethod update-deltatransitions-with-rules
    ((rules rules) (nlstates ordered-state-container))
  (let ((new nil))
    (sym-table-map
     (lambda (key target)
;;       (format *error-output* "~A ~A ~%" key target)
       (let ((container
	      (update-rhs-deltatransition-with-rules
	       key target rules nlstates)))
	 (when (> (container-size container)
		  (container-size (gstate-container (in-state target))))
	   (setf new t)
	   (add-dtransition-to-global-transitions (car key) (cdr key) (make-gstate container))
	   )))
     *global-sym-table*)
    new))

(defgeneric new-add-deltatransitions (afa transitions ncsignature))
(defmethod new-add-deltatransitions (afa transitions (ncsignature signature))
  (loop for symbol in (signature-symbols ncsignature)
     do
       (loop for arg in (aref afa (arity symbol))
	  do
	     (let ((target (apply-transitions-gft
			    symbol
			    (mapcar
			     (lambda (c) (gstate-container (in-state c)))
			     arg)
			    transitions)))
	       (when target
		 (add-dtransition-to-global-transitions symbol arg (make-gstate target))
		 )))))

(defgeneric adapt-deltatransitions (rules states newstates aut-u))
(defmethod adapt-deltatransitions ((rules rules) states newstates aut-u)
  (let* ((signature (signature aut-u))
	 (ncsignature (non-constant-signature signature))
	 (transitions (transitions-of aut-u))
	 (nlstates (container-union states newstates))
	 (afa (arrange-for-arities-and-filter
	       (container-unordered-contents nlstates) (max-arity ncsignature)
	       (container-unordered-contents states) signature)))
    (new-add-deltatransitions afa transitions ncsignature)
    (update-deltatransitions-with-rules rules nlstates)))

(defgeneric add-deltatransitions-for-constants (aut-u))
(defmethod add-deltatransitions-for-constants ((aut-u abstract-automaton))
  (let ((csignature (constant-signature (signature aut-u)))
	(transitions (transitions-of aut-u)))
    (mapc
     (lambda (symbol)
       (let ((target (apply-transitions symbol () transitions)))
	 (unless (target-empty-p target)
	   (add-dtransition-to-global-transitions symbol '() (make-gstate target))
	   )))
     (signature-symbols csignature))))

(defgeneric delta-transitions (rules aut-u))

(defmethod delta-transitions ((rules rules) (aut-u abstract-automaton))
  (add-deltatransitions-for-constants aut-u)
  (do* ((states (make-empty-ordered-state-container))
	(newstates (sym-table-cright-handsides *global-sym-table*)
	  (container-difference (sym-table-cright-handsides *global-sym-table*) states))
	(new
	  (adapt-deltatransitions rules states newstates aut-u)
	  (adapt-deltatransitions rules states newstates aut-u)))
      ((and (container-empty-p newstates) (not new)))
;;      (format *error-output* "*newstates*: ~A ~%" newstates)
    (container-nunion states newstates)))

(defgeneric final-det-states (dstates finalstates))
(defmethod final-det-states ((dstates ordered-state-container) (finalstates ordered-state-container))
  (container-remove-if
   (lambda (dstate)
     (container-empty-p
      (container-intersection (gstate-container (in-state dstate)) finalstates)))
   dstates))

(defgeneric nlsaturation (rules automaton))
(defmethod nlsaturation ((rules rules) (automaton table-automaton))
  (let ((*u-states* (get-states automaton))
	(signature (signature automaton)))
    (with-new-transitions signature
      (delta-transitions rules automaton)
      (make-table-automaton
       (make-table-transitions-from-global-sym-table)
       :name (format nil "aut-c-toyama~A~A"
		     (if (in-signature-extra signature)
			 "-@" "")
		     (if (in-signature *bullet-symbol* signature)
			 "-o" ""))
       :finalstates (final-det-states (states-from-states-table *automaton-states-table*)
				      (get-finalstates automaton))))))

(defgeneric det-saturation-automaton (seqsys aut-u &key minimize))
(defmethod det-saturation-automaton
    ((sys seqsys) (aut-u table-automaton) &key (minimize nil))
  (let ((signature (signature aut-u))
	(aut-c (nreduce-automaton (nlsaturation (get-approx-rules sys) aut-u))))
    (rename-object
     aut-c
     (format nil "aut-c-toyama-~A~A~A~A"
	     (name sys)
	     (if (in-signature-extra signature)
		 "@" "")
	     (get-approx-name (seqsys-approx sys))
	     (if (in-signature *bullet-symbol* signature)
		 "-o" "")))
    (when minimize
	(nminimize-automaton aut-c))
    aut-c))
