;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :termwrite
  (:use :common-lisp ::general :input-output
   :names :object :container :symbols :terms :trs :state :termauto)
  (:export
   #:trs
   #:seqsys
   #:get-lhs
   #:get-extra
   #:seqsys-merge-signature
   #:seqsys-aut-nf
   #:seqsys-aut-nf-extra
   #:seqsys-aut-nf-bullet-extra
   #:seqsys-aut-nf-bullet
   #:seqsys-aut-u
   #:seqsys-aut-u-t
   #:seqsys-aut-b
   #:seqsys-aut-c
   #:seqsys-aut-c-t
   #:seqsys-aut-d
   #:seqsys-aut-d-ndet
   #:seqsys-aut-d-det
   #:seqsys-aut-d-extra
   #:seqsys-aut-d-extra-ndet
   #:seqsys-aut-d-extra-det
   #:seqsys-normal-form
   #:*approximation*
   #:get-approx-rules
   #:seqsys-approx
   #:seqsys-signature
   #:make-seqsys
   #:make-seqsys-from-trs
   #:seqsys-change-approx
   #:with-left-linearity
   #:with-non-variable-left-handsides
   #:make-u-automaton
   #:make-c-automaton
   #:get-approx-name
   #:nv-approximation
   #:strong-approximation
   #:growing-approximation
   #:linear-growing-approximation
   #:needed-redex-positions
   #:outermost-redex-positions
   #:seqsys-inverse
   #:inverse-rules
   #:growing-terminating
   #:inverse-growing-terminating
   #:salinier
   #:name))
