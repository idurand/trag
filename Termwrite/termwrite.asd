;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Termwrite.
(in-package :asdf-user)

(defsystem :termwrite
  :description "Termwrite: Term rewriting and Automata"
  :name "termwrite"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:object input-output :container :terms :termauto :trs)
  :serial t
  :components
  ((:file "package")
   (:file "context")
   (:file "needed-redex")
   (:file "index-point")
   (:file "index-tree")
   (:file "salinier")
   (:file "forward-branching")
   (:file "dstate")
   (:file "gsaturation")
   (:file "nlsaturation")
   (:file "gd-automate")
   (:file "det-dautomate")
   (:file "dautomate")
   (:file "approximation")
   (:file "integration")
   (:file "termination")
   )
  :serial t)

(pushnew :termwrite *features*)
