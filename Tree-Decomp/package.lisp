;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :tree-decomp
  (:use :common-lisp :general :tree :decomp :graph :terms :clique-width :container)
  ;;:general :tree :input-output :vbits :color :object :symbols :container
;;   :terms :clique-width :graph)
  (:export
   #:cwd-twd-decomposition
   #:twd-decomposition
   #:super-twd-decomposition
   ))

