;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Tree-Decomp.
(in-package :asdf-user)

(defsystem :tree-decomp
  :description "Tree-Decomp: library of graph tree-decompositions"
  :name "tree-decomp"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:tree :decomp)
  :serial t
  :components ((:file "package")
	       (:file "twd-decomposition")
	       (:file "tree-decomposition-to-cwd-term")))

(pushnew :tree-decomp *features*)
