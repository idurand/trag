;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :tree-decomp)

(defclass super-bag (bag)
  ((bag-up :accessor bag-up :initform nil)
   (bag-ft* :accessor bag-ft* :initform nil)))

(defmethod print-object :after ((bag super-bag) stream)
  (when (bag-up bag) (format stream ":~A" (bag-up bag)))
  (when (bag-ft* bag) (format stream ":~A" (bag-ft* bag))))

(defun make-super-bag (bag)
  (make-instance 'super-bag :bag-node (bag-node bag)
			    :bag-nodes (bag-nodes bag)))

(defun root-bag (bags)
  (find-if (lambda (bag) (endp (bag-nodes bag))) bags))

(defun bags-filter (bags nodes)
  (remove-if-not
   (lambda (bag) (member (bag-node bag) nodes))
   bags))

(defgeneric node-up (node edges graph)
  (:method ((node integer) (edges list) (graph graph))
    (sort
     (copy-list
      (intersection
       (edges-ancestors node edges)
       (nodes-num (node-neighbours node graph))
       :test #'=))
     #'<)))

(defgeneric compute-ups (tree-decomposition graph)     
  (:method ((tree-decomposition tree-decomposition) (graph graph))
    (loop
      with edges = (td-tree tree-decomposition)
      with bags = (td-bags tree-decomposition)
      for bag in bags
      do (setf (bag-up bag)
	       (node-up (bag-node bag) edges graph)))))

(defgeneric node-ft* (node bags edges)
  (:method ((node integer) (bags list) (edges list))
    (let ((ft (reduce
	       #'union
	       (mapcar (lambda (descendant)
			 (intersection 
			  (bag-up (bag-from-node descendant bags))
			  (edges-ancestors node edges)))
		       (edges-descendants node edges))
	       :initial-value '())))
      (sort (copy-list (remove node ft)) #'<))))

(defgeneric compute-fts (tree-decomposition graph)		      
  (:method ((td tree-decomposition) (graph graph))
    (loop
      with edges = (td-tree td)
      with bags = (td-bags td)
      for bag in bags
      do (setf (bag-ft* bag) (node-ft* (bag-node bag) bags edges)))))

(defun make-super-td (td graph)
  (let ((super-td
	  (make-tree-decomposition
	   (mapcar #'make-super-bag (td-bags td))
	   (td-tree td))))
    (compute-fts super-td graph)
    (compute-ups super-td graph)
    super-td))

(defgeneric h-graph (node edges graph)
  (:method ((node integer) (edges list) (graph graph))
    (graph-induced-subgraph 
     (edges-descendants node edges)
     graph)))

(defvar *new-port*)
(defvar *dead-port*)
(setq *new-port* 1)
(setq *dead-port* 0)

(defun new-port-p (port) (= *new-port* port))

(defvar *up-port-table*)

(defun initial-up-port-table ()
  (let ((h (make-hash-table :test #'equal)))
    (setf (gethash '* h) *new-port*)
    (setf (gethash '() h) *dead-port*)
    h))

(defun remove-up-port (up port)
  (assert (eql (gethash up *up-port-table*) port))
  (remhash up *up-port-table*)
  ;;  (format *trace-output* "~A~%" (list-pairs *up-port-table*))
  )

(defun add-up-port (up port)
  (assert (not (gethash up *up-port-table*)))
  (setf (gethash up *up-port-table*) port))

(defun reserved-ports ()
  (make-ports (list-values *up-port-table*)))

(defun up-to-port (up)
  (let ((port (gethash up *up-port-table*)))
    (unless port
      (setq port (next-port (reserved-ports)))
      (add-up-port up (next-port (reserved-ports))))
    port))

(defun port-to-up (port)
  (maphash
   (lambda (up p)
     (when (= port p)
       (return-from port-to-up up)))
   *up-port-table*))

(defgeneric ren-ops (root-bag cwd-term ports))
(defmethod ren-ops ((root-bag bag) (cwd-term term) (ports ports))
  (let ((root (bag-node root-bag)))
    (when (singleton-container-p ports)
      (let* ((port (container-car ports))
    	     (up (port-to-up port)))
    	(when (equal (remove root up)  (bag-up root-bag))
    	  (setq cwd-term (cwd-term-ren *new-port* port cwd-term))
    	  (remove-up-port (port-to-up port) port)
    	  (return-from ren-ops cwd-term))))
    (container-map
     (lambda (port)
       (unless (new-port-p port)
	 (let ((up (port-to-up port)))
	   (when (member root up :test #'=)
	     (let ((new-up (remove root up)))
	       (let ((new-port (up-to-port new-up)))
		 (setq cwd-term (cwd-term-ren port new-port cwd-term)))
	       (remove-up-port up port))))))
     ports))
  cwd-term)

(defgeneric add-ops (root cwd-terms ports))
(defmethod add-ops ((root integer) (cwd-term term) (ports ports))
  (container-map
   (lambda (port)
     (when (member root (port-to-up port) :test #'=)
       (setq cwd-term (cwd-term-unoriented-add *new-port* port cwd-term))))
   ports)
  cwd-term)

(defgeneric ndtct-rec (root-bag bags edges)
  (:documentation "takes a normal tree-decomposition with up and ft set; returns a cwd-term"))

(defmethod ndtct-rec ((root-bag bag) (bags list) (edges list))
  ;;  (format *trace-output* "~A~%" (list-pairs *up-port-table*))
  (let* ((up (bag-up root-bag))
	 (root (bag-node root-bag))
	 (sons (edges-sons root edges)))
    (if (endp sons) ; leaf
	(set-term-eti
	 (cwd-term-vertex (up-to-port up)) root)
	(let* ((cwd-terms
		 (mapcar
		  (lambda (son)
		    (let ((descendant-nodes (edges-descendants son edges)))
		      (ndtct-rec
		       (bag-from-node son bags)
		       (bags-filter bags descendant-nodes)
		       (edges-filter edges descendant-nodes)
		       )))
		  sons))
	       (ports (graph-ports-of cwd-terms))
	       (cwd-term
		 (cwd-term-multi-oplus-list
		  (cons
		   (set-term-eti (cwd-term-vertex *new-port*) root)
		   cwd-terms))))
	  (setq cwd-term (add-ops root cwd-term ports))
	  (setq cwd-term (ren-ops root-bag cwd-term ports))
	  (setq cwd-term (cwd-term-ren *new-port* (up-to-port up) cwd-term))
	  ;;  (format *trace-output* "~A~%" (list-pairs *up-port-table*))
	  cwd-term
	  ))))

(defgeneric normal-decomposition-to-cwd-term (tree-decomposition graph)
  (:documentation "takes a normal tree-decomposition; returns a cwd-term")
  (:method ((td tree-decomposition) (graph graph))
    (setq td (make-super-td td graph))
    (let* ((*up-port-table* (initial-up-port-table))
	   (edges (td-tree td))
	   (bags (td-bags td))
	   (root-bag (root-bag bags)))
      (assert (endp (bag-nodes root-bag)))
      (values
       (ndtct-rec root-bag bags edges)
       (list-pairs *up-port-table*)))))

(defun test-recycle-ports (term)
  (let ((nt (recycle-ports term)))
    (assert (decomp::cwd-term-equivalent-p term nt))
    nt))

(defgeneric cwd-twd-basic-decomposition (graph)
  (:documentation "Courcelle's method")
  (:method ((g graph))
    (let ((td (twd-decomposition g)))
      (normal-decomposition-to-cwd-term td g))))

(defgeneric cwd-twd-decomposition (graph)
  (:documentation "Courcelle's method")
  (:method ((g graph))
    (remove-top-ren (recycle-ports (cwd-twd-basic-decomposition g)))))

(defgeneric test-cwd-twd-decomposition (graph)
  (:method ((g graph))
    (let ((term (recycle-ports (cwd-twd-decomposition g))))
      (assert (cwd-term-equivalent-to-graph-p term g))
      (values (clique-width term) term))))

(defun bio-graph-name (n)
  (format nil "/Users/idurand/TRAG-PROJECT/FAGES/DIMACS/BIOMD0000000~3,'0D.col" n))

(defun bio-graph (n) (load-dimacs-graph (bio-graph-name n)))

(defun twd-bio (n) (graph-twd (bio-graph n)))

(defgeneric super-twd-decomposition (graph &optional strategy-fun)
  (:method ((graph graph) &optional (strategy-fun #'graph-degree-min-node ))
    (make-super-td
     (tree-decomposition graph strategy-fun)
     graph)))
