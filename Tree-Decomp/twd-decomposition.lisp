;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :tree-decomp)

(defgeneric bag-ft* (bag)
  (:documentation "le f_t* de Courcelle"))

(defclass bag ()
  ((bag-node :reader bag-node :type integer :initarg :bag-node)
   (bag-nodes :reader bag-nodes :type list :initarg :bag-nodes)))

(defmethod print-object ((bag bag) stream)
  (format stream "~A:~A" (bag-node bag) (bag-nodes bag)))


(defgeneric make-bag (node nodes))
(defmethod make-bag ((node integer) (nodes list))
  (make-instance 'bag :bag-node node :bag-nodes nodes))

(defclass tree-decomposition ()
  ((td-bags :accessor td-bags :initarg :td-bags :type list)
   (td-tree :accessor td-tree :initarg :td-tree)))

(defgeneric nodes-from-bag (bag))
(defmethod nodes-from-bag ((bag bag))
  (adjoin (bag-node bag) (bag-nodes bag)))

(defgeneric bag-from-node (node bags))
(defmethod bag-from-node ((node integer) (bags list))
  (find-if 
   (lambda (bag)
     (= (bag-node bag) node))
   bags))

(defun nodes-from-bags (bags)
  (reduce #'union (mapcar #'nodes-from-bag bags)))

(defun make-tree-decomposition (td-bags td-tree)
  (make-instance 'tree-decomposition :td-bags td-bags :td-tree td-tree))

(defmethod print-object ((td tree-decomposition) stream)
  (format stream "[~A~% ~A] ~% twd:~A" (reverse (td-tree td)) (reverse (td-bags td))
	  (tree-decomposition-twd td)))

(defgeneric nvertex-elimination (graph num-or-node))
(defmethod nvertex-elimination ((graph graph) (num-node integer))
  (nvertex-elimination graph (find-node-with-num num-node graph)))

(defmethod nvertex-elimination ((graph graph) (node node))
  (let ((neighbours (nodes-contents (node-neighbours node graph))))
    (graph-delete-node node graph)
    (loop
       for n1 in neighbours
       do (loop 
	     for n2 in neighbours
	     when (< (num n1) (num n2))
	     do (graph-create-arc-nodes graph n1 n2 nil nil)))
    graph))

(defgeneric vertex-remove-effect (graph node))
(defmethod vertex-remove-effect ((graph graph) (node node))
  (let ((neighbours (nodes-contents (node-neighbours node graph))))
    (with-node-deleted node graph
      (loop
	for n1 in neighbours
	sum (loop 
	      for n2 in neighbours
	      count (and (< (num n1) (num n2))
			 (nodes-member n2 (node-out-nodes n1))))))))

(defgeneric vertex-remove-strategy (graph))
(defmethod vertex-remove-strategy ((graph graph))
  (compute-graph-nodes-weight graph (graph-nodes graph) #'vertex-remove-effect)
  (node-with-min-weight (graph-nodes graph)))

(defgeneric compute-graph-nodes-weight (graph nodes effect-fun))
(defmethod compute-graph-nodes-weight ((graph graph) (nodes nodes) effect-fun)
  (nodes-map
   (lambda (node)
     (change-weight node (funcall effect-fun graph node)))
   nodes))

(defgeneric mixed-strategy (graph))
(defmethod mixed-strategy ((graph graph))
  (let ((nodes (graph-degree-min-nodes graph)))
    (compute-graph-nodes-weight graph nodes #'vertex-remove-effect)
    (prog1
	(node-with-max-weight nodes)
      (set-nodes-weight graph))))

(defgeneric vertex-elimination (graph num-or-node))
(defmethod vertex-elimination ((graph graph) (num-node integer))
  (vertex-elimination graph (find-node-with-num num-node graph)))

(defmethod vertex-elimination ((graph graph) (node node))
  (nvertex-elimination (graph-copy graph) node))

(defgeneric nelimination-game (graph elimination-ordering))
(defmethod nelimination-game ((graph graph) (elimination-ordering list))
  (loop
     with g = (graph-copy graph)
     for num-node in elimination-ordering
     do (nvertex-elimination graph g num-node)
     finally (return graph)))

(defmethod nelimination-game :before ((graph graph) elimination-ordering)
  (assert (not (oriented-p graph))))

(defgeneric elimination-game (graph elimination-ordering))
(defmethod elimination-game ((graph graph) (elimination-ordering list))
  (nelimination-game (graph-copy graph) elimination-ordering))

(defgeneric tree-decomposition-twd (tree-decomposition))
(defmethod tree-decomposition-twd ((tree-decomposition tree-decomposition))
  (reduce #'max 
	  (mapcar (lambda (b) (length (bag-nodes b)))
		  (td-bags tree-decomposition))))

(defgeneric n-tree-decomposition (graph node-weight-fun)
(:method ((graph graph) (node-weight-fun function))
  (let* ((nodes (graph-nodes graph))
	 (nums (nodes-num nodes)))
    (if (= (nodes-size nodes) 1)
	(let* ((node (nodes-car nodes))
	       (num (num node)))
;;	  (format t "~A " (num node))
	  (make-tree-decomposition 
	   (list (make-bag num (remove num nums)))
	   '()))
	(let* ((node (funcall node-weight-fun graph))
	       (num (num node))
	       (neighbours (node-neighbours node graph))
	       (neighbour-nums (nodes-num neighbours))
	       (extremity-bag (make-bag num neighbour-nums)))
;;	  (format t "~A " (num node))
;;	  (graph-show graph)
	  (nvertex-elimination graph node)
	  (let* ((td (n-tree-decomposition graph node-weight-fun))
		 (td-bags (td-bags td))
		 (origin-bag 
		  (find-if 
		   (lambda (b) (subsetp neighbour-nums (nodes-from-bag b)))
		   td-bags)))
	    (make-tree-decomposition
	     (cons extremity-bag td-bags)
	     (cons (list (bag-node origin-bag) (bag-node extremity-bag)) ;; arc in T
		   (td-tree td)))))))))

(defgeneric tree-decomposition (graph node-choice-fun)
  (:method ((graph graph) (node-choice-fun function))
    (n-tree-decomposition (graph-copy graph) node-choice-fun)))

;; si sommets ordonnes selon elimination ordering: #'graph-num-min-node 
(defgeneric twd-decomposition (graph &optional strategy-fun)
  (:method ((graph graph) &optional (strategy-fun #'graph-degree-min-node ))
    (tree-decomposition graph strategy-fun)))

(defgeneric graph-twd (graph &optional strategy-fun)
  (:method ((graph graph) &optional (strategy-fun #'graph-degree-min-node))
    (if (graph-connected-p graph)
	(tree-decomposition-twd (twd-decomposition graph strategy-fun))
	(reduce #'max (mapcar #'graph-twd (graph-component-decomposition graph))))))

;; (1 11 2 7 10 9 6 4 5 3 8)
