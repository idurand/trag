(in-package :tree)

(defun max-tree (a b)
  (let ((d (- (tree-nb-nodes a) (tree-nb-nodes b))))
    (cond 
      ((plusp d) a)
      ((minusp d) b)
      (t ;; (zerop d)
       (let ((dsa (degree-sequence-of a))
	     (dsb (degree-sequence-of b)))
	 (cond
	   ((degree-sequence< dsa dsb) b)
	   ((degree-sequence< dsb dsa) a)
	   (t ;; (dsa = dsb)
	    (if (utree< a b)
		b
		a))))))))

(defgeneric edge-weight-difference (edge tree))

(defmethod edge-weight-difference ((edge list) (tree tree))
  (multiple-value-bind (tree1 tree2) (tree-edge-cut edge tree)
    (let* ((a (max-tree tree1 tree2))
	   (b (if (eq a tree1) tree2 tree1))
	   (d (- (* 2 (tree-nb-nodes a)) (tree-nb-nodes tree))))
      (values a b d))))

(defmethod edge-weight-difference ((edge list) (sexpr list))
  (edge-weight-difference edge (tree-from-sexpr sexpr)))

(defgeneric min-weight-difference (tree))
(defmethod min-weight-difference ((tree tree))
  (let ((edges (tree-edges tree)))
    (multiple-value-bind (a b d) (edge-weight-difference (pop edges) tree)
      (loop
	while edges
	do (multiple-value-bind (aa bb dd) (edge-weight-difference (pop edges) tree)
	     (if (< dd d)
		 (progn (setq d dd) (setq a aa) (setq b bb))
		 (when (= dd d) (setq a (max-tree a aa)) (when (eq a aa) (setq b bb)))))
	finally (return (values a b d))))))

(defgeneric balance-tree (tree))

(defmethod balance-tree ((sexpr list))
  (balance-tree (tree-from-sexpr sexpr)))

(defmethod balance-tree ((tree tree))
  (multiple-value-bind (a b d) (min-weight-difference tree)
    (declare (ignore d))
    (let ((subtrees (tree-subtrees a))
	  (root (tree-root a)))
      (let ((new-tree
	      (make-tree
	       root
	       (append subtrees (list b)))))
	(assert (= (tree-nb-nodes tree) (tree-nb-nodes new-tree)))
	new-tree))))
      

(defmethod min-weight-difference ((sexpr list))
  (min-weight-difference (tree-from-sexpr sexpr)))

(defgeneric find-root (sexpr))
(defmethod find-root ((sexpr list))
  (find-root (tree-from-sexpr sexpr)))

(defmethod find-root ((tree tree))
  (multiple-value-bind (d a)
      (min-weight-difference tree)
    (declare (ignore d))
    (tree-root a)))

(defun ds-equivalent-p (tree1 tree2)
  (and
   (equal (degree-sequence-of tree1) (degree-sequence-of tree2))
   (equal (dual-degree-sequence-of tree1) (dual-degree-sequence-of tree2))
   (not (root-change-equivalent-p tree1 tree2))))
