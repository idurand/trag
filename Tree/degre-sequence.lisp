;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; operations on tree whose nodes are integers and
;;; are represented by a list of edges (each edge being a list of 2 nodes

(in-package :tree)

(defun distinct-random (n m)
  (loop
    with l = '()
    while (plusp n)
    do (let ((i (random m)))
	 (unless (member i l)
	   (push i l)
	   (decf n)))
    finally (return l)))

(defun random-degrees (n)
  (case n
    (0 '())
    (1 (list 0))
    (2 (list 1 1))
    (t
      (let* ((degree (1+ (random (1- n))))
	     (rds (random-degrees (1- n)))
	     (len (length rds))
	     (distinct (distinct-random degree len)))
	(loop
	  for i in distinct 
	  do (incf (nth i rds)))
	(push degree rds)
	rds))))

(defun degrees-to-degree-sequence (degrees)
  (loop
    with ds = '()
    for degree in degrees
    do (let ((pair (rassoc degree ds)))
	 (if pair
	     (incf (car pair))
	     (push (cons 1 degree) ds)))
    finally (return (sort ds #'< :key #'cdr))))

(defun random-degree-sequence (n)
  (degrees-to-degree-sequence (random-degrees n)))
					
;; ((n1 . d1) ... (nk . dk))
(defun ds-nb-nodes (ds)
  (reduce #'+ ds :key #'car))

(defun ds-nb-nodes-of-degree (degree ds)
  (car (find-if (lambda (p) (= (cdr p) degree)) ds)))

(defgeneric degree-sequence-of (tree))

(defmethod degree-sequence-of ((tree tree))
  (let ((ds '()))
    (loop
      for node in (tree-nodes tree)
      do (let* ((d (tree-node-degree node tree))
		(p (rassoc d ds)))
	   (if p
	       (incf (car p))
	       (push (cons 1 d) ds))))
    (sort ds #'< :key #'cdr)))

(defgeneric dual-degree-sequence-of (tree))

(defmethod dual-degree-sequence-of ((tree tree))
  (let ((dds '()))
    (loop
      for edge in (tree-edges tree)
      do (multiple-value-bind (left right) (tree-edge-cut edge tree)
	   (let* ((pl (tree-nb-nodes left))
		  (pr (tree-nb-nodes right))
		  (ppi (rassoc pl dds))
		  (ppj (rassoc pr dds)))
	   (if ppi
	       (incf (car ppi))
	       (push (cons 1 pl) dds))
	   (if ppj
	       (incf (car ppj))
	       (push (cons 1 pr) dds)))))
    (sort dds #'< :key #'cdr)))

(defun degree-sequence= (ds1 ds2)
  (equal ds1 ds2))

(defun cons-rev (cons)
  (cons (cdr cons) (car cons)))

(defun ds-inv (ds)
  (loop
    with ids = '()
    for pair in ds
    do (push (cons-rev pair) ids)
       finally (return ids)))

(defun idegree-sequence< (ids1 ids2)
  (and ids1
       ids2
       (or
	(cons< (car ids1) (car ids2))
	(and
	 (equal (car ids1) (car ids2))
	 (idegree-sequence< (cdr ids1) (cdr ids2))))))

(defun degree-sequence< (ds1 ds2)
  (idegree-sequence< (ds-inv ds1) (ds-inv ds2)))

(defun degree-sequence<= (ds1 ds2)
  (or (equal ds1 ds2)
      (idegree-sequence< (ds-inv ds1) (ds-inv ds2))))
