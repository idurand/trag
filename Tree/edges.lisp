;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; operations on tree edges whose nodes are integers and
;;; are represented by a list of edges (each edge being a list of 2 nodes
;; rooted so oriented

(in-package :tree)

(defgeneric edges-nodes (edges))
(defmethod edges-nodes ((edges list))
  (sort
   (remove-duplicates (flatten edges)) #'<))

(defgeneric edges-nb-nodes (edges))
(defmethod edges-nb-nodes ((edges list))
  (length (edges-nodes edges)))

(defgeneric edges-father (node edges))
(defmethod edges-father ((node integer) (edges list))
  (assert edges)
  (car
   (find-if 
    (lambda (edge)
      (= (second edge) node))
    edges)))

(defgeneric edges-ancestors (node edges &optional strict))
(defmethod edges-ancestors ((node integer) (edges list) &optional (strict nil))
  (let* ((father (edges-father node edges))
	 (ancestors (if father
			(edges-ancestors father edges)
			'())))
    (unless strict
      (push node ancestors))
    ancestors))

(defun sort-edges (edges)
  (sort (copy-list edges) #'pair<))

(defun edges-origin-p (node edge)
  (= (first edge) node))

(defgeneric edges-root (edges))
(defmethod edges-root ((edges list))
  (let ((nodes (edges-nodes edges)))
    (find-if-not 
     (lambda (node)
       (edges-father node edges))
     nodes)))

(defgeneric edges-leaves (edges))
(defmethod edges-leaves ((edges list))
  (remove-if
   (lambda (node)
     (edges-sons node edges))
   (edges-nodes edges)))

(defgeneric edges-internal-nodes (edges))
(defmethod edges-internal-nodes ((edges list))
  (remove-if-not
   (lambda (node)
     (edges-sons node edges))
   (edges-nodes edges)))

(defgeneric edges-sons (node edges))
(defmethod edges-sons ((node integer) (edges list))
  (loop
    for edge in edges
    when (edges-origin-p node edge)
      collect (second edge)))

(defgeneric edges-nb-sons (node edges))
(defmethod edges-nb-sons ((node integer) (edges list))
  (length (edges-sons node edges)))

(defgeneric edges-descendants (node edges &optional strict))
(defmethod edges-descendants ((node integer) (edges list) &optional (strict nil))
  (let* ((sons (edges-sons node edges))
	 (descendants 
	   (reduce #'union
		   (mapcar 
		    (lambda (son)
		      (edges-descendants son edges))
		    sons)
		   :initial-value '())))
    (unless strict
      (push node descendants))
    descendants))

(defgeneric edges-filter (edges nodes))
(defmethod edges-filter ((edges list) (nodes list))
  (remove-if-not
   (lambda (edge)
     (subsetp edge nodes))
   edges))

(defgeneric node-edges (node edges))
(defmethod node-edges ((node integer) (edges list))
  (edges-filter edges (edges-descendants node edges)))
