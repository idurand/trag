;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Tree.
(in-package :asdf-user)

(defsystem :tree
  :description "Tree: useful stuff"
  :name "tree"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:graph)
  :serial t
  :components
   (
    (:file "package")
    (:file "edges")
    (:file "tree")
    (:file "degre-sequence")
    (:file "balanced-tree")
    ))

(pushnew :tree *features*)
