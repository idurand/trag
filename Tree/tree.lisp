;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; operations on tree unordered/ordered trees whose nodes are integers

(in-package :tree)

(defgeneric tree-nodes (tree))
(defgeneric tree-edges (tree))
(defgeneric tree-root (tree))
(defgeneric tree-subtrees (tree))

(defclass tree ()
  ((root :initarg :tree-root :initform 0 :accessor tree-root :type integer)
   (subtrees :initarg :subtrees :initform '() :accessor tree-subtrees))
  (:documentation "rooted tree"))

(defgeneric tree-to-sexpr (tree))
(defmethod tree-to-sexpr ((tree tree))
  (let ((subtrees (tree-subtrees tree))
	(root (tree-root tree)))
    (if (endp subtrees)
	root
	(cons root
	      (mapcar #'tree-to-sexpr subtrees)))))

(defgeneric tree-labels (tree))
(defmethod tree-labels ((tree tree))
  (let ((labs '()))
    (labels ((aux (tr)
	       (push (tree-root tr) labs)
	       (let ((subtrees (tree-subtrees tr)))
		 (mapc #'aux subtrees))))
      (aux tree)
      (nreverse labs))))

(defmethod print-object ((tree tree) stream)
  (format stream "[~A]" (tree-to-sexpr tree)))

(defun make-tree (root subtrees)
  ;; (assert (notany (lambda (subtree) (member root (tree-labels subtree))) subtrees))
  ;; (let ((tree-labels (reduce #'append (mapcar #'tree-labels subtrees))))
  ;;   (assert (= (length tree-labels) (length (remove-duplicates tree-labels))))
  (make-instance 'tree :tree-root root :subtrees subtrees))

(defun connect-trees (ta tb)
  (make-tree (tree-root ta) (append (tree-subtrees ta) (list tb))))

(defun make-leaf-tree (n)
  (make-tree n '()))

(defgeneric tree-leaf-p (tree))
(defmethod tree-leaf-p ((tree tree))
  (endp (tree-subtrees tree)))
      
(defgeneric node-tree (node tree))
(defmethod node-tree ((node integer) (tree tree))
  (and
   tree
   (if (= node (tree-root tree))
       tree
       (loop
	 for st in (tree-subtrees tree)
	 do (let ((r (node-tree node st)))
	      (when r
		(return-from node-tree r)))))))

(defgeneric tree-random-leaf (tree))
(defmethod tree-random-leaf ((tree tree))
  (if (tree-leaf-p tree)
      (tree-root tree)
      (tree-random-leaf (list-random-element (tree-subtrees tree)))))

(defgeneric tree-edge-cut (edge tree))
(defmethod tree-edge-cut ((edge list) (tree tree))
  (assert (member edge (tree-edges tree) :test #'equal))
  (let* ((origin (first edge))
	 (extremity (second edge))
	 (otree (root-change tree origin))
	 (subtrees (tree-subtrees otree))
	 (subtree (find-if (lambda (st) (= (tree-root st) extremity)) subtrees)))
    (values subtree (make-tree origin (remove subtree subtrees)))))
	  
(defgeneric tree-node-cut (node tree))
(defmethod tree-node-cut ((node integer) (tree tree))
  (and
   tree
   (let ((root (tree-root tree)))
     (cond
       ((= node root) nil)
       ((tree-leaf-p tree) (make-leaf-tree root))
       (t (make-tree (tree-root tree) (remove nil (mapcar (lambda (st) (tree-node-cut node st)) (tree-subtrees tree)))))))))

(defgeneric tree-adjoin (tree subtree))
(defmethod tree-adjoin ((tree tree) (subtree tree))
  (let ((root (tree-root tree)))
    (if (= root (tree-root subtree))
	subtree
	(make-tree root (mapcar (lambda (st) (tree-adjoin st subtree)) (tree-subtrees tree))))))

(defgeneric tree-nodes (tree))
(defmethod tree-nodes ((tree tree))
  (let ((nodes '()))
    (labels ((aux (tr)
	       (push (tree-root tr) nodes)
	       (mapc #'aux (tree-subtrees tr))))
    (aux tree)
    nodes)))

(defgeneric tree-nodes (tree))
(defmethod tree-edges ((tree tree))
  (let ((edges '()))
    (labels ((aux (father tr)
	       (let ((f (tree-root tr)))
		 (push (list father f) edges)
		 (mapc (lambda (st) (aux f st)) (tree-subtrees tr)))))
      (mapc (lambda (st) (aux (tree-root tree) st)) (tree-subtrees tree)))
    (nreverse edges)))
      
(defgeneric nadd-tree-edge (edge tree))
(defmethod nadd-tree-edge (edge (tree tree))
  (let ((nodes (tree-nodes tree)))
    (assert (member (first edge) nodes))
    (assert (not (member (second edge) nodes)))
    (let ((subtree (node-tree (first edge) tree)))
      (push (make-leaf-tree (second edge)) (tree-subtrees subtree)))
    tree))

(defun sexpr-to-tree (sexpr)
  (if (atom sexpr)
      (make-leaf-tree sexpr)
      (make-tree (car sexpr) (mapcar #'tree-from-sexpr (cdr sexpr)))))

(defun tree-from-sexpr (sexpr) (sexpr-to-tree sexpr))

(defun nadd-tree-edges (edges tree)
  (loop for edge in edges
	do (nadd-tree-edge edge tree))
  tree)

(defgeneric tree-leaves (tree))
(defmethod tree-leaves ((tree tree))
  (remove-if
   (lambda (node)
     (tree-sons node tree))
   (tree-nodes tree)))

(defmethod tree-leaves ((sexpr list))
  (tree-leaves (tree-from-sexpr sexpr)))

(defgeneric tree-root-degree (tree))
(defmethod tree-root-degree ((tree tree))
  (length (tree-subtrees tree)))

(defgeneric tree-nb-subtrees (tree))
(defmethod tree-nb-subtrees ((tree tree))
  (length (tree-subtrees tree)))

(defgeneric tree-root-degree (tree))
(defmethod tree-root-degree ((tree tree))
  (tree-nb-subtrees tree))

(defgeneric tree-node-nb-subtrees (node tree))
(defmethod tree-node-nb-subtrees ((node integer) (tree tree))
  (tree-nb-subtrees (node-tree node tree)))

(defgeneric tree-node-degree (node tree))
(defmethod tree-node-degree ((node integer) (tree tree))
  (let ((nb-subtrees (tree-node-nb-subtrees node tree)))
    (if (= node (tree-root tree))
	nb-subtrees
	(1+ nb-subtrees))))

(defgeneric tree-nodes-of-degree (degree tree))
(defmethod tree-nodes-of-degree ((degree integer) (tree tree))
  (remove-if
   (lambda (node)
     (/= (tree-node-degree node tree) degree))
   (tree-nodes tree)))

(defgeneric tree-internal-nodes (tree))
(defmethod tree-internal-nodes ((tree tree))
  (remove-if-not
   (lambda (node)
     (tree-sons node tree))
   (tree-nodes tree)))

(defgeneric tree-sons (node tree))
(defmethod tree-sons ((node integer) (tree tree))
  (let ((subtree (node-tree node tree)))
    (mapcar #'tree-root (tree-subtrees subtree))))

(defgeneric tree-nb-nodes (tree))
(defmethod tree-nb-nodes ((tree tree))
  (length (tree-nodes tree)))

(defgeneric tree-nb-edges (tree))
(defmethod tree-nb-edges ((tree tree))
  (length (tree-edges tree)))

(defgeneric tree-descendants (node tree &optional strict))
(defmethod tree-descendants ((node integer) (tree tree) &optional (strict nil))
  (edges-descendants node (tree-edges tree) strict))

(defgeneric tree-filter (tree nodes))
(defmethod tree-filter ((tree tree) (nodes list))
  (let ((new-edges (edges-filter (tree-edges tree) nodes)))
    (make-tree new-edges nodes)))

(defgeneric tree-father (node tree))
(defmethod tree-father ((node integer) (tree tree))
  (and
   (/= node (tree-root tree))
   (if (member node (tree-sons (tree-root tree) tree))
       (tree-root tree)
       (loop
	 for subtree in (tree-subtrees tree)
	 do (let ((father (tree-father node subtree)))
	      (when father (return-from tree-father father)))))))
		
(defgeneric root-change (tree root))
(defmethod root-change ((tree tree) (new-root integer))
  (if (= (tree-root tree) new-root)
      tree
      (let ((t1 (root-change (tree-node-cut new-root tree) (tree-father new-root tree))))
	(make-tree new-root (cons t1 (tree-subtrees (node-tree new-root tree)))))))

(defgeneric tree-isomorphic-p (tree1 tree2)
  (:documentation "ordered rooted trees"))

(defmethod tree-isomorphic-p ((tree1 tree) (tree2 tree))
  (and
   (= (tree-nb-subtrees tree1) (tree-nb-subtrees tree2))
   (every (lambda (t1 t2) (tree-isomorphic-p t1 t2))
	  (tree-subtrees tree1)
	  (tree-subtrees tree2))))

(defgeneric utree< (tree1 tree2))

(defgeneric utrees< (trees1 trees2))
(defmethod utrees< ((trees1 list) (trees2 list))
  (let ((permutations2 (permutations trees2)))
    (some
     (lambda (permutation2)
       (and
	(every #'utree<= trees1 permutation2)
	(some #'utree< trees1 permutation2)))
     permutations2)))

(defmethod utree< ((tree1 tree) (tree2 tree))
  (let ((n1 (tree-nb-nodes tree1))
	(n2 (tree-nb-nodes tree2)))
    (or (< n1 n2)
	(and
	 (= n1 n2)
	 (let ((rd1 (tree-root-degree tree1))
	       (rd2 (tree-root-degree tree2)))
	   (or (< rd1 rd2)
	       (and
		(= rd1 rd2)
		(utrees< (tree-subtrees tree1) (tree-subtrees tree2)))))))))

(defgeneric utree= (tree1 tree2) 
  (:documentation "equality of unordered rooted trees"))

(defmethod utree= ((tree1 tree) (tree2 tree))
  (and
   (= (tree-nb-subtrees tree1) (tree-nb-subtrees tree2))
   (let* ((subtrees1 (tree-subtrees tree1))
	  (subtrees2 (tree-subtrees tree2))
	  (set1 (remove-duplicates subtrees1 :test #'utree=))
	  (set2 (remove-duplicates subtrees2 :test #'utree=)))
     (and
      (subsetp set1 set2 :test #'utree=)
      (subsetp set2 set1 :test #'utree=)
      (every (lambda (st) (= (count st subtrees1 :test #'utree=) (count st subtrees2 :test #'utree=)))
	     set1)))))

(defgeneric utree<= (tree1 tree2))
(defmethod utree<= ((tree1 tree) (tree2 tree))
  (or (utree= tree1 tree2)
      (utree< tree1 tree2)))

(defgeneric utrees<= (trees1 trees2))
(defmethod utrees<= ((trees1 list) (trees2 list))
  (let ((permutations2 (permutations trees2)))
    (some
     (lambda (permutation2)
       (every #'utree<= trees1 permutation2))
     permutations2)))


(defgeneric tree-equivalent-p (tree1 tree2))
(defmethod tree-equivalent-p ((tree1 tree) (tree2 tree))
  (tree-isomorphic-p (order-tree tree1) (order-tree tree2)))

(defgeneric tree-root-degree (tree))
(defmethod tree-root-degree ((tree tree))
  (length (tree-subtrees tree)))

(defgeneric tree< (tree1 tree2))
(defmethod tree< ((tree1 tree) (tree2 tree))
  (let ((n1 (tree-nb-nodes tree1))
	(n2 (tree-nb-nodes tree2)))
    (or (< n1 n2)
	(and (= n1 n2)
	     (let ((rd1 (tree-root-degree tree1))
		   (rd2 (tree-root-degree tree2)))
	       (< rd1 rd2))))))

(defgeneric order-tree (tree))
(defmethod order-tree ((tree tree))
  (let ((root (tree-root tree))
	(subtrees (mapcar (lambda (subtree) (order-tree subtree)) (tree-subtrees tree))))
    (setq subtrees (sort subtrees #'tree<))
    (make-tree root subtrees)))

(defgeneric normalize-tree-numbers (tree &optional n)
  (:documentation "numbered tree and last number used"))

(defmethod normalize-tree-numbers ((tree tree) &optional (n 1))
  (if (tree-leaf-p tree)
      (values (make-leaf-tree n) n)
      (loop
	with subtrees = '()
	with lastn = n
	for subtree in (tree-subtrees tree)
	do (multiple-value-bind (nt ne) (normalize-tree-numbers subtree (1+ lastn))
	     (push nt subtrees)
	     (setq lastn ne))
	finally (return (values (make-tree n (nreverse subtrees)) lastn)))))

(defgeneric standardize-tree (tree))
(defmethod standardize-tree ((tree tree))
  (normalize-tree-numbers (order-tree tree)))

(defgeneric root-change-equivalent-p (tree1 tree2))
(defmethod root-change-equivalent-p ((tree1 tree) (tree2 tree))
  (setq tree1 (standardize-tree tree1))
  (setq tree2 (standardize-tree tree2))
  (let* ((nb-subtrees1 (tree-nb-subtrees tree1))
	 (roots2 (tree-nodes-of-degree nb-subtrees1 tree2)))
    (some
     (lambda (root2)
       (tree-equivalent-p
	tree1
	(standardize-tree (root-change tree2 root2))))
     roots2)))

(defgeneric root-change-tree-enumerator (tree))
(defmethod root-change-tree-enumerator ((tree tree))
  (let* ((nb-subtrees (tree-root-degree tree))
	 (roots2 (tree-nodes-of-degree nb-subtrees tree)))
    (make-funcall-enumerator
     (lambda (root2)
       (root-change tree root2))
     (make-list-enumerator roots2))))

(defmethod root-change-tree-enumerator ((sexpr list))
  (root-change-tree-enumerator (tree-from-sexpr sexpr)))

(defparameter *tree1* nil)
(defparameter *tree2* nil)

(defparameter *caterpillar1* nil)
(defparameter *caterpillar2* nil)

(defun random-tree (n)
  (assert (plusp n))
  (if (= n 1)
      (make-leaf-tree n)
      (let* ((rt (random-tree (1- n)))
	     (nodes (tree-nodes rt))
	     (node (list-random-element nodes)))
	(nadd-tree-edge (list node n) rt)
	rt)))

(defun standard-random-tree (n)
  (standardize-tree (random-tree n)))

(defun random-tree-degree-sequence (n)
  (degree-sequence-of (random-tree n)))

(defun ds-to-random-caterpillar (ds)
  (setq ds (copy-alist ds))
  (let* ((tree (make-leaf-tree 1))
	 (n 1)
	 (pair (list-random-element ds)))
    (loop
       with node = n
       repeat (cdr pair)
       do (nadd-tree-edge (list node (incf n)) tree))
;;    (print (list 'pair pair ds))
    (decf (car pair))
    (when (zerop (car pair))
      (setq ds (delete pair ds)))
;;    (print (list 'pair pair ds))
    (loop
       while ds
       do (let ((node n)
		(pair (list-random-element ds)))
;;	    (tree-show tree)
	    (loop
;;	       do (print (list 'pair pair ds))
	       repeat (1- (cdr pair))
	       do (nadd-tree-edge (list node (incf n)) tree))
	    (decf (car pair))
	    (when (zerop (car pair))
	      (setq ds (delete pair ds)))))
    tree))

(defun ds-to-standard-random-caterpillar (ds)
  (standardize-tree (ds-to-random-caterpillar ds)))

(defun ds-to-random-tree (ds)
  (setq ds (copy-alist ds))
  (let* ((tree (make-leaf-tree 1))
	 (n 1)
	 (pair (list-random-element ds)))
    (loop
      repeat (cdr pair)
      do (nadd-tree-edge (list 1 (incf n)) tree))
    (decf (car pair))
    (when (zerop (car pair))
      (setq ds (delete pair ds)))
    (loop
      while ds
      do (let* ((leaf (tree-random-leaf tree))
		(pair (list-random-element ds))
		(dj (cdr pair)))
	   (loop
	     repeat (1- dj)
	     do (nadd-tree-edge (list leaf (incf n)) tree))
	   (decf (car pair))
	   (when (zerop (car pair))
	     (setq ds (delete pair ds)))))
    tree))
	   
(defun ds-to-standard-random-tree (ds)
  (standardize-tree (ds-to-random-tree ds)))
    
(defun trees-with-same-ds-not-root-equivalent (ds)
  ;; may loop
  (loop
    with tree1 = (ds-to-standard-random-tree ds)
    with tree2 = (ds-to-standard-random-tree ds)
    while (root-change-equivalent-p tree1 tree2)
    do (setq tree2 (ds-to-standard-random-tree ds))
    finally
       (progn
	 (assert (not (root-change-equivalent-p tree1 tree2)))
	 (setq *tree1* tree1)
	 (setq *tree2* tree2)
	 (return (values tree1 tree2 ds)))))

(defun trees-not-root-equivalent (n)
  (assert (> n 3))
  (loop
     for ds = (random-tree-degree-sequence n) then
       (random-tree-degree-sequence n)
     for tree1 = (ds-to-standard-random-tree ds) then
       (ds-to-standard-random-tree ds)
     for tree2 = (ds-to-standard-random-tree ds) then
       (ds-to-standard-random-tree ds)
    while (root-change-equivalent-p tree1 tree2)
;;    do (format *error-output* ".")
    finally
       (progn
	 (assert (not (root-change-equivalent-p tree1 tree2)))
	 (setq *tree1* tree1)
	 (setq *tree2* tree2)
	 (return (values tree1 tree2 ds)))))

(defun caterpillars-not-root-equivalent (n)
  (assert (> n 3))
  (loop
     for ds = (random-tree-degree-sequence n) then
       (random-tree-degree-sequence n)
     for caterpillar1 = (ds-to-standard-random-caterpillar ds) then
       (ds-to-standard-random-caterpillar ds)
     for caterpillar2 = (ds-to-standard-random-caterpillar ds) then
       (ds-to-standard-random-caterpillar ds)
    while (root-change-equivalent-p caterpillar1 caterpillar2)
;;    do (format *error-output* ".")
    finally
       (progn
	 (assert (not (root-change-equivalent-p caterpillar1 caterpillar2)))
	 (setq *caterpillar1* caterpillar1)
	 (setq *caterpillar2* caterpillar2)
	 (return (values caterpillar1 caterpillar2 ds)))))

(defgeneric tree-to-graph (tree))
(defmethod tree-to-graph ((tree tree))
  (graph-from-earcs-and-enodes 
   (tree-edges tree)
   (list (tree-root tree)) :oriented t))

(defgeneric graph-to-tree (graph))
(defmethod graph-to-tree ((graph graph))
  (assert (not (graph-cycle-p graph)))
  (let ((tree (make-leaf-tree 1)))
    (labels ((aux (origin)
	       (container::container-map
		(lambda (extremity)
		  (nadd-tree-edge (list (num origin) (num extremity)) tree)
		  (aux extremity))
		(node-out-nodes origin))))
      (aux (find-node-with-num 1 graph)))
    tree))

(defgeneric tree-show (tree))
(defmethod tree-show ((tree tree))
  (graph-show (tree-to-graph tree)))

(defgeneric subtrees-average-size (tree))
(defmethod subtrees-average-size ((tree tree))
  (let ((subtrees (tree-subtrees tree)))
    (/ (reduce #'+ (mapcar #'tree-nb-nodes subtrees)) (length subtrees))))
