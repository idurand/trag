;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :trs)

(defun read-rules (stream)
  (handler-case
      (parse-rules stream)
    (parser-error (c)
      (prog1 nil (format *error-output* "read-rules parse error ~A: ~A~%" (parser-token c) (parser-message c))))
    (error ()
      (prog1 nil
	(format *error-output* "Unable to read rules")))))

(defun read-trs (stream)
  (handler-case 
      (parse-trs stream)
    (parser-error (c)
      (prog1 nil
	(format *error-output* "read-trs parse error ~A: ~A~%"
		(parser-token c) (parser-message c))))
    (error (err)
      (prog1 nil
	(format *error-output* "~A" err)))))
