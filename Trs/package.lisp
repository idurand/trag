;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :trs
  (:use :common-lisp :general :object :container :symbols :terms)
  (:export
   #:left-handside
   #:right-handside
   #:left-linear
   #:growing
   #:inverse-rule
   #:rule
   #:make-rule
   #:arbitrary-rule-p
   #:collapsing-rule-p
   #:equivalent-rules-p
   #:rules
   #:rules-list
   #:make-rules
   #:right-handsides
   #:left-handsides
   #:rules-closed-p
   #:collapsing-p
   #:constructor-p
   #:nb-rules
   #:right-linear-rules-p
   #:included-rules-p
   #:equiv-rules-p
   #:inverse-rules
   #:reduction
   #:*dsymbols*
   #:subscheme-p
   #:dsymbols
   #:trs
   #:trss
   #:make-trss
   #:rules-of
   #:make-trs
   #:trss-table
   #:forward-branching-def
   #:salinier
   #:scan-rules
   #:parse-trs
   #:read-rules
   ))
