;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :trs)
;; <rule> ::= <term> -> <term>
;; <trs> ::= TRS <name> <rule>*
;; <vars> ::= Vars <var>*
;; <var> ::= <memorized-name>

;; <trs-vars> ::= empty | [ <var> <suite-var> ]
;; <suite-var> ::= <empty> | , <var>

(defun scan-rule (stream)
  (let ((lh (scan-term stream)))
    (parse-arrow stream)
    (let ((rh (scan-term stream)))
      (make-rule lh rh))))

(defun parse-rule (stream)
  (lexmem stream)
  (scan-rule stream))

(defun scan-rules (stream)
  (do ((rules '()))
      ((not (symbol-or-string-p)) (make-rules (nreverse rules)))
    (push (scan-rule stream) rules)))

(defun parse-rules (stream)
  (lexmem stream)
  (scan-rules stream))

(defun scan-trs (stream)
  (if (symbol-or-string-p)
      (make-trs (name (lexmem stream))
		(scan-rules stream)
		(make-signature (all-symbols)))
      (signal-parser-error 'name-expected-error "name expected")))

(defun parse-trs (stream)
  (lexmem stream) 
  (scan-trs stream))
