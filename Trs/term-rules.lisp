;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :trs)

(defgeneric instance-of-any-rule (term rules)
  (:documentation "true if TERM is an instance of a rule in RULES"))

(defmethod instance-of-any-rule (term (rules rules))
  (instance-of-any-rule term (rules-list rules)))

(defmethod instance-of-any-rule (term (rules list))
  (find-if (lambda (rule) (instance term (left-handside rule))) rules))

(defgeneric reduction (term position rules)
  (:documentation
   "performs a reduction step on TERM at the given POSITION using RULES"))

(defmethod reduction (term position (rules rules))
  (let* ((subterm (term-at-position term position))
	 (rule (instance-of-any-rule
		subterm
		rules)))
    (multiple-value-bind (res subst)
	(unify-substi (left-handside rule) subterm '())
      (values
       (replace-term-at-position term position (apply-substitution (right-handside rule) subst))
       res))))

