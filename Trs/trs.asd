;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Trs.

(in-package :asdf-user)

(defsystem :trs
  :description "Trs: Term rewriting and Automata"
  :name "trs"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:input-output :terms)
  :serial t
  :components
  ((:file "package")
   (:file "rule")
   (:file "rules")
   (:file "term-rules")
   (:file "trs")
   (:file "parser")
   (:file "input")
   ))

(pushnew :trs *features*)
