;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :trs)

(defgeneric rules-of (trs)
  (:documentation "access to the rules of a trs"))

(defgeneric (setf rules-of) (trs newrules)
  (:documentation "modifies the rules of the trs"))

(defgeneric trss-table (trss)
  (:documentation "the hash-table containing the trss keyed by name"))

(defclass trss ()
  ((trss-table :initform (make-hash-table :test #'equal)
		    :accessor trss-table)))

(defun make-trss ()
  (make-instance 'trss))

(defclass trs (named-mixin signature-mixin)
  ((rules :initform nil
	  :initarg :rules
	  :accessor rules-of
	  )))

(defmethod symbols-from ((trs trs))
  (symbols-from (rules-of trs)))

(defun make-trs (name rules signature)
  (make-instance 'trs :name name :rules rules :signature signature))

(defmethod show :after ((trs trs) &optional (stream t))
  (format stream "~%~A"  (rules-of trs)))

(defmethod print-object :after ((trs trs) stream)
  (format stream "~%"))

(defmethod size ((trs trs))
  (size (rules-of trs)))

(defmethod growing ((trs trs))
  (growing (rules-of trs)))
