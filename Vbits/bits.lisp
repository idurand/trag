;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :vbits)

(defun toggle-bit (b) (if (zerop b) 1 0))

(defun onep (n) (= 1 n))

(defun bit-union-p (b bits)
  (or
   (and (zerop b) (every #'zerop bits))
   (and (onep b) (some #'onep bits))))

(defun bit-intersection-p (b bits)
  (or
   (and (zerop b) (some #'zerop bits))
   (and (= 1 b) (every #'onep bits))))

(defun union-p (bits) (some #'onep bits))

(defun intersection-p (bits) (every #'onep bits))

(defun union-bin-p (b1 b2)
  (union-p (list b1 b2)))

(defun intersection-bin-p (b1 b2)
  (intersection-p (list b1 b2)))

(defun bit-union-bin-p (b b1 b2)
  (bit-union-p b (list b1 b2)))

(defun bit-intersection-bin-p (b b1 b2)
  (bit-intersection-p b (list b1 b2)))
