;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :vbits
  (:use :common-lisp :general)
  (:export
   #:vbits-mixin-p
   #:union-bin-p
   #:intersection-bin-p
   #:lvbits
   #:vbits-mixin
   #:vbits
   #:zero-vbits
   #:randomize-vbits
   #:random-vbits
   #:random-partition-vbits
   #:make-vbits
   #:vbits-to-vbit
   #:vbits-to-string
   #:x1-to-xj-ifun
   #:xj-to-cxj-ifun
   #:xi-eq-cxj-ifun
   #:x1-to-bool-xi-ifun
   #:y1-y2-to-xj1-xj2-ifun
   #:y1-y2-to-xj-xj1-bool-xj2-ifun
   #:x1-to-xj-fun
   #:xj-to-cxj-fun
   #:xi-eq-cxj-fun
   #:x1-to-bool-xi-fun
   #:y1-y2-to-xj1-xj2-fun
   #:y1-y2-to-xj-xj1-bool-xj2-fun
   #:vbits-projection-fun
   #:vbits-projection-ifun
   #:complementary-positions
   #:x1-to-xj1-bool-xj2-fun
   #:x1-to-xj1-bool-xj2-ifun
   #:set-iota
   ))
