;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :vbits)

(defun x1-to-xj-fun (m j) 
  "[b] -> { [...b_j...b_m] with b_j = b"
  (assert (<= 1 j m))
  (lambda (vbits)
    (let ((lvbits (mapcar #'make-vbits (lvbits m))))
      (mapcar #'make-vbits
	      (remove-if-not
	       (lambda (nvbits)
		 (=  (aref vbits 0) (aref nvbits (1- j))))
	       lvbits)))))

(defun xj-to-cxj-fun (m j)
  "[...b_j...] -> { [...b'_j...] } b'_j = not b_j"  
  (assert (<= 1 j m))
  (lambda (vbits)
    (let ((nvbits (copy-vbits vbits))
	  (bit (aref vbits (1- j))))
      (setf (aref nvbits (1- j)) (toggle-bit bit))
      (list nvbits))))

(defun xi-eq-cxj-fun (m i j)
  "[...b_i...b_j...] -> {[...b_i...b_{j-1} b_{j+1}...]} if b_j = not b_i else {}"
  (assert (<= 1 j m))
  (assert (/= i j))
  (lambda (vbits)
    (unless (= (aref vbits (1- i)) (aref vbits (1- j)))
      (loop with nvbits = (zero-vbits (1- m))
	    for k from 0 below m
	    with l = -1
	    unless (= (1- j) k)
	      do (setf (aref nvbits (incf l)) (aref vbits k))
	    finally (return (list nvbits))))))

(defun x1-to-bool-xi-fun (m bool)
  "[b] -> [b_1...b_m] with Bool b_i = b"
  (lambda (vbits)
    (let ((lvbits (mapcar #'make-vbits (lvbits m))))
      (mapcar #'make-vbits
	      (remove-if-not
	       (lambda (nvbits)
		 (let ((b (aref vbits 0)))
		   (case bool
		     (union (bit-union-p b nvbits))
		     (intersection (bit-intersection-p b nvbits)))))
	       lvbits)))))

(defun x1-to-xj1-bool-xj2-fun (m j1 j2 bool)
  "[b] -> { [...b_j1...b_j2...b_m] with b_j1 bool bj2 = b}"
  (lambda (vbits)
    (let ((lvbits (mapcar #'make-vbits (lvbits m))))
      (mapcar #'make-vbits
	      (remove-if-not
	       (lambda (nvbits)
		 (let ((b (aref vbits 0))
		       (b1 (aref nvbits (1- j1)))
		       (b2 (aref nvbits (1- j2))))
		   (if (eq 'union bool)
		       (bit-union-bin-p b b1 b2)
		       (bit-intersection-bin-p b b1 b2))))
	       lvbits)))))

(defun y1-y2-to-xj1-xj2-fun-body (vbits m j1 j2)
  (let ((lvbits (mapcar #'make-vbits (lvbits m))))
    (remove-if-not
     (lambda (nvbits)
       (let ((b1 (aref nvbits (1- j1)))
	     (b2 (aref nvbits (1- j2))))
	 (and
	  (=  (aref vbits 0) b1)
	  (=  (aref vbits 1) b2))))
     lvbits)))

(defun y1-y2-to-xj1-xj2-fun (m j1 j2)
  (assert (<= 1 j1 m))
  (assert (<= 1 j2 m))
  "[b_1b_2] -> { [...b_j1...b_j2...b_m] with b_j1 = b_1 and b_j2 = b_2}"
  (lambda (vbits)
    (y1-y2-to-xj1-xj2-fun-body vbits m j1 j2)))

(defun y1-y2-to-xj-xj1-bool-xj2-fun (m j j1 j2 bool)
  "[b_1b_2] -> { [...b_j...b_j1...b_j2...b_m] with b_j=b_1 and b_j1 Bool b_j2 = b_2 }"
  (assert (<= 1 j m))
  (assert (<= 1 j1 m))
  (assert (<= 1 j2 m))
  (lambda (vbits)
    (let ((lvbits (mapcar #'make-vbits (lvbits m))))
      (remove-if-not
       (lambda (nvbits)
	 (let ((b (aref vbits 1))
	       (b1 (aref nvbits (1- j1)))
	       (b2 (aref nvbits (1- j2))))
	   (and
	    (=  (aref vbits 0) (aref nvbits (1- j)))
	    (case bool
	      (union  (bit-union-bin-p b b1 b2))
	      (intersection (bit-intersection-bin-p b b1 b2))))))
       lvbits))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun complementary-positions (positions m)
  (loop for i in (iota m 1)
	unless (member i positions :test #'=)
	  collect i))

(defgeneric vbits-projection-body (vbits positions))
(defmethod vbits-projection-body (vbits (positions list))
  (when positions
    (assert (<= 1 (reduce #'max positions) (length vbits)))
    (loop with nbits = ()
	  for i from 0 below (length vbits)
	  when (member (1+ i) positions :test #'=)
	    do (push (aref vbits i) nbits)
	  finally (return (make-vbits (nreverse nbits))))))

(defun vbits-projection-fun (positions)
  (lambda (vbits)
    (list (vbits-projection-body vbits positions))))
