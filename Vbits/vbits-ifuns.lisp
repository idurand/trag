;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :vbits)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun bool-int (bits bool-op)
  (if (case bool-op
	(union (union-p bits))
	(intersection (intersection-p bits))) 1 0))

(defun bool-bin-int (b1 b2 bool)
  (bool-int (list b1 b2) bool))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun x1-to-xj-ifun-body (vbits m j)
  (assert (= m (length vbits)))
  (list (make-vbits (list (aref vbits (1- j))))))

(defun x1-to-xj-ifun (m j)
  "[...b_j...b_m]-> { [b] } with b = b_j "
  (assert (<= 1 j m))
  (lambda (vbits)
    (x1-to-xj-ifun-body vbits m j)))

(defun xj-to-cxj-ifun (m j)
  " [...b'_j...] -> { [...b_j...] } b_j = not b'_j"  
  (assert (<= 1 j m))
  (lambda (vbits)
    (let ((nvbits (copy-vbits vbits))
	  (bit (aref vbits (1- j))))
      (setf (aref nvbits (1- j)) (toggle-bit bit))
      (list nvbits))))

(defun xi-eq-cxj-ifun (m i j)
  " [...b_i...] -> { [...b_i...b_j...] } b_j = not b_i"
  (assert (<= 1 j m))
  (assert (/= i j))
  (lambda (vbits)
    (loop with nvbits = (zero-vbits m)
	  with l = -1
	  for k from 0 below m
	  do (setf (aref nvbits k)
		   (if (= (1- j) k)
		       (toggle-bit (aref vbits (1- i)))
		       (aref vbits (incf l))))
	  finally (return (list nvbits)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun x1-to-bool-xi-ifun (m bool)
  "[b_1...b_m] -> { [b] } with Bool b_i = b"
  (lambda (vbits)
    (assert (= (length vbits) m))
    (list
     (make-vbits
      (list
       (bool-int vbits bool))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun x1-to-xj1-bool-xj2-ifun (m j1 j2 bool)
  "[...b_j1...b_j2...b_m] -> { [b] } with b = b_j1 bool bj2}"
  (lambda (vbits)
    (assert (= (length vbits) m))
    (let ((b1 (aref vbits (1- j1)))
	  (b2 (aref vbits (1- j2))))
      (list
       (make-vbits
	(list
	 (bool-bin-int b1 b2 bool)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun y1-y2-to-xj1-xj2-ifun-body (vbits j1 j2)
  (let ((b1 (aref vbits (1- j1)))
	(b2 (aref vbits (1- j2))))
    (list
     (make-vbits (list b1 b2)))))

(defun y1-y2-to-xj1-xj2-ifun (m j1 j2)
  (assert (<= 1 j1 m))
  (assert (<= 1 j2 m))
  "[...b_j1...b_j2...b_m] -> { [b_1b_2] } with b_1 = b_j1 and b_2 = b_j2"
  (lambda (vbits)
    (y1-y2-to-xj1-xj2-ifun-body vbits j1 j2)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun y1-y2-to-xj-xj1-bool-xj2-ifun (m j j1 j2 bool)
  "[...b_j...b_j1...b_j2...b_m] -> { [b_1b_2] } with b_1=b_j and b_2 = b_j1 Bool b_j2"
  (assert (<= 1 j m))
  (assert (<= 1 j1 m))
  (assert (<= 1 j2 m))
  (lambda (vbits)
    (let ((b1 (aref vbits (1- j1)))
	  (b2 (aref vbits (1- j2)))
	  (nvbits (make-vbits '(0 0))))
      (setf (aref nvbits 0) (aref vbits (1- j)))
      (setf (aref nvbits 1)
	    (bool-bin-int b1 b2 bool))
      (list nvbits))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric vbits-cylindrification-body (vbits positions)
  (:method (vbits (positions list))
    (let ((lvbits (list (zero-vbits 0))))
      (loop with j = 0
	    for i from 0 below (+ (length vbits) (length positions))
	    do (setf lvbits
		     (if (member (1+ i) positions :test #'=)
			 (append (multiply-lvbits lvbits 0)
				 (multiply-lvbits lvbits 1))
			 (prog1
			     (multiply-lvbits lvbits (aref vbits j))
			   (incf j)))))
      lvbits)))

(defgeneric vbits-projection-ifun (positions)
  (:method ((positions list))
    (lambda (vbits)
      (vbits-cylindrification-body vbits positions))))
