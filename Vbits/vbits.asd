;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Vbits.
(in-package :asdf-user)

(defsystem :vbits
  :description "vbits mixin"
  :name "vbits"
  :version "6.0"
  :license "CeCILL-C"
  :author "Irène Durand"
  :serial t
  :depends-on (:general)
  :components
  ((:file "package")
   (:file "bits")
   (:file "vbits")
   (:file "vbits-funs")
   (:file "vbits-ifuns")))

(pushnew :vbits *features*)
