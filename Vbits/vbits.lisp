;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :vbits)

(defgeneric vbits (vbits-mixin)
  (:documentation "vits of a vbits-mixin")
  (:method ((s t))
    (warn "vbits of a non vbits-mixin ~A:~A~%" s (type-of s))
    nil))

(defclass vbits-mixin ()
  ((vbits :initform nil :initarg :vbits :reader vbits)))

(defun vbits-mixin-p (object)
  (typep object 'vbits-mixin))

(defun bits-to-vbits (bits)
  "bit-vector from list of bits"
  (make-array (length bits) :initial-contents bits :element-type 'bit))

(defun vbits-to-bits (vbits)
  "bit-vector tolist of bits"
  (loop for i across vbits collect i))

(defun vbits-to-string (vbits)
  (concatenate 'string (mapcar #'digit-char (vbits-to-bits vbits))))

(defun zero-vbits (m)
  (bits-to-vbits (make-list m :initial-element 0)))

(defgeneric make-vbits (bits) 
  (:method ((bits list))
    (unless (endp bits) (bits-to-vbits bits)))
  (:method ((vbits vector))
    (copy-seq vbits)))

(defun copy-vbits (vbits) (make-vbits (vbits-to-bits vbits)))

(defgeneric randomize-vbits (bit-vector)
  (:documentation "fill bit vector with random bits; destructive")
  (:method ((vbits bit-vector))
    (let ((m (length vbits)))
      (dotimes (i m vbits)
	(setf (aref vbits i) (random 2))))))

(defun random-vbits (m) (randomize-vbits (zero-vbits m)))

(defun random-partition-vbits (m)
  (let ((vbits (zero-vbits m)))
    (setf (aref vbits (random m)) 1)
    vbits))

(defun lvbits (m)
  (cartesian-product (make-list m :initial-element '(0 1))))

(defun multiply-vbits (vbits bit)
  (make-vbits (append (coerce vbits 'list) (list bit))))

(defun multiply-lvbits (lvbits bit)
  (mapcar (lambda (vbits)
	    (multiply-vbits vbits bit))
	  lvbits))

(defun set-iota (m) (iota m 1))
