\documentclass[11pt, a4paper, titlepage]{article}

\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{a4}
\usepackage{url}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{lscape}
\usepackage{tikz}
\usepackage{amsmath,bm,times}
\usepackage{listings}

\usetikzlibrary{shapes,arrows}

\title{TITRE}
\author{Responsables : Courcelle Bruno et Durand Ir\`ene \\
  Bouvier Tom\\Kirman J\'er\^ome}

\begin{document}
  \maketitle
  \tableofcontents

  \lstset{language=C, backgroundcolor=\color{yellow!10}, frame=single}

  \newpage
  \section{Introduction}
  A l'issue de l'UE de projet de programmation, le logiciel GraphGen
  était techniquement fonctionnel, mais de nombreuses fonctionnalités
  envisagées en cours de route demeuraient encore non implémentées. En
  outre, l'interface en ligne de commande était un peu trop simpliste,
  et l'interface graphique avait un aspect austère et mal finalisé. \\

  L'objectif du stage était d'implémenter toutes les fonctionnalités
  listées comme extensions possibles à la fin du projet, ainsi que
  quelques autres suggérées par Irène Durand et Bruno Courcelle. La
  liste des tâches demandées, rédigée de façon informelle, se
  présentait ainsi :
  \begin{itemize}
  \item Création d'un bouton permettant de visualiser en un clic
    l'ensemble du terme courant
  \item Affichage de la taille et de la hauteur maximale du terme courant
  \item Mémorisation du dernier répertoire utilisé pour sauvegarder un terme
  \item Affichage des étiquettes du termes dans les sommets du graphe
  \item Ajout d'un ''marquage'' permettant de savoir quelle arête du
    graphe final est issue de quelle opération sur le terme
  \item Affichage plus clair des probabilités régissant les
    proportions entre les opérations
  \item Possibilité de choisir un ensemble de propriétés devant être
    satisfaites et de lancer la génération en boucle jusqu'à obtention
    un graphe ayant les propriétés souhaitées
  \item Ajout d'un bouton ''Stop'' pour arrêter la génération si elle se prolonge trop
  \item Implémentation de la génération à base de grammaires
  \item Support du multi-threading
  \end{itemize} ~\\

  S'y ajoutait l'addition de fonctions permettant de visualiser un
  graphe en un clic, d'isoler un sous-terme et son sous-graphe associé
  (comme prévu dans le projet original), et l'amélioration des
  interfaces utilisateur (graphique ou en ligne de commande).

  \section{Génération par grammaire}

  La génération en accord avec une grammaire sur les termes est une
  possibilité que nous avions envisagée dès que nous avons commencé à
  travailler sur GraphGen. Néanmoins, en raison de sa relative
  complexité et de son aspect secondaire, nous l'avions laissée de
  côté pour nous concentrer sur le coeur du logiciel. Au final, le
  manque de temps et surtout d'organisation au sein du groupe nous
  avaient contraints à abandonner l'idée. Le code source contenait
  encore de nombreuses fonctions 'creuses' et un module grammar, mais
  ceux-ci étaient commentés car largement incomplets. \\

  Durant le stage, nous avons du reprendre à zéro l'essentiel de ce
  code (écrit ''à la dernière minute''), et repenser le fonctionnement
  du module. En conservant la syntaxe de fichier décrivant une
  grammaire et l'idée de pondérer chaque production possible par une
  probabilité (afin de favoriser, pour un symbole non-terminal, le
  choix de certaines productions par rapport à d'autres), nous avons
  réécrit les prototypes du module grammar et construit un parser avec
  lex/yacc qui créait une grammaire à partir d'un fichier. \\

  Les structures de données représentant une grammaire se présentent
  comme suit : \\

  \begin{lstlisting}
  struct grammar {
    unsigned nbrules;
    rule** rules;
    unsigned axiom;
  };

  struct rule {
    char* name;
    unsigned nbprods;
    prod** prods;
  };

  struct prod {
    term* term;
    unsigned nbrules;
    unsigned* rules_id;
    unsigned minsize;
    unsigned maxsize;
    double w;
  };
  \end{lstlisting} ~

  Sans grande surprise, une grammaire (grammar) est munie d'un axiome
  et se compose d'un ensemble de symboles non-terminaux (rules) nommés,
  auxquels correspondent pour chacun une ou plusieurs productions (prods). \\

  S'agissant d'une grammaire sur les termes, chaque production est
  associée directement à un terme plutôt qu'à une chaîne de
  caractères. Nous avons dû ajouter pour l'occasion une nouvelle
  'opération' d'arité 0 à notre structure de terme, pour représenter un
  symbole non-terminal (donnant lieu à l'application d'une règle) dans
  le terme associé à une production. Cette opération est bien entendue
  destinée à être remplacée par un sous-terme correspondant à ce symbole
  non-terminal, et ne doit en aucun cas apparaître une fois le terme
  finalisé (ce qui est garanti par différentes assertions dans
  term.c). \\

  %% [ILLUSTRATION 1]

  Comme il était prévu de pondérer la probabilité de choisir telle ou
  telle production pour un une règle donnée, une variable de type
  double conserve le poids relatif du symbole, dont est issue sa
  probabilité d'être choisi. \\

  En outre, lors de la création de la grammaire, une fonction parcourt
  récursivement celle-ci afin de déterminer les longueurs minimum et
  maximum du terme généré par chaque production, et de mettre en cache
  ces informations. Lors de la création du terme à partir de la grammaire,
  ces informations permettent dans une certaine mesure à l'algorithme de
  ''savoir'' quelle branche choisir pour atteindre la longueur désirée par
  l'utilisateur.

  \section{Génération selon propriétés}
  Un autre objectif était de disposer d'un moyen simple de générer des termes
  ''en boucle'' jusqu'à obtention d'un terme dont le graphe associé répondrait à
  certaines propriétés spécifiées par l'utilisateur. \\

  C'est l'objet de la fonction $term\_with\_properties$, qui génère des termes
  aléatoires selon les caractéristiques spécifiées, puis calcule le graphe
  associé et ses propriétés avant de les comparer avec celles attendues,
  représentées par le type $p\_wanted$. \\

  \begin{lstlisting}
  typedef struct wanted p_wanted;
  struct wanted
  {
    bool yes;
    char* value;
  };
  \end{lstlisting} ~

  La structure publique $wanted$ représente très simplement une attente
  vis-a-vis d'une propriété ; le booléen $yes$ est vrai si et seulement si
  l'utilisateur exige de cette propriété qu'elle aie la valeur $value$ dans le
  graphe obtenu. Un tableau de p\_wanted (supposé de longueur
  $properties\_count()$) contient ainsi la description complète des propriétés
  désirées pour le graphe. \\

  Une fonction $properties\_check$ prenant en argument les propriétés d'un
  graphe et le tableau de $p\_wanted$ décrit plus haut retourne vrai lorsque le
  graphe répond à la demande de l'utilisateur. La génération est alors stoppée,
  et le terme valable renvoyé au reste du programme. Le cas échéant, le terme
  est effacé, et un nouveau terme est généré jusqu'à réussite ou annulation. \\

  Etant donné qu'il est possible qu'aucun terme ne réponde aux propriétés
  demandées, ou qu'un terme correct ne représente qu'une probabilité infime
  d'être généré par le programme en un temps ''raisonnable'', il fallait
  également prévoir la possibilité pour l'utilisateur d'interrompre la
  génération si elle ne donne pas de résultats. A cet effet, l'interface
  graphique ouvre désormais une fenêtre popup munie d'un bouton ''Abort'' dans
  un thread à part. L'activation de ce bouton (ou la fermeture de la fenêtre)
  provoque le basculement d'un booléen $stop$, dont l'état est vérifié à chaque
  nouveau terme généré par la fonction $term\_with\_properties$ ; s'il est vrai,
  la génération de nouveaux termes s'interrompt et aucun résultat n'est
  retourné.\\

  \section{Nettoyage du terme}
  Le nettoyage du terme avait déjà été implémenté lors de la première
  version. Cette implémentation supprimait seulement les $add\_x\_y$ et
  $ren\_x\_y$ quand $x$ ou $y$ n'étaient pas des labels valides du
  sous-terme. Elle ne traitait cepandant pas les cas s'apparentants
  aux termes : $add\_a\_b(add\_a\_b(T))$ ou $ren\_a\_b(ren\_b\_a(ren\_a\_b(T)))$
  et autres variantes. La nouvelle implémentation permet un nettoyage
  beaucoup plus poussé du terme mais aussi plus couteux en temps.
  Le nettoyage s'oppère donc en plusieurs phases.

  \subsection{Nettoyage de la racine}
  La première phase consiste à supprimer tous les $RENAME$
  superflus en partant de la racine du terme jusqu'à rencontrer un
  $ADD$ dans chaque arborescence. 

  \subsection{Nettoyage du reste du terme}
  La deuxième phase est un regroupement de plusieurs
  algorithmes opérant soit sur les $ADD$ soit sur les $RENAMES$.
  La fonction de nettoyage parcours le terme récursivement dans un
  ordre postfixe.

  \subsubsection{Le cas du $LABEL$}
  Quand le parcours se trouve sur un $LABEL$ il
  créé un graphe muni d'un sommet étiqueté par le $LABEL$ en
  question. Ce graphe va permettre de déterminer les labels utilisés
  et ainsi savoir si un $RENAME$ ou un $ADD$ est utile.

  \subsubsection{Le cas du $RENAME$}
  Le traitement des $RENAME$ est celui qui est le plus
  couteux en temps et en mémoire. En effet pour supprimer les
  rennomages inutiles (en particulier ceux qui ''bouclent'') une
  solution consiste à supprimer toutes ces opération entre chaque
  opéra-tions $ADD$ et $OPLUS$ et de suvegarder dans un tableau les
  trasformations effectuées. La création des opérations de renommages
  sera effectuée quand le parcours s'arrêtera sur une opération de
  type $ADD$ ou de type $OPLUS$. La création de ces opérations
  consiste à trouver la bonne combinaison pour disposer les
  $RENAME$. Le nombre de combinaison est potentiellement
  exponentiel. Dans le pire des cas le nombre de combinaisons est de
  l'ordre de $2^{{cwd} \over {2}}$, où $cwd$ est la largueur de
  clique définie lors de la génération. Heureusement ce genre de
  situations apparaissent relativement peu souvent. Et la largeur de
  clique restant relativement faible permet de ne pas trop aggraver
  la situation. Il se peut cependant qu'aucune combinaison ne
  convienne, nous somme, dans ce cas, en présence d'un ''cycle''
  (par exemple $a$ est renommé en $b$ et $b$ est renommé en $a$). On
  remédie à ce problème en supprimant des labels finaux les effets
  du ''cycle'' puis on relance l'algorithme de création des $RENAME$
  et une fois ce dernier terminés en choisis un ''pivot'' (un label
  ayant été renommé et qui va servir de variable d'échange) parmis
  ceux disponible et on ajoute les opérations de renommage dans
  l'ordre adéquate. \\
  
  \begin{lstlisting}
 unsigned pivot;
 for (unsigned i = 0; i < cwd; i++)
   if (is_good_pos(i, cwd, old_ren, old_ren)) {
     pivot = i;
     break;
   }

 term *tmp = term_create(RENAME, t->subterm[side],
                         cycle[0], pivot);
 tmp = term_create(RENAME, tmp, cycle[length - 1],
                   cycle[0]);
 for (unsigned i = length - 2; i > 0; i--)
   tmp = term_create(RENAME, tmp, cycle[i],
                     cycle[i + 1]);
 t->subterm[side] = term_create(RENAME, tmp, pivot,
                                cycle[1]);
  \end{lstlisting} ~\\

  La présence du pivot est inévitable car si l'on veut renommer $a$
  en $b$ il faut préalablement renommer $b$ en un label différent de
  $a$ (on peut facilement étendre ce raisonement pour des ''cycles''
  de longueur supèrieur à 2). \\
  Un autre cas de figure peut être représenté. Il sagit du cas où
  plusieur labels se renomment en un même label. On peut donc
  regrouper ces opérations et les raitées différement (et éviter
  de créer des cycle). \\
  La procédure est la suivante : On veut renommer $a$ et $b$ en $c$.
  \begin{itemize}
  \item Procédure naïve : on créer l'opération
    $ren\_a\_c(ren\_b\_c(T))$. Or si l'on veut renommer $c$ en $a$
    cela créer un ''cycle'' et fausse le résultat quelque-soit la
    postion de l'opération $ren\_c\_a(T)$.
  \item La solution est la suivante : utiliser le fait que $a$ et
    $b$ se renomment tous les deux en $c$ pour libérer un
    ''pivot''; dans ce cas le pivot sera $a$. On obtient donc
    l'opération $ren\_b\_c(ren_c\_a(ren\_a\_b(T)))$; on commence
    par renommer $a$ en $b$ pour permettre de renommer $c$ en $a$
    puis on finit en renommant $b$ (et donc l'ancien $a$) en $c$.
  \end{itemize}
  Le fait de trouver un $ren\_x\_y(T)$ permet d'autoriser les
  opérations $add\_y\_z(T)$ et $add\_z\_y(T)$ (interdites lors du
  traitement des $ADD$, détaillé dans le paragrphe \ref{ADD-case}). \\
  Les $RENAME$ supprimés lors de la première implémentation sont
  naturellements supprimés dans cette version aussi. \\
  Le graphe est lui aussi mis-à-jours.

  \subsubsection{Le cas du $ADD$}
  \label{ADD-case}
  Le nettoyage des opérations d'ajouts d'arêtes est, quand à
  lui, bien moins couteux. Il suffit en effet d'interdire certains
  $ADD$ en fonction de ceux que l'on a déja croisés. La rencontre
  d'une oppération $add\_x\_y(T)$ interdit à une opération traitant
  les mêmes labels d'apparaitre en amont du terme. Les opérations
  permettant d'autoriser de nouveau un $ADD$ sont les $RENAMES$ et
  les $OPLUS$. L'algorithme utilise une matrice carrée (rendant
  l'allocation de la mémoire uadratique en fonction de la largeur
  de clique) pour stocker les autorisations alors qu'elle pourrait
  être triangulaire (les opérations $add\_y\_x$ quand $y > x$ étant
  interdites). Mais cela compliquerait trop le code et
  n'économiserait pas suffisament de mémmoire pour être utile. En
  effet la largeur de clique est faible par rapport à la taille du
  terme et la place mémoire occupée par la matrice d'autorisations
  est presque insignifiante en comparaison de celle occupée par le
  terme (voir par le graphe quand celui-ci est demandé).

  \subsubsection{Le cas du $OPLUS$}
  Lorsqu'il croise un $OPLUS$, le parcous postfixe traite le
  sous-terme gauche puis le sous-terme droit; il fusionne ensuite
  les deux graphes en ne gardant qu'un seul sommet dans chaque label
  pour des raisons d'économies de mémoire. Une fois qu'un sous-terme
  a été traité, on lance la génération des $RENAME$; on effectue
  aussi une remise à zéro des opérations d'ajouts (toutes les
  opérations sont de nouveau utilisables).

  \section{Multithreading}

  Afin d'accélérer la génération, il était prévu de multithreader le logiciel
  pour exploiter pleinement les capacités des machines disposant de plusieurs
  coeurs. Notamment, nous avions envisagé de séparer la génération des
  sous-termes (celle-ci pouvant se faire de manière totalement indépendante), et
  de ne connecter les sous-termes obtenus entre eux qu'à la fin. \\

  Cependant, après étude, il s'avère que la génération du terme n'est pas la
  plus grande consommatrice en termes de temps ; nous nous sommes donc
  concentrés sur la génération du graphe associé, en particulier la création
  d'arêtes, de complexité quadratique par rapport à la taille du terme. \\

  Ainsi, lorsqu'une création d'arêtes intervient, on fait appel à une
  $GThreadPool$, structure chargée de répartir de nombreuses itérations d'une
  tâche particulière entre un ensemble de threads créés à l'avance, en les
  réutilisant aussitôt leur travail fini (évitant ainsi de créér et de détruire
  constamment de nouveaux threads). \\

  Chaque thread est chargé de connecter un sommet muni de l'étiquette de départ
  avec l'ensemble des sommets de l'étiquette d'arrivée (et une autre série de
  threads effectue l'opération inverse dans le cas d'un graphe non-orienté). On
  réduit ainsi grandement le temps requis pour générer le graphe. \\

  Une autre opération potentiellement très consommatrice de temps est la
  génération de termes dont les graphes associés doivent répondre à un ensemble
  de propriétés ; on génère alors séparément autant de termes que de threads
  dans la $GThreadPool$ (nombre défini à la compilation), qui chacun génère un
  terme puis calcule le graphe associé et ses propriétés pour les comparer au
  résultat attendu. \\

  Afin d'éviter d'encombrer la pile avec un trop grand nombre de threads, les
  graphes sont alors générés en utilisant de 'petites' $GThreadPool$ lors de la
  création d'arêtes. En effet, le nombre total de threads serait sinon égal au
  nombre de threads générant des termes en parallèle multiplié par le nombre de
  threads utilisé par chacun d'eux pour le calcul du graphe associé. \\

  Dans l'hypothèse où plusieurs threads trouveraient un terme correspondant aux
  critères attendus quasi-simultanément (ce qui est possible, en particulier si
  les propriétés demandées sont déjà garanties par les caractéristiques du terme
  ou pour des termes de taille réduite), un verrou d'exclusion mutuelle garantit
  qu'un seul d'entre eux (le premier arrivé) écrive ses résultats à l'adresse
  prévue. \\

  \section{Nettoyage du code}

  L'ensemble du code écrit durant le projet, bien que globalement satisfaisant,
  était souvent inégal (selon l'expérience du langage C de l'auteur des lignes),
  suivait parfois des logiques différentes (passage systématique par référence
  ou emploi de variables globales), et, dans l'ensemble, pouvait gagner à être
  nettoyé, uniformisé et commenté. \\

  \subsection{Interface graphique}
  %GUI->Callbacks/Popups/GtkTreeIter (?)

  \subsection{Module properties}
  A l'issue du projet, le module $properties$ était de loin le plus inégal. Il
  contenait un très grand nombre de variables globales, les tests des propriétés
  étaient codés ''en dur'', et on voyait des traces de la greffe de plusieurs
  fonctions sur le module. Cela s'explique par le fait que ce module avait été
  entamé dès le début du projet, et que nous n'avions envisagé l'ajout de
  propriétés supplémentaires qu'en cours de route, une fois le parcours en
  profondeur implémenté. En outre, de nombreuses propriétés devaient être codées
  'en dur' pour être testées simultanément en un seul parcours en profondeur.
  L'ajout de fonctions permettant d'ajouter de nouvelle propriétés en passant en
  argument la fonction de test a augmenté la souplesse du programme, mais
  marquait un contraste entre les propriétés ''de base'', toutes testées par un
  parcours en profondeur, et d'éventuelles propriétés supplémentaires ajoutées
  par la suite en respectant des structures de données génériques. \\

  Il fallait donc réécrire le module en préservant tout à la fois la vitesse
  d'exécution (tests des propriétés d'origines en une seule passe) et la
  généricité (emploi d'une même structure pour décrire toutes les propriétés à
  tester). De plus, la perspective de la génération d'après un ensemble de
  propriétés et celle de l'ajout du multi-threading imposait de disposer d'un
  module ''thread-safe'', afin que ses fonctions puissent être appelées
  simultanément depuis plusieurs threads sans provoquer d'erreurs. \\

  Le compromis entre vitesse et généricité a finalement pris la forme d'une
  fonction de test commune à toutes les propriétés testées ''en une passe''.
  Celle-ci conserve un tableau statique dont elle retourne les résultats un par
  un à chaque nouvel appel, jusqu'à épuisement. Cela impose toutefois de tester
  toutes les premières propriétés d'un graphe donné sans interruption, ce que
  garantit le reste du module, qui ne peut retourner qu'un tableau contenant les
  résultats de tous les tests à la fois et protège l'exécution des premiers
  tests grâce à un verrou d'exclusion mutelle. \\

  \begin{lstlisting}
  char* test_DFS (graph* g) {
    static unsigned count = -1;
    static char** results = NULL;
    count++;
    count %= NB_PROP;
    if (count == 0) {
      g_free (results);
      results = g_malloc0 (NB_PROP * sizeof(*results));
      DFS_run (g, results);
    }
    return results[count];
  }
  \end{lstlisting} ~\\

  Les seules variables globales restantes sont un vecteur contenant toutes les
  propriétés à tester, un compteur donnant la taille de celui-ci, et le mutex
  empêchant l'exécution simultanée des premiers tests par deux threads
  différents (pour les raisons mentionnées ci-dessus). Les variables globales
  anciennement utilisées par le parcours en profondeur ont été regroupées dans
  une structure $DFS\_data$ allouée dynamiquement pour faciliter leur
  manipulation. \\

  \begin{lstlisting}
  static unsigned prop_count;
  static prop** props;

  static GMutex* pmutex;

  struct DFS_data {
    unsigned nb_vertices;
    bool connected;
    bool acyclic;
    bool colorable_2;
    unsigned d_min;
    unsigned d_max;
    unsigned connected_parts;
    bool* seen;
    bool* color;
  };
  \end{lstlisting} ~\\

  Enfin, ces modifications ont nécessité la création d'une fonction
  $init\_properties$, qui doit toujours être appelée exactement une fois avant
  utilisation du module ; c'est cette fonction qui se charge d'ajouter toutes
  les propriétés déjà codées à la liste des propriétés à tester, et d'initialier
  le mutex protégeant le codage ''en dur'' du parcours en profondeur. Elle
  permet également (avec $add\_property$) d'ajouter facilement de nouvelles
  propriétés à tester au programme sans nécessiter une étude approfondie du code
  source. \\

  \subsection{Parsers}
  Etant donné l'absence d'implémentation du module $grammar$, le parser de
  grammaires était encore a l'état embryonnaire. En le développant, nous en
  avons profité pour apporter quelques modifications au parser de termes. \\

  Les modifications les plus importantes se situent au niveau de la gestion des
  erreurs. Dans l'ancienne version, si le programme rencontrait une erreur, il
  la traitait et continuait de lire le fichier comme si il n'y en avait pas eu,
  créant alors des morceaux de termes n'étant reliés a aucun autre et
  introduisant ainsi des fuites mémoires. Maitenant, dès qu'une erreur est
  rencontrée, la lecture du fichier est arrêtée et un message d'erreur est
  affiché. Le problème lors de l'arrêt brutal de la lecture du fichier est que
  le buffer d'entrée n'est pas vidé, et il y avait donc un fort risque de
  rencontrer une erreur ou de produire des données erronées lors du chargement
  du terme suivant. Il a donc fallu créer une fonction qui se charge de vider
  ce buffer. \\

  \subsection{Terme}
  Le nettoyage du code concernant les termes s'est effectué en plusieurs fois.\\

  La première partie fut la fusion des fichiers $term.c$ et $term_rand.c$. En
  effet, toutes les fonctions implémentées dans $term_rand.c$ pour des raisons
  historiques n'avaient plus vraiment de raison d'être séparées du fichier
  $term.c$, car elles avaient besoin de la même structure de données. \\

  La seconde partie concernait les fonctions de création d'un terme aléatoire.
  En effets ces fonctions créaient en premier lieu un squelette du terme
  (constitué uniquement de $LABEL$ et de $OPLUS$), puis un tableau de $ADD$ et
  $RENAME$, avant de fusionner les deux structures. En procédant de cette
  manière le terme était parcouru deux fois. Nous avons donc regroupé ces
  fonctions pour ne plus parcourir le terme qu'une seule fois. \\

  %% ajout de commentaire dans le code...

  \section{Sous-termes}

  \section{GraphViz}
  Jusqu'à présent, il était possible de produire un fichier au format DOT
  représentant le graphe issu d'un terme. Ce fichier pouvait, en passant par des
  logiciels tiers comme GraphViz, être reconverti en un graphe et générer une
  représentation visuelle, au format PostScript ou PDF. \\

  Bien que permettant de visualiser un graphe, cette méthode était peu
  satisfaisante, car nécessitant trop d'étapes intermédiaires pour se faire
  simplement une idée de l'aspect général du graphe. L'idéal aurait été de
  disposer d'un bouton intégré dans la GUI et produisant directement une
  visualisation du graphe, en incluant si possible des informations sur les
  liens entre le terme et son graphe associé (étiquettage des sommets et
  correspondance entre les arêtes et les opérations de créations d'arêtes). \\

  Grâce à la bibliothèque $GraphViz$, nous avons pu disposer de fonctions en C
  transformant une chaîne de caractères décrivant un graphe au format DOT en une
  image PNG stockée dans un fichier temporaire. En passant par des fonctions de
  la bibliothèque GDK, nous avons ensuite pu convertir ce fichier en une image
  interne à GDK ($GdkPixBuf$), et de là l'afficher dans une fenêtre popup,
  pouvant être examinée, réduite ou fermée à la convenance de l'utilisateur. \\

  Cependant, l'emploi de ces bibliothèques nous a obligé à produire du code un
  peu moins portable ; en effet, la fonction créant un graphe à partir d'un
  pointeur de fichier, $agread$, provoque une erreur de segmentation sous
  Windows pour des raisons mal définies. Une astuce permet de contourner ce
  problème, mais nous a contraints à utiliser un ''\#ifdef G\_OS\_WIN32'' pour
  faire fonctionner le programme sous Windows XP, et s'il fonctionne également
  sur les machines Linux du CREMI, nous ignorons ce qu'il en sera pour d'autres
  systèmes d'exploitation ou d'autres architectures. \\

  Par ailleurs, certaines installations de GTK sous Windows semblent ne pas
  fournir un composant (gdk-pixbuf.loaders) pourtant indispensable à la
  création d'images en mémoire. Il est donc possible que le programme rencontre
  une erreur (''assert(gpic!=NULL)'' main-window.c:507 ou 522) en utilisant
  certaines versions des fichiers dll de GTK+.

  \section{Ajout des méta-données sur le graphe}

  \section{Reskinnage CLI/GUI}

  \section{MISC - expand/collapse + probas + mémo répertoire)}

\end{document}
