/* main.c */

#include <glib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <gtk/gtk.h>

#include "main_window.h"
#include "term_parser.h"
#include "grammar_parser.h"

static term *tr = NULL;
static grammar *gm = NULL;
static unsigned clique_width;

static bool clean = false;
static bool props = false;

static FILE *graph_file = NULL;
static FILE *out_file = NULL;

static char *out_name = NULL;


static void
usage (char *s)
{
  char *suf = strrchr(s, G_DIR_SEPARATOR);
  suf = (suf == NULL) ? s : suf + 1;

  fprintf (stderr, "%s: too few arguments.\n"
	   "Try %s --help for more informations\n", suf, suf);
  exit (EXIT_FAILURE);
}

static int
help(char *s) {
  char *suf = strrchr(s, G_DIR_SEPARATOR);
  suf = (suf == NULL) ? s : suf + 1;

  printf("%s is a term generator.\n\nMAIN OPTIONS :\n\n", suf);
  printf("  %s -g|--graphic\n\n"
	 "     run %s in graphic mode.\n\n", suf, suf);
  printf("  %s [options] <term_size> <nleaves> <cwd> <proba>\n\n"
	 "     generate a term according to \033[4;38mterm_size\033[0m, "
	 "\033[4;38mnleaves\033[0m, \033[4;38mcwd\033[0m and "
	 "\033[4;38mproba\033[0m.\n\n", suf);
  printf("  %s [options] --grammar <file> <nleaves>\n\n"
	 "     generate a term from a grammar \033[4;38mfile\033[0m according to "
	 "\033[4;38mnleaves\033[0m.\n\n", suf);
  printf("  %s [options] --term <file>\n\n"
	 "     load a term from \033[4;38mfile\033[0m.\n", suf);
  printf("\nOTHER OPTIONS :\n\n"
	 "  -h, --help                display this help and exit.\n"
	 "  -o, --outfile <file>      write the term in \033[4;38mfile\033[0m.\n"
	 "  -c, --clean               clean the term. If -o is set, "
	 "write the cleaned term in \033[4;38mfile\033[0m prefixed by 'clean_'.\n"
	 "      --graph <file>        write the graph in .dot format in \033[4;38mfile\033[0m.\n"
	 "  -p, --properties          write properties of the graph.\n"
	 );

  return EXIT_SUCCESS;
}

static int
start_gui(int argc, char *argv[]) {
  gdk_threads_init();
  gtk_init (&argc, &argv);
  mw_open ();
  gdk_threads_enter();
  gtk_main ();
  gdk_threads_leave();

  return EXIT_SUCCESS;
}

static FILE *
open_file(char *file, char *mod) {
  FILE *fd = fopen(file, mod);

  if (fd == NULL) {
    fprintf(stderr, "fail to open %s.\n", file);
    exit(EXIT_FAILURE);
  }

  return fd;
}

static term *
read_term(char *file) {
  term_in = open_file(file, "r");

  if (term_parse() != 0) {
    fprintf(stderr, "%s", term_emsg);
    g_free(term_emsg);
    exit(EXIT_FAILURE);
  }

  clique_width = term_cwd;

  fclose(term_in);
  return term_parsed; 
}

static grammar *
read_grammar(char *file) {
  grammar_in = open_file(file, "r");

  if (grammar_parse() != 0) {
    fprintf(stderr, "%s", grammar_emsg);
    g_free(grammar_emsg);
    exit(EXIT_FAILURE);
  }

  clique_width = grammar_cwd;

  fclose(grammar_in);
  return grammar_parsed; 
}

static char *
make_clean_name(void) {
  char *suf = strrchr(out_name, G_DIR_SEPARATOR);

  if (suf == NULL)
    return g_strdup_printf("clean_%s", out_name);

  suf[0] = '\0';

  return g_strdup_printf("%s/clean_%s", out_name, suf + 1);
}

static void
write_term(term *t, unsigned cwd) {
  graph *gp = NULL;
  char ***properties = NULL;

  if (props) {
    gp = term_to_graph(t);
    properties = graph_properties(gp);
  }

  char *s = term_to_string(t, properties);

  if (out_file != NULL) {
    fwrite(s, sizeof(char), strlen(s), out_file);
    fclose(out_file);
  } else
    printf("term :\n\n%s\n", s);
  g_free(s);

  if (clean) {
    t = term_clean(t, cwd);
    s = term_to_string(t, properties);

    if (out_file != NULL) {
      char *clean_name = make_clean_name();
      FILE *clean_file = open_file(clean_name, "w");

      fwrite(s, sizeof(char), strlen(s), clean_file);
      fclose(clean_file);
      g_free(clean_name);
    } else
      printf("\nterm cleaned :\n\n%s\n", s);

    g_free(s);
  }

  if (graph_file != NULL) {
    if (gp == NULL)
      gp = term_to_graph(t);
    s = graph_to_dot(gp);
    fwrite(s, sizeof(char), strlen(s), graph_file);
    fclose(graph_file);
    g_free(s);
  }

  if (gp != NULL)
    graph_delete(gp);

  term_delete_rec(t);
}

static void
start_term(unsigned size, unsigned nleaves, unsigned cwd, unsigned proba) {
  gen args;
  args.tsize = size;
  args.nleaves = nleaves;
  args.cwd = cwd;
  args.proba = proba;

  term_random(&args);

  write_term(args.term, cwd);
}

static void
start_grammar(unsigned nleaves) {
  gen args;
  args.grammar = gm;
  args.nleaves = nleaves;

  grammar_to_term(&args);

  write_term(args.term, clique_width);
}

int
main(int argc, char *argv[]) {
  g_thread_init (NULL);
  init_properties();

  struct option long_opts[] = {
    { "help", 0, 0, 'h' },
    { "term", 1, 0, '1' },
    { "graph", 1, 0, '2' },
    { "clean", 0, 0, 'c' },
    { "grammar", 1, 0, '3' },
    { "graphic", 0, 0, 'g' },
    { "properties", 0, 0, 'p' },
    { 0, 0, 0, 0 },
  };

  char *short_opts = "cgho:p";

  int opt;
  while ((opt = getopt_long(argc, argv, short_opts, long_opts, NULL)) != -1) {
    switch (opt) {
    case '1' :
      if (gm != NULL) {
	fprintf(stderr, "G/T error\n");
	return EXIT_FAILURE;
      }
      tr = read_term(optarg);
      break;

    case '3' :
      if (tr != NULL) {
	fprintf(stderr, "G/T error\n");
	return EXIT_FAILURE;
      }
      gm = read_grammar(optarg);
      break;

    case '2' :
      graph_file = open_file(optarg, "w");
      break;

    case 'o' :
      out_name = optarg;
      out_file = open_file(optarg, "w");
      break;

    case 'c' :
      clean = true;
      break;

    case 'p' :
      props = true;
      break;

    case 'g' :
      return start_gui(argc, argv);

    case 'h' :
      return help(argv[0]);

    default : // ?
      usage(argv[0]);
    }
  }

  int nb_args = argc - optind;

  if (gm != NULL) {
    if (nb_args != 1)
      usage(argv[0]);
    start_grammar(atoi(argv[optind]));
  } else if (tr != NULL) {
    if (nb_args != 0)
      usage(argv[0]);
    write_term(tr, clique_width);
  } else {
    if (nb_args != 4)
      usage(argv[0]);
    start_term(atoi(argv[optind]), atoi(argv[optind + 1]),
	       atoi(argv[optind + 2]), atoi(argv[optind + 3]));
  }

  return EXIT_SUCCESS;
}
