/* main_window.c */

/* TODO :

-> Ajouter des couleurs aux aretes montrant leurs EDGE respectives.

*/

/* En cours :

*/

/* DONE :

1- Avoir un bouton pour visualiser le terme courant

2- afficher la taille et profondeur maximale du terme courant

3- se souvenir du dernier repertoire dans lequel on a sauve le dernier terme

4- afficher les labels dans les sommets du graphe

5- voir les valeurs courantes des probabilites regissant les proportions de operations
   (sinon on a aucun idee de valeurs a essayer)

6- Pourvoir choisir un ensemble de proprietes devant etre satisfaites et lancer
   la generation jusqu'a ce qu'on obtienne un graphe avec les proprietes souhaitees
6b Avoir un bouton Stop pour arreter au cas ou ca ne termine pas assez rapidement

7- Implementer la generation a base de grammaires

8- Multi-threading

+-
-> Ajouter la fonction pour isoler un sous-terme

*/

#include "main_window.h"

#include <gtk/gtk.h>
#include <assert.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <glib.h>

#include "term_parser.h"
#include "grammar_parser.h"
#include "term.h"
#include "graph.h"
#include "properties.h"
#include "utils.h"
#include "popups.h"

//Various error messages
#define TERM_PARSE_ERROR_MSG "Parsed term seems invalid.\n\n  \
  Parser error : %s"
#define GRAMMAR_PARSE_ERROR_MSG "Parsed grammar seems invalid.\n\n  \
  Parser error : %s"
#define FILE_OPEN_ERROR_MSG "I/O error : couldn't open file %s."
#define BAD_VALUE_ERROR_MSG "Bad entry value for %s.\n\n \
  Accepted formats are : \"42\" for one value or \"1-42\" to pick from a range."
#define RADIO_BUTTONS_ERROR_MSG "Please choose a term generation mode \
  (clique-width or grammar).\n\n \
  (And by the way : please tell us how you deselected both radio buttons, \
  because we thought it was impossible.)"
#define GRAMMAR_ERROR_MSG "Current grammar is empty."
#define GRAMMAR_LOOP_ERROR_MSG "Infinite loop detected in parsed grammar."
#define EMPTY_TERM_ERROR_MSG "There is no active term to %s."
#define EMPTY_SUBTERM_ERROR_MSG "No subterm has been selected."

//Signal names
#define SIGNAL_GEN_END "generation_complete"

//Maximum expected lengths for number entries.
#define UPPER_BOUND_LOG 8
#define PERCENT_LOG 3

// Contents of the main window
GtkWidget *window;

GtkWidget *vbox_main;

GtkWidget *menubar;
GtkWidget *prog_m, *gen_m, *term_m, *graph_m;
GtkWidget *prog_i, *gen_i, *term_i, *graph_i;
GtkWidget *prog_quit,
          *gen_go, *gen_grsv, *gen_grld,
          *term_save, *term_load, *term_clear, *term_remove, *term_flush,
          *graph_ex, *graph_vis;

GtkWidget *hbox_main;

GtkWidget *vbox_gen;
GtkWidget *label_gen,
          *hbox_tsize, *label_tsize, *entry_tsize,
          *hbox_nleaves, *label_nleaves, *entry_nleaves,
          *hbox_cwd, *rb_cwd, *entry_cwd,
          *hbox_proba, *label_proba, *entry_proba, *label_percent,
          *rb_grammar,
          *tview_gr,
          *b_grload;

GtkWidget *vbox_term;
GtkWidget *hbox_tlist, *cbox_terms, *b_tgen,
          *hbox_texco, *b_texp, *b_tcol,
          *scroll_term, *tview_term,
          *label_tprop,
          *hbox_tprop, *label_tpsize, *entry_tpsize, *label_tphei, *entry_tphei,
          *hbox_trmcl, *b_trm, *b_tcl,
          *hbox_tsvld, *b_tsv, *b_tld;

GtkWidget *vbox_graph;
GtkWidget *hbox_pselect, *cb_pselect, *b_pselect,
          *label_gprop, *scroll_gprop, *tview_gprop,
          *hbox_gout, *b_dotex, *b_gdraw, *b_sgdraw;

//Virtual contents
GtkListStore *lstore_gr;

term** termtab;
unsigned current_term, termtab_fill, termtab_size;
graph* current_graph;

grammar* current_grammar;

GtkListStore* lstore_cbox;
GtkCellRenderer* cbox_cell_renderer;

GtkTreeStore* tstore_term;

GtkListStore* lstore_gprop;

gchar* str_tsize,* str_thei;

p_wanted* gprop_wanted;

struct gui_gen {
  gen* gen;
  GtkWidget* popup;
};

//Fetch a term's tree model data and send results to the cell renderer.
void term_cell_datafunc (GtkTreeViewColumn* tree_column, GtkCellRenderer* cell,
        GtkTreeModel* tree_model, GtkTreeIter* iter, gpointer data)
{
  GValue text = {0}, term = {0}, color = {0};

  gtk_tree_model_get_value (tree_model, iter, 0, &text);
  gtk_tree_model_get_value (tree_model, iter, 1, &term);
  unsigned cnb = term_edge_color (g_value_peek_pointer (&term));

  g_value_init (&color, G_TYPE_STRING);
  if (cnb == -1)
    g_value_set_string (&color, "#000000");
  else
    g_value_set_string (&color, graph_colors(current_graph)[cnb]);

  g_object_set_property (G_OBJECT(cell), "text", &text);
  g_object_set_property (G_OBJECT(cell), "foreground", &color);

  g_value_unset (&text);
  g_value_unset (&term);
  g_value_unset (&color);
}

//Fetch any tree model data and send it to the cell renderer.
void txt_datafunc (GtkTreeViewColumn* tree_column, GtkCellRenderer* cell,
        GtkTreeModel* tree_model, GtkTreeIter* iter, gpointer data)
{
  GValue val = {0};
  //We use the 'data' gpointer to hold an integer value leading to the column.
  gtk_tree_model_get_value (tree_model, iter, GPOINTER_TO_INT(data), &val);
  g_object_set_property (G_OBJECT(cell), "text", &val);

  g_value_unset (&val);
}

//Recursively build tstore_term from a term
void model_rec_build (GtkTreeIter* iter, term* t)
{
  char* node_label = term_node_to_string (t);
  gtk_tree_store_set(tstore_term, iter, 0, (gchar*)node_label, 1, (void*)t, -1);
  g_free (node_label);

  GtkTreeIter iter2;
  for (int i = 0 ; i < term_arity(t) ; i++)
  {
    gtk_tree_store_append (tstore_term, &iter2, iter);
    model_rec_build (&iter2, term_son(t, i));
  }
}

//Destroy tstore_term and free its contents.
void model_clear ()
{
  gtk_tree_store_clear (tstore_term);
}

//Change the properties display to match with the currently loaded graph.
void refresh_properties ()
{
  char*** current_properties = graph_properties (current_graph);
  assert (current_properties != NULL);

  GtkTreeIter iter;
  gtk_list_store_clear (lstore_gprop);
  for (int i = 0; i < properties_count (); i++)
  {
    gtk_list_store_append (lstore_gprop, &iter);
    gtk_list_store_set (lstore_gprop, &iter, 0, current_properties[i][0], 1,
      current_properties[i][1], -1);
  }
}

//Change current term, update the combo box, tree model, graph and properties.
void load_term (unsigned new_id, graph* new_graph)
{
  if (new_id < 0 || new_id >= termtab_fill)
  {
    model_clear ();
    current_graph = graph_create (1);
    refresh_properties ();
    return;
  }

  current_term = new_id;
  gtk_combo_box_set_active (GTK_COMBO_BOX(cbox_terms), (gint) current_term);

  model_clear ();

  GtkTreeIter iter;
  gtk_tree_store_append (tstore_term, &iter, NULL);
  gtk_tree_model_get_iter_first (GTK_TREE_MODEL(tstore_term), &iter);
  model_rec_build (&iter, termtab[current_term]);

  g_free (str_tsize);
  g_free (str_thei);
  str_tsize = g_strdup_printf ("%u", term_size(termtab[current_term]));
  str_thei = g_strdup_printf ("%u", term_height(termtab[current_term]));
  gtk_entry_set_text (GTK_ENTRY(entry_tpsize), str_tsize);
  gtk_entry_set_text (GTK_ENTRY(entry_tphei), str_thei);

  graph_delete (current_graph);

  if (new_graph == NULL)
    current_graph = term_to_graph (termtab[current_term]);
  else
    current_graph = new_graph;

  refresh_properties ();
}

//Add a new term to the termtab and the terms combo box.
unsigned add_term (term* new_term, gchar* term_name)
{
  if (new_term == NULL)
    return current_term;

  unsigned new_id = termtab_fill++;
  termtab[new_id] = new_term;

  if (termtab_fill == termtab_size)
  {
    termtab = g_realloc (termtab, ++termtab_size * sizeof (*termtab));
    termtab[termtab_size - 1] = NULL;
  }

  gtk_list_store_insert_with_values (lstore_cbox, NULL, (gint) new_id, 0,
    term_name, -1);
  g_free (term_name);

  return new_id;
}

//Delete a term, repack termtab and update current_term to a valid number.
void remove_term (unsigned target_id)
{
  assert (target_id >= 0 && target_id < termtab_fill);

  term_delete_rec (termtab[target_id]);

  GtkTreeIter iter;
  gtk_tree_model_iter_nth_child (GTK_TREE_MODEL(lstore_cbox), &iter, NULL,
         target_id);
  gtk_list_store_remove (lstore_cbox, &iter);

  for (unsigned pos = target_id; pos < termtab_fill; pos++)
    termtab[pos] = termtab[pos + 1];
  termtab_fill--;
  if (current_term == termtab_fill)
    current_term--;
}

//Take an entry's text and convert it into an unsigned value.
//Will accept a simgle number or pick one from a "min-max" range.
unsigned pick_number (const gchar* str)
{
  gchar *mstr = (gchar *) str;
  unsigned long lmin = 0, lmax = 0;
  if (*mstr != '-')
    lmin = strtoul (mstr, &mstr, 10);
  else
    return 0;

  if ((*mstr) == '-') {
    mstr++;
    lmax = strtoul (mstr, &mstr, 10) + 1;
  }
  else
    lmax = lmin + 1;

  gint32 min, max;
  min = INT_MIN + (gint32) ((lmin > UINT_MAX) ? UINT_MAX : lmin);
  max = INT_MIN + (gint32) ((lmax > UINT_MAX) ? UINT_MAX : lmax);

  if ((*mstr) != '\0' || min >= max)
    return 0;

  return (unsigned) (g_random_int_range (min, max) - INT_MIN);
}

//Thread function : Wait until generation is complete, add term if necessary.
gpointer wait_thread (gpointer data)
{
  struct gui_gen* gg = (struct gui_gen*) data;
  gen* g = gg->gen;

  while (!(g->tok));

  gdk_threads_enter();
  if (g->stop)
  {
    term_delete_rec (g->term);
    g_free (g->name);
  }
  else
  {
    load_term (add_term(g->term, g->name), g->graph);

  #ifdef G_OS_WIN32
    gdk_flush();
    gdk_threads_leave();
  #endif
    //Note: It seems that there is a deadlock in the following call under Win32.
    //To prevent this, we turn down the thread-safe mode, as a lesser evil.
    //If the user is really unlucky, the GUI *might* go to undefined behaviour.
    gtk_widget_destroy (gg->popup);
  #ifdef G_OS_WIN32
    gdk_threads_enter();
  #endif
  }

  gtk_widget_set_sensitive (window, TRUE);
  gdk_flush();
  gdk_threads_leave();

  g_mutex_free (g->mutex);
  g_free (g->prop_wanted);
  g_free (g);
  g_free (gg);

  return NULL;
}

//Thread function : Generate term according to data parameters.
gpointer generation_thread (gpointer data)
{
  gen* g = (gen*) data;

  if (!g->gram)
    if (g->prop)
      term_with_properties (g);
    else
      term_random (g);
  else
    grammar_to_term (g);

  return NULL;
}

//Fetch generation data and get to work.
gen* start_generation ()
{
  static unsigned cwd_id = 0, prop_id = 0, gr_id = 0;
  gen* g = g_malloc0 (sizeof(*g));
  g->mutex = g_mutex_new();

  g->nleaves = pick_number (gtk_entry_get_text(GTK_ENTRY(entry_nleaves)));
  if (g->nleaves == 0)
  {
    display_error_box (BAD_VALUE_ERROR_MSG, "leaves number");
    g_free (g);
    return NULL;
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (rb_cwd)))
  {
    g->gram = false;
    g->cwd = pick_number (gtk_entry_get_text(GTK_ENTRY(entry_cwd)));
    g->tsize = pick_number (gtk_entry_get_text(GTK_ENTRY(entry_tsize)));
    g->proba = pick_number (gtk_entry_get_text(GTK_ENTRY(entry_proba)));

    if (g->cwd == 0)
      display_error_box (BAD_VALUE_ERROR_MSG, "clique-width");
    else if (g->tsize == 0)
      display_error_box (BAD_VALUE_ERROR_MSG, "term size");
    else if (g->proba == 0 || g->proba > 100)
      display_error_box (BAD_VALUE_ERROR_MSG, "edge probability");

    if (g->cwd == 0 || g->tsize == 0 || g->proba == 0 || g->proba > 100)
    {
      g_free (g);
      return NULL;
    }

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_pselect)))
    {
      g->prop = true;
      p_wanted* propcopy = g_malloc0 (properties_count() * sizeof(*propcopy));
      for (unsigned i = 0 ; i < properties_count() ; i++)
        propcopy[i] = gprop_wanted[i];
      g->prop_wanted = propcopy;
      g->name = g_strdup_printf("Property term #%u", prop_id++);
    }
    else
    {
      g->prop = false;
      g->name = g_strdup_printf("Random term #%u", cwd_id++);
    }
  }
  else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rb_grammar)))
  {
    g->gram = true;
    g->grammar = current_grammar;
    if (g->grammar == NULL)
    {
      display_error_box (GRAMMAR_ERROR_MSG, NULL);
      g_free (g);
      return NULL;
    }
  
    g->name = g_strdup_printf("Grammar term #%u", gr_id++);
  }
  else
  {
    display_error_box (RADIO_BUTTONS_ERROR_MSG, NULL);
    g_free (g);
    return NULL;
  }

  assert (g_thread_create (generation_thread, g, FALSE, NULL) != NULL);
  return g;
}

//Load a new grammar as the current one and display it in the grammar box.
void load_grammar (grammar* g)
{
  if (current_grammar != NULL)
    grammar_delete (current_grammar);

  current_grammar = g;

  gtk_list_store_clear (lstore_gr);
  GtkTreeIter iter;
  for (unsigned r = 0 ; r < grammar_rule_count(g) ; r++)
  {
    rule* rp = grammar_get_rule (g, r);
    for (unsigned p = 0 ; p < rule_prod_count(rp) ; p++)
    {
      prod* pp = rule_get_prod (rp, p);
      gtk_list_store_append (lstore_gr, &iter);
      gtk_list_store_set (lstore_gr, &iter,
        0, g_strdup_printf("%g", 100 * prod_get_weight(pp)),
        1, g_strdup_printf("%s -> %s", rule_name(rp), prod_term_desc(g, pp)),
        -1);
    }
  }
}

//Callback : Draw current graph with the selecter subterm's graph highlighted.
void c_sgvis (GtkWidget* widget, gpointer data)
{
  GValue val = {0};
  GtkTreeIter iter;
  GtkTreeModel* tmodel;
  GtkTreeSelection* ts = gtk_tree_view_get_selection(GTK_TREE_VIEW(tview_term));
  if (!gtk_tree_selection_get_selected(ts, &tmodel, &iter)) {
    display_error_box(EMPTY_SUBTERM_ERROR_MSG, NULL);
    return;
  }
  gtk_tree_model_get_value (tmodel, &iter, 1, &val);

  graph* subgraph = term_to_graph (g_value_peek_pointer(&val));

  g_value_unset (&val);

  gchar* dot = subgraph_to_dot (current_graph, subgraph);
  char* pngfile = dot_to_png (dot);
  g_free (dot);

  GdkPixbuf* gpic = gdk_pixbuf_new_from_file (pngfile, NULL);
  assert (gpic != NULL);
  g_free (pngfile);

  popup_image (gpic);
  graph_delete (subgraph);
}

//Callback : Draw current graph in a popup window
void c_gvis (GtkWidget* widget, gpointer data)
{
  gchar* dot = graph_to_dot (current_graph);
  char* pngfile = dot_to_png (dot);
  g_free (dot);

  GdkPixbuf* gpic = gdk_pixbuf_new_from_file (pngfile, NULL);
  assert (gpic != NULL);
  g_free (pngfile);

  popup_image (gpic);
}

//Callback : Open the properties selection popup
void c_propup (GtkWidget* widget, gpointer data)
{
  popup_properties (gprop_wanted);
}

//Callback : Property induced generation checkbox is ticked
void c_pgchk (GtkWidget* widget, gpointer data)
{
  gboolean set;
  g_object_get (G_OBJECT(widget), "active", &set, NULL);

  gtk_widget_set_sensitive (b_pselect, set);
}

//Callback : Expand all subterms
void c_termexp (GtkWidget* widget, gpointer data)
{
  gtk_tree_view_expand_all (GTK_TREE_VIEW(tview_term));
}

//Callback : Collapse all subterms
void c_termcol (GtkWidget* widget, gpointer data)
{
  gtk_tree_view_collapse_all (GTK_TREE_VIEW(tview_term));
}

//Callback : Term selected from combo box
void c_termselect (GtkWidget* widget, gpointer data)
{
  if (current_term != gtk_combo_box_get_active(GTK_COMBO_BOX (cbox_terms)))
    load_term (gtk_combo_box_get_active(GTK_COMBO_BOX (cbox_terms)), NULL);
}

//Callback : Generate term according to selected mode
void c_tgen (GtkWidget* widget, gpointer data)
{
  gen* g = start_generation();

  if (g == NULL)
    return;

  struct gui_gen* gg = g_malloc0 (sizeof(*gg));

  gtk_widget_set_sensitive (window, FALSE);
  gg->gen = g;
  gg->popup = popup_cancel (&(g->stop));

  assert (g_thread_create (wait_thread, gg, FALSE, NULL) != NULL);
}

//Callback : Clean current term
void c_tclean (GtkWidget* widget, gpointer data)
{
  if (current_term != -1)
  {
    termtab[current_term] = term_clean (termtab[current_term],
      graph_cwd(current_graph));
    load_term (current_term, NULL);
  }
  else
    display_error_box (EMPTY_TERM_ERROR_MSG, "clean");
}

//Callback : Remove current term
void c_trem (GtkWidget* widget, gpointer data)
{
  if (termtab_fill != 0)
  {
    remove_term (current_term);
    load_term (current_term, NULL);
  }
  else
    display_error_box (EMPTY_TERM_ERROR_MSG, "remove");
}

//Callback : Remove all terms
void c_tflush (GtkWidget* widget, gpointer data)
{
  for (unsigned term_id = termtab_fill - 1; termtab_fill != 0; term_id--)
    remove_term (term_id);
  load_term (0, NULL);
}

//Callback : Save current term
void c_tsave (GtkWidget* widget, gpointer data)
{
  gchar* path = file_chooser (window, true, "Save term file (.t)");
  if (path == NULL)
    return;

  int outfile = creat (path, 0700);
  if (outfile == -1)
    display_error_box (FILE_OPEN_ERROR_MSG, path);
  else if (current_term == -1)
    display_error_box (EMPTY_TERM_ERROR_MSG, "save");
  else
  {
    //Change current directory to save destination.
    gchar* filename = strrchr (path, G_DIR_SEPARATOR);
    *filename = '\0';
    chdir (path);

    char* termstr = term_to_string (termtab[current_term],
      graph_properties(current_graph));
    write (outfile, termstr, strlen (termstr));
    g_free (termstr);
  }

  g_free (path);
  close (outfile);
}

//Callback : Load term
void c_tload (GtkWidget* widget, gpointer data)
{
  gchar* path = file_chooser (window, false, "Open term file (.t)");
  if (path == NULL)
    return;

  term_in = fopen (path, "r");
  if (term_in != NULL)
  {
    if (term_parse() == 0)
    {
      gchar* filename = strrchr (path, G_DIR_SEPARATOR);
      gchar* termstr = g_strdup(++filename);
      //Change current directory to file source.
      *(--filename) = '\0';
      chdir (path);

      unsigned nid = add_term (term_parsed, termstr);
      load_term (nid, NULL);
    }
    else
    {
      display_error_box (TERM_PARSE_ERROR_MSG, term_emsg);
      g_free (term_emsg);
    }

    fclose (term_in);
  }
  else
    display_error_box (FILE_OPEN_ERROR_MSG, path);

  g_free (path);
}

//Callback : Load a grammar from a file.
void c_gload (GtkWidget* widget, gpointer data)
{
  gchar* path = file_chooser (window, false, "Load grammar file (.g)");
  if (path == NULL)
    return;

  grammar_in = fopen (path, "r");
  if (grammar_in != NULL)
  {
    if (grammar_parse() == 0)
    {
      if (grammar_parsed == NULL)
        display_error_box (GRAMMAR_LOOP_ERROR_MSG, NULL);
      else
        load_grammar (grammar_parsed);
    }
    else
    {
      display_error_box (GRAMMAR_PARSE_ERROR_MSG, grammar_emsg);
      g_free (grammar_emsg);
    }

    gchar* endpath = strrchr (path, G_DIR_SEPARATOR);
    *endpath = '\0';
    chdir (path);
  }
  else
    display_error_box (FILE_OPEN_ERROR_MSG, path);

  g_free (path);
}

//Callback : Export graph to a dot-format file
void c_gtodot (GtkWidget* widget, gpointer data)
{
  gchar* path = file_chooser (window, true, "Save DOT file (.dot)");
  if (path == NULL)
    return;

  int outfile = creat (path, 0700);
  if (outfile == -1)
    display_error_box (FILE_OPEN_ERROR_MSG, path);
  else
  {
    char* dotstr = graph_to_dot (current_graph);
    write (outfile, dotstr, strlen (dotstr));
    g_free (dotstr);
  }

  g_free (path);
  close (outfile);
}

//Callback : Close window
void c_quit (GtkWidget* widget, gpointer data)
{
  for (unsigned i = 0; i < termtab_fill; i++)
    if (termtab[i] != NULL)
      term_delete_rec (termtab[i]);
  g_free (termtab);
  g_free (str_tsize);
  g_free (str_thei);
  graph_delete (current_graph);
  model_clear ();
  gtk_main_quit ();
}

void mw_open ()
{
  //Main window
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (G_OBJECT(window), "destroy", G_CALLBACK(c_quit), NULL);
  gtk_widget_show (window);

  vbox_main = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER(window), vbox_main);
  gtk_widget_show (vbox_main);

  //Menu bar
  menubar = gtk_menu_bar_new ();
  gtk_box_pack_start (GTK_BOX(vbox_main), menubar, FALSE, FALSE, 0);
  gtk_widget_show (menubar);

  prog_i = gtk_menu_item_new_with_label ("Program");
  gen_i = gtk_menu_item_new_with_label ("Generation");
  term_i = gtk_menu_item_new_with_label ("Term");
  graph_i = gtk_menu_item_new_with_label ("Graph");

  gtk_menu_bar_append (GTK_MENU_BAR(menubar), prog_i);
  gtk_menu_bar_append (GTK_MENU_BAR(menubar), gen_i);
  gtk_menu_bar_append (GTK_MENU_BAR(menubar), term_i);
  gtk_menu_bar_append (GTK_MENU_BAR(menubar), graph_i);

  prog_m = gtk_menu_new ();
  gen_m = gtk_menu_new ();
  term_m = gtk_menu_new ();
  graph_m = gtk_menu_new ();

  gtk_menu_item_set_submenu (GTK_MENU_ITEM(prog_i), prog_m);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM(gen_i), gen_m);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM(term_i), term_m);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM(graph_i), graph_m);

  gtk_widget_show (prog_i);
  gtk_widget_show (gen_i);
  gtk_widget_show (term_i);
  gtk_widget_show (graph_i);

  prog_quit = gtk_menu_item_new_with_label ("Quit");
  gen_go = gtk_menu_item_new_with_label ("Generate term");
  gen_grld = gtk_menu_item_new_with_label ("Load grammar");
  term_save = gtk_menu_item_new_with_label ("Save term");
  term_load = gtk_menu_item_new_with_label ("Load term");
  term_clear = gtk_menu_item_new_with_label ("Clean term");
  term_remove = gtk_menu_item_new_with_label ("Remove term");
  term_flush = gtk_menu_item_new_with_label ("Flush all terms");
  graph_ex = gtk_menu_item_new_with_label ("Export graph");
  graph_vis = gtk_menu_item_new_with_label ("Visualize graph");

  gtk_menu_append (GTK_MENU(prog_m), prog_quit);
  gtk_menu_append (GTK_MENU(gen_m), gen_go);
  gtk_menu_append (GTK_MENU(gen_m), gen_grld);
  gtk_menu_append (GTK_MENU(term_m), term_save);
  gtk_menu_append (GTK_MENU(term_m), term_load);
  gtk_menu_append (GTK_MENU(term_m), term_clear);
  gtk_menu_append (GTK_MENU(term_m), term_remove);
  gtk_menu_append (GTK_MENU(term_m), term_flush);
  gtk_menu_append (GTK_MENU(graph_m), graph_ex);
  gtk_menu_append (GTK_MENU(graph_m), graph_vis);

  gtk_widget_show (prog_quit);
  gtk_widget_show (gen_go);
  gtk_widget_show (gen_grld);
  gtk_widget_show (term_save);
  gtk_widget_show (term_load);
  gtk_widget_show (term_clear);
  gtk_widget_show (term_remove);
  gtk_widget_show (term_flush);
  gtk_widget_show (graph_ex);
  gtk_widget_show (graph_vis);

  g_signal_connect (G_OBJECT(prog_quit), "activate", G_CALLBACK(c_quit), NULL);
  g_signal_connect (G_OBJECT(gen_go), "activate", G_CALLBACK(c_tgen), NULL);
  g_signal_connect (G_OBJECT(gen_grld), "activate", G_CALLBACK(c_gload), NULL);
  g_signal_connect (G_OBJECT(term_save), "activate", G_CALLBACK(c_tsave), NULL);
  g_signal_connect (G_OBJECT(term_load), "activate", G_CALLBACK(c_tload), NULL);
  g_signal_connect (G_OBJECT(term_clear), "activate", G_CALLBACK(c_tclean),
    NULL);
  g_signal_connect (G_OBJECT(term_remove), "activate", G_CALLBACK(c_trem),
    NULL);
  g_signal_connect (G_OBJECT(term_flush), "activate", G_CALLBACK(c_tflush),
    NULL);
  g_signal_connect (G_OBJECT(graph_ex), "activate", G_CALLBACK(c_gtodot), NULL);
  g_signal_connect (G_OBJECT(graph_vis), "activate", G_CALLBACK(c_gvis), NULL);

  //Main content
  hbox_main = gtk_hbox_new (FALSE, 10);
  gtk_box_pack_start (GTK_BOX(vbox_main), hbox_main, TRUE, TRUE, 0);
  gtk_widget_show (hbox_main);

  //Generation panel
  vbox_gen = gtk_vbox_new (FALSE, 5);
  gtk_box_pack_start (GTK_BOX(hbox_main), vbox_gen, FALSE, FALSE, 0);
  gtk_widget_show (vbox_gen);

  label_gen = gtk_label_new ("Random term :");
  gtk_box_pack_start (GTK_BOX(vbox_gen), label_gen, FALSE, FALSE, 0);
  gtk_widget_show (label_gen);

  //Number of leaves entry
  hbox_nleaves = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_gen), hbox_nleaves, FALSE, FALSE, 0);
  gtk_widget_show (hbox_nleaves);

  label_nleaves = gtk_label_new ("Leaves nb :");
  gtk_box_pack_start (GTK_BOX(hbox_nleaves), label_nleaves, FALSE, FALSE, 0);
  gtk_widget_show (label_nleaves);

  entry_nleaves = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX(hbox_nleaves), entry_nleaves, FALSE, FALSE, 0);
  gtk_entry_set_width_chars (GTK_ENTRY(entry_nleaves), 2*UPPER_BOUND_LOG);
  gtk_widget_show (entry_nleaves);

  //Clique-width option
  hbox_cwd = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_gen), hbox_cwd, FALSE, FALSE, 0);
  gtk_widget_show (hbox_cwd);

  rb_cwd = gtk_radio_button_new_with_label (NULL, "Clique-width :");
  gtk_box_pack_start (GTK_BOX(hbox_cwd), rb_cwd, FALSE, FALSE, 0);
  gtk_widget_show (rb_cwd);

  entry_cwd = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX(hbox_cwd), entry_cwd, FALSE, FALSE, 0);
  gtk_entry_set_width_chars (GTK_ENTRY(entry_cwd), 2*PERCENT_LOG);
  gtk_widget_show (entry_cwd);

  //Term size entry
  hbox_tsize = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_gen), hbox_tsize, FALSE, FALSE, 0);
  gtk_widget_show (hbox_tsize);

  label_tsize = gtk_label_new ("Term size :");
  gtk_box_pack_start (GTK_BOX(hbox_tsize), label_tsize, FALSE, FALSE, 0);
  gtk_widget_show (label_tsize);

  entry_tsize = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX(hbox_tsize), entry_tsize, FALSE, FALSE, 0);
  gtk_entry_set_width_chars (GTK_ENTRY(entry_tsize), 2*UPPER_BOUND_LOG);
  gtk_widget_show (entry_tsize);

  //Probability entry
  hbox_proba = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_gen), hbox_proba, FALSE, FALSE, 0);
  gtk_widget_show (hbox_proba);

  label_proba = gtk_label_new ("P(edge) / P(edge\\/rename) :");
  gtk_box_pack_start (GTK_BOX(hbox_proba), label_proba, FALSE, FALSE, 0);
  gtk_widget_show (label_proba);

  entry_proba = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX(hbox_proba), entry_proba, FALSE, FALSE, 0);
  gtk_entry_set_width_chars (GTK_ENTRY(entry_proba), PERCENT_LOG);
  gtk_widget_show (entry_proba);

  label_percent = gtk_label_new ("%");
  gtk_box_pack_start (GTK_BOX(hbox_proba), label_percent, FALSE, FALSE, 0);
  gtk_widget_show (label_percent);

  //Grammar option
  rb_grammar = gtk_radio_button_new_with_label(
    gtk_radio_button_get_group(GTK_RADIO_BUTTON(rb_cwd)), "Grammar :");
  gtk_box_pack_start (GTK_BOX(vbox_gen), rb_grammar, FALSE, FALSE, 0);
  gtk_widget_show (rb_grammar);

  //Grammar box
  lstore_gr = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);

  tview_gr = gtk_tree_view_new_with_model (GTK_TREE_MODEL(lstore_gr));
  gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW(tview_gr), 0,
    " % ", gtk_cell_renderer_text_new (), txt_datafunc, (gpointer) 0,
    NULL);
  gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW(tview_gr), 1,
    "Rule", gtk_cell_renderer_text_new (), txt_datafunc, (gpointer) 1,
    NULL);
  gtk_box_pack_start (GTK_BOX(vbox_gen), tview_gr, TRUE, TRUE, 0);
  gtk_widget_show (tview_gr);

  //Load grammar button
  b_grload = gtk_button_new_with_label ("Load grammar");
  g_signal_connect (G_OBJECT(b_grload), "clicked", G_CALLBACK(c_gload), NULL);
  gtk_box_pack_start (GTK_BOX(vbox_gen), b_grload, FALSE, FALSE, 0);
  gtk_widget_show (b_grload);

  //Term panel
  vbox_term = gtk_vbox_new (FALSE, 5);
  gtk_box_pack_start (GTK_BOX(hbox_main), vbox_term, TRUE, TRUE, 0);
  gtk_widget_show (vbox_term);

  //Terms list controls
  hbox_tlist = gtk_hbox_new (TRUE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_term), hbox_tlist, FALSE, FALSE, 0);
  gtk_widget_show (hbox_tlist);

  lstore_cbox = gtk_list_store_new (1, G_TYPE_STRING);
  cbox_cell_renderer = gtk_cell_renderer_text_new ();

  cbox_terms = gtk_combo_box_new_with_model (GTK_TREE_MODEL(lstore_cbox));
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT(cbox_terms), cbox_cell_renderer,
    FALSE);
  gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT(cbox_terms),
    cbox_cell_renderer, "text", 0);
  g_signal_connect (G_OBJECT(cbox_terms), "changed", G_CALLBACK(c_termselect),
    NULL);
  gtk_box_pack_start (GTK_BOX(hbox_tlist), cbox_terms, TRUE, TRUE, 0);
  gtk_widget_show (cbox_terms);

  b_tgen = gtk_button_new_with_label ("Generate");
  g_signal_connect (G_OBJECT(b_tgen), "clicked", G_CALLBACK(c_tgen), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_tlist), b_tgen, FALSE, FALSE, 0);
  gtk_widget_show (b_tgen);

  //Term tree view
  hbox_texco = gtk_hbox_new (TRUE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_term), hbox_texco, FALSE, FALSE, 0);
  gtk_widget_show (hbox_texco);

  b_texp = gtk_button_new_with_label ("(+) Expand all");
  g_signal_connect (G_OBJECT(b_texp), "clicked", G_CALLBACK(c_termexp), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_texco), b_texp, FALSE, FALSE, 0);
  gtk_widget_show (b_texp);

  b_tcol = gtk_button_new_with_label ("(-) Collapse all");
  g_signal_connect (G_OBJECT(b_tcol), "clicked", G_CALLBACK(c_termcol), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_texco), b_tcol, FALSE, FALSE, 0);
  gtk_widget_show (b_tcol);

  scroll_term = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll_term),
    GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX(vbox_term), scroll_term, TRUE, TRUE, 0);
  gtk_widget_show (scroll_term);

  tstore_term = gtk_tree_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);

  tview_term = gtk_tree_view_new_with_model (GTK_TREE_MODEL(tstore_term));
  gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW(tview_term), 0,
    "Node", gtk_cell_renderer_text_new (), term_cell_datafunc, NULL, NULL);
  gtk_container_add (GTK_CONTAINER(scroll_term), tview_term);
  gtk_widget_show (tview_term);

  //Term properties
  label_tprop = gtk_label_new ("Term properties :");
  gtk_box_pack_start (GTK_BOX(vbox_term), label_tprop, FALSE, FALSE, 0);
  gtk_widget_show (label_tprop);

  hbox_tprop = gtk_hbox_new (TRUE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_term), hbox_tprop, FALSE, FALSE, 0);
  gtk_widget_show (hbox_tprop);

  label_tpsize = gtk_label_new ("Size :");
  gtk_box_pack_start (GTK_BOX(hbox_tprop), label_tpsize, FALSE, FALSE, 0);
  gtk_widget_show (label_tpsize);

  entry_tpsize = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX(hbox_tprop), entry_tpsize, FALSE, FALSE, 0);
  gtk_entry_set_editable (GTK_ENTRY(entry_tpsize), FALSE);
  gtk_entry_set_width_chars (GTK_ENTRY(entry_tpsize), UPPER_BOUND_LOG);
  gtk_widget_show (entry_tpsize);

  label_tphei = gtk_label_new ("Height :");
  gtk_box_pack_start (GTK_BOX(hbox_tprop), label_tphei, FALSE, FALSE, 0);
  gtk_widget_show (label_tphei);

  entry_tphei = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX(hbox_tprop), entry_tphei, FALSE, FALSE, 0);
  gtk_entry_set_editable (GTK_ENTRY(entry_tphei), FALSE);
  gtk_entry_set_width_chars (GTK_ENTRY(entry_tphei), UPPER_BOUND_LOG);
  gtk_widget_show (entry_tphei);

  //Clean term button
  hbox_trmcl = gtk_hbox_new (TRUE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_term), hbox_trmcl, FALSE, FALSE, 0);
  gtk_widget_show (hbox_trmcl);

  b_tcl = gtk_button_new_with_label ("Clean term");
  g_signal_connect (G_OBJECT(b_tcl), "clicked", G_CALLBACK(c_tclean), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_trmcl), b_tcl, FALSE, FALSE, 0);
  gtk_widget_show (b_tcl);

  b_trm = gtk_button_new_with_label ("Remove term");
  g_signal_connect (G_OBJECT(b_trm), "clicked", G_CALLBACK(c_trem), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_trmcl), b_trm, FALSE, FALSE, 0);
  gtk_widget_show (b_trm);

  //Save/load term buttons
  hbox_tsvld = gtk_hbox_new (TRUE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_term), hbox_tsvld, FALSE, FALSE, 0);
  gtk_widget_show (hbox_tsvld);

  b_tsv = gtk_button_new_with_label ("Save term");
  g_signal_connect (G_OBJECT(b_tsv), "clicked", G_CALLBACK(c_tsave), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_tsvld), b_tsv, FALSE, FALSE, 0);
  gtk_widget_show (b_tsv);

  b_tld = gtk_button_new_with_label ("Load term");
  g_signal_connect (G_OBJECT(b_tld), "clicked", G_CALLBACK(c_tload), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_tsvld), b_tld, FALSE, FALSE, 0);
  gtk_widget_show (b_tld);

  //Graph panel
  vbox_graph = gtk_vbox_new (FALSE, 5);
  gtk_box_pack_start (GTK_BOX(hbox_main), vbox_graph, FALSE, FALSE, 0);
  gtk_widget_show (vbox_graph);

  //Properties selection
  hbox_pselect = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_graph), hbox_pselect, FALSE, FALSE, 0);
  gtk_widget_show (hbox_pselect);

  cb_pselect = gtk_check_button_new_with_label ("Generate term according to :");
  g_signal_connect (G_OBJECT(cb_pselect), "toggled", G_CALLBACK(c_pgchk), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_pselect), cb_pselect, FALSE, FALSE, 0);
  gtk_widget_show (cb_pselect);

  b_pselect = gtk_button_new_with_label ("Set properties");
  g_signal_connect (G_OBJECT(b_pselect), "clicked", G_CALLBACK(c_propup), NULL);
  gtk_widget_set_sensitive (b_pselect, FALSE);
  gtk_box_pack_start (GTK_BOX(hbox_pselect), b_pselect, FALSE, FALSE, 0);
  gtk_widget_show (b_pselect);

  //Graph properties
  label_gprop = gtk_label_new ("Graph properties :");
  gtk_box_pack_start (GTK_BOX(vbox_graph), label_gprop, FALSE, FALSE, 0);
  gtk_widget_show (label_gprop);

  scroll_gprop = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll_gprop),
    GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX(vbox_graph), scroll_gprop, TRUE, TRUE, 0);
  gtk_widget_show (scroll_gprop);

  lstore_gprop = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);

  tview_gprop = gtk_tree_view_new_with_model (GTK_TREE_MODEL(lstore_gprop));
  gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW(tview_gprop), 0,
    "Property", gtk_cell_renderer_text_new(), txt_datafunc, (gpointer) 0, NULL);
  gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW(tview_gprop), 1,
    "Value", gtk_cell_renderer_text_new (), txt_datafunc, (gpointer) 1, NULL);
  gtk_container_add (GTK_CONTAINER(scroll_gprop), tview_gprop);
  gtk_widget_show (tview_gprop);

  //Export/draw graph buttons
  hbox_gout = gtk_hbox_new (TRUE, 2);
  gtk_box_pack_start (GTK_BOX(vbox_graph), hbox_gout, FALSE, FALSE, 0);
  gtk_widget_show (hbox_gout);

  b_dotex = gtk_button_new_with_label ("Export to .dot");
  g_signal_connect (G_OBJECT(b_dotex), "clicked", G_CALLBACK(c_gtodot), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_gout), b_dotex, FALSE, FALSE, 0);
  gtk_widget_show (b_dotex);

  b_gdraw = gtk_button_new_with_label ("Draw graph");
  g_signal_connect (G_OBJECT(b_gdraw), "clicked", G_CALLBACK(c_gvis), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_gout), b_gdraw, FALSE, FALSE, 0);
  gtk_widget_show (b_gdraw);

  b_sgdraw = gtk_button_new_with_label ("Draw subgraph");
  g_signal_connect (G_OBJECT(b_sgdraw), "clicked", G_CALLBACK(c_sgvis), NULL);
  gtk_box_pack_start (GTK_BOX(hbox_gout), b_sgdraw, FALSE, FALSE, 0);
  gtk_widget_show (b_sgdraw);

  //Fill in virtual contents
  termtab = g_malloc0 (sizeof(*termtab));
  current_term = -1;
  termtab_fill = 0;
  termtab_size = 1;

  str_tsize = NULL;
  str_thei = NULL;

  current_grammar = NULL;

  gprop_wanted = g_malloc0 (properties_count() * sizeof(*gprop_wanted));
  for (int i = 0 ; i < properties_count() ; i++)
  {
    gprop_wanted[i].yes = false;
    gprop_wanted[i].value = g_strdup("");
  }

  load_term (0, NULL);
}

void property_filter_set (unsigned p, bool set)
{
  if (p >= properties_count())
    return;
  gprop_wanted[p].yes = set;
}

void property_filter_value (unsigned p, const char* val)
{
  if (p >= properties_count())
    return;
  g_free (gprop_wanted[p].value);
  gprop_wanted[p].value = g_strdup(val);
}
