/* main_window.h */

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <stdbool.h>

//Create and display the main window
extern void mw_open ();

//Set the property filter settings.
extern void property_filter_set (unsigned p, bool set);
extern void property_filter_value (unsigned p, const char* val);

#endif // MAIN_WINDOW_H
