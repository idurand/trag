#include "popups.h"

#include <assert.h>

#include "main_window.h"
#include "properties.h"

void display_error_box (const gchar* errormsg, gchar* moreinfo)
{
  GtkWidget* error_box = gtk_message_dialog_new (NULL, 0, GTK_MESSAGE_ERROR,
    GTK_BUTTONS_CLOSE, errormsg, moreinfo);

  //'if' to wait until the box is closed.
  if (gtk_dialog_run (GTK_DIALOG(error_box)));

  gtk_widget_destroy (error_box);
}

gchar* file_chooser (GtkWidget* parent, bool write, const gchar* name)
{
  GtkWidget* filechooser;
  gchar* file = NULL;

  if (write)
    filechooser = gtk_file_chooser_dialog_new (name, GTK_WINDOW(parent),
      GTK_FILE_CHOOSER_ACTION_SAVE, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT, NULL);
  else
    filechooser = gtk_file_chooser_dialog_new (name, GTK_WINDOW(parent),
      GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);

  if (gtk_dialog_run (GTK_DIALOG(filechooser)) == GTK_RESPONSE_ACCEPT)
    file = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER(filechooser));

  gtk_widget_destroy (filechooser);
  return file;
}

//Callback : Close popup window
void c_popdown (GtkWidget* widget, gpointer data)
{
  gtk_widget_destroy (GTK_WIDGET(data));
}

void popup_image (GdkPixbuf* pic)
{
  //Contents of the popup window.
  GtkWidget* popup,* scroll_img, * g_img;

  //Draw window.
  popup = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (G_OBJECT(popup), "destroy", G_CALLBACK(c_popdown), popup);
  gtk_widget_show (popup);

  scroll_img = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scroll_img),
    GTK_POLICY_ALWAYS, GTK_POLICY_ALWAYS);
  gtk_container_add (GTK_CONTAINER(popup), scroll_img);
  gtk_widget_show (scroll_img);

  g_img = gtk_image_new_from_pixbuf (pic);
  g_object_unref (pic);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scroll_img), g_img);
  gtk_widget_show (g_img);
}

//Callback : Validate properties filtering selection
void c_pok (GtkWidget* widget, gpointer data)
{
  // Data = TreeView C ScrollWindow C VBox C Window
  GtkWidget* popup =  gtk_widget_get_parent(
                        gtk_widget_get_parent(
                          gtk_widget_get_parent(data)));
  GtkTreeModel* lstore = gtk_tree_view_get_model(GTK_TREE_VIEW(data));
  GValue chkval = {0}, strval = {0};

  GtkTreeIter* iter = g_malloc0 (sizeof(*iter));
  gtk_tree_model_get_iter_first(lstore, iter);
  for (int i = 0 ; i < properties_count() ; i++)
  {
    gtk_tree_model_get_value (lstore, iter, 0, &chkval);
    gtk_tree_model_get_value (lstore, iter, 2, &strval);
    property_filter_set (i, g_value_get_boolean(&chkval));
    property_filter_value (i, g_value_get_string(&strval));
    g_value_unset(&chkval);
    g_value_unset(&strval);
    gtk_tree_model_iter_next(lstore, iter);
  }
  g_free (iter);
  gtk_widget_destroy (popup);
}

//Callback : Cancel properties filtering selection
void c_pcancel (GtkWidget* widget, gpointer data)
{
  gtk_widget_destroy (data);
}

void c_pchk (GtkCellRendererToggle* cellr, gchar* path, gpointer data)
{
  GtkTreeIter* iter = g_malloc0 (sizeof(*iter));
  GValue val = {0};
  g_value_init (&val, G_TYPE_BOOLEAN);
  g_value_set_boolean (&val, !gtk_cell_renderer_toggle_get_active(cellr));
  assert(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(data), iter, path));
  gtk_list_store_set_value(GTK_LIST_STORE(data), iter, 0, &val);
  g_free (iter);
}

void c_pval (GtkCellRendererText* cellr, gchar* path, gchar* txt, gpointer data)
{
  GtkTreeIter* iter = g_malloc0 (sizeof(*iter));
  GValue val = {0};
  g_value_init (&val, G_TYPE_STRING);
  g_value_set_string (&val, txt);
  assert(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(data), iter, path));
  gtk_list_store_set_value(GTK_LIST_STORE(data), iter, 2, &val);
  g_value_unset (&val);
  g_free (iter);
}

//See if a property is required and send info to the cell renderer.
void pcheck_datafunc (GtkTreeViewColumn* tree_column, GtkCellRenderer* cell,
        GtkTreeModel* tree_model, GtkTreeIter* iter, gpointer data)
{
  GValue val = {0};
  gtk_tree_model_get_value (tree_model, iter, 0, &val);
  g_object_set_property (G_OBJECT(cell), "active", &val);
  g_value_unset (&val);
}

//Fetch the name of a property and send it to the cell renderer.
void pname_datafunc (GtkTreeViewColumn* tree_column, GtkCellRenderer* cell,
        GtkTreeModel* tree_model, GtkTreeIter* iter, gpointer data)
{
  GValue val = {0};
  gtk_tree_model_get_value (tree_model, iter, 1, &val);
  g_object_set_property (G_OBJECT(cell), "text", &val);
  g_value_unset (&val);
}

//Fetch the property value data and send it to the cell renderer.
void pvalue_datafunc (GtkTreeViewColumn* tree_column, GtkCellRenderer* cell,
        GtkTreeModel* tree_model, GtkTreeIter* iter, gpointer data)
{
  GValue val = {0};
  gtk_tree_model_get_value (tree_model, iter, 2, &val);
  g_object_set_property (G_OBJECT(cell), "text", &val);
  g_value_unset (&val);
}

void popup_properties (p_wanted* wanted)
{
  //Contents of the popup window.
  GtkWidget* popup;

  GtkWidget* vbox_main;
  GtkWidget* scroll_props, * tview_props;

  GtkWidget* hbox_buttons,* b_ok,* b_cancel;

  //Virtual contents
  GtkListStore* lstore_props;

  //Draw window
  popup = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (G_OBJECT(popup), "destroy", G_CALLBACK(c_popdown), popup);
  gtk_widget_show (popup);

  vbox_main = gtk_vbox_new (FALSE, 20);
  gtk_container_add (GTK_CONTAINER(popup), vbox_main);
  gtk_widget_show (vbox_main);

  //Properties list view
  scroll_props = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scroll_props),
    GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX(vbox_main), scroll_props, TRUE, TRUE, 0);
  gtk_widget_show (scroll_props);

  lstore_props = gtk_list_store_new (3, G_TYPE_BOOLEAN, G_TYPE_STRING,
    G_TYPE_STRING);

  GtkCellRenderer* check_renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect(check_renderer, "toggled", G_CALLBACK(c_pchk), lstore_props);
  GtkCellRenderer* text_renderer = gtk_cell_renderer_text_new ();
  GtkCellRenderer* entry_renderer = gtk_cell_renderer_text_new ();
  g_object_set (entry_renderer, "editable", TRUE, NULL);
  g_signal_connect(entry_renderer, "edited", G_CALLBACK(c_pval), lstore_props);

  tview_props = gtk_tree_view_new_with_model (GTK_TREE_MODEL(lstore_props));
  gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW(tview_props), 0,
    "Check ?", check_renderer, pcheck_datafunc, NULL, NULL);
  gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW(tview_props), 1,
    "Property", text_renderer, pname_datafunc, NULL, NULL);
  gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW(tview_props), 2,
    "Desired value", entry_renderer, pvalue_datafunc, NULL, NULL);
  gtk_container_add (GTK_CONTAINER(scroll_props), tview_props);
  gtk_widget_show (tview_props);

  //Exit buttons
  hbox_buttons = gtk_hbox_new (TRUE, 10);
  gtk_box_pack_start (GTK_BOX(vbox_main), hbox_buttons, FALSE, FALSE, 0);
  gtk_widget_show (hbox_buttons);

  b_ok = gtk_button_new_with_label ("OK");
  g_signal_connect (G_OBJECT(b_ok), "clicked", G_CALLBACK(c_pok), tview_props);
  gtk_box_pack_start (GTK_BOX(hbox_buttons), b_ok, FALSE, FALSE, 0);
  gtk_widget_show (b_ok);

  b_cancel = gtk_button_new_with_label ("Cancel");
  g_signal_connect(G_OBJECT(b_cancel), "clicked", G_CALLBACK(c_pcancel), popup);
  gtk_box_pack_start (GTK_BOX(hbox_buttons), b_cancel, FALSE, FALSE, 0);
  gtk_widget_show (b_cancel);

  //Fill in virtual contents
  for (int i = 0 ; i < properties_count() ; i++)
  {
    gtk_list_store_insert_with_values (lstore_props, NULL, i,
      0, wanted[i].yes, 1, property_name(i), 2, wanted[i].value, -1);
  }
}

//Set stop to true when generation is cancelled.
void c_abort (GtkWidget* widget, gpointer data)
{
  bool* stop = (bool*) data;
  *stop = true;
}

GtkWidget* popup_cancel (bool* stop)
{
  GtkWidget* popup;

  GtkWidget* vbox_main;

  GtkWidget* b_cancel;

  popup = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (G_OBJECT(popup), "destroy", G_CALLBACK(c_abort), stop);
  gtk_widget_show (popup);

  vbox_main = gtk_vbox_new (FALSE, 10);
  gtk_container_add (GTK_CONTAINER(popup), vbox_main);
  gtk_widget_show (vbox_main);

  b_cancel = gtk_button_new_with_label ("Abort generation");
  g_signal_connect (G_OBJECT(b_cancel), "clicked", G_CALLBACK(c_abort), stop);
  g_signal_connect(G_OBJECT(b_cancel), "clicked", G_CALLBACK(c_popdown), popup);
  gtk_box_pack_start (GTK_BOX(vbox_main), b_cancel, TRUE, FALSE, 0);
  gtk_widget_show (b_cancel);

  return popup;
}
