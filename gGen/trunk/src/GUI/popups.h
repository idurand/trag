/* popups.h */

#ifndef POPUPS_H
#define POPUPS_H

#include <stdbool.h>
#include <gtk/gtk.h>
#include <glib.h>

#include "properties.h"

//Display a message box to the user giving error type and specific info.
extern void display_error_box (const gchar* errormsg, gchar* info);

//Display a file chooser with a title in front of 'parent', to save/load a file.
extern gchar* file_chooser (GtkWidget* parent, bool write, const gchar* name);

//Display a popup window featuring an image.
extern void popup_image (GdkPixbuf* pic);

//Display a popup window for selecting a set of desired graph properties.
extern void popup_properties (p_wanted* wanted);

//Display a cancel button that will set stop to TRUE if clicked. Returns itself.
extern GtkWidget* popup_cancel (bool* stop);

#endif  //POPUPS_H
