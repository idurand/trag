axiom: LEFT;

LEFT: 0 | REC;

REC: ren_1_0(add_1_1(oplus(LEFT, 1)));
