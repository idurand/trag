axiom: CYCLE;

CYCLE: add_0_1(REC);

REC: ren_2_1(ren_1_3(add_1_2(oplus(LEFT, 2))));

LEFT: add_0_1(oplus(0, 1)) | REC;
