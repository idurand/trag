axiom: BEGIN;

BEGIN: 0 | END;

END: oplus(0, 1);

MIDDLE: oplus(BEGIN, END); // this rule will never be used

LEFT: oplus(MIDDLE, LEFT);
