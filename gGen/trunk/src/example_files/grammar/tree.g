axiom: TREE;

TREE: REC | 1;

REC: ren_2_1(ren_1_0(add_1_2(oplus(LEAVES, 2))));

LEAVES: oplus(LEAVES, LEAVES) | REC | 1;
