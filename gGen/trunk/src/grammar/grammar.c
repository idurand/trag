/* grammar.c */

#include <glib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include "grammar.h"
#include "utils.h"

/*  A regular grammar will look something like this :
GRAMMAR0 -
RULE0 : PROD00 | PROD01 | PROD02;
RULE1 : PROD10;
RULE2 : PROD20 | PROD21;
*/

//  A grammar is a set of rules with a given axiom.
struct grammar {
  unsigned nbrules;
  rule** rules;
  unsigned axiom;
};

//  Each rule has several possible productions (prods), and a 'human' name.
struct rule {
  char* name;
  unsigned nbprods;
  prod** prods;
};

/*  Each production has a set of subrules, an associated term, and a weight.
Its term may be complete (if terminal) or have open-ended subterms (if not) :
ren_0_1(add_0_2(oplus(0, oplus(NT:3, 2))))    // 1 subrule (#3)
When a term is generated from a rule, the chosen production will be picked with
  a certain probability, determined by its relative weigth. */
struct prod {
  term* term;
  unsigned nbrules;
  unsigned* rules_id;
  unsigned minsize;
  unsigned maxsize;
  double w;
};

prod* prod_create (term* term, unsigned* nonterm, unsigned ntsize, double w)
{
  prod* p = g_malloc0 (sizeof(*p));
  p->term = term;
  p->rules_id = nonterm;
  p->nbrules = ntsize;
  p->minsize = -1;
  p->maxsize = -1;
  p->w = w;
  return p;
}

void prod_delete (prod* p)
{
  assert (p != NULL);
  term_delete_rec (p->term);
  g_free (p->rules_id);
  g_free (p);
}

gchar* prod_term_desc (grammar* g, prod* p)
{
  assert (p != NULL);
  gchar* str = term_to_string (p->term, NULL);
  gchar* modif,* ptr;
  for (unsigned i = 0 ; i < p->nbrules ; i++)
  {
    //Simply put : we search for the first "NT:??", then cut str just before it.
    //We read the number after "NT", and grab the end of the string doing that.
    //We replace the "NT:??" by the corresponding rule's name, and so on.
    ptr = strchr(str, ':');
    *(ptr-2) = '\0';
    while (*(++ptr) != ',' && *ptr != ')' && *ptr != '\0');
    modif = g_strdup_printf("%s%s%s", str, g->rules[p->rules_id[i]]->name, ptr);
    g_free (str);
    str = modif;
  }
  return str;
}

double prod_get_weight (prod* p)
{
  assert (p != NULL);
  return p->w;
}

rule* rule_create (char* name, prod** prods, unsigned nbprods)
{
  rule* r = g_malloc0 (sizeof(*r));
  r->name = name;
  r->prods = prods;
  r->nbprods = nbprods;
  return r;
}

void rule_delete (rule* r)
{
  assert (r != NULL);
  for (unsigned i = 0 ; i < r->nbprods ; i++)
    prod_delete (r->prods[i]);
  g_free (r->prods);
  g_free (r->name);
  g_free (r);
}

char* rule_name (rule* r)
{
  return r->name;
}

unsigned rule_prod_count (rule* r)
{
  assert (r != NULL);
  return r->nbprods;
}

prod* rule_get_prod (rule* r, unsigned p)
{
  assert (r != NULL);
  assert (p < r->nbprods);
  return r->prods[p];
}

//Put the 'changed' array back to 'true'.
static void reset_changed (bool** changed, grammar* g)
{
  for (int i = 0 ; i < g->nbrules ; i++)
    for (int j = 0 ; j < g->rules[i]->nbprods ; j++)
      changed[i][j] = true;
}

//Recursively explore the size of a grammar's production.
static void prod_size_rec (grammar* g, bool** changed, unsigned r, unsigned p)
{
  prod* pp = g->rules[r]->prods[p];
  while (changed[r][p])
  {
    changed[r][p] = false;

    unsigned newmin = term_part_size (pp->term);
    unsigned newmax = newmin;

    for (unsigned i = 0 ; i < pp->nbrules ; i++)
    { //For each nonterminal...
      unsigned r2 = pp->rules_id[i];
      unsigned rminsize = -1, rmaxsize = 0;
      for (unsigned p2 = 0 ; p2 < g->rules[r2]->nbprods ; p2++)
      { //..for each production of this nonterminal...        
        prod_size_rec (g, changed, r2, p2);

        rminsize = MIN(rminsize, g->rules[r2]->prods[p2]->minsize);
        rmaxsize = MAX(rmaxsize, g->rules[r2]->prods[p2]->maxsize);
        //...find its minmax size and keep the best...
      }

      //...then sum up the best sizes.
      if (newmin == -1 || rminsize == -1)
        newmin = -1;
      else
        newmin += rminsize;

      if (newmax == -1 || rmaxsize == -1)
        newmax = -1;
      else
        newmax += rmaxsize;
    }

    if (newmin < pp->minsize || newmax < pp->maxsize)
    {
      pp->minsize = newmin;
      pp->maxsize = newmax;
      reset_changed (changed, g);
    }
  }
}

//Recursively explore a grammar's production to see if it can always be reduced.
static bool loop_test_rec (grammar* g, bool** unseen, unsigned r, unsigned p)
{
  prod* pp = g->rules[r]->prods[p];

  if (!unseen[r][p])
    return false;
  unseen[r][p] = false;

  bool loop = (pp->minsize == -1);
  for (unsigned i = 0 ; i < pp->nbrules ; i++)
  {
    unsigned r2 = pp->rules_id[i];
    for (unsigned p2 = 0 ; p2 < g->rules[r2]->nbprods ; p2++)
      loop |= loop_test_rec (g, unseen, r2, p2);
  }
  return loop;
}

//Set minimum and maximum size for each production in a given grammar.
static bool grammar_set_sizes (grammar* g)
{
  bool loop = false;
  bool** changed = g_malloc0 (g->nbrules * sizeof(*changed));
  for (int i = 0 ; i < g->nbrules ; i++)
  {
    changed[i] = g_malloc0 (g->rules[i]->nbprods * sizeof(*changed[i]));
  }
  reset_changed (changed, g);

  for (unsigned p = 0 ; p < g->rules[g->axiom]->nbprods ; p++)
    prod_size_rec (g, changed, g->axiom, p);

  reset_changed (changed, g);
  for (unsigned p = 0 ; p < g->rules[g->axiom]->nbprods ; p++)
    loop |= loop_test_rec (g, changed, g->axiom, p);

  for (int i = 0 ; i < g->nbrules ; i++)
    g_free (changed[i]);
  g_free (changed);
  return loop;
}

grammar* grammar_create (unsigned axiom, rule** rules, unsigned nbrules)
{
  grammar* g = g_malloc0 (sizeof(*g));
  g->axiom = axiom;
  g->rules = rules;
  g->nbrules = nbrules;
  if (grammar_set_sizes (g))
    return NULL;
  else
    return g;
}

void grammar_delete (grammar* g)
{
  assert (g != NULL);
  for (unsigned i = 0 ; i < g->nbrules ; i++)
    rule_delete (g->rules[i]);
  g_free (g->rules);
  g_free (g);
}

unsigned grammar_rule_count (grammar* g)
{
  assert (g != NULL);
  return g->nbrules;
}

rule* grammar_get_rule (grammar* g, unsigned r)
{
  assert (g != NULL);
  assert (r < g->nbrules);
  return g->rules[r];
}

unsigned grammar_get_axiom (grammar* g)
{
  assert (g != NULL);
  return g->axiom;
}

//Divide recursively the expected term's leaves number between a set of rules.
void pick_subrules_sizes (unsigned* rmins, unsigned* rmaxs, unsigned* result,
  unsigned length, unsigned size)
{
  if (length > 1)
  {
    unsigned halflen = length/2, halflen2 = length-halflen;

    unsigned leftmin = sigma(rmins, halflen),
      leftmax = sigma (rmaxs, halflen),
      rightmin = sigma (rmins+halflen, halflen2),
      rightmax = sigma(rmaxs+halflen, halflen2);

    unsigned minpick = MAX(leftmin, (size-rightmax < 0)?0:size-rightmax);
    if (rightmax == -1)
      minpick = leftmin;
    unsigned maxpick = MIN(size-rightmin, leftmax);

    //If this fails, it's probably time to use 128-bits hardware.
    assert (maxpick < INT_MAX);

    if (minpick == maxpick)
      maxpick++;

    unsigned msize = (unsigned) (g_random_int_range(minpick, maxpick));

    pick_subrules_sizes (rmins, rmaxs, result, halflen, msize);
    pick_subrules_sizes (rmins+halflen, rmaxs+halflen, result+halflen, halflen2,
      size-msize);
  }
  else
    *result = size;
}

//Generate a term with a given leaves number from a grammar's rule.
term* rule_to_term (grammar* g, rule* r, unsigned size)
{
  double wmax = 0.0;
  prod* p,* p2,* pmin,* pmax;
  unsigned minsize = -1, maxsize = 0;
  list* symbstack = list_create();
  for (int i = 0 ; i < r->nbprods ; i++)
  {
    p2 = r->prods[i];

    if (p2->minsize < minsize)
    {
      minsize = p2->minsize;
      pmin = p2;
    }

    if (p2->maxsize > maxsize)
    {
      maxsize = p2->maxsize;
      pmax = p2;
    }

    if (p2->minsize <= size && p2->maxsize >= size)
    {
      list_push_queue (symbstack, p2);
      wmax += p2->w;
    }
  }

  if (list_empty(symbstack))
    p = (minsize > size)?pmin:pmax;
  else
  {
    double w = g_random_double_range (0, wmax);

    //Very unlikely, but not impossible.
    if (w == 0)
      w = wmax;

    while (w > 0)
    {
      p = list_current (symbstack);
      list_pop_head (symbstack);
      w -= p->w;
    }
  }
  list_delete (symbstack);

  //A fitting production has been choosen, now we pick sizes for its subrules.
  if (p->nbrules == 0)
    return term_copy(p->term);

  size -= term_part_size (p->term);

  unsigned* rminsizes = g_malloc0 (p->nbrules * sizeof(*rminsizes));
  unsigned* rmaxsizes = g_malloc0 (p->nbrules * sizeof(*rmaxsizes));
  unsigned rminsum = 0, rmaxsum = 0;

  for (unsigned i = 0 ; i < p->nbrules ; i++)
  {
    rule* r2 = g->rules[p->rules_id[i]];
    rminsizes[i] = -1;
    rmaxsizes[i] = 0;
    for (int j = 0 ; j < r2->nbprods ; j++)
    {
      p2 = r2->prods[j];
      rminsizes[i] = MIN(p2->minsize, rminsizes[i]);
      rmaxsizes[i] = MAX(p2->maxsize, rmaxsizes[i]);
    }
    rminsum += rminsizes[i];
    if (rmaxsizes[i] == -1 || rmaxsum == -1)
      rmaxsum = -1;
    else
      rmaxsum += rmaxsizes[i];
  }

  term** subterms = g_malloc0 (p->nbrules * sizeof(*subterms));
  unsigned* sr_sizes = g_malloc0 (p->nbrules * sizeof(*sr_sizes));

  if (rminsum < size && rmaxsum > size)
    pick_subrules_sizes (rminsizes, rmaxsizes, sr_sizes, p->nbrules, size);
  else if (rminsum >= size)
    pick_subrules_sizes (rminsizes, rmaxsizes, sr_sizes, p->nbrules, rminsum);
  else if (rmaxsum <= size)
    pick_subrules_sizes (rminsizes, rmaxsizes, sr_sizes, p->nbrules, rmaxsum);
  else assert(FALSE); //Nope. Shouldn't happen.

  for (unsigned i = 0 ; i < p->nbrules ; i++)
    subterms[i] = rule_to_term (g, g->rules[p->rules_id[i]], sr_sizes[i]);
  g_free (sr_sizes);

  term* t = term_copy(p->term);
  term_part_merge (&t, subterms);

  g_free (rminsizes);
  g_free (rmaxsizes);
  g_free (subterms);

  return t;
}

void grammar_to_term (gen* g)
{
  grammar* gr = (grammar*) g->grammar;
  term* t = rule_to_term (gr, gr->rules[gr->axiom], g->nleaves);
  term_prettify (t);  //The generation method being really dirty, we need this.
  g->term = t;
  g->tok = true;
}
