/* grammar.h */

#ifndef GRAMMAR_H
#define GRAMMAR_H

#include <glib.h>
#include <stdbool.h>

#include "list.h"
#include "term.h"

typedef struct grammar grammar;
typedef struct rule rule;
typedef struct prod prod;

//Create a new production with an associated term, a set of corresponding
//  subrules and a weight in picking probability.
extern prod* prod_create (term* term, unsigned* non_term, unsigned ntsize,
  double w);
//Delete a production and free associated memory.
extern void prod_delete (prod* p);
//Give a string description of a production's term.
extern gchar* prod_term_desc (grammar* g, prod* p);
//Get a productions's relative probability of picking.
extern double prod_get_weight (prod* p);

//Create a new rule with a name and a set of possible productions.
extern rule* rule_create (char* name, prod** prods, unsigned nbprods);
//Delete a rule and free associated memory.
extern void rule_delete (rule* r);
//Get a rule's name.
extern char* rule_name (rule* r);
//Get the possible productions count of a rule.
extern unsigned rule_prod_count (rule* r);
//Get production number p in a rule.
extern prod* rule_get_prod (rule* r, unsigned p);

//Create a new grammar with a complete set of rules and an axiom.
//Will return NULL if the given grammar has an infinite loop.
extern grammar* grammar_create (unsigned axiom, rule** rules, unsigned nbrules);
//Delete a grammar and free associated memory.
extern void grammar_delete (grammar* g);
//Get the total rule count of a grammar.
extern unsigned grammar_rule_count (grammar* g);
//Get rule number r in grammar.
extern rule* grammar_get_rule (grammar* g, unsigned r);
//Get the internal id of the grammar's axiom.
extern unsigned grammar_get_axiom (grammar* g);

//Generate a grammar-induced term through generation parameters.
extern void grammar_to_term (gen* g);

#endif /* GRAMMAR_H */
