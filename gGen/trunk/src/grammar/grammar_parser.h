/* grammar_parser.h */

#ifndef GRAMMAR_PARSER_H
#define GRAMMAR_PARSER_H

#include <stdio.h>

#ifndef TERM_PARSER_H
extern int fileno (FILE * fd);
#endif // TERM_PARSER_H

#include "list.h"
#include "grammar.h"

extern int grammar_lex (void);
extern int grammar_parse (void);
extern int grammar_error (char *s);

extern void clear_grammar_in(void);

extern FILE *grammar_in;
extern grammar* grammar_parsed;
extern unsigned grammar_cwd;
extern char *grammar_emsg;
extern list *grammar_token_stack;
extern int grammar_line;
extern int grammar_col;

#endif // GRAMMAR_PARSER_H
