%{
#include <glib.h>
#include <string.h>

#include "grammar.tab.c"

#define GRAMMAR_TAB 8

  list *grammar_token_stack;

  int grammar_line = 1;
  int grammar_col  = 0;
%}

%option noyywrap
%option nounput

ID              (([1-9][0-9]*)|0)
NON_TERMINAL    [A-Z]+

%%

"//".*     { /*ignor comments */ }

"axiom"    { grammar_col += 5; return AXIOM; }

"add"/"_"  { grammar_col += 3; return ADD; }
"ren"/"_"  { grammar_col += 3; return REN; }
"oplus"    { grammar_col += 5; return OPL; }

{ID} {
  grammar_col += yyleng;
  yylval.string = g_strdup(yytext);
  list_push_head(grammar_token_stack, yylval.string);
  return IDENTIFIER;
}

{ID}/"_" {
  grammar_col += yyleng;
  yylval.string = g_strdup(yytext);
  list_push_head(grammar_token_stack, yylval.string);
  return LABEL_1;
}

{NON_TERMINAL} {
  grammar_col += yyleng;
  yylval.string = g_strdup(yytext);
  list_push_head(grammar_token_stack, yylval.string);
  return NON_TERMINAL;
}

"_"/{ID} { grammar_col++; return yytext[0]; }

"("    { grammar_col++; return yytext[0]; }
")"    { grammar_col++; return yytext[0]; }
","    { grammar_col++; return yytext[0]; }

"|"    { grammar_col++; return yytext[0]; }
":"    { grammar_col++; return yytext[0]; }
";"    { grammar_col++; return yytext[0]; }

[ \v\f]  { grammar_col++; }
"\t"     { grammar_col += GRAMMAR_TAB - (grammar_col % GRAMMAR_TAB); }
"\n"     { grammar_col = 0; grammar_line++; }
.        { return yytext[0]; }

%%

void
clear_grammar_in(void) {
  while (input() != EOF)
    ;
}
