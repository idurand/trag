%{
#include <stdbool.h>

#include "grammar_parser.h"

#include "term.h"
#include "error.h"
#include "grammar_tools.h"

  void error(char *func, char *label);
  grammar *grammar_parsed;
  unsigned grammar_cwd;
  char *grammar_emsg;
  static err_t grammar_ecode;
  static list *term_stack;
  static unsigned color = 1;

  static void free_term_g(void *o);
%}

%union {
  char *string;
  rule *rule;
  prod *prod;
  grammar *grammar;
  term *term;
}

%token <string> ADD REN OPL
%token <string> IDENTIFIER LABEL_1
%token <string> NON_TERMINAL AXIOM

%type <rule> rule
%type <prod> prod
%type <grammar> grammar

%type <term> term

%start file

%%

file
: init_grammar grammar {
  grammar_parsed = $2;
  free_tools();
  g_free(non_term);
  list_delete(term_stack);
  list_delete(grammar_token_stack);
  YYACCEPT;
 }
;

init_grammar
: {
  grammar_ecode = SYN_ERR;
  grammar_parsed = NULL;
  grammar_emsg = NULL;
  grammar_cwd = 0;

  init_tools();
  init_non_term();
  term_stack = list_create();
  grammar_token_stack = list_create();
  list_define_free(grammar_token_stack, g_free);
}
;

grammar
: AXIOM ':' NON_TERMINAL ';' rules_list {
    rules_tab = g_realloc(rules_tab, nb_rules * sizeof (*rules_tab));
    unsigned id = non_term_id($3);
    list_pop_head(grammar_token_stack); // pop $3

    if (id > nb_rules || rules_tab[id] == NULL) {
      grammar_ecode = GRAMMAR_AXIOM;
      grammar_error("axiom error");
      YYABORT;
    }

    if (nb_rules != nb_rule_name) {
      grammar_ecode = GRAMMAR_RULE_NB;
      grammar_error("rule error");
      YYABORT;
    }

    $$ = grammar_create(id, rules_tab, nb_rules);
    rules_tab = NULL;
 }
| AXIOM error { YYABORT; }
;

rules_list
: rule { init_rules_tab(); add_rule($1); }
| rules_list rule {
  if (!add_rule($2)) {
    grammar_ecode = GRAMMAR_MULT_DEF;
    grammar_error("rule error");
    YYABORT;
  }
 }
;

rule
: NON_TERMINAL ':' prods_list ';' {
  prods_tab = g_realloc(prods_tab, nb_prods * sizeof (*prods_tab));
  $$ = rule_create(g_strdup($1), prods_tab, nb_prods);
  list_pop_head(grammar_token_stack); // pop $1
  prods_tab = NULL;
 }
| error { YYABORT; }
;

prods_list
: prod { init_prods_tab(); add_prod($1); }
| prods_list '|' prod { add_prod($3); }
| prods_list error { YYABORT; }
;

prod
: term {
  list_pop_head(term_stack); // pop $1
  $$ = prod_create($1, non_term, nb_non_term, 1.0); //TODO: Change this.
  init_non_term();
 }
;

term
: ADD '_' LABEL_1 '_' IDENTIFIER '(' term ')' {
  unsigned lbl1 = atol($3);
  unsigned lbl2 = atol($5);

  grammar_cwd = MAX(lbl1, grammar_cwd);
  grammar_cwd = MAX(lbl2, grammar_cwd);

  if (lbl1 == lbl2) {
    grammar_ecode = TERM_ADD;
    grammar_error("term error");
    YYABORT;
  } else
    $$ = term_create(EDGE, $7, lbl1, lbl2, color++);

  list_pop_head(term_stack); // pop $7
  list_push_head(term_stack, $$);

  list_pop_head(grammar_token_stack); // pop $3
  list_pop_head(grammar_token_stack); // pop $5
 }
| REN '_' LABEL_1 '_' IDENTIFIER '(' term ')' {
  unsigned lbl1 = atol($3);
  unsigned lbl2 = atol($5);

  grammar_cwd = MAX(lbl1, grammar_cwd);
  grammar_cwd = MAX(lbl2, grammar_cwd);

  if (lbl1 == lbl2) {
    grammar_ecode = TERM_REN;
    grammar_error("term error");
    YYABORT;
  } else
    $$ = term_create(RENAME, $7, lbl1, lbl2);

  list_pop_head(term_stack); // pop $7
  list_push_head(term_stack, $$);

  list_pop_head(grammar_token_stack); // pop $3
  list_pop_head(grammar_token_stack); // pop $5
 }
| OPL '(' term ',' term ')' {
  $$ = term_create(OPLUS, $3, $5);

  list_pop_head(term_stack); // pop $3
  list_pop_head(term_stack); // pop $5
  list_push_head(term_stack, $$);
  }
| IDENTIFIER {
  unsigned label = atol($1);
  grammar_cwd = MAX(label, grammar_cwd);

  $$ = term_create(LABEL, label, 0);

  list_pop_head(grammar_token_stack); // pop $1
  list_push_head(term_stack, $$); }
| NON_TERMINAL {
  $$ = term_create(LABEL_NT, non_term_id($1));
  add_non_term($1);

  list_pop_head(grammar_token_stack); // pop $1
  list_push_head(term_stack, $$); }
| error { YYABORT; }
;

%%

static void
free_term_g(void *o) {
  term_delete_rec((term *) o);
}

int
grammar_error(char *s) {
  grammar_emsg = g_strdup_printf("%s:line %d%s\n", s, grammar_line,
				 err_msg(grammar_ecode));

  clear_grammar_in();
  grammar_parsed = NULL;

  if (rules_tab != NULL) {
    for (int i = 0; i < nb_rules; i++)
      if (rules_tab[i] != NULL)
	rule_delete(rules_tab[i]);
    g_free(rules_tab);
  }

  if (prods_tab != NULL) {
    for (int i = 0; i < nb_prods; i++)
      prod_delete(prods_tab[i]);
    g_free(prods_tab);
  }

  free_tools();
  g_free(non_term);
  list_define_free(term_stack, free_term_g);
  list_delete(term_stack);
  list_delete(grammar_token_stack);

  return EXIT_FAILURE;
}
