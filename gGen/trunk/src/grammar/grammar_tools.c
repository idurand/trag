/* grammar_tools.c */

#include <glib.h>
#include <string.h>

#include "grammar_tools.h"

#define DEFAULT_SIZE 8

static char **rule_name_tab;
static unsigned max_rule_name;

static unsigned max_non_term;
static unsigned max_prods;
static unsigned max_rules;

unsigned nb_rule_name;
unsigned nb_non_term;
unsigned nb_prods;
unsigned nb_rules;

unsigned *non_term;
prod **prods_tab;
rule **rules_tab;

void
init_tools(void) {
  nb_rule_name = 0;
  max_rule_name = DEFAULT_SIZE;
  rule_name_tab = g_malloc(max_rule_name * sizeof (*rule_name_tab));
}

void
free_tools(void) {
  for (int i = 0; i < nb_rule_name; i++)
    g_free(rule_name_tab[i]);
  g_free(rule_name_tab);
}

void
init_prods_tab(void) {
  nb_prods = 0;
  max_prods = DEFAULT_SIZE;
  prods_tab = g_malloc(max_prods * sizeof (*prods_tab));
}

void
add_prod(prod *p) {
  if (nb_prods == max_prods) {
    max_prods *= 2;
    prods_tab = g_realloc(prods_tab, max_prods * sizeof (*prods_tab));
  }
  prods_tab[nb_prods++] = p;
}

void
init_rules_tab(void) {
  nb_rules = 0;
  max_rules = DEFAULT_SIZE;
  rules_tab = g_malloc0(max_rules * sizeof (*rules_tab));
}

bool
add_rule(rule *r) {
  unsigned id = non_term_id(rule_name(r));

  if (id >= max_rules) {
    unsigned old_size = max_rules;
    max_rules *= 2;
    rules_tab = g_realloc(rules_tab, max_rules * sizeof (*rules_tab));
    memset(rules_tab + old_size, 0, old_size);
  }

  if (rules_tab[id] != NULL)
    return false;

  rules_tab[id] = r;
  nb_rules++;

  return true;
}

void
init_non_term(void) {
  nb_non_term = 0;
  max_non_term = DEFAULT_SIZE;
  non_term = g_malloc(max_non_term * sizeof (*non_term));
}

void
add_non_term(char *name) {
  unsigned id = non_term_id(name);

  if (nb_non_term == max_non_term) {
    max_non_term *= 2;
    non_term = g_realloc(non_term, max_non_term * sizeof (*non_term));
  }
  non_term[nb_non_term++] = id;
}

unsigned
non_term_id(char *name) {
  for (unsigned i = 0; i < nb_rule_name; i++)
    if (strcmp(name, rule_name_tab[i]) == 0)
      return i;

  if (nb_rule_name == max_rule_name) {
    max_rule_name *= 2;
    rule_name_tab = g_realloc(rule_name_tab, max_rule_name *
			      sizeof (*rule_name_tab));
  }

  rule_name_tab[nb_rule_name] = g_strdup(name);

  return nb_rule_name++;
}
