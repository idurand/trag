/* grammar_tools.h */

#ifndef GRAMMAR_TOOLS_H
#define GRAMMAR_TOOLS_H

#include <stdbool.h>

#include "grammar.h"

extern unsigned nb_rule_name;
extern unsigned nb_non_term;
extern unsigned nb_prods;
extern unsigned nb_rules;

extern unsigned *non_term;
extern prod **prods_tab;
extern rule **rules_tab;

extern void init_tools(void);

extern void free_tools(void);

extern void init_prods_tab(void);

extern void add_prod(prod *p);

extern void init_rules_tab(void);

extern bool add_rule(rule *r);

extern void init_non_term(void);

extern void add_non_term(char *name);

extern unsigned non_term_id(char *name);

#endif // GRAMMAR_TOOLS_H
