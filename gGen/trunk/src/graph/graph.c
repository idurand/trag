/* graph.c */

#include <glib.h>
#include <assert.h>

#include "graph.h"
#include "utils.h"
#include "properties.h"

struct graph
{
  list **labels;
  unsigned nb_vertices;
  unsigned cwd;
  char ***properties;
  unsigned max_colors;
  char **colors;
};

static void
vertex_free (void *o)
{
  vertex_delete ((vertex *) o);
}

graph *
graph_create (unsigned cwd)
{
  graph *g = g_malloc0 (sizeof (*g));

  g->labels = g_malloc0 (cwd * sizeof (*(g->labels)));
  for (unsigned i = 0; i < cwd; i++)
    {
      g->labels[i] = list_create ();
      list_define_free (g->labels[i], vertex_free);
    }
  g->nb_vertices = 0;
  g->cwd = cwd;
  g->properties = NULL;
  g->max_colors = 1;
  g->colors = NULL;

  return g;
}

void
graph_delete (graph * g)
{
  assert (g != NULL);
  if (g->properties != NULL)
    properties_delete (g->properties);

  if (g->colors != NULL) {
    for (unsigned i = 0; i < g->max_colors; i++)
      g_free(g->colors[i]);
    g_free(g->colors);
  }

  for (unsigned i = 0; i < g->cwd; i++)
    list_delete (g->labels[i]);
  g_free (g->labels);
  g_free (g);
}

bool
graph_empty (graph * g)
{
  assert (g != NULL);
  return g->nb_vertices == 0;
}

unsigned
graph_nb_vertices (graph * g)
{
  assert (g != NULL);
  return g->nb_vertices;
}

unsigned
graph_cwd(graph *g) {
  assert (g != NULL);
  return g->cwd;
}

void
graph_foreach (graph* g, void (*f) (void*, void*), void* data)
{
  assert (g != NULL && f != NULL);

  for (int i = 0; i < g->cwd; i++)
  {
    list* l = g->labels[i];
    for (list_goto_head (l); !list_is_end (l); list_goto_next (l))
      f(list_current (l), data);
  }
}

char ***
graph_properties (graph * g)
{
  assert (g != NULL);

  if (g->properties == NULL)
    g->properties = properties_results(g);

  return g->properties;
}

char **
graph_colors(graph *g) {
  assert (g != NULL);

  if (g->colors == NULL)
    g->colors = create_colors(&(g->max_colors));

  return g->colors;
}

graph *
graph_make_oplus (graph * g1, graph * g2)
{
  assert (g1 != g2 && g1 != NULL && g2 != NULL);

  if (g1->cwd < g2->cwd)
    return graph_make_oplus (g2, g1);

  unsigned max_cwd = g1->cwd;
  graph *gn = g_malloc0 (sizeof (*gn));

  gn->cwd = max_cwd;
  gn->properties = NULL;
  gn->colors = NULL;
  gn->max_colors = g1->max_colors + g2->max_colors;
  gn->nb_vertices = g1->nb_vertices + g2->nb_vertices;
  gn->labels = g_malloc0 (max_cwd * sizeof (*(gn->labels)));
  for (unsigned i = 0; i < g1->cwd; i++)
    gn->labels[i] = g1->labels[i];

  for (unsigned i = 0; i < g2->cwd; i++)
    {
      list_append (gn->labels[i], g2->labels[i]);
      list_delete (g2->labels[i]);
    }

  g_free (g1->labels);
  g_free (g2->labels);
  g_free (g1);
  g_free (g2);

  return gn;
}

void
graph_add_vertex (graph * g, unsigned label, unsigned id)
{
  assert (g != NULL && label < g->cwd);

  vertex *v = vertex_create (id);
  list_push_head (g->labels[label], v);
  g->nb_vertices++;
}

void
graph_merge_labels (graph * g, unsigned label_1, unsigned label_2)
{
  assert (label_1 != label_2);

  if (!graph_good_rename (g, label_1))
    return;

  if (label_2 >= g->cwd)
    {
      g->labels =
	g_realloc (g->labels, (label_2 + 1) * sizeof (*(g->labels)));
      for (int i = g->cwd; i < label_2 + 1; i++)
	{
	  g->labels[i] = list_create ();
	  list_define_free (g->labels[i], vertex_free);
	}
      g->cwd = label_2 + 1;
    }

  list_append (g->labels[label_2], g->labels[label_1]);
}

struct link_args {
  vertex **tab;
  unsigned color;
};

static void
make_links_aux(gpointer data, gpointer user_data) {
  vertex *v = (vertex *) data;

  struct link_args *args = (struct link_args *) user_data;
  vertex **tab = args->tab;
  unsigned color = args->color;

  for (unsigned i = 0; tab[i] != NULL; i++)
    vertex_add_neighbor (v, tab[i], color);
}

static void
make_links (graph * g, bool oriented, unsigned label_1, unsigned label_2,
	    unsigned color, unsigned nbthreads)
{
  g->max_colors = MAX(g->max_colors, color);
  if (!graph_good_edge (g, label_1, label_2))
    return;

  vertex **t1 = (vertex **) list_to_table(g->labels[label_1]);
  vertex **t2 = (vertex **) list_to_table(g->labels[label_2]);

  struct link_args args1 = { t1, color };
  struct link_args args2 = { t2, color };
  
  GThreadPool *pool;
  pool = g_thread_pool_new (make_links_aux, &args2, nbthreads, FALSE, NULL);
  for (unsigned i = 0; t1[i] != NULL; i++)
    g_thread_pool_push(pool, t1[i], NULL);
  g_thread_pool_free(pool, FALSE, TRUE);

  if (!oriented) {
    pool = g_thread_pool_new (make_links_aux, &args1, nbthreads, FALSE, NULL);
    for (unsigned i = 0; t2[i] != NULL; i++)
      g_thread_pool_push(pool, t2[i], NULL);
    g_thread_pool_free(pool, FALSE, TRUE);
  }
}

void
graph_make_arcs (graph * g, unsigned label_1, unsigned label_2, unsigned color,
		 unsigned nbt)
{
  assert (g != NULL);
  make_links (g, true, label_1, label_2, color, nbt);
}

void
graph_make_edges (graph * g, unsigned label_1, unsigned label_2, unsigned color,
		  unsigned nbt)
{
  assert (g != NULL);
  make_links (g, false, label_1, label_2, color, nbt);
}

graph *
graph_make_clean_oplus (graph * g1, graph * g2)
{
  assert (g1 != g2 && g1 != NULL && g2 != NULL);

  if (g1->cwd < g2->cwd)
    return graph_make_oplus (g2, g1);

  graph *gn = g_malloc0 (sizeof (*gn));

  gn->cwd = g1->cwd;
  gn->properties = NULL;
  gn->colors = NULL;
  gn->max_colors = 1;
  gn->nb_vertices = g1->nb_vertices;
  gn->labels = g_malloc0 (g1->cwd * sizeof (*(gn->labels)));
  for (unsigned i = 0; i < g1->cwd; i++)
    gn->labels[i] = g1->labels[i];

  for (unsigned i = 0; i < g2->cwd; i++)
    {
      if (!list_empty (g2->labels[i]) && list_empty (gn->labels[i]))
        graph_add_vertex (gn, i, i);
      list_delete (g2->labels[i]);
    }

  g_free (g1->labels);
  g_free (g2->labels);
  g_free (g1);
  g_free (g2);

  return gn;
}

bool
graph_good_edge (graph * g, unsigned label_1, unsigned label_2)
{
  assert (g != NULL);
  return label_1 < g->cwd && label_2 < g->cwd &&
    !list_empty (g->labels[label_1]) && !list_empty (g->labels[label_2]);
}

bool
graph_good_rename (graph * g, unsigned label)
{
  assert (g != NULL);
  return label < g->cwd && !list_empty (g->labels[label]);
}

void
graph_end_creation (graph * g)
{
  assert (g != NULL);
  for (int i = 1; i < g->cwd; i++)
    list_append (g->labels[0], g->labels[i]);
}

static vertex *
graph_vertex(graph *g, unsigned id) {
  for (int i = 0; i < g->cwd; i++) {
    list *l = g->labels[i];

    if (!list_empty(l))
      for (list_goto_head (l); !list_is_end (l); list_goto_next (l))
	if (vertex_id ((vertex *) list_current (l)) == id)
	  return (vertex *) list_current (l);
  }

  return NULL;
}

static char *
node_to_dot(graph *g, graph *sg) {
  char *s = g_strdup ("");
  char *s_tmp = s;

  for (int i = 0; i < g->cwd; i++) {
    list *l = g->labels[i];

    if (!list_empty(l))
      {
	char *label = label_to_string(i);
	for (list_goto_head (l); !list_is_end (l); list_goto_next (l))
	  {
 	    unsigned v_id = vertex_id ((vertex *) list_current (l));
 	    char *color = (g == sg || graph_vertex(sg, v_id) != NULL)
	      ? FG_COLOR : BG_COLOR;
 	    s = g_strdup_printf ("%s\n  %d [ label=\"%d: %s\", color=\"%s\" ]",
 				 s, v_id, v_id, label, color);
 	    g_free (s_tmp);
 	    s_tmp = s;
	  }
	g_free(label);
      }
  }

  return s;
}

static char *
edge_to_dot(graph *g, graph *sg, char **colors) {
  char *s = g_strdup("");
  char *s_tmp = s;

  for (int i = 0; i < g->cwd; i++) {
    list *l = g->labels[i];

    if (!list_empty(l))
      for (list_goto_head (l); !list_is_end (l); list_goto_next (l))
	{
	  vertex *v = (vertex *) list_current (l);
	  vertex *w = (g == sg) ? v : graph_vertex(sg, vertex_id(v));
	  char *v_tmp = vertex_to_dot (v, w, colors);
	  s = g_strdup_printf ("%s%s", s, v_tmp);
	  g_free (v_tmp);
	  g_free (s_tmp);
	  s_tmp = s;
	}
  }

  return s;
}

char *
graph_to_dot (graph * g)
{
  return subgraph_to_dot(g, g);
}

char *
subgraph_to_dot (graph *g, graph *sg) {
  char *nodes = node_to_dot(g, sg);

  char **colors = graph_colors(sg);
  char *edges = edge_to_dot(g, sg, colors);

  char *s = g_strdup_printf ("graph G {\n  node [ style=\"filled\" ]%s%s\n}\n",
			     nodes, edges);
  g_free (nodes);
  g_free (edges);

  return s;
}
