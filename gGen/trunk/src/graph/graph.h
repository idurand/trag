/* graph.h */

#ifndef GRAPH_H
#define GRAPH_H

#include <stdbool.h>

#include "vertex.h"

typedef struct graph graph;

// Create a new graph
extern graph *graph_create (unsigned cwd);

// Free memory allocated through graph_create
extern void graph_delete (graph * g);

// Return true if and only if the graph is empty
extern bool graph_empty (graph * g);

// Return the number of vertices of a graph
extern unsigned graph_nb_vertices (graph * g);

// Return the clique width of a graph
extern unsigned graph_cwd(graph *g);

// Apply a function to all the vertices of a graph
extern void graph_foreach (graph* g, void (*f) (void*, void*), void* data);

// Return the properties of a graph
extern char ***graph_properties (graph * g);

// Return a table of colors used for displaying edges ; do not free or modify
extern char **graph_colors(graph *g);

// Return a new graph which is the union of g1 and g2
// g1 and g2 are deleted
extern graph *graph_make_oplus (graph * g1, graph * g2);

// Add a vertex to a graph
extern void graph_add_vertex (graph * g, unsigned label, unsigned id);

// Move all vertices from label_1 to label_2
extern void graph_merge_labels (graph * g, unsigned label_1,
				unsigned label_2);

// Create arcs from all vertices of label_1 to all vertices of label_2
extern void graph_make_arcs (graph * g, unsigned label_1, unsigned label_2,
			     unsigned nbt, unsigned color);

// Create edges between all vertices of label_1 and label_2
extern void graph_make_edges (graph * g, unsigned label_1, unsigned label_2,
			      unsigned nbt, unsigned color);

// Return a new graph with all labels of g1 and g2 represented
// g1 and g2 are deleted
extern graph *graph_make_clean_oplus (graph * g1, graph * g2);

// Return true if the EDGE operation is useful
extern bool graph_good_edge (graph * g, unsigned label_1, unsigned label_2);

// Return true if the RENAME operation is useful
extern bool graph_good_rename (graph * g, unsigned label);

// Move all vertices to the first label
extern void graph_end_creation (graph * g);

// Export a graph to the .dot format
extern char *graph_to_dot (graph * g);

// Export a subgraph and its graph to the .dot format
extern char *subgraph_to_dot (graph *g, graph *sg);

#endif // GRAPH_H
