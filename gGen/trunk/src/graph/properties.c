/* properties.c */

#include <glib.h>
#include <string.h>

#include "properties.h"

#define NB_PROP 8

struct property
{
  char* (*test) (char* g);
  char* (*test_intern) (graph* g);
  char* name;
};

static unsigned prop_count;
static prop** props;

//pmutex ensures that the NB_PROP first properties are tested all at once.
//This is necessary because they are partly hard-coded in test_DFS.
static GMutex* pmutex;


struct DFS_data
{
  unsigned nb_vertices;
  bool connected;
  bool acyclic;
  bool colorable_2;
  unsigned d_min;
  unsigned d_max;
  unsigned connected_parts;
  bool* seen;      // table of visited vertices
  bool* color;     // table used for the 2 coloration
};

//Convert a final DFS_data struct to a human-readable string table.
void DFS_results (struct DFS_data* data, char** results)
{
  results[0] = g_strdup(data->connected?"yes":"no");
  results[1] = g_strdup(data->acyclic?"yes":"no");
  if (data->d_min == data->d_max)
  {
    results[2] = g_strdup("yes");
    results[3] = g_strdup((data->d_min == data->nb_vertices-1)?"yes":"no");
    results[4] = g_strdup((data->d_min == 0)?"yes":"no");
  }
  else
  {
    results[2] = g_strdup("no");
    results[3] = g_strdup("no");
    results[4] = g_strdup("no");
  }
  results[5] = g_strdup(data->colorable_2?"yes":"no");
  results[6] = g_strdup_printf("%u", data->d_max);
  results[7] = g_strdup_printf("%u", data->d_min);
}

//Auxiliary depth-first search function.
static void DFS_aux (vertex* ancestor, vertex* current, struct DFS_data* data)
{
  list* adj = vertex_adj_list (current);

  for (list_goto_head(adj) ; !list_is_end(adj) ; list_goto_next(adj))
  {
    vertex* son = list_current (adj);

    unsigned ancestor_id = vertex_id(ancestor);
    unsigned current_id = vertex_id(current);
    unsigned son_id = vertex_id(son);

    // TODO : risque de seg fault (id > nb_vertices) ???
    if (data->seen[son_id])
    {
      if (ancestor_id != son_id)
      data->acyclic = false;
      data->colorable_2 &= data->color[son_id] ^ data->color[current_id];
      // Trulies.
      continue;
    }

    data->color[son_id] = !(data->color[current_id]);
    data->seen[son_id] = true;
    DFS_aux (current, son, data);
  }
  list_delete(adj);
}

//Run a depth-first search from vertex o.
void DFS_vertex (void* o, void* DFS_d)
{
  vertex* v = (vertex*) o;
  struct DFS_data* data = (struct DFS_data*) DFS_d;
  unsigned v_id = vertex_id(v);
  int d = vertex_nb_neighbors(v);

  data->d_min = (d < data->d_min) ? d : data->d_min;
  data->d_max = (d > data->d_max) ? d : data->d_max;

  if (!(data->seen[v_id]))
  {
    data->seen[v_id] = true;
    if (d > 0)
      DFS_aux (v, v, data);
    data->connected_parts++;
  }
}

//Set up DFS data, run it for every vertex, then write down the results.
void DFS_run (graph* g, char** results)
{
  struct DFS_data* data = g_malloc0 (sizeof(*data));

  data->nb_vertices = graph_nb_vertices(g);

  data->connected = false;
  data->acyclic = true;
  data->colorable_2 = true;
  data->d_min = data->nb_vertices;
  data->d_max = 0;
  data->connected_parts = 0;

  data->seen = g_malloc0 (data->nb_vertices * sizeof(*(data->seen)));
  data->color = g_malloc0 (data->nb_vertices * sizeof(*(data->color)));

  if (!graph_empty(g))
    graph_foreach (g, DFS_vertex, data);

  data->connected = data->connected_parts < 2;

  DFS_results (data, results);

  g_free (data->seen);
  g_free (data->color);
  g_free (data);
}

//This function tests NB_PROP properties in one run to improve performance.
//Since results are staticly stored, it is absolutely NOT thread safe !
char* test_DFS (graph* g)
{
  static unsigned count = -1;
  static char** results = NULL;
  count++;
  count %= NB_PROP;
  if (count == 0) //if (!count) would've looked a bit dirty.
  {
    g_free (results);
    results = g_malloc0 (NB_PROP * sizeof(*results));
    DFS_run (g, results);
  }
  return results[count];
}

void init_properties ()
{
  prop_count = 0;
  props = g_malloc0 (prop_count * sizeof(*props));
  char* pnames[NB_PROP] = {"Connected", "Acyclic", "Delta-regular", "Complete",
    "Independent", "2-colorable", "Dmax", "Dmin"};
  for (unsigned p = 0 ; p < NB_PROP ; p++)
    add_intern_property (pnames[p], test_DFS);

  pmutex = g_mutex_new();
}

unsigned properties_count ()
{
  return prop_count;
}

prop* create_property (const char* name)
{
  prop* p = g_malloc0 (sizeof (*p));
  p->name = g_strdup (name);
  return p;
}

void add_property (const char* name, char* (*test) (char*))
{
  prop *p = create_property (name);
  p->test = test;
  p->test_intern = NULL;
  props = g_realloc (props, (++prop_count) * sizeof(*props));
  props[prop_count-1] = p;
}

void add_intern_property (const char* name, char* (*test) (graph*))
{
  prop* p = create_property (name);
  p->test = NULL;
  p->test_intern = test;
  props = g_realloc (props, (++prop_count) * sizeof(*props));
  props[prop_count-1] = p;
}

char* property_name (unsigned p)
{
  if (p >= prop_count)
    return NULL;

  return g_strdup(props[p]->name);
}

char*** properties_results (graph* g)
{
  char*** properties = g_malloc0 (prop_count * sizeof(*properties));
  char* dotgraph = NULL;

  //Static lock for NB_PROP tests. Confer to pmutex declaration for details.
  g_mutex_lock (pmutex);
  unsigned i;
  for (i = 0 ; i < prop_count ; i++)
  {
    if (i == NB_PROP)
      g_mutex_unlock(pmutex);

    properties[i] = g_malloc0 (2 * sizeof(*(properties[i])));

    properties[i][0] = property_name (i);

    if (props[i]->test_intern != NULL)
      properties[i][1] = props[i]->test_intern(g);
    else
    {
      if (dotgraph == NULL)
        dotgraph = graph_to_dot (g);

      properties[i][1] = props[i]->test(dotgraph);
    }
  }

  if (i == NB_PROP)
    g_mutex_unlock(pmutex);

  g_free (dotgraph);
  return properties;
}

void properties_delete (char*** props)
{
  for (unsigned i = 0 ; i < prop_count ; i++)
  {
    g_free (props[i][0]);
    g_free (props[i][1]);
    g_free (props[i]);
  }
  g_free (props);
}

bool properties_check(char*** found, p_wanted* wanted)
{
  bool ret = true;
  for (int i = 0 ; i < prop_count && ret == true ; i++)
    ret &= (!(wanted[i].yes)) || (strcmp(wanted[i].value, found[i][1]) == 0);

  return ret;
}
