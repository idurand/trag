#ifndef PROPERTIES_H
#define PROPERTIES_H

#include <stdbool.h>

#include "graph.h"

typedef struct property prop;

typedef struct wanted p_wanted;
struct wanted
{
  bool yes;
  char* value;
};

//Initializes several values required for the property module to work.
extern void init_properties ();

//Return the current number of properties.
extern unsigned properties_count ();

//Add a property with a given name, and a test function that takes a dot-format
//  graph and should return a human-readable property value after testing it.
extern void add_property (const char* name, char* (*test) (char*));

//Same purpose, but the test function takes the internal graph instead. This
//  will be an internal graph pointer, which should NOT be modified or freed.
extern void add_intern_property (const char* name, char* (*test) (graph*));

//Get the name of a property.
extern char* property_name (unsigned p);

//Return a matrix with the properties names/values for the graph g, like this :
//  [property1][response1]
//  [property2][response2]
//  ... ( lengths = [property_count()][2][strlen(matrix[i][j])] )
extern char*** properties_results (graph* g);

//Frees memory allocated through property_results.
extern void properties_delete (char*** props);

//Check if wanted properties are equals to found ones.
extern bool properties_check (char*** found, p_wanted* props);

#endif //PROPERTIES_H
