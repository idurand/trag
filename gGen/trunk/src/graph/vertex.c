/* vertex.c */

#include <assert.h>
#include <glib.h>

#include "vertex.h"
#include "utils.h"

/*
 * we use 'GTree' to emulate an adjacency list
 * it is faster to find and to insert without
 * duplicates and it does not take much more memory
 */
struct vertex
{
  GTree *neighbors;		// "adjacency list"
  unsigned id;
};

typedef struct edge edge;

struct edge {
  vertex *tail;
  unsigned color;
};

static edge *
edge_create(vertex *v, unsigned color) {
  edge *e = g_malloc(sizeof(*e));

  e->tail = v;
  e->color = color;

  return e;
}

static void
edge_delete(gpointer e) {
  g_free(e);
}

static int
vertex_cmp (gconstpointer a, gconstpointer b, gpointer c)
{
  int n = *((int *) a);
  int m = *((int *) b);

  return n - m;
}

static int
vertex_cmp_inv (gconstpointer a, gconstpointer b)
{
  return -vertex_cmp(a, b, NULL); // wtf ???
}

vertex *
vertex_create (unsigned id)
{
  vertex *v = g_malloc0 (sizeof (*v));

  v->id = id;
  v->neighbors = g_tree_new_full (vertex_cmp, NULL, NULL, edge_delete);

  return v;
}

void
vertex_delete (vertex * v)
{
  assert (v != NULL);
  g_tree_destroy (v->neighbors);
  g_free (v);
}

unsigned
vertex_id (vertex * v) {
  assert (v != NULL);
  return v->id;
}

unsigned
vertex_nb_neighbors (vertex * v)
{
  assert (v != NULL);
  return g_tree_nnodes (v->neighbors);
}

static gboolean
vetex_adj_list_traverse (gpointer a, gpointer b, gpointer c)
{
  list *l = (list *) c;
  vertex *v = ((edge *) b)->tail;
  list_push_head (l, v);
  return FALSE;
}

list *
vertex_adj_list (vertex * v)
{
  assert (v != NULL);
  list *l = list_create ();
  g_tree_foreach (v->neighbors, vetex_adj_list_traverse, l);
  return l;
}

void
vertex_add_neighbor (vertex * v, vertex * neighbor, unsigned color)
{
  assert (v != NULL);
  edge *e = edge_create(neighbor, color);

  if (g_tree_search(v->neighbors, vertex_cmp_inv, &(neighbor->id)) == NULL)
    g_tree_insert (v->neighbors, &(neighbor->id), e);
}

static bool
edge_exists(vertex *v, vertex *w) {
  return w != NULL &&
    g_tree_search(w->neighbors, vertex_cmp_inv, &(v->id)) != NULL;
}

struct vtd_args
{
  vertex *w;
  char **s;
  unsigned id;
  char **colors;
};

static gboolean
vetex_to_dot_traverse (gpointer a, gpointer b, gpointer c)
{
  edge *e = (edge *) b;
  struct vtd_args *args = (struct vtd_args *) c;
  assert (e != NULL);

  vertex *v = e->tail;
  if (v->id >= args->id)
    {
      char **s = args->s;
      char *tmp = *s;
      char *color = edge_exists(v, (vertex *) args->w)
	? args->colors[e->color] : g_strdup(BG_COLOR);
      *s = g_strdup_printf ("%s\n  %d -- %d [ color=\"%s\" ];", *s, args->id,
			    v->id, color);
      g_free (tmp);
    }

  return FALSE;
}

char *
vertex_to_dot (vertex * v, vertex *w, char **colors)
{
  assert (v != NULL);

  char *s = g_strdup ("");
  if (g_tree_nnodes (v->neighbors) == 0)
    return s;

  struct vtd_args args = { w, &s, v->id, colors };
  g_tree_foreach (v->neighbors, vetex_to_dot_traverse, &args);

  return s;
}
