/* vertex.h */

#include "list.h"

#ifndef VERTEX_H
#define VERTEX_H

typedef struct vertex vertex;

// a new vertex
extern vertex *vertex_create (unsigned id);

// Frees the memory allocated through vertex_create
extern void vertex_delete (vertex * v);

// return the identifier of a vertex
extern unsigned vertex_id (vertex * v);

// return the number of neighbors of a vertex
extern unsigned vertex_nb_neighbors (vertex * v);

// return the adjacency list of a vertex
extern list *vertex_adj_list (vertex * v);

// add a vertex to the neighbors of a vertex
extern void vertex_add_neighbor (vertex * v, vertex * neighbor,
				 unsigned color);

// export a vertex to the .dot format
extern char *vertex_to_dot (vertex * v, vertex *w, char **colors);

#endif /* VERTEX_H */
