/* term.c */

#include <glib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include "term.h"
#include "graph.h"
#include "utils.h"
#include "properties.h"

#define TYPE_LABEL " "
#define TYPE_RENAME " ren_"
#define TYPE_EDGE " add_"
#define TYPE_OPLUS " (+) UNION"

#define TERM_WITH_PROP_THREADS 8

struct term
{
  op type;         // The type (LABEL, EDGE, RENAME or OPLUS)
  term **subterm;  // The subterms (array of size arity(type))
  label *label;    // Other agruments (array of the argc(type))
  unsigned height;
  unsigned size;
};

//Return the argument count (label information) of a term node type.
static short
argc (op type)
{
  switch (type)
    {
    case EDGE :
      return 3;

    case LABEL :
    case RENAME :
      return 2;

    case LABEL_NT :
      return 1;

    case OPLUS :
      return 0;
    }

  assert(false);
  return -1;
}

//Return the number of subterms of a term node type
static short
arity (op type)
{
  switch (type)
    {
    case LABEL :
    case LABEL_NT :
      return 0;

    case RENAME :
    case EDGE :
      return 1;

    case OPLUS :
      return 2;
    }

  assert(false);
  return -1;
}

term *
term_create (op type, ...)
{
  va_list args;
  term *t = g_malloc0 (sizeof (*t));
  t->type = type;
  assert (type == LABEL || type == LABEL_NT || type == RENAME || type == EDGE ||
	  type == OPLUS);

  t->subterm = g_malloc0 (arity(type) * sizeof(*(t->subterm)));
  t->label = g_malloc0 (argc(type) * sizeof(*(t->label)));

  va_start (args, type);

  switch (type)
    {
    case LABEL:
      t->label[0] = va_arg (args, label);
      t->label[1] = va_arg (args, unsigned);
      t->height = 1;
      t->size = 1;
      break;

    case LABEL_NT :
      t->label[0] = va_arg (args, unsigned);
      t->height = 1;
      t->size = 1;
      break;

    case RENAME:
      t->subterm[0] = va_arg (args, term *);
      t->label[0] = va_arg (args, label);
      t->label[1] = va_arg (args, label);

      if (t->subterm[0] == NULL) {
	t->height = 1;
	t->size = 1;
      } else {
	t->height = t->subterm[0]->height + 1;
	t->size = t->subterm[0]->size + 1;
      }
      break;

    case EDGE:
      t->subterm[0] = va_arg (args, term *);
      t->label[0] = va_arg (args, label);
      t->label[1] = va_arg (args, label);
      t->label[2] = va_arg (args, unsigned);

      if (t->subterm[0] == NULL) {
	t->height = 1;
	t->size = 1;
      } else {
	t->height = t->subterm[0]->height + 1;
	t->size = t->subterm[0]->size + 1;
      }
      break;

    case OPLUS:
      t->subterm[0] = va_arg (args, term *);
      t->subterm[1] = va_arg (args, term *);
      if (t->subterm[1] == NULL) {
	t->height = t->subterm[0]->height + 1;
	t->size = t->subterm[0]->size + 1;
      } else {
	t->height = MAX(t->subterm[0]->height, t->subterm[1]->height) + 1;
	t->size = t->subterm[0]->size + t->subterm[1]->size + 1;
      }
      break;
    }

  va_end (args);
  return t;
}

void
term_delete (term * t)
{
  assert (t != NULL);
  g_free (t->subterm);
  g_free (t->label);
  g_free (t);
}

void
term_delete_rec (term * t)
{
  if (t == NULL)
    return;

  switch (t->type)
    {
    case EDGE:
    case RENAME:
      term_delete_rec (t->subterm[0]);
      break;

    case OPLUS:
      term_delete_rec (t->subterm[0]);
      term_delete_rec (t->subterm[1]);
      break;

    default :
      break;
    }
  term_delete (t);
}

short
term_arity (term * t)
{
  assert (t != NULL);
  return arity (t->type);
}

unsigned
term_size (term * t) {
  assert (t != NULL);
  return t->size;
}

unsigned
term_height (term * t) {
  assert (t != NULL);
  return t->height;
}

term *
term_son (term * t, unsigned son)
{
  assert (t != NULL && son < arity(t->type));
  return t->subterm[son];
}

unsigned
term_edge_color (term* t)
{
  assert (t != NULL);
  if (t->type == EDGE)
    return t->label[2];
  return -1;
}


op
term_type(term *t) {
  assert (t != NULL);
  return t->type;
}

static void
delete_add_or_rename(term * ancestor, term * current) {
  if (current == ancestor->subterm[0])  // fix the side if ancestor is OPLUS
    ancestor->subterm[0] = current->subterm[0];
  else
    ancestor->subterm[1] = current->subterm[0];
  term_delete (current);
}

static void
update_height_and_size(term *t) {
  switch (t->type) {
  case EDGE :
  case RENAME :
    t->height = t->subterm[0]->height + 1;
    t->size = t->subterm[0]->size + 1;
    break;

  case OPLUS :
    if (t->subterm[1] == NULL) {
      t->height = t->subterm[0]->height + 1;
      t->size = t->subterm[0]->size + 1;
    } else {
      t->height = MAX(t->subterm[0]->height, t->subterm[1]->height) + 1;
      t->size = t->subterm[0]->size + t->subterm[1]->size + 1;
    }
    break;

  default :
    break;
  }
}

static void
term_clean_root(term *ancestor, term *current) {
  switch (current->type) {
  case RENAME :
    delete_add_or_rename(ancestor, current);
    term_clean_root(ancestor, ancestor->subterm[0]);
    break;

  case OPLUS :
    term_clean_root(current, current->subterm[0]);
    term_clean_root(current, current->subterm[1]);
    break;

  case EDGE :
  case LABEL :
    break;

  default:
    assert(false);
  }

  update_height_and_size(ancestor);
}

static bool
is_good_pos(unsigned pos, unsigned cwd, unsigned *comb_line, unsigned *ren) {
  if (comb_line[pos] != -1 && comb_line[pos] == ren[pos]) {
    for (unsigned i = 0; i < cwd; i++)
      if (pos == ren[i])
	return false;

    return true;
  }

  return false;
}

static void term_clean_rename(term *t, int side, unsigned *ren, unsigned cwd);

static void
term_clean_cycle(term *t, int side, unsigned *ren, unsigned cwd) {
  unsigned *cycle = g_malloc0(cwd * sizeof (*cycle));
  bool *seen = g_malloc0(cwd * sizeof (*seen));
  unsigned length = 0;
  bool found = false;

  for (unsigned i = 0; i < cwd; i++) {
    memset(seen, false, cwd);
    length = 0;

    for (unsigned cur = i; ren[cur] != cur; cur = ren[cur]) {
      if (seen[cur]) {
	found = true;
	break;
      }
      
      cycle[length++] = cur;
      seen[cur] = true;
    }

    if (found)
      break;
  }

  assert(length != 0);

  unsigned *old_ren = g_malloc(cwd * sizeof (*old_ren));
  for (unsigned i = 0; i < cwd; i++) {
    old_ren[i] = (ren[i] != i) ? ren[i] : -1;
    if (seen[i])
      ren[i] = i;
  }

  term_clean_rename(t, side, ren, cwd);

  unsigned pivot;
  for (unsigned i = 0; i < cwd; i++)
    if (is_good_pos(i, cwd, old_ren, old_ren)) {
      pivot = i;
      break;
    }

  term *tmp = term_create(RENAME, t->subterm[side], cycle[0], pivot);
  tmp = term_create(RENAME, tmp, cycle[length - 1], cycle[0]);
  for (unsigned i = length - 2; i > 0; i--)
    tmp = term_create(RENAME, tmp, cycle[i], cycle[i + 1]);
  t->subterm[side] = term_create(RENAME, tmp, pivot, cycle[1]);

  g_free(old_ren);
  g_free(seen);
}

static void
term_clean_rename(term *t, int side, unsigned *ren, unsigned cwd) {
  unsigned **sort_by_rename = g_malloc(cwd * sizeof (*sort_by_rename));
  for (unsigned i = 0 ; i < cwd ; i++)
    sort_by_rename[i] = g_malloc0((cwd - 1) * sizeof (**sort_by_rename));

  unsigned *nb_sbr = g_malloc0(cwd * sizeof (*nb_sbr));
  unsigned *ren_cpy = g_malloc(cwd * sizeof (*ren_cpy));
  unsigned nb_ren = 0;

  for (int i = 0 ; i < cwd ; i++) {
    if (ren[i] != i) {
      unsigned col = ren[i];
      unsigned line = nb_sbr[col];
      sort_by_rename[col][line] = i;
      ren_cpy[i] = ren[i];
      nb_sbr[col]++;
      nb_ren++;
    } else
      ren_cpy[i] = -1;
  }

  if (nb_ren == 0) {
    for (int i = 0; i < cwd; i++)
      g_free(sort_by_rename[i]);
    g_free(sort_by_rename);
    g_free(ren_cpy);
    g_free(nb_sbr);
    return;
  }

  unsigned nb_combin = 1;
  for (unsigned i = 0 ; i < cwd ; i++)
    if (nb_sbr[i] > 1)
      nb_combin *= nb_sbr[i];

  unsigned nb_group = 1;
  unsigned **group_info = g_malloc(cwd * sizeof (*group_info));
  for (unsigned i = 0 ; i < cwd ; i++)
    group_info[i] = g_malloc0(2 * sizeof (**group_info));

  unsigned *group = g_malloc0(cwd * sizeof (*group));
  group_info[0][0] = nb_combin;

  for (unsigned i = 0; i < cwd ; i++)
    if (nb_sbr[i] > 1) {
      group_info[nb_group][0] = group_info[nb_group - 1][0] / nb_sbr[i];
      group_info[nb_group][1] = nb_sbr[i];
      for (unsigned j = 0; j < nb_sbr[i]; j++)
	group[sort_by_rename[i][j]] = nb_group;
      nb_group++;
    }

  unsigned **combinations = g_malloc(nb_combin * sizeof (*combinations));
  for (unsigned i = 0 ; i < nb_combin ; i++)
    combinations[i] = g_malloc0(cwd * sizeof (**combinations));

  for (unsigned col = 0; col < cwd ; col++)
    if (nb_sbr[ren[col]] == 0 || ren[col] == col)
      for (unsigned line = 0; line < nb_combin; line++)
	combinations[line][col] = -1;
    else if (nb_sbr[ren[col]] == 1)
      for (unsigned line = 0; line < nb_combin; line++)
	combinations[line][col] = ren[col];
    else
      for (unsigned line = 0; line < nb_combin; ) {
	unsigned gp_num = group[col];
	for (unsigned boss = 0; boss < group_info[gp_num][1] ; boss++)
	  for (unsigned i = 0; i < group_info[gp_num][0]; i++)
	    combinations[line++][col] = (col == sort_by_rename[ren[col]][boss])
	      ? ren[col] : sort_by_rename[ren[col]][boss];
      }

  term **ret = g_malloc0(nb_ren * sizeof (*ret));

  unsigned bad_line = 0;
  for (unsigned line = 0; line < nb_combin; line++) {
    unsigned count = 0;

    for (unsigned i = 0; i < nb_ren; i++) {
      for (unsigned pos = 0; pos < cwd ; pos++) {
	unsigned *comb_line = combinations[line];

	if (is_good_pos(pos, cwd, comb_line, ren_cpy)) {
	  ret[i] = term_create(RENAME, NULL, pos, comb_line[pos]);
	  count++;

	  for (unsigned j = 0 ; j < nb_sbr[ren[pos]] ; j++) {
	    unsigned tmp = sort_by_rename[ren[pos]][j];
	    if (tmp != pos && comb_line[tmp] != -1 &&
		ren[pos] == comb_line[pos])
	      comb_line[tmp] = pos;
	  }

	  for (unsigned j = 0; j < cwd ; j++)
	    if (comb_line[pos] == ren_cpy[j] && j != ren_cpy[j] &&
		comb_line[pos] == ren[pos])
	      ren_cpy[j] = pos;

	  comb_line[pos] = -1;

	  break;
	}
      }

      if (count != i + 1)
	break;
    }

    if (count == nb_ren)
      break;

    for (unsigned i = 0 ; i < count ; i++)
      term_delete(ret[i]);

    for (unsigned i = 0; i < cwd ; i++)
      ren_cpy[i] = (ren[i] != i) ? ren[i] : -1;

    bad_line++;
  }

  if (bad_line == nb_combin)
    term_clean_cycle(t, side, ren, cwd);
  else {
    for (unsigned i = 0; i < cwd ; i++)
      ren[i] = i;

    term *new = t->subterm[side];
    for (unsigned i = nb_ren - 1 ; i != -1; i--) {
      ret[i]->subterm[0] = new;
      ret[i]->height = new->height + 1;
      ret[i]->size = new->size + 1;
      new = ret[i];
    }

    t->subterm[side] = new;
  }

  for (unsigned i = 0; i < cwd; i++) {
    g_free(sort_by_rename[i]);
    g_free(group_info[i]);
  }

  for (unsigned i = 0; i < nb_combin; i++)
    g_free(combinations[i]);

  g_free(sort_by_rename);
  g_free(combinations);
  g_free(group_info);
  g_free(ren_cpy);
  g_free(nb_sbr);
  g_free(group);
  g_free(ret);
}

static graph *
term_clean_up_aux (bool *add, unsigned cwd, term * ancestor, term * current, unsigned *ren)
{
  graph *g, *left, *right;

  assert (current != NULL);

  switch (current->type)
    {
    case EDGE:
      g = term_clean_up_aux (add, cwd, current, current->subterm[0], ren);
      if (!graph_good_edge (g, current->label[0], current->label[1]) ||
	  !add[current->label[0] * cwd + current->label[1]])
	delete_add_or_rename(ancestor, current);
      else {
	// finding add_x_y prohibits add_x_y
	add[current->label[0] * cwd + current->label[1]] = false;

	term_clean_rename(current, 0, ren, cwd);

	current->height = current->subterm[0]->height + 1;
	current->size = current->subterm[0]->size + 1;
      }
      break;

    case RENAME:
      g = term_clean_up_aux (add, cwd, current, current->subterm[0], ren);
      if (!graph_good_rename (g, current->label[0]))
	delete_add_or_rename(ancestor, current);
      else {
	graph_merge_labels (g, current->label[0], current->label[1]);
	
	// finding ren_x_y allows add_y_z and add_z_y
	for (int i = 0; i < cwd; i++) {
	  add[current->label[1] * cwd + i] = true;
	  add[i * cwd + current->label[1]] = true;
	}

	for (int i = 0 ; i < cwd ; i++)
	  if (ren[i] == current->label[0])
	    ren[i] = current->label[1];

	delete_add_or_rename(ancestor, current);
      }
      break;

    case OPLUS:
      // diagonal does not need to be set to false add_x_x does not exist
      left  = term_clean_up_aux (add, cwd, current, current->subterm[0], ren);
      term_clean_rename(current, 0, ren, cwd);
      memset(add, true, cwd * cwd);

      right = term_clean_up_aux (add, cwd, current, current->subterm[1], ren);
      term_clean_rename(current, 1, ren, cwd);
      memset(add, true, cwd * cwd);

      current->height = MAX(current->subterm[0]->height, current->subterm[1]->height) + 1;
      current->size = current->subterm[0]->size + current->subterm[1]->size + 1;

      g = graph_make_clean_oplus (left, right);
      break;

    case LABEL:
      g = graph_create (current->label[0] + 1);
      graph_add_vertex (g, current->label[0], current->label[0]);
      break;

    default :
      assert(false);
    }

  update_height_and_size(ancestor);

  assert (g != NULL);
  return g;
}

static void
term_clean_up (term * ancestor, unsigned cwd) {
  bool *add = g_malloc(cwd * cwd * sizeof (bool));
  memset(add, true, cwd * cwd); 

  unsigned *ren = g_malloc(cwd * sizeof (*ren));
  for (int i = 0 ; i < cwd ; i++)
    ren[i] = i;

  graph *g = term_clean_up_aux(add, cwd, ancestor, ancestor->subterm[0], ren);
  graph_delete(g);
  g_free(ren);
  g_free(add);
}

term *
term_clean (term * t, unsigned cwd)
{
  term *ancestor = term_create (OPLUS, t, NULL);
  unsigned old_size;
  unsigned size = t->size;

  do {
    term_clean_root (ancestor, ancestor->subterm[0]);
    term_clean_up (ancestor, cwd);

    old_size = size;
    size = ancestor->subterm[0]->size;
  } while (size != old_size);

  term *ret = ancestor->subterm[0];
  term_delete (ancestor);

  return ret;
}

graph *
term_to_graph_threads (term *t, unsigned nbt)
{
  graph *g = NULL;

  assert (t != NULL);

  switch (t->type)
  {
    case EDGE:
      g = term_to_graph_threads (t->subterm[0], nbt);
      graph_make_edges (g, t->label[0], t->label[1], t->label[2], nbt);
      break;

    case RENAME:
      g = term_to_graph_threads (t->subterm[0], nbt);
      graph_merge_labels (g, t->label[0], t->label[1]);
      break;

    case OPLUS:
      g = graph_make_oplus (term_to_graph_threads (t->subterm[0], nbt),
			    term_to_graph_threads (t->subterm[1], nbt));
      break;

    case LABEL:
      g = graph_create (t->label[0] + 1);
      graph_add_vertex (g, t->label[0], t->label[1]);
      break;

    default:
      assert(FALSE);
  }

  assert (g != NULL);
  return g;
}

graph* term_to_graph (term *t)
{
  return term_to_graph_threads (t, MAX_THREADS);
}

gchar*
term_node_to_string (term* t)
{
  gchar* node_label;
  gchar *lstr0, *lstr1;

  assert (t != NULL);
  assert (t->type == LABEL || t->type == EDGE || t->type == RENAME ||
	  t->type == OPLUS);

  switch (t->type)
    {
    case LABEL:
      lstr0 = label_to_string (t->label[0]);
      node_label = g_strdup_printf ("%s%s: %d", TYPE_LABEL, lstr0, t->label[1]);
      g_free (lstr0);
      break;

    case RENAME:
      lstr0 = label_to_string (t->label[0]);
      lstr1 = label_to_string (t->label[1]);
      node_label = g_strdup_printf ("%s%s_%s", TYPE_RENAME, lstr0, lstr1);
      g_free (lstr0);
      g_free (lstr1);
      break;

    case EDGE:
      lstr0 = label_to_string (t->label[0]);
      lstr1 = label_to_string (t->label[1]);
      node_label = g_strdup_printf ("%s%s_%s", TYPE_EDGE, lstr0, lstr1);
      g_free (lstr0);
      g_free (lstr1);
      break;

    case OPLUS:
      node_label = g_strdup (TYPE_OPLUS);
      break;

    default :
      assert(false);
    }

  return node_label;
}

char *term_to_string_aux(term *t) {
  if (t == NULL)
    return g_strdup("");

  char *s;
  char *tmp_1;
  char *tmp_2;

  switch (t->type)
    {
    case EDGE:
      tmp_1 = term_to_string_aux (t->subterm[0]);
      s = g_strdup_printf ("add_%d_%d(%s)", t->label[0], t->label[1], tmp_1);
      g_free (tmp_1);
      break;

    case RENAME:
      tmp_1 = term_to_string_aux (t->subterm[0]);
      s = g_strdup_printf ("ren_%d_%d(%s)", t->label[0], t->label[1], tmp_1);
      g_free (tmp_1);
      break;

    case OPLUS:
      tmp_1 = term_to_string_aux (t->subterm[0]);
      tmp_2 = term_to_string_aux (t->subterm[1]);
      s = g_strdup_printf ("oplus(%s,%s)", tmp_1, tmp_2);
      g_free (tmp_1);
      g_free (tmp_2);
      break;

    case LABEL_NT:
      s = g_strdup_printf ("NT:%d", t->label[0]);
      break;

    case LABEL:
      s = g_strdup_printf ("%d", t->label[0]);
      break;
    }

  return s;
}

char* term_to_string (term* t, char ***prop) {
  char *p = g_strdup("");

  if (prop != NULL)
    for (int i = 0; i < properties_count(); i++) {
      char *tmp = p;
      p = g_strdup_printf("%s// %s : %s\n", p, prop[i][0], prop[i][1]);
      g_free(tmp);
    }

  char *s = term_to_string_aux(t);

  if (p[0] != '\0') {
    char *tmp = s;
    s = g_strdup_printf("%s\n%s", p, s);
    g_free(tmp);
  }

  g_free(p);
  return s;
}

static term *
merge_insert (term *t, list *l) {
  term *next = t;

  // if list_empty(l) then list_is_end(l) is true
  for (list_goto_head(l); !list_is_end(l); list_goto_next(l)) {
    term *cur = (term *) list_current(l);
    cur->subterm[0] = next;
    cur->height = next->height + 1;
    cur->size = next->size + 1;
    next = cur;
  }

  list_delete(l);
  return next;
}

// Build a term skeleton and merge it with edges and renames.
static term *
term_struct (unsigned nleaves, unsigned cwd, unsigned min_id, list ** add_tab,
	     unsigned add_pos)
{
  if (nleaves == 1)
    return term_create (LABEL, g_random_int_range (0, cwd), min_id);

  //Number of leaves of LST, between 1 and nleaves-1 included.
  unsigned leftweight = g_random_int_range (1, nleaves);
  unsigned rightweight = nleaves - leftweight;

  term *left, *right;

  if (rightweight > nleaves / 2) {
    left = term_struct (leftweight, cwd, min_id, add_tab, add_pos + 1);
    right = term_struct (rightweight, cwd, min_id + leftweight, add_tab,
			 add_pos + leftweight);
  } else {
    left = term_struct (rightweight, cwd, min_id, add_tab, add_pos + 1);
    right = term_struct (leftweight, cwd, min_id + rightweight, add_tab,
			 add_pos + rightweight);
  }

  term *root = term_create(OPLUS, left, right);

  return merge_insert (root, add_tab[add_pos]);
}

// Returns a table of random renames and edges terms
static list** random_add_tab (unsigned nleaves, unsigned nb_terms_to_add,
  unsigned cwd, unsigned proba)
{
  unsigned size = nleaves - 1;
  list** tab = g_malloc0 (size * sizeof(*tab));

  for (unsigned i = 0; i < size; i++)
    tab[i] = list_create();

  if (cwd == 1 || nleaves == 1)
    return tab;

  unsigned color = 1;
  for (int i = 0; i < nb_terms_to_add; i++)
  {
    unsigned pos = g_random_int_range (0, size);
    unsigned term_type = g_random_int_range (0, 100);
    unsigned label_1 = g_random_int_range (0, cwd);
    unsigned label_2 = g_random_int_range (0, cwd - 1);
    if (label_2 >= label_1)
      label_2++;

    term *t;

    if (term_type < proba)
    {
      if (label_2 < label_1)
      {
        unsigned tmp = label_1;
        label_1 = label_2;
        label_2 = tmp;
      }

      t = term_create (EDGE, NULL, label_1, label_2, color++);
    } else
      t = term_create (RENAME, NULL, label_1, label_2);


    list_push_queue(tab[pos], t);
  }

  return tab;
}

void
term_random (gen* g)
{
  unsigned added = g->tsize - MIN(g->nleaves - 1, g->tsize);
  list** add_tab = random_add_tab (g->nleaves, added, g->cwd, g->proba);
  term* t = term_struct (g->nleaves, g->cwd, 0, add_tab, 0);

  g_free(add_tab);
  g->term = t;
  g->tok = true;
}

//Thread function : keep generating and testing terms until the job is done.
void term_prop_thread (gpointer data, gpointer user_data)
{
  gen* gd = (gen*) data;
  gen gencopy = {gd->mutex, gd->nleaves, gd->prop, gd->gram, gd->cwd, gd->tsize,
    gd->proba, gd->prop_wanted, gd->grammar, gd->name, gd->term, gd->graph,
    gd->tok, gd->stop};
  term* t = term_create(LABEL, 0, 0);
  graph* g = graph_create(1);
  char*** found;

  do
  {
    term_delete_rec(t);
    graph_delete(g);

    if (gd->stop || gd->term != NULL)
      return;

    term_random (&gencopy);
    t = gencopy.term;

    g = term_to_graph_threads (t, MAX_THREADS/TERM_WITH_PROP_THREADS);
    found = graph_properties(g);
  } while (!properties_check(found, gd->prop_wanted));

  g_mutex_lock (gd->mutex);
  if (gd->term != NULL)
  {
    g_mutex_unlock(gd->mutex);
    term_delete_rec (t);
    graph_delete (g);
  }
  else
  {
    gd->term = t;
    gd->graph = g;
    g_mutex_unlock (gd->mutex);
  }
}

void term_with_properties (gen* gen)
{
  assert (TERM_WITH_PROP_THREADS <= MAX_THREADS);
  GThreadPool* tpool = g_thread_pool_new (term_prop_thread, NULL,
    TERM_WITH_PROP_THREADS, FALSE, NULL);

  for (unsigned i = 0 ; i < TERM_WITH_PROP_THREADS ; i++)
    g_thread_pool_push (tpool, gen, NULL);

  g_thread_pool_free (tpool, FALSE, TRUE);

  gen->tok = true;
}

term* term_copy (term* t)
{
  term* t2 = g_malloc0 (sizeof(*t2));

  t2->subterm = g_malloc0 (arity(t->type) * sizeof(*t2->subterm));
  for (unsigned i = 0 ; i < arity(t->type) ; i++)
    t2->subterm[i] = term_copy(t->subterm[i]);

  t2->label = g_malloc0 (argc(t->type) * sizeof(*t2->label));
  for (unsigned i = 0 ; i < argc(t->type) ; i++)
    t2->label[i] = t->label[i];

  t2->type = t->type;
  t2->height = t->height;
  t2->size = t->size;

  return t2;
}

//Recursive function that merges an open-ended term with a set of subterms.
void term_merge_rec (term* main, term** subterms, unsigned* current)
{
  assert (main != NULL);

  for (unsigned i = 0 ; i < arity(main->type) ; i++)
    if (main->subterm[i]->type == LABEL_NT)
    {
      term_delete_rec (main->subterm[i]);
      main->subterm[i] = subterms[(*current)++];
    }
    else
      term_merge_rec (main->subterm[i], subterms, current);
}

void term_part_merge (term** main, term** subterms)
{
  assert (main != NULL);
  assert (*main != NULL);

  unsigned pos = 0;

  if ((*main)->type == LABEL_NT)
    {
      term_delete_rec (*main);
      *main = *subterms;
    }
  else
    term_merge_rec (*main, subterms, &pos);
}

unsigned term_part_size (term* t)
{
  assert (t != NULL);
  switch (t->type)
    {
    case LABEL:
      return 1;

    case LABEL_NT:
      return 0;

    case RENAME:
    case EDGE:
      return term_part_size(t->subterm[0]);

    case OPLUS:
      return term_part_size(t->subterm[0]) + term_part_size(t->subterm[1]);

    default:
      assert (t->type == LABEL  || t->type == LABEL_NT || t->type == EDGE ||
	      t->type == RENAME || t->type == OPLUS);
      return -1;
    }
}

//Recursively run through a term and set up labels, heights and sizes correctly.
static void term_prettify_rec (term *t, unsigned *label_id, unsigned *color) {
  assert(t != NULL);
  switch (t->type) {
  case LABEL :
    t->label[1] = (*label_id)++;
    break;

  case EDGE :
    term_prettify_rec(t->subterm[0], label_id, color);
    t->size = t->subterm[0]->size + 1;
    t->height = t->subterm[0]->height + 1;
    t->label[2] = (*color)++;
    break;

  case RENAME :
    term_prettify_rec(t->subterm[0], label_id, color);
    t->size = t->subterm[0]->size + 1;
    t->height = t->subterm[0]->height + 1;
    break;

  case OPLUS :
    term_prettify_rec(t->subterm[0], label_id, color);
    term_prettify_rec(t->subterm[1], label_id, color);
    t->size = t->subterm[0]->size + t->subterm[1]->size + 1;
    t->height = MAX(t->subterm[0]->height, t->subterm[1]->height) + 1;
    break;

  default :
    assert(false);
  }
}

void term_prettify (term* t)
{
  unsigned label_id = 0;
  unsigned color = 1;
  term_prettify_rec(t, &label_id, &color);
}
