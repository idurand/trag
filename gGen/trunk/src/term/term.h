/* term.h */

#ifndef TERM_H
#define TERM_H

#include <stdlib.h>
#include <stdarg.h>

#include "graph.h"
#include "properties.h"

typedef unsigned label;

typedef enum
{
  LABEL,
  LABEL_NT,
  RENAME,
  EDGE,
  OPLUS
} op;

typedef struct term term;

typedef struct generation gen;
struct generation
{
  GMutex* mutex;
  unsigned nleaves;
  bool prop, gram;
  unsigned cwd, tsize, proba;
  p_wanted* prop_wanted;
  void* grammar;
  char* name;
  term* term;
  graph* graph;
  bool tok;
  bool stop;
};

// Create a new term
// Arguments after type are firstly the subtrees, then the labels.
// term_create(EDGE, term, label_0, label_1);
// term_create(RENAME, term, label_0, label_1);
// term_create(OPLUS, term_1, term_2);
// term_create(LABEL, label, node_id);
// ** for grammar usage only **
// term_create(LABEL_NT, rule_id);
extern term *term_create (op type, ...);

// Frees the memory allocated through term_create
extern void term_delete (term * t);

// Frees the memory allocated through term_create and subterms
extern void term_delete_rec (term * t);

// Return the number of sons of a term
extern short term_arity (term * t);

// Return the size of a term
extern unsigned term_size (term * t);

// Return the height of a term
extern unsigned term_height (term * t);

// Return a subterm of a term
extern term *term_son (term * t, unsigned son);

// Return an a color number from graph_colors if t is an EDGE and -1 if not
extern unsigned term_edge_color (term* t);

// Cleans the term of all useless nodes
extern term *term_clean (term * t, unsigned cwd);

// Retrun a human-readable name for a node
extern char *term_node_to_string (term * t);

// Create a new graph from a term
extern graph *term_to_graph (term * t);

// Converts a term into a string, which should be freed with 'g_free'
extern char *term_to_string (term * t, char ***properties);

// Generates a random tree ; if size doesn't match nleaves it will be increased
// (Size represents the number of OPLUS, EDGE, and RENAME in the term.)
extern void term_random (gen* g);

// Create a random term with provided generation data
// This may take some time which is why it can be stopped
extern void term_with_properties (gen* gen);

// Return a carbon-copy of a term
extern term* term_copy (term* t);

// Merge a partial term with a set of subterms
extern void term_part_merge (term** main, term** subterms);

// Return the number of leaves in a partial term
extern unsigned term_part_size (term* t);

// Set the labels, heights and sizes to a correct value
extern void term_prettify (term* t);

#endif // TERM_H
