/* term_parser.h */

#ifndef TERM_PARSER_H
#define TERM_PARSER_H

#include <stdio.h>

#ifndef GRAMMAR_PARSER_H
extern int fileno (FILE * fd);
#endif // GRAMMAR_PARSER_H

#include "term.h"
#include "list.h"

extern int term_lex (void);
extern int term_parse (void);
extern int term_error (char *s);

extern void clear_term_in(void);

extern FILE *term_in;
extern term *term_parsed;
extern unsigned term_cwd;
extern char *term_emsg;
extern list *term_token_stack;
extern int term_line;
extern int term_col;

#endif // TERM_PARSER_H
