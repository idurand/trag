%{
#include <glib.h>
#include <string.h>

#include "term.tab.c"

#define TERM_TAB 8

  list *term_token_stack;

  int term_line;
  int term_col;
%}

%option noyywrap
%option nounput

ID        (([1-9][0-9]*)|0)

%%

"//".*     { /*ignor comments */ }

"add"/"_"  { term_col += 3; return ADD; }
"ren"/"_"  { term_col += 3; return REN; }
"oplus"    { term_col += 5; return OPL; }

{ID} {
  yylval.string = g_strdup(yytext);
  term_col += strlen(yylval.string);
  list_push_head(term_token_stack, yylval.string);
  return IDENTIFIER; }

{ID}/"_" {
  yylval.string = g_strdup(yytext);
  term_col += strlen(yylval.string);
  list_push_head(term_token_stack, yylval.string);
  return LABEL_1; }

"_"/{ID} { term_col++; return yytext[0]; }

"("    { term_col++; return yytext[0]; }
")"    { term_col++; return yytext[0]; }
","    { term_col++; return yytext[0]; }

[ \v\f]  { term_col++; }
"\t"     { term_col += TERM_TAB - (term_col % TERM_TAB); }
"\n"     { term_col = 0; term_line++; }
.        { return yytext[0]; }

%%

void
clear_term_in(void) {
  while (input() != EOF)
    ;
}
