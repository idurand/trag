%{
#include <stdbool.h>

#include "error.h"
#include "term_parser.h"

  term *term_parsed;
  unsigned term_cwd;
  char *term_emsg;
  static err_t term_ecode;
  static list *term_stack;
  static int vertices_id = 0;
  static unsigned color = 1;

  static void free_term(void *o);
%}

%union {
  char *string;
  term *term;
}

%token <string> ADD REN OPL
%token <string> IDENTIFIER LABEL_1

%type <term> term

%start file
%%

file
: init_term term {
  term_parsed = $2;
  list_delete(term_stack);
  list_delete(term_token_stack);
  YYACCEPT;
 }
;

init_term
: {
  term_ecode = SYN_ERR;
  term_parsed = NULL;
  term_emsg = NULL;
  term_cwd = 0;

  term_col = 0;
  term_line = 1;
  vertices_id = 0;
  term_stack = list_create();
  term_token_stack = list_create();
  list_define_free(term_token_stack, g_free);
}
;

term
: ADD '_' LABEL_1 '_' IDENTIFIER '(' term ')' {
  unsigned lbl1 = atol($3);
  unsigned lbl2 = atol($5);

  term_cwd = MAX(lbl1, term_cwd);
  term_cwd = MAX(lbl2, term_cwd);

  if (lbl1 == lbl2) {
    term_ecode = TERM_ADD;
    term_error("term error");
    YYABORT;
  } else
    $$ = term_create(EDGE, $7, lbl1, lbl2, color++);

  list_pop_head(term_stack); // pop $7
  list_push_head(term_stack, $$);

  list_pop_head(term_token_stack); // pop $5
  list_pop_head(term_token_stack); // pop $3
 }
| REN '_' LABEL_1 '_' IDENTIFIER '(' term ')' {
  unsigned lbl1 = atol($3);
  unsigned lbl2 = atol($5);

  term_cwd = MAX(lbl1, term_cwd);
  term_cwd = MAX(lbl2, term_cwd);

  if (lbl1 == lbl2) {
    term_ecode = TERM_REN;
    term_error("term error");
    YYABORT;
  } else
    $$ = term_create(RENAME, $7, lbl1, lbl2);

  list_pop_head(term_stack); // pop $7
  list_push_head(term_stack, $$);

  list_pop_head(term_token_stack); // pop $5
  list_pop_head(term_token_stack); // pop $3
 }
| OPL '(' term ',' term ')' {
  $$ = term_create(OPLUS, $3, $5);

  list_pop_head(term_stack); // pop $5
  list_pop_head(term_stack); // pop $3
  list_push_head(term_stack, $$);
  }
| IDENTIFIER {
  unsigned label = atol($1);

  term_cwd = MAX(label, term_cwd);

  $$ = term_create(LABEL, label, vertices_id++);

  list_pop_head(term_token_stack); // pop $1
  list_push_head(term_stack, $$);
}
| error { YYABORT; }
;

%%

int
term_error(char *s) {
  term_emsg = g_strdup_printf("%s:line %d%s\n", s, term_line,
			      err_msg(term_ecode));

  clear_term_in();
  term_parsed = NULL;
  list_define_free(term_stack, free_term);
  list_delete(term_stack);
  list_delete(term_token_stack);

  return EXIT_FAILURE;
}

static void
free_term(void *o) {
  term_delete_rec((term *) o);
}
