#!/bin/bash

if [ ! -e test_term_rand ] ; then
    make -C ..
    make test_term_rand
    echo -e "----\n"
fi

if [ ! -e test_term_to_graph ] ; then
    make -C ..
    make test_term_to_graph
    echo -e "----\n"
fi

./test_term_rand 10 7 2 50 | tee ~/tmp/plop.t
echo
./test_term_to_graph < ~/tmp/plop.t | tee ~/tmp/plop.dot
dot -Tps ~/tmp/plop.dot > ~/tmp/plop.ps
