/* test_clean.c */

#include <glib.h>
#include <stdio.h>

#include "term.h"
#include "properties.h"

#ifdef TEST_CLEAN

static void
usage(char *s) {
  fprintf(stderr, "\033[33mUsage:\033[0m %s <size> <nb_leaves> <cwd> <proba>\n", s);
  exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[]) {
  if (argc != 5)
    usage(argv[0]);

  char *s = NULL;

  char ***f = g_malloc(8 * sizeof (char **));
  for (int i = 0; i < 8; i++)
    f[i] = g_malloc0(2 * sizeof(char *));

  char *w[8] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
  bool  m[8] = { true, true, true, true, true, true, true, true };

  int c = 0;

  do {
    g_free(s);

    fprintf(stderr, "\r%d", c++);

    term *t = term_random(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]),
			  atoi(argv[4]), NULL);
    graph *g = term_to_graph(t);
    unsigned cwd = graph_cwd(g);
    s = term_to_string(t);
    char ***p = graph_properties(g);
    for (int i = 0; i < 8; i++) {
      g_free(w[i]);
      w[i] = g_strdup(p[i][1]);
    }
    graph_delete(g);

    t = term_clean(t, cwd);
    g = term_to_graph(t);
    p = graph_properties(g);
    for (int i = 0; i < 8; i++) {
      g_free(f[i][1]);
      f[i][1] = g_strdup(p[i][1]);
    }
    term_delete_rec(t);
    graph_delete(g);
  } while (properties_check(m, w, f));

  fprintf(stderr, "\n");
  printf("%s\n", s);

  for (int i = 0; i < 8; i++) {
    g_free(f[i][1]);
    g_free(f[i]);
  }
  g_free(f);

  for (int i = 0; i < 8; i++)
    g_free(w[i]);

  g_free(s);

  return EXIT_SUCCESS;
}

#endif // TEST_CLEAN
