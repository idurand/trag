/* test_dot2png.c */

#include <gvc.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "term.h"
#include "graph.h"
#include "utils.h"

#ifdef TEST_DOT2PNG

int
main (int argc, char *argv[])
{
  term *t = term_random (20, 5, 2, 50, NULL);
  graph *g = term_to_graph (t);
  char *s = graph_to_dot (g);

  graph_delete (g);
  term_delete_rec (t);

  int png = dot_to_png(s);
  
  int c;
  for (int r = read(png, &c, sizeof (char)); r == sizeof (char) && c != EOF; r = read(png, &c, 1))
    write(STDOUT_FILENO, &c, sizeof (char));

  close(png);

  g_free (s);
  
  return EXIT_SUCCESS;
}

#endif // TEST_DOT2PNG
