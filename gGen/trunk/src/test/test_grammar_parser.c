/* test_grammar_parser.c */

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include "grammar_parser.h"
#include "grammar.tab.h"

#ifdef TEST_GRAMMAR_PARSER

int
main (int argc, char *argv[])
{
  int n = grammar_parse ();

  if (n == 0) {
    printf("yep\n");
    grammar_delete (grammar_parsed);
  } else {
    printf("%s", grammar_emsg);
    g_free(grammar_emsg);
  }

  return EXIT_SUCCESS;
}

#endif //TEST_GRAMMAR_PARSER
