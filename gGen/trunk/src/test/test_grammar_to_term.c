/* test_grammar_to_term */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <glib.h>

#include "grammar_parser.h"
#include "grammar.h"
#include "term.h"

#ifdef TEST_GRAMMAR_TO_TERM

int main(int argc, char* argv[])
{
  int n = grammar_parse();
  assert (n == 0);

  grammar* g = grammar_parsed;

  term *t = grammar_to_term(g, 10);
  char *s = term_to_string(t);
  graph *gr = term_to_graph(t);
  char *d = graph_to_dot(gr);

  fprintf(stderr, "%s\n\n", s);

  printf("%s\n", d);

  g_free(d);
  g_free(s);
  graph_delete(gr);
  term_delete_rec(t);
  grammar_delete(g);

  return EXIT_SUCCESS;
}

#endif /* TEST_GRAMMAR_TO_TERM */
