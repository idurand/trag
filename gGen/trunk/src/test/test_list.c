/* test_list.c */

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include "list.h"

#define SIZE 32

static void
print_list (list * l)
{
  for (list_goto_head (l); !list_is_end (l); list_goto_next (l))
    printf ("%s, ", (char *) list_current (l));
  printf ("\n");
}

#ifdef TEST_LIST
int
main (int argc, char *argv[])
{
  char *table[SIZE] = {
    "a", "b", "c", "d", "e", "f", "g", "h",
    "i", "j", "k", "l", "m", "n", "o", "p",
    "q", "r", "s", "t", "u", "v", "w", "x",
    "y", "z", "a", "h", "i", "l", "g", "n"
  };

  list *l = list_create ();
  list_define_free (l, g_free);

  if (list_empty (l))
    printf ("the list is empty\n");

  for (int i = 0; i < SIZE / 2; i++)
    {
      char *tmp = g_strdup (table[i]);
      list_push_head (l, tmp);
    }
  print_list (l);

  for (int i = SIZE / 2; i < SIZE; i++)
    {
      char *tmp = g_strdup (table[i]);
      list_push_queue (l, tmp);
    }
  print_list (l);

  for (int i = 0; i < SIZE / 4; i++)
    list_pop_head (l);
  print_list (l);

  for (int i = 0; i < SIZE / 4; i++)
    list_pop_queue (l);
  print_list (l);

  while (!list_empty (l))
    list_pop_head (l);

  if (list_empty (l))
    printf ("the list is empty\n");

  list_delete (l);

  return EXIT_SUCCESS;
}
#endif //TEST_LIST
