/* test_subgraph.c */

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include "term.h"
#include "graph.h"

#ifdef TEST_SUBGRAPH

int
main(int argc, char *argv[]) {
  term *t1;
  term *t2;

  t1 = term_create(OPLUS,
		   term_create(EDGE,
			       term_create(OPLUS,
					   term_create(LABEL, 0, 0),
					   term_create(LABEL, 1, 1)
					   ),
			       0,
			       1,
			       0
			       ),
		   term_create(LABEL, 0, 2)
		   );

  t2 = term_create(EDGE,
		   term_create(OPLUS,
			       t1,
			       term_create(LABEL, 1, 3)
			       ),
		   0,
		   1,
		   0
		   );

  graph *g1 = term_to_graph(t1);
  graph *g2 = term_to_graph(t2);

  char *dot = subgraph_to_dot(g2, g1);
  printf("%s", dot);

  graph_delete(g1);
  graph_delete(g2);
  term_delete(t1);
  term_delete(t2);
  g_free(dot);

  return EXIT_SUCCESS;
}

#endif // TEST_SUBGRAPH
