/* test_term_parser.c */

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include "term_parser.h"
#include "term.tab.h"

#ifdef TEST_TERM_PARSER

int
main (int argc, char *argv[])
{
  int n = term_parse ();

  if (n == 0) {
    char *s = term_to_string (term_parsed);
    printf ("%s\n", s);
    g_free (s);

    term_delete_rec (term_parsed);
  } else {
    printf("%s", term_emsg);
    g_free(term_emsg);
  }

  return EXIT_SUCCESS;
}

#endif //TEST_TERM_PARSER
