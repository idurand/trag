/* test_term_rand.c */

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include "term.h"

#ifdef TEST_TERM_RAND

static void
usage(char *s) {
  fprintf(stderr, "\033[33mUsage:\033[0m %s <size> <nb_leaves> <cwd> <proba>\n", s);
  exit(EXIT_FAILURE);
}

int
main (int argc, char *argv[])
{
  if (argc != 5)
    usage(argv[0]);

  term *t = term_random (atoi (argv[1]), atoi (argv[2]), atoi (argv[3]),
			 atoi (argv[4]));
  char *s = term_to_string (t);

  printf ("%s\n", s);

  term_delete_rec (t);
  g_free (s);

  return EXIT_SUCCESS;
}

#endif // TEST_TERM_RAND
