/* test_term_to_graph.c */

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include "term.h"

#ifdef TEST_TERM_TO_GRAPH

int
main (int argc, char *argv[])
{
  g_thread_init(NULL);

  term *t = term_random (100000, 10000, 30, 70);

  graph *g = term_to_graph (t);
  /*  char *s = graph_to_dot (g);

  printf ("%s", s);

  g_free (s);
  graph_delete (g);
  term_delete_rec (term_parsed);*/

  return EXIT_SUCCESS;
}

#endif // TEST_TERM_TO_GRAPH
