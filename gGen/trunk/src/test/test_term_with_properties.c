/* test_term_with_properties.c */

#include <glib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

#include "graph.h"
#include "term.h"
#include "properties.h"

#ifdef TEST_TERM_WITH_PROPERTIES

static void
test_handler(int sig) {
  if (sig == SIGTSTP)
    kill(getpid(), SIGUSR1);
}

int
main(int argc, char *argv[]) {
  struct sigaction s;
  s.sa_handler = test_handler;
  sigemptyset(&s.sa_mask);
  s.sa_flags=0;
  sigaction(SIGTSTP, &s, NULL);

  bool  m[8] = { true,  true,  true, true, true, true,  true, true };
  char *w[8] = { "yes", "yes", "no", "no", "no", "yes", "4",  "1"  };

  printf("recherche d'un graphe connexe, sans cycle avec Dmax = 4 et Dmin = 1\n");

  term *t = term_with_properties(200, 7, 3, 70, m, w, NULL);

  if (t == NULL) {
    printf("\nArret !!\n");
    return EXIT_SUCCESS;
  }

  graph *g = term_to_graph(t);
  char *dot = graph_to_dot(g);
  printf("%s", dot);

  term_delete_rec(t);
  graph_delete(g);
  g_free(dot);

  return EXIT_SUCCESS;
}

#endif // TEST_TERM_WITH_PROPERTIES
