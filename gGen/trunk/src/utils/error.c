/* error.c */

#include "error.h"

char *
err_msg(err_t code) {
  switch (code) {
  case SYN_ERR :
    return "";

  case TERM_ADD :
    return ": same labels in 'add'.";
  case TERM_REN :
    return ": same labels in 'ren'.";
  case GRAMMAR_AXIOM :
    return ": axiom rule does not exist.";
  case GRAMMAR_RULE_NB :
    return ": some rules are called but no defined.";
  case GRAMMAR_MULT_DEF :
    return ": multiple definitions of a rule.";
  }

  return "";
}
