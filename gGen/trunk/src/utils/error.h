/* error.h */

#ifndef ERROR_H
#define ERROR_H

typedef enum {
  SYN_ERR,
  TERM_ADD,
  TERM_REN,
  GRAMMAR_AXIOM,
  GRAMMAR_RULE_NB,
  GRAMMAR_MULT_DEF,
} err_t;

extern char *err_msg(err_t code);

#endif // ERROR_H
