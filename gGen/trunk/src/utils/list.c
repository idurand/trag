/* list.c */

#include <glib.h>
#include <assert.h>

#include "list.h"

#define DEFAULT_SIZE 8

typedef struct item item;

struct item
{
  void *data;
  item *next;
};

struct list
{
  item *head;
  item *queue;
  item *current;
  void (*free_object) (void *);
};

list *
list_create (void)
{
  list *l = g_malloc (sizeof (list));

  l->head = NULL;
  l->queue = NULL;
  l->current = NULL;
  l->free_object = NULL;

  return l;
}

void
list_delete (list * l)
{
  while (!list_empty (l))
    list_pop_head (l);
  g_free (l);
}

void
list_define_free (list * l, void (*free_object) (void *))
{
  l->free_object = free_object;
}

bool
list_empty (list * l)
{
  return l->head == NULL;
}

bool
list_is_head (list * l)
{
  return l->current == l->head;
}

bool
list_is_queue (list * l)
{
  return l->current == l->queue;
}

bool
list_is_end (list * l)
{
  return l->current == NULL;
}

void
list_append (list * l1, list * l2)
{
  if (list_empty (l2))
    return;

  if (list_empty (l1))
    {
      l1->head = l2->head;
      l1->queue = l2->queue;
      l1->current = l2->current;
    }
  else
    {
      l1->queue->next = l2->head;
      l1->queue = l2->queue;
    }

  l2->head = NULL;
  l2->queue = NULL;
  l2->current = NULL;
}

void
list_push_head (list * l, void *data)
{
  item *new = g_malloc (sizeof (item));

  new->data = data;
  new->next = l->head;

  if (list_empty (l))
    {
      l->queue = new;
      l->current = new;
    }
  l->head = new;
}

void
list_push_queue (list * l, void *data)
{
  item *new = g_malloc (sizeof (item));

  new->data = data;
  new->next = NULL;

  if (list_empty (l))
    {
      l->head = new;
      l->current = new;
    }
  else
    l->queue->next = new;
  l->queue = new;
}

static void
item_delete (item * i, void (*free_object) (void *))
{
  if (free_object != NULL)
    free_object (i->data);
  g_free (i);
}

void
list_pop_head (list * l)
{
  if (list_empty (l))
    return;

  if (list_is_head (l))
    l->current = l->head->next;
  item *tmp = l->head;
  l->head = l->head->next;

  if (list_empty (l))
    l->queue = NULL;

  item_delete (tmp, l->free_object);
}

void
list_pop_queue (list * l)
{
  if (list_empty (l))
    return;

  if (l->head->next == NULL)
    {
      // there is only one element
      item_delete (l->head, l->free_object);
      l->head = NULL;
      l->queue = NULL;
      l->current = NULL;
    }
  else
    {
      item *tmp = l->head;
      // go to the penultimate element
      while (tmp->next != l->queue)
	tmp = tmp->next;

      if (list_is_queue (l))
	l->current = tmp;
      l->queue = tmp;
      item_delete (tmp->next, l->free_object);
      l->queue->next = NULL;
    }
}

void *
list_current (list * l)
{
  assert (!list_empty (l));
  return l->current->data;
}

void
list_goto_next (list * l)
{
  if (!list_is_end (l))
    l->current = l->current->next;
}

void
list_goto_head (list * l)
{
  l->current = l->head;
}

void
list_goto_queue (list * l)
{
  l->current = l->queue;
}

void **
list_to_table(list *l) {
  unsigned size = 0;
  unsigned max_size = DEFAULT_SIZE;
  void **ret = g_malloc(max_size * sizeof(*ret));

  for (list_goto_head (l); !list_is_end (l); list_goto_next (l))
  {
    if (size == max_size) {
      max_size *= 2;
      ret = g_realloc(ret, max_size * sizeof(*ret));
    }

    ret[size++] = list_current (l);
  }

  ret = g_realloc(ret, (size + 1) * sizeof(*ret));
  ret[size] = NULL;

  return ret;
}
