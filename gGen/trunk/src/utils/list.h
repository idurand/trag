/* list.h */

#ifndef LIST_H
#define LIST_H

#include <stdbool.h>

typedef struct list list;

// create an empty list
extern list *list_create (void);

// free a list memory
extern void list_delete (list * l);

// define the fonction for deleting data
extern void list_define_free (list * l, void (*free_object) (void *));

// return true if an only if the list is empty
extern bool list_empty (list * l);

// return true if and only if the current
// position is the head of the list
// RETURN true IF THE LIST IS EMPTY
extern bool list_is_head (list * l);

// return true if and only if the current
// position is the queue of the list
// RETURN true IF THE LIST IS EMPTY
extern bool list_is_queue (list * l);

// return true if and only if the current
// position is the end of the list
// RETURN true IF THE LIST IS EMPTY
extern bool list_is_end (list * l);

// append l2 to l1 and l2 is emptied ; the cursor is l1 one's
// if l1 is empty : l1 is replaced by l2 and l2 is emptied
// if l2 is empty : noting changes
// THE "free_object" FUNCTION IS NOT AFFECTED
extern void list_append (list * l1, list * l2);

// add an element to the head of the list
// if the list was empty the cursor is placed on this element
extern void list_push_head (list * l, void *data);

// add an element to the end of the list
// if the list was empty the cursor is placed on this element
extern void list_push_queue (list * l, void *data);

// remove an element from the head of the list
// if the list is empty nothing changes
// if the cursor was on the head it is placed on the next element
extern void list_pop_head (list * l);

// remove an element from the end of the list
// if the list is empty nothing changes
// if the cursor was on the queue it is placed on the previous element
// THIS FUNCTION IS O(n)
extern void list_pop_queue (list * l);

// return the data of the current element
// THE LIST MUST NOT BE EMPTY
extern void *list_current (list * l);

// move the cursor to the next element
// if the cursor is at the end of the list nothing changes
extern void list_goto_next (list * l);

// move the cursor to the head of the list
// if the list is empty nothing changes
extern void list_goto_head (list * l);

// move the cursor to the queue of the list
// if the list is empty nothing changes
extern void list_goto_queue (list * l);

// return a NULL eneded table containing
// all the elements of the list
extern void **list_to_table(list *l);

#endif // LIST_H
