/* utils.c */

#include <gvc.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "utils.h"

#define ALPHA_CHARS 26

gchar *
label_to_string (unsigned l)
{
  int strlen = 1;
  for (unsigned l2 = l; l2 >= ALPHA_CHARS; l2 /= ALPHA_CHARS)
    strlen++;

  gchar *str = g_malloc0 ((strlen + 1) * sizeof (*str));
  gchar base = 'a';

  for (int pos = strlen - 1; pos >= 0; pos--)
    {
      str[pos] = base + l % ALPHA_CHARS;
      l /= ALPHA_CHARS;
      l -= 1;
    }

  return str;
}

//NOTE: The following line is needed to compile without warnings with -std=c99.
//(Because fdopen is not c99 standard, even though it is declared in <stdio.h>.)
extern FILE* fdopen (int filedes, const char* mode);

//NOTE: Still don't know how the following works, nor why it is needed.
//  http://old.nabble.com/GraphViz-as-a-library:-agread-problem-td17782558.html
#ifdef G_OS_WIN32
char* workaround(char *buf, int n, FILE* fp)
{
  return fgets(buf,n,fp);
}
#endif

#ifndef G_OS_WIN32
static int
mingwfread(void *chan, char *buf, int bufsize)
{
    return fread(buf, 1, bufsize, (FILE*)chan);
}

static Agraph_t*
readGraph (FILE* fp)
{
    Agiodisc_t mingwIoDisc;
    Agdisc_t disc;

    mingwIoDisc.afread = mingwfread;
    mingwIoDisc.putstr = AgIoDisc.putstr;
    mingwIoDisc.flush = AgIoDisc.flush;
    disc.mem = &AgMemDisc;
    disc.id = &AgIdDisc;
    disc.io = &mingwIoDisc;
    return agread (fp, &disc);
}
#endif

char*
dot_to_png(char* graph)
{
  GVC_t *gvc = gvContext();
  char* dest = g_build_filename (g_get_tmp_dir(), "ggengraph.png", NULL);
  char *args[] = {"", "-Tpng", "-o", dest, NULL};
  int nb_args = 4;
  gvParseArgs(gvc, nb_args, args);

  int dot = g_file_open_tmp(NULL, NULL, NULL);
  FILE *fdot = fdopen(dot, "w+");

  fwrite(graph, sizeof(char), strlen(graph), fdot);

  fseek(fdot, 0, SEEK_SET);
#ifdef G_OS_WIN32
  Agraph_t* g = agread_usergets (fdot, workaround);
#else
  //  Agraph_t *g = agread(fdot);
  Agraph_t *g = readGraph(fdot);
#endif

  fclose(fdot);

  gvLayoutJobs(gvc, g);
  gvRenderJobs(gvc, g);

  gvFreeLayout(gvc, g);
  agclose(g);

  gvFreeContext(gvc);

  return dest;
}

unsigned sigma (unsigned* array, unsigned length)
{
  unsigned sum = 0;
  for (unsigned i = 0 ; i < length ; i++)
    if (array[i] != -1)
      sum += array[i];
    else
      return -1;
  return sum;
}

char **
create_colors(unsigned *max) {
  unsigned nb_colors;
  unsigned partitions = 1;

  do {
    partitions++;
    nb_colors = partitions * partitions * partitions;
  } while (nb_colors - 1 < *max);

  char **tab = g_malloc(nb_colors * sizeof(*tab));
  unsigned step = 255 / (partitions - 1);

  for (unsigned b = 0; b < partitions; b++)
    for (unsigned g = 0; g < partitions; g++)
      for (unsigned r = 0; r < partitions; r++) {
	unsigned i = b * partitions * partitions + g * partitions + r;
	tab[i] = g_strdup_printf("#%2X%2X%2X", r * step, g * step, b * step);

	// set ' '  to '0' because %2X format
	// is not well implemented
	for (int j = 1; j < 6; j += 2)
	  if (tab[i][j] == ' ')
	    tab[i][j] = '0';
      }

  *max = nb_colors;

  return tab;
}
