/* utils.h */

#ifndef UTILS_H
#define UTILS_H

#include <glib.h>

#define FG_COLOR "#00FF00"    // green
#define BG_COLOR "#00000022"  // black transparent

#define MAX_THREADS 32

// Converts an integer label into a string representation.
extern gchar *label_to_string (unsigned l);

// Converts a .dot string to a .png filename (to be freed with g_free)
extern char* dot_to_png (char * graph);

// Return the sum of array's cells
extern unsigned sigma (unsigned* array, unsigned length);

// Return a table of N colors where N is the smallest perfect
// cube such as N > max + 2 (black and white are forbidden)
// WARNING: *max is set to N
extern char **create_colors(unsigned *max);

#endif /* UTILS_H */
