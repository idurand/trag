(in-package :gautograph)

(define-presentation-type spec (&optional (pattern "*.*"))
)

(define-presentation-method accept ((type autograph-spec) stream
                                     (view textual-view)
                                     &key)
  (handler-case
     (let ((spec 
	    (completing-from-suggestions
	     (stream)
	     (loop
	      for spec in (list-values (specs-table (current-specs)))
	      do
	      (suggest (object:name spec) spec)))))
       spec)
     (simple-completion-error ()
       (prog1 nil
	 (format-output "no match~%")))
     (gui-error ()
       (prog1 nil
	 (format-output "error")))))

(define-presentation-method accept ((type automaton) stream
                                     (view textual-view)
                                     &key)
  (handler-case 
     (completing-from-suggestions (stream)
       (loop
           for automaton in (list-values (automata-table (automata (current-spec))))
           do (suggest (name automaton) automaton)))
     (simple-completion-error ()
       (prog1 nil
	 (format-output "no match~%")))
     (gui-error ()
       (prog1 nil
	 (format-output "error")))))

(define-presentation-method accept ((type graph) stream
                                     (view textual-view)
                                     &key)
  (handler-case 
     (completing-from-suggestions (stream)
       (loop
           for graph in (list-values (graphs-table (graphs (current-spec))))
           do (suggest (name graph) graph)))
     (simple-completion-error ()
       (prog1 nil
	 (format-output "no match~%")))
     (gui-error ()
       (prog1 nil
	 (format-output "error")))))


(define-presentation-method accept ((type termset) stream
                                     (view textual-view)
                                     &key)
  (handler-case 
     (completing-from-suggestions (stream)
       (loop
           for termset in (list-values (termsets-table (termsets (current-spec))))
           do (suggest (name termset) termset)))
     (simple-completion-error ()
       (prog1 nil
	 (format-output "no match~%")))
     (gui-error ()
       (prog1 nil
	 (format-output "error")))))

(define-presentation-method accept ((type term) stream
                                     (view textual-view)
                                     &key)
  (handler-case 
     (completing-from-suggestions (stream)
       (loop
           for term in (list-values (terms-table (terms (current-spec))))
           do (suggest (name term) term)))
     (simple-completion-error ()
       (prog1 nil
	 (format-output "no match~%")))
     (gui-error ()
       (prog1 nil
	 (format-output "error")))))

(define-presentation-method accept ((type graph) stream
                                     (view textual-view)
                                     &key)
  (handler-case 
     (completing-from-suggestions (stream)
       (loop
           for graph in (list-values (graphs-table (graphs (current-spec))))
           do (suggest (name graph) graph)))
     (simple-completion-error ()
       (prog1 nil
	 (format-output "no match~%")))
     (gui-error ()
       (prog1 nil
	 (format-output "error")))))

