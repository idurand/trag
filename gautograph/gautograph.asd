;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Gautograph.

(in-package :asdf-user)

(defsystem :gautograph
  :description "Graphical Inferface for Gautograph"
  :version "6.0"
  :author "Irène Durand"
  :depends-on (:autograph :gui-clim)
  :serial t
  :components ((:file "package")
	       (:file "interface-variables")
	       (:file "utils-interface")
	       (:file "functions")
	       (:file "completion")
	       (:file "processes")
	       (:file "toy")
	       (:file "interface")
))

(pushnew :gautograph *features*)
