;; -*- Mode: Lisp; Package: GAUTOGRAPH -*-

;;;  (c) copyright 2015 by
;;;           Irene Durand (irene.durand@u-bordeaux.fr)

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Library General Public License for more details.
;;;
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA  02111-1307  USA.

					;(setf interface:*interface-style* :tty)
#+cmu
(setf ext:*gc-verbose* nil)

(in-package :gautograph)

(defvar *largeur-texte* (1+ 20))
(defvar *processname-pane*)

(defun run-test (name)
  (let ((frame (make-application-frame name)))
    (run-frame-top-level frame)))

(defun gautograph ()
  (init-autograph)
  (run-test 'autograph)
  0)

(make-command-table
 'file-command-table
 :errorp nil
 :menu '(
	 ("Change Data Directory" :command com-change-data-directory)
	 ("Default Data Directory" :command com-default-data-directory)
	 ("Show Data Directory" :command com-show-data-directory)
	 ("Clear all" :command com-clear-all)
	 ("Quit" :command com-quit)))

(make-command-table
 'graph-command-table
 :errorp nil
 :menu '(
	 ("Load Graph (.edge)" :command com-load-graph)
	 ("Rename graph" :command com-rename-graph)
	 ("Save graph" :command com-save-graph)
	 ("Show graph" :command com-show-graph)
	 ("Decomposition" :menu decomposition-command-table)
	 ))

(make-command-table
 'cwd-terms-command-table
 :errorp nil
 :menu '(
	 ("Petersen" :command com-petersen)
	 ("Grunbaum" :command com-grunbaum)
	 ("Mcgee" :command com-mcgee)
	 ("Kn" :command com-kn)
	 ("Pn" :command com-pn)
	 ("Cn" :command com-cn)
	 ("Gnxn" :command com-gnxn)
	 ("Gnxm" :command com-gnxm)
	 ))

(make-command-table
 'term-command-table
 :errorp nil
 :menu '(
	 ("Retrieve term" :command com-retrieve-term)
	 ("Clique width" :command com-clique-width)
	 ("Size" :command com-term-size)
	 ("Depth" :command com-term-depth)
	 ("Compute target" :command com-compute-target)
	 ("Regonize" :command com-recognize-term)
	 ("Enum recognize" :command com-enum-recognize-term)
	 ("Term to graph" :command com-term-to-graph)
	 ("Load term (.cwd)" :command com-load-term)
	 ("Read term" :command com-read-term)
	 ("Clear term" :command com-clear-term)
	 ))

(make-command-table
 'decomposition-command-table
 :errorp nil
 :menu
 '(
   ("Cwd decomposition" :command com-cwd-decomposition)
   ("Cwd optimal decomposition" :command com-cwd-optimal-decomposition)
   ))

(make-command-table
 'menubar-command-table
 :errorp nil
 :menu
 '(
   ("File " :menu file-command-table)
   ("Graph " :menu graph-command-table)
   ("Term " :menu term-command-table)
   ("Automaton " :menu automaton-command-table)
   ("Normal automata " :menu normal-automata-command-table)
   ("Color automata " :menu color-automata-command-table)
   ("Vbits automata " :menu vbits-automata-command-table)
   ("Automata " :menu automata-command-table)
   ("Cwd Terms " :menu cwd-terms-command-table)
   )
 )

(make-command-table
 'normal-automata-command-table
 :errorp nil
 :menu
 '(
   ("kcolorability automaton" :command com-kcolorability-automaton)
   ("k-acyclic-colorability automaton" :command com-k-acyclic-colorability-automaton)
   ("connectedness automaton" :command com-connectedness-automaton)
   ("forest automaton" :command com-forest-automaton)
   ("stable automaton" :command com-stable-automaton)
   ("clique automaton" :command com-clique-automaton)
   ))

(make-command-table
 'vbits-automata-command-table
 :errorp nil
 :menu
 '(("equal automaton" :command com-equal-automaton)
   ("subset automaton" :command com-subset-automaton)
   ("singleton automaton" :command com-singleton-automaton)
   ("edge automaton" :command com-edge-automaton)
   ))

(make-command-table
 'color-automata-command-table
 :errorp nil
 :menu '(
	 ("coloring automaton" :command com-coloring-automaton)
	 ("acyclic-coloring automaton" :command com-acyclic-coloring-automaton)
	 ))

(make-command-table
 'automata-command-table
 :errorp nil
 :menu '(
	 ("Complement automaton" :command com-complement-automaton)
	 ("Reduce automaton" :command com-reduce-automaton)
	 ("Determinize automaton" :command com-determinize-automaton)
	 ("Minimize automaton" :command com-minimize-automaton)
	 ("Intersect automaton" :command com-intersect-automaton)
	 ("Union automaton" :command com-union-automaton)
	 ("Color projection automaton" :command com-color-projection-automaton)
	 ("Vbits projection automaton" :command com-color-vbits-automaton)
	 ("Duplicate automaton" :command com-duplicate-automaton)
	 ("Compile automaton" :command com-compile-automaton)
	 ("Change automaton cwd" :command com-change-automaton-cwd)
	 ("Compute target" :command com-compute-target)
	 ("Compute final target" :command com-compute-final-target)
	 ("Enum compute final target" :command com-enum-compute-final-target)
	 ))

(make-command-table
 'automaton-command-table
 :errorp nil
 :menu '(
	 ("Retrieve automaton" :command com-retrieve-automaton)
	 ("Rename automaton" :command com-rename-automaton)
	 ("Number states" :command com-number-states)
	 ("Name states" :command com-name-states)
	 ("Show automaton signature" :command com-show-automaton-signature)
	 ("Is deterministic?" :command com-is-deterministic)
	 ("Is minimal?" :command com-is-minimal)
	 ("Is empty?" :command com-is-empty)
	 ("Equivalence Classes?" :command com-equivalence-classes)
	 ("Equality automaton?" :command com-equality-automaton)
	 ("Inclusion automaton?" :command com-inclusion-automaton)
	 ("Empty Intersection?" :command com-empty-intersection)
	 ("Load automaton" :command com-load-automaton)
	 ("Clear automaton" :command com-clear-automaton)
	 ("Save automaton" :command com-save-automaton)))

(defun process-name ()
  (normalize-string
   (if *with-processes*
       "processes"
       "no processes")
   *largeur-texte*))

(defmethod display-processname :around ((name string))
  (if *processname-pane*
      (setf (gadget-value *processname-pane*)
	    (normalize-string name *largeur-texte*))
      (format t "~A" name)))

(define-application-frame autograph () ()
  (:panes
   (result-pane
    (make-pane 'application-pane
               :name 'result-pane :display-time nil))
   (graph-pane
    (make-pane 'application-pane
	       :name 'graph-pane :display-time nil))
   (automaton-pane
    (make-pane 'application-pane
	       :name 'automaton-pane :display-time nil))
   (term-pane
    (make-pane 'application-pane
	       :name 'term-pane :display-time nil))
   (interactor-pane :interactor)
   (menu-bar
    (climi::make-menu-bar
     'menubar-command-table))
   (quit
    :push-button
    :label "Quit"
    :activate-callback (lambda (x) (declare (ignore x)) (com-quit)))
   (limit-transitions
    :toggle-button
    :label "Limit transitions"
    :indicator-type :one-of
    :value *limit-transitions*
    :value-changed-callback
    (lambda (gadget value)
      (declare (ignore gadget value))
      (com-toggle-limit-transitions)))
   (clear
    :push-button
    :label "clear" ;:max-width 100 :max-height 100
    :activate-callback #'(lambda (x)
			   (declare (ignore x))
			   (com-clear)))
   (processname-pane
    :text-field
    :value (normalize-string " " *largeur-texte*)))
  (:layouts
   (default
    (vertically (:width 800)
      menu-bar
      (horizontally ()
	(vertically ()
	  (horizontally (:height 100) interactor-pane)
	  (scrolling (:height 150) graph-pane)
	  (scrolling (:height 150 :width 300) term-pane)
	  (scrolling (:height 150) result-pane))
	(scrolling (:width 300) automaton-pane))
      (horizontally ()
	quit clear
	;;	  stop
	+fill+ limit-transitions processname-pane
	;;	  process
	)))))

(defmethod default-frame-top-level :before
    ((frame application-frame)
     &key (command-parser 'command-line-command-parser)
       (command-unparser 'command-line-command-unparser)
       (partial-command-parser
	'command-line-read-remaining-arguments-for-partial-command)
       (prompt "command: "))
  (declare (ignore command-parser))
  (declare (ignore command-unparser))
  (declare (ignore partial-command-parser))
  (declare (ignore prompt))
  (setf *output-stream*
	(climi::find-pane-named *application-frame* 'result-pane)
	*processname-pane*
	(climi::find-pane-named *application-frame* 'processname-pane)
	*automaton-pane*
	(climi::find-pane-named *application-frame* 'automaton-pane)
	*term-pane*
	(climi::find-pane-named *application-frame* 'term-pane)
	*graph-pane*
	(climi::find-pane-named *application-frame* 'graph-pane)))

(defun check-graph ()
  (or (graph (current-spec))
      (format-output "No current GRAPH~%")))

(defun check-automaton ()
  (or (automaton (current-spec))
      (format-output "No current Automaton~%")))

(defun check-term ()
  (or (term (current-spec))
      (format-output "No current Term~%")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Commands
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-autograph-command (com-toggle-limit-transitions :name t) ()
  (toggle-limit-transitions)
  (display-current-spec))

(define-autograph-command (com-show-data-directory :name t) ()
  (format-output "Data directory is ~A~%" *data-directory*))

(define-autograph-command (com-change-data-directory :name t) ()
  (let ((pathname (accept 'pathname :prompt "data directory?"
				    :default (initial-data-directory)
				    :default-type 'pathname
				    :insert-default t)))
    (if (and pathname (probe-file pathname))
	(setf *data-directory* (namestring (probe-file pathname)))
	(progn
	  (format-output "no such directory~%")
	  (setf *data-directory* (initial-data-directory))))
    (format-output "Data directory is ~A~%" *data-directory*)))

(define-autograph-command (com-default-data-directory :name t) ()
  (setf *data-directory* (initial-data-directory))
  (format-output "Data directory is ~A~%" *data-directory*))

(define-autograph-command (com-load-graph :name t) ()
  (let ((*data-directory* (concatenate 'string *data-directory* "Graphs/")))
    (let ((path (accept '(data-file-name "*.edge")
			:prompt "graph filename"
			:display-default nil)))
      (when path
	(let ((graph (load-dimacs-graph path)))
	  (when graph
	    (setf (name graph) (remove-extension (basename (namestring path))))
	    (set-current-graph graph)
	    (unset-current-term)
	    (display-current-spec)))))))

(define-autograph-command (com-petersen :name t) ()
  (set-and-display-current-term (petersen)))

(define-autograph-command (com-grunbaum :name t) ()
  (set-and-display-current-term (grunbaum)))

(define-autograph-command (com-mcgee :name t) ()
  (set-and-display-current-term (mcgee)))

(define-autograph-command (com-kn :name t) ()
  (let ((n (accept 'integer :prompt "n" :display-default nil)))
    (when (and n (plusp n))
      (set-and-display-current-term (cwd-term-kn n)))))

(define-autograph-command (com-pn :name t) ()
  (let ((n (accept 'integer :prompt "n" :display-default nil)))
    (when (and n (plusp n))
      (set-and-display-current-term (cwd-term-pn n)))))

(define-autograph-command (com-rename-graph :name t) ()
  (let ((graph (graph (current-spec))))
    (when graph
      (let ((name 
	      (accept 'string
		      ;;		 :default nil :display-default nil
		      :prompt "graph new name")))
	(when name
	  (remove-graph graph (current-spec))
	  (set-and-display-current-graph
	   (rename-object graph name)))))))

(define-autograph-command (com-show-graph :name t) ()
  (let ((graph (graph (current-spec))))
    (when graph
      (graph-show graph))))

(define-autograph-command (com-save-graph :name t) ()
  (let ((name (accept 'string :prompt "graph filename"
			      :default "save")))
    (let ((filename (concatenate 'string name ".edge")))
      (if (save-dimacs-graph (graph (current-spec)) filename)
	  (format-output "Graph saved in ~A ~%" filename)
	  (format-output "unable to open ~A~%" filename)))))

(define-autograph-command (com-retrieve-graph :name t) ()
  (let ((graph 
	  (accept 'graph
		  :prompt "graph name")))
    (when graph
      (set-and-display-current-graph graph))))

(define-autograph-command (com-quit :name t) ()
  (setf *output-stream* t)
  (frame-exit *application-frame*))

(define-autograph-command (com-clear-term :name t) ()
  (clear-current-term))

(define-autograph-command (com-retrieve-term :name t) ()
  (let ((term 
	  (accept 'term
		  :prompt "term"
		  )))
    (when term
      (set-and-display-current-term term))))

(define-autograph-command (com-load-term :name t) ()
  (let ((name (accept '(data-file-name)
		      :prompt "term filename"
		      :default nil
		      :display-default nil
		      )))
    (when name
      (let ((term (load-file-from-absolute-filename name #'read-cwd-term)))
	(when term
	  (set-and-display-current-term term))))))

(define-autograph-command (com-read-term :name t) ()
  (let ((term-string (accept 'string :prompt "term")))
    (when term-string
      (with-input-from-string (foo term-string)
	(let ((term (read-cwd-term foo)))
	  (when term
	    (set-and-display-current-term term)))))))

(define-autograph-command (com-process :name t) ()
  (setf *with-processes* (not *with-processes*))
  (setf (gadget-value
	 (climi::find-pane-named *application-frame* 'processname-pane))
	(process-name)))

(define-autograph-command (com-clear :name t) ()
  (window-clear *output-stream*))

(define-autograph-command (com-term-size :name t) ()
  (let ((term (term (current-spec))))
    (format-output "The size of the current term is ~A~%" (term-size term))))

(define-autograph-command (com-term-depth :name t) ()
  (let ((term (term (current-spec))))
    (format-output "The depth of the current term is ~A~%" (term-depth term))))

(define-autograph-command (com-stop :name t) ()
  (when *aux-process*
    (clim-sys::destroy-process *aux-process*)
    (format-output "Process stopped~%")
    ))

(define-autograph-command (com-load-automaton :name t) ()
  (let ((name (accept '(data-file-name "*.aut")
		      :prompt "spec filename"
		      :default nil
		      :display-default nil
		      )))
    (format *error-output* "~A~%" name)
    (when name
      (let ((automaton (load-file-from-absolute-filename name #'read-automaton)))
	(when automaton
	  (set-and-display-current-automaton automaton))))))

(define-autograph-command (com-show-automaton-signature :name t) ()
  (when (automaton (current-spec))
    (format-output "~A~%" (signature (automaton (current-spec))))))

(define-autograph-command (com-number-states :name t) ()
  (when (automaton (current-spec))
    (number-states (automaton (current-spec)))
    (display-current-automaton)))

(define-autograph-command (com-name-states :name t) ()
  (when (automaton (current-spec))
    (name-states (automaton (current-spec)))
    (display-current-automaton)))

(define-autograph-command (com-retrieve-automaton :name t) ()
  (let ((automaton 
	  (accept 'automaton
		  ;;		 :default nil
		  ;;		 :display-default nil
		  :prompt "automaton name"
		  )))
    (when automaton
      (set-and-display-current-automaton automaton))))

(define-autograph-command (com-rename-automaton :name t) ()
  (let ((automaton (automaton (current-spec))))
    (when automaton
      (let ((name 
	      (accept 'string
		      ;;		 :default nil :display-default nil
		      :prompt "automaton new name")))
	(when name
	  (remove-automaton automaton (current-spec))
	  (set-and-display-current-automaton
	   (rename-object automaton name)))))))

(define-autograph-command (com-is-empty :name t) ()
  (let ((automaton (automaton (current-spec))))
    (when automaton
      (if (signature-finite-p (signature automaton))
	  (automaton-empty automaton)
	  (format-output "Not decidable for a non finite automaton")))))

(define-autograph-command (com-is-deterministic :name t) ()
  (when (automaton (current-spec))
    (format-output "~A is ~:[not ~;~]deterministic~%"
		   (name (automaton (current-spec)))
		   (deterministic-p (automaton (current-spec))))))

(define-autograph-command (com-is-minimal :name t) ()
  (when (automaton (current-spec))
    (if (deterministic-p (automaton (current-spec)))
	(format-output "~A is ~:[not ~;~]minimal~%"
		       (name (automaton (current-spec)))
		       (minimal-p (automaton (current-spec))))
	(format-output "~A should be determinized first~%" (name (automaton (current-spec)))))))

(define-autograph-command (com-equivalence-classes :name t) ()
  (let ((a (automaton (current-spec))))
    (when a
      (if (and (reduced-p a) (deterministic-p a))
	  (format-output "Equivalence classes of ~A: ~A~%"
			 (name (automaton (current-spec)))
			 (equivalence-classes (automaton (current-spec))))
	  (format-output
	   "~A should be reduced and determinized first~%" (name a))))))

(define-autograph-command (com-intersect-automaton :name t) ()
  (when (automaton (current-spec))
    (let ((aut2 (accept 'automaton
			:prompt "intersection with automaton")))
      (when aut2
	(set-and-display-current-automaton
	 (intersection-automaton (automaton (current-spec)) aut2))))))

(define-autograph-command (com-inclusion-automaton :name t) ()
  (when (automaton (current-spec))
    (let ((aut2 (accept 'automaton
			;;			:default nil :display-default nil
			:prompt "inclusion in automaton"
			)))
      (when aut2
	(multiple-value-bind (res term)
	    (inclusion-automaton (automaton (current-spec)) aut2)
	  (if res
	      (format-output "L(~A) included in L(~A)~%"
			     (name (automaton (current-spec)))
			     (name aut2))
	      (format-output "L(~A) not included in L(~A) witnessed by ~A~%"
			     (name (automaton (current-spec)))
			     (name aut2) term)))))))

(define-autograph-command (com-equality-automaton :name t) ()
  (let ((aut2 (accept 'automaton
		      ;;		      :default nil :display-default nil
		      :prompt "equality with automaton")))
    (when aut2
      (let ((aut1 (automaton (current-spec))))
	(multiple-value-bind (res term)
	    (equality-automaton aut1 aut2)
	  (let ((name1 (name aut1))
		(name2 (name aut2)))
	    (if res
		(format-output "L(~A) = L(~A) ~%"
			       (name aut1)
			       (name aut2))
		(if term
		    (format-output
		     "L(~A) not equal to L(~A) witnessed by ~A~%"
		     name1 name2 term)
		    (format-output
		     "~A and ~A do not have the same signature~%"
		     name1 name2)))))))))

(define-autograph-command (com-empty-intersection :name t) ()
  (let ((a (automaton (current-spec))))
    (when a
      (let ((aut2 (accept 'automaton
			  :prompt "emptiness of intersection with automaton")))
	(when aut2
	  (multiple-value-bind (res term)
	      (intersection-emptiness a aut2)
	    (if res
		(format-output
		 "L(~A) inter L(~A) is empty ~%"
		 (name a)
		 (name aut2))
		(format-output
		 "L(~A) inter L(~A) not empty witnessed by ~A~%"
		 (name a)
		 (name aut2) term))))))))

(define-autograph-command (com-union-automaton :name t) ()
  (when (automaton (current-spec))
    (let ((aut2 (accept 'automaton
			;;			:default nil :display-default nil
			:prompt "union with automaton"
			)))
      (when aut2
	(set-and-display-current-automaton
	 (union-automaton (automaton (current-spec)) aut2))))))

(define-autograph-command (com-complement-automaton :name t) ()
  (when (automaton (current-spec))
    (process-complement)))

(define-autograph-command (com-reduce-automaton :name t) ()
  (when (automaton (current-spec))
    (if (reduced-p (automaton (current-spec)))
	(format-output "~A already reduced~%" (name (automaton (current-spec))))
	(process-reduce))))

(define-autograph-command (com-termset-automaton :name t) ()
  (when (termset (current-spec))
    (let ((aut (aut-termset (termset (current-spec)))))
      (set-and-display-current-automaton aut))))

(define-autograph-command (com-duplicate-automaton :name t) ()
  (let ((a (automaton (current-spec))))
    (when a
      (let ((aut (duplicate-automaton a)))
	(rename-object aut (compose-name "copy-of-" (name a)))
	(set-and-display-current-automaton aut)))))

(define-autograph-command (com-determinize-automaton :name t) ()
  (when (automaton (current-spec))
    (if (deterministic-p (automaton (current-spec)))
	(format-output "~A already deterministic~%"
		       (name (automaton (current-spec))))
	(process-determinize))))

(define-autograph-command (com-minimize-automaton :name t) ()
  (when (automaton (current-spec))
    (if (deterministic-p (automaton (current-spec)))
	(if (minimal-p (automaton (current-spec)))
	    (format-output "~A already minimal~%"
			   (name (automaton (current-spec))))
	    (process-minimize))
	(format-output "~A should be determinized first~%"
		       (name (automaton (current-spec)))))))

(define-autograph-command (com-color-projection-automaton :name t) ()
  (let ((automaton (automaton (current-spec))))
    (when automaton
      (if (symbols::color-signature-p (signature automaton))
	  (set-and-display-current-automaton 
	   (constants-color-projection automaton))
	  (format-output
	   "~A not a color automaton, no color-projection possible~%"
	   (name (automaton (current-spec))))))))

(define-autograph-command (com-vbits-projection-automaton :name t) ()
  (let ((automaton (automaton (current-spec))))
    (when automaton
      (if (symbols::vbits-signature-p (signature automaton))
	  (set-and-display-current-automaton 
	   (vbits-projection-automaton automaton))
	  (format-output
	   "~A not a vbits automaton, no vbits-projection possible~%"
	   (name (automaton (current-spec))))))))

(define-autograph-command (com-epsilon-closure :name t) ()
  (when (automaton (current-spec))
    (let ((aut (epsilon-closure-automaton
		(automaton (current-spec)))))
      (set-and-display-current-automaton aut))))

(define-autograph-command (com-save-automaton :name t) ()
  (when (automaton (current-spec))
    (let ((*limit-transitions* nil))
      (write-object-to-file (automaton (current-spec))
			    (name (automaton (current-spec)))
			    #'show))))

(define-autograph-command (com-clear-automaton :name t) ()
  (clear-current-automaton)
  (window-clear *automaton-pane*))

(define-autograph-command (com-clear-all :name t) ()
  (set-and-display-current-spec (get-spec "empty")))

(define-autograph-command (com-cwd-decomposition :name t) ()
  (let ((graph (graph (current-spec))))
    (when graph
      (let ((term (cwd-decomposition graph)))
	(when term
	  (set-and-display-current-term term))))))

(define-autograph-command (com-cwd-optimal-decomposition :name t) ()
  (let ((graph (graph (current-spec))))
    (when graph
      (let ((term (cwd-optimal-decomposition graph)))
	(when term
	  (set-and-display-current-term term))))))

(define-autograph-command (com-clique-width :name t) ()
  (let ((term (term (current-spec))))
    (when term
      (format *output-stream* "Clique width: ~A~%" (clique-width::clique-width term)))))

(define-autograph-command (com-term-to-graph :name t) ()
  (let ((term (term (current-spec))))
    (when term
      (set-and-display-current-graph (cwd-term-to-graph term)))))

(define-autograph-command (com-kcolorability-automaton :name t) ()
  (let ((k (accept 'integer :prompt "Number of colors")))
    (when k
      (let ((automaton (kcolorability-automaton k)))
	(set-and-display-current-automaton automaton)))))

(define-autograph-command (com-k-coloring-automaton :name t) ()
  (let ((k (accept 'integer :prompt "Number of colors")))
    (when k
      (let ((automaton (coloring-automaton k)))
	(set-and-display-current-automaton automaton)))))

(define-autograph-command (com-k-acyclic-colorability-automaton :name t) ()
  (let ((k (accept 'integer :prompt "Number of colors")))
    (when k
      (let ((automaton (k-acyclic-colorability-automaton k)))
	(set-and-display-current-automaton automaton)))))

(define-autograph-command (com-subset-automaton :name t) ()
  (set-and-display-current-automaton (subset-automaton 2 1 2)))

(define-autograph-command (com-equal-automaton :name t) ()
  (set-and-display-current-automaton (equal-automaton 2 1 2)))

(define-autograph-command (com-stable-automaton :name t) ()
  (set-and-display-current-automaton (stable-automaton)))

(define-autograph-command (com-clique-automaton :name t) ()
  (set-and-display-current-automaton (clique-automaton)))

(define-autograph-command (com-singleton-automaton :name t) ()
  (set-and-display-current-automaton (singleton-automaton)))

(define-autograph-command (com-connectedness-automaton :name t) ()
  (set-and-display-current-automaton (connectedness-automaton)))

(define-autograph-command (com-edge-automaton :name t) ()
  (set-and-display-current-automaton (edge-automaton 2 1 2)))

(define-autograph-command (com-forest-automaton :name t) ()
  (set-and-display-current-automaton (forest-automaton)))

(define-autograph-command (com-coloring-automaton :name t) ()
  (let ((nb-colors (accept 'integer :prompt "Number of colors")))
    (when nb-colors
      (let ((automaton (coloring-automaton nb-colors)))
	(set-and-display-current-automaton automaton)))))

(define-autograph-command (com-acyclic-coloring-automaton :name t) ()
  (let ((nb-colors (accept 'integer :prompt "Number of colors")))
    (when nb-colors
      (let ((automaton (acyclic-coloring-automaton nb-colors)))
	(set-and-display-current-automaton automaton)))))

(define-autograph-command (com-compile-automaton :name t) ()
  (let ((automaton (automaton (current-spec))))
    (when automaton
      (if (signature-finite-p (signature automaton))
	  (set-and-display-current-automaton (compile-automaton automaton))
	  (format *output-stream*
		  "An automaton with an infinite signature cannot be compiled")))))

(define-autograph-command (com-change-automaton-cwd :name t) ()
  (let ((automaton (automaton (current-spec))))
    (let ((cwd (accept 'integer :prompt "cwd [0 for infinite]")))
      (set-and-display-current-automaton (change-automaton-cwd automaton cwd)))))

(define-autograph-command (com-compute-target :name t) ()
  (let ((automaton (automaton (current-spec)))
	(term (term (current-spec))))
    (when (and automaton term)
      (format-output
       "The target is ~A~%" (compute-target term automaton)))))

(define-autograph-command (com-compute-final-target :name t) ()
  (let ((automaton (automaton (current-spec)))
	(term (term (current-spec))))
    (when (and automaton term)
      (format-output
       "The target is ~A~%" (compute-final-target term automaton)))))

(define-autograph-command (com-enum-compute-final-target :name t) ()
  (let ((automaton (automaton (current-spec)))
	(term (term (current-spec))))
    (when (and automaton term)
      (format-output
       "The target is ~A~%" (compute-final-target term automaton :enum t)))))

(define-autograph-command (com-recognize-term :name t) ()
  (let ((automaton (automaton (current-spec)))
	(term (term (current-spec))))
    (when (and automaton term)
      (format-output
       "The current term is ~Arecognized by ~A~%"
       (if (recognized-p term automaton) "" "not ")
       (name automaton)))))

(define-autograph-command (com-enum-recognize-term :name t) ()
  (let ((automaton (automaton (current-spec)))
	(term (term (current-spec))))
    (when (and automaton term)
      (format-output
       "The current term is ~Arecognized by ~A~%"
       (if (enum-recognized-p term automaton) "" "not ")
       (name automaton)))))

;;;;; End of Commands
