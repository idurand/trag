(in-package :gautograph)

(defun process-cwd-decomposition ()
  (let ((graph (graph (current-spec))))
    (when graph
      (in-process
       (setf (term (current-spec)) (cwd-decomposition graph))
       "CWD-DECOMPOSITION"))))
