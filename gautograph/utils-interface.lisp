(in-package :gautograph)

(defun display-graph (graph &key (prefix "CURRENT GRAPH") (clear t))
  (display-object
   graph
   *graph-pane*
   :prefix prefix
   :clear clear))

(defun display-current-graph ()
  (display-graph (graph (current-spec))))

(defmethod terms::display-spec (spec)
  (when spec
    (when (graph spec)
      (display-graph (graph spec)))
    (when (term spec)
      (display-term (term spec)))
    (when (automaton spec)
      (display-automaton (automaton spec)))
    (when (termset spec)
      (display-termset (termset spec)))))

(defun set-and-display-current-graph (graph)
  (set-current-graph graph)
  (display-current-graph))
