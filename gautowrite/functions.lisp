(in-package :gautowrite)

(defun wn-inclusion (&key (bullet nil))
  (let ((aut-c-extra (seqsys-aut-c (trs (current-spec)) :bullet bullet :extra t)))
    (multiple-value-bind (res term)
	(inclusion-automaton
	 (automaton-without-extra-symbol aut-c-extra)
	 (seqsys-aut-c (trs (current-spec)) :bullet bullet))
	(if res
	    (if bullet
		(format-output "WNo(S,G,F) == WNo(S,F)")
	    (format-output "WN(S,G,F) == WN(S,F)"))
	    (progn
	    (if bullet
		(format-output
			"WNo(S,G,F) not included in WNo(S,F)")
		(format-output
			"WN(S,G,F) not included in WN(S,F)"))
	    (format-output " witnessed by ~A" term))))))

(defun approx-closed ()
  (if (rules-closed-p (get-approx-rules (trs (current-spec))))
      (format-output "approx closed~%")
      (format-output "approx not closed~%")))

(defun approx-arbitrary ()
  (let ((arbitrary (arbitrary)))
    (format-output "approx trs ")
    (if arbitrary
	(format-output
		"arbitrary witnessed by ~A" arbitrary)
        (format-output "not arbitrary"))))
