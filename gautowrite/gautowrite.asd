;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

;;; ASDF system definition for Gautowrite.

(in-package :asdf-user)

(defsystem :gautowrite
  :description "Graphical Inferface for autowrite"
  :version "6.0"
  :author "Irène Durand"
  :licence "Public Domain"
  :depends-on (:gui-clim :autowrite)
  :serial t
  :components ((:file "package")
	       (:file "interface-variables")
	       (:file "utils-interface")
	       (:file "functions")
	       (:file "toy")
	       (:file "completion")
	       (:file "processes")
	       (:file "interface")
))

(pushnew :gautowrite *features*)
