(in-package :common-lisp-user)

(defpackage :gautowrite
  (:use
   :clim-lisp
   :clim
   :clim-extensions
   :input-output
   :gui-clim
   :general
   :symbols
   :names
   :object
   :terms
   :trs
   :termwrite
   :termauto
   :autowrite
   ))
