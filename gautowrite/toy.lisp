;; -*- Mode: Lisp; Package: AUTOWRITE -*-

;;;  (c) copyright 2001 by
;;;           Irène Durand (idurand@labri.fr)

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Library General Public
;;; License as published by the Free Software Foundation; either
;;; version 2 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Library General Public License for more details.
;;;
;;; You should have received a copy of the GNU Library General Public
;;; License along with this library; if not, write to the
;;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA  02111-1307  USA.


;; faire que les ) trailing (ou manquantes) ne bloquent pas l'analyse
;; tuer les processus avant de tout quitter

(in-package :gautowrite)

;(defun get-absolute-filename-from-browser ()
;  (car (clim-demo::file-selector-test)))

;;; trs
(defun sequentiality (sys &key (deterministic t) (extra nil) (automata t))
  (block nil
    (unless automata
      (multiple-value-bind (succeeded res)
	  (try-to-check-seq-with-properties sys)
	(when succeeded
	  (display-sequential res sys :extra extra)
	  (return))))
    (let (aut-c aut-d)
      (with-time-to-output-stream
       (progn
	 (setf aut-c
	       (seqsys-aut-c sys :extra extra :bullet t
			     :deterministic deterministic))
	 (format-output "~A" aut-c)))
      (with-time-to-output-stream
       (progn
	 (setf aut-d
	       (seqsys-aut-d
		sys
		:deterministic deterministic
		:partial t
		:extra extra))
	 (format-output "~A" aut-d)))
    (display-sequentiality-results sys aut-d :extra extra))))

(defun trs-fb-to-constructor (sys &optional (elimination nil))
  (when sys
    (cond
      ((constructor-p (rules-of sys))
       (format-output "~A is already constructor~%" (name sys)))
      ((not (forward-branching-index-tree sys))
       (format-output "~A is not forward-branching~%" (name sys)))
      (t (set-and-display-current-trs (salinier sys elimination))))))

;;; reduction
(defun apply-reduction (&key needed (parallel nil))
  (when (and (term (current-spec)) (trs (current-spec)))
    (if (seqsys-merge-signature (trs (current-spec)) (signature (term (current-spec))))
	(let ((rp (redex-positions
		   (term (current-spec))
		   (trs (current-spec)))))
	  (if (endp rp)
	      (format-output "already in normal form~%")
	      (let ((nrp (if needed
			     (needed-redex-positions
			      (term (current-spec))
			      (trs (current-spec))
			      :extra (in-signature-extra
				      (signature
				       (term (current-spec)))))
			     (outer-positions rp))))
		(if (endp nrp)
		    (format-output "no needed-redex~%")
		    (progn
		      (if parallel
			  (parallel-reduction-step nrp)
			  (reduction-step (car nrp)))
		      (window-clear *term-pane*)
		      (display-term
		       (previous-term (current-spec)) :prefix "PREVIOUS TERM")
		      (display-term (term (current-spec)) :clear nil))
		))))
	(format *error-output* "incompatible signatures~%"))))

(defun apply-reduction-nf (&key (needed nil))
  (when (and (term (current-spec)) (trs (current-spec)))
    (if (seqsys-normal-form (term (current-spec)) (trs (current-spec)))
	(format-output "already in normal form~%")
	(progn
	  (let* ((initial-term (term (current-spec)))
		 (fun
		  (if needed
		      (lambda (term trs)
			(needed-redex-positions
			 term
			 trs
			 :extra
			 (in-signature-extra
				 (signature initial-term))))
		      #'outermost-redex-positions)))
	    (do
	     ((nrp
	       (funcall fun (term (current-spec)) (trs (current-spec)))
	       (funcall fun (term (current-spec)) (trs (current-spec)))))
	     ((or (endp nrp) (compare-object (term (current-spec))
					     (previous-term (current-spec)))) t)
	      (if needed
		  (reduction-step (car nrp) :tr t)
		  (parallel-reduction-step nrp :tr t))
	      (process-yield))
	    (when
		(and
		 (redex-positions (term (current-spec)) (trs (current-spec)))
		   (not (contains-bullet-p (term (current-spec)))))
		(if
		  (compare-object (term (current-spec)) (previous-term (current-spec)))
		 (format-output
		  "looping computation~%")
		 (format-output
		  "no needed redex~%")))
	      (window-clear *term-pane*)
	      (display-term initial-term :prefix "INITIAL TERM")
	      (display-term (term (current-spec)) :prefix "FINAL TERM" :clear nil))))))

(defun automaton-d (sys &key (deterministic t) (extra nil) (partial nil))
  (when sys
    (let ((aut (seqsys-aut-d sys :deterministic deterministic :extra extra :partial partial)))
      (set-and-display-current-automaton aut))))

(defun automaton-d-extra (sys)
  (automaton-d sys :extra t))

(defun automaton-c (sys &key (deterministic t) (bullet nil) (extra nil))
  (when sys
    (set-and-display-current-automaton
     (seqsys-aut-c sys
		   :bullet bullet
		   :extra extra
		   :deterministic deterministic))))

(defun automaton-c-s (trs termset &key (deterministic t))
  (when (and trs termset)
    (seqsys-change-aut-t trs (aut-termset termset))
    (set-and-display-current-automaton
     (seqsys-aut-c trs :deterministic deterministic))))

(defun automaton-c-a (trs automaton &key (deterministic t))
  (when (and trs automaton)
    (seqsys-change-aut-t trs automaton)
    (set-and-display-current-automaton
     (seqsys-aut-c-t trs :deterministic deterministic))))
