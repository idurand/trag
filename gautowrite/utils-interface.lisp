(in-package :gautowrite)

(defun display-approximation (trs)
  (window-clear *approx-pane*)
  (when trs
    (format *approx-pane* "~A of ~A~%~A"
	    (symbol-name (seqsys-approx trs))
	    (name trs)
	    (get-approx-rules trs))
    (scroll-extent *approx-pane* 0 0)))

(defun display-current-approximation ()
  (display-approximation (trs (current-spec))))

(defun display-trs (trs)
  (when trs
    (window-clear *trs-pane*)
    (format *trs-pane* "CURRENT TRS ~A~%~A" (name trs) (rules-of trs))
    (let ((additional-signature
	    (signature-difference
	     (signature trs)
	     (signature (rules-of trs)))))
      (unless (signature-empty-p additional-signature)
	(format *trs-pane*
		"additional symbol~:[~;s~]: ~A~%"
		(> (nb-symbols additional-signature) 1) additional-signature))
      (scroll-extent *trs-pane* 0 0))))

(defun display-current-trs ()
  (display-trs (trs (current-spec))))

(defmethod terms::display-spec (spec)
  (when spec
    (display-trs (trs spec))
    (display-approximation (trs spec))
    (display-automaton (automaton spec))
    (display-tautomaton (tautomaton spec))
    (display-termset (termset spec))
    (display-term (term spec))
    (window-clear *output-stream*)))

(defun set-and-display-current-trs (trs)
  (set-current-trs trs)
  (display-current-trs)
  (display-current-approximation))

(defun clear-current-trs ()
  (setf (trs (current-spec)) nil))
